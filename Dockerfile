FROM ubuntu:xenial

RUN apt-get update && apt-get install openjdk-8-jre -y

ADD server/target/server-7.0.jar .
EXPOSE 8080
CMD ["java", "-jar", "server-7.0.jar"]

