# **Merge Details (Merger only)**  

**Story Number:**  

  
**Description of implementation**:  

  
**AC's Covered:**  

  
**Agilefant:**  
- [ ] Have you updated the agilefant tasks to pending?  

  

# **Merge Request review checklist (Reviewer only)**  

**Javadoc**
- [ ] Is there a **_sufficient_** amount of Javadoc?  
- [ ] Is it up to date?  
- [ ] Does all Javadoc have the **_correct_** @params, @return, etc?
- [ ] Is it actually **_helpful_**?
- [ ] Does it make sense?

**Acceptance criteria**
- [ ] Do **_all_** mentioned AC's in the merge request pass?
- [ ] Would you as a customer be **_satisfied_** with the current implementation?
- [ ] Are there extra AC's that you should test that are closely related but not mentioned in the merge request?

**Code**
- [ ] Is it **_readable_**?
- [ ] Are individual lines an **_appropriate_** size?
- [ ] Any code smells?

**Testing**
- [ ] Is there **_sufficient_** testing?
- [ ] Are the tests actually performing **_useful_** tests? i.e. Not just AssertTrue(true)
- [ ] Do the tests have an appropriate **_coverage_**?
- [ ] Are tests **_unique_** and not repeat the same test?

**Feedback**
- [ ] Are there any suggestions, improvements, different approaches, etc. which could be used in future?  

  
**Agilefant:**  
- [ ] Have you updated the agilefant tasks to ready?  