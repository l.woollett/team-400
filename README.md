# Installation

Assumes you have Maven installed. If you need to install Maven on a Unix machine run the command `sudo apt-get install mvn`.

### Building the jar
1. Download the zip the repository with the tag sprint_X.Y where X is the current sprint and Y is the latest version.
2. Extract the files to the desired directory.
3. In a terminal, change the current directory to the root of the project where `pom.xml` is contained. (It will be the one called drmoe and the other POM's will be client and server respectively)
4. Run the command `mvn package`.
5. The jars are now built in the `target` folder. 
6. Run the command `java -jar ./target/client-X.0.jar` to start the application. (Where X is the sprint release number)
6. Run the command `java -jar ./target/server-X.0.jar` to start the server. (Where X is the sprint release number)

Alternatively, you can move the jar file to another desired directory. To then run the jar, navigate to the same directory in a terminal and execute the command `java -jar clientX.Y.jar`.


# Licenses
### Gson
Gson was used to save and save objects to a JSON file.

Copyright 2008 Google Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


***
### Ant
Ant's was used to parse the value entered into the command line and identify the desired command as well as their arguments.

Copyright 2018 Apache Ant

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.


***
### JLine
Was used to capture the terminal and allow the user to trace through their command history.

Copyright (c) 2002-2017, the original author or authors.

This software is distributable under the BSD license. See the terms of the
BSD license in the documentation provided with this software.

    http://www.opensource.language governing permissions and limitations under the License.org/licenses/bsd-license.php


***
### Common CLI
Commons CLI was used to parse the input from the command line, retrieve values for arguments and create the usage

Copyright 2017 Apache Commons.

Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.


***
### ControlsFX
ControlsFX was used for their controls that allow multiple selection boxes and lists in JavaFX GUI's

 Copyright (c) 2013, 2014, ControlsFX
 
licensed under the ControlsFX license. You may not use this file except in complience with their license. A copy of the license is obtainable from [here.](https://bitbucket.org/controlsfx/controlsfx/src/default/license.txt) Alternatively:

    https://bitbucket.org/controlsfx/controlsfx/src/default/license.txt

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.




