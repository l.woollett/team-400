## General Information
To run the application, in a terminal navigate to the directory which has the application jar. The run

    Java -jar /client/client(version number).jar

The application will then start the GUI. To use the terminal, login to an admin account and then click the terminal button on the menu bar. Pressing the UP and DOWN arrows will let the user to trace through their history of commands.

## GUI Usage - Normal User
### Creating a profile.
To create a profile, first run the application with the jar provided. This can either be launched with the command line command `java -jar //PATH_TO_JAR//` or by double clicking the jar file provided.  
Next you will see a screen saying welcome. Please click the button that says "Sign Up".  
Now fill out your user information, making sure to fill in the required fields(Denoted with a *) and make sure your username is easy to remember, as this will be how you sign in to the application.  
Next click create user.  
Congratulations! You are now signed up for the Organ Donation Management System. You should have been redirected to a page that shows you your user information!

### Signing in to your profile. 
To sign into your newly created profile, please launch the application via the the method described in create a profile. Once you are on the "Welcome" screen, please type your username into the text field and then click the "Login" button. You should now be seeing a screen that shows your user information. If you don't, you have probably entered your username wrong, or have not signed up to the Organ Donation Management System yet.  
Once in the profile screen, you are now able to view all the information that the organ donation management system has on you. Please familiarize yourself with the tabs and what is in them. 

### Editing your profile.  
To edit your profile, first sign into the application if you haven't already. Then on the top bar, click file, then edit.  
From here, you can edit lots of information that the System has on you. You can register and unregistered as an organ donor, and change some of the basic information about yourself.  
Once you are done with editing your profile, please click the 'Apply Changes' button, to save the changes that you have made to yourself. 

### Editing the organs you have set yourself to donate.
To edit your selected donation organs, first sign into the application, and open the edit screen.  
Next, click on the "Donating Organs" tab.  
You will now be shown a list of Organs that are available to donate. 
If a staff member has set you to receive organs, they will be highlighted in orange. Once you have decided on what organs you want to donate, please click the "Apply Changes" button. 

### Adding medications to your user profile.
As per before, please sign into your profile, and open up the editing screen.  
Now click the tab labeled "Medications" You should see a text field up the top of the screen.  Start typing in a name of a mediation that you are taking, and it should come up with a suggestion near what you are typing. Once you have found your medication in the drop down, click the button beside it to move it into the current medications list.  
If this is actually a previous medication that you have taken, you can click the button beside the list to move it to previous medications.  
If you have added this medication wrongly, you can click the delete button in between the two lists.  

### Viewing medication information. 
To get to the screen where you can view medication, please sign in to your profile, click the medications tab, and then using the menu at the top of the screen, click file > edit. 
To view the information on a medication, please click on the medication. This will show it's active ingredients in the active ingredients text field. To see what symptoms might occur if you mix two drugs, please click the first drug you would like to compare, and then, while holding `CTRL` on your keyboard, click the second drug you would like to compare.  
This comparison will be shown in the 'Drug interactions' text pane. 

### Viewing the history of your profile. 
To view the history of your profile, please sign in to the application, and then click on the tab that says "History". From here, you can see all the changes that have been made to your profile since your profile had been added to the database. 

### Viewing the medical history of your profile.
To view the medical history of your profile, please sign in to the application, and then click on the tab that says "Medical History". From here, you can see your current diseases, and your past diseases. 

### Viewing your Procedures.
To view the procedures tat you are signed up for or have previously had, please sign in to the application, and then click on the tab that says "Procedures". From here, you can see your pending procedures, their date and summary, and your past procedures, with their date and summary. 

### Logging out from your profile.
To logout of the application, please click the file button on the menu bar, and then click logout. You should now be on the application main page.

## User Guide for an Clinician
### Logging in.
To login to an Clinician, first run the application with the jar provided. This can either be launched with the command line command `java -jar //PATH_TO_JAR//` or by double clicking the jar file provided. Next, on this main screen, you should see a text box. Please enter the username that you have been given into this text box and then click login. 

### Editing your profile.
To edit your Clinician profile, first login to your profile from the main application window. Next, please click file then click edit. You will now be able to edit all of your details, except for your username.  
Once you have finished editing your profile, click apply. Your changes have now been updated on the server.

### Searching for an user. 
To search for an user, first sign into your profile. Next, click the tab that says "Search" You can now search users via a bunch of different criteria. 
- `name:` This will search by the user's name.
- `age:` Here you can search for a user's age, you can do searches like `40-60` as well. 
- `Results To Show:` You can choose how many user's will appear on this page.
- `Clear Filters:` Clears all of the combo boxes, so you can start your search fresh.
- `Birth Gender:` Search by the user's birth gender.
- `Preferred Gender:` Search by the user's preferred gender. 
- `Region:` Find a user based on the region of New Zealand that they live in.
- `Donor / Receiver:` You can choose to show only donors, only receivers, or a mix of both.
- `Donating Organs:` Here you can choose to show only donors that have `x` organ. 
- `Receiving Organs:` Here you can choose to only show receivers that want `y` organ.   
To open a profile from this search bar, double click on the profile.   
You can use the `>>>` and `<<<` to move between pages of users.   

### Searching the Transplant Waiting list.
Using the transplant waiting list is almost exactly the same as using the profile search tab. However you can only filter by 2 methods, and these are as follows: 
- `Filter By Organ:` Filter by the organ that the user needs to have transplanted into them.
- `Filter By Region:` Filter by the region that the user lives in.
- `Clear Filters:` This clears the previous combo boxes, so that you can start your search anew or look at all the users.  
You can also double click on a user here to open their profile. 

### Removing a user from the Transplant Waiting list. 
To remove a user from the Transplant Waiting List, there are two ways of doing this. You can either right click on their profile, and click remove. Or double click on their profile, go to the menu bar, click edit, then click the tab that says 'Receiving Organs' and then click on a organ you want to remove.  
Either way you do this, will show a popup box where you will be asked to give a reason as to why the organ is being removed from the transplant waiting list. Please click an option, and then OK, and the user's entry in the reciving organ list will be removed. This will also be added to the user's history. 

### Editing a user
To edit a user, go to the profile search page, find the user that you want to edit, and double click on them. Now click file, then edit, and you are able to edit the user's data. 

You are able to do the same edits to a user as they are, except more. 

### Registering / Deregistering a user as a Organ Receiver.
To register a user as a Organ Reciever, you must first get into the profile of the user, as described in "Editing a user" now, if you look at their basic information, there should be a button for "Organ Recivier status". If you click this, you will register the user as a Organ Reciever, and get sent to the "Reciving Organs" screen where you can register the selected user for what organs they are to recieive. 

### Editing a user's medical history. 
Follow the steps in "Editing a user" to get to the editing user screen. Now click on the "Medical History" tab. You will be presented with an area to submit a disease, and two tables, for Current Diseases and past diseases. 
#### Adding a disease to current diseases
To add a disease to the current diseases table, first enter a name for the Disease. If the disease is chronic, please tick the tickbox. Now add a diagnosis date, that is inbetween today, and when the user was born. If you are satisfied with the disease you are about to add to the user, click the "Add" button. 

#### Moving diseases between past and current
To move a disease between current and past, you can click the ">>>" button that is between the two tables. If you cannot move the disease, it is probably because the disease is still chronic. 

#### Updating a Disease / Curing a chronic disease. 
To update a disease, right click on the disease in the table. Next, click update. You will be presented with a pop up dialog, where you can change the name of the disease, as well as the date of diagnosis and if it is chronic or not.  
Click save, and the edit will save to the medical history.  

### Adding / Editing Procedures
To add a procedure, Follow the edit profile instructions and then click on the "Procedures" tab. After this, add a summary of the procedure, a description of the procedure, and then a date that the procedure will happen. This procedure cannot happen before the user is born.  
To edit the procedure, you have to right click the procedure and then select update. You can then edit the same information that you could when creating the procedure. 

## User Guide for an Admin
### Admin functionality
Admin is the same as clinician except with the permission to delete and create accounts and use the terminal.
### Logging in.
To login as an admin, first run the application with the jar provided. This can either be launched with the command line command `java -jar //PATH_TO_JAR//` or by double clicking the jar file provided. Then tick the checkbox in the bottom left hand corner, that says login as admin. Next enter your username and password. Now click login. 

### Modifying accounts.
To get to the modifying accounts screen, login to an admin with the "Logging in" instructions. Next click on the "Modify Accounts" tab. 
To create a donor or an admin, select the respective button, and it will open up a popup screen. To delete an account, type the username of the account into the respective text box and then click the delete button. Note that you cannot delete your own account. 

### Terminal 
To open the terminal as an admin, click terminal, then click open. 

## Commands

Running `help -all` in the application will also display the following information.

### Delete A Profile
Usage: `delete_profile [--username <USERNAME>]`  
#### Arguments:  
- `--username <USERNAME>` The USERNAME that will be searched for.

***

### Help Screen (This)
usage: `help [--all | --command <COMMAND>]`
#### Arguments:    
- `--all`                   Will display all the values.  
- `--command <COMMAND>`     Specifies the specific command you want
                            Information on.  

***

### Delete Clinician
usage: `delete_clinician [--username <USERNAME>]`
#### Arguments
- `--username <USERNAME>`   The USERNAME that will be searched for.

***

### Edit an Clinician
usage: `edit_clinician --username <USERNAME> [--currentAddress <CURRENT
       ADDRESS>] [--dateOfBirth <DATE OF BIRTH>] [--firstName <FIRST
       NAME>] [--gender <GENDER>] [--lastName <LAST NAME>] [--middleName
       <MIDDLE NAME>] [--organisation <ORGANISATION>] [--region
       <REGION>] [--id <STAFF ID>]`
#### Arguments
- `--username <USERNAME>`                  Enter the profile's
                                            username. When creating,
                                            must be unique.
- `--currentAddress <CURRENT ADDRESS>`     Specify their current
                                            address.
- `--dateOfBirth <DATE OF BIRTH>`        Specify their date of birth.
                                            Enter as YYYY-MM-DD.
- `--firstName <FIRST NAME>`               Specify their first name.
- `--gender <GENDER>`                      Specify their gender, enter
                                            either M, F, or O. 'O'
                                            stands for other.
- `--lastName <LAST NAME>`                 Specify their last name.
- `--middleName <MIDDLE NAME>`             Specify their middle name.
- `--organisation <ORGANISATION>`          The name of the clinician's
                                            organisation.
- `--region <REGION>`                      Specify their region.
     --id <STAFF ID>                        The clinician's staff ID
                                            within their organisation.

*** 

#### Create A Clinician
usage: `create_clinician --dateOfBirth <DATE OF BIRTH> --firstName <FIRST
       NAME> --organisation <ORGANISATION> --region <REGION> --id <STAFF
       ID> --username <USERNAME> [--currentAddress <CURRENT ADDRESS>]
       [--gender <GENDER>] [--lastName <LAST NAME>] [--middleName
       <MIDDLE NAME>]`
- `--dateOfBirth <DATE OF BIRTH>`          Specify their date of birth.
                                            Enter as YYYY-MM-DD.
- `--firstName <FIRST NAME>`               Specify their first name.
- `--organisation <ORGANISATION>`          The name of the clinician's
                                            organisation.
- `--region <REGION>`                      Specify their region.
- `--id <STAFF ID>`                        The clinician's staff ID
                                            within their organisation.
- `--username <USERNAME>`                  Enter the profile's
                                            username. When creating,
                                            must be unique.
- `--currentAddress <CURRENT ADDRESS>`     Specify their current
                                            address.
- `--gender <GENDER>`                      Specify their gender, enter
                                            either M, F, or O. 'O'
                                            stands for other.
- `--lastName <LAST NAME>`                 Specify their last name.
- `--middleName <MIDDLE NAME>`             Specify their middle name.


### Create Profile
usage: `create_profile --username <username> --dateOfBirth <DATE OF BIRTH> --firstName <FIRST NAME> [--bloodType <BLOOD TYPE>] [--currentAddress <CURRENT ADDRESS>] [--dateOfDeath <DATE OF DEATH>] [--gender <GENDER>] [--height <HEIGHT>] [--isDonor <IS DONOR>] [--lastName <LAST NAME>] [--organs <ORGANS>] [--region <REGION>] [--weight <WEIGHT>] [--isSmoker <SMOKER>] [--bloodPressure <BLOODPRESSURE>] [--alcoholConsumption <ALCOHOLCONSUMPTION>] [--chronicDiseases <DISEASES>]`

#### Arguments
- `username` Specify the user's username. Must be unique.
- `dateOfBirth` Specify their date of birth. Enter as YYYY-MM-DD.
- `firstName` Specify their first name.
- `bloodType` Specify their blood type, enter one of the following; A+, A-, B+, B-, O+, O-, AB+, or AB-.
- `currentAddress` Specify their current address.
- `dateOfDeath` Specify their date of death. Enter as YYYY-MM-DD.
- `gender` Specify their gender, enter either M, F, or O. 'O' stands for other.
- `height` Specify their height.
- `isDonor` Specify if they are a donor. Enter either true or false.
- `lastName` Specify their last name.
- `organs` Specify what organs they want to donate. If this is done then they are automatically set as a donor. Enter the desired organs where they are separated by a
comma. Choices are; LIVER, KIDNEY, PANCREAS, HEART,HEART_VALVES, LUNG, INTESTINE, CORNEA, MIDDLE_EAR, SKIN, BONE, BONE_MARROW, CONNECTIVE_TISSUE
- `region` Specify their region.
- `weight` Specify their weight.
- `isSmoker` Specify if the user is a smoker. Either true or false
- `bloodPressure` Specify the users blood pressure, EX: 40/80
- `alcoholConsumption` Enter the users alcohol consumption. NONE, LOW, MEDIUM, HIGH, EXTREME   
- `chronicDiseases` Specify what chronic diseases the user has. Options are: Asthma, Bipolar, Cardiac
                                                   Failure, Chronic Kidney, Chronic Artery, Crohns,
                                                   Diabetes, Dysrythmia, Epilepsy, HIV,
                                                   Hyperlipidemia, Hypertension, Hypothyroidism,
                                                   Multiple Scierosis, Parkinsons, Arthritis,
                                                   Shizophrenia, Lupus Erythematosis

***
### Quit
usage: `quit`

***
### Display Profile
usage: `display_profile [--all | --username <username>]`

#### Arguments
- `all` Will display all the values.
-`username` The username that will be searched for.

***

***
### Edit Profile
usage: `edit_profile [--bloodType <BLOOD TYPE>] [--currentAddress <CURRENT ADDRESS>] [--dateOfBirth <DATE OF BIRTH>] [--dateOfDeath <DATE OF DEATH>] [--firstName <FIRST NAME>] [--gender <GENDER>] [--height <HEIGHT>] [--username <username>] [--isDonor <IS DONOR>] [--lastName <LAST NAME>] [--addOrgan <ORGAN TO ADD>] [--removeOrgan <ORGAN TO REMOVE>] [--region <REGION>] [--weight <WEIGHT>] [--isSmoker <SMOKER>] [--bloodPressure <BLOODPRESSURE>] [--alcoholConsumption <ALCOHOLCONSUMPTION>]`

#### Arguments
- `bloodType` Specify their blood type, enter one of the following: A+, A-, B+, B-, O+, O-, AB+, or AB-.
- `currentAddress` Specify their current address.
- `dateOfBirth` Specify their date of birth. Enter as YYYY-MM-DD.
- `dateOfDeath` Specify their date of death. Enter as YYYY-MM-DD.
- `firstName` Specify their first name.
- `gender` Specify their gender, enter either M, F, or O. 'O' stands for other.
- `height` Specify their height.
- `username` The username that will be searched for.
- `isDonor` Specify if they are a donor. Enter either true or false.
- `lastName` Specify their last name.
- `addOrgan` Specify one of the following organs to add: LIVER,KIDNEY, PANCREAS, HEART, HEART_VALVES, LUNG, INTESTINE, CORNEA MIDDLE_EAR, SKIN, BONE, BONE_MARROW, CONNECTIVE_TISSUE
- `removeOrgan` Specify one of the following organs to remove: LIVER, KIDNEY, PANCREAS, HEART, HEART_VALVES, LUNG, INTESTINE, CORNEA, MIDDLE_EAR, SKIN, BONE, BONE_MARROW, CONNECTIVE_TISSUE
- `region` Specify their region.
- `weight` Specify their weight.
- `isSmoker` Specify if the user is a smoker. Either true or false
- `bloodPressure` Specify the users blood pressure, EX: 40/80
- `alcoholConsumption` Enter the users alcohol consumption. NONE, LOW, MEDIUM, HIGH, EXTREME   

***

#### SQL
usage: `sql`
