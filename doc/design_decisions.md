## Development/Code Design Decisions

This project follows the MVC architecture. 

#### Unique Identifiers (Username & Staff ID)
The staff ID is only unique with an organisation. Hence within the ODMS system there can be multiple staff ID's with the same value. But specifying the organisation with the staff ID will make it unique. The username will always unique within the ODMS system. The username is still used for logging in to the system, and the staff ID is displayed on the clinician's profile.

#### Constants
Enumerators are used to predefine constants that are accessible throughout the whole application. This removes the need to define the same constants in each class and introduce the possibility of bugs from inconsistent constants for the blood types, genders, organs and commands that can be executed.

The string representation of each command (which the user has to type into the command line) are tied to CommandEnum constants. Instead of checking the string input when deciding what code to execute, a comparison with the CommandEnum constant can be done instead.

The CommandEnum is also used in HashMaps. This allows a O(1) retrieval time for specific command descriptions and the command enumerator (given the correct user input).

### Main Loop
Main.java is the main class. It will parse the command and look for the 'cli' argument which will decide if the GUI or console version of application is launched.

### User Messages
When out-putting messages call AppTerminal.display(...) and not System.out.println(...) as this will handle the case of what terminal (Unix/custom JavaFX) is being used.

### Undo and Redo
#### Overview
The class UndoRedo.java contains methods for creating an EventHandler to determine if the keys pressed are for undo redo. Also contains methods to create listeners for each Node in the FXML scene that are to be added to their appropriate properties. E.g. TextFields will have their listener bound to their TextProperty.

The entire scene will add an EventHandler to on key press to detect when either Ctrl+Z or Ctrl+Y is pressed for undo, redo respectively.

TextFields must also have the EventHandler set on key press as the EventHandler also consumes the event. This prevents TextField's in-built undo redo functionality from executing.

Listeners are added to each Node in the JavaFX scenes that will implement the undo redo functionality.

#### Implementation
There is an undo stack, a redo stack and a global variable called `currentEvent`. There is a private class called `Event` which is placed on the stack and contains the value a Node is to be restored to as well as representing the current focused Node and its value.

When the a Node's listener detects a change, it will create an `Event` instance with the old value and Node ID as attributes. This `Event` will be placed on the undo stack and the redo stack will be cleared. The Node's new/current value, and its ID, will be used to create another Event instance and will be assigned to the global variable `currentEvent`. 

When an undo is performed, the `Event` instance assigned to `currentEvent` will be placed on top of the redo stack. An `Event` instance is popped off the undo stack and is assigned to the `currentEvent` variable. The function `UndoRedo.restoreEvent(Event event)` is then called to restore a Node with its previous value (both specified in the `currentEvent').

When an redo is performed, the `Event` instance assigned to `currentEvent` will be placed on top of the undo stack. An `Event` instance is popped off the redo stack and is assigned to the `currentEvent` variable. The function `UndoRedo.restoreEvent(Event event)` is then called to restore a Node with its previous value (both specified in the `currentEvent').

When the redo stack is empty, a redo can not be performed. When the undo stack is empty an undo can not be performed. 

#### Edge Cases/Exceptions
##### CheckBox Node
CheckBoxes do not fit nicely with the previous implementation. If you only store the previous state on the undo stack then you will be missing information on the state to restore it to during a future redo. This is solved by adding the CheckBoxes to a list, when a change is detected on a CheckBox all the states of the CheckBoxes are added to an `Event` instance.

##### Changing Nodes
Given you have TextField A and TextField B and given the following actions have occurred:
- Wrote "ABC" to TextField A. Undo Stack will contain [{A: ""}, {A: "A"}, {A: "AB"}]. Current event will store the state "ABC".
- Changed Focus to TextField B.
- Wrote "D" to TextField B. Undo stack will contains [{A: ""}, {A: "A"}, {A: "AB"}, {B: ""}, {B: "D"}]. 

Notice that the value "ABC" for TextField A is missing. Hence, in the change listeners for Nodes we must check if we have changed Node (i.e. check if previous `Event` instance's Node ID is different to the `currentEvent` instance's ID). If there is a change then we must store the value `currentEvent` onto the undo stack.

## Product Owner Design Decisions
- 1st of March 16:00
 - Had discussion with product owner about profile uniqueness. The application will generate a unique username for each created profile. The user will be able to search the profiles by name to find their associated username. User can delete, search, and edit profiles by username. 
- 12th of March 
 - 09:30: Moffat has said that that after story 10, any stories that specifically say CLI will be included in the CLI version of the application. Otherwise just the GUI. For now...
 - 10:10: Moffat wants to be able to use the terminal and the GUI at the same time and switch between them at any time.
- 14th of March
 - The staff ID is only unique with an organisation. Hence within the ODMS system there can be multiple staff ID's with the same value.