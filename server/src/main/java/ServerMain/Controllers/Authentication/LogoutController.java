package ServerMain.Controllers.Authentication;

import ServerMain.Model.Authentication.LogoutQuery;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import seng302.Database.DatabaseManager;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Logger;

@RestController
public class LogoutController {
    private static Logger LOGGER = Logger.getLogger(LogoutController.class.getName());

    /**
     * End point for logging a user out of the application.
     *
     * @param token String which is the authentication token.
     * @return The appropriate error code, see LogoutQuery. Will return 500 if there is an SQL exception thrown.
     */
    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    public ResponseEntity logout(@RequestBody String token) {
        DatabaseManager dbManager = new DatabaseManager();

        try (Connection conn = dbManager.getConnection()) {
            LogoutQuery logoutQuery = new LogoutQuery(conn);
            return logoutQuery.logoutUser(token);
        } catch (SQLException e) {
            LOGGER.severe(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
