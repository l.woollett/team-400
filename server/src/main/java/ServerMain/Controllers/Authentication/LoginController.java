package ServerMain.Controllers.Authentication;

import ServerMain.Model.Authentication.LoginQuery;
import ServerMain.Model.Authentication.LoginRequestBody;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import seng302.Database.DatabaseManager;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Logger;

@RestController
public class LoginController {

    private static Logger LOGGER = Logger.getLogger(LoginController.class.getName());

    /**
     * Endpoint for logging into the application.
     *
     * @param loginResponseBody the body of the request.
     * @return ResponseEntity with Appropriate error code, see LoginQuery.
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity login(
            @RequestBody LoginRequestBody loginResponseBody) {

        String username = loginResponseBody.getUsername();
        String password = loginResponseBody.getPassword();
        DatabaseManager dbManager = new DatabaseManager();

        try (Connection conn = dbManager.getConnection()) {
            LoginQuery loginQuery = new LoginQuery(conn);
            return loginQuery.authenticateUser(username, password);
        } catch (SQLException e) {
            LOGGER.severe(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
