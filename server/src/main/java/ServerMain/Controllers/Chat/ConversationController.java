package ServerMain.Controllers.Chat;

import ServerMain.Model.Authentication.TokenVerifier;
import ServerMain.Model.Chat.ConversationQueries;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpServerErrorException;
import seng302.CustomException.EmptyList;
import seng302.Database.DatabaseManager;
import seng302.Model.Chat.Conversation;
import seng302.ServerInteracton.ServerQueries.Chat.ParticipantsWrapper;
import seng302.Model.Chat.ConversationClinicianJoin;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@RestController
public class ConversationController {

    private static final String X_AUTH = "X-Authorization";
    private final Logger LOGGER = Logger.getLogger(getClass().getName());

    /**
     * Post endpoint for creating a new conversation.
     *
     * @param conversation The conversation object that we want to insert into the database.
     * @param authToken The auth token to check if the user is authorised.
     * @return A response entity with either a successful status code or an error code.
     */
    @PostMapping("/conversation")
    @ResponseBody
    public ResponseEntity<Integer> postConversation(@RequestBody Conversation conversation, @RequestHeader(value =
            X_AUTH) String authToken) {
        DatabaseManager dbManager = new DatabaseManager();
        ConversationQueries conversationQueries = new ConversationQueries();
        Integer conversationId;

        try (Connection connection = dbManager.getConnection()) {
            if(!TokenVerifier.isStaff(authToken, connection)) {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }
            conversationId = conversationQueries.createConversation(conversation, connection);
        } catch (SQLException e) {
            LOGGER.severe(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (HttpServerErrorException | NullPointerException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(conversationId, HttpStatus.OK);
    }

    /**
     * Get endpoint for getting all of the conversations for a particular clinician and the status.
     *
     * @param authToken The auth token.
     * @param username The username of the clinician that we want to get conversations from.
     * @param status The status of conversations that you are requesting.
     * @return A response entity which contains a list of conversations.
     */
    @GetMapping("/conversations")
    @ResponseBody
    public ResponseEntity<List<Conversation>> getConversations(
            @RequestHeader(value = X_AUTH) String authToken,
            @RequestParam(value = "clinicianUsername") String username,
            @RequestParam(value = "status") String status) {
       ConversationQueries conversationQueries = new ConversationQueries();
       List<Conversation> conversations;
       DatabaseManager dbManager = new DatabaseManager();

       try (Connection connection = dbManager.getConnection()) {
           if (!TokenVerifier.isStaff(authToken, connection)) {
               return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
           }
           conversations = conversationQueries.getConversations(status, username, connection);
       } catch (SQLException e) {
           if (e.getErrorCode() == 0) {
               return new ResponseEntity<>(HttpStatus.NOT_FOUND);
           } else {
               return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
           }
       }
       return new ResponseEntity<>(conversations, HttpStatus.OK);
    }

    /**
     * End point for adding a clinician(s) to a new chat/ conversation.
     * An empty list of clinician usernames will result in a 400 being sent back.
     *
     * @param id                  The conversation id that the clinician needs to be added to.
     * @param authToken           Auth token of the clinician making the request.
     * @param participantsWrapper The list of clinicians usernames that need to be added to a chat.
     * @return A ResponseEntity with a 200 if the clinicians were added or an error code if something went wrong.
     */
    @PostMapping("/conversation/{conversationId}/addParticipants")
    @ResponseBody
    public ResponseEntity postParticipants(
            @PathVariable("conversationId") int id,
            @RequestHeader(value = X_AUTH) String authToken,
            @RequestBody ParticipantsWrapper participantsWrapper) {

        ConversationQueries conversationQueries = new ConversationQueries();
        DatabaseManager dbManager = new DatabaseManager();

        try (Connection connection = dbManager.getConnection()) {
            if (!TokenVerifier.isStaff(authToken, connection)) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }
            try {
                conversationQueries.addParticipants(id, participantsWrapper.getParticipants(), connection);
            } catch (EmptyList e) {
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }
        } catch (SQLException e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("/conversation/{conversationId}/removeParticipants")
    @ResponseBody
    public ResponseEntity deleteParticipants (@PathVariable("conversationId") int id,
                                              @RequestHeader(X_AUTH) String authToken,
                                              @RequestBody ParticipantsWrapper participants) {

        ConversationQueries queries = new ConversationQueries();
        DatabaseManager databaseManager = new DatabaseManager();

        try (Connection connection = databaseManager.getConnection()) {

            if(!TokenVerifier.isStaff(authToken, connection)) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }

            try {
                queries.removeParticipants(id, participants.getParticipants(), connection);
            } catch (EmptyList emptyList) {
                LOGGER.severe(emptyList.getMessage());
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            }

        } catch (SQLException e) {
            LOGGER.severe(e.getMessage());
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * Handles the sending of the result of the sql update to the client.
     * @param authToken String auth token for authorization.
     * @param updateItem ConversationClinicianJoin holding the clinician username and Conversation id.
     * @return ResponseEntity holding the status of the query. ie 404 not found.
     */
    @PutMapping("/conversations/lastRead")
    @ResponseBody
    public ResponseEntity updateLastRead(
            @RequestHeader(value = X_AUTH) String authToken,
            @RequestBody ConversationClinicianJoin updateItem) {

        ConversationQueries conversationQueries = new ConversationQueries();
        DatabaseManager dbManager = new DatabaseManager();
        Integer result;

        try (Connection connection = dbManager.getConnection()) {
            if (!TokenVerifier.isStaff(authToken, connection)) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }

            result = conversationQueries.updateLastRead(updateItem, connection);

        } catch (SQLException e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (result == 0) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        } else if (result == 1) {
            return new ResponseEntity(HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
    }


    /**
     * Sets the conversation to end with a given status.
     *
     * @param conversationId Given conversation id to end with.
     * @param authToken      String of auth code to be checked for valid permission.
     * @return Integer of rows affected by the query. 0 if failure. 1 if success.
     */
    @PutMapping("/conversation/{conversationId}/endConversation")
    @ResponseBody
    public ResponseEntity endConversation(@PathVariable("conversationId") int conversationId,
                                     @RequestHeader(value =
                                             X_AUTH) String authToken) {
        ConversationQueries conversationQueries = new ConversationQueries();

        try (Connection connection = new DatabaseManager().getConnection()) {

            if (!TokenVerifier.isStaff(authToken, connection)) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }

            int numUpdatedRows = conversationQueries.endConversation(conversationId, new DatabaseManager().getConnection());

            if (numUpdatedRows == 0) {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            } else if (numUpdatedRows == 1) {
                return new ResponseEntity(HttpStatus.OK);
            } else {
                return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            }

        } catch (SQLException e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Calls the query that retrieves unread conversation's ids.
     * @param username String clinicianUsername
     * @param authToken String token for authorization.
     * @return ResponseEntity holding a list of ids
     */
    @GetMapping("/conversation/unread/{clinicianUsername}")
    @ResponseBody
    public ResponseEntity<List<Integer>> getUnreadConversationIds(@PathVariable("clinicianUsername") String username,
                                                   @RequestHeader(value = X_AUTH) String authToken) {
        ConversationQueries conversationQueries = new ConversationQueries();
        List<Integer> conversationIds;
        DatabaseManager dbManager = new DatabaseManager();

        try (Connection connection = dbManager.getConnection()) {
            if (!TokenVerifier.isStaff(authToken, connection)) {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }
            conversationIds = conversationQueries.getUnreadConversationIds(username, connection);
        } catch (SQLException e) {
            if (e.getErrorCode() == 0) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                e.printStackTrace();
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity<>(conversationIds, HttpStatus.OK);
    }
}
