package ServerMain.Controllers.Chat;

import ServerMain.Model.Authentication.TokenVerifier;
import ServerMain.Model.Chat.MessageQueries;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import seng302.Database.DatabaseManager;
import seng302.Model.Chat.Message;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@RestController
public class MessageController {

    private static final String X_AUTH = "X-Authorization";
    private final Logger LOGGER = Logger.getLogger(getClass().getName());

    /**
     * Get endpoint for getting all of the conversations for a particular clinician and the status.
     *
     * @param authToken      The auth token.
     * @param conversationId The id of the conversation that we want to get messages from.
     * @param lastMessageId  The ID of the last message retrieved.
     * @return A response entity which contains a list of messages.
     */
    @GetMapping("/messages/{conversationId}")
    @ResponseBody
    public ResponseEntity<List<Message>> getMessagesFromConversation(
            @RequestHeader(value = X_AUTH) String authToken,
            @PathVariable("conversationId") int conversationId,
            @RequestParam(value = "id", required = false) Integer lastMessageId) {
        MessageQueries messageQueries = new MessageQueries();
        List<Message> conversations = new ArrayList<>();
        DatabaseManager dbManager = new DatabaseManager();

        try (Connection connection = dbManager.getConnection()) {
            if (!TokenVerifier.isStaff(authToken, connection)) {
                return new ResponseEntity<>(conversations, HttpStatus.UNAUTHORIZED);
            }
            conversations = messageQueries.getMessageListForConversationId(conversationId, lastMessageId, connection);
        } catch (SQLException e) {
            if (e.getErrorCode() == 0) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<>(conversations, HttpStatus.OK);
    }

    /**
     * Endpoint for posting a message to the database.
     *
     * @param message   The message that needs to be sent.
     * @param authToken The auth token.
     * @return A ResponseEntity with a 200 code if the message was successfully sent or another error code if it failed.
     */
    @PostMapping("/message")
    @ResponseBody
    public ResponseEntity sendMessage(
            @RequestBody Message message,
            @RequestHeader(value = X_AUTH) String authToken) {
        MessageQueries messageQueries = new MessageQueries();
        DatabaseManager dbManager = new DatabaseManager();

        try (Connection connection = dbManager.getConnection()) {
            if (!TokenVerifier.isStaff(authToken, connection)) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }
            messageQueries.postMessage(message, connection);
        } catch (SQLException e) {
            LOGGER.severe(e.getMessage());
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * Returns the result of the query to check for new messages.
     * @param authToken String auth token required for authorization.
     * @param username String of clinician's username.
     * @param id Integer of the conversationId.
     * @return ResponseEntity holding the Boolean result and the status of the query. ie 400.
     */
    @GetMapping("/messages/new")
    @ResponseBody
    public ResponseEntity<Boolean> checkForMessages(
            @RequestHeader(value = X_AUTH) String authToken,
            @RequestParam(value = "clinicianUsername") String username,
            @RequestParam(value = "conversationId") Integer id) {
        MessageQueries messageQueries = new MessageQueries();
        DatabaseManager dbManager = new DatabaseManager();
        Boolean newMessages;

        try (Connection connection = dbManager.getConnection()) {
            if (!TokenVerifier.isStaff(authToken, connection)) {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }
            newMessages = messageQueries.checkForMessages(username, id, connection);
        } catch (SQLException e) {
            if (e.getErrorCode() == 0) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity<>(newMessages, HttpStatus.OK);
    }
}
