package ServerMain.Controllers;

import ServerMain.Model.Authentication.TokenVerifier;
import ServerMain.Model.NotificationQueries;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import seng302.Database.DatabaseManager;
import seng302.Model.AvailableOrganNotification;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

@RestController
public class NotificationController {

    private DatabaseManager dbManager = new DatabaseManager();
    private final Logger LOGGER = Logger.getLogger(getClass().getName());
    private final NotificationQueries notificationQueries = new NotificationQueries();


    /**
     * Server end point used for adding a request to the database on the server.
     *
     * @param notification The request instance to be added to the database.
     * @param authToken    The authorization token.
     * @return The response entity which contains the ID for the notification.
     */
    @PostMapping("/notification")
    @ResponseBody
    public ResponseEntity<Integer> postNotification(@RequestBody AvailableOrganNotification notification,
                                                    @RequestHeader(value = "X-Authorization") String authToken) {

        try (Connection connection = dbManager.getConnection()) {

            if (!TokenVerifier.isStaff(authToken, connection)) {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }

            int notificationId = notificationQueries.createNotification(notification, connection);
            return new ResponseEntity<>(notificationId, HttpStatus.OK);

        } catch (SQLException e) {
            LOGGER.severe(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PutMapping("/notification")
    @ResponseBody
    public ResponseEntity putNotification(@RequestBody AvailableOrganNotification notification,
                                          @RequestHeader(value = "X-Authorization") String authToken) {
        try (Connection connection = dbManager.getConnection()) {

            if (!TokenVerifier.isStaff(authToken, connection)) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }

            notificationQueries.replaceNotification(notification, connection);

            return new ResponseEntity(HttpStatus.OK);

        } catch (SQLException | NullPointerException e) {
            LOGGER.severe(e.getMessage());
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/notifications")
    @ResponseBody
    public ResponseEntity<List<AvailableOrganNotification>> getNotifications(@RequestParam("clinician") String clinicianUsername,
                                                                             @RequestHeader("X-Authorization") String authToken) {
        try (Connection connection = dbManager.getConnection()) {

            if (!TokenVerifier.isStaff(authToken, connection)) {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }

            List<AvailableOrganNotification> notifications = notificationQueries.getNotifications(clinicianUsername, connection);

            return new ResponseEntity<>(notifications, HttpStatus.OK);

        } catch (SQLException e) {
            LOGGER.severe(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Server end point used for getting all null-value incoming notifications for a clinician from the database on the server.
     *
     * @param username  The clinician's username.
     * @param authToken The authorization token.
     * @return The response entity which contains a list of the not yet respond incoming notification of a clinician.
     */
    @GetMapping("/notification/{username}")
    @ResponseBody
    public ResponseEntity<AvailableOrganNotification> getAllIncomingNotifications(@PathVariable("username") String username, @RequestHeader(value =
            "X-Authorization") String authToken) {
        NotificationQueries notificationQueries = new NotificationQueries();
        List<AvailableOrganNotification> allIncomingNotifications = null;
        ResponseEntity response = null;

        try (Connection connection = dbManager.getConnection()) {

            // Checks if the user is a staff member.
            if (!TokenVerifier.isStaff(authToken, connection)) {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }
            allIncomingNotifications = notificationQueries.getAllIncomingNotifications(connection, username);


        } catch (SQLException e) {
            response = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (response == null) {
            if (allIncomingNotifications == null) {
                response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                response = new ResponseEntity<>(allIncomingNotifications, HttpStatus.OK);
            }
        }

        return response;


    }
}
