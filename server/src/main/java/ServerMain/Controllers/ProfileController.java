package ServerMain.Controllers;

import ServerMain.Model.Authentication.TokenVerifier;
import ServerMain.Model.ProfileQueries;
import ServerMain.Model.UserQueries;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import seng302.Database.DatabaseManager;
import seng302.Enum.OrganEnum;
import seng302.Model.OrganMatchQueryResult;
import seng302.Model.Profile;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

@RestController
public class ProfileController {

    private static final String X_AUTH = "X-Authorization";
    private static final ArrayList<Integer> MALFORMED_REQUEST_CODES = new ArrayList<>(Arrays.asList(1048, 1062));
    private final Logger LOGGER = Logger.getLogger(getClass().getName());
    private DatabaseManager dbManager = new DatabaseManager();

    /**
     * Calls the delete queries for profile and handles statuses.
     * @param username String username of profiles to delete.
     * @param authToken The user's authorization token.
     * @return response ResponseEntity containing status.
     */
    @DeleteMapping("/profile/{username}")
    @ResponseBody
    public ResponseEntity deleteProfile(@PathVariable("username") String username, @RequestHeader(value =
            X_AUTH) String authToken) {
        ProfileQueries profileQueries = new ProfileQueries();
        ResponseEntity response = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        int result = -1;
        try (Connection connection = dbManager.getConnection()) {

            // Checks if the user is a staff member.
            if (!TokenVerifier.isStaff(authToken, connection)) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }
            result = profileQueries.deleteProfile(username, connection);

        } catch (SQLException e) {
            LOGGER.severe(e.getMessage());
            response = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (result == 0) {
            response = new ResponseEntity(HttpStatus.NOT_FOUND);
        } else if (result == 1) {
            response = new ResponseEntity(HttpStatus.OK);
        }
        return response;
    }

    /**
     * Calls the patch queries for profile and handles statuses.
     * @param username String username of profiles to delete.
     * @param profile Profile to update in DB.
     * @param authToken The user's authorization token.
     * @return response ResponseEntity containing status.
     */
    @PutMapping("/profile/{username}")
    @ResponseBody
    public ResponseEntity putProfile(@PathVariable("username") String username, @RequestBody Profile profile, @RequestHeader(value =
            X_AUTH) String authToken) {
        ProfileQueries profileQueries = new ProfileQueries();
        UserQueries userQueries = new UserQueries();
        ResponseEntity response = null;
        int result = -1;

        if (!username.equals(profile.getUsername())) {
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }

        try (Connection connection = dbManager.getConnection()) {

            boolean isUser = userQueries.isUser(username, connection);
            if (!isUser){
                response = new ResponseEntity(HttpStatus.NOT_FOUND);
            } else {
                // Checks if the token is valid.
                if (!TokenVerifier.userTokenExists(authToken, connection)) {
                    return new ResponseEntity(HttpStatus.UNAUTHORIZED);
                }
                result = profileQueries.updateProfile(profile, connection);
            }

        } catch (SQLException e) {
            LOGGER.severe(e.getLocalizedMessage());
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (result == 0) {
            response = new ResponseEntity(HttpStatus.BAD_REQUEST);
        } else if (result == 1) {
            response = new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return response;
    }

    /**
     * Calls queries to post a single Clinician from the database.
     * @param profile Profile to post to DB.
     * @return response ResponseEntity holding status of operation.
     */
    @PostMapping("/profile")
    @ResponseBody
    public ResponseEntity postProfile(@RequestBody Profile profile) {
        ProfileQueries profileQueries = new ProfileQueries();

        try (Connection connection = dbManager.getConnection()) {
            profileQueries.postProfile(profile, connection);
        } catch (SQLException e) {
            if (MALFORMED_REQUEST_CODES.contains(e.getErrorCode())) {
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            } else {
                LOGGER.severe(e.getMessage());
                return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        return new ResponseEntity(HttpStatus.OK);
    }


    /**
     * Calls the query for getting all profiles.
     * This handles the query creation for handling the many optional query parameters.
     * Also can be used for collecting the count of profiles matching the criteria with the
     * getSize param.
     *
     * @param authToken       For authorization.
     * @param startIndex      Number of items to skip for pagination.
     * @param count           Number to display on one page.
     * @param q               Query string to search for in database by name.
     * @param age             Age to select.
     * @param birthGender     Required birth gender param.
     * @param preferredGender Required preferred gender param.
     * @param region          Region that the profiles should reside from.
     * @param donorStatus     Whether donor, receiver or both.
     * @param donatingOrgan   specific organ being donated.
     * @param receivingOrgan  Specific organ required.
     * @param clinician       Clinicians name to be searched for as the profiles foreign key.
     * @param organisation    Clinicians name to linked to the hospital in order to find profiles from the same hospital.
     * @return Returns a list of profile objects that match the criteria.
     */
    @GetMapping("/profiles")
    @ResponseBody
    public ResponseEntity<List<Profile>> getProfiles(
            @RequestHeader(value = X_AUTH) String authToken,
            @RequestParam(value = "startIndex", required = false) String startIndex,
            @RequestParam(value = "count", required = false) String count,
            @RequestParam(value = "q", required = false) String q,
            @RequestParam(value = "age", required = false) String age,
            @RequestParam(value = "birthGender", required = false) String birthGender,
            @RequestParam(value = "preferredGender", required = false) String preferredGender,
            @RequestParam(value = "region", required = false) String region,
            @RequestParam(value = "donorStatus", required = false) String donorStatus,
            @RequestParam(value = "donatingOrgan", required = false) String donatingOrgan,
            @RequestParam(value = "receivingOrgan", required = false) String receivingOrgan,
            @RequestParam(value = "clinician", required = false) String clinician,
            @RequestParam(value = "organisation", required = false) String organisation) {
        ProfileQueries profileQueries = new ProfileQueries();
        List<Profile> profiles = null;
        ResponseEntity<List<Profile>> response = null;

        String queryString = queryBuilder(startIndex,
                count, q, age, birthGender, preferredGender, region,
                donorStatus, donatingOrgan, receivingOrgan, null, clinician, organisation);

        if (queryString == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        try (Connection connection = dbManager.getConnection()) {

            // Checks if the user is a staff member.
            if (!TokenVerifier.isStaff(authToken, connection)) {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }

            profiles = profileQueries.searchProfiles(connection, queryString);

        } catch (SQLException e) {

            LOGGER.severe(e.getMessage());

            response = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (response == null) {
            response = new ResponseEntity<>(profiles, HttpStatus.OK);
        }

        return response;
    }

    /**
     * Used for collecting the count of profiles matching the criteria with the
     * getSize param.
     *
     * @param authToken       For authorization.
     * @param startIndex      Number of items to skip for pagination.
     * @param count           Number to display on one page.
     * @param q               Query string to search for in database by name.
     * @param age             Age to select.
     * @param birthGender     Required birth gender param.
     * @param preferredGender Required preferred gender param.
     * @param region          Region that the profiles should reside from.
     * @param donorStatus     Whether donor, receiver or both.
     * @param donatingOrgan   specific organ being donated.
     * @param receivingOrgan  Specific organ required.
     * @param size            Indication that the count of profiles that match the query is needed.
     * @param clinician       Clinicians name to be searched for as the profiles foreign key.
     * @param organisation    Clinicians name to linked to the hospital in order to find profiles from the same hospital.
     * @return ResponseEntity holding an int of the number of profiles the criteria matches
     * and the resulting status
     */
    @GetMapping("/profileCount")
    @ResponseBody
    public ResponseEntity<Integer> getProfileCount(
            @RequestHeader(value = X_AUTH) String authToken,
            @RequestParam(value = "startIndex", required = false) String startIndex,
            @RequestParam(value = "count", required = false) String count,
            @RequestParam(value = "q", required = false) String q,
            @RequestParam(value = "age", required = false) String age,
            @RequestParam(value = "birthGender", required = false) String birthGender,
            @RequestParam(value = "preferredGender", required = false) String preferredGender,
            @RequestParam(value = "region", required = false) String region,
            @RequestParam(value = "donorStatus", required = false) String donorStatus,
            @RequestParam(value = "donatingOrgan", required = false) String donatingOrgan,
            @RequestParam(value = "receivingOrgan", required = false) String receivingOrgan,
            @RequestParam(value = "getSize", required = false) String size,
            @RequestParam(value = "clinician", required = false) String clinician,
            @RequestParam(value = "organisation", required = false) String organisation) {
        ProfileQueries profileQueries = new ProfileQueries();
        Integer profileCount = null;
        ResponseEntity<Integer> response = null;

        String queryString = queryBuilder(startIndex,
                count, q, age, birthGender, preferredGender, region,
                donorStatus, donatingOrgan, receivingOrgan, size, clinician, organisation);

        if (queryString == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        try (Connection connection = dbManager.getConnection()) {

            // Checks if the user is a staff member.
            if (!TokenVerifier.isStaff(authToken, connection)) {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }

            profileCount = profileQueries.searchProfilesCount(connection, queryString);

        } catch (SQLException e) {
            LOGGER.severe(e.getMessage());

            response = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (response == null) {
            response = new ResponseEntity<>(profileCount, HttpStatus.OK);
        }

        return response;
    }


    /**
     * Calls the query for getting all profiles in the waiting list.
     * This handles the query creation for handling the query parameters.
     * @param authToken String for authentication.
     * @param organ     organ to search for
     * @param region    region to search for
     * @return response ResponseEntity with list of Profiles.
     */
    @GetMapping("/waiting_list")
    @ResponseBody
    public ResponseEntity<ArrayList<Profile>> getWaitingList(
            @RequestHeader(value = X_AUTH) String authToken,
            @RequestParam(value = "organ", required = false) String organ,
            @RequestParam(value = "region", required = false) String region) {
        ProfileQueries profileQueries = new ProfileQueries();
        ArrayList<Profile> profiles = null;
        ResponseEntity<ArrayList<Profile>> response = null;
        String queryStart = "SELECT * FROM `Profile` JOIN `User` ON Profile.username = User.username";
        queryStart += " JOIN ReceivingOrgans ON ReceivingOrgans.username = Profile.username";
        String whereClauses = "";
        boolean whereClause = false;

        List<String> arguments = new ArrayList<>();

        if (organ != null) {
            if (organ.equals("")) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
            whereClauses += " WHERE ReceivingOrgans.organ = ?";
            whereClause = true;
            arguments.add(organ.toUpperCase());
        }

        if (region != null) {
            if (region.equals("")) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }

            if (whereClause) {
                whereClauses += " AND User.Region = ?";
                arguments.add(region.toUpperCase());
            } else {
                whereClauses += " WHERE User.Region = ?";
                whereClause = true;
                arguments.add(region.toUpperCase());
            }
        }

        try (Connection connection = dbManager.getConnection()) {
            // Checks if the user is a staff member.
            if (!TokenVerifier.isStaff(authToken, connection)) {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }

            if (whereClause) {
                queryStart += whereClauses;
            }
            profiles = profileQueries.getWaitingList(connection, queryStart, arguments);

        } catch (SQLException e) {
            LOGGER.severe(e.getMessage());
            response = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (response == null) {
            response = new ResponseEntity<>(profiles, HttpStatus.OK);
        }

        return response;
    }


    /**
     * Calls query to get single profile from the DB.
     * @param username  String username of profile in DB.
     * @param authToken String for authorization purposes.
     * @return response ResponseEntity holding profile object and status.
     */
    @GetMapping("/profile/{username}")
    @ResponseBody
    public ResponseEntity<Profile> getProfile(@PathVariable("username") String username, @RequestHeader(value =
            X_AUTH) String authToken) {
        ProfileQueries profileQueries = new ProfileQueries();
        UserQueries userQueries = new UserQueries();
        Profile profile = null;
        ResponseEntity<Profile> response = null;

        try (Connection connection = dbManager.getConnection()) {
            Boolean isUser = userQueries.isUser(username, connection);
            if (!isUser) {
                response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                // Checks if the token is valid.
                if (!TokenVerifier.userTokenExists(authToken, connection)) {
                    return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
                }
                profile = profileQueries.getProfile(connection, username);
            }

        } catch (SQLException e) {
            response = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (response == null) {
            if (profile == null) {
                response = new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            } else {
                response = new ResponseEntity<>(profile, HttpStatus.OK);
            }
        }
        return response;
    }

    /**
     * Handles the building of the search query string.
     * This may be used to also count the number of profiles that match the params.
     * All of these params are optional.
     *
     * @param startIndex      Number of items to skip for pagination.
     * @param count           Number to display on one page.
     * @param q               Query string to search for in database by name.
     * @param age             Age to select.
     * @param birthGender     Required birth gender param.
     * @param preferredGender Required preferred gender param.
     * @param region          Region that the profiles should reside from.
     * @param donorStatus     Whether donor, receiver or both.
     * @param donatingOrgan   specific organ being donated.
     * @param receivingOrgan  Specific organ required.
     * @param clinician       clinician in charge of the desired profiles.
     * @param organisation    the clinicians name to be linked to the hospital in order
     *                        to retrieve profiles from that same hospital.
     * @return Integer of number of profiles that match the params in DB.
     * This must be handled by the caller of this function.
     */
    private String queryBuilder(String startIndex,
                                String count,
                                String q,
                                String age,
                                String birthGender,
                                String preferredGender,
                                String region,
                                String donorStatus,
                                String donatingOrgan,
                                String receivingOrgan,
                                String size,
                                String clinician,
                                String organisation) {
        String queryStart;
        boolean getCount = size != null;
        if (getCount) {
            queryStart = "SELECT COUNT(*) FROM `Profile`JOIN `User` ON Profile.username = User.username LEFT JOIN `Hospitals` ON Profile.hospitalId = Hospitals.HospitalID";
        } else {
            queryStart = "SELECT * FROM `Profile`JOIN `User` ON Profile.username = User.username LEFT JOIN `Hospitals` ON Profile.hospitalId = Hospitals.HospitalID";
        }
        String whereClauses = "";
        boolean whereClause = false;

        if (donatingOrgan != null) {
            if (donatingOrgan.equals("")) {
                return null;
            }
            queryStart += " JOIN DonatingOrgans ON DonatingOrgans.username = Profile.username";
            whereClauses += " WHERE DonatingOrgans.organ = '" + donatingOrgan + "'";
            whereClause = true;
        }

        if (organisation != null) {
            if (organisation.equals("")) {
                return null;
            }
            queryStart += " JOIN Clinician ON Clinician.organisation = Hospitals.HospitalID";
            whereClauses += " WHERE Clinician.username = '" + organisation + "'";
            whereClause = true;
        }

        if (receivingOrgan != null) {
            if (receivingOrgan.equals("")) {
                return null;
            }
            queryStart += " JOIN ReceivingOrgans ON ReceivingOrgans.username = Profile.username";
            if (whereClause) {
                whereClauses += " AND ReceivingOrgans.organ = '" + receivingOrgan + "'";
            } else {
                whereClauses += " WHERE ReceivingOrgans.organ = '" + receivingOrgan + "'";
                whereClause = true;
            }
        }

        if (age != null) {
            if (age.equals("")) {
                return null;
            }
            if (whereClause) {
                whereClauses += " AND ((Profile.dateOfDeath IS null AND (TIMESTAMPDIFF(year, User.dateOfBirth, CURRENT_DATE) = " + age + ")) OR (Profile.dateOfDeath IS NOT null AND (TIMESTAMPDIFF(year, User.dateOfBirth, Profile.dateOfDeath) = " + age + ")))";
            } else {
                whereClauses += " WHERE ((Profile.dateOfDeath IS null AND (TIMESTAMPDIFF(year, User.dateOfBirth, CURRENT_DATE) = " + age + ")) OR (Profile.dateOfDeath IS NOT null AND (TIMESTAMPDIFF(year, User.dateOfBirth, Profile.dateOfDeath) = " + age + ")))";
                whereClause = true;
            }
        }

        if (birthGender != null) {
            if (birthGender.equals("")) {
                return null;
            }
            if (whereClause) {
                whereClauses += " AND Profile.birthGender = '" + birthGender + "'";
            } else {
                whereClauses += " WHERE Profile.birthGender = '" + birthGender + "'";
                whereClause = true;
            }
        }

        if (preferredGender != null) {
            if (preferredGender.equals("")) {
                return null;
            }
            if (whereClause) {
                whereClauses += " AND User.gender = '" + preferredGender + "'";
            } else {
                whereClauses += " WHERE User.gender = '" + preferredGender + "'";
                whereClause = true;
            }
        }

        if (region != null) {
            if (region.equals("")) {
                return null;
            }
            if (whereClause) {
                whereClauses += " AND User.region = '" + region.replace("'", "''") + "'";
            } else {
                whereClauses += " WHERE User.region = '" + region.replace("'", "''") + "'";
                whereClause = true;
            }
        }

        if (donorStatus != null) {
            if (donorStatus.equals("")) {
                return null;
            }
            String start = "WHERE";
            if (whereClause) {
                start = "AND";
            }
            whereClause = true;
            switch (donorStatus) {
                case "0":
                    whereClauses += " " + start + " Profile.isDonor = 1";
                    break;
                case "1":
                    whereClauses += " " + start + " Profile.isReceiver = 1";
                    break;
                case "2":
                    whereClauses += " " + start + " Profile.isDonor = 1 AND Profile.isReceiver = 1";
                    break;
                default:
                    return null;
            }

        }

        if (q != null) {
            if (q.equals("")) {
                return null;
            }

            if (whereClause) {
                whereClauses += " AND (User.firstName LIKE " + "'" + q + "%' OR User.middleName LIKE '" + q + "%' OR User.lastName LIKE '" + q + "%')";
            } else {
                whereClauses += " WHERE (User.firstName LIKE " + "'" + q + "%' OR User.middleName LIKE '" + q + "%' OR User.lastName LIKE '" + q + "%')";
                whereClause = true;
            }
        }

        if (clinician != null) {
            if (clinician.equals("")) {
                return null;
            }
            if (whereClause) {
                whereClauses += " AND Profile.clinicianUserName = '" + clinician + "'";
            } else {
                whereClauses += " WHERE Profile.clinicianUserName = '" + clinician + "'";
            }
        }

        int upperLimit = 50;
        int startPoint = 0;

        if (count != null) {
            if (count.equals("")) {
                return null;
            }
            upperLimit = Integer.parseInt(count);
        }

        if (startIndex != null) {
            if (startIndex.equals("")) {
                return null;
            }

            whereClauses += " LIMIT " + startIndex + ", " + Integer.toString(upperLimit);
        } else {
            whereClauses += " LIMIT " + startPoint + ", " + Integer.toString(upperLimit);
        }

        return queryStart + whereClauses;
    }

    /**
     * Endpoint for getting matching organs
     * @param authToken User's authorization token
     * @param username  The username to match for
     * @param organ     The organ we're matching
     * @return An arraylist of strings of usernames that match
     */
    @GetMapping("/organs/")
    @ResponseBody
    public ResponseEntity<ArrayList<OrganMatchQueryResult>> getProfiles(
            @RequestHeader(value = X_AUTH) String authToken,
            @RequestParam(value = "username") String username,
            @RequestParam(value = "organ") String organ) {
        ProfileQueries profileQueries = new ProfileQueries();
        ArrayList<OrganMatchQueryResult> usernames = null;
        ResponseEntity<ArrayList<OrganMatchQueryResult>> response = null;

        try (Connection connection = dbManager.getConnection()) {

            // Checks if the user is a staff member.
            if (!TokenVerifier.isStaff(authToken, connection)) {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }

            usernames = profileQueries.getCompatibleOrgans(connection, username, organ);

        } catch (SQLException e) {


            LOGGER.severe(e.getMessage());

            response = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (response == null) {
            response = new ResponseEntity<>(usernames, HttpStatus.OK);
        }

        return response;
    }

    /**
     * End point for deleting an organ from a donor. Used as a way to signify an organ has been transferred.
     *
     * @param authToken The auth token of the user.
     * @param username  The username of the donor who will have the organ removed.
     * @param organ     The organ that needs to be removed.
     * @return A ResponseEntity with a 200 code if the deletion was successful or another error code if it was not.
     */
    @DeleteMapping("/organ")
    @ResponseBody
    public ResponseEntity deleteOrgan(
            @RequestHeader(value = X_AUTH) String authToken,
            @RequestParam(value = "username") String username,
            @RequestParam(value = "organ") String organ) {
        ProfileQueries profileQueries = new ProfileQueries();

        try (Connection connection = dbManager.getConnection()) {

            if (!TokenVerifier.isStaff(authToken, connection)) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }

            int rows;
            rows = profileQueries.deleteDonatingOrgan(OrganEnum.getOrgan(organ), username, connection);
            if (rows < 1) {
                return new ResponseEntity(HttpStatus.NOT_FOUND);
            }

        } catch (SQLException e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity(HttpStatus.OK);
    }
}
