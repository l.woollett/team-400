package ServerMain.Controllers;

import ServerMain.Model.Authentication.TokenVerifier;
import ServerMain.Model.AvailableOrganQueries;
import ServerMain.Model.HospitalQueries;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import seng302.Database.DatabaseManager;
import seng302.Model.AvailableOrgan;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@RestController
public class AvailableOrganController {

    /**
     * End point for getting all available organs from a certain hospital.
     *
     * @param hospitalId The id of the hospital that the organs will come from.
     * @param authToken  The auth token of the user.
     * @return A list of all available organs. Note: Expired organs may be returned from the endpoint so must check them
     * yourself before you use them.
     */
    @GetMapping("/availableOrgans/{hospitalId}")
    @ResponseBody
    public ResponseEntity<List<AvailableOrgan>> getOrgansFromHospital(@PathVariable("hospitalId") int hospitalId,
                                                                      @RequestHeader(value = "X-Authorization") String authToken) {

        DatabaseManager dbManager = new DatabaseManager();
        AvailableOrganQueries organQueries = new AvailableOrganQueries();
        List<AvailableOrgan> organs;

        try (Connection connection = dbManager.getConnection()) {

            // Checks if the user is a staff member.
            if (!TokenVerifier.isStaff(authToken, connection)) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }

            if (!checkHospitalExists(connection, hospitalId)) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

            organs = organQueries.getAvailableOrgansFromHospital(hospitalId, connection);

        } catch (SQLException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(organs, HttpStatus.OK);
    }

    /**
     * Checks if a hospital exists and will then be used to send back a 404 if the said hospital does not exist.
     *
     * @param connection The connection object to the database.
     * @param hospitalId The id of the hospital which we want to get the available organs from.
     * @return True if the hospital exists. False if it does not.
     */
    private boolean checkHospitalExists(Connection connection, int hospitalId) {
        HospitalQueries hospitalQueries = new HospitalQueries();

        try {
            hospitalQueries.getHospitalById(connection, hospitalId);
            return true;
        } catch (SQLException e) {
            return false;
        }
    }
}
