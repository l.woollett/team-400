package ServerMain.Controllers;

import ServerMain.Model.Authentication.TokenVerifier;
import ServerMain.Model.ClinicianQueries;
import ServerMain.Model.HospitalQueries;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import seng302.Database.DatabaseManager;
import seng302.Model.Clinician;
import seng302.Model.Hospital;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

/**
 * Controller will intercept the requests to end
 */
@RestController
public class HospitalController {

    private final DatabaseManager databaseManager = new DatabaseManager();
    private final Logger LOGGER = Logger.getLogger(getClass().getName());

    /**
     * End point on the server to get all the hospitals.
     *
     * @return List of all the hospitals.
     */
    @GetMapping("/hospitals")
    @ResponseBody
    public ResponseEntity<List<Hospital>> getHospitals(@RequestHeader(value = "X-Authorization") String authToken) {

        ResponseEntity<List<Hospital>> response = null;

        try (Connection connection = databaseManager.getConnection()) {

            if (!TokenVerifier.userTokenExists(authToken, connection)) {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }


            HospitalQueries hospitalQueries = new HospitalQueries();
            List<Hospital> hospitals = hospitalQueries.getAllHospitals(connection);
            response = new ResponseEntity<>(hospitals, HttpStatus.OK);
        } catch (SQLException e) {
            LOGGER.severe(e.getMessage());
            response = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return response;
    }


    /**
     * End point for getting a hospital id using the name of that hospital.
     *
     * @param hospitalName The name of the hospital you want to find the id for.
     * @param authToken    The authentication token string.
     * @return A response entity with the hospital id if it was successful, otherwise it will return a status code.
     */
    @GetMapping("/hospital")
    @ResponseBody
    public ResponseEntity<Integer> getHospitalId(@RequestParam("name") String hospitalName,
                                                 @RequestHeader(value = "X-Authorization") String authToken) {
        DatabaseManager dbManager = new DatabaseManager();
        HospitalQueries hospitalQueries = new HospitalQueries();
        int hospitalId;

        try (Connection connection = dbManager.getConnection()) {

            // Checks if the user is logged in.
            if (!TokenVerifier.userTokenExists(authToken, connection)) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }

            hospitalId = hospitalQueries.getHospitalId(connection, hospitalName);
        } catch (SQLException e) {
            LOGGER.severe(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>(hospitalId, HttpStatus.OK);
    }

    @GetMapping("/hospital/{hospitalName}/clinicians")
    @ResponseBody
    public ResponseEntity<List<Clinician>> getCliniciansAtHospital(@PathVariable("hospitalName") String hospitalName,
                                                                   @RequestHeader(value = "X-Authorization") String authToken) {
        DatabaseManager dbManager = new DatabaseManager();
        ClinicianQueries queries = new ClinicianQueries();

        try (Connection connection = dbManager.getConnection()) {

            if (!TokenVerifier.isStaff(authToken, connection)) {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }

            List<Clinician> clinicians = queries.getClinicians(hospitalName, connection);

            return new ResponseEntity<>(clinicians, HttpStatus.OK);


        } catch (SQLException e) {
            LOGGER.severe(e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
