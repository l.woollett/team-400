package ServerMain.Controllers;

import ServerMain.Model.UserQueries;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import seng302.Database.DatabaseManager;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.logging.Logger;

@RestController
public class UserController {

    private final Logger LOGGER = Logger.getLogger(getClass().getName());
    private UserQueries userQueries = new UserQueries();
    private DatabaseManager dbManager = new DatabaseManager();

    /**
     * Deletes the User from the DB.
     *
     * @param username String username of User to remove.
     * @return response ResponseEntity.
     */
    @DeleteMapping(value = "/users/{username}")
    @ResponseBody
    public ResponseEntity deleteUser(@PathVariable("username") String username) {
        DatabaseManager dbManager = new DatabaseManager();
        UserQueries userQueries = new UserQueries();
        ResponseEntity response = null;
        int result = -1;
        try (Connection connection = dbManager.getConnection()) {
            result = userQueries.deleteUser(username, connection);
        } catch (SQLException e) {
            response = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (result == 0) {
            response = new ResponseEntity(HttpStatus.NOT_FOUND);
        } else if (result == 1) {
            response = new ResponseEntity(HttpStatus.OK);
        }
        return response;
    }
}
