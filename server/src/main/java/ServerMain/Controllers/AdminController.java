package ServerMain.Controllers;

import ServerMain.Model.AdminQueries;
import ServerMain.Model.Authentication.TokenVerifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import seng302.Database.DatabaseManager;
import seng302.Model.Admin;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@RestController
public class AdminController {

    private static final int DUPLICATE_ERROR_CODE = 1062;


    /**
     * Returns all admins on the database.
     *
     * @param authToken String token allowing the function to be called.
     * @return response ResponseEntity of admins.
     */
    @GetMapping("/admins")
    @ResponseBody
    public ResponseEntity<List<Admin>> getAdmins(@RequestHeader(value =
            "X-Authorization") String authToken) {
        DatabaseManager dbManager = new DatabaseManager();
        AdminQueries adminQueries = new AdminQueries();
        ResponseEntity<List<Admin>> response = null;

        List<Admin> admins = new ArrayList<>();
        try (Connection connection = dbManager.getConnection()) {

            // Checks if the user is a staff member.
            if (!TokenVerifier.isStaff(authToken, connection)) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }
            admins = adminQueries.getAllAdmins(connection);

        } catch (SQLException e) {
            response = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (response == null) {
            response = new ResponseEntity<>(admins, HttpStatus.OK);
        }

        return response;
    }

    /**
     * Gets a single admin from the database given the correct username.
     *
     * @param username  String username of admin to retrieve.
     * @param authToken String token allowing the function to be called.
     * @return response ResponseEntity.
     */
    @GetMapping("/admin/{username}")
    @ResponseBody
    public ResponseEntity<Admin> getAdmin(@PathVariable("username") String username, @RequestHeader(value =
            "X-Authorization") String authToken) {
        DatabaseManager dbManager = new DatabaseManager();
        AdminQueries adminQueries = new AdminQueries();

        Admin admin;
        try (Connection connection = dbManager.getConnection()) {
            // Checks if the user is a staff member.
            if (!TokenVerifier.isStaff(authToken, connection)) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }
            admin = adminQueries.getAdmin(username, connection);
        } catch (SQLException e) {
            if (e.getErrorCode() == 0) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity<>(admin, HttpStatus.OK);
    }

    /**
     * Deletes the admin from the DB.
     *
     * @param username  String username of admin to remove.
     * @param authToken String token allowing the function to be called.
     * @return response ResponseEntity.
     */
    @DeleteMapping("/admin/{username}")
    @ResponseBody
    public ResponseEntity deleteAdmin(@PathVariable("username") String username, @RequestHeader(value =
            "X-Authorization") String authToken) {
        DatabaseManager dbManager = new DatabaseManager();
        AdminQueries adminQueries = new AdminQueries();

        ResponseEntity response = null;
        int result = -1;
        try (Connection connection = dbManager.getConnection()) {
            // Checks if the user is a staff member.
            if (!TokenVerifier.isStaff(authToken, connection)) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }
            result = adminQueries.deleteAdmin(username, connection);
        } catch (SQLException e) {
            response = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (result == 0) {
            response = new ResponseEntity(HttpStatus.NOT_FOUND);
        } else if (result == 1) {
            response = new ResponseEntity(HttpStatus.OK);
        }
        return response;
    }

    /**
     * Takes an admin and inserts them into the DB.
     *
     * @param admin Admin to be inserted.
     * @return ResponseEntity: Http packet containing status info and a body of data
     */
    @PostMapping("/admin")
    @ResponseBody
    public ResponseEntity postAdmin(@RequestBody Admin admin) {
        DatabaseManager dbManager = new DatabaseManager();
        AdminQueries adminQueries = new AdminQueries();

        try (Connection connection = dbManager.getConnection()) {
            adminQueries.insertAdmin(admin, connection);
        } catch (SQLException e) {
            if (e.getErrorCode() == DUPLICATE_ERROR_CODE) {
                return new ResponseEntity(HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        return new ResponseEntity(HttpStatus.OK);
    }

}
