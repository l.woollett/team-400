package ServerMain.Controllers;

import ServerMain.Model.Authentication.TokenVerifier;
import ServerMain.Model.ClinicianQueries;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import seng302.Database.DatabaseManager;
import seng302.Model.Clinician;
import seng302.Model.Profile;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Logger;

@RestController
public class ClinicianController {

    private final Logger LOGGER = Logger.getLogger(getClass().getName());
    private DatabaseManager dbManager = new DatabaseManager();

    /**
     * Calls queries to retrieve clinician from DB.
     *
     * @param username  String username of clinician ot get from DB.
     * @param authToken String for authentication.
     * @return response ResponseEntity holding a Clinician object if successful, and a status.
     */
    @GetMapping("/clinician/{username}")
    @ResponseBody
    public ResponseEntity<Clinician> getClinician(@PathVariable("username") String username, @RequestHeader(value =
            "X-Authorization") String authToken) {
        ClinicianQueries clinicianQueries = new ClinicianQueries();
        Clinician clinician = null;
        ResponseEntity response = null;

        try (Connection connection = dbManager.getConnection()) {

            // Checks if the user is a staff member.
            if (!TokenVerifier.isStaff(authToken, connection)) {
                return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            }
            clinician = clinicianQueries.getClinician(connection, username);


        } catch (SQLException e) {
            response = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (response == null) {
            if (clinician == null) {
                response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
            } else {
                response = new ResponseEntity<>(clinician, HttpStatus.OK);
            }
        }

        return response;
    }

    /**
     * Calls queries to post a single Clinician to the database.
     *
     * @param clinician Clinician to post to DB.
     * @return response ResponseEntity holding status of operation.
     */
    @PostMapping("/clinician")
    @ResponseBody
    public ResponseEntity postClinician(@RequestBody Clinician clinician, @RequestHeader(value =
            "X-Authorization") String authToken) {
        DatabaseManager dbManager = new DatabaseManager();
        ClinicianQueries clinicianQueries = new ClinicianQueries();
        ResponseEntity response = null;
        int result = -1;
        try (Connection connection = dbManager.getConnection()) {

            // Checks if the user is a staff member.
            if (!TokenVerifier.isStaff(authToken, connection)) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }
            result = clinicianQueries.postClinician(clinician, connection);
        } catch (SQLException e) {
            if (e.toString().contains("Duplicate entry")) {
                LOGGER.warning(e.getMessage());
                response = new ResponseEntity(HttpStatus.BAD_REQUEST);
            } else {
                LOGGER.severe(e.getMessage());
                response = new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        if (result == 0) {
            response = new ResponseEntity(HttpStatus.BAD_REQUEST);
        } else if (result == 1) {
            response = new ResponseEntity(HttpStatus.OK);
        }
        return response;
    }

    /**
     * Calls queries to delete the clinician from the DB.
     *
     * @param username  String username of Clinician to remove.
     * @param authToken The user's authorization token.
     * @return response ResponseEntity.
     */
    @DeleteMapping("/clinician/{username}")
    @ResponseBody
    public ResponseEntity deleteClinician(@PathVariable("username") String username, @RequestHeader(value =
            "X-Authorization") String authToken) {
        DatabaseManager dbManager = new DatabaseManager();
        ClinicianQueries clinicianQueries = new ClinicianQueries();
        int result;

        try (Connection connection = dbManager.getConnection()) {
            // Checks if the user is a staff member.
            if (!TokenVerifier.isStaff(authToken, connection)) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }
            result = clinicianQueries.deleteClinician(username, connection);

        } catch (SQLException e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }

        if (result == 0) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity((HttpStatus.OK));
    }

    /**
     * Handles Queries to update a Clinician in the DB.
     *
     * @param username  String username of Clinician to update.
     * @param clinician Clinician to send.
     * @param authToken The user's authorization token.
     * @return response ResponseEntity holding resulting status of queries.
     */
    @PutMapping("/clinician/{username}")
    @ResponseBody
    public ResponseEntity putClinician(@PathVariable("username") String username, @RequestBody Clinician clinician, @RequestHeader(value =
            "X-Authorization") String authToken) {
        ClinicianQueries clinicianQueries = new ClinicianQueries();
        ResponseEntity response = null;
        int result;
        try {
            if (!TokenVerifier.isStaff(authToken, new DatabaseManager().getConnection())) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }
            result = clinicianQueries.putClinician(username, clinician, new DatabaseManager().getConnection());

        } catch (SQLException e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (result == 0) {
            response = new ResponseEntity(HttpStatus.BAD_REQUEST);
        } else if (result == 1) {
            response = new ResponseEntity(HttpStatus.OK);
        }
        return response;
    }

    /**
     * Sets the profiles foreign key to the clinicians username.
     *
     * @param clinicianUsername String username to be linked.
     * @param profile           Profile to be be updated.
     * @param authToken         String of auth code to be checked for valid permission.
     * @return Integer of rows affected by the query. 0 if failure. 1 if success.
     */
    @PutMapping("/clinician/linkPatient/{clinician}/")
    @ResponseBody
    public ResponseEntity addPatient(@PathVariable("clinician") String clinicianUsername, @RequestBody Profile profile,
                                     @RequestHeader(value =
                                             "X-Authorization") String authToken) {
        ClinicianQueries clinicianQueries = new ClinicianQueries();
        ResponseEntity response = null;
        int result;
        try {
            if (!TokenVerifier.isStaff(authToken, new DatabaseManager().getConnection())) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }
            result = clinicianQueries.addPatient(clinicianUsername, profile.getUsername(), new DatabaseManager().getConnection());

        } catch (SQLException e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (result == 0) {
            response = new ResponseEntity(HttpStatus.BAD_REQUEST);
        } else if (result == 1) {
            response = new ResponseEntity(HttpStatus.OK);
        }
        return response;
    }


    /**
     * Sets the profiles hospitalId to that of the clinicians.
     *
     * @param clinicianUsername String username of clinician in charge of the patient.
     * @param profile           Profile profile to be updated.
     * @param authToken         String authorization key used to verify permission to this endpoint.
     * @return Integer of rows affected by the query. 0 if failure. 1 if success.
     */
    @PutMapping("/clinician/organisation/{clinician}/")
    @ResponseBody
    public ResponseEntity putProfileOrganisation(@PathVariable("clinician") String clinicianUsername, @RequestBody Profile profile,
                                                 @RequestHeader(value =
                                                         "X-Authorization") String authToken) {
        ClinicianQueries clinicianQueries = new ClinicianQueries();
        ResponseEntity response = null;
        int result;
        try {
            if (!TokenVerifier.isStaff(authToken, new DatabaseManager().getConnection())) {
                return new ResponseEntity(HttpStatus.UNAUTHORIZED);
            }
            result = clinicianQueries.putProfileOrganisation(clinicianUsername, profile.getUsername(), new DatabaseManager().getConnection());

        } catch (SQLException e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (result == 0) {
            response = new ResponseEntity(HttpStatus.BAD_REQUEST);
        } else if (result == 1) {
            response = new ResponseEntity(HttpStatus.OK);
        }
        return response;
    }
}
