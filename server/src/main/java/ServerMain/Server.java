package ServerMain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Server {

    /**
     * Starts up the spring server application.
     *
     * @param args String of arguments entered in the terminal at start up.
     */
    public static void main(String[] args) {

        SpringApplication.run(Server.class, args);
    }
}