package ServerMain.Model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserQueries {
    /**
     * Deletes the Clinician from the DB given the Clinician has the param username.
     *
     * @param username   String username of Clinician to delete.
     * @param connection Connection of DB to access.
     * @return int indicating how many rows where changed in the database after query execution.
     * @throws SQLException Exception when error occurs in database.
     */
    public int deleteUser(String username, Connection connection) throws SQLException {
        String deleteQueryForUser = "DELETE FROM User WHERE username = ?";
        PreparedStatement statementForUser = connection.prepareStatement(deleteQueryForUser);
        statementForUser.setString(1, username);
        int response = statementForUser.executeUpdate();
        statementForUser.close();
        return response;
    }

    /**
     * Returns true if the user with the given username exists in the DB.
     *
     * @param username   String of username to find.
     * @param connection Connection of DB.
     * @return Boolean
     * @throws SQLException Exception when error occurs in database.
     */
    public boolean isUser(String username, Connection connection) throws SQLException {
        String getQueryForUser = "SELECT * FROM User WHERE username = ?";
        PreparedStatement isUserStatement = connection.prepareStatement(getQueryForUser);
        isUserStatement.setString(1, username);
        ResultSet response = isUserStatement.executeQuery();
        boolean rows = response.next();
        isUserStatement.close();
        if (!rows) {
            return false;
        } else {
            return true;
        }
    }
}
