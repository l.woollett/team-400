package ServerMain.Model;

import seng302.Enum.AvailableOrganRequestStatus;
import seng302.Enum.OrganEnum;
import seng302.Model.AvailableOrganNotification;
import seng302.Model.Clinician;
import seng302.Model.Hospital;
import seng302.Model.Profile;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Will handle querying directly to the database.
 */
public class NotificationQueries {

    private HospitalQueries hospitalQueries = new HospitalQueries();

    /**
     * Used to create/add a request to the database.
     *
     * @param notification The request instance to be added.
     * @param connection   The connection to the database.
     * @throws SQLException An error occurred during the query.
     */
    public int createNotification(AvailableOrganNotification notification, Connection connection) throws SQLException {

        final String INSERT_QUERY = "INSERT INTO `Notification` (`organ`, `originClinician`, " +
                "`originHospital`, `recipientClinician`, `recipientHospital`, `requestStatus`, `donor`, `createdTime`, `conversationId`) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);";
        PreparedStatement statement = connection.prepareStatement(INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);
        statement.setString(1, notification.getOrgan().toString());
        statement.setString(2, notification.getSender().getUsername());

        int originalHospitalId = hospitalQueries.getHospitalId(connection, notification.getOrigin().getName());
        statement.setInt(3, originalHospitalId);

        statement.setString(4, notification.getReceiver().getUsername());

        int destinationHospitalId = hospitalQueries.getHospitalId(connection, notification.getDestination().getName());
        statement.setInt(5, destinationHospitalId);

        statement.setString(6, notification.getStatus().toString());
        statement.setString(7, notification.getDonor().getUsername());
        statement.setTimestamp(8, Timestamp.valueOf(notification.getSentTimeStamp()));
        statement.setInt(9, notification.getConversationId());

        statement.executeUpdate();
        ResultSet generatedKey = statement.getGeneratedKeys();

        if (generatedKey.next()) {
            return generatedKey.getInt(1);
        } else {
            throw new SQLException("The notification was not created.");
        }
    }

    /**
     * Replace the notification with a new notification. The old and new notification should share the same ID, we
     * will use this to identify which notification was updated.
     *
     * @param notification The new notification whose value will be saved into the database.
     * @param connection   The connection to the database.
     * @throws SQLException Exception occurred during the query.
     */
    public void replaceNotification(AvailableOrganNotification notification, Connection connection) throws SQLException, NullPointerException {
        final String REPLACE = "UPDATE Notification SET organ = ?, originClinician = ?, originHospital = ?, recipientClinician = ?, recipientHospital = ?, requestStatus = ?, donor = ?, createdTime = ?, conversationId = ? WHERE requestId = ?;";
        PreparedStatement statement = connection.prepareStatement(REPLACE);
        statement.setString(1, notification.getOrgan().toString());
        statement.setString(2, notification.getSender().getUsername());

        int originalHospitalId = hospitalQueries.getHospitalId(connection, notification.getOrigin().getName());
        statement.setInt(3, originalHospitalId);

        statement.setString(4, notification.getReceiver().getUsername());

        int destinationHospitalId = hospitalQueries.getHospitalId(connection, notification.getDestination().getName());
        statement.setInt(5, destinationHospitalId);

        statement.setString(6, notification.getStatus().toString());
        statement.setString(7, notification.getDonor().getUsername());
        statement.setTimestamp(8, Timestamp.valueOf(notification.getSentTimeStamp()));
        statement.setInt(9, notification.getConversationId());
        statement.setInt(10, notification.getNotificationId());
        statement.executeUpdate();
    }

    /**
     * Get all not respond incoming notifications for a clinician.
     *
     * @param connection The connection to the database.
     * @param userName   The given clinician username.
     * @return A list of all null value incoming notifications.
     * @throws SQLException Exception occurred during the query.
     */
    public List<AvailableOrganNotification> getAllIncomingNotifications(Connection connection, String userName) throws SQLException {
        ProfileQueries profileQueries = new ProfileQueries();
        ClinicianQueries clinicianQueries = new ClinicianQueries();
        HospitalQueries hospitalQueries = new HospitalQueries();
        List<AvailableOrganNotification> allIncomingNotification = new ArrayList<>();

        final String GET_QUERY = "SELECT * FROM `Notification` WHERE Notification.recipientClinician = ?";

        try (PreparedStatement statement = connection.prepareStatement(GET_QUERY)) {
            statement.setString(1, userName);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Integer conversationId = resultSet.getInt("conversationId");
                    OrganEnum organ = OrganEnum.getOrgan(resultSet.getString("organ"));
                    String donorUserName = resultSet.getString("donor");
                    Profile donor = profileQueries.getProfile(connection, donorUserName);
                    String originClinician = resultSet.getString("originClinician");
                    Clinician sender = clinicianQueries.getClinician(connection, originClinician);
                    String receiverClinician = resultSet.getString("recipientClinician");
                    Clinician receiver = clinicianQueries.getClinician(connection, receiverClinician);
                    int originHospitalId = resultSet.getInt("originHospital");
                    Hospital origin = hospitalQueries.getHospitalById(connection, originHospitalId);
                    int destinationHospitalId = resultSet.getInt("recipientHospital");
                    Hospital destination = hospitalQueries.getHospitalById(connection, destinationHospitalId);
                    AvailableOrganRequestStatus sendStatus = AvailableOrganRequestStatus.getStatus(resultSet.getString("requestStatus"));
                    AvailableOrganNotification anIncomingNotifications = new AvailableOrganNotification(organ, donor, sender, receiver,
                            origin, destination, sendStatus, conversationId);
                    anIncomingNotifications.setNotificationId(resultSet.getInt("requestId"));
                    allIncomingNotification.add(anIncomingNotifications);
                }
            }


            statement.close();
            return allIncomingNotification;
        }
    }

    /**
     * Retrieve all the notifications that the specified clinician created.
     *
     * @param senderClinicianUsername The username of the clinician who created the notifications.
     * @param connection              The connection to the database.
     * @return A collection of the notifications they have created.
     * @throws SQLException Error occurred during the queries.
     */
    public List<AvailableOrganNotification> getNotifications(String senderClinicianUsername, Connection connection) throws SQLException {
        ProfileQueries profileQueries = new ProfileQueries();
        HospitalQueries hospitalQueries = new HospitalQueries();
        ClinicianQueries clinicianQueries = new ClinicianQueries();

        final String GET_QUERY = "SELECT * FROM Notification WHERE originClinician = ?;";
        try (PreparedStatement statement = connection.prepareStatement(GET_QUERY)) {
            statement.setString(1, senderClinicianUsername);
            ResultSet rs = statement.executeQuery();
            List<AvailableOrganNotification> notifications = new ArrayList<>();

            while (rs.next()) {
                int notificationId = rs.getInt("requestId");
                int conversationId = rs.getInt("conversationId");
                OrganEnum organ = OrganEnum.getOrgan(rs.getString("organ"));
                int originalHospitalId = rs.getInt("originHospital");
                String receiverClinicianUsername = rs.getString("recipientClinician");
                int recipientHospitalId = rs.getInt("recipientHospital");
                String donorUsername = rs.getString("donor");
                AvailableOrganRequestStatus status = AvailableOrganRequestStatus.getStatus(rs.getString("requestStatus"));
                LocalDateTime createdTime = rs.getTimestamp("createdTime").toLocalDateTime();

                Hospital originHospital = hospitalQueries.getHospitalById(connection, originalHospitalId);
                Hospital recipientHospital = hospitalQueries.getHospitalById(connection, recipientHospitalId);

                Clinician senderClinician = clinicianQueries.getClinician(connection, senderClinicianUsername);
                Clinician receiverClinician = clinicianQueries.getClinician(connection, receiverClinicianUsername);

                Profile donor = profileQueries.getProfile(connection, donorUsername);

                notifications.add(new AvailableOrganNotification(organ, donor, senderClinician, receiverClinician,
                        originHospital, recipientHospital, status, createdTime, notificationId, conversationId));
            }

            return notifications;
        }

    }
}
