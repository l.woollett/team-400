package ServerMain.Model.Authentication;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class LogoutQuery {

    private String logoutSql = "UPDATE User SET token = NULL WHERE token = ?";
    private Connection conn;

    /**
     * Constructor for a logout query.
     *
     * @param connection Connection to the database
     */
    public LogoutQuery(Connection connection) {
        this.conn = connection;
    }

    /**
     * Attempts to log the user out by removing the token and sends back the appropriate error code.
     *
     * @param authToken token which will be need to be removed from the database if it exists.
     * @return ResponseEntity with code 201 for successful deletion of a token, thus being logged out. 400 if the request
     * is malformed.
     * @throws SQLException If there is an error from the database.
     */
    public ResponseEntity logoutUser(String authToken) throws SQLException {
        PreparedStatement statement = conn.prepareStatement(logoutSql);
        statement.setString(1, authToken);
        int numRows = statement.executeUpdate();

        if (numRows > 0) {
            return new ResponseEntity<>(HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
