package ServerMain.Model.Authentication;

import ServerMain.Model.Enums.Role;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static ServerMain.Model.Enums.Role.*;

public class LoginQuery {

    private final static String checkProfileSql = "SELECT * FROM User JOIN Profile ON User.username = Profile.username WHERE User.username = ?";
    private final static String checkClinicianSql = "SELECT * FROM User JOIN Clinician ON User.username = Clinician.username WHERE User.username = ?";
    private final static String checkAdminSql = "SELECT * FROM User WHERE User.username = ? AND password = ? AND isAdmin = 1";
    private final static String addTokenSql = "UPDATE User SET token = ? WHERE User.username = ?";
    private Connection conn;

    /**
     * Constructor for the login query.
     *
     * @param connection That is used to connect to the database.
     */
    public LoginQuery(Connection connection) {
        this.conn = connection;
    }

    /**
     * Attempts to authenticate a user and log them in.
     *
     * @param username Username of the user we are trying to authenticate.
     * @param password Password of the user we are trying to authenticate.
     * @return ResponseEntity with a response code and possibly an AuthToken object inside.
     * Response code 200 and AuthToken if username and password match, user is logged in.
     * Response code 400 if username/ password does not match the database.
     * Response code 500 if SQLException is thrown.
     * @throws SQLException If there is an error between the database.
     */
    public ResponseEntity authenticateUser(String username, String password) throws SQLException {

        // Fist check if it was a user that logged in (donor/receiver)
        PreparedStatement statement = conn.prepareStatement(checkProfileSql);
        statement.setString(1, username);
        ResultSet rs = statement.executeQuery();

        if (rs.next()) {
            AuthToken authToken = addToken(username, USER);
            return new ResponseEntity<>(authToken, HttpStatus.OK);
        }

        // Now try checking for the clinician
        statement = conn.prepareStatement(checkClinicianSql);
        statement.setString(1, username);
        rs = statement.executeQuery();

        if (rs.next()) {
            AuthToken authToken = addToken(username, CLINICIAN);
            return new ResponseEntity<>(authToken, HttpStatus.OK);
        }

        // Now try checking for the admin
        statement = conn.prepareStatement(checkAdminSql);
        statement.setString(1, username);
        statement.setString(2, password);
        rs = statement.executeQuery();
        if (rs.next()) {
            AuthToken authToken = addToken(username, ADMIN);
            return new ResponseEntity<>(authToken, HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    /**
     * Create token to be sent back to the user.
     *
     * @param username The username which relates to the account the token will be added to.
     * @param role     The role determines what type of user just logged in.
     * @return Instance that will contain the token and user level.
     * @throws SQLException If there is an error between the database.
     */
    private AuthToken addToken(String username, Role role) throws SQLException {
        AuthToken authToken = new AuthToken(role);
        PreparedStatement statement = conn.prepareStatement(addTokenSql);
        statement.setString(1, authToken.getToken());
        statement.setString(2, username);
        statement.execute();
        statement.close();

        return authToken;
    }
}
