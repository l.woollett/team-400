package ServerMain.Model.Authentication;

/**
 * A class representing the body of a client side login request. It is required if we wish to take full advantage of
 * spring boots automatic conversion to JSON when sending objects to the server.
 */
public class LoginRequestBody {

    /**
     * Username to log in with.
     */
    private String username;
    /**
     * Password to log in with.
     */
    private String password;

    /**
     * Constructs a LoginRequestBody.
     */
    public LoginRequestBody() {
    }

    /**
     * Constructs a LoginRequestBody with a given username and password.
     *
     * @param username The username String of the user logging in.
     * @param password The password String of the user logging in.
     */
    public LoginRequestBody(String username, String password) {
        this.username = username;
        this.password = password;
    }

    /**
     * Gets the current username of the LoginRequestBody.
     *
     * @return The current username of the LoginRequestBody.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Gets the current password of the LoginRequestBody.
     *
     * @return The current password of the LoginRequestBody.
     */
    public String getPassword() {
        return password;
    }
}

