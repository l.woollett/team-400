package ServerMain.Model.Authentication;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;

/**
 * Is used to check if the request has suitable authorization.
 */
public class TokenVerifier {

    private static final Logger LOGGER = Logger.getLogger(TokenVerifier.class.getName());

    /**
     * Checks whether there is a user in the DB with the given name and token.
     *
     * @param token      String Authorization token assigned when logged in.
     * @param connection Connection to the DB.
     * @return boolean true if user with token exists.
     * @throws SQLException Exception occurred during the query.
     */
    public static boolean userTokenExists(String token, Connection connection) throws SQLException {
        String userExistsQuery = "SELECT * FROM User WHERE token = ?";
        PreparedStatement tokenExists = connection.prepareStatement(userExistsQuery);
        tokenExists.setString(1, token);
        ResultSet response = tokenExists.executeQuery();
        boolean exists = response.next();
        tokenExists.close();
        if (exists) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks whether there is a Clinician in the DB with the given username and token.
     *
     * @param token      String Authorization token assigned when logged in.
     * @param connection Connection to the DB.
     * @return boolean true if user with token exists.
     * @throws SQLException Exception occurred during the query.
     */
    private static boolean clinicianTokenExists(String token, Connection connection) throws SQLException {
        String clinicianExistsQuery = "SELECT token FROM `User` JOIN `Clinician` ON User.username = Clinician.username WHERE token = ?";
        PreparedStatement tokenExists = connection.prepareStatement(clinicianExistsQuery);
        tokenExists.setString(1, token);
        ResultSet response = tokenExists.executeQuery();
        boolean exists = response.next();
        tokenExists.close();
        return exists;
    }

    /**
     * Checks whether there is a Admin in the DB with the given username and token.
     *
     * @param token      String Authorization token assigned when logged in.
     * @param connection Connection to the DB.
     * @return boolean true if user with token exists.
     * @throws SQLException Exception occurred during the query.
     */
    private static boolean adminTokenExists(String token, Connection connection) throws SQLException {
        String adminExistsQuery = "SELECT token FROM `User` WHERE token = ? AND isAdmin = 1";
        PreparedStatement tokenExists = connection.prepareStatement(adminExistsQuery);
        tokenExists.setString(1, token);
        ResultSet response = tokenExists.executeQuery();
        boolean exists = response.next();
        tokenExists.close();
        return exists;
    }

    /**
     * Checks if the token belongs to any member of staff (clinician/admin).
     *
     * @param token      String of auth token.
     * @param connection Connection to DB.
     * @return boolean of whether the token belongs to a member of staff (clinician or admin).
     */
    public static boolean isStaff(String token, Connection connection) {
        boolean isAdmin = false;
        boolean isClinician = false;
        try {
            isAdmin = adminTokenExists(token, connection);
            isClinician = clinicianTokenExists(token, connection);

        } catch (SQLException e) {
            LOGGER.severe(e.getMessage());
        }

        return (isAdmin || isClinician);
    }

}
