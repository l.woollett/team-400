package ServerMain.Model.Authentication;

import ServerMain.Model.Enums.Role;

import java.util.UUID;

public class AuthToken {

    private String token;
    private Role role;

    /**
     * Constructs and AuthToken. This is constructor is required by spring boot for conversion after receiving a
     * response with an AuthToken in it.
     */
    public AuthToken() {
    }

    /**
     * Constructor for an authentication token which returns a string representation of a UUID.
     */
    AuthToken(Role role) {
        this.token = UUID.randomUUID().toString();
        this.role = role;
    }

    /**
     * Gets the current token of the AuthToken.
     *
     * @return The current token of the AuthToken.
     */
    public String getToken() {
        return token;
    }

    /**
     * Gets the current role of the AuthToken.
     *
     * @return The current role of the AuthToken.
     */
    public Role getRole() {
        return role;
    }
}
