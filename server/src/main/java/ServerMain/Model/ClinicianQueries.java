package ServerMain.Model;

import seng302.Enum.GenderEnum;
import seng302.Enum.RegionEnum;
import seng302.Model.Clinician;
import seng302.Model.Hospital;
import seng302.ModelController.UserController;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ClinicianQueries extends Queries {

    public ClinicianQueries() {
    }

    /**
     * Deletes the Clinician from the DB given the Clinician has the param username.
     *
     * @param username   String username of Clinician to delete.
     * @param connection Connection of DB to access.
     * @return int indicating how many rows where changed in the database after query execution.
     * @throws SQLException Exception when error occurs in database
     */
    public int deleteClinician(String username, Connection connection) throws SQLException {
        String deleteQueryForClinician = "DELETE FROM Clinician WHERE username = ?";
        PreparedStatement statementForClinician = connection.prepareStatement(deleteQueryForClinician);
        statementForClinician.setString(1, username);
        statementForClinician.executeUpdate();
        statementForClinician.close();

        String deleteQueryForUser = "DELETE FROM User WHERE username = ?";
        PreparedStatement statementForUser = connection.prepareStatement(deleteQueryForUser);
        statementForUser.setString(1, username);
        int response = statementForUser.executeUpdate();
        statementForUser.close();

        return response;
    }

    /**
     * Gets a Clinician from the DB.
     *
     * @param connection Connection to the DB.
     * @param userName   String of username .
     * @return Clinician object.
     * @throws SQLException Exception when error occurs in database
     */
    public Clinician getClinician(Connection connection, String userName) throws SQLException {
        ResultSet resultSet;
        String query = "SELECT * FROM `Clinician` JOIN `User` ON Clinician.username = User.username WHERE User.username = ?";

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, userName);
            resultSet = statement.executeQuery();
            boolean rows = resultSet.next();

            if (!rows) {
                return null;
            }

            return extractClinicianFromResults(resultSet, connection);
        }
    }

    /**
     * Add a clinician into the database.
     *
     * @param clinician  The clinician to be added into the database.
     * @param connection The connection to the database.
     * @return response Int rows affected.
     * @throws SQLException Exception when error occurs in database.
     */
    public int postClinician(Clinician clinician, Connection connection) throws SQLException {
        final String insertUserQuery =
                "INSERT INTO `User` (`username`, `address`, `createdDate`, `region`, `dateOfBirth`, `lastName`, `firstName`, `middleName`, `gender`, `modifiedDate`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
        PreparedStatement insertUser = connection.prepareStatement(insertUserQuery);

        insertUser.setString(1, clinician.getUsername());
        insertUser.setString(2, clinician.getAddress());


        java.sql.Date sqlCreatedDate = convertToSqlDate(clinician.getCreatedDate());

        insertUser.setDate(3, sqlCreatedDate);
        if (clinician.getRegion() != null) {
            insertUser.setString(4, clinician.getRegion().toString());
        } else {
            insertUser.setString(4, "");
        }

        insertUser.setDate(5, java.sql.Date.valueOf(clinician.getDateOfBirth()));

        insertUser.setString(6, clinician.getLastName());
        insertUser.setString(7, clinician.getFirstName());
        insertUser.setString(8, clinician.getMiddleName());

        if (clinician.getGender() == UserController.GENDER_NOT_SET) {
            insertUser.setString(9, null);
        } else {
            insertUser.setString(9, clinician.getGender().toString());
        }

        java.sql.Date sqlModifiedDate = convertToSqlDate(clinician.getModifiedDate());
        insertUser.setDate(10, sqlModifiedDate);
        insertUser.execute();
        insertUser.close();

        final String insertClinicianQuery = "INSERT INTO `Clinician` (`staffId`, `organisation`, `username`) VALUES (?, ?, ?);";

        int hospitalId = new HospitalQueries().getHospitalId(connection, clinician.getHospital().getName());

        PreparedStatement insertClinician = connection.prepareStatement(insertClinicianQuery);
        insertClinician.setString(1, clinician.getStaffId());
        insertClinician.setInt(2, hospitalId);
        insertClinician.setString(3, clinician.getUsername());
        int result = insertClinician.executeUpdate();
        insertClinician.close();
        return result;
    }

    /**
     * Patch a given clinician in the DB.
     *
     * @param username   String of clinician's username in the DB.
     * @param clinician  changes to be made.
     * @param connection Connection to the database.
     * @return int of rows affected.
     * @throws SQLException Exception when error occurs in database.
     */
    public int putClinician(String username, Clinician clinician, Connection connection) throws SQLException {

        saveUser(clinician, connection);

        final String SAVE_CLINICIAN_QUERY = "INSERT INTO Clinician (staffId, organisation, username) VALUES (?, ?, ?) " +
                "ON DUPLICATE KEY UPDATE staffId = ?, organisation = ?;";

        int hospitalId = new HospitalQueries().getHospitalId(connection, clinician.getHospital().getName());

        PreparedStatement saveClinicianStatement = connection.prepareStatement(SAVE_CLINICIAN_QUERY);
        saveClinicianStatement.setString(1, clinician.getStaffId());
        saveClinicianStatement.setInt(2, hospitalId);
        saveClinicianStatement.setString(3, username);
        saveClinicianStatement.setString(4, clinician.getStaffId());
        saveClinicianStatement.setInt(5, hospitalId);
        int result = saveClinicianStatement.executeUpdate();
        saveClinicianStatement.close();
        return result;
    }

    /**
     * Sets the Profiles clinicianUserName foreign key to link to the given clinicians key.
     *
     * @param patientsUsername   String username of the profile to be updated.
     * @param cliniciansUsername String username of the clinician that is in charge of this patient.
     * @param connection         Connection to the DB.
     * @return Integer of the number of rows affected by the query. 0 if failure, 1 if successful.
     * @throws SQLException When the Sql statement is incorrect or the DB is broken.
     */
    public int addPatient(String cliniciansUsername, String patientsUsername, Connection connection) throws SQLException {

        String queryString = "UPDATE Profile SET clinicianUserName = ? WHERE Profile.username = ?";
        PreparedStatement setProfilesClinicianStatement = connection.prepareStatement(queryString);
        setProfilesClinicianStatement.setString(1, cliniciansUsername);
        setProfilesClinicianStatement.setString(2, patientsUsername);

        int result = setProfilesClinicianStatement.executeUpdate();
        setProfilesClinicianStatement.close();
        return result;
    }


    /**
     * Updates the profiles hospitalId to the clinicians organisation number.
     *
     * @param cliniciansUsername String username of the clinician that is in charge of this patient.
     * @param patientsUsername   String username of the profile to be updated.
     * @param connection         Connection to the DB.
     * @return Integer of the number of rows affected by the query. 0 if failure, 1 if successful.
     * @throws SQLException When the Sql statement is incorrect or the DB is broken.
     */
    public int putProfileOrganisation(String cliniciansUsername, String patientsUsername, Connection connection) throws SQLException {
        String queryString = "UPDATE Profile JOIN Clinician ON Profile.clinicianUserName = Clinician.username SET Profile.hospitalId = Clinician.organisation WHERE Profile.username = ? AND Clinician.username = ?";
        PreparedStatement setProfilesClinicianStatement = connection.prepareStatement(queryString);
        setProfilesClinicianStatement.setString(1, patientsUsername);
        setProfilesClinicianStatement.setString(2, cliniciansUsername);

        int result = setProfilesClinicianStatement.executeUpdate();
        setProfilesClinicianStatement.close();
        return result;
    }

    /**
     * Get all the clinicians at a hospital.
     *
     * @param hospitalName The name of the hospital.
     * @param connection   The connection to the database.
     * @return The clinicians that work at the specific hospital.
     */
    public List<Clinician> getClinicians(String hospitalName, Connection connection) throws SQLException {
        final String QUERY = "SELECT * FROM Clinician JOIN User ON Clinician.username = User.username JOIN Hospitals ON Clinician.organisation = Hospitals.HospitalID WHERE Hospitals.Name = ?;";
        List<Clinician> clinicians = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(QUERY)) {
            statement.setString(1, hospitalName);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Clinician clinician = extractClinicianFromResults(resultSet, connection);
                clinicians.add(clinician);
            }

            return clinicians;
        }
    }

    /**
     * This will create a clinician using the results from the result set. This relies on the column names being
     * accurate and the provided result set contains all the necessary columns.
     *
     * @param resultSet  The result set which contains all the required columns.
     * @param connection The connection to the database.
     * @return The clinician.
     * @throws SQLException The column didn't exist.
     */
    private Clinician extractClinicianFromResults(ResultSet resultSet, Connection connection) throws SQLException {
        String firstName = resultSet.getString("firstName");
        String middleName = resultSet.getString("middleName");
        String lastName = resultSet.getString("lastName");
        String username = resultSet.getString("userName");
        String address = resultSet.getString("address");
        RegionEnum region = RegionEnum.getRegion(resultSet.getString("region"));
        GenderEnum gender = GenderEnum.getGender(resultSet.getString("gender"));
        Date createdDate = resultSet.getDate("createdDate");

        LocalDate dob = null;
        if (resultSet.getDate("createdDate") != null) {
            dob = resultSet.getDate("createdDate").toLocalDate();
        }

        int organisationId = resultSet.getInt("organisation");
        HospitalQueries hospitalQueries = new HospitalQueries();
        Hospital hospital = hospitalQueries.getHospitalById(connection, organisationId);

        String staffId = resultSet.getString("staffId");

        return new Clinician(firstName, middleName, lastName, username, address, region, gender, createdDate, dob,
                hospital, staffId);
    }
}
