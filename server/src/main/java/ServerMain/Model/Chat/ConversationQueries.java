package ServerMain.Model.Chat;

import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpServerErrorException;
import seng302.CustomException.EmptyList;
import seng302.Enum.ConversationStatusEnum;
import seng302.Model.Chat.ConversationClinicianJoin;
import seng302.Model.Chat.Conversation;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class ConversationQueries {

    private final Logger LOGGER = Logger.getLogger(getClass().getName());

    /**
     * Inserts a new conversation into the database.
     *
     * @param conversation The conversation object that is to be inserted into the database.
     * @param connection The connection between the server and the database.
     * @throws SQLException Thrown if there was an error in the query.
     */
    public int createConversation(Conversation conversation, Connection connection) throws SQLException {
        final String conversationSql = "INSERT INTO `Conversation` (creatorUsername, topic) VALUES (?, ?)";
        int insertedId;
        String usernameCreator = conversation.getClinicianUsername();
        String topic = conversation.getConversationTopic();
        if (usernameCreator == null) {
            throw new HttpServerErrorException(HttpStatus.BAD_REQUEST);
        }
        PreparedStatement statement = connection.prepareStatement(conversationSql, Statement.RETURN_GENERATED_KEYS);

        statement.setString(1, usernameCreator);
        statement.setString(2, topic);
        statement.executeUpdate();

        ResultSet resultSet = statement.getGeneratedKeys();
        if (resultSet.next()) {
            insertedId = resultSet.getInt(1);
            insertClinicianConversationJoin(insertedId, usernameCreator, connection);
        } else {
            throw new SQLException("Failed to insert conversation!");
        }

        statement.close();
        return insertedId;
    }

    /**
     * Does the extra insert to have the join table contain the required data.
     *
     * @param id The id of the conversation.
     * @param clinicianUsername The clinicians username.
     * @param connection The connection to the database.
     * @throws SQLException If an exception has occurred during the query.
     */
    private void insertClinicianConversationJoin(int id, String clinicianUsername,Connection connection) throws SQLException {
        final String sql = "INSERT INTO `ConversationClinicianJoin` (`conversationId`, `clinician`) VALUES (?, ?)";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, id);
        statement.setString(2, clinicianUsername);
        statement.executeUpdate();
    }

    /**
     * Gets all of the conversations for a particular clinician based on which status of conversation.
     *
     * @param status The conversation status, can be active/ inactive or all.
     * @param username The username of the clinician that we are getting the conversations for.
     * @param connection The connection to the database.
     * @return A list of conversations. Empty list if no conversations exist.
     * @throws SQLException Thrown if an error occurs during the query.
     */
    public List<Conversation> getConversations(String status, String username, Connection connection) throws SQLException {
        List<Conversation> conversations = new ArrayList<>();
        PreparedStatement statement;

        final String sqlWithStatus = "SELECT Conversation.conversationId, Conversation.creationTimestamp, " +
        "Conversation.conversationStatus, Conversation.conversationEndTimestamp, " +
                "Conversation.creatorUsername, Conversation.topic " +
                "FROM Conversation " +
                "JOIN ConversationClinicianJoin ON Conversation.conversationId = ConversationClinicianJoin.conversationId " +
                "WHERE ConversationClinicianJoin.clinician = ? " +
                "AND Conversation.conversationStatus = ? " +
                "GROUP BY ConversationClinicianJoin.conversationId;";

        final String sqlAllStatus = "SELECT Conversation.conversationId, Conversation.creationTimestamp, " +
                "Conversation.conversationStatus, Conversation.conversationEndTimestamp, " +
                "Conversation.creatorUsername, Conversation.topic " +
                "FROM Conversation " +
                "JOIN ConversationClinicianJoin ON Conversation.conversationId = ConversationClinicianJoin.conversationId " +
                "WHERE ConversationClinicianJoin.clinician = ? " +
                "GROUP BY ConversationClinicianJoin.conversationId";

        if (status == null || username == null) {
            throw  new HttpServerErrorException(HttpStatus.BAD_REQUEST);
        }

        if (status.toUpperCase().equals(ConversationStatusEnum.ALL.toString())) {
            statement = connection.prepareStatement(sqlAllStatus);
        } else {
            statement = connection.prepareStatement(sqlWithStatus);
            statement.setString(2, status.toUpperCase());
        }

        statement.setString(1, username);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next()) {
            int id = resultSet.getInt("conversationId");
            LocalDateTime timestamp = resultSet.getTimestamp("creationTimeStamp").toLocalDateTime();
            String clinicianUsername = resultSet.getString("creatorUsername");
            String topic = resultSet.getString("topic");
            Timestamp sqlTime = resultSet.getTimestamp("conversationEndTimestamp");
            LocalDateTime endTime;
            if (sqlTime != null) {
                endTime = sqlTime.toLocalDateTime();
            } else {
                endTime = null;
            }
            ConversationStatusEnum conversationStatus = ConversationStatusEnum.getStatus(resultSet.getString("conversationStatus"));

            conversations.add(new Conversation(id, timestamp, clinicianUsername, topic, endTime, conversationStatus));
        }

        statement.close();
        return conversations;
    }

    /**
     * Query to add participants to a conversation.
     *
     * @param conversationId     The conversation id that the clinicians need to be added to.
     * @param clinicianUsernames A list of clinician usernames that need to be added.
     * @param connection         The connection to the database.
     * @throws EmptyList    Thrown if an empty list of clinicians is passed in.
     * @throws SQLException Thrown is an error occurs during the query.
     */
    public void addParticipants(int conversationId, List<String> clinicianUsernames, Connection connection) throws EmptyList, SQLException {
        PreparedStatement statement;
        StringBuilder sb = new StringBuilder("VALUES");

        if (clinicianUsernames.isEmpty()) {
            throw new EmptyList("Cannot add 0 participants to a chat!");
        }

        for (int i = 0; i < clinicianUsernames.size(); i++) {
            // Dynamically construct the query
            sb.append("(?, ?),");
        }

        // Gets rid of trailing comma.
        String values = sb.substring(0, sb.length() - 1);

        final String sql = "INSERT INTO ConversationClinicianJoin (conversationId, clinician) " + values;
        statement = connection.prepareStatement(sql);
        int k = 0; // Index that the conversation id needs to be inserted into.
        int j = 2; // Index that clinician username needs to be inserted into.
        for (int i = 0; i < clinicianUsernames.size(); i++) {
            k = i == 0 ? 1 : k + 2;
            statement.setInt(k, conversationId);
            statement.setString(j, clinicianUsernames.get(i));
            j += 2; // Move forward 2 indexes as the clinician username is the second value that needs to be inserted.
        }

        statement.executeUpdate();
    }

    /**
     * Will remove the list of clinicians from the conversation. It will does not count as an error if we're removing
     * a clinician from a chat they were not originally in.
     *
     * @param conversationId The unique identifier for the conversation.
     * @param clinicianUsernames The list of clinicians that will be removed from the table.
     * @param connection The connection to the database.
     * @exception SQLException Error occurred during the query.
     * @exception EmptyList No clinician usernames were provided.
     */
    public void removeParticipants(int conversationId, List<String> clinicianUsernames, Connection connection) throws SQLException, EmptyList {
        final String QUERY = "DELETE FROM ConversationClinicianJoin WHERE conversationId = ? AND clinician = ?";

        if(clinicianUsernames.isEmpty()) {
            throw new EmptyList("There are no clinicians to remove from the chat.");
        }

        for (String username : clinicianUsernames) {
            try (PreparedStatement statement = connection.prepareStatement(QUERY)) {
                statement.setInt(1, conversationId);
                statement.setString(2,username);
                statement.executeUpdate();
            }
        }
    }

    /**
     * Updates the lastRead column of the given updateItem fields.
     * This is called when the clinician gets a conversation.
     * @param updateItem ConversationClinicianJoin holding the clinicians username and the ConversationId.
     * @param connection Connection to the DB.
     * @return Integer of rows affected by update.
     * @throws SQLException if an error occurred during the query.
     */
    public Integer updateLastRead(ConversationClinicianJoin updateItem, Connection connection) throws SQLException {
        final String sql = "UPDATE `ConversationClinicianJoin` SET lastRead = CURRENT_TIMESTAMP WHERE conversationId = ? AND clinician = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setInt(1, updateItem.getConversationId());
        statement.setString(2, updateItem.getClinicianUsername());
        int result = statement.executeUpdate();
        statement.close();
        return result;
    }



    /**
     * End one conversation by updating the matching record to inactive status and log the end time with a current time.
     *
     * @param conversationId  Given conversation id to end with.
     * @param connection         Connection to the DB.
     * @return Integer of the number of rows affected by the query. 0 if failure, 1 if successful.
     * @throws SQLException When the Sql statement is incorrect or the DB is broken.
     */
    public int endConversation(int conversationId, Connection connection) throws SQLException {
        String queryString = "UPDATE Conversation SET conversationStatus = ?, conversationEndTimestamp= ? WHERE conversationId = ?";
        PreparedStatement endOneConversationStatement = connection.prepareStatement(queryString);
        endOneConversationStatement.setString(1, ConversationStatusEnum.INACTIVE.toString());
        endOneConversationStatement.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
        endOneConversationStatement.setInt(3, conversationId);

        int result = endOneConversationStatement.executeUpdate();
        endOneConversationStatement.close();
        return result;
    }

    /**
     * Retrieves the conversationIds of the chats that have unread messages.
     * @param clinicianName String of the clinicians name to be found.
     * @param connection Connection to the DB.
     * @return List of the conversations which have unread messages.
     * @throws SQLException If something screws up with the sql query.
     */
    public List<Integer> getUnreadConversationIds(String clinicianName, Connection connection) throws SQLException {
        final String queryString = "SELECT ConversationClinicianJoin.conversationId FROM `ConversationClinicianJoin` " +
                "JOIN `Messages` ON ConversationClinicianJoin.conversationId = Messages.conversationId " +
                "JOIN `Conversation` ON ConversationClinicianJoin.conversationId = Conversation.conversationId " +
                "WHERE (lastRead <= sentTimestamp OR lastRead IS NULL) " +
                "AND clinician = ? " +
                "AND NOT Messages.sender = ? " +
                "AND Conversation.conversationStatus = ? " +
                "GROUP BY ConversationClinicianJoin.conversationId";
        List<Integer> unreadChatIds = new ArrayList<>();
        PreparedStatement statement = connection.prepareStatement(queryString);

        statement.setString(1, clinicianName);
        statement.setString(2, clinicianName);
        statement.setString(3,"ACTIVE");
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next()){
            unreadChatIds.add(resultSet.getInt(1));
        }

        statement.close();
        return unreadChatIds;
    }

}
