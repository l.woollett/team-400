package ServerMain.Model.Chat;

import seng302.Model.Chat.Message;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MessageQueries {

    /**
     * Get a list of messages with the same conversation Id.
     *
     * @param conversationId Given Id for the conversation to query with.
     * @param connection     Database connection.
     * @return A list of messages relates to the given conversation Id.
     * @throws SQLException Exception occurred during the query.
     */
    public List<Message> getMessageListForConversationId(int conversationId, Integer lastMessageId,
                                                         Connection connection) throws SQLException {
        List<Message> messages = new ArrayList<>();
        PreparedStatement statement;

        String queryString;

        if(lastMessageId == null) {
            queryString = "SELECT * FROM `Messages` WHERE conversationId = ? ";
            statement = connection.prepareStatement(queryString);
            statement.setInt(1, conversationId);
        } else {
            queryString = "SELECT * FROM `Messages` WHERE conversationId = ? AND messageId > ? ";
            statement = connection.prepareStatement(queryString);
            statement.setInt(1, conversationId);
            statement.setInt(2, lastMessageId);
        }

        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next()) {
            int messageId = resultSet.getInt("messageId");
            LocalDateTime sentTimestamp = resultSet.getTimestamp("sentTimestamp").toLocalDateTime();
            String senderClinician = resultSet.getString("sender");
            String messageText = resultSet.getString("messageText");

            messages.add(new Message(messageId, sentTimestamp, senderClinician, messageText, conversationId));
        }

        statement.close();
        return messages;

    }

    /**
     * Send a message and insert it into the database.
     *
     * @param message    The message that needs to be sent.
     * @param connection The connection to the database.
     * @throws SQLException Thrown if there is an error with the query.
     */
    public void postMessage(Message message, Connection connection) throws SQLException {
        final int MAX_CHAR_LEN = 4000;

        final String sql = "INSERT INTO Messages (Messages.messageText, Messages.sender, Messages.conversationId) VALUES (?, ?, ?);";

        final String messageText;

        if (message.getMessageText().length() > MAX_CHAR_LEN) {
            messageText = message.getMessageText().substring(0, MAX_CHAR_LEN);
        } else{
            messageText = message.getMessageText();
        }

        final String senderUsername = message.getSenderUsername();
        final int conversationId = message.getConversationId();

        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setString(1, messageText);
        statement.setString(2, senderUsername);
        statement.setInt(3, conversationId);

        statement.executeUpdate();
        statement.close();
    }

    /**
     * Returns true if there are any messages sent after the last viewing.
     * @param username String of the Clinician's username.
     * @param id Integer of the conversationId.
     * @param connection Connection to DB.
     * @return boolean True if there are unread messages.
     * @throws SQLException if an error occurs.
     */
    public Boolean checkForMessages(String username, Integer id, Connection connection) throws SQLException {
        final String query = "SELECT COUNT(*) FROM `ConversationClinicianJoin` JOIN `Messages` ON ConversationClinicianJoin.conversationId = "
                + "Messages.conversationId WHERE (lastRead <= sentTimestamp OR lastRead IS NULL) AND clinician = ? AND Messages.conversationId = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, username);
        statement.setInt(2, id);
        ResultSet resultSet = statement.executeQuery();

        resultSet.next();
        return (resultSet.getInt(1) != 0);
    }

}
