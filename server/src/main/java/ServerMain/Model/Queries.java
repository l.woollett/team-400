package ServerMain.Model;

import seng302.Enum.BloodTypeEnum;
import seng302.Enum.GenderEnum;
import seng302.Enum.OrganEnum;
import seng302.Enum.RegionEnum;
import seng302.Model.*;
import seng302.ModelController.ProfileController;

import java.sql.*;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

/**
 * Contains common functionality when querying to the database.
 */
public abstract class Queries {

    /**
     * Delete a profile's disease from the database.
     *
     * @param username   The profile's username.
     * @param connection The connection to the database.
     * @throws SQLException Exception occurred during the query.
     */
    public static void deleteDisease(String username, Connection connection) throws SQLException {
        String deleteQueryForDisease = "DELETE FROM Disease WHERE profileUsername = ?";
        PreparedStatement statementForDisease = connection.prepareStatement(deleteQueryForDisease);
        statementForDisease.setString(1, username);
        statementForDisease.executeUpdate();
        statementForDisease.close();
    }

    /**
     * Delete the medical procedure information from the database.
     *
     * @param username   The profile's username.
     * @param connection The connection to the database.
     * @throws SQLException Exception occurred during the query.
     */
    public static void deleteMedicalProcedures(String username, Connection connection) throws SQLException {
        final String DELETE_ORGANS = "DELETE FROM ProcedureOrgans WHERE username = ?";
        try(PreparedStatement statement = connection.prepareStatement(DELETE_ORGANS)) {
            statement.setString(1, username);
            statement.executeUpdate();
        }

        final String DELETE_PROCEDURE = "DELETE FROM MedicalProcedure WHERE username = ?;";
        try(PreparedStatement statement = connection.prepareStatement(DELETE_PROCEDURE)) {
            statement.setString(1, username);
            statement.executeUpdate();
        }
    }

    /**
     * Delete the user's history from the database
     *
     * @param username   The user's username.
     * @param connection The connection to the database.
     * @throws SQLException Exception occurred during the query.
     */
    public static void deleteHistory(String username, Connection connection) throws SQLException {
        String deleteQueryForHistory = "DELETE FROM History WHERE username = ?";
        PreparedStatement statementForHistory = connection.prepareStatement(deleteQueryForHistory);
        statementForHistory.setString(1, username);
        statementForHistory.executeUpdate();
        statementForHistory.close();
    }

    /**
     * Delete the medication from the database.
     *
     * @param username   The user's username.
     * @param connection The connection to the database.
     * @throws SQLException Exception occurred during the query.
     */
    public static void deleteMedication(String username, Connection connection) throws SQLException {
        String deleteQueryForMedication = "DELETE FROM Medication WHERE profileUsername = ?";
        PreparedStatement statementForMedication = connection.prepareStatement(deleteQueryForMedication);
        statementForMedication.setString(1, username);
        statementForMedication.executeUpdate();
        statementForMedication.close();
    }

    /**
     * Will remove information about an donated/receiving organs status from the database.
     *
     * @param username   The user's username.
     * @param connection The connection to the database.
     * @throws SQLException Exception occurred during the query.
     */
    public static void deleteDonorOrganInfo(String username, Connection connection) throws SQLException {
        String deleteQueryForOrganProfile = "DELETE FROM DonatingOrgans WHERE username = ?";
        PreparedStatement statementForOrganProfile = connection.prepareStatement(deleteQueryForOrganProfile);
        statementForOrganProfile.setString(1, username);
        statementForOrganProfile.executeUpdate();
        statementForOrganProfile.close();
    }

    /**
     * Function deletes the receiving organs related to the username provided.
     *
     * @param username   Username of the profile who's receiving organs are to be removed.
     * @param connection Connection to the database.
     * @throws SQLException Exception from the server if something is wrong with the query
     */
    public static void deleteReceivingOrganInfo(String username, Connection connection) throws SQLException {
        String query = "DELETE FROM ReceivingOrgans WHERE username = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, username);
        statement.executeUpdate();
        statement.close();
    }

    /**
     * Set the values relating to inserting a profile to the statement.
     *
     * @param profile    The profile that we will retrieve values from.
     * @param statement  The statement values will be set to.
     * @param connection The connection to the database.
     * @return The same statement with values relating to inserting a profile set.
     * @throws SQLException Exception occurred when setting values.
     */
    public static PreparedStatement setInsertProfileValues(Profile profile, PreparedStatement statement, Connection connection)
            throws SQLException {
        statement.setString(1, profile.getBloodPressure());
        statement.setFloat(2, profile.getHeight());

        statement.setBoolean(3, profile.isSmoker());

        if (profile.getBloodType() == ProfileController.BLOOD_TYPE_NOT_SET) {
            statement.setString(4, null);
        } else {
            statement.setString(4, profile.getBloodType().toString());
        }

        if (profile.getBirthGender() == ProfileController.BIRTH_GENDER_NOT_SET) {
            statement.setString(5, null);
        } else {
            statement.setString(5, profile.getBirthGender().toString());
        }

        statement.setString(6, profile.getAlias());
        statement.setFloat(7, profile.getWeight());
        statement.setBoolean(8, profile.isReceiver());

        Death death = profile.getDeath();

        if (death == ProfileController.DEATH_NOT_SET) {
            statement.setDate(9, null);
        } else {
            statement.setDate(9, Date.valueOf(death.getDateOfDeath()));
        }
        statement.setBoolean(10, profile.isDonor());
        statement.setString(11, profile.getAlcoholConsumption().toString());
        statement.setString(12, profile.getUsername());


        if (death == null) {
            statement.setNull(13, Types.INTEGER);
            statement.setNull(14, Types.TIME);
        } else {
            HospitalQueries hospitalQueries = new HospitalQueries();
            int hospitalId = hospitalQueries.getHospitalId(connection, death.getHospital().getName());
            statement.setInt(13, hospitalId);
            statement.setTime(14, profile.getDeath().getTimeOfDeath());
        }
        return statement;
    }

    /**
     * Add donating organs to a profile.
     *
     * @param organs     The organs to add.
     * @param connection The connection to the database.
     * @param username   The username of the profile.
     * @throws SQLException Unexpected MySQL exception.
     */
    public static void insertDonorOrgans(List<OrganEnum> organs, Connection connection, String username)
            throws SQLException {
        final String insertOrgansQuery = "INSERT INTO `DonatingOrgans` (`organ`, `username`) VALUES (?, ?)";
        PreparedStatement insertOrgans = connection.prepareStatement(insertOrgansQuery);
        for (OrganEnum eachOrgan : organs) {
            insertOrgans.setString(1, eachOrgan.toString());
            insertOrgans.setString(2, username);
            insertOrgans.executeUpdate();
        }
        insertOrgans.close();
    }

    /**
     * Adds receiving organs to a profile.
     *
     * @param organs     The list of ReceiverOrgans to be added to the profile.
     * @param connection Connection to the database.
     * @param username   Username of the relevant Profile.
     * @throws SQLException Exception thrown by the database when something is wrong with the query.
     */
    public static void insertReceivingOrgans(List<ReceiverOrgan> organs, Connection connection, String username)
            throws SQLException {
        final String query = "INSERT INTO ReceivingOrgans (organ, dateRegistered, username) VALUES (?, ?, ?)";
        PreparedStatement statement = connection.prepareStatement(query);
        for (ReceiverOrgan receiverOrgan : organs) {
            statement.setString(1, receiverOrgan.getRegisteredOrgan().toString());
            statement.setDate(2, Date.valueOf(receiverOrgan.getDateRegistered()));
            statement.setString(3, username);
            statement.executeUpdate();
        }
        statement.close();
    }

    /**
     * Add the history to a user.
     *
     * @param history    The history to add.
     * @param connection The connection to the database.
     * @param username   The user's username.
     * @throws SQLException Unexpected MySQL exception.
     */
    public static void insertHistory(ArrayList<String> history, Connection connection, String username)
            throws SQLException {
        final String insertHistoryQuery =
                "INSERT INTO `History` (`username`, `description`) VALUES (?, ?)";
        PreparedStatement insertHistory = connection.prepareStatement(insertHistoryQuery);
        for (String lineOfHistory : history) {
            insertHistory.setString(1, username);
            insertHistory.setString(2, lineOfHistory);
            insertHistory.executeUpdate();
        }
        insertHistory.close();
    }

    /**
     * Add a procedure to the user.
     *
     * @param procedures The procedure to add to the profile.
     * @param connection The connection to the database.
     * @param username   The username of the user.
     * @throws SQLException Unexpected MySQL exception.
     */
    public static void insertProcedure(List<Procedure> procedures, Connection connection, String username) throws
            SQLException {
        final String insertProcedureQuery =
                "INSERT INTO `MedicalProcedure` (`dateOfProcedure`, `summary`, `description`, `username`) VALUES (?, " +
                        "?, ?, ?)";
        PreparedStatement insertProcedure =
                connection.prepareStatement(insertProcedureQuery, Statement.RETURN_GENERATED_KEYS);
        for (Procedure procedure : procedures) {
            insertProcedure.setDate(1, Date.valueOf(procedure.getDateOfProcedure()));
            insertProcedure.setString(2, procedure.getSummary());
            insertProcedure.setString(3, procedure.getDescription());
            insertProcedure.setString(4, username);
            insertProcedure.executeUpdate();
            ResultSet resultSet = insertProcedure.getGeneratedKeys();
            resultSet.next();
            int medicalProcedureId = resultSet.getInt(1);
            final String insertProcedureOrganQuery =
                    "INSERT INTO `ProcedureOrgans` (`procedureId`, `organ`, `username`) VALUES (?, ?, ?)";
            PreparedStatement insertProcedureOrgan = connection.prepareStatement(insertProcedureOrganQuery);
            for (OrganEnum organ : procedure.getAffectedOrgans()) {
                insertProcedureOrgan.setInt(1, medicalProcedureId);
                insertProcedureOrgan.setString(2, organ.toString());
                insertProcedureOrgan.setString(3, username);
                insertProcedureOrgan.execute();
            }
            insertProcedureOrgan.close();
        }
        insertProcedure.close();
    }

    /**
     * Add a medication to the user.
     *
     * @param medications The medications to add.
     * @param isCurrent   True is the collection are medications are current.
     * @param connection  The connection to the database.
     * @param username    The username of the user.
     * @throws SQLException Unexpected exception.
     */
    public static void insertMedication(List<String> medications, boolean isCurrent, Connection connection, String
            username) throws SQLException {
        final String insertMedicationQuery =
                "INSERT INTO `Medication` (`isCurrent`, `name`, `profileUsername`) VALUES (?, ?, ?)";
        PreparedStatement statement = connection.prepareStatement(insertMedicationQuery);
        for (String medication : medications) {
            statement.setBoolean(1, isCurrent);
            statement.setString(2, medication);
            statement.setString(3, username);
            statement.execute();
        }
        statement.close();
    }

    /**
     * Add a disease to the profile.
     *
     * @param diseases   The disease to be added to the profile.
     * @param isCurrent  True if the disease are current.
     * @param connection The connection to the database.
     * @param username   The username of the profile.
     * @throws SQLException The unexpected exception.
     */
    public static void insertDisease(List<Disease> diseases, boolean isCurrent, Connection connection, String username)
            throws SQLException {
        final String insertDiseaseQuery =
                "INSERT INTO `Disease` (`isChronicDisease`, `diseaseName`, `isDiseaseCured`, `dateOfDiagnosis`, `isCurrent`, `profileUsername`) VALUES (?, ?, ?, ?, ?, ?);";
        PreparedStatement insertDisease = connection.prepareStatement(insertDiseaseQuery);
        for (Disease disease : diseases) {
            insertDisease.setBoolean(1, disease.getIsChronicDisease());
            insertDisease.setString(2, disease.getDiseaseName());
            insertDisease.setBoolean(3, disease.getIsDiseaseCured());
            insertDisease.setDate(4, Date.valueOf(disease.getDateOfDiagnosis()));
            insertDisease.setBoolean(5, isCurrent);
            insertDisease.setString(6, username);
            insertDisease.execute();
        }
    }

    /**
     * Method to retrieve an array list of medication
     *
     * @param username   the primary key for profile
     * @param isCurrent  boolean indicating if retrieving current medications list.
     * @param connection The connection to the database.
     * @return a list of medication own by the profile.
     * @throws SQLException if query has an error.
     */
    public static ArrayList<String> getMedicationByUsername(String username, boolean isCurrent, Connection connection)
            throws SQLException {
        ArrayList<String> medications = new ArrayList<>();
        String query = "SELECT * FROM `Medication` WHERE profileUsername = ? AND isCurrent = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, username);
        statement.setBoolean(2, isCurrent);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            String medicationName = resultSet.getString("name");
            medications.add(medicationName);
        }
        statement.close();
        return medications;
    }

    /**
     * Method to retrieve an array list of history changes
     *
     * @param username   the username of either profile or clinician
     * @param connection The connection to the database.
     * @return an array list of string of the history changes
     * @throws SQLException if query has an error
     */
    public static ArrayList<String> getHistory(String username, Connection connection) throws SQLException {
        ArrayList<String> listOfHistory = new ArrayList<>();
        String query = "SELECT * FROM `History` WHERE username = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, username);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            String history = resultSet.getString("description");
            listOfHistory.add(history);
        }
        statement.close();
        return listOfHistory;
    }

    /**
     * Get disease by the profile username from the disease table
     *
     * @param username   the profile id that is used to be search
     * @param isCurrent  boolean indicating if retrieving current diseases list.
     * @param connection The connection to the database.
     * @return a list of disease own by the profile
     * @throws SQLException if query contains an error
     */
    public static ArrayList<Disease> getDiseaseByUsername(String username, boolean isCurrent, Connection connection)
            throws
            SQLException {
        ArrayList<Disease> diseases = new ArrayList<>();
        String query = "SELECT * FROM `Disease` WHERE profileUsername = ? AND isCurrent = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, username);
        statement.setBoolean(2, isCurrent);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            String diseaseName = resultSet.getString("diseaseName");
            boolean isChronicDisease = resultSet.getBoolean("isChronicDisease");
            boolean isDiseaseCured = resultSet.getBoolean("isDiseaseCured");
            LocalDate dateOfDiagnosis = resultSet.getDate("dateOfDiagnosis").toLocalDate();
            Disease disease = new Disease(diseaseName, isChronicDisease, isDiseaseCured, dateOfDiagnosis);
            diseases.add(disease);
        }
        statement.close();
        return diseases;
    }

    /**
     * Get list of procedure own by the profile username from medical procedure table
     *
     * @param username   the profile username
     * @param connection The connection to the database.
     * @return a list of procedures
     * @throws SQLException Exception occurred during the query.
     */
    public static ArrayList<Procedure> getProcedureByUsername(String username, Connection connection)
            throws SQLException {
        ArrayList<Procedure> procedures = new ArrayList<>();
        String query = "SELECT * FROM `MedicalProcedure` WHERE username = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, username);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            int procedureId = resultSet.getInt("procedureId");
            String summary = resultSet.getString("summary");
            String description = resultSet.getString("description");
            LocalDate dateOfProcedure = resultSet.getDate("dateOfProcedure").toLocalDate();
            ArrayList<OrganEnum> procedureOrgans = getProcedureOrganByProcedureId(procedureId, connection);
            Procedure resultProcedure = new Procedure(summary, description, dateOfProcedure, procedureOrgans);
            procedures.add(resultProcedure);

        }
        statement.close();
        return procedures;
    }

    /**
     * Retrieve a organs related to a procedure based on the procedure ID.
     *
     * @param procedureId The Procedure ID for the procedure.
     * @param connection  The connection to the database.
     * @return The organs related to the procedure.
     * @throws SQLException Unexpected exception.
     */
    public static ArrayList<OrganEnum> getProcedureOrganByProcedureId(int procedureId, Connection connection) throws
            SQLException {
        ArrayList<OrganEnum> procedureOrgans = new ArrayList<>();
        String query = "SELECT * FROM `ProcedureOrgans` WHERE procedureId = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, procedureId);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            procedureOrgans.add(OrganEnum.getOrgan(resultSet.getString("organ")));
        }
        statement.close();
        return procedureOrgans;
    }

    /**
     * Get a list of organs for the profile id
     *
     * @param username   the profile username that is going to be searched
     * @param connection The connection to the database.
     * @return a list of organs for the profile
     * @throws SQLException Exception occurred during the query.
     */
    public static ArrayList<OrganEnum> getDonorOrgansByUsername(String username, Connection connection)
            throws SQLException {
        ArrayList<OrganEnum> profileOrgans = new ArrayList<>();
        String query = "SELECT * FROM `DonatingOrgans` WHERE username = ?";

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, username);
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    profileOrgans.add(OrganEnum.getOrgan(resultSet.getString("organ")));
                }
            }
        }
        return profileOrgans;
    }

    /**
     * Function retrieves all the ReceiverOrgans from the database related to the Profile.
     *
     * @param username   The username of the relevant Profile.
     * @param connection Connection to the database.
     * @return Arraylist of receiverOrgans
     * @throws SQLException Exception when something is wrong with the query
     */
    public static ArrayList<ReceiverOrgan> getReceiverOrgansOrgansByUsername(String username, Connection connection)
            throws SQLException {
        ArrayList<ReceiverOrgan> receiverOrgans = new ArrayList<>();
        String query = "SELECT * FROM ReceivingOrgans WHERE username = ?";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, username);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            ReceiverOrgan organ = new ReceiverOrgan(OrganEnum.getOrgan(resultSet.getString("organ")), resultSet
                    .getDate("dateRegistered").toLocalDate());
            receiverOrgans.add(organ);
        }
        statement.close();
        return receiverOrgans;
    }

    /**
     * Function solves an issue where we have been using Date objects from different packages. Need to turn a
     * java.sql.Date to a java.util.Date
     *
     * @param date The date as a java.util.Date instance.
     * @return The same date as a java.sql.Date instance.
     */
    protected static java.sql.Date convertToSqlDate(java.util.Date date) {
        if (date != null) {
            return java.sql.Date.valueOf(date.toInstant().atZone(ZoneId.of("Pacific/Auckland")).toLocalDate());
        } else {
            return null;
        }
    }

    /**
     * Function updates the User table in the database for the relevant user entry.
     *
     * @param profile    Profile of the user to be updated.
     * @param connection Connection to the database.
     * @return int of the number of rows that have been updated, should always be at most 1.
     * @throws SQLException thrown when an error has occurred due to the query.
     */
    protected static int updateUserTable(Profile profile, Connection connection) throws SQLException {
        final String UPDATE_USER_QUERY = "UPDATE User SET firstName = ?, middleName = ?, lastName = ?,  address = ?," +
                " region = ?, gender = ?, dateOfBirth = ?, modifiedDate = ? WHERE username = ?";
        PreparedStatement statement = connection.prepareStatement(UPDATE_USER_QUERY);
        statement.setString(1, profile.getFirstName());
        statement.setString(2, profile.getMiddleName());
        statement.setString(3, profile.getLastName());
        statement.setString(4, profile.getAddress());
        statement.setString(5, profile.getRegion().toString());
        if (profile.getGender() == null) {
            statement.setString(6, null);
        } else {
            statement.setString(6, profile.getGender().toString());
        }

        statement.setDate(7, Date.valueOf(profile.getDateOfBirth()));
        statement.setDate(8, convertToSqlDate(profile.getModifiedDate()));
        statement.setString(9, profile.getUsername());
        return statement.executeUpdate();
    }

    /**
     * Function updates the Profile table in the database for the relevant user entry.
     *
     * @param profile    Profile of the user to be updated.
     * @param connection Connection to the database.
     * @return int of the number of rows that have been updated, should always be at most 1.
     * @throws SQLException thrown when an error has occurred due to the query.
     */
    protected static int updateProfileTable(Profile profile, Connection connection) throws SQLException {
        final String UPDATE_PROFILE_QUERY = "UPDATE Profile SET dateOfDeath = ?, height = ?, weight = ?, bloodType = " +
                "?, isDonor = ?, isReceiver = ?, isSmoker = ?, bloodPressure = ?, alcoholConsumption = ?, birthGender" +
                " = ?, alias = ?, hospitalId = ?, clinicianUserName = ?, timeOfDeath = ? WHERE username = ?";

        PreparedStatement statement = connection.prepareStatement(UPDATE_PROFILE_QUERY);
        if (profile.getDeath() == null) {
            statement.setDate(1, null);
            statement.setNull(12, Types.INTEGER);
        } else {
            statement.setDate(1, Date.valueOf(profile.getDeath().getDateOfDeath()));
            HospitalQueries hospitalQueries = new HospitalQueries();
            int hospitalId = hospitalQueries.getHospitalId(connection, profile.getDeath().getHospital().getName());
            statement.setInt(12, hospitalId);
        }
        statement.setFloat(2, profile.getHeight());
        statement.setFloat(3, profile.getWeight());
        if (profile.getBloodType() != null) {
            statement.setString(4, profile.getBloodType().toString());
        } else {
            statement.setString(4, BloodTypeEnum.NOT_SET.toString());
        }
        statement.setBoolean(5, profile.isDonor());
        statement.setBoolean(6, profile.isReceiver());
        statement.setBoolean(7, profile.isSmoker());
        statement.setString(8, profile.getBloodPressure());
        statement.setString(9, profile.getAlcoholConsumption().toString());
        if (profile.getBirthGender() != null) {
            statement.setString(10, profile.getBirthGender().toString());
        } else {
            statement.setString(10, GenderEnum.NOT_SET.toString());
        }
        statement.setString(11, profile.getAlias());

        statement.setString(13, profile.getClinicianName());
        if (profile.getDeath() != null) {
            statement.setTime(14, profile.getDeath().getTimeOfDeath());
        } else {
            statement.setTime(14, null);
        }
        statement.setString(15, profile.getUsername());
        int numRows = statement.executeUpdate();
        statement.close();
        return numRows;
    }

    /**
     * Will save the provided user to the database.
     *
     * @param user       The user to be saved.
     * @param connection The connection to the database.
     * @param <U>        Will accept any model that extends User.
     * @throws SQLException Unexpected error during execution of the query.
     */
    protected <U extends User> void saveUser(U user, Connection connection) throws SQLException {
        final String SAVE_USER_QUERY =
                "INSERT INTO User (username, address, createdDate, region, dateOfBirth, lastName, firstName, middleName, gender, modifiedDate) " +
                        "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE address = ?, createdDate = ?, region = ?, dateOfBirth = ?, lastName = ?, firstName = ?, middleName = ?, gender = ?, modifiedDate = ? ;";

        PreparedStatement saveUserStatement = connection.prepareStatement(SAVE_USER_QUERY);
        saveUserStatement.setString(1, user.getUsername());
        saveUserStatement.setString(2, user.getAddress());
        saveUserStatement.setDate(3, convertToSqlDate(user.getCreatedDate()));

        if (user.getRegion() != null) {
            saveUserStatement.setString(4, user.getRegion().toString());
        } else {
            saveUserStatement.setString(4, null);
        }

        if (user.getDateOfBirth() == null) {
            saveUserStatement.setDate(5, null);
        } else {
            saveUserStatement.setDate(5, Date.valueOf(user.getDateOfBirth()));
        }

        saveUserStatement.setString(6, user.getLastName());
        saveUserStatement.setString(7, user.getFirstName());
        saveUserStatement.setString(8, user.getMiddleName());

        if (user.getGender() == null) {
            saveUserStatement.setString(9, null);
        } else {
            saveUserStatement.setString(9, user.getGender().toString());
        }

        saveUserStatement.setDate(10, convertToSqlDate(user.getModifiedDate()));
        saveUserStatement.setString(11, user.getAddress());
        saveUserStatement.setDate(12, convertToSqlDate(user.getCreatedDate()));

        if (user.getRegion() == null) {
            saveUserStatement.setString(13, RegionEnum.NOT_SET.toString());
        } else {
            saveUserStatement.setString(13, user.getRegion().toString());
        }

        if (user.getDateOfBirth() == null) {
            saveUserStatement.setDate(14, null);
        } else {
            saveUserStatement.setDate(14, Date.valueOf(user.getDateOfBirth()));

        }

        saveUserStatement.setString(15, user.getLastName());
        saveUserStatement.setString(16, user.getFirstName());
        saveUserStatement.setString(17, user.getMiddleName());

        if (user.getGender() == null) {
            saveUserStatement.setString(18, null);
        } else {
            saveUserStatement.setString(18, user.getGender().toString());
        }

        saveUserStatement.setDate(19, convertToSqlDate(user.getModifiedDate()));
        saveUserStatement.execute();
        saveUserStatement.close();
    }

}
