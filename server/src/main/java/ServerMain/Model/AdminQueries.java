package ServerMain.Model;


import seng302.Model.Admin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AdminQueries {


    /**
     * Queries the database to retrieve all admins.
     *
     * @param connection url to the server.
     * @return admins ArrayList of admins retrieved.
     * @throws SQLException Exception when error occurs in database
     */
    public ArrayList<Admin> getAllAdmins(Connection connection) throws SQLException {
        ArrayList<Admin> admins = new ArrayList<>();

        String query = "SELECT username, password FROM User WHERE isAdmin = 1";
        PreparedStatement statement = connection.prepareStatement(query);
        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            String username = resultSet.getString("username");
            String password = resultSet.getString("password");
            admins.add(new Admin(username, password));
        }
        statement.close();
        return admins;
    }

    /**
     * Gets an admin by their username.
     *
     * @param adminName  String username.
     * @param connection Connection to db.
     * @return admin Admin to acquire.
     * @throws SQLException Exception when error occurs in database
     */
    public Admin getAdmin(String adminName, Connection connection) throws SQLException {
        String query = "SELECT username, password FROM `User` WHERE username = ? AND isAdmin = 1";
        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, adminName);
        ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        String password = resultSet.getString("password");
        Admin admin = new Admin(adminName, password);
        statement.close();
        return admin;
    }


    /**
     * Deletes the admin from the DB given the admin has the param username.
     *
     * @param username   String username of admin to delete.
     * @param connection Connection of DB to access.
     * @return int indicating how many rows where changed in the database after query execution.
     * @throws SQLException Exception when error occurs in database.
     */
    public int deleteAdmin(String username, Connection connection) throws SQLException {
        String deleteQueryForAdmin = "DELETE FROM User WHERE username = ? AND isAdmin = 1";
        PreparedStatement statementForAdmin = connection.prepareStatement(deleteQueryForAdmin);
        statementForAdmin.setString(1, username);
        int response = statementForAdmin.executeUpdate();
        statementForAdmin.close();
        return response;
    }

    /**
     * Inserting an admin into the database.
     *
     * @param admin      The admin to be inserted.
     * @param connection The connection to the database.
     * @return int indicating how many rows where changed in the database after query execution.
     * @throws SQLException Exception when error occurs in database
     */
    public int insertAdmin(Admin admin, Connection connection) throws SQLException {
        String insertQueryForAdmin = "INSERT INTO `User` (`username`, `password`, `isAdmin`) VALUES (?, ?, ?);";
        PreparedStatement statementForAdmin = connection.prepareStatement(insertQueryForAdmin);
        statementForAdmin.setString(1, admin.getUsername());
        statementForAdmin.setString(2, admin.getPassword());
        statementForAdmin.setInt(3, 1);
        int response = statementForAdmin.executeUpdate();
        statementForAdmin.close();
        return response;
    }

}
