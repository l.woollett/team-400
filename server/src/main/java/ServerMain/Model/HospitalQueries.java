package ServerMain.Model;

import seng302.Enum.RegionEnum;
import seng302.Model.Hospital;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Directly queries the tables in the database that relates to hospitals.
 */
public class HospitalQueries {

    private final Logger LOGGER = Logger.getLogger(getClass().getName());

    /**
     * Queries the database to get information on all the hospitals.
     *
     * @param connection The connection to the database.
     * @return All the hospitals.
     * @throws SQLException An error occurred during the query.
     */
    public List<Hospital> getAllHospitals(Connection connection) throws SQLException {
        List<Hospital> hospitals = new ArrayList<>();
        final String query = "SELECT Name, Hospital_Region, StreetAddress, Latitude, Longitude FROM `Hospitals`;";
        PreparedStatement statement = connection.prepareStatement(query);
        ResultSet rs = statement.executeQuery();
        while (rs.next()) {
            String name = rs.getString("Name");
            RegionEnum region = RegionEnum.getRegion(rs.getString("Hospital_Region"));
            String address = rs.getString("StreetAddress");
            float latitude = rs.getFloat("Latitude");
            float longitude = rs.getFloat("Longitude");
            hospitals.add(new Hospital(name, region, address, latitude, longitude));
        }

        return hospitals;
    }

    /**
     * Get a hospital's ID based on the name of the hospital.
     *
     * @param connection   The connection to the database.
     * @param hospitalName The hospital's name.
     * @return The hospital's ID.
     * @throws SQLException Error occurred during the query.
     */
    public int getHospitalId(Connection connection, String hospitalName) throws SQLException {
        final String QUERY = "SELECT * FROM `Hospitals` WHERE Name = ?;";
        PreparedStatement statement = connection.prepareStatement(QUERY);
        statement.setString(1, hospitalName);
        ResultSet rs = statement.executeQuery();
        if (rs.next()) {
            return rs.getInt("HospitalID");
        } else {
            throw new SQLException("The provided hospital name '" + hospitalName + "' doesn't match any hospitals");
        }
    }

    /**
     * Returns a hospital based on their ID.
     *
     * @param connection The connection to the database.
     * @param hospitalId The hospital's ID.
     * @return A hospital instance that matches the provided ID.
     * @throws SQLException Error occurred during the query or the ID did not match any hospitals.
     */
    public Hospital getHospitalById(Connection connection, int hospitalId) throws SQLException {
        final String QUERY = "SELECT * FROM `Hospitals` WHERE HospitalID = ?;";
        PreparedStatement statement = connection.prepareStatement(QUERY);
        statement.setInt(1, hospitalId);
        ResultSet rs = statement.executeQuery();
        if (rs.next()) {
            String name = rs.getString("Name");
            RegionEnum region = RegionEnum.getRegion(rs.getString("Hospital_Region"));
            String address = rs.getString("StreetAddress");
            float latitude = rs.getFloat("Latitude");
            float longitude = rs.getFloat("Longitude");
            return new Hospital(name, region, address, latitude, longitude);
        } else {
            throw new SQLException("The hospital ID did not match any existing hospitals. Provided ID: " + hospitalId);
        }

    }
}
