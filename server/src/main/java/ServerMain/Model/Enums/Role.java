package ServerMain.Model.Enums;

public enum Role {
    ADMIN,
    CLINICIAN,
    USER // The basic user of the application (donor/receiver)
}
