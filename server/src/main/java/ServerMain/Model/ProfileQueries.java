package ServerMain.Model;

import seng302.Enum.*;
import seng302.Model.*;
import seng302.ModelController.ProfileController;
import seng302.ModelController.UserController;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

public class ProfileQueries extends Queries {

    private final Logger LOGGER = Logger.getLogger(getClass().getName());
    private final String insertProfileQuery = "INSERT INTO `Profile` (`bloodPressure`, `height`, `isSmoker`, `bloodType`, `birthGender`, `alias`, `weight`, `isReceiver`, `dateOfDeath`, `isDonor`, `alcoholConsumption`, `username`, `hospitalId`, `timeOfDeath`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
    private final String insertUserQuery = "INSERT INTO `User` (`username`, `address`, `createdDate`, `region`, `dateOfBirth`, `lastName`, `firstName`, `middleName`, `gender`, `modifiedDate`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

    public ProfileQueries() {
    }

    /**
     * Deletes a given profile from the DB.
     *
     * @param username   String username of profile to remove.
     * @param connection Connection to DB.
     * @return int of rows affected.
     * @throws SQLException Exception occurred during the query.
     */
    public int deleteProfile(String username, Connection connection) throws SQLException {

        // Deleting associated table rows
        Queries.deleteDonorOrganInfo(username, connection);
        Queries.deleteReceivingOrganInfo(username, connection);
        Queries.deleteMedication(username, connection);
        Queries.deleteHistory(username, connection);
        Queries.deleteDisease(username, connection);
        Queries.deleteMedicalProcedures(username, connection);


        String deleteQueryForProfile = "DELETE FROM Profile WHERE username = ?";
        PreparedStatement statementForProfile = connection.prepareStatement(deleteQueryForProfile);
        statementForProfile.setString(1, username);
        statementForProfile.executeUpdate();
        statementForProfile.close();

        String deleteQueryForUser = "DELETE FROM User WHERE username = ?";
        PreparedStatement statementForUser = connection.prepareStatement(deleteQueryForUser);
        statementForUser.setString(1, username);
        int response = statementForUser.executeUpdate();
        statementForUser.close();
        return response;
    }


    /**
     * Patch a given profile in the DB.
     *
     * @param profile    Profile of person to update.
     * @param connection Connection to the database.
     * @return int of rows affected.
     * @throws SQLException Exception occurred during the query.
     */
    public int updateProfile(Profile profile, Connection connection) throws SQLException {

        int rowsUpdated = Queries.updateUserTable(profile, connection);

        if (rowsUpdated != 1) {
            throw new SQLException("Update table error: The user doesn't exist or multiple entries for username: " +
                    profile.getUsername());
        }

        rowsUpdated = Queries.updateProfileTable(profile, connection);


        // Saving extra fields to DB
        Queries.deleteDonorOrganInfo(profile.getUsername(), connection);
        Queries.deleteReceivingOrganInfo(profile.getUsername(), connection);
        Queries.insertDonorOrgans(profile.getDonorOrgans(), connection, profile.getUsername());
        Queries.insertReceivingOrgans(profile.getReceiverOrgansWithTimes(), connection, profile.getUsername());

        Queries.deleteMedication(profile.getUsername(), connection);
        Queries.insertMedication(profile.getCurrentMedications(), true, connection, profile.getUsername());
        Queries.insertMedication(profile.getPreviousMedications(), false, connection, profile.getUsername());

        Queries.deleteHistory(profile.getUsername(), connection);
        Queries.insertHistory(profile.getChanges(), connection, profile.getUsername());

        Queries.deleteDisease(profile.getUsername(), connection);
        Queries.insertDisease(profile.getCurrentDiseases(), true, connection, profile.getUsername());
        Queries.insertDisease(profile.getPastDiseases(), false, connection, profile.getUsername());

        Queries.deleteMedicalProcedures(profile.getUsername(), connection);
        Queries.insertProcedure(profile.getPastProcedures(), connection, profile.getUsername());
        Queries.insertProcedure(profile.getPendingProcedures(), connection, profile.getUsername());


        return rowsUpdated;
    }

    /**
     * Inserts a profile into the DB.
     *
     * @param profile    Profile to be posted to the DB
     * @param connection Connection to the DB.
     * @return result of rows affected.
     * @throws SQLException Exception occurred during the query.
     */
    public int postProfile(Profile profile, Connection connection) throws SQLException {

        // INSERT A USER
        PreparedStatement insertUser = connection.prepareStatement(insertUserQuery);
        insertUser.setString(1, profile.getUsername());
        insertUser.setString(2, profile.getAddress());

        Date sqlCreatedDate = convertToSqlDate(profile.getCreatedDate());
        insertUser.setDate(3, sqlCreatedDate);
        if (profile.getRegion() != null) {
            insertUser.setString(4, profile.getRegion().toString());
        } else {
            insertUser.setString(4, null);
        }
        insertUser.setDate(5, Date.valueOf(profile.getDateOfBirth()));
        insertUser.setString(6, profile.getLastName());
        insertUser.setString(7, profile.getFirstName());
        insertUser.setString(8, profile.getMiddleName());

        if (profile.getGender() == UserController.GENDER_NOT_SET) {
            insertUser.setString(9, null);
        } else {
            insertUser.setString(9, profile.getGender().toString());
        }

        Date sqlModifiedDate = convertToSqlDate(profile.getModifiedDate());
        insertUser.setDate(10, sqlModifiedDate);
        insertUser.executeUpdate();
        insertUser.close();

        // INSERT A PROFILE
        PreparedStatement insertProfile = connection.prepareStatement(insertProfileQuery);
        insertProfile = Queries.setInsertProfileValues(profile, insertProfile, connection);
        int result = insertProfile.executeUpdate();
        insertProfile.close();

        // Inserting extra data
        Queries.insertDisease(profile.getCurrentDiseases(), true, connection, profile.getUsername());
        Queries.insertDisease(profile.getPastDiseases(), false, connection, profile.getUsername());

        Queries.insertMedication(profile.getCurrentMedications(), true, connection, profile.getUsername());
        Queries.insertMedication(profile.getCurrentMedications(), false, connection, profile.getUsername());

        Queries.insertProcedure(profile.getPendingProcedures(), connection, profile.getUsername());
        Queries.insertProcedure(profile.getPastProcedures(), connection, profile.getUsername());

        Queries.insertDonorOrgans(profile.getDonorOrgans(), connection, profile.getUsername());
        Queries.insertReceivingOrgans(profile.getReceiverOrgansWithTimes(), connection, profile.getUsername());
        Queries.insertHistory(profile.getChanges(), connection, profile.getUsername());

        return result;
    }


    /**
     * Searches the DB and retrieves all profiles that match the provided query string.
     *
     * @param connection Connection to the DB.
     * @param query      String of search parameters and restrictions.
     * @return Array of profiles that match the criteria.
     * @throws SQLException Error during execution of a query
     */
    public ArrayList<Profile> searchProfiles(Connection connection, String query) throws SQLException {
        ArrayList<Profile> allProfiles = new ArrayList<>();

        PreparedStatement statement = connection.prepareStatement(query);
        ResultSet resultSet = statement.executeQuery();

        while (resultSet.next()) {
            Profile profile = constructProfileFromResultSet(resultSet, connection);
            allProfiles.add(profile);
        }
        return allProfiles;
    }

    /**
     * Create a profile based on a result set.
     *
     * @param resultSet  The result set from a query
     * @param connection The connection to the database.
     * @return The Profile
     * @throws SQLException Error occurred during a query.
     */
    private Profile constructProfileFromResultSet(ResultSet resultSet, Connection connection) throws SQLException {
        String username = resultSet.getString("username");
        String address = resultSet.getString("address");
        Date createdDate = resultSet.getDate("createdDate");
        RegionEnum region = RegionEnum.getRegion(resultSet.getString("region"));
        LocalDate dateOfBirth = resultSet.getDate("dateOfBirth").toLocalDate();
        //In case its not set
        Death death = ProfileController.DEATH_NOT_SET;
        Date dateOfDeath = resultSet.getDate("dateOfDeath");
        if (dateOfDeath != null) {
            String hospitalName = resultSet.getString("Name");
            RegionEnum hospitalRegion = RegionEnum.getRegion(resultSet.getString("Hospital_Region"));
            String hospitalAddress = resultSet.getString("StreetAddress");
            float hospitalLatitude = resultSet.getFloat("Latitude");
            float hospitalLongitude = resultSet.getFloat("Longitude");
            Time timeOfDeath = resultSet.getTime("timeOfDeath");
            Hospital hospital = new Hospital(hospitalName, hospitalRegion, hospitalAddress, hospitalLatitude, hospitalLongitude);
            death = new Death(dateOfDeath.toLocalDate(), hospital, timeOfDeath);
        }

        String lastName = resultSet.getString("lastName");
        String firstName = resultSet.getString("firstName");
        String middleName = resultSet.getString("middleName");
        GenderEnum gender = null;
        if (resultSet.getString("gender") != null) {
            gender = GenderEnum.getGender(resultSet.getString("gender").toUpperCase());
        }
        GenderEnum birthGender = null;
        if (resultSet.getString("birthGender") != null) {
            birthGender = GenderEnum.getGender(resultSet.getString("birthGender").toUpperCase());
        }
        String alias = resultSet.getString("alias");
        float height = resultSet.getFloat("height");
        float weight = resultSet.getFloat("weight");
        BloodTypeEnum bloodType = null;
        for (BloodTypeEnum eachBloodTypeEnum : BloodTypeEnum.values()) {
            if (eachBloodTypeEnum.toString().equals(resultSet.getString("bloodType"))) {
                bloodType = eachBloodTypeEnum;
            }
        }
        boolean isDonor = resultSet.getBoolean("isDonor");
        boolean isReceiver = resultSet.getBoolean("isReceiver");
        ArrayList<OrganEnum> donatedOrgans = Queries.getDonorOrgansByUsername(username, connection);
        ArrayList<ReceiverOrgan> receiverOrgans = Queries.getReceiverOrgansOrgansByUsername(username, connection);

        Date modifiedDate = resultSet.getDate("modifiedDate");

        boolean isSmoker = resultSet.getBoolean("isSmoker");
        String bloodPressure = resultSet.getString("bloodPressure");
        AlcoholConsumptionEnum alcoholConsumption = AlcoholConsumptionEnum.getAlcoholConsumption(resultSet.getString("alcoholConsumption"));


        Profile resultProfile = new Profile(firstName, middleName, lastName, dateOfBirth, death,
                gender, birthGender, height, weight, bloodType,
                address, region, createdDate, modifiedDate, isDonor, donatedOrgans, username, alias, isReceiver,
                new ArrayList<>(), bloodPressure, alcoholConsumption, isSmoker);

        resultProfile.setReceiverOrgansWithTimes(receiverOrgans);

        ArrayList<String> historyChanges = Queries.getHistory(username, connection);

        ArrayList<Disease> currentDisease = Queries.getDiseaseByUsername(username, true, connection);
        ArrayList<Disease> pastDisease = Queries.getDiseaseByUsername(username, false, connection);
        resultProfile.setPastDiseases(pastDisease);
        resultProfile.setCurrentDiseases(currentDisease);

        for (Procedure eachProcedure : Queries.getProcedureByUsername(username, connection)) {
            resultProfile.addProcedure(eachProcedure);
        }


        ArrayList<String> currentMedication = Queries.getMedicationByUsername(username, true, connection);
        ArrayList<String> previousMedication = Queries.getMedicationByUsername(username, false, connection);
        resultProfile.addPreviousMedication(previousMedication);
        for (String nameOfEachMedication : currentMedication) {
            resultProfile.addCurrentMedication(nameOfEachMedication);
        }
        resultProfile.setChanges(historyChanges);

        resultProfile.setClinicianName(resultSet.getString("clinicianUserName"));
        resultProfile.setOrganisationName(resultSet.getString("Name"));

        return resultProfile;
    }

    /**
     * Gets a single profile from the DB.
     *
     * @param connection Connection to the DB.
     * @param userName   String username of profile.
     * @return profile Profile to return.
     * @throws SQLException Exception occurred during the query.
     */
    public Profile getProfile(Connection connection, String userName) throws SQLException {

        String query = "SELECT * FROM `Profile` JOIN `User` ON Profile.username = User.username LEFT JOIN `Hospitals` ON Profile.hospitalId = Hospitals.HospitalID WHERE User.username = ?";

        PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userName);
        ResultSet resultSet = statement.executeQuery();
        boolean rows = resultSet.next();

        if (!rows) {
            return null;
        }

        return constructProfileFromResultSet(resultSet, connection);
    }

    /**
     * Gets all the users that are needed for the transplant waiting list
     *
     * @param connection Connection to the DB.
     * @param arguments  Args to insert into the query.
     * @param query      String of the prepared query.
     * @return profile Profiles to return.
     * @throws SQLException Exception occurred during the query.
     */
    public ArrayList<Profile> getWaitingList(Connection connection, String query, List<String> arguments) throws SQLException {
        PreparedStatement statement = connection.prepareStatement(query);
        int index = 1;
        for (String argument : arguments) {
            statement.setString(index, argument);
            index++;
        }

        ResultSet resultSet = statement.executeQuery();

        HashMap<String, Profile> profiles = new HashMap<>();
        ArrayList<Profile> toReturn = new ArrayList<>();
        while (resultSet.next()) {
            ArrayList<ReceiverOrgan> organs = new ArrayList<>();
            if (profiles.containsKey(resultSet.getString("User.username"))) {
                //The user has already got some organs so we want to add to, not create
                Profile profile = profiles.get(resultSet.getString("User.username"));
                organs.addAll(profile.getReceiverOrgansWithTimes());
                OrganEnum organName = OrganEnum.getOrgan(resultSet.getString("ReceivingOrgans.organ").toUpperCase());
                LocalDate organDate = LocalDate.parse(resultSet.getDate("ReceivingOrgans.dateRegistered").toString());
                organs.add(new ReceiverOrgan(organName, organDate));
                profile.setReceiverOrgansWithTimes(organs);
                profiles.replace(resultSet.getString("User.username"), profile);
            } else {
                Profile profile = new Profile();
                OrganEnum organName = OrganEnum.getOrgan((resultSet.getString("ReceivingOrgans.organ").toUpperCase()));
                LocalDate organDate = LocalDate.parse(resultSet.getDate("ReceivingOrgans.dateRegistered").toString());
                organs.add(new ReceiverOrgan(organName, organDate));
                profile.setReceiverOrgansWithTimes(organs);
                profile.setRegion(RegionEnum.getRegion(resultSet.getString("User.region")));
                profile.setFirstName(resultSet.getString("User.firstName"));
                profile.setMiddleName(resultSet.getString("User.middleName"));
                profile.setLastName(resultSet.getString("User.lastName"));
                profile.setUsername(resultSet.getString("User.username"));
                profile.setDateOfBirth(LocalDate.now());
                profiles.put(profile.getUsername(), profile);
            }
        }
        toReturn.addAll(profiles.values());
        return toReturn;
    }


    /**
     * Returns the count of the profiles that match the given search query.
     *
     * @param connection Connection to the DB.
     * @param query      String query with search params.
     * @return Int of number of rows in DB that match the query.
     * @throws SQLException Error during execution of the sql query
     */
    public Integer searchProfilesCount(Connection connection, String query) throws SQLException {

        PreparedStatement statement = connection.prepareStatement(query);
        ResultSet resultSet = statement.executeQuery();

        resultSet.next();
        return resultSet.getInt(1);
    }

    /**
     * Currently only receiving the username of the matched profile as the task says:
     * "Match the donor and receiver with the same organ, blood type and max age diff."
     * Should really return more, but can be added more with the select part of the statement.
     *
     * @param connection Connection to the database
     * @param username   The username of the donor of the organ
     * @param organ      The organ to be donated
     * @return A arraylist of organ receivers usernames.
     * @throws SQLException Error during execution of SQL query.
     */
    public ArrayList<OrganMatchQueryResult> getCompatibleOrgans(Connection connection, String username, String organ) throws SQLException {
        Date now = new java.sql.Date(System.currentTimeMillis());
        String ageQuery = "SELECT dateOfBirth, dateOfDeath FROM User " +
                "Join Profile on Profile.username = User.username " +
                "WHERE Profile.username=?;";

        boolean isUnder12 = false;

        try (PreparedStatement statement = connection.prepareStatement(ageQuery)) {
            statement.setString(1, username);
            try (ResultSet resultSet = statement.executeQuery()) {
                LocalDate dob = LocalDate.now();
                LocalDate dod = LocalDate.now();
                while (resultSet.next()) {
                    dob = resultSet.getDate("dateOfBirth").toLocalDate();
                    dod = resultSet.getDate("dateOfDeath").toLocalDate();
                }
                if ((dod.getYear() - dob.getYear()) < 12) {
                    //Profile is younger than 12
                    isUnder12 = true;
                }
            }
        }
        String query = "SELECT Rec.username, DATEDIFF(?, ReceivingOrgans.dateRegistered) AS diff, " +
                "DonH.Latitude AS dLat, DonH.Longitude as dLong, RecH.Latitude, RecH.Longitude, DonH.Name, RecH.StreetAddress " +
                "FROM ReceivingOrgans " +
                "JOIN Profile Rec ON ReceivingOrgans.username = Rec.username " + //For now, only grabbing the username
                "JOIN Profile Don ON Don.username = ? " +
                "JOIN User DonU ON Don.username = DonU.username " +
                "JOIN Hospitals DonH on Don.hospitalId = DonH.HospitalID " + //Getting the hospital so that we can compare distances
                "JOIN Clinician C ON Rec.clinicianUserName = C.username " + //Since we dont have a rec.hospital id since hte receiver is not dead, we have to use the clinician's org id as the hospital identifier
                "JOIN Hospitals RecH ON C.organisation = RecH.HospitalID " + //Getting the reciever hospital for distance comparison
                "JOIN User RecU ON Rec.username = RecU.username " + //Grabbing the reciever username
                "WHERE organ = ? " +
                "      AND RecH.Name = DonH.Name " + //Same hospital
                "      AND Rec.dateOfDeath IS NULL " +
                "      AND Rec.bloodType = Don.bloodType " + //Making sure their blood types are the same
                "      AND Rec.username != Don.username " + //And not the same person
                "      AND DATEDIFF(RecU.dateOfBirth, DonU.dateOfBirth) < 5475 " + //Checking if there's 15 years diff between the people
                "      AND DATEDIFF(RecU.dateOfBirth, DonU.dateOfBirth) > -5475 "; //Checking the other way if there's more than 15 years difference
        if (isUnder12) {
            query += "AND DATEDIFF(?, RecU.dateOfBirth) < 4380;"; //Making sure the recipient is less than 12 years old
        } else {
            query += "AND DATEDIFF(?, RecU.dateOfBirth) > 4380;"; //Making sure the recipient is more than 12 years old
        }


        ArrayList<OrganMatchQueryResult> matchedOrganReceivers = new ArrayList<>();

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.setString(1, now.toString());
            statement.setString(2, username);
            statement.setString(3, organ);
            statement.setString(4, now.toString());
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    String recUsername = resultSet.getString("username");
                    String name = resultSet.getString("Name");
                    Double donHospitalLat = resultSet.getDouble("dLat");
                    Double donHospitalLong = resultSet.getDouble("dLong");
                    Double recHospitalLat = resultSet.getDouble("Latitude");
                    Double recHospitalLong = resultSet.getDouble("Longitude");
                    int dateDiff = resultSet.getInt("diff");
                    OrganMatchQueryResult result = new OrganMatchQueryResult(recUsername, name, donHospitalLat, donHospitalLong, recHospitalLat, recHospitalLong, dateDiff);
                    matchedOrganReceivers.add(result);
                }
            }
        }
        //Sort
        matchedOrganReceivers.sort(OrganMatchQueryResult::compareTo);

        return matchedOrganReceivers;
    }

    /**
     * Delete an organ from a profile.
     *
     * @param organ      The organ that needs to be deleted.
     * @param username   The username of the profile that the organ needs to be deleted from.
     * @param connection The connection to the database.
     * @return The number of rows that were deleted.
     * @throws SQLException Thrown is there was an error with the query.
     */
    public int deleteDonatingOrgan(OrganEnum organ, String username, Connection connection) throws SQLException {

        PreparedStatement statement;
        final String sql = "DELETE FROM DonatingOrgans WHERE DonatingOrgans.organ = ? AND DonatingOrgans.username = ?";
        statement = connection.prepareStatement(sql);
        statement.setString(1, organ.toString());
        statement.setString(2, username);

        return statement.executeUpdate();
    }

}
