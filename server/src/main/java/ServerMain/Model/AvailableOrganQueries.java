package ServerMain.Model;

import seng302.Enum.OrganEnum;
import seng302.Model.AvailableOrgan;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class AvailableOrganQueries {

    public AvailableOrganQueries() {
        // Default Constructor
    }

    /**
     * Gets all of the organs which are available for a transplant. The donor must be dead in order for an organ to be
     * available for transplantation.
     *
     * @param hospitalId The id of the hospital that we want to get the organs from.
     * @param connection The database connection object.
     * @return All of the available organs. Note an organ may be returned even though it is expired.
     * Check for this before you use organs from here.
     * @throws SQLException If there was an error in the SQL query.
     */
    public List<AvailableOrgan> getAvailableOrgansFromHospital(int hospitalId, Connection connection) throws SQLException {
        List<AvailableOrgan> organs = new ArrayList<>();
        final String QUERY =
                "SELECT `DonatingOrgans`.`organ`, `DonatingOrgans`.`username`, `Profile`.`dateOfDeath`, `Profile`.`timeOfDeath` " +
                        "FROM DonatingOrgans JOIN Profile on `Profile`.`username` = `DonatingOrgans`.`username` " +
                        "WHERE `Profile`.`isDonor` = true AND CONCAT(`Profile`.`dateOfDeath`, \" \", `Profile`.`timeOfDeath`)" +
                        " < NOW() AND`Profile`.`hospitalId` = ?";
        PreparedStatement statement = connection.prepareStatement(QUERY);
        statement.setInt(1, hospitalId);

        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            OrganEnum organ = OrganEnum.getOrgan(resultSet.getString("organ"));
            String username = resultSet.getString("username");
            LocalDate dateOfDeath = resultSet.getDate("dateOfDeath").toLocalDate();
            Time timeOfDeath = resultSet.getTime("timeOfDeath");
            organs.add(new AvailableOrgan(username, dateOfDeath, timeOfDeath, organ));
        }

        return organs;
    }

}
