package seng302.CustomException;

/**
 * Custom exception thrown when we have a case of duplicates.
 */
public class AlreadyExists extends Exception {
    public AlreadyExists(String message) {
        super(message);
    }
}
