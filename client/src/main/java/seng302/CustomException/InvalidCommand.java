package seng302.CustomException;

/**
 * Custom Exception that is thrown when the user enters an invalid command.
 */
public class InvalidCommand extends Exception {

    public InvalidCommand(String message) {
        super(message);
    }
}
