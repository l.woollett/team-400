package seng302.CustomException;

/**
 * Custom exception that is thrown when there is invalid syntax.
 */
public class InvalidSyntax extends Exception {
    public InvalidSyntax(String message) {
        super(message);
    }
}
