package seng302.CustomException;

/**
 * Custom exception thrown when the desired item does not exist.
 * <p>
 * Intended to ensure the programmer deals with the possibility of the element not existing; rather than the programmer
 * forgetting to check for a returned null value. This would be done by having a method throw the exception.
 */
public class DoesNotExist extends Exception {
    public DoesNotExist(String message) {
        super(message);
    }
}
