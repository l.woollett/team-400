package seng302.CustomException;

/**
 * EmptyList Exception which is thrown when a list is empty, but should not be. Used for server endpoints for adding
 * clinicians to a conversation.
 */
public class EmptyList extends Exception {

    public EmptyList(String message) {
        super(message);
    }

}
