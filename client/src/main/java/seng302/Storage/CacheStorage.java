package seng302.Storage;

import seng302.Model.CachedItem;

import java.util.ArrayList;

public class CacheStorage {
    ArrayList<CachedItem> cachedItems;

    public CacheStorage() {
        cachedItems = new ArrayList<>();
    }

    public CacheStorage(ArrayList<CachedItem> cache) {
        cachedItems = cache;
    }

    public ArrayList<CachedItem> getCache() {
        return this.cachedItems;
    }

    public void setCache(ArrayList<CachedItem> toSet) {
        cachedItems = toSet;
    }
}
