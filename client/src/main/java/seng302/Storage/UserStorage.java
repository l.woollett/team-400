package seng302.Storage;

import seng302.Model.Admin;
import seng302.Model.Clinician;
import seng302.Model.Profile;

import java.util.ArrayList;

/**
 * Used to save and load profiles and clinicians from persistent storage to and from a file.
 */
public class UserStorage {

    private ArrayList<Profile> profiles;

    private ArrayList<Clinician> clinicians;

    private ArrayList<Admin> admins;

    public UserStorage(ArrayList<Profile> profiles, ArrayList<Clinician> clinicians, ArrayList<Admin> admins) {
        this.profiles = profiles;
        this.clinicians = clinicians;
        this.admins = admins;
    }

    public ArrayList<Profile> getProfiles() {
        return profiles;
    }

    public void setProfiles(ArrayList<Profile> profiles) {
        this.profiles = profiles;
    }

    public ArrayList<Clinician> getClinicians() {
        return clinicians;
    }

    public void setClinicians(ArrayList<Clinician> clinicians) {
        this.clinicians = clinicians;
    }

    public ArrayList<Admin> getAdmins() {
        return admins;
    }

    public void setAdmins(ArrayList<Admin> admins) {
        this.admins = admins;
    }
}
