package seng302.Main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import seng302.ModelController.CacheController;
import seng302.Utilities.DataInteraction;

/**
 * First method called when starting the GUI. Create the stage and sets the default view of the map with raw data points
 * and set that as the scene.
 */
public class AppGui extends Application {

    /**
     * Allow the GUI to be started from else where in the application.
     */
    public static void runGui() {
        launch();
    }

    /**
     * Perform necessary initialisations.
     * <p>
     * Load all the profiles and clinicians from persistent storage. - Add the default clinician if they don't exist.
     */
    private void initialise() {
        CacheController.setCache(DataInteraction.loadCache());
    }

    /**
     * Initialize the GUI.
     *
     * @param primaryStage Primary stage of the GUI
     * @throws Exception Throws exception when the fxml does not exists.
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        initialise();

        FXMLLoader loginLoader = new FXMLLoader();
        loginLoader.setLocation(getClass().getResource("login.fxml"));
        Parent loginRoot = loginLoader.load();
        Scene loginScene = new Scene(loginRoot);
        primaryStage.setTitle("Login");
        primaryStage.setScene(loginScene);
        primaryStage.setResizable(true);
        primaryStage.getIcons().add(new Image("https://i.imgur.com/9elFQGf.png"));
        primaryStage.show();
        primaryStage.setOnCloseRequest(t -> {

            System.exit(0); // This avoid memory leaks?????? If the CLI is run only then
            // when the quit command is executed JRE crashes.

            // Also used to close all the stages if there are multiple open to actually close the program.
        });
    }
}