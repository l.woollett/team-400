package seng302.Main;

import seng302.ModelController.CommandController;

import java.util.Date;

/**
 * Serves as the entry point to the Organ Donation Management System.
 */
public class Main {
    /**
     * Main method for the application.
     *
     * @param args Arguments provided when the application is run.
     */
    public static void main(String[] args) {
        CommandController.setUsingJavaFxTerminal(true); // Legacy issue, still have old code that uses
        // JLine and the bash terminal. Use this flag to avoid using code that uses JLine
        AppGui.runGui();
    }
}
                                                                                                                                     