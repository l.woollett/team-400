package seng302.Utilities;

import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Control;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import seng302.Enum.AvailableOrganRequestStatus;

import java.time.LocalDate;
import java.util.logging.Logger;

import static seng302.Utilities.Tooltips.createNewTooltip;

/**
 * Abstract class applies CSS styles and tooltips to JavaFX Nodes based on the provided conditions.
 */
public abstract class Style {

    // Load css files into local variables for easier use in class
    private static final String VALID_CSS_STYLE =
            Style.class.getResource("/seng302/Stylesheets/valid.css").toExternalForm();
    private static final String INVALID_CSS_STYLE =
            Style.class.getResource("/seng302/Stylesheets/invalid.css").toExternalForm();


    public final static String CLEAR_STATUS_CSS = "-fx-background-color: none; -fx-text-fill: none";
    private final static String LIGHT_GREEN_CSS = "-fx-background-color: #66BB6A; -fx-text-fill:#000000 ";
    private final static String LIGHT_YELLOW_CSS = "-fx-background-color: #FFEE58; -fx-text-fill:#000000 ";
    private final static String LIGHT_RED_CSS = "-fx-background-color: #EF5350; -fx-text-fill:#000000 ";
    private final static String BLACK_CSS = "-fx-background-color: #000000; -fx-text-fill:#FFFFFF";

    private static final Logger LOGGER = Logger.getLogger(Style.class.getName());


    /**
     * Will apply a CSS style sheet and tool tip to a DatePicker.
     *
     * @param datePicker  The DatePicker the style will be applied to.
     * @param cssStyle    The CSS style to be applied.
     * @param toolTipText The tool tip text that will give information about the current state of the DatePicker.
     */
    private static void applyStyle(DatePicker datePicker, String cssStyle, String toolTipText) {
        datePicker.getStylesheets().clear();
        datePicker.setTooltip(createNewTooltip(toolTipText));
        datePicker.getStylesheets().add(cssStyle);
    }

    /**
     * Will apply a CSS style sheet and tool tip to a TextField.
     *
     * @param textField   The TextField the style will be applied to.
     * @param cssStyle    The CSS style to be applied.
     * @param toolTipText The tool tip text that will give information about the current state of the DatePicker.
     */
    private static void applyStyle(TextField textField, String cssStyle, String toolTipText) {
        textField.getStylesheets().clear();
        textField.setTooltip(createNewTooltip(toolTipText));
        textField.getStylesheets().add(cssStyle);
    }

    /**
     * Will apply a CSS style sheet to a ChoiceBox.
     *
     * @param choiceBox The TextField the style will be applied to.
     * @param cssStyle  The CSS style to be applied.
     */
    private static void applyStyle(ChoiceBox choiceBox, String cssStyle) {
        choiceBox.getStylesheets().clear();
        choiceBox.getStylesheets().add(cssStyle);
    }

    /**
     * Applies just the invalid/valid style to the node.
     *
     * @param <N>     Object of type Control.
     * @param node    The control node the style will be applied to.
     * @param isValid If true the valid CSS style will be applied. If false the invalid CSS style will be applied.
     */
    public static <N extends Control> void applyStyle(N node, boolean isValid) {
        node.getStylesheets().clear();
        if (isValid) {
            node.getStylesheets().add(VALID_CSS_STYLE);
        } else {
            node.getStylesheets().add(INVALID_CSS_STYLE);
        }
    }

    /**
     * Generic method used to apply a CSS style and tooltip to a DatePicker node based on if the value it's currently
     * set to is valid or not. Will also take into account if the node represents an optional field. If it's optional
     * then if it holds the default value then it will clear all styles.
     *
     * @param datePicker         The date picker the style and tooltip will be applied to.
     * @param isValid            If the current value is valid or not.
     * @param isOptional         If the field the date picker represents is optional or mandatory.
     * @param validToolTipText   The tooltip that will be applied if the date is valid.
     * @param invalidToolTipText The tooltip that will be applied if the date is invalid.
     * @param currentValue       The current value of the date picker.
     */
    public static void applyDatePickerStyle(DatePicker datePicker, boolean isValid, boolean isOptional,
                                            String validToolTipText, String invalidToolTipText,
                                            LocalDate currentValue) {
        final LocalDate DEFAULT_VALUE = null; // If the DatePicker is set to the default value and is optional, then
        // there shouldn't be a style applied at all.

        if (!isValid) {
            applyStyle(datePicker, INVALID_CSS_STYLE, invalidToolTipText);
        } else if (currentValue == DEFAULT_VALUE && isOptional) {
            datePicker.getStylesheets().clear();
        } else {
            applyStyle(datePicker, VALID_CSS_STYLE, validToolTipText);
        }
    }

    /**
     * Generic method used to apply a CSS style and tooltip to a TextField node based on if the value it's currently set
     * to is valid or not. Will also take into account if the node represents an optional field. If it's optional then
     * if it holds the default value then it will clear all styles.
     *
     * @param textField          The date picker the style and tooltip will be applied to.
     * @param isValid            If the current value is valid or not.
     * @param isOptional         If the field the TextField represents is optional or mandatory.
     * @param validToolTipText   The tooltip that will be applied if the date is valid.
     * @param invalidToolTipText The tooltip that will be applied if the date is invalid.
     * @param currentValue       The current value of the date picker.
     */
    public static void applyTextFieldStyle(TextField textField, boolean isValid, boolean isOptional,
                                           String validToolTipText, String invalidToolTipText, String currentValue) {
        final String DEFAULT_VALUE = ""; // If the TextField is set to the default value and is optional, then
        // there shouldn't be a style applied at all.

        if (!isValid) {
            applyStyle(textField, INVALID_CSS_STYLE, invalidToolTipText);
        } else if (currentValue.equals(DEFAULT_VALUE) && isOptional) {
            textField.getStylesheets().clear();
        } else {
            applyStyle(textField, VALID_CSS_STYLE, validToolTipText);
        }
    }

    /**
     * Generic method used to apply a CSS style and tooltip to a ChoiceBox node based on if the value it's currently set
     * to is valid or not. Will also take into account if the node represents an optional field. If it's optional then
     * if it holds the default value then it will clear all styles.
     *
     * @param choiceBox    The date picker the style and tooltip will be applied to.
     * @param isValid      If the current value is valid or not.
     * @param isOptional   If the field the date picker represents is optional or mandatory.
     * @param currentValue The current value of the date picker.
     */
    public static void applyChoiceBoxStyle(ChoiceBox choiceBox, boolean isValid, boolean isOptional,
                                           Object currentValue) {
        final Object DEFAULT_VALUE = null; // If the DatePicker is set to the default value and is optional, then
        // there shouldn't be a style applied at all.

        if (!isValid) {
            applyStyle(choiceBox, INVALID_CSS_STYLE);
        } else if ((currentValue == DEFAULT_VALUE) && isOptional) {
            choiceBox.getStylesheets().clear();
        } else {
            applyStyle(choiceBox, VALID_CSS_STYLE);
        }
    }

    /**
     * Retrieve a CSS style based on the notification status.
     *
     * @param status The status for the notification.
     * @return The CSS style as a String.
     */
    public static String getStatusStyle(AvailableOrganRequestStatus status) {

        if(status == null) {
            return CLEAR_STATUS_CSS;
        }

        if (status == AvailableOrganRequestStatus.UNREAD || status == AvailableOrganRequestStatus.READ || status == AvailableOrganRequestStatus.IN_PROGRESS) {
            return LIGHT_YELLOW_CSS;
        } else if (status == AvailableOrganRequestStatus.INTERESTED || status == AvailableOrganRequestStatus.TRANSFERRED) {
            return LIGHT_GREEN_CSS;
        } else if (status == AvailableOrganRequestStatus.UNINTERESTED || status == AvailableOrganRequestStatus.DECLINED ) {
            return LIGHT_RED_CSS;
        } else if (status == AvailableOrganRequestStatus.EXPIRED || status == AvailableOrganRequestStatus.WITHDRAWN){
            return BLACK_CSS;
        } else {
            LOGGER.severe("Does not have an associated style: " + status.toString());
            return "";
        }
    }

}
