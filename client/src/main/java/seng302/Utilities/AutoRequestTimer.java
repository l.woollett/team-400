package seng302.Utilities;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * A utility class containing useful functions for creating and cancelling timers.
 */
public class AutoRequestTimer {

    /**
     * The default time delay in milliseconds before a task is executed by a timer.
     */
    private static final int DEFAULT_TIMER_DELAY = 5000;
    /**
     * The default time period in milliseconds between successive task executions by a timer.
     */
    private static final int DEFAULT_TIMER_PERIOD = 5000;
    /**
     * Acts as a flag value for when default parameters are wanting to be used.
     */
    private static final int USE_DEFAULT_ARGUMENT_VALUE = -1;
    private static AutoRequestTimer autoRequestTimer;

    //Our threadpool, using as many threads as the computer will give us.
    ScheduledExecutorService executor = Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors());



    private AutoRequestTimer() {
        // Singleton
    }

    /**
     * Singleton constructor
     *
     * @return AvailableOrganRequests either created or acquired.
     */
    public static AutoRequestTimer getInstance() {
        if (autoRequestTimer == null) {
            autoRequestTimer = new AutoRequestTimer();
        }
        return autoRequestTimer;
    }

    /**
     * Creates a scheduled thread call to a function.
     * The period and delay can be given as a positive integer, or a -1 can be passed in signifying that the default
     * DEFAULT_TIMER_DELAY or DEFAULT_TIMER_PERIOD respectively should be used.
     *
     * @param givenDelay      A positive integer representing the delay in milliseconds before the handlerFunction will be
     *                        called by the timer. If it is -1 then DEFAULT_TIMER_DELAY will be used as the delay.
     * @param givenInterval   A positive integer representing the time in milliseconds before the handlerFunction will be
     *                        called by the timer between successive calls. If it is -1 then DEFAULT_TIMER_DELAY will be
     *                        used as the delay.
     * @param handlerFunction The function that will be called by the thread executor. Note: The function must have a return type
     *                        of Boolean to be accepted.
     */
    public void createAutoRequestTimer(int givenDelay, int givenInterval, Runnable handlerFunction) {
        if (executor.isShutdown()) {
            executor = Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors());
        }

        int delay = givenDelay;
        int interval = givenInterval;

        if (delay == USE_DEFAULT_ARGUMENT_VALUE) {
            delay = DEFAULT_TIMER_DELAY;
        }

        if (interval == USE_DEFAULT_ARGUMENT_VALUE) {
            interval = DEFAULT_TIMER_PERIOD;
        }

        executor.scheduleWithFixedDelay(handlerFunction, delay, interval, TimeUnit.MILLISECONDS);

    }

    /**
     * Shutdown the timer when logging out / critical error
     */
    public void stopTimers() {
        executor.shutdown();
    }
}
