package seng302.Utilities;

import javafx.stage.Stage;
import seng302.CustomException.DoesNotExist;
import seng302.GUI.Admin.AdminSceneController;
import seng302.GUI.ControllerAccess;
import seng302.GUI.MainController;

import java.util.*;

/**
 * This class helps bring out a profile in different window. The class is an utility class.
 */
public final class ProfileDisplayUtility {

    //Double click will open up a profile
    public static final int DOUBLE_CLICK_COUNT = 2;
    /**
     * Used to keep track of what stages have been created for a profile. When you search a profile you can open up a
     * new stage to view that profile's details.
     */
    private static Map<String, Stage> openedProfileStagesHashMap = new HashMap<>();
    private static Map<String, MainController> openedProfileControllers = new HashMap<>();
    private static Map<String, Stage> openedAdminStages = new HashMap<>();
    private static Map<String, AdminSceneController> openedAdminControllers = new HashMap<>();
    /**
     * When you search a profile you can open up a new stage to view that profile's details. There is an issue where if
     * an AlertDialog is shown, after clicking 'Ok' the order of how the stages layer on top of each other will follow
     * the order how the stages were created. To fix this issue, the index of each stage will represent the order of how
     * each stage should be layered. For n stages, the 0th index will represent the bottom layer and the n-1th index
     * will represent the top layer. When the alert dialogue is clicked, the application will iterate through the stages
     * starting at the 'bottom' and call Stage.toFront() to give the proper layering.
     */
    private static ArrayList<Stage> openedProfileStagesArray = new ArrayList<>();

    /**
     * Don't let anyone instantiate this class.
     */
    private ProfileDisplayUtility() {
    }

    /**
     * Return the openedProfileControllers related to the opened stages.
     *
     * @return Unmodifiable collection of openedProfileControllers.
     */
    public static Collection<MainController> getOpenedProfileControllers() {
        Collection<MainController> stageControllers = openedProfileControllers.values();
        return Collections.unmodifiableCollection(stageControllers);
    }

    /**
     * Check if a stage has already been created and shown for the profile.
     *
     * @param username The username of the profile.
     * @return True if the stage is created, otherwise false.
     */
    public static boolean isStageCreated(String username) {
        Stage openedStage = openedProfileStagesHashMap.get(username);
        return openedStage != null;
    }

    /**
     * Will keep track of what stages have been created for a profile.
     *
     * @param username     The profile's username.
     * @param profileStage The stage that will display the profile's details.
     * @param controller   The main controller associated with the stage/loaded FXML.
     * @param <T>          The controller extends MainController so we are guaranteed the controller will know how to
     *                     update its own title.
     */
    public static <T extends MainController> void registerProfileStage(String username, Stage profileStage,
                                                                       T controller) {
        openedProfileStagesHashMap.put(username, profileStage);
        openedProfileStagesArray.add(profileStage); // Add to the end, so its index will represent the top
        openedProfileControllers.put(username, controller);
    }

    /**
     * The stage has been closed, we no longer keep track of it. If we have no record of a stage it means the stage
     * isn't created.
     *
     * @param username The profile's username. The unique identifier for the stage.
     */
    private static void unregisterProfileStage(String username) {
        openedProfileStagesArray.remove(openedProfileStagesHashMap.get(username));
        openedProfileStagesHashMap.remove(username);
        openedProfileControllers.remove(username);
    }

    /**
     * Retrieve the stage that displays the profile's information.
     *
     * @param username The profile's username. Unique identifier for the stage.
     * @return The stage that display's the profile's details if it exists.
     * @throws DoesNotExist The stage isn't created for the username.
     */
    public static Stage retrieveProfileStage(String username) throws DoesNotExist {
        Stage profileStage = openedProfileStagesHashMap.get(username);
        if (profileStage == null) {
            throw new DoesNotExist("The stage hasn't been created for the profile yet.");
        } else {
            return profileStage;
        }
    }

    public static Stage retrieveAdminStage(String username) throws DoesNotExist {
        Stage adminStage = openedAdminStages.get(username);
        if (adminStage == null) {
            throw new DoesNotExist("Stage hasn't been created");
        } else {
            return adminStage;
        }
    }

    public static AdminSceneController retrieveAdminController(String username) throws DoesNotExist {
        return openedAdminControllers.get(username);
    }

    /**
     * Utility method to set the stage to the related profile information
     *
     * @param <T>          Custom controller object
     * @param username     Given username to register or unregister for a user's stage
     * @param profileStage Given profile stage to keep in track and to display
     * @param controller   controller object of type CustomController
     */
    public static <T extends MainController> void displayProfileStage(String username, Stage profileStage, T controller) {
        registerProfileStage(username, profileStage, controller);

        profileStage.setOnCloseRequest(event -> {
            unregisterProfileStage(username);
        });

        profileStage.focusedProperty().addListener((observable, prevFocusState, newFocusState) -> {
            if (newFocusState) { // boolean is true, we are focusing on the stage

                // The stage is focused on, must mean its position is on the front. This must be reflected in the
                // openedProfileStagesArray
                openedProfileStagesArray.remove(profileStage); // Remove from it's old index/pos
                openedProfileStagesArray.add(profileStage); // Move to the end, i.e. front
            }

        });

        // When we open a profile from a clinician we do not want to see the logout button as the donor is not
        // actually logged in, it is the clinician logged in.
        ControllerAccess.getProfileSceneController().hideLogoutButton();
        ControllerAccess.getMedicalHistoryTabController().enableClinicianFunctions();
    }

    /**
     * Goes through all stages that are open and closes them.
     */
    public static void closeStages() {

        for (Stage stage : openedProfileStagesArray) {
            stage.close();
        }

        clearStageRecords();

    }

    /**
     * Clears all collections used to keep track of the profile stages, and any related information, that have been
     * opened.
     */
    private static void clearStageRecords() {
        openedProfileStagesHashMap.clear();
        openedProfileStagesArray.clear();
        openedProfileControllers.clear();

    }
}
