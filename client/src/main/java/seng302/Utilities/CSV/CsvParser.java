package seng302.Utilities.CSV;

import seng302.CustomException.DoesNotExist;
import seng302.Enum.BloodTypeEnum;
import seng302.Enum.GenderEnum;
import seng302.Enum.RegionEnum;
import seng302.Model.Death;
import seng302.Model.Profile;
import seng302.ModelController.ProfileController;
import seng302.ModelController.UserController;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 * Used to parse a CSV file containing records of profiles. Will extract the only the most relevant information.
 */
public class CsvParser {

    private final Logger LOGGER = Logger.getLogger(getClass().getName());
    private final String NO_ENTRY = "";
    private final String USERNAME_HEADER = "nhi";
    private final String FIRST_NAME_HEADER = "first_names";
    private final String LAST_NAME_HEADER = "last_names";
    private final String DOB_HEADER = "date_of_birth";
    private final String DOD_HEADER = "date_of_death";
    private final String BIRTH_GENDER_HEADER = "birth_gender";
    private final String PREFERRED_GENDER_HEADER = "gender";
    private final String BLOOD_TYPE_HEADER = "blood_type";
    private final String HEIGHT_HEADER = "height";
    private final String WEIGHT_HEADER = "weight";
    private final String STREET_NUMBER_HEADER = "street_number";
    private final String STREET_NAME_HEADER = "street_name";
    private final String CITY_HEADER = "city";
    private final String REGION_HEADER = "region";
    private File file;
    private BufferedReader reader;
    private String line;
    private String csvSplitBy;
    private List<Profile> profiles = new ArrayList<>();

    public CsvParser(File selectedFile) {
        this.file = selectedFile;
        this.reader = null;
        this.line = "";
        this.csvSplitBy = ",";
    }

    /**
     * The dates in the CSV follow the format MM/DD/YYYY, this method is to handle this type of format.
     *
     * @param dateData The value from the CSV relating to a date.
     * @return The LocalDate instance based on the values from the CSV.
     * @throws NumberFormatException Will possibly be raised by the value in the CSV not following the format MM/DD/YYYY
     * @throws DateTimeException     Possibly the format was not MM/DD/YYYY or the invalid values for the month, day or year.
     */
    public LocalDate extractDate(String dateData) throws NumberFormatException, DateTimeException {
        String[] dateComponents = dateData.split("/");
        try {
            int month = Integer.parseInt(dateComponents[0]);
            int day = Integer.parseInt(dateComponents[1]);
            int year = Integer.parseInt(dateComponents[2]);

            return LocalDate.of(year, month, day);
        } catch (NumberFormatException e) {
            LOGGER.warning("The date format in the CSV did not follow the format mm/dd/yyyy");
            throw e;
        } catch (DateTimeException e) {
            LOGGER.warning("The value for the date was not valid.");
            throw e;
        }
    }

    /**
     * Will parse through the CSV and construct the profiles based on the read data.
     *
     * @return List containing the profiles from the CSV.
     */
    public List<Profile> parse() {

        try {
            reader = new BufferedReader(new FileReader(file));

            String headers = reader.readLine();

            // The file is empty
            if (headers.equals("")) {
                LOGGER.warning("The file is empty.");
                return profiles;
            }

            List<String> headerList = Arrays.asList(headers.split(csvSplitBy));

            int usernameIndex = headerList.indexOf(USERNAME_HEADER);
            int firstNameIndex = headerList.indexOf(FIRST_NAME_HEADER);
            int lastNameIndex = headerList.indexOf(LAST_NAME_HEADER);
            int dobIndex = headerList.indexOf(DOB_HEADER);
            int birthGenderIndex = headerList.indexOf(BIRTH_GENDER_HEADER);
            int preferredBirthGenderIndex = headerList.indexOf(PREFERRED_GENDER_HEADER);
            int bloodTypeIndex = headerList.indexOf(BLOOD_TYPE_HEADER);
            int heightIndex = headerList.indexOf(HEIGHT_HEADER);
            int weightIndex = headerList.indexOf(WEIGHT_HEADER);
            int streetNumberIndex = headerList.indexOf(STREET_NUMBER_HEADER);
            int streetNameIndex = headerList.indexOf(STREET_NAME_HEADER);
            int cityIndex = headerList.indexOf(CITY_HEADER);
            int regionIndex = headerList.indexOf(REGION_HEADER);


            while ((line = reader.readLine()) != null) {
                String[] data = line.split(csvSplitBy);

                String username = data[usernameIndex];
                GenderEnum birthGender;
                GenderEnum preferredGender;
                BloodTypeEnum bloodTypeEnum = ProfileController.BLOOD_TYPE_NOT_SET;
                RegionEnum region = RegionEnum.NOT_SET;
                LocalDate dateOfBirth = null;
                Death death = ProfileController.DEATH_NOT_SET;
                String address = UserController.ADDRESS_NOT_SET;
                float height = ProfileController.HEIGHT_NOT_SET;
                float weight = ProfileController.WEIGHT_NOT_SET;

                try {
                    birthGender = convertBirthGender(data, birthGenderIndex);
                } catch (DoesNotExist doesNotExist) {
                    LOGGER.warning(doesNotExist.getMessage());
                    continue;
                }

                try {
                    preferredGender = convertPreferredGender(data, preferredBirthGenderIndex);
                } catch (DoesNotExist doesNotExist) {
                    LOGGER.warning(doesNotExist.getMessage());
                    continue;
                }

                try {
                    bloodTypeEnum = convertBloodType(data, bloodTypeIndex);
                } catch (DoesNotExist doesNotExist) {
                    LOGGER.severe(doesNotExist.getMessage());
                    continue;
                }

                try {
                    region = convertRegion(data, regionIndex);
                } catch (DoesNotExist doesNotExist) {
                    LOGGER.warning(doesNotExist.getMessage());
                }

                String streetNumberComponent = data[streetNumberIndex];
                String streetNameComponent = data[streetNameIndex];
                String cityComponent = data[cityIndex];

                if (streetNameComponent.equalsIgnoreCase(NO_ENTRY) ||
                        streetNumberComponent.equalsIgnoreCase(NO_ENTRY) ||
                        cityComponent.equalsIgnoreCase(NO_ENTRY)) {
                    LOGGER.warning("The address was invalid, this entry will be skipped");
                    continue;
                } else {
                    address = data[streetNumberIndex] + " " + data[streetNameIndex] + " Street, " + data[cityIndex];
                }

                String heightValue = data[heightIndex];
                if (!heightValue.equalsIgnoreCase(NO_ENTRY)) {
                    height = Float.parseFloat(data[heightIndex]) / 100;
                }

                String weightValue = data[weightIndex];
                if (!weightValue.equalsIgnoreCase(NO_ENTRY)) {
                    weight = Float.parseFloat(data[weightIndex]);
                }

                try {
                    String dateOfBirthValue = data[dobIndex];
                    dateOfBirth = extractDate(dateOfBirthValue);
                } catch (NumberFormatException | DateTimeException e) {
                    LOGGER.warning("Invalid date of birth, the value will be ignored.");
                    continue;
                }


                profiles.add(new Profile(data[firstNameIndex], UserController.MIDDLE_NAME_NOT_SET,
                        data[lastNameIndex], dateOfBirth, death, preferredGender, birthGender, height, weight,
                        bloodTypeEnum, address, region, new Date(), new Date(), false,
                        ProfileController.ORGAN_NOT_SET, username, UserController.ALIAS_NOT_SET,
                        false, ProfileController.ORGAN_NOT_SET, ProfileController.BLOOD_PRESSURE_NOT_SET,
                        ProfileController.ALCOHOL_CONSUMPTION_NOT_SET, ProfileController.IS_SMOKER_NOT_SET));

            }
        } catch (IOException e) {
            LOGGER.severe(e.getMessage());
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    LOGGER.severe(e.getMessage());
                }
            }
        }

        return profiles;
    }

    /**
     * Converts the string representation of a birth gender to one of the pre-defined gender enums.
     * If the gender does not match one of the pre-defined enums then a DoesNotExist exception is thrown.
     *
     * @param data             the String array we are trying to parse (the line).
     * @param birthGenderIndex the index in the String array where the birth gender is located.
     * @return the gender enum which corresponds to the String representation of the enum we are trying to convert.
     * @throws DoesNotExist if the String representation does not match any enum.
     */
    private GenderEnum convertBirthGender(String[] data, int birthGenderIndex) throws DoesNotExist {
        switch (data[birthGenderIndex].toLowerCase()) {
            case "male":
                return GenderEnum.MALE;
            case "female":
                return GenderEnum.FEMALE;
            default:
                throw new DoesNotExist("The birth gender was not recognized, it must either be male or female. This entry will be ignored.");
        }
    }

    /**
     * Converts the string representation of a preferred gender to one of the pre-defined birth gender enums.
     * If the gender does not match one of the pre-defined enums then a DoesNotExist exception is thrown.
     *
     * @param data                      the String array we are trying to parse (the line).
     * @param preferredBirthGenderIndex the index in the String array where the preferred gender is located.
     * @return the gender enum which corresponds to the String representation of the enum we are trying to convert.
     * @throws DoesNotExist if the String representation does not match any enum.
     */
    private GenderEnum convertPreferredGender(String[] data, int preferredBirthGenderIndex) throws DoesNotExist {
        switch (data[preferredBirthGenderIndex].toLowerCase()) {
            case "male":
                return GenderEnum.MALE;
            case "female":
                return GenderEnum.FEMALE;
            case "other":
                return GenderEnum.OTHER;
            case NO_ENTRY:
                return UserController.GENDER_NOT_SET;
            default:
                throw new DoesNotExist("The preferred gender was not recognized, this entry will be ignored.");

        }
    }

    /**
     * Converts the string representation of a blood type to one of the pre-defined blood type enums.
     * If the bloodtype does not match one of the pre-defined enums then a DoesNotExist exception is thrown.
     *
     * @param data           the String array we are trying to parse (the line).
     * @param bloodTypeIndex the index in the array where the blood type is located.
     * @return the blood type enum which corresponds to the String representation of the enum we are trying to convert.
     * @throws DoesNotExist if the String representation does not match any enum.
     */
    private BloodTypeEnum convertBloodType(String[] data, int bloodTypeIndex) throws DoesNotExist {
        switch (data[bloodTypeIndex].toUpperCase()) {
            case "B+":
                return BloodTypeEnum.B_POSITIVE;
            case "B-":
                return BloodTypeEnum.B_NEGATIVE;
            case "AB-":
                return BloodTypeEnum.AB_NEGATIVE;
            case "AB+":
                return BloodTypeEnum.AB_POSITIVE;
            case "A+":
                return BloodTypeEnum.A_POSITIVE;
            case "A-":
                return BloodTypeEnum.A_NEGATIVE;
            case "O+":
                return BloodTypeEnum.O_POSITIVE;
            case "O-":
                return BloodTypeEnum.O_NEGATIVE;
            case NO_ENTRY:
                return ProfileController.BLOOD_TYPE_NOT_SET;
            default:
                throw new DoesNotExist("Blood type is not recognized and they will be ignored.");
        }
    }

    /**
     * Converts a String representation of a region to one of the pre-defined region enums.
     * If the string representation does not match any of the region enums then a DoesNotExist exception is thrown.
     *
     * @param data        the String array we are trying to parse (the line).
     * @param regionIndex the index in the array where the string representation of the region is located.
     * @return the region enum which corresponds to the String representation of the enum we are trying to convert,
     * @throws DoesNotExist if the given region does not match any of the pre-defined region enums.
     */
    private RegionEnum convertRegion(String[] data, int regionIndex) throws DoesNotExist {
        switch (data[regionIndex].toLowerCase()) {
            case "auckland":
                return RegionEnum.AUCKLAND;
            case "bay of plenty":
                return RegionEnum.BAY_OF_PLENTY;
            case "canterbury":
                return RegionEnum.CANTERBURY;
            case "otago":
                return RegionEnum.OTAGO;
            case "nelson":
                return RegionEnum.NELSON;
            case "tasman":
                return RegionEnum.TASMAN;
            case "waikato":
                return RegionEnum.WAIKATO;
            case "taranaki":
                return RegionEnum.TARANAKI;
            case "gisborne":
                return RegionEnum.GISBOURNE;
            case "hawke's bay":
                return RegionEnum.HAWKES_BAY;
            case "northland":
                return RegionEnum.NORTHLAND;
            case "southland":
                return RegionEnum.SOUTHLAND;
            case "west coast":
                return RegionEnum.WEST_COAST;
            case "marlborough":
                return RegionEnum.MARLBOROUGH;
            case "wellington":
                return RegionEnum.WELLINGTION;
            case "manawatu-wanganui":
                return RegionEnum.MANAWATU_WANGANUI;
            default:
                throw new DoesNotExist("The region was not recognized, this entry will be ignored.");
        }
    }

}
