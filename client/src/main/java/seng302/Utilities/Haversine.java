package seng302.Utilities;

/**
 * Will calculate the Haversine distance, it is the distance between two GPS coordinates on the globe which takes
 * into account the curvature of the globe.
 */
public class Haversine {
    /**
     * Calculate the shortest distance between two points on the Earth's surface. Takes into account the curvature
     * of the earth.
     *
     * @param lat1 Latitude of the first coordinate.
     * @param lon1 Longitude of the first coordinate.
     * @param lat2 Latitude of the second coordinate.
     * @param lon2 Longitude of the second coodinate.
     * @return The distance between the two points in km.
     */
    public double calcDistance(double lat1, double lon1, double lat2, double lon2) {
        final double EARTH_RADIUS_KM = 6371;
        double latDiff = convertDegToRad(lat2 - lat1);
        double lonDiff = convertDegToRad(lon2 - lon1);
        double a = Math.sin(latDiff / 2) * Math.sin(latDiff / 2) +
                Math.cos(convertDegToRad(lat1)) * Math.cos(convertDegToRad(lat2)) *
                        Math.sin(lonDiff / 2) * Math.sin(lonDiff / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return EARTH_RADIUS_KM * c;
    }

    /**
     * Function to convert degrees into radians.
     *
     * @param deg Angle in degrees.
     * @return Same value in radians.
     */
    private double convertDegToRad(double deg) {
        return deg * (Math.PI / 180.0);
    }
}