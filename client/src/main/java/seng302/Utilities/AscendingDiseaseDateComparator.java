package seng302.Utilities;

import seng302.Model.Disease;

import java.util.Comparator;

public class AscendingDiseaseDateComparator implements Comparator<Disease> {
    @Override
    public int compare(Disease o1, Disease o2) {
        return o1.getDateOfDiagnosis().compareTo(o2.getDateOfDiagnosis());
    }
}
