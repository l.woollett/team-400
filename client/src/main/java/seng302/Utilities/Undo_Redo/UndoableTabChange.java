package seng302.Utilities.Undo_Redo;

import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;

/**
 * Class to create a wrapper object that changes a TabPane selection to be it's previously selected value. Class
 * implements the Command interface.
 */
public class UndoableTabChange implements Command {
    private SingleSelectionModel<Tab> selectionModel;
    private Tab previousTab;
    private Tab nextTab;

    /**
     * Constructs the UndoableTabChange wrapper object and initialise each of it's attribute to be the values passed
     * in.
     *
     * @param singleSelectionModel The SingleSelectionModel of the TabPane being made undo-able.
     * @param tab                  The tab that was previously selected on the TabPane relating to
     *                             SingleSelectionModel.
     */
    public UndoableTabChange(SingleSelectionModel<Tab> singleSelectionModel, Tab tab) {
        this.selectionModel = singleSelectionModel;
        this.previousTab = tab;
        this.nextTab = singleSelectionModel.getSelectedItem();
    }

    /**
     * Changes the currently selected tab to be the tab given (which should be the last selected tab if used
     * correctly).
     */
    @Override
    public void execute() {
        selectionModel.select(previousTab);
    }

    /**
     * Reverts the currently selected tab to be the tab that was selected before execute() was called.
     */
    @Override
    public void revert() {
        selectionModel.select(nextTab);
    }

    public Tab getOldTab() {
        return this.nextTab;
    }
}
