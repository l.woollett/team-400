package seng302.Utilities.Undo_Redo;

import javafx.scene.control.TableView;
import seng302.Enum.OrganEnum;
import seng302.Model.ReceiverOrgan;

import java.util.ArrayList;
import java.util.List;

public class UndoableOrganCheckTableView implements Command {
/*

    private SimpleBooleanProperty guiValue;
    private boolean oldValue;
    private TableView<ReceiverOrgan> reference;


    public UndoableOrganCheckTableView(SimpleBooleanProperty guiValue, TableView<ReceiverOrgan> tableReference) {
        this.guiValue = guiValue;
        this.reference = tableReference;
    }

    */
/*
      Function is called when the UndoableOrganCheckTableView is popped off the undo deque. Stores the current GUI
      checked items in a temp variable. Then the GUI checked elements is set to !guiValue
 *//*

    public void execute() {
        this.guiValue.setValue(!this.guiValue.getValue());
        reference.refresh();
    }


    */
    /**
     * Function is called when the UndoableOrganCheckTableView is popped off the redo deque. The function simply calls
     * the execute function.
     *//*

    public void revert() {
        execute();
        reference.refresh();
    }
*/



    private List<OrganEnum> oldCheckedOrgans;
    private List<OrganEnum> guiCheckedOrgans;
    private TableView<ReceiverOrgan> guiTableReference;

    public UndoableOrganCheckTableView(List<OrganEnum> oldList, TableView<ReceiverOrgan> tableReference) {
        this.oldCheckedOrgans = new ArrayList<>(oldList);
        this.guiCheckedOrgans = oldList;
        this.guiTableReference = tableReference;
    }

    @Override
    public void execute() {
        List<OrganEnum> temp = new ArrayList<>(this.guiCheckedOrgans);
        guiCheckedOrgans.clear();
        guiCheckedOrgans.addAll(oldCheckedOrgans);
        guiTableReference.refresh();
        this.oldCheckedOrgans = temp;
    }

    @Override
    public void revert() {
        execute();
    }


}
