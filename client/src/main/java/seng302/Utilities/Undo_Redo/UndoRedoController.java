package seng302.Utilities.Undo_Redo;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Class handles the undo and redo Deque. Implement it by adding a UndoRedoController to each controller that controls a
 * JavaFX scene. If you want fxml files that are injected to work, pass the main fxml controller's UndoRedoController to
 * the injected controllers so that they have access.
 */
public class UndoRedoController {
    private Deque<Command> undoDeque;
    private Deque<Command> redoDeque;

    public UndoRedoController() {
        this.undoDeque = new ArrayDeque<>();
        this.redoDeque = new ArrayDeque<>();
    }

    /**
     * Function pushes a command implementing object onto the undo Deque. Function also clear the redo deque, this
     * function does not get called for redo events.
     *
     * @param cmd A undo-able object that implements the command interface
     */
    public void pushToUndoDeque(Command cmd) {
        this.undoDeque.push(cmd);
        clearRedoDeque();
    }

    /**
     * Function clears the undo Deque.
     */
    private void clearUndoDeque() {
        this.undoDeque.clear();
    }

    /**
     * Function clears the redo Deque. Should be called the moment a user performs any actions after performing an undo.
     * A.k.a the moment an object is pushed to the undo Deque.
     */
    private void clearRedoDeque() {
        this.redoDeque.clear();
    }

    /**
     * Function clears the undo Deque and redo Deque
     */
    public void reset() {
        clearUndoDeque();
        clearRedoDeque();
    }

    /**
     * Function pops the top object of the undo deque off, calls the execute function of the object and pushes it onto
     * the redo Deque.
     */
    public void undo() {
        if (!this.undoDeque.isEmpty()) {
            Command temp = this.undoDeque.pop();
            temp.execute();
            this.redoDeque.push(temp);
        }
    }

    /**
     * Function pops the top object of the redo Deque off, calls the revert function for the object and pushes it onto
     * the undo Deque.
     */
    public void redo() {
        if (!this.redoDeque.isEmpty()) {
            Command temp = this.redoDeque.pop();
            temp.revert();
            this.undoDeque.push(temp);
        }
    }

    /**
     * Function checks whether the undo deque is empty
     *
     * @return boolean
     */
    public boolean isUndoEmpty() {
        return this.undoDeque.isEmpty();
    }

    /**
     * Function checks whether the redo deque is empty
     *
     * @return boolean
     */
    public boolean isRedoEmpty() {
        return this.redoDeque.isEmpty();
    }

    /**
     * Function returns the number of items that are in the undo deque.
     *
     * @return int number of items in undo deque.
     */
    public int getUndoDequeSize() {
        return this.undoDeque.size();
    }

    /**
     * Function returns the number of items that are in the redo deque.
     *
     * @return int number of items in redo deque.
     */
    public int getRedoDequeSize() {
        return this.redoDeque.size();
    }
}
