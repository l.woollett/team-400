package seng302.Utilities.Undo_Redo;

import java.util.ArrayList;

/**
 * Represents a collection of undoable objects. The objects in this collection are all undone/redone in one undo/redo.
 */
public class UndoableListOfUndoableObjects implements Command {
    /**
     * ArrayList of undoable objects.
     */
    private ArrayList<Command> undoableList;

    /**
     * Creates the UndoableListOfUndoableObjects object.
     *
     * @param list The list of undoable objects that will all be undone/redone when undo/redo is executed.
     */
    public UndoableListOfUndoableObjects(ArrayList<Command> list) {
        undoableList = list;
    }

    /**
     * Executes execute() for all undoable objects in the undoableList unless they are null.
     */
    @Override
    public void execute() {
        for (Command item : undoableList) {
            if (item != null) {
                item.execute();
            }
        }
    }

    /**
     * Executes revert() for all undoable objects in the undoableList unless they are null.
     */
    @Override
    public void revert() {
        for (Command item : undoableList) {
            if (item != null) {
                item.revert();
            }
        }
    }
}
