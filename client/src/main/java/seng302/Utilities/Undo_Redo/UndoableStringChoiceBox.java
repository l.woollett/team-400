package seng302.Utilities.Undo_Redo;

import javafx.scene.control.ChoiceBox;

public class UndoableStringChoiceBox implements Command {

    private String oldValue;
    private ChoiceBox<String> choiceBox;

    public UndoableStringChoiceBox(ChoiceBox<String> stringChoiceBox, String oldValue) {
        this.choiceBox = stringChoiceBox;
        this.oldValue = oldValue;
    }

    /**
     * Function is called when the UndoableStringChoiceBox is popped off the undo deque. The GUI current value is stored
     * in a temp variable before it gets set to the stored oldValue. The oldValue is then overwritten by the temp.
     */
    @Override
    public void execute() {
        String temp = this.choiceBox.getValue();
        this.choiceBox.setValue(this.oldValue);
        this.oldValue = temp;
    }

    /**
     * Function is called when the UndoableStringChoiceBox is popped off the redo deque. It simply calls the execute
     * function.
     */
    @Override
    public void revert() {
        execute();
    }
}
