package seng302.Utilities.Undo_Redo;

import seng302.GUI.Procedures.UserMedicalProceduresTabController;
import seng302.Model.Procedure;

public class UndoableProcedureEdit implements Command {

    private Procedure guiProcedure;
    private Procedure oldProcedure;
    private UserMedicalProceduresTabController controllerReference;

    /**
     * Class will create an undoable Procedure. For undoing an edit, pass two references to the same Procedure. To undo
     * creation, pass null as the oldProcedure. For undoing a delete pass null as the guiProcedure.
     *
     * @param oldProcedure        Procedure
     * @param guiProcedure        Procedure
     * @param controllerReference Controller object for the procedures tab.
     */
    public UndoableProcedureEdit(Procedure oldProcedure, Procedure guiProcedure, UserMedicalProceduresTabController
            controllerReference) {
        if (oldProcedure != null) {
            this.oldProcedure = new Procedure(oldProcedure);
        } else {
            this.oldProcedure = null;
        }
        this.guiProcedure = guiProcedure;
        this.controllerReference = controllerReference;
    }

    @Override
    public void execute() {
        Procedure temp = null;
        if (this.guiProcedure != null) {
            temp = new Procedure(guiProcedure);
        } else if (this.oldProcedure != null) {
            temp = new Procedure(this.oldProcedure);
        }

        if (oldProcedure == null) {
            this.oldProcedure = temp;
            this.guiProcedure = null;
        } else if (guiProcedure == null) {
            this.guiProcedure = temp;
            this.oldProcedure = null;
        } else {
            this.guiProcedure.setAffectedOrgans(this.oldProcedure.getAffectedOrgans());
            this.guiProcedure.setDateOfProcedure(this.oldProcedure.getDateOfProcedure());
            this.guiProcedure.setDescription(this.oldProcedure.getDescription());
            this.guiProcedure.setSummary(this.oldProcedure.getSummary());
            this.oldProcedure.setAffectedOrgans(temp.getAffectedOrgans());
            this.oldProcedure.setDateOfProcedure(temp.getDateOfProcedure());
            this.oldProcedure.setDescription(temp.getDescription());
            this.oldProcedure.setSummary(temp.getSummary());
        }
        this.controllerReference.populateTables();
    }

    @Override
    public void revert() {
        execute();
    }
}
