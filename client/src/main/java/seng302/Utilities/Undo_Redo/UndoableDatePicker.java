package seng302.Utilities.Undo_Redo;

import javafx.scene.control.DatePicker;

import java.time.LocalDate;

public class UndoableDatePicker implements Command {
    private LocalDate oldValue;
    private DatePicker datePicker;

    public UndoableDatePicker(DatePicker datePicker, LocalDate oldValue) {
        this.datePicker = datePicker;
        this.oldValue = oldValue;
    }

    /**
     * Function is called when popping the UndoableDatePicker off the undo deque. The function stores the latest value
     * from the GUI in a temp variable before setting the GUI element value to oldValue. The oldValue is then
     * overwritten with the temp.
     */
    public void execute() {
        LocalDate temp = this.datePicker.getValue();
        this.datePicker.setValue(this.oldValue);
        this.oldValue = temp;
        this.datePicker.valueProperty().addListener((obs, old, newV) -> {
        });
    }

    /**
     * This function is called when popping the UndoableDatePicker off the redo deque. The function simply calls the
     * execute().
     */
    public void revert() {
        execute();
    }
}
