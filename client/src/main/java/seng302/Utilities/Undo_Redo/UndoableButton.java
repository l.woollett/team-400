package seng302.Utilities.Undo_Redo;

import javafx.scene.control.Button;

public class UndoableButton implements Command {

    private Button guiBtn;
    private String oldText;
    private UndoableTabChange tabChange;
    private boolean blockUndoing;

    public UndoableButton(Button btn, UndoableTabChange tabChange, boolean blockUndoing) {
        this.blockUndoing = blockUndoing;
        this.oldText = btn.getText();
        this.guiBtn = btn;
        this.tabChange = tabChange;
    }


    @Override
    public void execute() {
        this.blockUndoing = true;
        String temp = this.guiBtn.getText();
        this.guiBtn.setText(this.oldText);
        this.oldText = temp;
        this.tabChange.execute();
        this.blockUndoing = false;
    }

    @Override
    public void revert() {
        this.blockUndoing = true;
        String temp = this.guiBtn.getText();
        this.guiBtn.setText(this.oldText);
        this.oldText = temp;
        this.tabChange.revert();
        this.blockUndoing = false;
    }
}
