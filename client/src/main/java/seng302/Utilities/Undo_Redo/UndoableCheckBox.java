package seng302.Utilities.Undo_Redo;

import javafx.scene.control.CheckBox;

public class UndoableCheckBox implements Command {

    private CheckBox checkBox;

    public UndoableCheckBox(CheckBox checkBox) {
        this.checkBox = checkBox;
    }

    /**
     * Function is called when the UndoableCheckBox is popped of the undo deque. It simply sets the value of the
     * checkbox in the GUI to the opposite of its current value.
     */
    @Override
    public void execute() {
        checkBox.setSelected(!checkBox.isSelected());
    }

    /**
     * Function is called when the UndoableCheckBox is popped off of he redo deque. This function just calls execute.
     */
    @Override
    public void revert() {
        execute();
    }
}
