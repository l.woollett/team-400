package seng302.Utilities.Undo_Redo;


import javafx.scene.control.ListView;

import java.util.ArrayList;
import java.util.List;

public class UndoableListViewString implements Command {

    private ListView<String> listView;
    private String oldValue;

    public UndoableListViewString(ListView<String> listView, String oldValue) {
        this.listView = listView;
        this.oldValue = oldValue;
    }

    /**
     * Function is called when the ListView is popped off the undo deque. The GUI current value is stored
     * in a temp variable before it gets set to the stored oldValue. The oldValue is then overwritten by the temp.
     */
    @Override
    public void execute() {
        List<String> temp = new ArrayList<>(this.listView.getItems());
        this.listView.getItems().remove(oldValue);
        this.oldValue = temp.toString();
        this.listView.cellFactoryProperty().addListener((obs, old, newV) -> {
        });

    }

    /**
     * Function is called when the UndoableListViewString is popped off the redo deque. It simply calls the execute
     * function.
     */
    @Override
    public void revert() {
        execute();
    }
}
