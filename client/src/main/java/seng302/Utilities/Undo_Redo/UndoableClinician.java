package seng302.Utilities.Undo_Redo;

import seng302.GUI.Clinician.ClinicianProfileTabController;
import seng302.Model.Clinician;

import java.util.ArrayList;

public class UndoableClinician implements Command {

    private Clinician oldClinician;
    private Clinician currentClinician;
    private ClinicianProfileTabController undoableController;

    /**
     * Pass two of the Clinician objects to create an old and a new copy for undo redo purposes. If event was to
     * create a new Clinician pass (null, newClinician, controller), edit event pass two copies of the same Clinician object
     * before changes apply, in delete scenario pass (Clinician, null, controller).
     *
     * @param oldClinician       Clinician object before changes apply (Null if a new Clinician is being made).
     * @param clinician          Clinician object in it's state after the changes apply (Null if a Clinician is deleted).
     * @param undoableController The controller object for the clinician scene.
     */
    public UndoableClinician(Clinician oldClinician, Clinician clinician,
                             ClinicianProfileTabController undoableController) {
        this.oldClinician = createDeepCopy(oldClinician);
        this.currentClinician = clinician;
        this.undoableController = undoableController;
    }

    /**
     * Function is called when the UndoableClinician is popped off the undo deque.
     */
    @Override
    public void execute() {
        //Create a copy of the newly created clinician for redo purposes
        Clinician temp = createDeepCopy(this.currentClinician);
        //Clinician edit event
        //reset the attribute values
        this.currentClinician.setFirstName(oldClinician.getFirstName());
        this.currentClinician.setMiddleName(oldClinician.getMiddleName());
        this.currentClinician.setLastName(oldClinician.getLastName());
        this.currentClinician.setStaffId(oldClinician.getStaffId());
        this.currentClinician.setUsername(oldClinician.getUsername());
        this.currentClinician.setAddress(oldClinician.getAddress());
        this.currentClinician.setRegion(oldClinician.getRegion());
        this.currentClinician.setHospital(oldClinician.getHospital());
        this.currentClinician.setCreatedDate(oldClinician.getCreatedDate());
        this.currentClinician.setModifiedDate(oldClinician.getModifiedDate());
        this.currentClinician.setChanges(oldClinician.getChanges());
        //change the oldClinician for redo
        this.oldClinician = temp;
        undoableController.editClinicianEvent(true);
        undoableController.setClinicianFields();


    }

    /**
     * Function is called when the UndoableProfile is popped off the redo deque. It simply calls the execute function
     */
    @Override
    public void revert() {
        execute();
        undoableController.parentController.updateUnsavedChangesIndicator(true, false);
        undoableController.cancelEditEvent(true);
    }


    /**
     * Creates a copy of the clinician.
     *
     * @param clinician Clinician object to be copied.
     * @return copy of clinician
     */
    private Clinician createDeepCopy(Clinician clinician) {
        if (clinician != null) {
            Clinician copy = new Clinician(clinician.getFirstName(), clinician.getMiddleName(), clinician.getLastName(),
                    clinician.getUsername(), clinician.getAddress(), clinician.getRegion(), clinician.getGender(),
                    clinician.getCreatedDate(), clinician.getDateOfBirth(), clinician.getHospital(),
                    clinician.getStaffId());
            copy.setChanges(new ArrayList<>(clinician.getChanges()));
            return copy;
        } else {
            return null;
        }
    }
}