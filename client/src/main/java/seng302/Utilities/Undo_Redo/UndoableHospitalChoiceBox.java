package seng302.Utilities.Undo_Redo;

import javafx.scene.control.ChoiceBox;
import seng302.Model.Hospital;

/**
 * Implements the command pattern to be able to undo redo selecting a hospital in a choicebox.
 */
public class UndoableHospitalChoiceBox implements Command {

    private ChoiceBox<Hospital> choiceBox;
    private Hospital oldHospital;

    public UndoableHospitalChoiceBox(ChoiceBox<Hospital> choiceBox, Hospital hospital) {
        this.choiceBox = choiceBox;
        this.oldHospital = hospital;
    }

    @Override
    public void execute() {
        Hospital temp = null;
        if (choiceBox.getValue() != null) {
            temp = new Hospital(choiceBox.getValue());
        }
        this.choiceBox.setValue(this.oldHospital);
        this.oldHospital = temp;


    }

    @Override
    public void revert() {
        execute();
    }
}
