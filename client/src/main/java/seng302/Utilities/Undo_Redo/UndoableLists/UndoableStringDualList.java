package seng302.Utilities.Undo_Redo.UndoableLists;

import javafx.collections.ObservableList;
import seng302.Utilities.Undo_Redo.Command;

import java.util.ArrayList;
import java.util.List;

public class UndoableStringDualList implements Command {

    private List<String> oldList1;
    private List<String> oldList2;
    private ObservableList<String> guiList1;
    private ObservableList<String> guiList2;

    public UndoableStringDualList(ObservableList<String> oldList1, ObservableList<String> oldList2) {
        this.oldList1 = new ArrayList<>(oldList1);
        this.oldList2 = new ArrayList<>(oldList2);
        this.guiList1 = oldList1;
        this.guiList2 = oldList2;

    }

    /**
     * Function is called when the UndoableObjectDualList is popped off the undo deque. Stores the current guiList
     * values in temp variables, then sets the guiLists to contain the oldValues before overwriting the oldValues with
     * the temp lists
     */
    @Override
    public void execute() {
        List<String> temp1 = new ArrayList<>(guiList1);
        List<String> temp2 = new ArrayList<>(guiList2);
        this.guiList1.clear();
        this.guiList1.addAll(this.oldList1);
        this.guiList2.clear();
        this.guiList2.addAll(this.oldList2);
        this.oldList1.clear();
        this.oldList1.addAll(temp1);
        this.oldList2.clear();
        this.oldList2.addAll(temp2);
    }

    @Override
    public void revert() {
        execute();
    }
}

