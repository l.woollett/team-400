package seng302.Utilities.Undo_Redo.UndoableLists;

import javafx.collections.ObservableList;
import seng302.GUI.MedicalHistory.MedicalHistoryTabController;
import seng302.Model.Disease;
import seng302.Utilities.Undo_Redo.Command;

import java.util.ArrayList;
import java.util.List;

public class UndoableDiseaseDualList implements Command {

    private List<Disease> oldList1;
    private List<Disease> oldList2;
    private ObservableList<Disease> guiList1;
    private ObservableList<Disease> guiList2;
    private MedicalHistoryTabController medicalHistoryTabController;

    public UndoableDiseaseDualList(ObservableList<Disease> oldList1, ObservableList<Disease> oldList2, MedicalHistoryTabController controller) {
        this.oldList1 = new ArrayList<>(oldList1);
        this.oldList2 = new ArrayList<>(oldList2);
        this.guiList1 = oldList1;
        this.guiList2 = oldList2;
        this.medicalHistoryTabController = controller;

    }

    /**
     * Function is called when the UndoableDiseaseDualList is popped off the undo deque. Stores the current guiList
     * values in temp variables, then sets the guiLists to contain the oldValues before overwriting the oldValues
     * with the temp lists
     */
    @Override
    public void execute() {
        List<Disease> temp1 = new ArrayList<>(guiList1);
        List<Disease> temp2 = new ArrayList<>(guiList2);
        this.guiList1.clear();
        this.guiList1.addAll(this.oldList1);
        this.guiList2.clear();
        this.guiList2.addAll(this.oldList2);
        this.oldList1.clear();
        this.oldList1.addAll(temp1);
        this.oldList2.clear();
        this.oldList2.addAll(temp2);
        medicalHistoryTabController.updateTables();
    }

    @Override
    public void revert() {
        execute();
    }
}
