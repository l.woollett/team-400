package seng302.Utilities.Undo_Redo;

import javafx.scene.control.TextArea;

public class UndoableTextArea implements Command {

    private TextArea textArea;
    private String oldValue;

    public UndoableTextArea(TextArea textArea, String oldValue) {
        this.textArea = textArea;
        this.oldValue = oldValue.trim();
    }

    /**
     * The execute function is called when the UndoableTextField is popped off the undo deque. First the value of the
     * TextField in the GUI is saved inside temp, then the TextField value is set to the oldValue stored in the
     * UndoableTextField. The oldValue is then overwritten with the temp before the UndoableTextField is pushed onto the
     * redo deque.
     */
    @Override
    public void execute() {
        String temp = textArea.getText().trim();
        textArea.setText(this.oldValue);
        this.oldValue = temp;
    }

    /**
     * The revert function is called when the object is popped off the redo deque. It simply performs the execute
     * function.
     */
    @Override
    public void revert() {
        execute();
    }
}
