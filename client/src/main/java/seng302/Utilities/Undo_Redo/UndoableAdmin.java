package seng302.Utilities.Undo_Redo;

import seng302.GUI.Admin.AdminSceneController;
import seng302.GUI.ControllerAccess;
import seng302.GUI.Notifications.PushNotification;
import seng302.Model.Admin;
import seng302.ModelController.AdminController;
import seng302.Utilities.DataInteraction;

import java.io.IOException;
import java.util.logging.Logger;


public class UndoableAdmin implements Command {

    private final Logger LOGGER = Logger.getLogger(getClass().getName());
    private Admin oldAdmin;
    private Admin currentAdmin;
    private AdminSceneController adminController;
    private boolean redo = false;

    /**
     * Pass two of the Admin object to create an old and a new copy for undo redo purposes. If event was to create a
     * new Admin pass (null, newAdmin, controller), edit event pass two copies of the same Admin object before changes
     * apply, in delete scenario pass (Admin, null, controller)
     *
     * @param oldAdmin        The Admin object before changes occurred. Null when admin created.
     * @param currentAdmin    The Admin object as it is after changes applied.
     * @param adminController The controller class object for the admin scene
     */
    public UndoableAdmin(Admin oldAdmin, Admin currentAdmin, AdminSceneController adminController) {
        this.oldAdmin = createDeepCopy(oldAdmin);
        this.currentAdmin = currentAdmin;
        this.adminController = adminController;
    }

    /**
     * Function is called when the UndoableAdmin is popped off the undo deque.
     */
    @Override
    public void execute() {
        //Create a copy of the newly created clinician for redo purposes
        Admin temp = createDeepCopy(this.currentAdmin);
        // Case for deleting admin.
        if (this.oldAdmin == null) {
            AdminController.removeAdmin(currentAdmin.getUsername());

            this.currentAdmin = null;
            this.oldAdmin = temp;
            PushNotification pushNotification = new PushNotification("Admin removed!",
                    "The admin has been removed.");
            pushNotification.show();
            ControllerAccess.getAdminSceneController().isEditing();

            // Case for restoring deleted admin.
        } else if (this.currentAdmin == null) {
            this.currentAdmin = this.oldAdmin;
            AdminController.addAdmin(this.currentAdmin.getUsername(), this.currentAdmin);
            //set the oldProfile to null for redoing the delete
            this.oldAdmin = temp;
            PushNotification pushNotification = new PushNotification("Admin added!",
                    "The admin has been added.");
            pushNotification.show();
        } else {
            //reset the attribute values
            this.currentAdmin.setUsername(oldAdmin.getUsername());
            this.currentAdmin.setPassword(oldAdmin.getPassword());
            //change the oldClinician for redo
            this.oldAdmin = temp;


            if (!this.redo) {
                PushNotification pushNotification = new PushNotification("Admin changes removed!",
                        "The admin has been returned to its previous state.");
                pushNotification.show();
            } else {
                PushNotification pushNotification = new PushNotification("Admin changes saved!",
                        "The admin changes have been saved");
                pushNotification.show();
            }
        }
        try {
            DataInteraction.saveUsers();
        } catch (IOException e) {
            LOGGER.severe(e.getMessage());
        }

    }

    /**
     * Function is called when the UndoableProfile is popped off the redo deque. It simply calls the execute function
     */
    @Override
    public void revert() {
        this.redo = true;
        execute();
        adminController.updateUnsavedChangesIndicator(true, false);
        this.redo = false;
    }


    /**
     * Creates a copy of the admin.
     *
     * @param admin Admin object to make a copy of
     * @return copy of admin
     */
    private Admin createDeepCopy(Admin admin) {
        if (admin != null) {
            return new Admin(admin.getUsername(), admin.getPassword());
        } else {
            return null;
        }
    }
}

