package seng302.Utilities.Undo_Redo;

import seng302.GUI.Clinician.ClinicianProfileTabController;
import seng302.GUI.Clinician.ClinicianSceneController;

public class UndoableClinicianCancel implements Command {

    private ClinicianProfileTabController undoableController;
    private ClinicianSceneController parentController;

    public UndoableClinicianCancel(ClinicianProfileTabController undoableController, ClinicianSceneController parentController) {
        this.undoableController = undoableController;
        this.parentController = parentController;
    }

    /**
     * Function is called when the UndoableProfileEvent is popped of the undo deque. It calls the edit
     * event or save event .
     */
    @Override
    public void execute() {

        if (!parentController.isEditing()) {
            undoableController.editClinicianEvent(true);
            undoableController.setClinicianFieldsVisible(false);
            undoableController.setEditFieldsVisible(true);

        } else {
            undoableController.cancelEditEvent(true);
        }
    }

    /**
     * Function is called when the UndoableCheckBox is popped off of he redo deque. This function just calls execute.
     */
    @Override
    public void revert() {
        execute();
    }
}

