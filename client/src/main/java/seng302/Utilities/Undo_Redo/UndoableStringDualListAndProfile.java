package seng302.Utilities.Undo_Redo;

import javafx.collections.ObservableList;
import seng302.Enum.AlcoholConsumptionEnum;
import seng302.Model.Profile;

import java.util.ArrayList;
import java.util.List;

public class UndoableStringDualListAndProfile implements Command {

    private List<String> oldList1;
    private List<String> oldList2;
    private ObservableList<String> guiList1;
    private ObservableList<String> guiList2;
    private Profile guiProfile;
    private Profile copyProfile;

    public UndoableStringDualListAndProfile(ObservableList<String> oldList1, ObservableList<String> oldList2, Profile
            profile) {
        this.oldList1 = new ArrayList<>(oldList1);
        this.oldList2 = new ArrayList<>(oldList2);
        this.guiList1 = oldList1;
        this.guiList2 = oldList2;
        guiProfile = profile;
        copyProfile = new Profile(profile);
    }

    /**
     * Function is called when the UndoableObjectDualList is popped off the undo deque. Stores the current guiList
     * values in temp variables, then sets the guiLists to contain the oldValues before overwriting the oldValues with
     * the temp lists
     */
    @Override
    public void execute() {
        List<String> temp1 = new ArrayList<>(guiList1);
        List<String> temp2 = new ArrayList<>(guiList2);
        this.guiList1.clear();
        this.guiList1.addAll(this.oldList1);
        this.guiList2.clear();
        this.guiList2.addAll(this.oldList2);
        this.oldList1.clear();
        this.oldList1.addAll(temp1);
        this.oldList2.clear();
        this.oldList2.addAll(temp2);
        undoProfileChanges();
    }

    @Override
    public void revert() {
        execute();
    }

    private void undoProfileChanges() {
        Profile temp = new Profile(this.guiProfile);
        this.guiProfile.setFirstName(copyProfile.getFirstName());
        this.guiProfile.setMiddleName(copyProfile.getMiddleName());
        this.guiProfile.setLastName(copyProfile.getLastName());
        this.guiProfile.setUsername(copyProfile.getUsername());
        this.guiProfile.setAddress(copyProfile.getAddress());
        this.guiProfile.setRegion(copyProfile.getRegion());
        this.guiProfile.setCreatedDate(copyProfile.getCreatedDate());
        this.guiProfile.setGender(copyProfile.getGender());
        this.guiProfile.setDateOfBirth(copyProfile.getDateOfBirth());
        this.guiProfile.setDeath(copyProfile.getDeath());
        this.guiProfile.setHeight(copyProfile.getHeight());
        this.guiProfile.setWeight(copyProfile.getWeight());
        this.guiProfile.setBloodType(copyProfile.getBloodType());
        this.guiProfile.setIsDonor(copyProfile.isDonor());
        this.guiProfile.setDonorOrgans(copyProfile.getDonorOrgans());
        this.guiProfile.setSmoker(copyProfile.isSmoker());
        this.guiProfile.setBloodPressure(copyProfile.getBloodPressure());
        this.guiProfile.setAlcoholConsumption((AlcoholConsumptionEnum) copyProfile.getAlcoholConsumption());
        this.guiProfile.setBirthGender(copyProfile.getBirthGender());
        this.guiProfile.setAlias(copyProfile.getAlias());
        this.guiProfile.setCurrentDiseases(copyProfile.getCurrentDiseases());
        this.guiProfile.setPastDiseases(copyProfile.getPastDiseases());
        this.guiProfile.setReceiverOrgans(copyProfile.getReceiverOrgans());
        this.guiProfile.setPendingProcedures(copyProfile.getPendingProcedures());
        this.guiProfile.setPastProcedures(copyProfile.getPastProcedures());
        this.guiProfile.setCurrentMedications(copyProfile.getCurrentMedications());
        this.guiProfile.setPreviousMedications(copyProfile.getPreviousMedications());
        this.guiProfile.setIsReceiver(copyProfile.isReceiver());
        this.guiProfile.setModifiedDate(copyProfile.getModifiedDate());
        this.guiProfile.setChanges(copyProfile.getChanges());
        //change the copyProfile for redo
        this.copyProfile = temp;
    }
}

