package seng302.Utilities.Undo_Redo;

/**
 * Interface that forms part of the command pattern. Essential for the undo/redo functionality. If you want to add undo
 * functionality to any object or element create a class that accepts the object and stores the relevant values and
 * ensure that it implements this interface. Then you can make use of the UndoRedoController
 */
public interface Command {
    void execute();

    void revert();
}

