package seng302.Utilities.Undo_Redo;

import seng302.CustomException.DoesNotExist;
import seng302.Enum.AlcoholConsumptionEnum;
import seng302.GUI.Notifications.PushNotification;
import seng302.GUI.Profile.ProfileSceneController;
import seng302.Model.Profile;
import seng302.ModelController.ProfileController;
import seng302.Utilities.DataInteraction;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * This object must be created before changes are applied on profiles. So if you create a new a new profile, then
 * create this object with a null as the parameter. If you apply changes, then create this object before applying to
 * the profile so that the old profile is copied. When a profile gets deleted, create this object before deleting
 */
public class UndoableProfile implements Command {

    private final Logger LOGGER = Logger.getLogger(getClass().getName());
    private Profile oldProfile;
    private Profile currentProfile;
    private ProfileSceneController undoableController;
    private boolean redo = false;
    private UndoableButton donorBtn;
    private UndoableButton receiverBtn;

    /**
     * Constructor creates a deep copy of the provided profile and stores the actual profile to track it for later
     * replacement to undo creation, applying changes and deletion of profiles. In the case where wanting to undo a
     * creation the oldProfile value must be null. If delete case, pass the Profile in oldValue and null as new value
     * . If modification, before applying the event pass two references to the same object.
     *
     * @param oldProfile         Profile object (null if create case, duplicate profiles else)
     * @param profile            Profile object that will be created.
     * @param undoableController Controller object for the profile scene
     * @param donorBtn           The register as a donor button.
     * @param receiverBtn        The register as a receiver button.
     */
    public UndoableProfile(Profile oldProfile, Profile profile, ProfileSceneController undoableController,
                           UndoableButton donorBtn, UndoableButton receiverBtn) {
        this.oldProfile = createDeepCopy(oldProfile);
        this.currentProfile = profile;
        this.undoableController = undoableController;
        this.donorBtn = donorBtn;
        this.receiverBtn = receiverBtn;
    }

    /**
     * Function is called when the UndoableProfile is popped off the undo deque. Depending on the situation for the
     * profile object (created, deleted, modified) certain actions will occur
     */
    @Override
    public void execute() {
        //Create a copy of the newly created profile for redo purposes
        Profile temp = createDeepCopy(this.currentProfile);
        if (this.oldProfile == null) { //Situation where a profile was created
            //simply remove profile from list of profiles and set current Profile = null
            try {
                ProfileController.deleteProfile(this.currentProfile.getUsername());
            } catch (DoesNotExist e) {
                LOGGER.severe(e.getMessage());
            }
            this.currentProfile = null;
            this.oldProfile = temp;
            PushNotification pushNotification = new PushNotification("Profile removed!",
                    "The profile has been removed.");
            pushNotification.show();
        } else if (this.currentProfile == null) { //situation where the profile has been deleted
            this.currentProfile = this.oldProfile;
            ProfileController.addProfile(this.currentProfile.getUsername(), this.currentProfile);
            //set the oldProfile to null for redoing the delete
            this.oldProfile = temp;

            PushNotification pushNotification = new PushNotification("Profile added!",
                    "The profile has been added.");
            pushNotification.show();
        } else { //Profile edit event
            //reset the attribute values
            if (donorBtn != null && receiverBtn != null) {
                donorBtn.execute();
                receiverBtn.execute();
            }
            this.currentProfile.setFirstName(oldProfile.getFirstName());
            this.currentProfile.setMiddleName(oldProfile.getMiddleName());
            this.currentProfile.setLastName(oldProfile.getLastName());
            this.currentProfile.setUsername(oldProfile.getUsername());
            this.currentProfile.setAddress(oldProfile.getAddress());
            this.currentProfile.setRegion(oldProfile.getRegion());
            this.currentProfile.setCreatedDate(oldProfile.getCreatedDate());
            this.currentProfile.setGender(oldProfile.getGender());
            this.currentProfile.setDateOfBirth(oldProfile.getDateOfBirth());
            this.currentProfile.setDeath(oldProfile.getDeath());
            this.currentProfile.setHeight(oldProfile.getHeight());
            this.currentProfile.setWeight(oldProfile.getWeight());
            this.currentProfile.setBloodType(oldProfile.getBloodType());
            this.currentProfile.setIsDonor(oldProfile.isDonor());
            this.currentProfile.setDonorOrgans(oldProfile.getDonorOrgans());
            this.currentProfile.setSmoker(oldProfile.isSmoker());
            this.currentProfile.setBloodPressure(oldProfile.getBloodPressure());
            this.currentProfile.setAlcoholConsumption((AlcoholConsumptionEnum) oldProfile.getAlcoholConsumption());
            this.currentProfile.setBirthGender(oldProfile.getBirthGender());
            this.currentProfile.setAlias(oldProfile.getAlias());
            this.currentProfile.setCurrentDiseases(oldProfile.getCurrentDiseases());
            this.currentProfile.setPastDiseases(oldProfile.getPastDiseases());
            this.currentProfile.setReceiverOrgans(oldProfile.getReceiverOrgans());
            this.currentProfile.setPendingProcedures(oldProfile.getPendingProcedures());
            this.currentProfile.setPastProcedures(oldProfile.getPastProcedures());
            this.currentProfile.setCurrentMedications(oldProfile.getCurrentMedications());
            this.currentProfile.setPreviousMedications(oldProfile.getPreviousMedications());
            this.currentProfile.setIsReceiver(oldProfile.isReceiver());
            this.currentProfile.setModifiedDate(oldProfile.getModifiedDate());
            this.currentProfile.setChanges(oldProfile.getChanges());
            //change the oldProfile for redo
            this.oldProfile = temp;
            undoableController.editProfileEvent(true);
            undoableController.setProfileFieldsText();
            if (this.redo) {
                PushNotification pushNotification = new PushNotification("Profile Changes Saved!",
                        "The profile changes have been saved");
                pushNotification.show();
            }
        }
        try {
            DataInteraction.saveUsers();
        } catch (IOException e) {
            LOGGER.severe(e.getMessage());
        }

    }

    /**
     * Function is called when the UndoableProfile is popped off the redo deque. It simply calls the execute function
     */
    @Override
    public void revert() {
        this.redo = true;
        execute();
        if (undoableController != null) {
            undoableController.updateUnsavedChangesIndicator(true, false);
            undoableController.cancelProfileEditEvent(true);
        }
        this.redo = false;
    }


    /**
     * Function creates a deep copy of the provided profile.
     *
     * @param profile Profile to be copied
     * @return Deep copy of the Profile
     */
    private Profile createDeepCopy(Profile profile) {
        if (profile != null) {
            return new Profile(profile);
        } else {
            return null;

        }

    }
}
