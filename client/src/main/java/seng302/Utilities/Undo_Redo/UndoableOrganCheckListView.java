package seng302.Utilities.Undo_Redo;

import javafx.beans.property.SimpleBooleanProperty;

public class UndoableOrganCheckListView implements Command {

    private SimpleBooleanProperty guiValue;

    public UndoableOrganCheckListView(SimpleBooleanProperty guiValue) {
        this.guiValue = guiValue;
    }

    /**
     * Function is called when the UndoableOrganCheckListView is popped off the undo deque. First the current value in
     * the GUI is set to a temp HashMap, then the GUI's HashMap is cleared and repopulated with the stored oldValue. The
     * oldValue is then cleared and repopulated with the temp value.
     */
    @Override
    public void execute() {
        guiValue.setValue(!guiValue.getValue());
    }

    /**
     * Function is called when the UndoableOrganCheckListView is popped off the redo stack. It simply calls the execute
     * function.
     */
    @Override
    public void revert() {
        execute();
    }
}