package seng302.Utilities.Undo_Redo;

import seng302.GUI.Profile.ProfileSceneController;

public class UndoableProfileCancel implements Command {

    private ProfileSceneController undoableController;

    public UndoableProfileCancel(ProfileSceneController controller) {
        this.undoableController = controller;
    }

    /**
     * Function is called when the UndoableProfileEvent is popped of the undo deque. It calls the edit
     * event or save event .
     */
    @Override
    public void execute() {


        if (!undoableController.isEditing()) {
            undoableController.editProfileEvent(true);
            undoableController.setProfileTextElementsInvisible();
            undoableController.reveal();

        } else {
            undoableController.cancelProfileEditEvent(true);
        }
    }

    /**
     * Function is called when the UndoableCheckBox is popped off of he redo deque. This function just calls execute.
     */
    @Override
    public void revert() {
        execute();
    }
}
