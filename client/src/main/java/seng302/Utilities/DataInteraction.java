package seng302.Utilities;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import seng302.GUI.Notifications.PushNotification;
import seng302.Model.Admin;
import seng302.Model.Clinician;
import seng302.Model.Profile;
import seng302.ModelController.AdminController;
import seng302.ModelController.CacheController;
import seng302.ModelController.ClinicianController;
import seng302.ModelController.ProfileController;
import seng302.Storage.CacheStorage;
import seng302.Storage.UserStorage;

import java.io.*;
import java.util.ArrayList;
import java.util.logging.Logger;

public class DataInteraction {
    private static String DEFAULT_USER_DATA_FILE = "userdata.json";
    private static String DEFAULT_USER_CACHE_FILE = "cache.json";

    private static Logger LOGGER = Logger.getLogger(DataInteraction.class.getName());

    /**
     * Save the users to a JSON file for persistent storage. This will save the profiles and clinicians.
     *
     * @throws IOException input output exception
     */
    public static void saveUsers() throws IOException {
        FileWriter fileWriter = null;
        try {
            ArrayList<Profile> profiles = ProfileController.getProfiles();
            ArrayList<Clinician> clinicians = ClinicianController.getClinicians();
            ArrayList<Admin> admins = AdminController.getAdmins();
            UserStorage profilesToSave = new UserStorage(profiles, clinicians, admins);
            Gson gson = new Gson();
            String jsonProfilesString = gson.toJson(profilesToSave);
            File file = new File(DEFAULT_USER_DATA_FILE);
            fileWriter = new FileWriter(file);
            fileWriter.write(jsonProfilesString);
            fileWriter.flush();

        } catch (IOException e) {
            LOGGER.severe(e.getMessage());
        } finally {
            if (fileWriter != null) {
                fileWriter.close();
            }
        }
    }

    public static void setTestFileName(String fileName) {
        DEFAULT_USER_DATA_FILE = fileName;
    }

    /**
     * If the the file doesn't exist or the it is corrupted, we will create a blank file by creating an a UserStorage
     * that contains no Clinicians or profiles. When we save this object we will have a blank file.
     *
     * @return An empty UserStorage object that will be saved to a JSON to create a blank file.
     */
    private static UserStorage createBlankFile() {
        try {
            saveUsers();
        } catch (IOException e) {
            LOGGER.severe(e.getMessage());
        }
        PushNotification notification = new PushNotification("Missing File", "Data file was not found or had a error. Created a blank file.");
        notification.showError();
        UserStorage userStorage = new UserStorage(new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
        return userStorage;
    }

    /**
     * Create a UserStorage from the JSON file which contains all the saved clinicians and profiles.
     *
     * @param filePath Where we will load the data from.
     * @return The UserStorage instance containing all the saved clinicians and profiles.
     */
    public static UserStorage loadUsersFromPath(String filePath) {
        Gson gson = new Gson();
        UserStorage userStorage = null;
        try {
            BufferedReader br = new BufferedReader(new FileReader(filePath));
            userStorage = gson.fromJson(br, UserStorage.class);
        } catch (JsonSyntaxException | FileNotFoundException | NumberFormatException e) {
            userStorage = createBlankFile();
        }
        return userStorage;
    }

    /**
     * Load the data from the JSON file.
     *
     * @return The data from the JSON file.
     */
    public static UserStorage loadUsersApplicationStart() {
        return loadUsersFromPath(DEFAULT_USER_DATA_FILE);
    }

    /**
     * Method to save the cached interactions to the JSON file.
     */
    public static void saveCache() {
        FileWriter fileWriter = null;
        try {
            CacheStorage cacheStorage = new CacheStorage(CacheController.getCache());
            Gson gson = new Gson();
            String jsonProfilesString = gson.toJson(cacheStorage);
            File file = new File(DEFAULT_USER_CACHE_FILE);
            fileWriter = new FileWriter(file);
            fileWriter.write(jsonProfilesString);
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            LOGGER.severe(e.getMessage());
        }
    }

    /**
     * Loads the cache from the JSON file.
     *
     * @return A cachestorage that will get passed to a CacheController on program load.
     */
    public static CacheStorage loadCache() {
        Gson gson = new Gson();
        CacheStorage cacheStorage;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(DEFAULT_USER_CACHE_FILE));
            cacheStorage = gson.fromJson(reader, CacheStorage.class);
        } catch (IOException e) {
            saveCache();
            cacheStorage = new CacheStorage();
        }
        return cacheStorage;
    }
}
