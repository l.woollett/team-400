package seng302.Utilities;

import seng302.Model.Disease;

import java.util.Comparator;

/**
 * It sorts the disease name
 */
public class DescendingDiseaseNameComparator implements Comparator<Disease> {
    @Override
    public int compare(Disease o1, Disease o2) {
        return o2.getDiseaseName().compareTo(o1.getDiseaseName());
    }
}
