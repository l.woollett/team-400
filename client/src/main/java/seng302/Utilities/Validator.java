package seng302.Utilities;

import seng302.CustomException.DoesNotExist;
import seng302.Model.Disease;
import seng302.Model.Procedure;
import seng302.Model.Profile;
import seng302.ModelController.GenderController;
import seng302.ModelController.RegionTypeController;
import seng302.ModelController.UserController;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.logging.Logger;

/**
 * Contains constants and methods used for validating data.
 *
 * @author Connor McEwan-McDowall
 */
public abstract class Validator {

    public final static int MAX_ADDRESS_LENGTH = 120;
    private static final Logger LOGGER = Logger.getLogger(Validator.class.getName());

    // The regular expressions used for validating inputs
    /**
     * Generic String: - Can contain alphabet characters - Can contain spaces - Any non-alphabet character must be
     * between two alphabet characters - Must be 1-40 characters long (inclusive)
     */
    private final static String GENERIC_STRING_REGEX = "[\\w](([ ]?[\\w])+)?";

    /**
     * Username: - Can contain word characters - Can contain digits - Can contain underscores - Can not contain spaces -
     * Must be 1-20 characters long (inclusive)
     */
    private final static String USERNAME_REGEX = "[\\w]{1,20}";

    /**
     * First name: - Can contain alphabet characters - Can contain hyphens - Can contain spaces - Can contain
     * apostrophes - Any non-alphabet character must be between two alphabet characters - Must be 1-20 characters long
     * (inclusive)
     */
    private final static String FIRST_NAME_REGEX = "([a-zA-Z]([-' ][a-zA-Z])?)+";

    /**
     * Alias: - Can contain alphabet characters - Can contain hyphens - Can contain spaces - Can contain apostrophes -
     * Any non-alphabet character must be between two alphabet characters - Must be 1-40 characters long (inclusive)
     */
    private final static String ALIAS_REGEX = "([a-zA-Z]+[- ']?[a-zA-Z]?)+";

    /**
     * Organisation: - Can contain alphabet characters - Can contain hyphens - Can contain spaces - Can contain apostrophes -
     * Any non-alphabet character must be between two alphabet characters - Must be 1-40 characters long (inclusive)
     */
    private final static String ORGANISATION_REGEX = "([a-zA-Z]+[- ']?[a-zA-Z]?)+";

    /**
     * Middle name: - Can contain alphabet characters - Can contain hyphens - Can contain spaces - Can contain
     * apostrophes - Any non-alphabet character must be between two alphabet characters - Must be 1-40 characters long
     * (inclusive)
     */
    private final static String MIDDLE_NAME_REGEX = "([a-zA-Z]+[- ']?[a-zA-Z]?)+";

    /**
     * Last name: - Can contain alphabet characters - Can contain hyphens - Can contain spaces - Can contain apostrophes
     * - Any non-alphabet character must be between two alphabet characters - Must be 1-40 characters long (inclusive)
     */
    private final static String LAST_NAME_REGEX = "([a-zA-Z]([-' ][a-zA-Z])?)+";

    /**
     * Height: - Only accepts decimal numbers of the form x.xx where x is a numeric digit
     */
    private final static String HEIGHT_REGEX = "[\\d]\\.[\\d]{2}";

    /**
     * Weight: - Only accepts decimal numbers of the form xxx.xx where x is a numeric digit
     */
    private final static String WEIGHT_REGEX = "[\\d]{1,3}\\.[\\d]{2}";

    /**
     * alphabet characters - Can contain digits - Can contain hyphens - Can contain spaces - Can contain apostrophes -
     * Any non-alphabet character must be between two alphabet characters or digits - Must be 1-40 characters long
     * (inclusive)
     */
    private final static String ADDRESS_REGEX = "([a-zA-Z\\d]([-', ./][a-zA-Z\\d])?([ ,][ ])?){1,120}";

    /**
     * Blood Pressure: - Format: number/number - Forward slash is required - Forward slash must go between two numbers -
     * No spaces are allowed - The numbers must both be 1-3 digits long (inclusive)
     */
    private final static String BLOOD_PRESSURE_REGEX = "([0-9]{1,3}[/][0-9]{1,3})?";

    /**
     * Password: - Can contain word characters - Can contain digits - Can contain underscores - Can not contain spaces -
     * Must be 1-40 characters long (inclusive)
     */
    private final static String PASSWORD_REGEX = "[\\w]{1,40}";

    /**
     * The earliest allowable date of birth.
     */
    private final static LocalDate EARLIEST_DATE_OF_BIRTH =
            LocalDate.parse("01-01-1900", DateTimeFormatter.ofPattern("dd-MM-yyyy"));

    /**
     * Returns true if the given disease name is valid against the GENERIC_STRING_REGEX constant, false otherwise.
     *
     * @param diseaseName The disease name to be checked for validity.
     * @return True if valid, false otherwise.
     */
    public static Boolean isValidDiseaseName(String diseaseName) {
        return diseaseName.matches(GENERIC_STRING_REGEX);
    }

    /**
     * Returns true if the given diagnosis date is valid, false otherwise. A valid diagnosis date must be between the
     * given date of birth and current date.
     *
     * @param diagnosisDate The diagnosis date to be checked for validity.
     * @param dob           The date of birth to be used in the validity check.
     * @return True if valid, false otherwise.
     */
    public static Boolean isValidDiseaseDiagnosisDate(LocalDate diagnosisDate, LocalDate dob) {
        return ((diagnosisDate.compareTo(dob) >= 0) && (diagnosisDate.compareTo(LocalDate.now()) <= 0));
    }

    /**
     * Returns true if the given procedure date is valid, false otherwise. A valid procedure date must be between the
     * given date of birth and current date.
     *
     * @param procedureDate The procedure date to be checked for validity.
     * @param dob           The date of birth to be used in the validity check.
     * @return True if valid, false otherwise.
     */
    public static Boolean isValidProcedureDate(LocalDate procedureDate, LocalDate dob) {
        return ((procedureDate.compareTo(dob) >= 0) && (procedureDate.compareTo(LocalDate.now()) <= 0));
    }

    /**
     * Returns true if the given username is valid against the USERNAME_REGEX constant, false otherwise. Will also check
     * if the username is not used.
     *
     * @param username The username to be checked for validity.
     * @return True if valid, false otherwise.
     */
    public static Boolean isValidUsername(String username) {
        return (username.matches(USERNAME_REGEX) && (!UserController.isUsernameUsed(username)));
    }

    /**
     * Returns true if the integer is valid within the specified range, false otherwise.
     *
     * @param integer    The integer to be checked for validity.
     * @param lowerBound The minimum integer it can be
     * @param upperBound The highest integer it can be.
     * @return True if valid, false otherwise.
     */
    public static Boolean isValidIntegerWithRange(String integer, int lowerBound, int upperBound) {
        try {
            int parsedInteger = Integer.parseInt(integer);
            if (parsedInteger >= lowerBound && parsedInteger < upperBound) {
                return true;
            }
        } catch (NumberFormatException e) {
            return false;
        }
        return false;
    }

    /**
     * Returns true if the hour of death is within 0 to (including) 23.
     *
     * @param hour The hour to checked for validity
     * @return True if valid, false otherwise.
     */
    public static Boolean isValidHourTimeOfDeath(String hour) {
        return isValidIntegerWithRange(hour, 0, 24);
    }

    /**
     * Returns true if the provided time is before or equal to the current time.
     *
     * @param hrStr The hours entered as a String.
     * @param minuteStr The minutes entered as a String.
     * @param secStr The seconds entered as a String.
     * @return True if the timestamp is valid.
     */
    public static Boolean isValidTimeStamp(String hrStr, String minuteStr, String secStr) {
        try {
            int hours = Integer.parseInt(hrStr);
            int minute = Integer.parseInt(minuteStr);
            int seconds = Integer.parseInt(secStr);

            LocalDateTime currentDateTime = LocalDateTime.now();

            if (hours < currentDateTime.getHour()) {
                return true;
            } else if (hours == currentDateTime.getHour() && minute < currentDateTime.getMinute()) {
                return true;
            } else if (hours == currentDateTime.getHour() && minute == currentDateTime.getMinute() &&
                    seconds <= currentDateTime.getSecond()) {
                return true;
            } else {
                return false;
            }

        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * Returns true if the minute of death is within 0 to (including) 59.
     *
     * @param minute The minute to checked for validity
     * @return True if valid, false otherwise.
     */
    public static Boolean isValidMinuteTimeOfDeath(String minute) {
        return isValidIntegerWithRange(minute, 0, 60);
    }

    /**
     * Returns true if the second of death is within 0 to (including) 59.
     *
     * @param second The second to checked for validity
     * @return True if valid, false otherwise.
     */
    public static Boolean isValidSecondTimeOfDeath(String second) {
        return isValidIntegerWithRange(second, 0, 60);
    }

    /**
     * Returns true if the given staffId is valid less than 99999, false otherwise. Will also check
     * if the staffId is not used. Staff id cannot be 0
     *
     * @param staffId The username to be checked for validity.
     * @return True if valid, false otherwise.
     */
    public static Boolean isValidStaffId(String staffId) {
        try {
            int parsedId = Integer.parseInt(staffId);
            if (parsedId < 99999 && parsedId > 0) {
                return true;
            }
        } catch (NumberFormatException e) {
            return false;
        }
        return false;
    }

    /**
     * Returns true if the given password is valid against the PASSWORD_REGEX constant, false otherwise.
     *
     * @param password The password to be checked for validity.
     * @return True if valid, false otherwise.
     */
    public static Boolean isValidPassword(String password) {
        return password.matches(PASSWORD_REGEX);
    }

    /**
     * Returns true if the given alias is valid against the alias constant, false otherwise.
     *
     * @param alias The alias to be checked for validity.
     * @return True if valid, false otherwise.
     */
    public static Boolean isValidAlias(String alias) {
        return (alias.matches(ALIAS_REGEX) && alias.length() < 40 || alias.trim().equals(""));
    }


    /**
     * Returns true if the given first name is valid against the FIRST_NAME_REGEX constant, false otherwise.
     *
     * @param firstName The first name to be checked for validity.
     * @return True if valid, false otherwise.
     */
    public static Boolean isValidFirstName(String firstName) {
        return (firstName.matches(FIRST_NAME_REGEX) && (firstName.length() <= 20));
    }

    /**
     * Returns true if the given middle name is valid against the MIDDLE_NAME_REGEX constant, false otherwise.
     *
     * @param middleName The middle name to be checked for validity.
     * @return True if valid, false otherwise.
     */
    public static Boolean isValidMiddleName(String middleName) {
        return (middleName.matches(MIDDLE_NAME_REGEX) && middleName.length() < 40 || middleName.trim().equals(""));
    }

    /**
     * Returns true if the given last name is valid against the LAST_NAME_REGEX constant, false otherwise.
     *
     * @param lastName The last name to be checked for validity.
     * @return True if valid, false otherwise.
     */
    public static Boolean isValidLastName(String lastName) {
        return (lastName.matches(LAST_NAME_REGEX) && lastName.length() < 40 || lastName.trim().equals(""));
    }

    /**
     * Returns true if the given height is valid against the HEIGHT_REGEX constant, false otherwise.
     *
     * @param height The height to be checked for validity.
     * @return True if valid, false otherwise.
     */
    public static Boolean isValidHeight(String height) {
        return height.matches(HEIGHT_REGEX);
    }

    /**
     * Returns true if the given weight is valid against the WEIGHT_REGEX constant, false otherwise.
     *
     * @param weight The weight to be checked for validity.
     * @return True if valid, false otherwise.
     */
    public static Boolean isValidWeight(String weight) {
        return (weight.matches(WEIGHT_REGEX));
    }

    /**
     * Returns true if the given address is valid against the ADDRESS_REGEX constant, false otherwise.
     *
     * @param address The address to be checked for validity.
     * @return True if valid, false otherwise.
     */
    public static Boolean isValidAddress(String address) {
        return (address.matches(ADDRESS_REGEX) && address.length() <= MAX_ADDRESS_LENGTH);
    }

    /**
     * Returns true if the given date of birth given is greater than or equal to EARLIEST_DATE_OF_BIRTH and less than or
     * equal to today's date.
     *
     * @param dateOfBirth The date of birth to be validated.
     * @return True if valid, false otherwise.
     */
    public static Boolean isValidDateOfBirth(LocalDate dateOfBirth) {
        if (dateOfBirth != null) {
            try {
                LocalDate currentDate = LocalDate.now();
                return ((dateOfBirth.compareTo(currentDate) <= 0) &&
                        (dateOfBirth.compareTo(EARLIEST_DATE_OF_BIRTH) >= 0));
            } catch (DateTimeParseException e) {
                return false; // Invalid string to parse
            }
        } else {
            return false;
        }

    }

    /**
     * Returns true if the given date of birth given is greater than or equal to EARLIEST_DATE_OF_BIRTH and less than
     * or equal to today's date. also must be greater than all the procedures/ medications date
     *
     * @param dateOfBirth    The date of birth to be validated.
     * @param currentProfile The profile whose date of birth is attempting to be changed..
     * @return True if valid, false otherwise.
     */
    public static Boolean isValidChangeOfDateOfBirth(LocalDate dateOfBirth, Profile currentProfile) {
        try {
            LocalDate currentDate = LocalDate.now();
            if ((dateOfBirth.compareTo(currentDate) <= 0) && (dateOfBirth.compareTo(EARLIEST_DATE_OF_BIRTH) >= 0)) {
                for (Procedure eachProcedure : currentProfile.getPastProcedures()) {
                    if (eachProcedure.getDateOfProcedure().isBefore(dateOfBirth)) {
                        return false;
                    }
                }
                for (Procedure eachProcedure : currentProfile.getPendingProcedures()) {
                    if (eachProcedure.getDateOfProcedure().isBefore(dateOfBirth)) {
                        return false;
                    }
                }
                for (Disease eachDisease : currentProfile.getCurrentDiseases()) {
                    if (eachDisease.getDateOfDiagnosis().isBefore(dateOfBirth)) {
                        return false;
                    }
                }
                for (Disease eachDisease : currentProfile.getPastDiseases()) {
                    if (eachDisease.getDateOfDiagnosis().isBefore(dateOfBirth)) {
                        return false;
                    }
                }
                return true;
            } else {
                return false;
            }
        } catch (DateTimeParseException e) {
            return false; // Invalid string to parse
        }
    }

    /**
     * Will check if the value is a valid representation of a LocalDate.
     *
     * @param localDate The string representation of a local date.
     * @return True if it can be parsed to a LocalDate, false otherwise.
     */
    public static Boolean isValidGenericDateInput(String localDate) {
        try {
            LocalDate.parse(localDate);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Returns true if the given date of death is greater than or equal to the given date of birth and less than or
     * equal to today's date.
     *
     * @param dateOfDeath      The date of death to be validated.
     * @param dateOfBirth      The date of birth to be used in validating the date of death.
     * @param isHospitalChosen The date of death must have an associated hospital
     * @param hour             The hour of death
     * @param min              The Minute of death
     * @param sec              The Second of death
     * @return True is valid, false otherwise.
     */
    public static Boolean isValidDateOfDeath(LocalDate dateOfDeath, LocalDate dateOfBirth, boolean isHospitalChosen, String hour, String min, String sec) {
        boolean isValid = false;
        try {
            final LocalDate DEFAULT_VALUE = null;
            if (dateOfDeath == DEFAULT_VALUE && !isHospitalChosen) {
                isValid = true; // Optional field can be null which is the default value.
            }

            LocalDate currentDate = LocalDate.now();
            if (dateOfBirth != DEFAULT_VALUE && isHospitalChosen && dateOfDeath != DEFAULT_VALUE) {
                isValid = ((dateOfDeath.compareTo(dateOfBirth) >= 0) && (dateOfDeath.compareTo(currentDate) <= 0));
            }
            if (isValidHourTimeOfDeath(hour) && isValidMinuteTimeOfDeath(min) && isValidSecondTimeOfDeath(sec)) {
                isValid = true;
            }

        } catch (DateTimeParseException e) {
            LOGGER.severe(e.getMessage());
        }
        return isValid;
    }

    /**
     * Returns true if the given blood pressure is valid against the BLOOD_PRESSURE_REGEX constant, false otherwise.
     *
     * @param bloodPressure The blood pressure to be check for validity.
     * @return True if valid, false otherwise.
     */
    public static Boolean isValidBloodPressure(String bloodPressure) {
        return bloodPressure.matches(BLOOD_PRESSURE_REGEX);
    }

    /**
     * Returns true if the String representation of the gender is associated with a gender enum or if the gender value
     * has the default value of the ComboBox.
     *
     * @param genderObj The representation of the gender.
     * @return True if the gender is valid or is the default value, false if it does not exist.
     */
    public static Boolean isValidGender(Object genderObj) {

        String genderStr = (String) genderObj;

        try {
            GenderController.getGender(genderStr);
            return true; // Did not throw exception.
        } catch (DoesNotExist doesNotExist) {
            return false;
        }
    }
}
