package seng302.Utilities;

import javafx.beans.value.ObservableValue;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.util.Callback;
import seng302.Enum.OrganEnum;

import java.util.List;

/**
 * Custom CheckBoxListCell is used to highlight the cell when there are clashes between the donor/receiver organs.
 */
public class CustomCheckboxListCell extends CheckBoxListCell<OrganEnum> {

    private List<OrganEnum> organsToHighLight;

    public CustomCheckboxListCell(Callback<OrganEnum, ObservableValue<Boolean>> getSelectedProperty,
                                  List<OrganEnum> organsToHighlight) {
        super(getSelectedProperty);
        this.organsToHighLight = organsToHighlight;
    }

    /**
     * Will display the name of the organ as well as making the cell highlighted if there are clashes.
     *
     * @param item  The OrganEnum item that will populate the cell
     * @param empty Boolean to indicate if the cell contains an item or not
     */
    @Override
    public void updateItem(OrganEnum item, boolean empty) {
        super.updateItem(item, empty);
        if (item == null || empty) {
            setItem(null);
        } else {
            setText(item.toString());
            if (organsToHighLight.contains(item)) {
                setStyle("-fx-background-color: orange");
            }
        }
    }
}
