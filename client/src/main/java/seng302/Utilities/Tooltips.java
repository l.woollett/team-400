package seng302.Utilities;

import javafx.scene.control.Tooltip;

public abstract class Tooltips {

    // Declaring the text to be shown in Tooltips
    public final static String VALID_GENERIC_STRING_TOOLTIP_TEXT = "Valid String Entered";
    public final static String INVALID_GENERIC_STRING_TOOLTIP_TEXT = "String must be 1-20 characters long and only " +
            "contain alphabet characters, numbers, and underscores.\n" +
            "For example: MrFun69";

    public final static String VALID_USERNAME_TOOLTIP_TEXT = "Valid username entered";
    public final static String INVALID_USERNAME_TOOLTIP_TEXT = "Username must be 1-20 characters long and only " +
            "contain alphabet characters, numbers, and underscores.\n" +
            "For example: MrFun69";
    public final static String INVALID_USERNAME_NON_UNIQUE_TOOLTIP_TEXT =
            "That username is already taken. Please choose another one.";

    public final static String VALID_FIRST_NAME_TOOLTIP_TEXT = "Valid first name entered";
    public final static String INVALID_FIRST_NAME_TOOLTIP_TEXT = "First name must be 1-20 characters long and only " +
            "contain alphabet characters, hyphens, spaces, and apostrophes.\n" +
            "It must also start with an alphabet character.\n" +
            "For example: John";

    public final static String VALID_LAST_NAME_TOOLTIP_TEXT = "Valid last name entered";
    public final static String INVALID_LAST_NAME_TOOLTIP_TEXT = "Last name must be 1-40 characters long and only " +
            "contain alphabet characters, hyphens, spaces, and apostrophes.\n" +
            "It must also start with an alphabet character.\n" +
            "For example: O'Reily";

    public final static String VALID_HEIGHT_TOOLTIP_TEXT = "Valid height entered";
    public final static String INVALID_HEIGHT_TOOLTIP_TEXT = "Height is in meters and follows the format x.xx\n" +
            "For example: 1.76";

    public final static String VALID_WEIGHT_TOOLTIP_TEXT = "Valid weight entered";
    public final static String INVALID_WEIGHT_TOOLTIP_TEXT = "Weight format needs to be xxx.xx\n" +
            "For example: 130.50 or 80.75";


    public final static String VALID_ADDRESS_TOOLTIP_TEXT = "Valid address entered";
    public final static String INVALID_ADDRESS_TOOLTIP_TEXT = "Address must be 1-" + Validator.MAX_ADDRESS_LENGTH + " characters long and only " +
            "contain alphabet characters, digits, hyphens, spaces, and apostrophes.\n" +
            "It must also start with an alphabet character.\n" +
            "For example: 123 North-end Road";

    public final static String VALID_DATE_OF_BIRTH_TOOLTIP_TEXT = "Valid date of birth entered";
    public final static String INVALID_DATE_OF_BIRTH_TOOLTIP_TEXT = "Date of birth must be between 01/01/1900 and " +
            "today's date";

    public final static String VALID_DATE_OF_DEATH_TOOLTIP_TEXT = "Valid date of death entered";
    public final static String INVALID_DATE_OF_DEATH_TOOLTIP_TEXT =
            "Date of death must be between your date of birth " +
                    "and today's date. A hospital must also be selected.";

    public final static String VALID_BLOOD_PRESSURE_TOOLTIP_TEXT = "Valid blood pressure entered";
    public final static String INVALID_BLOOD_PRESSURE_TOOLTIP_TEXT =
            "Blood pressure must be follow the format: xxx/xxx\n" +
                    "For example: 86/105";

    public final static String VALID_MIDDLE_NAME_TOOLTIP_TEXT = "Valid middle name entered";
    public final static String INVALID_MIDDLE_NAME_TOOLTIP_TEXT = "Middle name must be 1-40 characters long and only " +
            "contain alphabet characters, hyphens, spaces, and apostrophes.\n" +
            "It must also start with an alphabet character.\n" +
            "For example: Patrick Matthews";

    public final static String VALID_ORGANISATION_TOOLTIP_TEXT = "Valid organisation entered";
    public final static String INVALID_ORGANISATION_TOOLTIP_TEXT = "Organisation must be 1-40 characters long and only " +
            "contain alphabet characters, hyphens, spaces, and apostrophes.\n" +
            "It must also start with an alphabet character.\n" +
            "For example: Organisation";

    public final static String VALID_STAFF_ID_TOOLTIP_TEXT = "Valid staff id entered";
    public final static String INVALID_STAFF_ID_TOOLTIP_TEXT = "Staff id must be below 99999 and also" +
            "contain integers only.\n" +
            "For example: 123";


    public final static String VALID_DISEASE_NAME_TOOLTIP_TEXT = "Valid disease name entered";
    public final static String INVALID_DISEASE_NAME_TOOLTIP_TEXT =
            "Disease name must start with a word character and only contain " +
                    "word characters and spaces.";

    public final static String VALID_DISEASE_DIAGNOSIS_DATE_TOOLTIP_TEXT = "Valid diagnosis date entered";
    public final static String INVALID_DISEASE_DIAGNOSIS_DATE_TOOLTIP_TEXT =
            "Disease diagnosis date between your date of " +
                    "birth and today's date";

    public final static String VALID_PROCEDURE_DATE_TOOLTIP_TEXT = "Valid procedure date entered";
    public final static String INVALID_PROCEDURE_DATE_TOOLTIP_TEXT =
            "Procedure date must be between your date of " +
                    "birth and today's date";

    public final static String VALID_ALIAS_TOOLTIP_TEXT = "Valid alias entered";
    public final static String INVALID_ALIAS_TOOLTIP_TEXT = "Alias must be 1-40 characters long and only " +
            "contain alphabet characters, hyphens, spaces, and apostrophes.\n" +
            "It must also start with an alphabet character.\n" +
            "For example: BooHoo";

    public final static String VALID_PASSWORD_TOOLTIP_TEXT = "Valid password entered.";
    public final static String INVALID_PASSWORD_TOOLTIP_TEXT = "Password must be 1-40 characters long and only " +
            "contain alphabet characters, numbers, and underscores.\n" +
            "For example: VerySecurePasswordOhYesInd33d";

    public final static String VALID_HR_TIME_OF_DEATH_TOOLTIP_TEXT = "Valid time of death hour entered.";
    public final static String INVALID_HR_TIME_OF_DEATH_TOOLTIP_TEXT = "Time of death hour must be between 0 to 23\n" +
            "For example: 19 means 7pm";

    public final static String VALID_MIN_TIME_OF_DEATH_TOOLTIP_TEXT = "Valid time of death minute entered.";
    public final static String INVALID_MIN_TIME_OF_DEATH_TOOLTIP_TEXT = "Time of death minute must be between 0 to 59";


    public final static String VALID_SEC_TIME_OF_DEATH_TOOLTIP_TEXT = "Valid time of death second entered.";
    public final static String INVALID_SEC_TIME_OF_DEATH_TOOLTIP_TEXT = "Time of death second must be between 0 to 59";


    /**
     * Creates and returns a Tooltip with the passed in text as it's value.
     *
     * @param text The text to be displayed in the tooltip.
     * @return The newly created Tooltip.
     */
    public static Tooltip createNewTooltip(String text) {
        Tooltip tool = new Tooltip();
        tool.setText(text);
        return tool;
    }
}