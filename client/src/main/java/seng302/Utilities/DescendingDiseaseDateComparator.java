package seng302.Utilities;

import seng302.Model.Disease;

import java.util.Comparator;

/**
 * It sorts the disease date
 */
public class DescendingDiseaseDateComparator implements Comparator<Disease> {
    public int compare(Disease o1, Disease o2) {
        return o2.getDateOfDiagnosis().compareTo(o1.getDateOfDiagnosis());
    }
}
