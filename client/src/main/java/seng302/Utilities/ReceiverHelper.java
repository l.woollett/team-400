package seng302.Utilities;

import seng302.Enum.OrganEnum;

import java.time.LocalDateTime;

public class ReceiverHelper {
    private OrganEnum registeredOrgan;
    private LocalDateTime dateRegistered;

    public ReceiverHelper(OrganEnum registeredOrgan, LocalDateTime dateRegistered) {
        this.registeredOrgan = registeredOrgan;
        this.dateRegistered = dateRegistered;
    }

    public OrganEnum getRegisteredOrgan() {
        return registeredOrgan;
    }

    public LocalDateTime getDateRegistered() {
        return dateRegistered;
    }
}
