package seng302.ServerInteracton.ServerQueries;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import seng302.Enum.OrganEnum;
import seng302.Model.AvailableOrganNotification;
import seng302.Model.Profile;

import java.util.List;

/**
 * Handles querying the server to sent a request/notification about a newly available organ at a hospital.
 */
public class AvailableOrganNotificationRequest extends Request {

    private static AvailableOrganNotificationRequest availableOrganNotificationRequest;

    private AvailableOrganNotificationRequest() {
        // Singleton
    }

    /**
     * Singleton method
     * @return the class
     */
    public static AvailableOrganNotificationRequest getInstance() {
        if (availableOrganNotificationRequest == null) {
            availableOrganNotificationRequest = new AvailableOrganNotificationRequest();
        }
        return availableOrganNotificationRequest;
    }


    /**
     * Sends an HTTP request to the server to store a request (for a newly available organ) to be stored on the
     * server.
     *
     * @param notification The request instance which contains information about the newly available organ.
     * @param token        The current clinician's token.
     * @return The response entity.
     * @throws HttpClientErrorException Error occurred during the Http request.
     */
    public ResponseEntity<Integer> sendNotification(AvailableOrganNotification notification, String token) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "notification";
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("X-Authorization", token);

        return restTemplate.exchange(serverUrl,
                HttpMethod.POST,
                new HttpEntity<>(notification, headers),
                Integer.class);
    }

    /**
     * Get all the notifications which are meant for the specified clinician.
     *
     * @param username The clinician's username who made the notification.
     * @param token    The clinician's token.
     * @return A collection of notifications which are meant for the clinician.
     * @throws HttpClientErrorException An error occurred during the HTTP GET request.
     */
    public ResponseEntity<List<AvailableOrganNotification>> getNotifications(String username, String token) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "notifications?clinician=" + username;
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("X-Authorization", token);

        return restTemplate.exchange(serverUrl,
                HttpMethod.GET,
                new HttpEntity<>(headers),
                new ParameterizedTypeReference<List<AvailableOrganNotification>>() {
                });
    }

    /**
     * Sends an Http request to update the notification. This will be used to update the notification's status.
     *
     * @param notification The notification to be saved.
     * @param token        The clinician's token.
     * @return The response entity.
     * @throws HttpClientErrorException Error occurred during the http request.
     */
    public ResponseEntity updateNotification(AvailableOrganNotification notification, String token) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "notification";
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("X-Authorization", token);

        return restTemplate.exchange(serverUrl,
                HttpMethod.PUT,
                new HttpEntity<>(notification, headers),
                AvailableOrganNotification.class);
    }
}