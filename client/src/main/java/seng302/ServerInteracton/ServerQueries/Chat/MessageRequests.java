package seng302.ServerInteracton.ServerQueries.Chat;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import seng302.Model.Chat.Message;
import seng302.Model.Chat.ConversationClinicianJoin;
import seng302.ServerInteracton.ServerQueries.Request;

import java.util.List;
import java.util.logging.Logger;

public class MessageRequests extends Request {

    private static final Logger LOGGER = Logger.getLogger(MessageRequests.class.getName());
    private static final String X_AUTH = "X-Authorization";
    private static MessageRequests messageRequests;

    private MessageRequests() {
        // Singleton
    }

    public static MessageRequests getInstance() {
        if (messageRequests == null) {
            messageRequests = new MessageRequests();
        }

        return messageRequests;
    }

    /**
     * Request to get all messages for a particular conversation.
     *
     * @param conversationId The id of the conversation that we need to get the messages for.
     * @param token          The auth token.
     * @return A ResponseEntity with a list of messages.
     * @throws HttpClientErrorException Thrown if an error occurs during the request.
     */
    public ResponseEntity<List<Message>> getMessages(int conversationId, String token) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "messages/" + conversationId;

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(X_AUTH, token);

        return restTemplate.exchange(
                serverUrl,
                HttpMethod.GET,
                new HttpEntity<>(conversationId, headers),
                new ParameterizedTypeReference<List<Message>>() {
                });
    }

    /**
     * Request to get all messages for a particular conversation.
     *
     * @param conversationId The id of the conversation that we need to get the messages for.
     * @param lastMessageId The ID of the last message retrieved, will get all messages that came after this.
     * @param token          The auth token.
     * @return A ResponseEntity with a list of messages.
     * @throws HttpClientErrorException Thrown if an error occurs during the request.
     */
    public ResponseEntity<List<Message>> getMessages(int conversationId, int lastMessageId, String token) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "messages/" + conversationId + "?id=" + lastMessageId;

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(X_AUTH, token);

        return restTemplate.exchange(
                serverUrl,
                HttpMethod.GET,
                new HttpEntity<>(conversationId, headers),
                new ParameterizedTypeReference<List<Message>>() {
                });
    }

    /**
     * Request to send a message and insert into the database.
     *
     * @param message The message that needs to be sent.
     * @param token   The auth token.
     * @return A ResponseEntity with a 200 if the message was sent correctly or an error code if there was a problem.
     * @throws HttpClientErrorException Thrown if there was an error in the request.
     */
    public ResponseEntity postMessage(Message message, String token) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "message";

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(X_AUTH, token);

        return restTemplate.exchange(
                serverUrl,
                HttpMethod.POST,
                new HttpEntity<>(message, headers),
                Message.class);
    }


    /**
     * Returns the Boolean of whether there are new messages in the DB.
     * @param convoJoinItem ConversationClinicianJoin holding the conversationId and clinicians username.
     * @param token String auth token for authorization.
     * @return ResponseEntity holding the Boolean result of the query and the status.
     * @throws HttpClientErrorException if the query broke somewhere.
     */
    public ResponseEntity<Boolean> newMessageCheck(ConversationClinicianJoin convoJoinItem, String token) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "messages/new" + "?clinicianUsername=" + convoJoinItem.getClinicianUsername() +
                "&conversationId=" + convoJoinItem.getConversationId();

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(X_AUTH, token);

        return restTemplate.exchange(
                serverUrl,
                HttpMethod.GET,
                new HttpEntity<>(headers),
                Boolean.class);
    }
}
