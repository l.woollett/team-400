package seng302.ServerInteracton.ServerQueries.Chat;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import seng302.Enum.ConversationStatusEnum;
import seng302.Model.Chat.ConversationClinicianJoin;
import seng302.Model.Chat.Conversation;
import seng302.ServerInteracton.ServerQueries.Request;

import java.util.List;

public class ConversationRequests extends Request {

    private static ConversationRequests conversationRequests;

    private static final String X_AUTH = "X-Authorization";

    private ConversationRequests() {
        // Singleton
    }

    public static ConversationRequests getInstance() {
        if (conversationRequests == null) {
            conversationRequests = new ConversationRequests();
        }

        return conversationRequests;
    }

    /**
     * Request to send to the server in order to create a conversation.
     *
     * @param conversation The conversation that needs to be sent to the server.
     * @param token The Auth token for the clinician.
     * @return ResponseEntity holding ID for the conversation
     * @throws HttpClientErrorException If the response is something bad like a 4XX.
     */
    public ResponseEntity<Integer> postConversation(Conversation conversation, String token) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "conversation";

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(X_AUTH, token);

        return restTemplate.exchange(
                serverUrl,
                HttpMethod.POST,
                new HttpEntity<>(conversation, headers),
                Integer.class);
    }

    /**
     * Gets all conversations the given clinician is involved in which also has the given status.
     *
     * @param username String Clinician's username.
     * @param status ConversationStatusEnum of the status of conversation required.
     * @param token Auth token.
     * @return List of conversation objects and a response status.
     * @throws HttpClientErrorException Thrown if there is an error with the request.
     */
    public ResponseEntity<List<Conversation>> getConversations(String username, ConversationStatusEnum status, String token) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "conversations?clinicianUsername=" + username + "&status=" + status.toString();

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(X_AUTH, token);

        return restTemplate.exchange(serverUrl,
                HttpMethod.GET,
                new HttpEntity<>(headers),
                new ParameterizedTypeReference<List<Conversation>>() {
                });
    }

    /**
     * Adds a list of clinicians (their username is needed) to a group chat.
     *
     * @param clinicians     The list of clinician usernames.
     * @param conversationId The conversation that the clinicians need to be added into.
     * @param token          The auth token.
     * @return A response entity with a 200 code if the clinicians were added to the conversation successfully.
     * An error code if there was an error with the request.
     * @throws HttpClientErrorException Thrown if there was an error with the request.
     */
    public ResponseEntity addParticipants(List<String> clinicians, int conversationId, String token) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "conversation/" + conversationId + "/addParticipants";

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(X_AUTH, token);
        ParticipantsWrapper participants = new ParticipantsWrapper(clinicians);

        return restTemplate.exchange(serverUrl,
                HttpMethod.POST,
                new HttpEntity<>(participants, headers),
                ParticipantsWrapper.class);
    }

    /**
     * Update the last read column of the conversationClinicianJoin table.
     * @param updateItem ConversationClinicianJoin holding the id and username of the clinician to edit in table.
     * @param token String auth token.
     * @return Response entity holding response status ie 200 OK.
     */
    public ResponseEntity updateLastRead(ConversationClinicianJoin updateItem, String token) {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "/conversations/lastRead/";

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("X-Authorization", token);

        return restTemplate.exchange(
                serverUrl,
                HttpMethod.PUT,
                new HttpEntity<>(updateItem, headers),
                ConversationClinicianJoin.class);
    }


    /**
     * Removes a list of clinicians (referenced by a list of their usernames) to be removed from a group chat.
     *
     * @param clinicianUsernames The clinicians to be removed, referenced by their usernames.
     * @param conversationId The unique identifier for the conversation.
     * @param token The logged in user's token.
     * @return Will have a 200 code if the HTTP request was successful.
     * @throws HttpClientErrorException There was an error with the Http request.
     */
    public ResponseEntity removeParticipant(List<String> clinicianUsernames, int conversationId, String token) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "/conversation/" + conversationId + "/removeParticipants";
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(X_AUTH, token);
        ParticipantsWrapper participantsWrapper = new ParticipantsWrapper(clinicianUsernames);

        return restTemplate.exchange(serverUrl, HttpMethod.DELETE,
                new HttpEntity<>(participantsWrapper, headers),
                ParticipantsWrapper.class);
    }

    /**
     * Sets a conversation to be inactive.
     *
     * @param conversationId The unique identifier for the conversation to be set to inactive.
     * @param token The current logged in user's token.
     * @return The response entity from the server with 200 code for success.
     * @throws HttpClientErrorException Error occurred during the HTTP request.
     */
    public ResponseEntity endConversation(int conversationId, String token) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "/conversation/" + conversationId + "/endConversation";
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(X_AUTH, token);

        return restTemplate.exchange(serverUrl, HttpMethod.PUT, new HttpEntity<>(headers), Object.class);
    }

    /**
     * Calls the query to get the unread conversations from the DB.
     * @param clinicianUsername String name of clinician.
     * @param token String token for authorization.
     * @return ResponseEntity.
     */
    public ResponseEntity<List<Integer>> getUnreadChats(String clinicianUsername, String token) {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "/conversation/unread/" + clinicianUsername;

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(X_AUTH, token);

        return restTemplate.exchange(serverUrl,
                HttpMethod.GET,
                new HttpEntity<>(headers),
                new ParameterizedTypeReference<List<Integer>>() {
                });
    }
}
