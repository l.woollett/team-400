package seng302.ServerInteracton.ServerQueries.Chat;

import java.util.List;

/**
 * Need this wrapper class so that SpringBoot can deserialize the list of clinician usernames.
 */
public class ParticipantsWrapper {

    private List<String> participants;

    public ParticipantsWrapper() {
        // Spring boot constructor
    }

    public ParticipantsWrapper(List<String> participants) {
        this.participants = participants;
    }

    public List<String> getParticipants() {
        return participants;
    }

    public void setParticipants(List<String> participants) {
        this.participants = participants;
    }
}
