package seng302.ServerInteracton.ServerQueries;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import seng302.Model.Clinician;
import seng302.Model.Hospital;

import java.util.List;

public class HospitalRequests extends Request {

    private static HospitalRequests hospitalRequests;

    private HospitalRequests() {
        // Singleton
    }

    public static HospitalRequests getInstance() {
        if (hospitalRequests == null) {
            hospitalRequests = new HospitalRequests();
        }

        return hospitalRequests;
    }

    /**
     * Get request for getting all of the hospitals from the server.
     *
     * @param token Auth token for the currently logged in user.
     * @return ResponseEntity with the status and the hospitals in the body.
     * @throws HttpClientErrorException An exception if there is an error with the client.
     */
    public ResponseEntity<List<Hospital>> getHospitals(String token) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "hospitals";
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("X-Authorization", token);

        return restTemplate.exchange(serverUrl,
                HttpMethod.GET,
                new HttpEntity<>(headers),
                new ParameterizedTypeReference<List<Hospital>>() {
                });
    }

    /**
     * Get request for getting the id of a hospital by using the name of the hospital.
     *
     * @param token        The authentication token.
     * @param hospitalName The name of the hospital that you want the id for.
     * @return The id of the hospital if successful, otherwise an error code.
     * @throws HttpClientErrorException If there was an error getting the hospital.
     */
    public ResponseEntity<Integer> getHospitalIdFromName(String token, String hospitalName) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        final String serverUrl = BASE_URL + "hospital?name=" + hospitalName;
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("X-Authorization", token);

        return restTemplate.exchange(serverUrl,
                HttpMethod.GET,
                new HttpEntity<>(headers),
                Integer.class);
    }

    /**
     * Get request for getting all the clinicians in the specified hospital.
     *
     * @param hospitalName The specificed hospital we want to get all the clinicians from.
     * @param token        The token of the currently logged in user.
     * @return List of all the clinicians.
     * @throws HttpClientErrorException Error occured during the HTTP GET request.
     */
    public ResponseEntity<List<Clinician>> getCliniciansAtHospital(String hospitalName, String token) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "hospital/" + hospitalName + "/clinicians";
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("X-Authorization", token);

        return restTemplate.exchange(serverUrl,
                HttpMethod.GET,
                new HttpEntity<>(headers),
                new ParameterizedTypeReference<List<Clinician>>() {
                });
    }
}
