package seng302.ServerInteracton.ServerQueries.Authentication;

/**
 * A singleton class for holding a user's authentication data. Due to it being a singleton, only one instance can exist
 * at a time.
 */
public class AuthToken {
    /**
     * used for tracking the current instance of AuthToken in existence. It is required for the singleton
     * implementation.
     */
    private static AuthToken authTokenInstance = null;
    /**
     * An authentication token.
     */
    private String token;
    /**
     * Represents the role of the user the token is associated with.
     */
    private Role role;

    /**
     * Constructs and AuthToken. This is constructor is required by spring boot for conversion after receiving a
     * response with an AuthToken in it.
     */
    private AuthToken() {
    }

    /**
     * Returns an instance of AuthToken. If one does not already exist then one is created and returned.
     *
     * @return An instance of AuthToken.
     */
    public static AuthToken getInstance() {
        if (authTokenInstance == null) {
            authTokenInstance = new AuthToken();
        }
        return authTokenInstance;
    }

    /**
     * Used for initialising the attributes of the AuthToken instance. If there is no current AuthToken instance then
     * nothing is set.
     *
     * @param token The authentication token to be set.
     * @param role  The role to be set.
     */
    public void init(String token, Role role) {
        if (authTokenInstance != null) {
            authTokenInstance.setToken(token);
            authTokenInstance.setRole(role);
        }
    }

    /**
     * Clears the currently stored AuthToken and it's details.
     */
    public void clearToken() {
        authTokenInstance = null;
    }

    /**
     * Gets the current token of the AuthToken.
     *
     * @return The current token of the AuthToken.
     */
    public String getToken() {
        return token;
    }

    /**
     * Sets the current authentication token of the AuthToken.
     *
     * @param token The new authentication token to be set.
     */
    private void setToken(String token) {
        this.token = token;
    }

    /**
     * Gets the current role of the AuthToken.
     *
     * @return The current role of the AuthToken.
     */
    public Role getRole() {
        return role;
    }

    /**
     * Sets the current role of the AuthToken.
     *
     * @param role The new role to be set.
     */
    private void setRole(Role role) {
        this.role = role;
    }
}