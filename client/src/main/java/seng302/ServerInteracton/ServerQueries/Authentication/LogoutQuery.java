package seng302.ServerInteracton.ServerQueries.Authentication;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import seng302.ServerInteracton.ServerQueries.Request;

/**
 * Client side request to the server endpoint for logout. In the request body, we set up token for this user.
 * The content-type is application/json.
 */
public class LogoutQuery extends Request {

    /**
     * Logs out the user with the given token if they are logged in currently.
     *
     * @param token String representing an authentication token.
     * @return ResponseEntity holding status of the request performed.
     * @throws HttpClientErrorException Error occurred during the request.
     */
    public ResponseEntity postLogout(String token) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "logout";

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("X-Authorization", token);

        return restTemplate.exchange(serverUrl,
                HttpMethod.POST,
                new HttpEntity<>(token, headers),
                String.class);
    }
}