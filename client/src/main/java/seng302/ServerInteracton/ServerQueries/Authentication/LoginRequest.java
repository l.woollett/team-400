package seng302.ServerInteracton.ServerQueries.Authentication;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import seng302.ServerInteracton.ServerQueries.Request;

public class LoginRequest extends Request {

    /**
     * Posts a login request to the server to log in the user with the given details.
     *
     * @param loginRequestBody Object representing the user's details for the login request.
     * @return A ResponseEntity with the result of the login, including the authorisation token.
     * @throws HttpClientErrorException If any error occurred during the request.
     */
    public ResponseEntity<AuthToken> postLogin(LoginRequestBody loginRequestBody) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "login";

        return restTemplate.exchange(serverUrl,
                HttpMethod.POST,
                new HttpEntity<>(loginRequestBody),
                AuthToken.class);
    }
}
