package seng302.ServerInteracton.ServerQueries.Authentication;

public enum Role {
    ADMIN,
    CLINICIAN,
    USER // The basic user of the application (donor/receiver)
}
