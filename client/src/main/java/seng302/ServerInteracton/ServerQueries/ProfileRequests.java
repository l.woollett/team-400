package seng302.ServerInteracton.ServerQueries;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import seng302.Enum.OrganEnum;
import seng302.Model.OrganMatchQueryResult;
import seng302.Model.Profile;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Requests for profile related end points.
 */
public class ProfileRequests extends Request {
    private static final String X_AUTH = "X-Authorization";
    private static ProfileRequests profileRequestsInstance;

    private ProfileRequests() {
        // Singleton
    }

    public static ProfileRequests getInstance() {
        if (profileRequestsInstance == null) {
            profileRequestsInstance = new ProfileRequests();
        }
        return profileRequestsInstance;
    }

    /**
     * Function to call to get the transplant waiting list from the server
     *
     * @param token  The user's token that is calling the function (Admin / Clinician)
     * @param organ  The organ to be searched for
     * @param region The region to be searched for
     * @return A response entity where the body will be an arraylist of profiles
     * @throws HttpClientErrorException Thrown if there was an error with the request.
     */
    public ResponseEntity<ArrayList<Profile>> getTransplantProfiles(String token, String organ, String region) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "waiting_list";
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(X_AUTH, token);

        if (organ != null || region != null) {
            serverUrl += "?";
            if (organ != null && region != null) {
                serverUrl += "organ=" + organ + "&";
            } else if (organ != null) {
                serverUrl += "organ=" + organ;
            }

            if (region != null) {
                serverUrl += "region=" + region.toUpperCase();
            }
        }


        return restTemplate.exchange(serverUrl,
                HttpMethod.GET, new HttpEntity<>(headers), new ParameterizedTypeReference<ArrayList<Profile>>() {
                });
    }

    /**
     * Hands an admin to the server to be created in the DB.
     *
     * @param profile Profile to be created.
     * @return response ResponseEntity holding a status of the request.
     * @throws HttpClientErrorException Error occurred during the request.
     */
    public ResponseEntity postProfile(Profile profile) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "profile";

        return restTemplate.exchange(serverUrl,
                HttpMethod.POST,
                new HttpEntity<>(profile),
                Profile.class);
    }

    /**
     * Gets a single profile from a server.
     *
     * @param username Username of the profile to retrieve from the database.
     * @param token    String authentication token.
     * @return response ResponseEntity holding a profile and the status of the request.
     * @throws HttpClientErrorException Error occurred during the request.
     */
    public ResponseEntity<Profile> getProfile(String username, String token) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "profile/" + username;

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(X_AUTH, token);

        return restTemplate.exchange(serverUrl,
                HttpMethod.GET,
                new HttpEntity<>(headers),
                Profile.class);
    }

    /**
     * Sends a PUT request to the server that updates the Profile.
     *
     * @param editedProfile Profile that needs to be updated.
     * @param token         Authentication token.
     * @return ResponseEntity that holds the status of the request.
     * @throws HttpClientErrorException thrown when an error occurred during the request.
     */
    public ResponseEntity putProfile(Profile editedProfile, String token) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "profile/" + editedProfile.getUsername();

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(X_AUTH, token);

        HttpEntity<Profile> entity = new HttpEntity<>(editedProfile, headers);

        return restTemplate.exchange(serverUrl,
                HttpMethod.PUT,
                entity,
                Profile.class);
    }

    /**
     * Deletes a Profile from the server with the given username if they exist and authentication token is valid.
     *
     * @param username Username of the Profile to be deleted.
     * @param token    Authentication token.
     * @return ResponseEntity indicating the status of the delete request.
     */
    public ResponseEntity deleteProfile(String username, String token) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "profile/" + username;

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(X_AUTH, token);

        return restTemplate.exchange(serverUrl,
                HttpMethod.DELETE,
                new HttpEntity<>(username, headers),
                Profile.class);
    }

    /**
     * Returns a list of profiles from the DB that match the given parameters.
     *
     * @param token  String for authorization.
     * @param params Map of String pairs that map a query param to its value. eg. startIndex, 5
     * @return ResponseEntity holding a list of profiles.
     * @throws HttpClientErrorException Thrown if there was an error with the request.
     */
    public ResponseEntity<List<Profile>> searchProfiles(String token, Map<String, String> params) throws HttpClientErrorException {

        RestTemplate restTemplate = new RestTemplate();
        String urlWithQuery = formParams(params, "profiles", false);

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(X_AUTH, token);

        return restTemplate.exchange(urlWithQuery,
                HttpMethod.GET,
                new HttpEntity<>(headers),
                new ParameterizedTypeReference<List<Profile>>() {
                });
    }

    /**
     * Retrieves the size of the profiles table.
     *
     * @param token for Authorization
     * @return Integer object holding the size of the profiles table.
     * @throws HttpClientErrorException Thrown if there was an error with the request.
     */
    public ResponseEntity<Integer> profileSearchCount(String token, Map<String, String> params) throws HttpClientErrorException {

        RestTemplate restTemplate = new RestTemplate();
        String urlWithQuery = formParams(params, "profileCount", true);

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(X_AUTH, token);

        return restTemplate.exchange(urlWithQuery,
                HttpMethod.GET,
                new HttpEntity<>(headers),
                new ParameterizedTypeReference<Integer>() {
                });
    }

    /**
     * Forms the params for the search query to
     * be sent with the http request based on whether the count is needed.
     *
     * @param params Map of param value pairs
     * @param query  Endpoint required
     * @param count  boolean of whether to take the count or not.
     * @return String of formed query string.
     */
    private String formParams(Map<String, String> params, String query, boolean count) {
        String serverUrl = BASE_URL + query;
        String paramString = "?";
        boolean paramSet = false;

        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (paramSet) {
                paramString += "&" + entry.getKey() + "=" + entry.getValue();
            } else {
                paramString += entry.getKey() + "=" + entry.getValue();
                paramSet = true;
            }
        }

        if (count) {
            if (paramSet) {
                paramString += "&getSize=1";
            } else {
                paramString += "getSize=1";
            }
        }
        return serverUrl + paramString;
    }

    /**
     * Gets username, hospital of everyone who matches the donor's age, organ and blood type.
     *
     * @param token    The user's token
     * @param username The username of the donor
     * @param organ    The organ we want to search for
     * @return An arraylist of OrganMatchQueryResults
     * @throws HttpClientErrorException Thrown if there was an error with the request.
     */
    public ResponseEntity<ArrayList<OrganMatchQueryResult>> getMatchingProfiles(String token, String username, String organ) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String url = BASE_URL + "organs/?username=" + username + "&organ=" + organ;
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(X_AUTH, token);

        return restTemplate.exchange(url,
                HttpMethod.GET,
                new HttpEntity<>(headers),
                new ParameterizedTypeReference<ArrayList<OrganMatchQueryResult>>() {
                });
    }

    /**
     * Request for deleting an organ from a user's donating organs.
     *
     * @param token    The auth token of the user
     * @param username The username of the user who's organ is to be removed.
     * @param organ    The organ which needs to be removed.
     * @return A ResponseEntity with a status code based on the success or failure of the request.
     * @throws HttpClientErrorException Thrown if there was an error in the request.
     */
    public ResponseEntity removeDonatingOrgan(String token, String username, OrganEnum organ) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String url = BASE_URL + "organ?username=" + username + "&organ=" + organ;
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add(X_AUTH, token);

        return restTemplate.exchange(url,
                HttpMethod.DELETE,
                new HttpEntity<>(headers),
                ResponseEntity.class);
    }

}