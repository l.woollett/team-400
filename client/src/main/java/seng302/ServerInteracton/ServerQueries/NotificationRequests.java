package seng302.ServerInteracton.ServerQueries;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import seng302.Model.AvailableOrganNotification;

import java.util.ArrayList;

public class NotificationRequests extends Request {
    private static NotificationRequests notificationRequests;

    public static NotificationRequests getInstance() {
        if (notificationRequests == null) {
            notificationRequests = new NotificationRequests();
        }
        return notificationRequests;
    }

    private NotificationRequests() {
        // Singleton
    }

    /**
     * Function to call to get a not yet respond incoming notification list from the server
     *
     * @param token             The user's token that is calling the function (Clinician)
     * @param clinicianUserName The clinician username to use for.
     * @return A response entity where the body will be an arraylist of availableOrganNotification
     * @throws HttpClientErrorException a httpError
     */
    public ResponseEntity<ArrayList<AvailableOrganNotification>> getAllIncomingNotifications(String token, String clinicianUserName) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "notification/" + clinicianUserName;


        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("X-Authorization", token);

        return restTemplate.exchange(serverUrl,
                HttpMethod.GET, new HttpEntity<>(headers), new ParameterizedTypeReference<ArrayList<AvailableOrganNotification>>() {
                });
    }

    /**
     * Edits a notification status.
     *
     * @param notification The notification that is being edited.
     * @param token        The auth token for user making the request.
     * @return A response entity with the status of the request.
     * @throws HttpClientErrorException Thrown if there was an error on the client side when getting the response back.
     */
    public ResponseEntity putAnIncomingNotification(AvailableOrganNotification notification, String token) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "notification";

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("X-Authorization", token);

        HttpEntity<AvailableOrganNotification> entity = new HttpEntity<>(notification, headers);
        return restTemplate.exchange(serverUrl,
                HttpMethod.PUT,
                entity,
                AvailableOrganNotification.class);
    }
}
