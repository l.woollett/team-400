package seng302.ServerInteracton.ServerQueries;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import seng302.Model.Admin;

import java.util.List;

/**
 * Will query the server for admin related end points.
 */
public class AdminRequests extends Request {

    private static AdminRequests adminRequests;

    private AdminRequests() {
        // Singleton
    }

    public static AdminRequests getInstance() {
        if (adminRequests == null) {
            adminRequests = new AdminRequests();
        }
        return adminRequests;
    }

    /**
     * Gets a single admin.
     *
     * @param username Username of the admin to retrieve from the database.
     * @return response ResponseEntity holding an admin and a status.
     * @throws HttpClientErrorException Error occurred during the request.
     */
    public ResponseEntity<Admin> getAdmin(String username, String token) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "admin/" + username;
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("X-Authorization", token);

        return restTemplate.exchange(serverUrl,
                HttpMethod.GET,
                new HttpEntity<>(headers),
                Admin.class);
    }

    /**
     * Uses a RestTemplate to request admins from the server application.
     *
     * @param token String authentication token.
     * @return response ResponseEntity holding a list of admins and a status.
     * @throws HttpClientErrorException Error occurred during the request.
     */
    public ResponseEntity<List<Admin>> getAdmins(String token) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "admins";
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("X-Authorization", token);

        return restTemplate.exchange(serverUrl,
                HttpMethod.GET,
                new HttpEntity<>(headers),
                new ParameterizedTypeReference<List<Admin>>() {
                });
    }

    /**
     * Hands an admin to the server to be created in the DB.
     *
     * @param admin Admin to be created.
     * @return response ResponseEntity holding a status of the request.
     * @throws HttpClientErrorException Error occurred during the request.
     */
    public ResponseEntity postAdmin(Admin admin) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "admin";

        return restTemplate.exchange(serverUrl,
                HttpMethod.POST,
                new HttpEntity<>(admin),
                Admin.class);
    }

    /**
     * Requests for the delete query to be executed on the given user.
     *
     * @param username String of admins username to remove.
     * @param token    String for authentication.
     * @return response ResponseEntity holding status of request performed.
     * @throws HttpClientErrorException Error occurred during the request.
     */
    public ResponseEntity deleteAdmin(String username, String token) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "admin/" + username;
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("X-Authorization", token);

        return restTemplate.exchange(serverUrl,
                HttpMethod.DELETE,
                new HttpEntity<>(headers),
                String.class
        );
    }
}
