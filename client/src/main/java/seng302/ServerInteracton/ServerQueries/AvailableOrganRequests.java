package seng302.ServerInteracton.ServerQueries;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import seng302.Model.AvailableOrgan;

import java.util.List;

public class AvailableOrganRequests extends Request {

    private static AvailableOrganRequests availableOrganRequests;

    /**
     * Singleton constructor
     *
     * @return AvailableOrganRequests either created or acquired.
     */
    public static AvailableOrganRequests getInstance() {
        if (availableOrganRequests == null) {
            availableOrganRequests = new AvailableOrganRequests();
        }
        return availableOrganRequests;
    }

    private AvailableOrganRequests() {
        // Singleton
    }

    /**
     * Requests the available organs to be donated from the hospital with the given ID.
     *
     * @param hospitalId String of the integer of hospital ID of the hospital in the DB.
     * @param token      String authorization token/.
     * @return ResponseEntity holding a list of available organs and response code for request.
     * @throws HttpClientErrorException If something goes wrong with the query.
     */
    public ResponseEntity<List<AvailableOrgan>> getAvailableOrgans(String hospitalId, String token) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "availableOrgans/" + hospitalId;

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("X-Authorization", token);

        return restTemplate.exchange(serverUrl,
                HttpMethod.GET,
                new HttpEntity<>(headers),
                new ParameterizedTypeReference<List<AvailableOrgan>>() {
                });
    }
}
