package seng302.ServerInteracton.ServerQueries;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import seng302.Model.Clinician;
import seng302.Model.Profile;

public class ClinicianRequests extends Request {

    private static ClinicianRequests clinicianRequests;

    private ClinicianRequests() {
        // Singleton
    }

    public static ClinicianRequests getInstance() {
        if (clinicianRequests == null) {
            clinicianRequests = new ClinicianRequests();
        }
        return clinicianRequests;
    }

    /**
     * Posts a clinician to the server to be created in the database.
     *
     * @param clinician Clinician to be added to the database.
     * @param token     Authentication token.
     * @return ResponseEntity with the status of the post request.
     * @throws HttpClientErrorException Error occurred during the request.
     */
    public ResponseEntity postClinician(Clinician clinician, String token) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "clinician";

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("X-Authorization", token);

        return restTemplate.exchange(serverUrl,
                HttpMethod.POST,
                new HttpEntity<>(clinician, headers),
                Clinician.class);
    }

    /**
     * Gets a Clinician from the server with the given username if they exist.
     *
     * @param username The username of the Clinician to get.
     * @param token    Authentication token.
     * @return ResponseEntity with the clinician from the server (If they exist).
     * @throws HttpClientErrorException Error occurred during the request.
     */
    public ResponseEntity<Clinician> getClinician(String username, String token) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "clinician/" + username;

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("X-Authorization", token);

        return restTemplate.exchange(serverUrl,
                HttpMethod.GET,
                new HttpEntity<>(username, headers),
                Clinician.class);
    }

    /**
     * Deletes a Clinician from the server with the given username if they exist and authentication token is valid.
     *
     * @param username Username of the Clinician to be deleted.
     * @param token    Authentication token.
     * @return ResponseEntity indicating the status of the delete request.
     * @throws HttpClientErrorException Error occurred during the request.
     */
    public ResponseEntity deleteClinician(String username, String token) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "clinician/" + username;

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("X-Authorization", token);

        return restTemplate.exchange(serverUrl,
                HttpMethod.DELETE,
                new HttpEntity<>(username, headers),
                Clinician.class);
    }


    /**
     * Posts a clinician to the server to be created in the database.
     *
     * @param clinician Clinician to be added to the database.
     * @param token     Authentication token.
     * @return ResponseEntity with the status of the post request.
     * @throws HttpClientErrorException Error occurred during the request.
     */
    public ResponseEntity putClinician(Clinician clinician, String token) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "clinician/" + clinician.getUsername();

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("X-Authorization", token);

        return restTemplate.exchange(serverUrl,
                HttpMethod.PUT,
                new HttpEntity<>(clinician, headers),
                Clinician.class);
    }


    /**
     * Requests the server to add link the profile to the given clinicians username via foreign key.
     *
     * @param clinicianName String clinicians username to be added to the foreign key of profile.
     * @param profile       Profile object that will be updated for in the DB.
     * @param token         String Auth token to be checked for valid permissions.
     * @return ResponseEntity holding the status of the query (ie HTTPStatus.OK)
     * @throws HttpClientErrorException If something goes wrong with the server interaction.
     */
    public ResponseEntity addPatient(String clinicianName, Profile profile, String token) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "clinician/linkPatient/" + clinicianName + "/";

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("X-Authorization", token);

        return restTemplate.exchange(serverUrl,
                HttpMethod.PUT,
                new HttpEntity<>(profile, headers),
                Profile.class);
    }

    /**
     * Requests the server to update the profiles hospital to the same as the clinicians.
     *
     * @param cliniciansUsername String to select the organisation value from.
     * @param profile            Profile to set the hospital value of.
     * @param token              String Auth token to be checked for valid permissions.
     * @return ResponseEntity holding the status of the query (ie HTTPStatus.OK)
     * @throws HttpClientErrorException If something goes wrong with the server interaction.
     */
    public ResponseEntity setOrganisation(String cliniciansUsername, Profile profile, String token) throws HttpClientErrorException {
        RestTemplate restTemplate = new RestTemplate();
        String serverUrl = BASE_URL + "clinician/organisation/" + cliniciansUsername + "/";

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("X-Authorization", token);

        return restTemplate.exchange(serverUrl,
                HttpMethod.PUT,
                new HttpEntity<>(profile, headers),
                Profile.class);
    }
}
