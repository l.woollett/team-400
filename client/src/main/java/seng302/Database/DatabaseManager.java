package seng302.Database;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Logger;


/**
 * A manager to execute SQL queries
 */
public class DatabaseManager {
    private static final String TEST_DB = "/seng302-2018-team400-test";
    private static final String PROD_DB = "/seng302-2018-team400-prod";
    private static final String URL_TO_CONNECT = "jdbc:mysql://132.181.16.98:3306";
    private static final String USERNAME = "seng302-team400";
    private static final String PASSWORD = "ToastierNoisome6820";
    private static final boolean IS_TEST_DATABASE = false;


    private Logger LOGGER = Logger.getLogger(getClass().getName());

    public DatabaseManager() {
        // Make sonarQube happy
    }

    /**
     * Chooses either test database or product database and returns the connection towards the chosen database
     *
     * @return the connection to either test or prod database, throws parameter exception if specified connection type is invalid.
     * @throws SQLException if connection can't be establish between program and db.
     */
    public Connection getConnection() throws SQLException {
        if (IS_TEST_DATABASE) {
            LOGGER.warning("Currently using the test database.");
            return DriverManager.getConnection(URL_TO_CONNECT + TEST_DB, USERNAME, PASSWORD);
        } else {
            return DriverManager.getConnection(URL_TO_CONNECT + PROD_DB, USERNAME, PASSWORD);
        }
    }
}
