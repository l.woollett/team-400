package seng302.API;

import seng302.Model.CachedItem;
import seng302.ModelController.CacheController;
import seng302.Utilities.DataInteraction;

import java.util.ArrayList;
import java.util.List;

/**
 * ApiCache Class to load from the cached api calls. If it doesnt find them, it will call the API
 */
public class ApiCache {

    /**
     * A direct replacement for the EhealthMe getDrugInteractions class. It will create a arraylist of params
     * which it will use to check against the cached interactions. If it doesnt find one, it will query the api
     * for one then add it to the cache
     *
     * @param drug1  The first drug to check
     * @param drug2  The second drug to check
     * @param gender The user's gender
     * @param age    The users age
     * @return An list of strings containing either the interactions between the two drugs or a string saying
     * that  no interactions were found.
     */
    public ArrayList<String> getDrugInteractions(String drug1, String drug2, String gender, Integer age) {
        ArrayList<String> interactions;
        List<String> params = new ArrayList<>();
        params.add(drug1);
        params.add(drug2);
        params.add(gender);
        params.add(String.valueOf(age));
        interactions = getCachedInteraction(params);

        if (interactions.isEmpty()) {
            interactions = EhealthMe.getDrugInteractions(drug1, drug2, gender, age);
            addCachedInteraction(interactions, params);
        }
        return interactions;
    }

    /**
     * A function to help with adding a interaction to the cache.
     *
     * @param interactions The interactions that have been found by the API
     * @param params       The user's params.
     */
    private void addCachedInteraction(ArrayList<String> interactions, List<String> params) {
        CachedItem toAdd = new CachedItem(interactions, params);
        CacheController.addItem(toAdd);
        DataInteraction.saveCache();
    }

    /**
     * Another helper function. This will either return null or a list of interactions
     *
     * @param params The user's params (Drug1, 2, age, gender)
     * @return The interactions or null
     */
    private ArrayList<String> getCachedInteraction(List<String> params) {
        return CacheController.getItem(params);
    }
}
