package seng302.API;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Logger;

/**
 * Class to handle interaction between the program and the mapi-us.iterar.co api
 */
public abstract class Mapi {

    private static final Logger LOGGER = Logger.getLogger(Mapi.class.getName());

    /**
     * @param sURL      the url to query
     * @param fieldname a json fieldname or null
     * @return an arraylist
     */
    private static ArrayList<String> getUrl(String sURL, String fieldname) {
        try {
            URL url = new URL(sURL);
            HttpURLConnection request = (HttpURLConnection) url.openConnection();
            request.setConnectTimeout(2000); //Setting these timeouts means that we will wait for a
            request.setReadTimeout(2000); //Maximum of 10 seconds total over both the connections for a error
            request.connect();

            // Convert to a GSON object to print data
            JsonParser jp = new JsonParser(); //from gson
            JsonElement root = jp.parse(new InputStreamReader(
                    (InputStream) request.getContent())); //Convert the input stream to a json element
            String suggestionlist;
            if (fieldname != null) {
                JsonObject rootobj = root.getAsJsonObject();
                suggestionlist = rootobj.get(fieldname).toString();
            } else {
                suggestionlist = root.toString();
            }
            suggestionlist = suggestionlist.replaceAll("\\[|\\]|\"", "");
            return new ArrayList<>(Arrays.asList(suggestionlist.split(",")));

        } catch (IOException e) {
            ArrayList<String> fail = new ArrayList<>();
            fail.add("");
            return fail;
        }

    }

    /**
     * Check what active ingredients a medication has
     *
     * @param toSearch is the Medicine to search for
     * @return ArrayList of active ingredients or null if there's an error
     */
    public static ArrayList<String> getIngredients(String toSearch) {
        String sURL = "http://mapi-us.iterar.co/api/" + toSearch + "/substances.json";
        if (medicineExists(toSearch)) {
            return getUrl(sURL, null);
        } else {
            return new ArrayList<>();
        }
    }

    /**
     * Check if the toSearch string of a medicine is in the result from the api query
     *
     * @param toSearch is the string to check if its a medicine or not
     * @return True or False. If true, medicine exists
     */

    public static Boolean medicineExists(String toSearch) {
        ArrayList<String> attempts = getAutoComplete(toSearch);
        try {
            return attempts.contains(toSearch);

        } catch (Exception e) {
            LOGGER.severe(e.getMessage());
            return false;
        }
    }

    /**
     * Replace space and percent signal by the end fromm the toSearch string, then pass this string to the api for
     * query
     *
     * @param toSearch The medicine string is going to query with the api
     * @return ArrayList: with items in it if successful, and an empty string as the only element if not successful
     */
    public static ArrayList<String> getAutoComplete(String toSearch) {
        toSearch = toSearch.replaceAll(" ", "%20");
        toSearch = toSearch.replaceAll("%", "&percent");
        if (toSearch.endsWith("%")) {
            toSearch = toSearch.substring(0, toSearch.length() - 1);
        }
        String sURL = "http://mapi-us.iterar.co/api/autocomplete?query=" + toSearch;
        return getUrl(sURL, "suggestions");
    }
}
