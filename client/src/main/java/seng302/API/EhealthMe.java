package seng302.API;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;
import java.util.logging.Logger;

/**
 * This API is to get interactions between two drugs.
 */
public abstract class EhealthMe {

    private static final Logger LOGGER = Logger.getLogger(EhealthMe.class.getName());

    /**
     * Gets the data from a url as a JSON Object.
     *
     * @param sURL The completed url to get data from
     * @return A Json Object with all the web page data
     */
    private static JsonObject getURL(String sURL) {
        try {
            URL url = new URL(sURL);
            HttpURLConnection request = (HttpURLConnection) url.openConnection();
            request.setConnectTimeout(2000); //Setting these timeouts means that we will wait for a
            request.setReadTimeout(2000); //Maximum of 2 seconds total over both the connections for a error
            request.connect();
            Integer res = request.getResponseCode();
            if (res != 200) {
                return null;
            }

            JsonParser jp = new JsonParser(); //from GSON
            return jp.parse(new InputStreamReader((InputStream) request.getContent()))
                    .getAsJsonObject();

        } catch (IOException e) {
            LOGGER.severe(e.getMessage());
            return null;
        }
    }

    /**
     * @param drug1  - The first drug to be compared.
     * @param drug2  - The other drug to be compared
     * @param gender - The users gender
     * @param age    - The users age.
     * @return An ArrayList of strings relating to the interactions that the drugs have with each other.
     */
    public static ArrayList<String> getDrugInteractions(String drug1, String drug2, String gender, Integer age) {
        ArrayList<String> toReturn = new ArrayList<>();
        String sURL = "https://www.ehealthme.com/api/v1/drug-interaction/" + drug1 + "/" + drug2 + "/";
        JsonObject toParse = getURL(sURL);
        if (toParse == null) {
            toReturn.add("Error. No drug interactions");
            return toReturn;
        }
        toReturn.addAll(parseInteractions(toParse, gender, age));
        toReturn.sort(String.CASE_INSENSITIVE_ORDER);
        return toReturn;
    }

    /**
     * @param toClean is the string that needs to be cleaned.
     * @return A string without quote marks and with the first letter caps'ed
     */
    private static String cleanString(String toClean) {
        String toReturn = toClean.replaceAll("\"|\'", "");
        toReturn = toReturn.substring(0, 1).toUpperCase() + toReturn.substring(1);
        return toReturn;
    }


    /**
     * @param toParse A GSON object that needs to be parsed into interactions
     * @param gender  The users gender
     * @param age     The users age
     * @return A set of strings that are the interactions the drug has with the user.
     */
    private static Set<String> parseInteractions(JsonObject toParse, String gender, Integer age) {
        Set<String> toReturn = new HashSet<>();
        JsonElement ageInteraction = toParse.get("age_interaction");
        Set<Map.Entry<String, JsonElement>> keys = ageInteraction.getAsJsonObject().entrySet();
        for (Map.Entry<String, JsonElement> s : keys) {
            String key = s.getKey();
            if (key.equals("60+")) key = "60-1000"; //Just making inputs match the way we're doing things.
            else if (key.equals("nan")) key = "0-1000";
            //Unknown if nan means no age or all ages. Need to confirm with PO.

            List<String> toCompare = Arrays.asList(key.split("-"));
            if (age > Integer.parseInt(toCompare.get(0)) && age < Integer.parseInt(toCompare.get(1))) {
                for (JsonElement interaction : s.getValue().getAsJsonArray()) {
                    toReturn.add(cleanString(interaction.toString()));
                }
            }
        }
        //Gender interaction, strings passed into this must be lowercase male, female, or else
        //The function will give back the interactions for both.
        JsonElement genderInteraction = toParse.get("gender_interaction");
        if (gender.equals("MALE") || gender.equals("FEMALE")) {
            JsonElement genderInteractions = genderInteraction.getAsJsonObject().get(gender.toLowerCase());
            for (JsonElement interaction : genderInteractions.getAsJsonArray()) {
                toReturn.add(cleanString(interaction.toString()));
            }
        } else {
            JsonElement maleInteractions = genderInteraction.getAsJsonObject().get("male");
            JsonElement femaleInteractions = genderInteraction.getAsJsonObject().get("female");
            for (JsonElement interaction : maleInteractions.getAsJsonArray()) {
                toReturn.add(cleanString(interaction.toString()));
            }
            for (JsonElement interaction : femaleInteractions.getAsJsonArray()) {
                toReturn.add(cleanString(interaction.toString()));
            }
        }
        //Duration interactions. We check through the list we've compiled about users age and
        //Gender, and all ones that intersect with a duration, we replace the base string with the
        //String with the duration. A problem with this is for things that have multiple durations,
        //But this will show the longest duration.
        JsonElement durationInteraction = toParse.get("duration_interaction");
        Set<Map.Entry<String, JsonElement>> durationKeys = durationInteraction.getAsJsonObject().entrySet();
        for (Map.Entry<String, JsonElement> iter : durationKeys) {
            for (JsonElement interaction_element : iter.getValue().getAsJsonArray()) {
                String interaction = cleanString(interaction_element.toString());
                if (toReturn.contains(interaction)) {
                    toReturn.remove(interaction);
                    if (iter.getKey().equals("not specified")) toReturn.add(interaction);
                    else toReturn.add(interaction + " (" + iter.getKey() + ")");
                }
            }
        }
        return toReturn;
    }
}