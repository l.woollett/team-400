package seng302.API;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import seng302.Model.Hospital;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Will query Google's Distance Matrix API to get information about the distance and duration between two hospitals.
 */
public class GoogleDistanceMatrix {


    private static final String BASE_URL = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&";

    public double getDuration(Hospital origin, Hospital destination) throws IOException {

        URL url = new URL(BASE_URL + "origins=" + origin.getLatitude() + "," + origin.getLongitude() + "&destinations=" +
                destination.getLatitude() + "," + destination.getLongitude() + "&key=AIzaSyB4pl6ve_wkVh3aow66r8OvxVqW0Eqwf1U");
        HttpURLConnection request = (HttpURLConnection) url.openConnection();
        request.setConnectTimeout(2000);
        request.setReadTimeout(2000);
        request.connect();

        JsonParser jp = new JsonParser();
        JsonObject jsonElement = jp.parse(new InputStreamReader((InputStream) request.getContent())).getAsJsonObject();

        return jsonElement.get("rows").getAsJsonArray().get(0)
                .getAsJsonObject().get("elements")
                .getAsJsonArray().get(0)
                .getAsJsonObject().get("duration")
                .getAsJsonObject().get("value")
                .getAsDouble();
    }
}
