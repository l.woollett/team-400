package seng302.ModelController;

import seng302.CustomException.DoesNotExist;
import seng302.Model.Admin;

import java.util.ArrayList;
import java.util.HashMap;


public class AdminController {

    public static final String DEFAULT_ADMIN_USERNAME = "admin";
    public static final String DEFAULT_ADMIN_PASSWORD = "admin";

    // The admin is identified by their username
    private static HashMap<String, Admin> admins = new HashMap<>();

    /**
     * Will check to see if there is a default admin in the system.
     *
     * @return True if the default admin exists, false otherwise.
     */
    public static boolean hasDefaultAdmin() {
        return UserController.isUsernameUsed(DEFAULT_ADMIN_USERNAME);
    }

    /**
     * The default admin does not exist, add the default admin. The check to see if the default admin
     * is not made in this function.
     */
    public static void addDefaultAdmin() {
        String username = DEFAULT_ADMIN_USERNAME;
        String password = DEFAULT_ADMIN_PASSWORD;

        Admin defaultAdmin = new Admin(username, password);

        addAdmin(username, defaultAdmin);
    }

    /**
     * Will clear the collection containing the admins.
     */
    public static void clearAdmins() {
        admins.clear();
    }


    /**
     * Will add a generic admin. This should not be used to add the default admin. Use
     * AdminSceneController.addDefaultAdmin(); instead
     *
     * @param username The admin's username.
     * @param admin    The admin that is to be added.
     */
    public static void addAdmin(String username, Admin admin) {
        admins.put(username, admin);
    }

    /**
     * Return the admin that has the specified username.
     *
     * @param username The username that identifies a admin.
     * @return If the admin exists, then they are returned.
     * @throws DoesNotExist The specified admin does not exist.
     */
    public static Admin getAdmin(String username) throws DoesNotExist {
        Admin admin = admins.get(username);

        if (admin == null) {
            throw new DoesNotExist("The specified admin does not exist.");
        } else {
            return admin;
        }
    }

    /**
     * Get all the admins that have been added to the system so far.
     *
     * @return All the admins as an ArrayList
     */
    public static ArrayList<Admin> getAdmins() {
        ArrayList<Admin> createdAdmins = new ArrayList<>();

        for (Admin admin : admins.values()) {
            createdAdmins.add(admin);
        }
        return createdAdmins;
    }

    /**
     * Set all the new admins.
     *
     * @param newAdmins The new admins.
     */
    public static void setAdmins(ArrayList<Admin> newAdmins) {
        admins.clear(); // Get rid of all the old ones

        for (Admin admin : newAdmins) {
            admins.put(admin.getUsername(), admin);
        }
    }

    /**
     * Removes the given admin from the HashMap.
     * Precondition that the user of this function will check it exists.
     *
     * @param username String of username key.
     * @return Admin object that has been removed.
     */
    public static Admin removeAdmin(String username) {
        Admin admin = admins.remove(username);
        return admin;
    }
}
