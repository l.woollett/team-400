package seng302.ModelController;

import seng302.CustomException.DoesNotExist;
import seng302.Enum.RegionEnum;

/**
 * Provides utility which revolves around the RegionEnum.Use final to restrict any extension/child class from it.
 */
public final class RegionTypeController {

    // Utility class does not need objects
    private RegionTypeController() {
    }


    /**
     * Return the RegionEnum that's associated with its string representation.
     *
     * @param regionType The region entered as a string.
     * @return The RegionEnum that represents the string's value.
     * @throws DoesNotExist Thrown if the string does not represent any of the blood type options.
     */
    public static RegionEnum getRegionType(String regionType) throws DoesNotExist {
        if (regionType.equalsIgnoreCase("auckland")) {
            return RegionEnum.AUCKLAND;
        } else if (regionType.equalsIgnoreCase("bay of plenty")) {
            return RegionEnum.BAY_OF_PLENTY;
        } else if (regionType.equalsIgnoreCase("canterbury")) {
            return RegionEnum.CANTERBURY;
        } else if (regionType.equalsIgnoreCase("gistourne")) {
            return RegionEnum.GISBOURNE;
        } else if (regionType.equalsIgnoreCase("hawke bay")) {
            return RegionEnum.HAWKES_BAY;
        } else if (regionType.equalsIgnoreCase("manawatu wanguanui")) {
            return RegionEnum.MANAWATU_WANGANUI;
        } else if (regionType.equalsIgnoreCase("marlborough")) {
            return RegionEnum.MARLBOROUGH;
        } else if (regionType.equalsIgnoreCase("nelson")) {
            return RegionEnum.NELSON;
        } else if (regionType.equalsIgnoreCase("northland")) {
            return RegionEnum.NORTHLAND;
        } else if (regionType.equalsIgnoreCase("otago")) {
            return RegionEnum.OTAGO;
        } else if (regionType.equalsIgnoreCase("southland")) {
            return RegionEnum.SOUTHLAND;
        } else if (regionType.equalsIgnoreCase("taranaki")) {
            return RegionEnum.TARANAKI;
        } else if (regionType.equalsIgnoreCase("west coast")) {
            return RegionEnum.WEST_COAST;
        } else if (regionType.equalsIgnoreCase("tasman")) {
            return RegionEnum.TASMAN;
        } else if (regionType.equalsIgnoreCase("waikato")) {
            return RegionEnum.WAIKATO;
        } else if (regionType.equalsIgnoreCase("wellingtion")) {
            return RegionEnum.WELLINGTION;
        } else {
            throw new DoesNotExist("The provided value does not represent a region type.");
        }

    }
}
