package seng302.ModelController;

import seng302.CustomException.DoesNotExist;
import seng302.Enum.BloodTypeEnum;

/**
 * Provides utility which revolves around the BloodTypeEnum.
 */
public abstract class BloodTypeController {

    /**
     * Return the BloodTypeEnum that's associated with its string representation.
     *
     * @param s The blood type entered as a string.
     * @return The BloodTypeEnum that represents the string's value.
     * @throws DoesNotExist Thrown if the string does not represent any of the blood type options.
     */
    public static BloodTypeEnum getBloodType(String s) throws DoesNotExist {
        switch (s) {
            case "A+":
                return BloodTypeEnum.A_POSITIVE;
            case "A-":
                return BloodTypeEnum.A_NEGATIVE;
            case "B+":
                return BloodTypeEnum.B_POSITIVE;
            case "B-":
                return BloodTypeEnum.B_NEGATIVE;
            case "O+":
                return BloodTypeEnum.O_POSITIVE;
            case "O-":
                return BloodTypeEnum.O_NEGATIVE;
            case "AB+":
                return BloodTypeEnum.AB_POSITIVE;
            case "AB-":
                return BloodTypeEnum.AB_NEGATIVE;
            default:
                throw new DoesNotExist("The provided value does not represent a blood type.");
        }

    }
}
