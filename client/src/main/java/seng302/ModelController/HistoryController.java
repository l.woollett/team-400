package seng302.ModelController;

import seng302.Enum.AlcoholConsumptionEnum;
import seng302.Enum.BloodTypeEnum;
import seng302.Enum.GenderEnum;
import seng302.Enum.OrganEnum;
import seng302.Model.Death;
import seng302.Model.Disease;
import seng302.Model.Hospital;
import seng302.Model.Procedure;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 * Class with methods that generate the changes made string for donor profile history
 */
public abstract class HistoryController {

    private static final Logger LOGGER = Logger.getLogger(HistoryController.class.getName());
    private static String description;

    /**
     * Generate a description for the change in the first name.
     *
     * @param changeTimeStamp The timestamp for when this change occurred.
     * @param oldName         The old name.
     * @param newName         The new name.
     * @return The summary of the change.
     */
    public static String createFirstNameChangeDescription(Date changeTimeStamp, String oldName, String newName) {
        if (newName.equals(UserController.FIRST_NAME_NOT_SET)) {
            // First name has been unset. This should not happen.
            LOGGER.severe(new Exception("The first name is mandatory should not have been set to be the empty string.").getMessage());
        } else if (oldName.equals(UserController.FIRST_NAME_NOT_SET)) {
            description = changeTimeStamp.toString() + ": " + "First name was set to " + newName;
        } else {
            description = changeTimeStamp.toString() + ": " + "Changed first name from " + oldName + " to " + newName;
        }
        return description;
    }

    /**
     * Generate a description for the change in the alias.
     *
     * @param changeTimeStamp The timestamp for when this change occurred.
     * @param oldName         The old middle name.
     * @param newName         The new middle name.
     * @return The summary of the change.
     */
    public static String createAliasChangeDescription(Date changeTimeStamp, String oldName, String newName) {
        if (oldName == null) {
            description = changeTimeStamp.toString() + ": " + "Alias was set to " + newName;
        } else if (newName.equals(UserController.ALIAS_NOT_SET)) {
            description = changeTimeStamp.toString() + ": " + "Alias was removed.";
        } else {
            description = changeTimeStamp.toString() + ": " + "Changed alias from " + oldName + " to " + newName;
        }
        return description;
    }


    /**
     * Generate a description for the change in the middle name.
     *
     * @param changeTimeStamp The timestamp for when this change occurred.
     * @param oldName         The old middle name.
     * @param newName         The new middle name.
     * @return The summary of the change.
     */
    public static String createMiddleNameChangeDescription(Date changeTimeStamp, String oldName, String newName) {
        if (newName.equals(UserController.MIDDLE_NAME_NOT_SET)) {
            description = changeTimeStamp.toString() + ": " + "Middle name was removed.";
        } else if (oldName.equals(UserController.MIDDLE_NAME_NOT_SET)) {
            description = changeTimeStamp.toString() + ": " + "Middle name was set to " + newName;
        } else {
            description = changeTimeStamp.toString() + ": " + "Changed middle name from " + oldName + " to " + newName;
        }
        return description;
    }

    /**
     * Generate a description for the change in the last name.
     *
     * @param changeTimeStamp The timestamp for when this change occurred.
     * @param oldName         The old last name.
     * @param newName         The new lat name.
     * @return The summary of the change.
     */
    public static String createLastNameChangeDescription(Date changeTimeStamp, String oldName, String newName) {
        if (newName.equals(UserController.MIDDLE_NAME_NOT_SET)) {
            description = changeTimeStamp.toString() + ": " + "Last name was removed.";
        } else if (oldName.equals(UserController.MIDDLE_NAME_NOT_SET)) {
            description = changeTimeStamp.toString() + ": " + "Last name was set to " + newName;
        } else {
            description = changeTimeStamp.toString() + ": " + "Changed last name from " + oldName + " to " + newName;
        }
        return description;
    }

    /**
     * Generate a description for the change in the date of birth.
     *
     * @param changeTimeStamp The timestamp for when this change occurred.
     * @param oldDob          The new date of birth.
     * @param newDob          The old date of birth.
     * @return The summary of the change.
     */
    public static String createDateOfBirthChangeDescription(Date changeTimeStamp, LocalDate oldDob, LocalDate newDob) {
        if (newDob == UserController.DATE_OF_BIRTH_NOT_SET) {
            description = changeTimeStamp.toString() + ": " + "Date of birth was removed.";
        } else if (oldDob == UserController.DATE_OF_BIRTH_NOT_SET) {
            description = changeTimeStamp.toString() + ": " + "Date of birth was set to " + newDob.toString();
        } else {
            description = changeTimeStamp.toString() + ": " + "Changed date of birth from " + oldDob.toString() + " to "
                    + newDob.toString();
        }
        return description;
    }

    //TODO: After the map, this method will be removed.

    /**
     * Generate a description for the change in the date of death.
     *
     * @param changeTimeStamp The timestamp for when this change occurred.
     * @param oldDod          The new date of death.
     * @param newDod          The old date of death.
     * @return The summary of the change.
     */
    @Deprecated
    public static String createDateOfDeathChangeDescription(Date changeTimeStamp, LocalDate oldDod, LocalDate newDod) {
        if (newDod == ProfileController.DATE_OF_DEATH_NOT_SET) {
            description = changeTimeStamp.toString() + ": " + "Date of death was removed.";
        } else if (oldDod == ProfileController.DATE_OF_DEATH_NOT_SET) {
            description = changeTimeStamp.toString() + ": " + "Date of death was set to " + newDod.toString();
        } else {
            description = changeTimeStamp.toString() + ": " + "Changed date of death from " + oldDod.toString() +
                    " to " + newDod.toString();
        }
        return description;
    }

    /**
     * Generate a description for the change in the gender.
     *
     * @param changeTimeStamp The timestamp for when this change occurred.
     * @param oldGender       The old gender.
     * @param newGender       The new gender.
     * @return The summary of the change.
     */
    public static String createGenderChangeDescription(Date changeTimeStamp, GenderEnum oldGender, GenderEnum newGender) {
        if (newGender == UserController.GENDER_NOT_SET) {
            description = changeTimeStamp.toString() + ": " + "Gender was removed.";
        } else if (oldGender == UserController.GENDER_NOT_SET) {
            description = changeTimeStamp.toString() + ": " + "Gender was set to " + newGender.toString();
        } else {
            description = changeTimeStamp.toString() + ": " + "Changed gender from " + oldGender.toString() + " to "
                    + newGender.toString();
        }
        return description;
    }

    /**
     * Generate a description for the change in the birth gender.
     *
     * @param changeTimeStamp The timestamp for when this change occurred.
     * @param oldGender       The old gender.
     * @param newGender       The new gender.
     * @return The summary of the change.
     */
    public static String createBirthGenderChangeDescription(Date changeTimeStamp, GenderEnum oldGender, GenderEnum newGender) {
        if (newGender == UserController.BIRTH_GENDER_NOT_SET) {
            description = changeTimeStamp.toString() + ": " + "Birth gender was removed.";
        } else if (oldGender == UserController.BIRTH_GENDER_NOT_SET) {
            description = changeTimeStamp.toString() + ": " + "Birth gender was set to " + newGender.toString();
        } else {
            description = changeTimeStamp.toString() + ": " + "Changed birth gender from " + oldGender.toString() + " to "
                    + newGender.toString();
        }
        return description;
    }

    /**
     * Generate a description for the change in the blood type.
     *
     * @param changeTimeStamp The timestamp for when this change occurred.
     * @param oldBloodType    The old blood type.
     * @param newBloodType    The new blood type.
     * @return The summary of the change.
     */
    public static String createBloodTypeChangeDescription(Date changeTimeStamp, BloodTypeEnum oldBloodType, BloodTypeEnum newBloodType) {
        if (newBloodType == ProfileController.BLOOD_TYPE_NOT_SET) {
            description = changeTimeStamp.toString() + ": " + "Blood type was removed.";
        } else if (oldBloodType == ProfileController.BLOOD_TYPE_NOT_SET) {
            description = changeTimeStamp.toString() + ": " + "Blood type was set to " + newBloodType.toString();
        } else {
            description = changeTimeStamp.toString() + ": " + "Blood type changed from " + oldBloodType.toString() + " to "
                    + newBloodType.toString();
        }
        return description;
    }


    /**
     * Generate a description for the change in the death details.
     *
     * @param changeTimeStamp The timestamp for when this change occurred.
     * @param oldDeath        The old death details.
     * @param newDeath        The new death details.
     * @return The summary of the change.
     */
    public static String createDeathChangeDescription(Date changeTimeStamp, Death oldDeath, Death newDeath) {
        if (newDeath == ProfileController.DEATH_NOT_SET) {
            description = changeTimeStamp.toString() + ": " + ": " + "Death was removed.";
        } else if (oldDeath == ProfileController.DEATH_NOT_SET) {
            description = changeTimeStamp.toString() + ": " + "Death was set to " + newDeath.toString();
        } else {
            description = changeTimeStamp.toString() + ": " + "Death changed from " + oldDeath.toString() + " to "
                    + newDeath.toString();
        }
        return description;
    }

    /**
     * Generate a description for the change in registration as an organ donor.
     *
     * @param changeTimeStamp The timestamp for when this change occurred.
     * @param newStatus       The new donor status.
     * @return A summary of the change.
     */
    public static String createDonorStatusChangeDescription(Date changeTimeStamp, boolean newStatus) {
        if (newStatus) {
            description = changeTimeStamp.toString() + ": " + "Registered as an organ donor";
        } else {
            description = changeTimeStamp.toString() + ": " + "Unregistered as an organ donor";
        }
        return description;
    }

    /**
     * Generate a description for the change in registration as an organ receiver.
     *
     * @param changeTimeStamp The timestamp for when this change occurred.
     * @param newStatus       The new donor status.
     * @return A summary of the change.
     */
    public static String createReceiverStatusChangeDescription(Date changeTimeStamp, boolean newStatus) {
        if (newStatus) {
            description = changeTimeStamp.toString() + ": " + "Registered as an organ receiver";
        } else {
            description = changeTimeStamp.toString() + ": " + "Unregistered as an organ receiver";
        }
        return description;
    }

    /**
     * Generate a description for the change in the list of organs to donate.
     *
     * @param changeTimeStamp The timestamp for when this change occurred.
     * @param newList         The new list of organs.
     * @return A summary of the changes.
     */
    public static String createOrganDonorListChangeDescription(Date changeTimeStamp, List<OrganEnum> newList) {
        int index = 0;
        if (!newList.isEmpty()) {
            description = changeTimeStamp.toString() + ": Changed the organs to donate to: ";
            for (OrganEnum organ : newList) {
                if (index + 1 == newList.size()) {
                    description += organ.toString() + ".";
                } else {
                    description += organ.toString() + ", ";
                }
                index++;
            }
        } else {
            description = changeTimeStamp.toString() + ": Removed all organs for donation";
        }
        return description;
    }

    /**
     * Function creates a history entry for when a receiving organ has been removed from the profile
     *
     * @param changeTimeStamp Date of when the removal occurred
     * @param removedOrgan    OrganEnum of the organ that has been removed
     * @param reasonValue     Additional reason
     * @return String description of the change, contains the date and organ that was removed
     */
    public static String createReceiverOrganRemovalDescription(Date changeTimeStamp, String removedOrgan,
                                                               String reasonValue) {
        String reason;
        if (reasonValue == null || "".equals(reasonValue.trim())) {
            reason = "";
        } else {
            reason = String.format(" (%s)", reasonValue);
        }

        description = changeTimeStamp.toString() + ": No longer registered to receive: " + removedOrgan + reason;
        return description;
    }

    public static String createReceiverOrganAdditionDescription(Date changeTimeStamp, OrganEnum newOrgan) {
        description = changeTimeStamp.toString() + ": Has been registered to receive: " + newOrgan.toString();
        return description;
    }

    /**
     * Function creates a history entry when a list level change is made for receiver organs
     *
     * @param changeTimeStamp LocalDate timestamp of when the change was made
     * @param newList         ArrayList of organs that have been registered to receive
     * @return String of the history description.
     */
    public static String createOrganReceiverListChangeDescription(Date changeTimeStamp, List<OrganEnum> newList) {
        int index = 0;
        if (!newList.isEmpty()) {
            description = changeTimeStamp.toString() + ": Changed the organs to receive to: ";
            for (OrganEnum organ : newList) {
                if (index + 1 == newList.size()) {
                    description += organ.toString() + ".";
                } else {
                    description += organ.toString() + ", ";
                }
                index++;
            }
        } else {
            description = changeTimeStamp.toString() + ": Removed all organs to be received";
        }
        return description;
    }

    /**
     * Generate a description for the change in the weight.
     *
     * @param changeTimeStamp The timestamp for when this change occurred.
     * @param oldValue        The old value for the weight.
     * @param newValue        The new value for the weight.
     * @return The summary of the change.
     */
    public static String createChangeWeightDescription(Date changeTimeStamp, float oldValue, float newValue) {

        if (newValue == ProfileController.WEIGHT_NOT_SET) {
            description = changeTimeStamp.toString() + ": " + "Weight was removed.";
        } else if (oldValue == ProfileController.WEIGHT_NOT_SET) { // First time the weight is being set.
            description = changeTimeStamp.toString() + ": Weight was set to " + newValue;
        } else {
            description = changeTimeStamp.toString() + ": Changed weight from " + oldValue + " to " + newValue;
        }
        return description;
    }

    /**
     * Generate a description for the change in the height.
     *
     * @param changeTimeStamp The timestamp for when this change occurred.
     * @param oldValue        The old height value.
     * @param newValue        The new height value.
     * @return The summary of the change.
     */
    public static String createChangeHeightDescription(Date changeTimeStamp, float oldValue, float newValue) {
        if (newValue == ProfileController.HEIGHT_NOT_SET) {
            description = changeTimeStamp.toString() + ": " + "Height was removed.";
        } else if (oldValue == ProfileController.HEIGHT_NOT_SET) { // First time the height is being set.
            description = changeTimeStamp.toString() + ": Height was set to " + newValue;
        } else {
            description = changeTimeStamp.toString() + ": Changed height from " + oldValue + " to " + newValue;
        }
        return description;
    }

    /**
     * Generate a description for the change in the smoker attribute.
     *
     * @param changeTimeStamp The timestamp the change occurred.
     * @param isSmoker        The new smoker status.
     * @return The summary of the change.
     */
    public static String createSmokerChangeDescription(Date changeTimeStamp, boolean isSmoker) {
        if (isSmoker) {
            description = changeTimeStamp.toString() + ": Changed smoker status to YES";
        } else {
            description = changeTimeStamp.toString() + ": Changed smoker status to NO";
        }
        return description;
    }

    /**
     * Generate a description for the change in the blood pressure attribute.
     *
     * @param changeTimeStamp  The timestamp the change occurred.
     * @param oldBloodPressure The old blood pressure value.
     * @param newBloodPressure The new blood pressure value.
     * @return The summary of the change.
     */
    public static String createBloodPressureChangeDescription(Date changeTimeStamp, String oldBloodPressure, String newBloodPressure) {
        if (newBloodPressure.equals(ProfileController.BLOOD_PRESSURE_NOT_SET)) {
            description = changeTimeStamp.toString() + ": " + "Blood pressure was removed.";
        } else if (oldBloodPressure.equals(ProfileController.BLOOD_PRESSURE_NOT_SET)) {
            description = changeTimeStamp.toString() + ": " + "Blood pressure was set to " + newBloodPressure;
        } else {
            description = changeTimeStamp.toString() + ": Changed blood pressure from " + oldBloodPressure + " to " + newBloodPressure;
        }
        return description;
    }

    /**
     * Generate a description for the change in alcohol consumption.
     *
     * @param changeTimeStamp The timestamp for the change occurred.
     * @param oldConsumption  The old blood pressure value.
     * @param newConsumption  The new blood pressure value.
     * @return The summary of the change.
     */
    public static String createAlcoholConsumptionChangeDescription(Date changeTimeStamp, AlcoholConsumptionEnum oldConsumption, AlcoholConsumptionEnum newConsumption) {
        if (newConsumption == null) {
            newConsumption = AlcoholConsumptionEnum.NOT_SET;
        }
        if (oldConsumption == null) {
            oldConsumption = AlcoholConsumptionEnum.NOT_SET;
        }
        if (newConsumption.equals(ProfileController.ALCOHOL_CONSUMPTION_NOT_SET)) {
            description = changeTimeStamp.toString() + ": Alcohol consumption was removed.";
        } else if (oldConsumption.equals(ProfileController.ALCOHOL_CONSUMPTION_NOT_SET)) {
            description = changeTimeStamp.toString() + ": Alcohol consumption was set to " + newConsumption;
        } else {
            description = changeTimeStamp.toString() + ": Updated alcohol consumption to " + newConsumption;
        }
        return description;
    }

    /**
     * Generate a description for the change in the address.
     *
     * @param changeTimeStamp The timestamp for when the change occurred.
     * @param oldAddress      The old address.
     * @param newAddress      The new address.
     * @return The summary of the change.
     */
    public static String createAddressChangeDescription(Date changeTimeStamp, String oldAddress, String newAddress) {
        if (newAddress.equals(UserController.ADDRESS_NOT_SET)) {
            description = changeTimeStamp.toString() + ": Address was removed.";
        } else if (oldAddress.equals(UserController.ADDRESS_NOT_SET)) {
            description = changeTimeStamp.toString() + ": Address was set to " + newAddress;
        } else {
            description = changeTimeStamp.toString() + ": Changed address to " + newAddress;
        }
        return description;
    }

    /**
     * Generate a description for the change in the region.
     *
     * @param changeTimeStamp The timestamp for when the change occurred.
     * @param oldRegion       The old address.
     * @param newRegion       The new address.
     * @return The summary of the change.
     */
    public static String createRegionChangeDescription(Date changeTimeStamp, String oldRegion, String newRegion) {
        if (newRegion.equals(String.valueOf(UserController.REGION_NOT_SET))) {
            description = changeTimeStamp.toString() + ": Region was removed.";
        } else if (oldRegion.equals(String.valueOf(UserController.REGION_NOT_SET))) {
            description = changeTimeStamp.toString() + ": Region was set to " + newRegion;
        } else {
            description = changeTimeStamp.toString() + ": Changed region from " + oldRegion + " to " + newRegion;
        }
        return description;
    }

    public static String addCurrentMedicationDescription(String medication) {
        Date now = new Date();
        description = now.toString() + ": Added " + medication + " into current medications";
        return description;
    }

    public static String moveFromCurrentToPreviousDescription(String medication) {
        Date now = new Date();
        description = now.toString() + ": Moved " + medication + " from current medications to previous medications.";
        return description;
    }

    public static String moveFromPreviousToCurrentDescription(String medication) {
        Date now = new Date();
        description = now.toString() + ": Moved " + medication + " from previous medications to current medications.";
        return description;
    }

    public static String deleteMedicationDescription(String medication) {
        Date now = new Date();
        description = now.toString() + ": Deleted " + medication + " from medications.";
        return description;
    }

    /**
     * Creates the change description for past diseases.
     *
     * @param pastDiseases An ArrayList of past diseases
     * @return the string of the description
     */
    public static String createPastDiseasesChangeDescription(List<Disease> pastDiseases) {
        Date now = new Date();
        if (!pastDiseases.isEmpty()) {
            description = now.toString() + ": Past diseases have been set to: ";
            for (Disease disease : pastDiseases) {
                description += disease.toString() + ", ";
            }
            description = description.replaceAll(", $", "");
        } else {
            description = now.toString() + ": All past diseases have been removed.";
        }
        return description;
    }

    /**
     * Creates the change description for current diseases
     *
     * @param currentDiseases An ArrayList of current diseases
     * @return the string of the description
     */
    public static String createCurrentDiseasesChangeDescription(List<Disease> currentDiseases) {
        Date now = new Date();
        if (!currentDiseases.isEmpty()) {
            description = now.toString() + ": Current diseases have been set to: ";
            for (Disease disease : currentDiseases) {
                description += disease.toString() + ", ";
            }
            description = description.replaceAll(", $", "");
        } else {
            description = now.toString() + ": All current diseases have been removed.";
        }
        return description;
    }

    /**
     * Creates the change description for current organisation
     *
     * @param changeTimeStamp The time which the changed occurred
     * @param newOrganisation The new organisation
     * @return the string of the description
     */
    public static String createOrganisationChangeDescription(Date changeTimeStamp, Hospital newOrganisation) {
        return changeTimeStamp.toString() + ": Changed organisation to " + newOrganisation.getName();
    }

    /**
     * Crates the change description for staff ID
     *
     * @param changeTimeStamp The time which the changed occurred
     * @param newStaffId      The new staff ID
     * @return the string of the description
     */
    public static String createStaffIdChangeDescription(Date changeTimeStamp, String newStaffId) {
        return changeTimeStamp.toString() + ": Changed staff ID to " + newStaffId;
    }

    /**
     * Create the change description for adding a pending procedure
     *
     * @param changeTimeStamp     The time that this change has occurred :o
     * @param newPendingProcedure The pending procedure
     * @return The string of the description
     */
    public static String addPendingProcedureChangeDescription(Date changeTimeStamp, Procedure newPendingProcedure) {
        String affectedOrgans = "None";
        List<String> organs = new ArrayList<>();

        for (OrganEnum organ : newPendingProcedure.getAffectedOrgans()) {
            organs.add(organ.toString());
        }

        if (!newPendingProcedure.getAffectedOrgans().isEmpty()) {
            affectedOrgans = String.join(", ", organs);
        }
        return changeTimeStamp.toString() + ": Added procedure: " + newPendingProcedure.getSummary() +
                ", scheduled for: " + newPendingProcedure.getDateOfProcedure().toString() + " organs affected: " +
                affectedOrgans;
    }

    /**
     * Create the change description for adding a past procedure
     *
     * @param changeTimeStamp  The time that this change has occurred :o
     * @param newPastProcedure The past procedure
     * @return The string of the description
     */
    public static String addPastProcedureChangeDescription(Date changeTimeStamp, Procedure newPastProcedure) {
        String affectedOrgans = "None";
        List<String> organs = new ArrayList<>();

        for (OrganEnum organ : newPastProcedure.getAffectedOrgans()) {
            organs.add(organ.toString());
        }

        if (!newPastProcedure.getAffectedOrgans().isEmpty()) {
            affectedOrgans = String.join(", ", organs);
        }
        return changeTimeStamp.toString() + ": Added procedure: " + newPastProcedure.getSummary() +
                ", happened on : " + newPastProcedure.getDateOfProcedure().toString() + " organs affected: " +
                affectedOrgans;
    }

    /**
     * Create the change description for updating a procedure
     *
     * @param changeTimeStamp  The time that this change has occurred :o
     * @param updatedProcedure The updated procedure
     * @return The string of the description
     */
    public static String updateProcedureChangeDescription(Date changeTimeStamp, Procedure updatedProcedure) {
        String affectedOrgans = "None";
        List<String> organs = new ArrayList<>();

        for (OrganEnum organ : updatedProcedure.getAffectedOrgans()) {
            organs.add(organ.toString());
        }

        if (!updatedProcedure.getAffectedOrgans().isEmpty()) {
            affectedOrgans = String.join(", ", organs);
        }
        return changeTimeStamp.toString() + ": Updated procedure: " + updatedProcedure.getSummary() +
                ", has a description of: " + updatedProcedure.getDescription() + ", date is now: " +
                ", organs that are affected: " + affectedOrgans;
    }


    /**
     * Create the change description for a deleted procedure
     *
     * @param changeTimeStamp  The time that this change has occurred :o
     * @param deletedProcedure The deleted procedure
     * @return The string of the description
     */
    public static String deleteProcedureChangeDescription(Date changeTimeStamp, Procedure deletedProcedure) {
        return changeTimeStamp.toString() + ": Removed procedure: " + deletedProcedure.getSummary();
    }

}
