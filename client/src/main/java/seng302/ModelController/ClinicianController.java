package seng302.ModelController;

import seng302.CustomException.DoesNotExist;
import seng302.Model.Clinician;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Will provide utility for all the clinicians.
 */
public class ClinicianController {

    private static final String DEFAULT_CLINICIAN_USERNAME = "defaultClinician";

    // The clinician is identified by their username
    private static HashMap<String, Clinician> clinicians = new HashMap<>();

    public static String getDefaultClinicianUsername() {
        return DEFAULT_CLINICIAN_USERNAME;
    }

    /**
     * Will check to see if there is a default clinician in the system.
     *
     * @return True if the default clinician exists, false otherwise.
     */
    public static boolean hasDefaultClinician() {
        return UserController.isUsernameUsed(DEFAULT_CLINICIAN_USERNAME);
    }

    /**
     * Clear the clinicians that are stored.
     */
    public static void clearClinicians() {
        clinicians.clear();
    }


    /**
     * Will add a generic clinician. This should not be used to add the default clinician. Use
     * ClinicianController.addDefaultClinician(); instead
     *
     * @param username  The clinician's username.
     * @param clinician The clinician that is to be added.
     */
    public static void addClinician(String username, Clinician clinician) {
        clinicians.put(username, clinician);

    }

    /**
     * Return the clinician that has the specified username.
     *
     * @param username The username that identifies a clinician.
     * @return If the clinician exists, then they are returned.
     * @throws DoesNotExist The specified clinician does not exist.
     */
    public static Clinician getClinician(String username) throws DoesNotExist {
        Clinician clinician = clinicians.get(username);

        if (clinician == null) {
            throw new DoesNotExist("The specified clinician does not exist.");
        } else {
            return clinician;
        }
    }

    /**
     * Get all the clinicians that have been added to the system so far.
     *
     * @return All the clinicians as an ArrayList
     */
    public static ArrayList<Clinician> getClinicians() {

        ArrayList<Clinician> createdClinicians = new ArrayList<>(clinicians.values());
        return createdClinicians;
    }

    /**
     * Set all the new clinicians.
     *
     * @param newClinicians The new clinicians.
     */
    public static void setClinicians(ArrayList<Clinician> newClinicians) {
        clinicians.clear(); // Get rid of all the old ones

        for (Clinician clinician : newClinicians) {
            clinicians.put(clinician.getUsername(), clinician);
        }
    }
}
