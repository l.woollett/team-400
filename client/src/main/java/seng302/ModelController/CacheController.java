package seng302.ModelController;

import seng302.Model.CachedItem;
import seng302.Storage.CacheStorage;
import seng302.Utilities.DataInteraction;

import java.util.ArrayList;
import java.util.List;

/**
 * Controller class for the Cache. We're just emulating the
 * other data storage models
 */
public class CacheController {
    private static ArrayList<CachedItem> cache = new ArrayList<>();

    /**
     * Getter for a single item, this is only called from ApiCache
     *
     * @param params The user's data
     * @return Either a null value or a arrayList of strings for the interactions between two drugs.
     */
    public static ArrayList<String> getItem(List<String> params) {
        for (CachedItem item : cache) {
            if (item.hasExpired()) {
                removeItem(item);
            } else if (item.matchParams(params)) {
                return item.getData();
            }
        }
        return new ArrayList<>();
    }

    /**
     * Adds an item to the cache
     *
     * @param toAdd A CachedItem to be added to the cache.
     */
    public static void addItem(CachedItem toAdd) {
        cache.add(toAdd);
    }

    /**
     * Returns the entire cache, used for saving to the JSON file
     *
     * @return a cache
     */
    public static ArrayList<CachedItem> getCache() {
        return cache;
    }

    /**
     * Sets the cache to cacheStorage
     *
     * @param cacheStorage The cache to be set to.
     */
    public static void setCache(CacheStorage cacheStorage) {
        cache = cacheStorage.getCache();
    }

    /**
     * A function used to remove items from the cache
     *
     * @param toRemove the CachedItem we want to remove
     */
    private static void removeItem(CachedItem toRemove) {
        cache.remove(toRemove);
    }

    /**
     * Empties the cache by setting it to an empty array list.
     */
    public static void emptyCache() {
        cache = new ArrayList<>();
        DataInteraction.saveCache();
    }
}
