package seng302.ModelController;

import com.google.gson.Gson;
import org.apache.commons.cli.*;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import seng302.CustomException.AlreadyExists;
import seng302.CustomException.DoesNotExist;
import seng302.CustomException.InvalidCommand;
import seng302.Enum.*;
import seng302.GUI.ControllerAccess;
import seng302.Model.*;
import seng302.ServerInteracton.ServerQueries.Authentication.AuthToken;
import seng302.ServerInteracton.ServerQueries.ClinicianRequests;
import seng302.ServerInteracton.ServerQueries.ProfileRequests;
import seng302.Utilities.DataInteraction;
import seng302.Utilities.Tooltips;
import seng302.Utilities.Validator;

import java.io.*;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.logging.Logger;

import static seng302.Enum.CommandEnum.*;


/**
 * Organises the all the commands that can be used in the application. Contains the code to execute all the various
 * commands with arguments and validate the provided arguments.<br>
 * <br><br>
 * To create new commands. Must first add them to the 'commands' HashMap which will specify what user input relates
 * to which CommandEnum. The CommandEnum will be used to identify what methods should be called to execute the
 * command entered by the user. The arguments for the command (i.e. options) must also be created. An Option will define
 * a single argument for the command, all relevant commands must be grouped into an Options object that will be used
 * to specify all the possible arguments that can be executed with a command. Options must be added to the 'arguments'
 * HashMap as this will be used to identify what options are needed when printing out a command's usage/'man page'.
 * Must also add the command in the checkArguments method so the entered arguments with the command can be validated.<br>
 * <br>
 * <br>
 * =============================================== L I C E N S E S =================================================<br>
 * <br>
 * Gson is used to to save and load objects from JSON files. <br>
 * <br>
 * Copyright 2008 Google Inc. <br>
 * <br>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at <br>
 * <br>
 * http://www.apache.org/licenses/LICENSE-2.0 <br>
 * <br>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. <br>
 * <br>
 * <br>
 * <br>
 * Commons CLI was used to parse the input from the command line, retrieve values for arguments and create the usage
 * documentation. <br>
 * <br>
 * Copyright 2017 Apache Commons. <br>
 * <br>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at <br>
 * <br>
 * http://www.apache.org/licenses/LICENSE-2.0 <br>
 * <br>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public abstract class CommandController {

    /**
     * System dependent line separator.
     */
    private static final String LINE_SEPARATOR = System.lineSeparator();
    private static final String USERNAME_ARG = "username";
    private static final String ALL_ARG = "all";
    //Arguments for the help command
    private static final String COMMAND_ARG = "command";
    //Arguments for creating a user profile (and can be reused for editing a user profile)
    private static final String FIRST_NAME_ARG = "firstName";
    private static final String MIDDLE_NAME_ARG = "middleName";
    private static final Option middleNameOpt = Option.builder().longOpt(MIDDLE_NAME_ARG).hasArg(true).argName("MIDDLE NAME").required(false)
            .desc("Specify their middle name.").build();
    private static final String LAST_NAME_ARG = "lastName";
    private static final String DOB_ARG = "dateOfBirth";
    private static final String GENDER_ARG = "gender";
    private static final String HEIGHT_ARG = "height";
    private static final String WEIGHT_ARG = "weight";
    private static final String BLOOD_TYPE_ARG = "bloodType";
    private static final String CURRENT_ADDRESS_ARG = "currentAddress";
    private static final String REGION_ARG = "region";
    private static final String IS_DONOR_ARG = "isDonor";
    private static final String IS_RECEIVER_ARG = "isReceiver";
    private static final String DONOR_ORGANS_ARG = "donorOrgans";
    private static final String RECEIVER_ORGANS_ARG = "receiverOrgans";
    private static final String IS_SMOKER_ARG = "isSmoker";
    private static final String BLOOD_PRESSURE_ARG = "bloodPressure";
    private static final String ALCOHOL_CONSUMPTION_ARG = "alcoholConsumption";
    private static final String CHRONIC_DISEASE_ARG = "chronicDisease";
    //Options used specifically for searching.
    private static final String USERNAME_SEARCH_ARG = "username";
    private static final String FIRST_NAME_SEARCH_ARG = "firstName";
    private static final String LAST_NAME_SEARCH_ARG = "lastName";
    private static final String IS_DONOR_SEARCH_ARG = "isDonor";
    private static final String ADD_DONOR_ORGAN_ARG = "addDonorOrgan";
    private static final String ADD_RECEIVER_ORGAN_ARG = "addReceiverOrgan";
    private static final String REMOVE_DONOR_ORGAN_ARG = "removeDonorOrgan";
    private static final Option removeDonorOrgan = Option.builder().longOpt(REMOVE_DONOR_ORGAN_ARG).hasArg(true).required(false)
            .argName("ORGAN TO REMOVE").desc("Specify one of the following organs, for donation, to remove: " + OrganController.getOrganList()).build();
    private static final String REMOVE_RECEIVER_ORGAN_ARG = "removeReceiverOrgan";
    private static final Option removeReceiverOrgan = Option.builder().longOpt(REMOVE_RECEIVER_ORGAN_ARG).hasArg(true).required(false)
            .argName("ORGAN TO REMOVE").desc("Specify one of the following organs, for a needed transplant, to remove: " + OrganController.getOrganList()).build();
    // Options specific to creating a clinician
    private static final String STAFF_ID_ARG = "id";
    //Generic message for when something related to a command or argument is invalid
    private static final String INVALID_ARGUMENT_MESSAGE = "Improper use of command.";
    //This should never actually be displayed, if this is shown then the user entered an argument
    //for a feature that has not been implemented yet.
    private static final String UNKNOWN_ARGUMENT_MESSAGE = "Unknown argument.";
    private static final String WRONG_LOCAL_DATE_FORMAT_MESSAGE = "Wrong format for date, use YYYY-MM-DD.";
    private static final String MISSING_ARGUMENT_MESSAGE = "Missing argument.";
    private static final String MISSING_FIRST_NAME_INPUT_MESSAGE = "Missing values for the first name, it is a mandatory field and cannot be left blank.";
    private static final int EXPECTED_NUM_ARGS_AFTER_SUCCESSFUL_PARSE = 0;
    private static final String NO_STRING_INPUT = "";
    //URI for files
    private static final String USER_DATA_FILE = "userdata.json";
    private static final String ACTION_HISTORY_FILE = "action_history.json";
    // Legacy issue. The application was originally built using the OS's native terminal but we have no moved onto using
    // a custom terminal made using JavaFX. This boolean is used to redirect all the outputs to the JavaFX terminal
    // instead of the native console.
    private static boolean isUsingJavaFxTerminal = false;
    private static Option usernameOpt = Option.builder().longOpt(USERNAME_ARG).hasArg(true).argName("USERNAME").type(String.class).required(true)
            .desc("Enter the profile's username. When creating, must be unique.").build();
    private static Option allOpt = Option.builder().longOpt(ALL_ARG).hasArg(false).required(true)
            .desc("Will display all the values.").build();
    private static Option commandOpt = Option.builder().longOpt(COMMAND_ARG).hasArg(true).argName("COMMAND").required(true)
            .desc("Specifies the specific command you want information on.").build();
    private static Option requiredFirstNameOpt = Option.builder().longOpt(FIRST_NAME_ARG).hasArg(true).argName("FIRST NAME").required(true)
            .desc("Specify their first name.").build();
    private static Option lastNameOpt = Option.builder().longOpt(LAST_NAME_ARG).hasArg(true).argName("LAST NAME").required(false)
            .desc("Specify their last name.").build();
    private static Option requiredDobOpt = Option.builder().longOpt(DOB_ARG).hasArg(true).required(true).argName("DATE OF BIRTH").type(LocalDate.class)
            .desc("Specify their date of birth. Enter as YYYY-MM-DD.").build();
    private static Option genderOpt = Option.builder().longOpt(GENDER_ARG).hasArg(true).argName("GENDER").required(false)
            .desc("Specify their gender, enter either M, F, or O. 'O' stands for other.").build();
    private static Option heightOpt = Option.builder().longOpt(HEIGHT_ARG).hasArg(true).argName("HEIGHT").required(false)
            .desc("Specify their height.").build();
    private static Option weightOpt = Option.builder().longOpt(WEIGHT_ARG).hasArg(true).argName("WEIGHT").required(false)
            .desc("Specify their weight.").build();
    private static Option bloodTypeOpt = Option.builder().longOpt(BLOOD_TYPE_ARG).hasArg(true).argName("BLOOD TYPE").required(false)
            .desc("Specify their blood type, enter one of the following: A+, A-, B+, B-, O+, O-, AB+, or AB-.").build();
    private static Option currentAddressOpt = Option.builder().longOpt(CURRENT_ADDRESS_ARG).hasArg(true).argName("CURRENT ADDRESS").required(false)
            .desc("Specify their current address.").build();
    private static Option regionOpt = Option.builder().longOpt(REGION_ARG).hasArg(true).argName("REGION").required(false)
            .desc("Specify their region.").build();
    private static Option isDonorOpt = Option.builder().longOpt(IS_DONOR_ARG).hasArg(true).argName("IS DONOR").type(Boolean.class).required(false)
            .desc("Specify if they are a donor. Enter either true or false.").build();
    private static Option isReceiverOpt = Option.builder().longOpt(IS_RECEIVER_ARG).hasArg(true).argName("IS RECEIVER").type(Boolean.class).required(false)
            .desc("Specify if they are a receiver. Enter either true or false.").build();
    private static Option donorOrganListOpt = Option.builder().longOpt(DONOR_ORGANS_ARG).hasArg(true).argName("ORGANS FOR DONATION").required(false)
            .desc("Specify what organs they want to donate. If this is done then they are automatically set as a donor. This field is optional." +
                    LINE_SEPARATOR +
                    "Enter the desired organs separated by a comma. Choices are : " + OrganController.getOrganList()).build();
    private static Option receiverOrganListOpt = Option.builder().longOpt(RECEIVER_ORGANS_ARG).hasArg(true).argName("NEEDED ORGANS").required(false)
            .desc("Specify what organs they need for a transplant. If this is done then they are automatically set as a receiver. This field is optional." +
                    LINE_SEPARATOR +
                    "Enter the desired organs separated by a coma. Choices are: " + OrganController.getOrganList()).build();
    private static Option isSmokerOpt = Option.builder().longOpt(IS_SMOKER_ARG).hasArg(true).argName("IS SMOKER").type(Boolean.class).required(false)
            .desc("Specify if they are a smoker. Enter either true or false.").build();
    private static Option bloodPressureOpt = Option.builder().longOpt(BLOOD_PRESSURE_ARG).hasArg(true).argName("BLOOD PRESSURE").desc("Specify the blood pressure as a number such a 80/120.").required(false).build();
    private static Option alcoholConsumptionOpt = Option.builder().longOpt(ALCOHOL_CONSUMPTION_ARG).hasArg(true).argName("ALCOHOL CONSUMPTION").desc("Specify how much the user drinks a week, being none, low, medium, high, extreme.").build();
    private static Option chronicDiseaseListOpt = Option.builder().longOpt(CHRONIC_DISEASE_ARG).hasArg(true).argName("CHRONIC DISEASES").required(false)
            .desc("Specify what chronic diseases the donor has." + LINE_SEPARATOR + "The options are: " +
                    java.util.Arrays.asList(ChronicDiseaseEnum.values())).build();
    private static Option optionalUsernameSearchOpt = Option.builder().longOpt(USERNAME_SEARCH_ARG).hasArg(true).argName("USERNAME")
            .required(false).desc("The USERNAME that will be searched for.").build();
    private static Option requiredUsernameSearchOpt = Option.builder().longOpt(USERNAME_SEARCH_ARG).hasArg(true).argName("USERNAME")
            .required(true).desc("The USERNAME that will be searched for.").build();
    private static Option firstNameSearchOpt = Option.builder().longOpt(FIRST_NAME_SEARCH_ARG).hasArg(true).
            argName(FIRST_NAME_SEARCH_ARG).required(false).desc("Search based on the entered first name of the user.").build();
    private static Option lastNameSearchOpt = Option.builder().longOpt(LAST_NAME_SEARCH_ARG).hasArg(true).argName("LAST NAME")
            .required(false).desc("Search based on the entered last name of the user.").build();
    private static Option isDonorSearchOpt = Option.builder().longOpt(IS_DONOR_SEARCH_ARG).hasArg(true).argName("IS DONOR")
            .type(Boolean.class).required(false)
            .desc("Specify if we want to search for donors or not. Enter true or false.").build();
    //Options that used for editing. The rest of the profile related arguments can be reused from the creating profile
    //command arguments.
    private static Option optionalFirstNameOpt = Option.builder().longOpt(FIRST_NAME_ARG).hasArg(true).argName("FIRST NAME").required(false)
            .desc("Specify their first name.").build();
    private static Option optionalDobOpt = Option.builder().longOpt(DOB_ARG).hasArg(true).required(false)
            .argName("DATE OF BIRTH").type(LocalDate.class).desc("Specify their date of birth. Enter as YYYY-MM-DD.").build();
    private static Option addDonorOrganOpt = Option.builder().longOpt(ADD_DONOR_ORGAN_ARG).hasArg(true).required(false)
            .argName("ORGAN TO ADD").desc("Specify one of the following organs, for donation, to add: " + OrganController.getOrganList()).build();
    private static Option addReceiverOrganOpt = Option.builder().longOpt(ADD_RECEIVER_ORGAN_ARG).hasArg(true).required(false)
            .argName("ORGAN TO ADD").desc("Specify one of the following organs, for a needed transplant, to add: " + OrganController.getOrganList()).build();
    private static Option clinicianStaffIdOpt = Option.builder().longOpt(STAFF_ID_ARG).hasArg(true).required(true)
            .argName("STAFF ID").desc("The clinician's staff ID within their organisation.").build();
    private static Option requiredRegionOpt = Option.builder().longOpt(REGION_ARG).hasArg(true).argName("REGION").required(true)
            .desc("Specify their region.").build();
    private static Option optionalClinicianStaffIdOpt = Option.builder().longOpt(STAFF_ID_ARG).hasArg(true).required(false)
            .argName("STAFF ID").desc("The clinician's staff ID within their organisation.").build();
    //All the final Options for each command
    private static Options createProfileOpts = buildCreateProfileOptions();
    private static Options helpOptions = buildHelpCommandOptions();
    private static Options displayProfileOpts = buildDisplayProfileOptions();
    private static Options editProfileOpts = buildEditProfileOptions();
    private static Options deleteProfileOpts = buildDeleteProfileOptions();
    private static Options createClinicianOpts = buildCreateClinicianOptions();
    private static Options editClinicianOpts = buildEditClinicianOptions();
    private static Options deleteClinicianOpts = buildDeleteClinicianOptions();
    //This HashMap will pair the String needed to be written into the console to execute its associated
    //command. The input is the key and the values is the associated enum constant for the command.
    private static HashMap<String, CommandEnum> commands = createCommandsHashMap();
    private static HashMap<CommandEnum, Options> arguments = createArgumentsHashMap();
    private static Logger LOGGER = Logger.getLogger(CommandController.class.getName());

    public static void setUsingJavaFxTerminal(boolean usingJavaFxTerminal) {
        isUsingJavaFxTerminal = usingJavaFxTerminal;
    }

    /**
     * Create the options (i.e. arguments) for the delete clinician command.
     *
     * @return The options used to delete a clinician account.
     */
    private static Options buildDeleteClinicianOptions() {
        Options options = new Options();
        options.addOption(requiredUsernameSearchOpt);
        return options;
    }

    /**
     * Create the options (i.e. arguments) for the delete profile command.
     *
     * @return The options used to delete a user's profile.
     */
    private static Options buildDeleteProfileOptions() {
        Options options = new Options();
        options.addOption(requiredUsernameSearchOpt);
        return options;
    }

    /**
     * Create the options (i.e. arguments) for the edit clinician command. The only mandatory command is the username
     * because we use it to search for the clinician.
     *
     * @return The options used to edit a clinician.
     */
    private static Options buildEditClinicianOptions() {
        Options options = new Options();
        options.addOption(usernameOpt);
        options.addOption(optionalClinicianStaffIdOpt);
        options.addOption(optionalFirstNameOpt);
        options.addOption(middleNameOpt);
        options.addOption(lastNameOpt);
        options.addOption(currentAddressOpt);
        options.addOption(optionalDobOpt);
        options.addOption(regionOpt);
        options.addOption(genderOpt);
        return options;
    }

    /**
     * Create the options (i.e. arguments) for the create clinician command.
     *
     * @return The options used to create a clinician.
     */
    private static Options buildCreateClinicianOptions() {
        Options options = new Options();
        options.addOption(usernameOpt);
        options.addOption(clinicianStaffIdOpt);
        options.addOption(requiredFirstNameOpt);
        options.addOption(middleNameOpt);
        options.addOption(lastNameOpt);
        options.addOption(currentAddressOpt);
        options.addOption(requiredDobOpt);
        options.addOption(requiredRegionOpt);
        options.addOption(genderOpt);

        return options;
    }

    /**
     * Create the options (i.e. arguments) for the search profiles command.
     *
     * @return The options used to search for a user's profile.
     */
    private static Options buildSearchProfilesOptions() {
        Options options = new Options();
        options.addOption(optionalUsernameSearchOpt);
        options.addOption(firstNameSearchOpt);
        options.addOption(lastNameSearchOpt);
        options.addOption(isDonorSearchOpt);
        return options;
    }

    /**
     * Create the options (i.e. arguments) for the edit profile command.
     * <p>
     * Can't build options such that the usage can be auto generated where we can either specify
     * the ID or at least one of the following: firstName, lastName.
     * <p>
     * This condition must be manually checked and usage be manually written for the command description.
     *
     * @return The options for the command.
     */
    private static Options buildEditProfileOptions() {
        Options options = new Options();

        //Can't reuse the options for first name and date of birth since they are required in creating the profile.
        //These must be optional.
        options.addOption(optionalFirstNameOpt);
        options.addOption(optionalDobOpt);

        //The rest can be reused.
        options.addOption(middleNameOpt);
        options.addOption(lastNameOpt);
        options.addOption(genderOpt);
        options.addOption(weightOpt);
        options.addOption(bloodTypeOpt);
        options.addOption(currentAddressOpt);
        options.addOption(regionOpt);
        options.addOption(addDonorOrganOpt);
        options.addOption(addReceiverOrganOpt);
        options.addOption(removeDonorOrgan);
        options.addOption(removeReceiverOrgan);
        options.addOption(heightOpt);
        options.addOption(isSmokerOpt);
        options.addOption(bloodPressureOpt);
        options.addOption(alcoholConsumptionOpt);

        options.addOption(requiredUsernameSearchOpt);

        return options;
    }

    /**
     * Crete all the options (i.e. arguments) to the display profile command.
     *
     * @return The group of options for the command.
     */
    private static Options buildDisplayProfileOptions() {
        //Should only have one argument at a time, either 'all' or a profile number
        OptionGroup profileOptGroup = new OptionGroup();
        profileOptGroup.addOption(allOpt);
        profileOptGroup.addOption(requiredUsernameSearchOpt);

        Options displayProfileOpts = new Options();
        displayProfileOpts.addOptionGroup(profileOptGroup);
        return displayProfileOpts;
    }

    /**
     * Create all the options (i.e. arguments) to the create profile command.
     *
     * @return The group of options for the command.
     */
    private static Options buildCreateProfileOptions() {
        Options createProfileOpts = new Options();
        createProfileOpts.addOption(requiredFirstNameOpt);
        createProfileOpts.addOption(requiredDobOpt);
        createProfileOpts.addOption(middleNameOpt);
        createProfileOpts.addOption(lastNameOpt);
        createProfileOpts.addOption(genderOpt);
        createProfileOpts.addOption(weightOpt);
        createProfileOpts.addOption(bloodTypeOpt);
        createProfileOpts.addOption(currentAddressOpt);
        createProfileOpts.addOption(regionOpt);
        createProfileOpts.addOption(isDonorOpt);
        createProfileOpts.addOption(isReceiverOpt);
        createProfileOpts.addOption(donorOrganListOpt);
        createProfileOpts.addOption(receiverOrganListOpt);
        createProfileOpts.addOption(heightOpt);
        createProfileOpts.addOption(isSmokerOpt);
        createProfileOpts.addOption(bloodPressureOpt);
        createProfileOpts.addOption(alcoholConsumptionOpt);
        createProfileOpts.addOption(chronicDiseaseListOpt);
        createProfileOpts.addOption(usernameOpt);
        return createProfileOpts;
    }

    /**
     * Create all the options (i.e. arguments) to the help command.
     *
     * @return The group of options for the command.
     */
    private static Options buildHelpCommandOptions() {
        OptionGroup helpOptGroup = new OptionGroup();
        helpOptGroup.addOption(allOpt);
        helpOptGroup.addOption(commandOpt);

        Options helpOptions = new Options();
        helpOptions.addOptionGroup(helpOptGroup);
        return helpOptions;
    }


    /**
     * Populated the HashMap with the CLI input values and its associated command.
     *
     * @return Populated HashMap with CLI inputs paired up with their commands.
     */
    private static HashMap<String, CommandEnum> createCommandsHashMap() {
        HashMap commands = new HashMap<>();
        commands.put(QUIT.toString().toLowerCase(), QUIT);
        commands.put(CREATE_PROFILE.toString().toLowerCase(), CREATE_PROFILE);
        commands.put(DISPLAY_PROFILE.toString().toLowerCase(), DISPLAY_PROFILE);
        commands.put(HELP.toString().toLowerCase(), HELP);
        commands.put(EDIT_PROFILE.toString().toLowerCase(), EDIT_PROFILE);
        commands.put(EDIT_CLINICIAN.toString().toLowerCase(), EDIT_CLINICIAN);
        commands.put(DELETE_PROFILE.toString().toLowerCase(), DELETE_PROFILE);
        commands.put(DELETE_CLINICIAN.toString().toLowerCase(), DELETE_CLINICIAN);
        commands.put(CREATE_CLINICIAN.toString().toLowerCase(), CREATE_CLINICIAN);
        commands.put(SQL.toString().toLowerCase(), SQL);
        commands.put(DELETE_CACHE.toString().toLowerCase(), DELETE_CACHE);
        return commands;
    }

    /**
     * Populate the HashMap with command's paired up with their options to define their accepted arguments.
     *
     * @return Populated HashMap with all the commands paired up with their arguments.
     */
    private static HashMap<CommandEnum, Options> createArgumentsHashMap() {
        HashMap<CommandEnum, Options> args = new HashMap<>();

        //Empty because these commands don't take any arguments
        args.put(QUIT, new Options());
        args.put(DELETE_PROFILE, deleteProfileOpts);
        args.put(DELETE_CLINICIAN, deleteClinicianOpts);
        args.put(CREATE_PROFILE, createProfileOpts);
        args.put(CREATE_CLINICIAN, createClinicianOpts);
        args.put(DISPLAY_PROFILE, displayProfileOpts);
        args.put(HELP, helpOptions);
        args.put(EDIT_PROFILE, editProfileOpts);
        args.put(EDIT_CLINICIAN, editClinicianOpts);
        args.put(SQL, new Options());
        args.put(DELETE_CACHE, new Options());

        return args;
    }

    /**
     * Check if the user's entered value is a command and return its associated Commands.
     *
     * @param enteredValue The user's input into the command line.
     * @return The associated Commands or null if the command does not exist.
     * @throws DoesNotExist Thrown if the command does not exists.
     */
    public static CommandEnum retrieveCommand(String enteredValue) throws DoesNotExist {
        CommandEnum retrievedCommandEnum = commands.get(enteredValue);
        if (retrievedCommandEnum == null) {
            throw new DoesNotExist("Command does not exist.");
        }
        return retrievedCommandEnum;
    }

    /**
     * Will close the terminal.
     */
    private static void quitTerminal() {
        if (isUsingJavaFxTerminal) {
            ControllerAccess.getTerminalSceneController().quitTerminal();
        }
    }

    /**
     * Execute the provided command and any additional arguments.
     * <p>
     * If the command was successful it will be stored to a file containing all commands/actions that have occurred.
     *
     * @param cliInput The input to the command line to be executed.
     * @throws InvalidCommand Exception that was thrown from functions called, throw the error to the main loop so the
     * @throws IOException    input output exception
     *                        error message can be displayed to the the user.
     */
    public static void execute(CliInput cliInput) throws InvalidCommand, IOException {
        CommandEnum command = cliInput.getCommandEnum();
        String[] args = cliInput.getArguments();
        String annotation = null; //Used to describe what just happened

        switch (command) {
            case QUIT:
                annotation = "User exited the application.";
                quitTerminal();
                break;
            case CREATE_PROFILE:
                Profile profile = createProfile(args);
                ProfileRequests.getInstance().postProfile(profile);
                annotation = "Profile " + profile.getUsername() + " hsa been created.";
                break;
            case CREATE_CLINICIAN:
                Clinician clinician = createClinician(args);
                ClinicianRequests.getInstance().postClinician(clinician, AuthToken.getInstance().getToken());
                annotation = "Clinician " + clinician.getUsername() + " has been created.";
                break;
            case DISPLAY_PROFILE:
                decideDisplayProfileCommandToExecute(args);
                annotation = "Profile was displayed.";
                break;
            case HELP:
                decideHelpCommandToExecute(args);
                annotation = "User displayed the documentation for commands.";
                break;
            case EDIT_PROFILE:
                Profile editedProfile = editProfile(args);
                ProfileRequests.getInstance().putProfile(editedProfile, AuthToken.getInstance().getToken());
                annotation = "User edited a user's profile.";
                break;
            case EDIT_CLINICIAN:
                Clinician editedClinician = editClinician(args);
                ClinicianRequests.getInstance().putClinician(editedClinician, AuthToken.getInstance().getToken());
                annotation = "Clinician was edited.";
                break;
            case DELETE_PROFILE:
                deleteProfile(args);
                annotation = "User deleted a profile.";
                break;
            case DELETE_CLINICIAN:
                deleteClinician(args);
                annotation = "Clinician profiles was deleted.";
                break;
            case SQL:
                // Do nothing, it is handled by the shell.
                LOGGER.severe("should not have reached this point.");
                break;

            case DELETE_CACHE:
                //Delete the cached items.
                CacheController.emptyCache();
                DataInteraction.saveCache();
                displayToTerminal("Cache Cleared.");
        }

        saveAction(command, args, annotation);
    }

    /**
     * Will delete the specified clinician in the args.
     *
     * @param args The arguments entered by the user.
     * @throws InvalidCommand Exception contains a message detailing what went wrong.
     */
    private static void deleteClinician(String[] args) throws InvalidCommand {
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd;
        try {
            cmd = parser.parse(deleteClinicianOpts, args);
        } catch (ParseException e) {
            throw new InvalidCommand(e.getMessage());
        }

        String username = cmd.getOptionValue(USERNAME_SEARCH_ARG);
        try {
            ClinicianRequests.getInstance().deleteClinician(username, AuthToken.getInstance().getToken());
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
                throw new InvalidCommand("The clinician doesn't exist.");
            } else {
                throw new InvalidCommand(e.getMessage());
            }
        }
    }

    /**
     * Will edit the clinician based on the arguments entered passed in by the user.
     *
     * @param args Arguments entered by the user.
     * @return The edited clinician.
     * @throws InvalidCommand The command was used incorrectly.
     */
    private static Clinician editClinician(String[] args) throws InvalidCommand {
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd;
        try {
            cmd = parser.parse(editClinicianOpts, args);
        } catch (ParseException e) {
            throw new InvalidCommand(e.getMessage());
        }

        Clinician clinician;
        try {
            String username = cmd.getOptionValue(USERNAME_ARG);
            clinician = ClinicianRequests.getInstance().getClinician(username, AuthToken.getInstance().getToken()).getBody();
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
                throw new InvalidCommand("The clinician doesn't exist");
            } else {
                throw new InvalidCommand(e.getMessage());
            }
        }

        clinician.setHospital(new Hospital("DEFAULT", RegionEnum.NOT_SET, "DEFAULT", 0, 0));

        if (cmd.hasOption(STAFF_ID_ARG)) {
            String staffId = cmd.getOptionValue(STAFF_ID_ARG);
            clinician.setStaffId(staffId);
        }

        if (cmd.hasOption(FIRST_NAME_ARG)) {
            String firstName = cmd.getOptionValue(FIRST_NAME_ARG);
            clinician.setFirstName(firstName);
        }

        if (cmd.hasOption(MIDDLE_NAME_ARG)) {
            String middleName = cmd.getOptionValue(MIDDLE_NAME_ARG);
            clinician.setMiddleName(middleName);
        }

        if (cmd.hasOption(LAST_NAME_ARG)) {
            String lastName = cmd.getOptionValue(LAST_NAME_ARG);
            clinician.setLastName(lastName);
        }

        if (cmd.hasOption(DOB_ARG)) {
            LocalDate dob = LocalDate.parse(cmd.getOptionValue(DOB_ARG));
            clinician.setDateOfBirth(dob);
        }

        if (cmd.hasOption(CURRENT_ADDRESS_ARG)) {
            String address = cmd.getOptionValue(CURRENT_ADDRESS_ARG);
            clinician.setAddress(address);
        }

        if (cmd.hasOption(REGION_ARG)) {
            RegionEnum region;
            try {
                region = RegionTypeController.getRegionType(cmd.getOptionValue(REGION_ARG));
                clinician.setRegion(region);
            } catch (DoesNotExist doesNotExist) {
                LOGGER.severe(doesNotExist.getMessage());
            }

        }

        if (cmd.hasOption(GENDER_ARG)) {
            try {
                GenderEnum gender = GenderController.getGender(cmd.getOptionValue(GENDER_ARG));
                clinician.setGender(gender);
            } catch (DoesNotExist doesNotExist) {
                throw new InvalidCommand(doesNotExist.getMessage());
            }
        }

        return clinician;
    }

    /**
     * Delete the specified user.
     *
     * @param args Arguments the user entered for deleting a user.
     * @throws InvalidCommand Command was used incorrectly or an exception occurred by functions called.
     */
    private static void deleteProfile(String[] args) throws InvalidCommand {
        CommandLine cmd;

        try {
            cmd = new DefaultParser().parse(deleteProfileOpts, args);
        } catch (ParseException e) {
            throw new InvalidCommand(e.getMessage());
        }

        String username = cmd.getOptionValue(USERNAME_SEARCH_ARG);
        try {
            ProfileRequests.getInstance().deleteProfile(username, AuthToken.getInstance().getToken());
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
                throw new InvalidCommand("The profile doesn't exist.");
            } else {
                throw new InvalidCommand(e.getMessage());
            }
        }
    }

    /**
     * Edit the a user's profile, found by using their profile ID.
     *
     * @param args The arguments entered by the user. Will contains information on what details to to edit and what
     *             their updated values will be.
     * @throws InvalidCommand Command was used incorrectly or an exception occurred by functions called.
     */
    private static Profile editProfile(String[] args) throws InvalidCommand {
        CommandLine cmd;

        try {
            cmd = new DefaultParser().parse(editProfileOpts, args);
        } catch (ParseException e) {
            throw new InvalidCommand(e.getMessage());
        }

        String username = cmd.getOptionValue(USERNAME_SEARCH_ARG);

        Profile profileToUpdate;

        try {
            profileToUpdate = ProfileRequests.getInstance().getProfile(username, AuthToken.getInstance().getToken()).getBody();
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
                throw new InvalidCommand("The profile does not exist.");
            } else {
                throw new InvalidCommand(e.getMessage());
            }
        }


        if (cmd.hasOption(FIRST_NAME_ARG)) {
            String enteredFirstNameValue = cmd.getOptionValue(FIRST_NAME_ARG);
            profileToUpdate.setFirstName(enteredFirstNameValue);

        }

        if (cmd.hasOption(DOB_ARG)) {
            String enteredDateOfBirthValue = cmd.getOptionValue(DOB_ARG);
            LocalDate updatedDob = LocalDate.parse(enteredDateOfBirthValue);
            profileToUpdate.setDateOfBirth(updatedDob);
        }

        if (cmd.hasOption(ADD_DONOR_ORGAN_ARG)) {
            try {
                String enteredOrgan = cmd.getOptionValue(ADD_DONOR_ORGAN_ARG);
                OrganEnum organ = OrganController.getOrgan(enteredOrgan);
                profileToUpdate.addDonorOrgan(organ);
            } catch (DoesNotExist | AlreadyExists e) {
                throw new InvalidCommand(e.getMessage());
            }
        }

        if (cmd.hasOption(ADD_RECEIVER_ORGAN_ARG)) {
            try {
                String enteredOrgan = cmd.getOptionValue(ADD_RECEIVER_ORGAN_ARG);
                OrganEnum organ = OrganController.getOrgan(enteredOrgan);

                profileToUpdate.addReceivingOrgan(organ);
            } catch (DoesNotExist e) {
                throw new InvalidCommand(e.getMessage());
            }
        }

        if (cmd.hasOption(REMOVE_DONOR_ORGAN_ARG)) {
            try {
                String enteredOrgan = cmd.getOptionValue(REMOVE_DONOR_ORGAN_ARG);
                OrganEnum organ = OrganController.getOrgan(enteredOrgan);
                profileToUpdate.removeDonorOrgan(organ);
            } catch (DoesNotExist e) {
                throw new InvalidCommand(e.getMessage());
            }
        }

        if (cmd.hasOption(REMOVE_RECEIVER_ORGAN_ARG)) {
            try {
                String enteredOrgan = cmd.getOptionValue(REMOVE_RECEIVER_ORGAN_ARG);
                OrganEnum organ = OrganController.getOrgan(enteredOrgan);
                profileToUpdate.removeReceiverOrgan(organ);
            } catch (DoesNotExist e) {
                throw new InvalidCommand(e.getMessage());
            }
        }

        //Update all the non required fields for profiles that are shared between commands.
        //(e.g. all excluding first name and date of birth).
        return modifyProfile(cmd, profileToUpdate);


    }

    /**
     * There are multiple help commands that be executed. Choose one based the arguments entered by the user.
     *
     * @param args Arguments entered by the user.
     * @throws InvalidCommand Thrown if there is an unrecognized argument or feature not yet implemented.
     */
    private static void decideHelpCommandToExecute(String[] args) throws InvalidCommand {
        int width = ControllerAccess.getTerminalSceneController().getTerminalDisplayWidth();
        int leftPad = 2;
        int descPad = 5;

        HelpFormatter formatter = new HelpFormatter();
        formatter.setOptionComparator((opt1, opt2) -> {
            if (opt1.isRequired() && !opt2.isRequired()) {
                return -1; //The required option will be listed first
            } else if (!opt1.isRequired() && opt2.isRequired()) {
                return 1; //The required option will be listed first.
            } else {
                //Both are required or aren't required. Now compare alphabetically
                if (opt1.hasArgName() && opt2.hasArgName()) {
                    return opt1.getArgName().compareTo(opt2.getArgName());
                } else {
                    return opt1.getLongOpt().compareTo(opt2.getLongOpt());
                }
            }
        });

        CommandLine cmd;
        try {
            cmd = new DefaultParser().parse(helpOptions, args);
        } catch (ParseException e) {
            LOGGER.severe(e.getMessage());
            throw new InvalidCommand(e.getMessage());
        }

        try {
            if (cmd.hasOption(ALL_ARG)) {
                helpAll(formatter, width, leftPad, descPad);
            } else if (cmd.hasOption(COMMAND_ARG)) {
                String enteredValue = cmd.getOptionValue(COMMAND_ARG);
                CommandEnum command = retrieveCommand(enteredValue);
                help(command, formatter, width, leftPad, descPad);
            } else {
                throw new InvalidCommand(MISSING_ARGUMENT_MESSAGE);
            }

        } catch (DoesNotExist e) {
            throw new InvalidCommand(e.getMessage());
        }
    }

    /**
     * Retrieve the String the user has to type into the terminal.
     *
     * @param command The command whose associated input into the command line we're interested in.
     * @return The user input into the command line for the desired command.
     * @throws DoesNotExist The value for the command entered by the user does not match any commands.
     */
    public static String retrieveCommandInput(CommandEnum command) throws DoesNotExist {
        for (Map.Entry<String, CommandEnum> entry : commands.entrySet()) {
            if (Objects.equals(command, entry.getValue())) {
                return entry.getKey();
            }
        }

        throw new DoesNotExist("The desired command does not exist.");
    }

    /**
     * Display the description for all commands.
     *
     * @param formatter The HelpFormatter used to display the usage and argument descriptions for a command.
     * @param width     Specifies how far the text can span across the terminal.
     * @param leftPad   Specifies how much padding is used between the left side of the terminal and the arguments
     * @param descPad   Specifies how much padding is used between the arguments and description.
     * @throws DoesNotExist The specified command by the user does not exist.
     */
    private static void helpAll(HelpFormatter formatter, int width, int leftPad, int descPad) throws DoesNotExist {

        for (CommandEnum command : commands.values()) {
            StringWriter out = new StringWriter();
            PrintWriter pw = new PrintWriter(out);

            formatter.printHelp(pw, width, retrieveCommandInput(command), "", arguments.get(command), leftPad, descPad, "", true);
            pw.flush();
            displayToTerminal(out.toString());
            pw.close();
        }
    }

    /**
     * Display the description for a single command.
     *
     * @param command   The command's usage and argument description will be displayed.
     * @param formatter The HelpFormatter used to display the usage and argument descriptions for a command.
     * @param width     Specifies how far the text can span across the terminal.
     * @param leftPad   Specifies how much padding is used between the left side of the terminal and the arguments
     * @param descPad   Specifies how much padding is used between the arguments and description.
     * @throws DoesNotExist The command that was specified by the user does not exist.
     */
    private static void help(CommandEnum command, HelpFormatter formatter, int width, int leftPad, int descPad) throws DoesNotExist {
        StringWriter out = new StringWriter();
        PrintWriter pw = new PrintWriter(out);

        formatter.printHelp(pw, width, retrieveCommandInput(command), "", arguments.get(command), leftPad,
                descPad, "", true);
        pw.flush();
        displayToTerminal(out.toString());
        pw.close();
    }

    /**
     * There are multiple display profile methods that can be executed. Choose one based on the arguments entered
     * by the user.
     *
     * @param args Arguments entered by the user.
     * @throws InvalidCommand Thrown if there is an unrecognized argument or feature not yet implemented.
     */
    private static void decideDisplayProfileCommandToExecute(String[] args) throws InvalidCommand {
        CommandLine cmd;
        try {
            cmd = new DefaultParser().parse(displayProfileOpts, args);
        } catch (ParseException e) {
            throw new InvalidCommand(e.getMessage());
        }

        if (cmd.hasOption(ALL_ARG)) {
            displayAllProfiles();
        } else if (cmd.hasOption(USERNAME_ARG)) {
            String username = (cmd.getOptionValue(USERNAME_ARG));
            displayProfile(username);
        } else {
            throw new InvalidCommand(UNKNOWN_ARGUMENT_MESSAGE);
        }
    }


    /**
     * Checks if the arguments are correct for the command given.
     *
     * @param arguments The arguments the user entered as a String array that was split up with a space used as the
     *                  delimiter.
     * @param command   The command entered.
     * @throws InvalidCommand Thrown if the argument is wrong for the command.
     */
    public static void checkArguments(CommandEnum command, String[] arguments) throws InvalidCommand {
        final int NO_ARGUMENTS = 0; //contains the command to be executed

        //Check for all argument validity for commands that do not take arguments.
        ArrayList<CommandEnum> noArguments = new ArrayList<>(Arrays.asList(QUIT, SQL, DELETE_CACHE));

        if (noArguments.contains(command) && arguments.length != NO_ARGUMENTS) {
            throw new InvalidCommand(INVALID_ARGUMENT_MESSAGE);
        }


        // If the user only provided the command without any arguments (when it must need them) then it is not valid.
        if (!noArguments.contains(command) && arguments.length == NO_ARGUMENTS) {
            throw new InvalidCommand(INVALID_ARGUMENT_MESSAGE);
        }

        //NOTE THE FOLLOWING CHECKS ASSUME THAT THERE WERE ARGUMENTS ENTERED! THE EDGE CASE WAS HANDLED ABOVE WHEN
        //NO ARGUMENTS WERE ENTERED FOR A COMMAND THAT WAS EXPECTING ARGUMENTS.

        // Check the arguments for commands that take a single argument
        if (command == DISPLAY_PROFILE) {
            boolean isSuccess = checkDisplayProfileArg(arguments);
            if (isSuccess) {
                return;
            }
        }

        if (command == CommandEnum.HELP) {
            boolean isSuccess = checkHelpArguments(arguments);
            if (isSuccess) {
                return;
            }

        }

        if (command == CREATE_PROFILE) {
            boolean isSuccess = checkCreateProfileArguments(arguments);
            if (isSuccess) {
                return;
            }
        }

        if (command == EDIT_PROFILE) {
            boolean isSuccess = checkEditProfileArguments(arguments);
            if (isSuccess) {
                return;
            }
        }

        if (command == EDIT_CLINICIAN) {
            boolean isSuccess = checkEditClinicianArguments(arguments);
            if (isSuccess) {
                return;
            }
        }

        if (command == DELETE_PROFILE) {
            boolean isSuccess = checkDeleteProfileArguments(arguments);
            if (isSuccess) {
                return;
            }
        }

        if (command == DELETE_CLINICIAN) {
            boolean isSuccess = checkDeleteClinicianArguments(arguments);
            if (isSuccess) {
                return;
            }
        }

        if (command == CREATE_CLINICIAN) {
            boolean isSuccess = checkCreateClinicianArguments(arguments);
            if (isSuccess) {
                return;
            }
        }

    }

    /**
     * Check the arguments for the delete clinician command is valid.
     *
     * @param args The arguments entered for the command.
     * @return True if all the arguments pass the checks.
     * @throws InvalidCommand The arguments were invalid. Exception contains a message for what went wrong.
     */
    private static boolean checkDeleteClinicianArguments(String[] args) throws InvalidCommand {
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd;

        try {
            cmd = parser.parse(deleteClinicianOpts, args);
        } catch (ParseException e) {
            throw new InvalidCommand(e.getMessage());
        }

        //Check if all values entered have been . If they are not then there were extra elements entered into
        //the command line that were not paid with an argument
        int numArgs = cmd.getArgs().length;
        if (numArgs != EXPECTED_NUM_ARGS_AFTER_SUCCESSFUL_PARSE) {
            throw new InvalidCommand(UNKNOWN_ARGUMENT_MESSAGE);
        }

        String username = cmd.getOptionValue(USERNAME_SEARCH_ARG);
        if (username.equals("")) {
            throw new InvalidCommand("You must enter a valid username for the clinician.");
        }

        return true;
    }

    /**
     * Check all the arguments for the create clinician command is valid.
     * <p>
     * Legacy issue is that the method does not return false because if there was something wrong
     * the method would throw an exception saying what went wrong.
     *
     * @param args The arguments entered by the user.
     * @return True if all the arguments pass the checks.
     * @throws InvalidCommand The arguments were incorrect.
     */
    private static boolean checkCreateClinicianArguments(String[] args) throws InvalidCommand {
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd;
        try {
            cmd = parser.parse(createClinicianOpts, args);
        } catch (ParseException e) {
            throw new InvalidCommand(e.getMessage());
        }


        if (cmd.hasOption(USERNAME_ARG)) {
            String username = cmd.getOptionValue(USERNAME_ARG);

            try {
                ClinicianRequests.getInstance().getClinician(username, AuthToken.getInstance().getToken());
                throw new InvalidCommand("The username is already used.");
            } catch (HttpClientErrorException e) {
                if (e.getStatusCode() != HttpStatus.NOT_FOUND) {
                    throw new InvalidCommand(e.getMessage());
                }
            }

        }

        return checkClinicianArgs(cmd);
    }

    /**
     * Check all the arguments for the delete profile command.
     *
     * @param args All the arguments entered by the user.
     * @return true if all the arguments pass the checks.
     * @throws InvalidCommand The arguments were incorrect.
     */
    private static boolean checkDeleteProfileArguments(String[] args) throws InvalidCommand {
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd;

        try {
            cmd = parser.parse(deleteProfileOpts, args);
        } catch (ParseException e) {
            throw new InvalidCommand(e.getMessage());
        }

        //Check if all values entered have been . If they are not then there were extra elements entered into
        //the command line that were not paid with an argument
        int numArgs = cmd.getArgs().length;
        if (numArgs != EXPECTED_NUM_ARGS_AFTER_SUCCESSFUL_PARSE) {
            throw new InvalidCommand(UNKNOWN_ARGUMENT_MESSAGE);
        }
        return true;
    }

    /**
     * Check all the arguments for the edit clinician command.
     *
     * @param args All the arguments entered by the user.
     * @return True if all the arguments pass the checks.
     * @throws InvalidCommand The arguments were incorrect. Exception will contain a message of what went wrong.
     */
    private static boolean checkEditClinicianArguments(String[] args) throws InvalidCommand {
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd;
        try {
            cmd = parser.parse(editClinicianOpts, args);
        } catch (ParseException e) {
            throw new InvalidCommand(e.getMessage());
        }

        if (cmd.hasOption(USERNAME_ARG)) {
            try {
                String username = cmd.getOptionValue(USERNAME_ARG);
                ClinicianRequests.getInstance().getClinician(username, AuthToken.getInstance().getToken());
            } catch (HttpClientErrorException e) {
                if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
                    throw new InvalidCommand("The username doesn't exist.");
                } else {
                    throw new InvalidCommand(e.getMessage());
                }
            }
        } else {
            throw new InvalidCommand("Need a value for the username.");
        }

        return checkClinicianArgs(cmd);
    }

    /**
     * Check all the arguments which are the same for clinician commands.
     *
     * @param cmd The Commons.cli.CommandLine that contains all the parsed arguments
     * @return True if all the arguments pass the checks.
     * @throws InvalidCommand The arguments were incorrect. Exception will contain a message of what went wrong.
     */
    private static boolean checkClinicianArgs(CommandLine cmd) throws InvalidCommand {
        int numArgs = cmd.getArgs().length;
        if (numArgs != EXPECTED_NUM_ARGS_AFTER_SUCCESSFUL_PARSE) {
            throw new InvalidCommand(UNKNOWN_ARGUMENT_MESSAGE);
        }

        // Assuming that the staff ID and username will match the same regex
        if (cmd.hasOption(STAFF_ID_ARG) && !Validator.isValidUsername(cmd.getOptionValue(STAFF_ID_ARG))) {
            throw new InvalidCommand("Invalid value for the staff ID.");
        }

        if (cmd.hasOption(FIRST_NAME_ARG) && !Validator.isValidFirstName(cmd.getOptionValue(FIRST_NAME_ARG))) {
            throw new InvalidCommand("Invalid value for the first name.");
        }

        if (cmd.hasOption(MIDDLE_NAME_ARG) && !Validator.isValidMiddleName(cmd.getOptionValue(MIDDLE_NAME_ARG))) {
            throw new InvalidCommand("Invalid value for the middle name.");
        }

        if (cmd.hasOption(LAST_NAME_ARG) && !Validator.isValidLastName(cmd.getOptionValue(LAST_NAME_ARG))) {
            throw new InvalidCommand("Invalid value for the last name.");
        }

        if (cmd.hasOption(DOB_ARG)) {
            try {
                if (!Validator.isValidDateOfBirth(LocalDate.parse(cmd.getOptionValue(DOB_ARG)))) {
                    throw new InvalidCommand("Invalid value for the date of birth.");
                }
            } catch (DateTimeParseException e) {
                throw new InvalidCommand("Invalid value for the date of birth.");
            }
        }

        if (cmd.hasOption(GENDER_ARG) && !Validator.isValidGender(cmd.getOptionValue(GENDER_ARG))) {
            throw new InvalidCommand("Invalid value for the gender.");
        }

        return true;
    }

    /**
     * Check all the arguments for the edit profile command.
     *
     * @param args All the arguments entered by the user.
     * @return true if all the arguments pass the checks.
     * @throws InvalidCommand The arguments were incorrect.
     */
    private static boolean checkEditProfileArguments(String[] args) throws InvalidCommand {
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd;

        try {
            cmd = parser.parse(editProfileOpts, args);
        } catch (ParseException e) {
            throw new InvalidCommand(e.getMessage());
        }

        //Check if all values entered have been . If they are not then there were extra elements entered into
        //the command line that were not paid with an argument
        int numArgs = cmd.getArgs().length;
        if (numArgs != EXPECTED_NUM_ARGS_AFTER_SUCCESSFUL_PARSE) {
            throw new InvalidCommand(UNKNOWN_ARGUMENT_MESSAGE);
        }

        if (!cmd.hasOption(USERNAME_SEARCH_ARG)) {
            throw new InvalidCommand("Need to include the username for the profile that will be edited.");
        }
        //Check if only the search ID argument was provided. Need args to update the values of the profile.
        if (cmd.hasOption(USERNAME_SEARCH_ARG) && args.length == 1) {
            throw new InvalidCommand("Need to include arguments to update the profile.");
        }

        //Check if the values entered by the use are valid.
        if (cmd.hasOption(FIRST_NAME_ARG)) {
            String firstName = cmd.getOptionValue(FIRST_NAME_ARG).trim();
            if (firstName.equals(NO_STRING_INPUT)) {
                throw new InvalidCommand(MISSING_FIRST_NAME_INPUT_MESSAGE);
            }
        }

        if (cmd.hasOption(DOB_ARG)) {
            try {
                LocalDate.parse(cmd.getOptionValue(DOB_ARG));
            } catch (DateTimeParseException e) {
                throw new InvalidCommand(WRONG_LOCAL_DATE_FORMAT_MESSAGE);
            }
        }

        if (cmd.hasOption(GENDER_ARG)) {
            String enteredValue = cmd.getOptionValue(GENDER_ARG).toLowerCase();
            GenderEnum gender;
            switch (enteredValue) {
                case "m":
                case "male":
                    //pass the check
                    break;
                case "f":
                case "female":
                    //pass the check
                    break;
                case "o":
                case "other":
                    //pass the check
                    break;
                default:
                    throw new InvalidCommand("Gender can only be M/F/Other");
            }
        }

        try {
            if (cmd.hasOption(HEIGHT_ARG)) {
                String enteredValue = cmd.getOptionValue(HEIGHT_ARG);
                Float.parseFloat(enteredValue);
            }
        } catch (NumberFormatException e) {
            throw new InvalidCommand("Entered invalid value for height argument.");
        }

        try {
            if (cmd.hasOption(WEIGHT_ARG)) {
                String enteredValue = cmd.getOptionValue(WEIGHT_ARG);
                Float.parseFloat(enteredValue);
            }
        } catch (NumberFormatException e) {
            throw new InvalidCommand("Entered invalid value for weight argument.");
        }

        if (cmd.hasOption(BLOOD_TYPE_ARG)) {
            String enteredValue = cmd.getOptionValue(BLOOD_TYPE_ARG);
            try {
                BloodTypeController.getBloodType(enteredValue);
            } catch (DoesNotExist doesNotExist) {
                throw new InvalidCommand(doesNotExist.getMessage());
            }
        }


        //Check if the entered organ is valid
        if (cmd.hasOption(REMOVE_DONOR_ORGAN_ARG)) {
            try {
                String enteredValue = cmd.getOptionValue(REMOVE_DONOR_ORGAN_ARG);
                OrganController.getOrgan(enteredValue);
            } catch (DoesNotExist e) {
                throw new InvalidCommand(e.getMessage());
            }
        }

        if (cmd.hasOption(ADD_DONOR_ORGAN_ARG)) {
            try {
                String enteredValue = cmd.getOptionValue(ADD_DONOR_ORGAN_ARG);
                OrganController.getOrgan(enteredValue);
            } catch (DoesNotExist e) {
                throw new InvalidCommand(e.getMessage());
            }
        }

        if (cmd.hasOption(REMOVE_RECEIVER_ORGAN_ARG)) {
            try {
                String enteredValue = cmd.getOptionValue(REMOVE_RECEIVER_ORGAN_ARG);
                OrganController.getOrgan(enteredValue);
            } catch (DoesNotExist e) {
                throw new InvalidCommand(e.getMessage());
            }
        }

        if (cmd.hasOption(ADD_RECEIVER_ORGAN_ARG)) {
            try {
                String enteredValue = cmd.getOptionValue(ADD_RECEIVER_ORGAN_ARG);
                OrganController.getOrgan(enteredValue);
            } catch (DoesNotExist e) {
                throw new InvalidCommand(e.getMessage());
            }
        }

        if (cmd.hasOption(IS_SMOKER_ARG)) {
            String enteredValue = cmd.getOptionValue(IS_SMOKER_ARG);
            ArrayList<String> validAnswers = new ArrayList<>(Arrays.asList("true", "false"));
            if (!validAnswers.contains(enteredValue)) {
                throw new InvalidCommand("Invalid value entered for isSmoker.");
            }
        }

        if (cmd.hasOption(ALCOHOL_CONSUMPTION_ARG)) {
            String enteredValue = cmd.getOptionValue(ALCOHOL_CONSUMPTION_ARG);
            ArrayList<String> validAnswers = new ArrayList<>(Arrays.asList("none", "low", "medium", "high", "extreme"));
            if (!validAnswers.contains(enteredValue.toLowerCase())) {
                throw new InvalidCommand("Invalid option entered for the donors alcohol consumption");
            }
        }
        return true;
    }

    /**
     * Check all the arguments for the create profile command.
     *
     * @param arguments All the arguments entered by the user.
     * @return true if all the arguments pass the checks.
     * @throws InvalidCommand The arguments were incorrect.
     */
    private static boolean checkCreateProfileArguments(String[] arguments) throws InvalidCommand {
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd;

        try {
            cmd = parser.parse(createProfileOpts, arguments);
        } catch (ParseException e) {
            throw new InvalidCommand(e.getMessage());
        }

        //Check if all values entered have been . If they are not then there were extra elements entered into
        //the command line that were not paid with an argument. E.g. '-firstName=John Smith' where Smith is no longer
        //paired with the argument 'firstName'
        int numArgs = cmd.getArgs().length;
        if (numArgs != EXPECTED_NUM_ARGS_AFTER_SUCCESSFUL_PARSE) {
            throw new InvalidCommand(UNKNOWN_ARGUMENT_MESSAGE);
        }

        String username = cmd.getOptionValue(USERNAME_ARG);

        if (Validator.isValidUsername(username)) {
            try {
                ProfileRequests.getInstance().getProfile(username, AuthToken.getInstance().getToken());
                throw new InvalidCommand("The username is already used.");
            } catch (HttpClientErrorException e) {
                if (e.getStatusCode() != HttpStatus.NOT_FOUND) {
                    throw new InvalidCommand(e.getMessage());
                }
            }
        } else {
            throw new InvalidCommand("The username is already used.");
        }


        //Check if the values entered by the use are valid.
        String firstName = cmd.getOptionValue(FIRST_NAME_ARG).trim();
        if (NO_STRING_INPUT.equals(firstName) || !Validator.isValidFirstName(firstName)) {
            throw new InvalidCommand(Tooltips.INVALID_FIRST_NAME_TOOLTIP_TEXT);
        }

        LocalDate dateOfBirth;
        try {
            dateOfBirth = LocalDate.parse(cmd.getOptionValue(DOB_ARG));

            if (!Validator.isValidDateOfBirth(dateOfBirth)) {
                throw new InvalidCommand(Tooltips.INVALID_DATE_OF_BIRTH_TOOLTIP_TEXT);
            }

        } catch (DateTimeParseException e) {
            throw new InvalidCommand(WRONG_LOCAL_DATE_FORMAT_MESSAGE);
        }

        if (cmd.hasOption(GENDER_ARG)) {
            String enteredValue = cmd.getOptionValue(GENDER_ARG).toLowerCase();
            switch (enteredValue) {
                case "m":
                case "male":
                    //pass the check
                    break;
                case "f":
                case "female":
                    //pass the check
                    break;
                case "o":
                case "other":
                    //pass the check
                    break;
                default:
                    throw new InvalidCommand("Gender can only be M/F/Other");
            }
        }

        try {
            if (cmd.hasOption(HEIGHT_ARG)) {
                String enteredValue = cmd.getOptionValue(HEIGHT_ARG);
                Float.parseFloat(enteredValue);

                if (!Validator.isValidHeight(enteredValue)) {
                    throw new InvalidCommand(Tooltips.INVALID_HEIGHT_TOOLTIP_TEXT);
                }

            }
        } catch (NumberFormatException e) {
            throw new InvalidCommand(Tooltips.INVALID_HEIGHT_TOOLTIP_TEXT);
        }

        try {
            if (cmd.hasOption(WEIGHT_ARG)) {
                String enteredValue = cmd.getOptionValue(WEIGHT_ARG);
                Float.parseFloat(enteredValue);

                if (!Validator.isValidWeight(enteredValue)) {
                    throw new InvalidCommand(Tooltips.INVALID_WEIGHT_TOOLTIP_TEXT);
                }

            }
        } catch (NumberFormatException e) {
            throw new InvalidCommand(Tooltips.INVALID_WEIGHT_TOOLTIP_TEXT);
        }

        if (cmd.hasOption(BLOOD_TYPE_ARG)) {
            String enteredValue = cmd.getOptionValue(BLOOD_TYPE_ARG);
            BloodTypeEnum bloodType;
            try {
                BloodTypeController.getBloodType(enteredValue);

            } catch (DoesNotExist doesNotExist) {
                throw new InvalidCommand(doesNotExist.getMessage());
            }
        }

        if (cmd.hasOption(IS_DONOR_ARG)) {
            String enteredValue = cmd.getOptionValue(IS_DONOR_ARG).toLowerCase();
            ArrayList<String> validAnswers = new ArrayList<>(Arrays.asList("true", "false"));
            if (!validAnswers.contains(enteredValue)) {
                throw new InvalidCommand("Invalid value provided for isDonor argument.");
            }
        }

        if (cmd.hasOption(IS_RECEIVER_ARG)) {
            String enteredValue = cmd.getOptionValue(IS_RECEIVER_ARG).toLowerCase();
            ArrayList<String> validAnswers = new ArrayList<>(Arrays.asList("true", "false"));
            if (!validAnswers.contains(enteredValue)) {
                throw new InvalidCommand("Invalid value provided for isReceiver argument.");
            }
        }

        if (cmd.hasOption(DONOR_ORGANS_ARG)) {
            String enteredValues = cmd.getOptionValue(DONOR_ORGANS_ARG);
            try {
                OrganController.extractOrgans(enteredValues);
            } catch (DoesNotExist doesNotExist) {
                throw new InvalidCommand(doesNotExist.getMessage());
            }
        }

        if (cmd.hasOption(RECEIVER_ORGANS_ARG)) {
            String enteredValues = cmd.getOptionValue(RECEIVER_ORGANS_ARG);
            try {
                OrganController.extractOrgans(enteredValues);
            } catch (DoesNotExist doesNotExist) {
                throw new InvalidCommand(doesNotExist.getMessage());
            }
        }

        if (cmd.hasOption(IS_SMOKER_ARG)) {
            String enteredValue = cmd.getOptionValue(IS_SMOKER_ARG);
            ArrayList<String> validAnswers = new ArrayList<>(Arrays.asList("true", "false"));
            if (!validAnswers.contains(enteredValue)) {
                throw new InvalidCommand("Invalid value entered for isSmoker.");
            }
        }

        if (cmd.hasOption(ALCOHOL_CONSUMPTION_ARG)) {
            String enteredValue = cmd.getOptionValue(ALCOHOL_CONSUMPTION_ARG);
            ArrayList<String> validAnswers = new ArrayList<>(Arrays.asList("none", "low", "medium", "high", "extreme"));
            if (!validAnswers.contains(enteredValue.toLowerCase())) {
                throw new InvalidCommand("Invalid option entered for the donors alcohol consumption");
            }
        }

        return true;
    }

    /**
     * Check all the arguments for the help command.
     *
     * @param arguments All the arguments entered by the user.
     * @return true if all the arguments pass the checks. false otherwise.
     * @throws InvalidCommand Thrown if the arguments were incorrect
     */
    private static boolean checkHelpArguments(String[] arguments) throws InvalidCommand {
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd;

        try {
            cmd = parser.parse(helpOptions, arguments, true);
        } catch (ParseException e) {
            throw new InvalidCommand(e.getMessage());
        }

        //Check if all values entered have been . If they are not then there were extra elements entered into
        //the command line that were not paid with an argument. E.g. '-firstName=John Smith' where Smith is no longer
        //paired with the argument 'firstName'
        int numArgs = cmd.getArgs().length;
        if (numArgs != EXPECTED_NUM_ARGS_AFTER_SUCCESSFUL_PARSE) {
            throw new InvalidCommand(UNKNOWN_ARGUMENT_MESSAGE);
        }

        if (cmd.hasOption(COMMAND_ARG)) {
            try {
                String enteredValue = cmd.getOptionValue(COMMAND_ARG);
                retrieveCommand(enteredValue);
            } catch (DoesNotExist e) {
                throw new InvalidCommand(e.getMessage());
            }
        }

        return true;
    }

    /**
     * Check all the arguments for the display_profile command.
     *
     * @param arguments All the arguments entered by the user.
     * @return true if all the arguments pass the checks. False otherwise.
     * @throws InvalidCommand Thrown if the arguments were incorrect.
     */
    private static boolean checkDisplayProfileArg(String[] arguments) throws InvalidCommand {
        CommandLine cmd;
        CommandLineParser parser = new DefaultParser();


        try {
            cmd = parser.parse(displayProfileOpts, arguments);
        } catch (ParseException e) {
            throw new InvalidCommand(e.getMessage());
        }


        //Check if all values entered have been . If they are not then there were extra elements entered into
        //the command line that were not paid with an argument. E.g. '-firstName=John Smith' where Smith is no longer
        //paired with the argument 'firstName'
        int numArgs = cmd.getArgs().length;
        if (numArgs != EXPECTED_NUM_ARGS_AFTER_SUCCESSFUL_PARSE) {
            throw new InvalidCommand(UNKNOWN_ARGUMENT_MESSAGE);
        }
        return true;

    }

    /**
     * Display the profile with the specified ID.
     *
     * @param profile_id The ID for the profile that will be displayed.
     */
    private static void displayProfile(String profile_id) {

        try {
            Profile profile = ProfileRequests.getInstance().getProfile(profile_id, AuthToken.getInstance().getToken()).getBody();
            displayToTerminal(profile.toString());
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
                displayToTerminal("The profile does not exist.");
            } else {
                displayToTerminal(e.getMessage());
            }
        }
    }

    /**
     * Update the attributes in the provided profile if there are arguments for it from the command line.
     *
     * @param cmd     CommandLine instance from Common CLI that contains parsed arguments.
     * @param profile The profile that whose attributes will be modified.
     * @return The same profile instance but with updated values.
     * @throws InvalidCommand Issue when retrieving the values entered by the user when settings the profile attributes.
     */
    private static Profile modifyProfile(CommandLine cmd, Profile profile) throws InvalidCommand {

        if (cmd.hasOption(MIDDLE_NAME_ARG)) {
            String middleName = cmd.getOptionValue(MIDDLE_NAME_ARG);
            profile.setMiddleName(middleName);

        }

        if (cmd.hasOption(LAST_NAME_ARG)) {
            String lastName = cmd.getOptionValue(LAST_NAME_ARG);
            profile.setLastName(lastName);
        }

        if (cmd.hasOption(GENDER_ARG)) {
            String enteredValue = cmd.getOptionValue(GENDER_ARG).toLowerCase();
            GenderEnum gender;
            switch (enteredValue) {
                case "m":
                case "male":
                    gender = GenderEnum.MALE;
                    break;
                case "f":
                case "female":
                    gender = GenderEnum.FEMALE;
                    break;
                case "o":
                case "other":
                    gender = GenderEnum.OTHER;
                    break;
                default:
                    throw new InvalidCommand("Gender can only be M/F/Other");
            }
            profile.setGender(gender);
        }

        try {
            if (cmd.hasOption(HEIGHT_ARG)) {
                String enteredValue = cmd.getOptionValue(HEIGHT_ARG);
                float height = Float.parseFloat(enteredValue);
                profile.setHeight(height);
            }
        } catch (NumberFormatException e) {
            throw new InvalidCommand("Entered invalid value for height argument.");
        }

        try {
            if (cmd.hasOption(WEIGHT_ARG)) {
                String enteredValue = cmd.getOptionValue(WEIGHT_ARG);
                float weight = Float.parseFloat(enteredValue);
                profile.setWeight(weight);
            }
        } catch (NumberFormatException e) {
            throw new InvalidCommand("Entered invalid value for weight argument.");
        }


        if (cmd.hasOption(BLOOD_TYPE_ARG)) {
            String enteredValue = cmd.getOptionValue(BLOOD_TYPE_ARG);
            BloodTypeEnum bloodType;
            try {
                bloodType = BloodTypeController.getBloodType(enteredValue);
            } catch (DoesNotExist doesNotExist) {
                throw new InvalidCommand(doesNotExist.getMessage());
            }
            profile.setBloodType(bloodType);
        }

        if (cmd.hasOption(CURRENT_ADDRESS_ARG)) {
            String enteredValue = cmd.getOptionValue(CURRENT_ADDRESS_ARG);
            profile.setAddress(enteredValue);
        }

        if (cmd.hasOption(REGION_ARG)) {
            String enteredValue = cmd.getOptionValue(REGION_ARG);
            RegionEnum regionType;
            try {
                regionType = RegionTypeController.getRegionType(enteredValue);
            } catch (DoesNotExist doesNotExist) {
                throw new InvalidCommand(doesNotExist.getMessage());
            }
            profile.setRegion(regionType);
        }

        if (cmd.hasOption(IS_DONOR_ARG)) {
            String enteredValue = cmd.getOptionValue(IS_DONOR_ARG).toLowerCase();

            ArrayList<String> validAnswers = new ArrayList<>(Arrays.asList("true", "false"));

            if (!validAnswers.contains(enteredValue)) {
                throw new InvalidCommand("Invalid value provided for isDonor argument.");
            }

            profile.setIsDonor(Boolean.parseBoolean(enteredValue));

            //For the situation where the user was a donor, added organs to donate. Then opt out of being a donor.
            //Then opt back into being a donor. Their old list of organs get cleared out.
            if (!profile.isDonor()) {
                profile.clearDonorOrgans();
            }
        }

        if (cmd.hasOption(IS_RECEIVER_ARG)) {
            String enteredValue = cmd.getOptionValue(IS_RECEIVER_ARG).toLowerCase();
            ArrayList<String> validAnswers = new ArrayList<>(Arrays.asList("true", "false"));
            if (!validAnswers.contains(enteredValue)) {
                throw new InvalidCommand("Invalid value provided for isReceiver argument.");
            }

            profile.setIsReceiver(Boolean.parseBoolean(enteredValue));

            //For the situation where the user was a receiver, listed needed organs. Then opt out of being a receiver.
            //Then opt back into being a receiver. Their old list of organs get cleared out.
            if (!profile.isReceiver()) {
                profile.clearReceiverOrgans();
            }

        }


        if (cmd.hasOption(IS_SMOKER_ARG)) {
            String enteredValue = cmd.getOptionValue(IS_SMOKER_ARG);
            ArrayList<String> validAnswers = new ArrayList<>(Arrays.asList("true", "false"));
            if (!validAnswers.contains(enteredValue)) {
                throw new InvalidCommand("Invalid value entered for isSmoker.");
            }
            profile.setSmoker(Boolean.parseBoolean(enteredValue));
        }

        if (cmd.hasOption(BLOOD_PRESSURE_ARG)) {
            String enteredValue = cmd.getOptionValue(BLOOD_PRESSURE_ARG);
            try {
                profile.setBloodPressure(enteredValue);
            } catch (NumberFormatException e) {
                throw new InvalidCommand("Invalid number for the blood pressure argument.");
            }
        }

        if (cmd.hasOption(ALCOHOL_CONSUMPTION_ARG)) {
            boolean success = false;

            String enteredValue = cmd.getOptionValue(ALCOHOL_CONSUMPTION_ARG);
            for (AlcoholConsumptionEnum level : AlcoholConsumptionEnum.values()) {
                if (level.equals(enteredValue.toUpperCase())) {
                    profile.setAlcoholConsumption(level);
                    success = true;
                }
            }
            if (!success) {
                throw new InvalidCommand("Invalid option entered for the donors alcohol consumption");
            }
        }


        return profile;
    }


    /**
     * Creates a new clinician based on the arguments entered by the user. They supply details for about the
     * clinician.
     *
     * @param args The arguments entered by the user.
     * @return The created clinician.
     * @throws InvalidCommand Error when creating the profile. Exception contains a message of what went wrong.
     */
    private static Clinician createClinician(String[] args) throws InvalidCommand {
        CommandLine cmd;
        try {
            cmd = new DefaultParser().parse(createClinicianOpts, args);
        } catch (ParseException e) {
            throw new InvalidCommand(e.getMessage());
        }

        String username = cmd.getOptionValue(USERNAME_ARG);
        String staffId = cmd.getOptionValue(STAFF_ID_ARG);
        String firstName = cmd.getOptionValue(FIRST_NAME_ARG);

        RegionEnum region = null;
        try {
            region = RegionTypeController.getRegionType(cmd.getOptionValue(REGION_ARG));
        } catch (DoesNotExist doesNotExist) {
            LOGGER.severe(doesNotExist.getMessage());
        }
        LocalDate dateOfBirth = LocalDate.parse(cmd.getOptionValue(DOB_ARG));

        String middleName = UserController.MIDDLE_NAME_NOT_SET;
        if (cmd.hasOption(MIDDLE_NAME_ARG)) {
            middleName = cmd.getOptionValue(MIDDLE_NAME_ARG);
        }

        String lastName = UserController.LAST_NAME_NOT_SET;
        if (cmd.hasOption(LAST_NAME_ARG)) {
            lastName = cmd.getOptionValue(LAST_NAME_ARG);
        }

        String address = UserController.ADDRESS_NOT_SET;
        if (cmd.hasOption(CURRENT_ADDRESS_ARG)) {
            address = cmd.getOptionValue(CURRENT_ADDRESS_ARG);
        }

        GenderEnum gender = UserController.GENDER_NOT_SET;
        if (cmd.hasOption(GENDER_ARG)) {
            try {
                gender = GenderController.getGender(cmd.getOptionValue(GENDER_ARG));
            } catch (DoesNotExist doesNotExist) {
                throw new InvalidCommand(doesNotExist.getMessage());
            }
        }


        return new Clinician(firstName, middleName, lastName, username, address, region, gender, new Date(), dateOfBirth,
                new Hospital("DEFAULT", RegionEnum.NOT_SET, "DEFAULT", 0, 0), staffId);


    }

    /**
     * Creates a new profile based on the arguments entered by the user. The arguments give information about the
     * profile.
     *
     * @param args The arguments entered by the user.
     * @return The newly created profile.
     * @throws InvalidCommand Error when creating the profile. Has message specifying what went wrong.
     */
    private static Profile createProfile(String[] args) throws InvalidCommand {
        CommandLine cmd;
        try {
            cmd = new DefaultParser().parse(createProfileOpts, args);
        } catch (ParseException e) {
            throw new InvalidCommand(e.getMessage());
        }

        String firstName = cmd.getOptionValue(FIRST_NAME_ARG);
        LocalDate dob;
        String username = "NULL";
        try {
            dob = LocalDate.parse(cmd.getOptionValue(DOB_ARG));
        } catch (DateTimeParseException e) {
            throw new InvalidCommand(WRONG_LOCAL_DATE_FORMAT_MESSAGE);
        }
        if (cmd.hasOption(USERNAME_ARG)) {
            String enteredValue = cmd.getOptionValue(USERNAME_ARG);
            if (UserController.isUsernameUsed(enteredValue)) {
                throw new InvalidCommand("Username is already in use.");
            } else {
                username = enteredValue;
            }
        }

        Date createdDate = new Date();

        //First set all the mandatory fields since we will have values for those and then see if all the other fields
        //have values for them. If so set that field.
        Profile profile = new Profile(firstName, dob, createdDate, username);


        //Set the values for the profile using arguments from the command line.
        profile = modifyProfile(cmd, profile);

        // User enters a list of organs for donation when creating the profile for the first time.
        if (cmd.hasOption(DONOR_ORGANS_ARG)) {
            //Automatically set them as a donor
            profile.setIsDonor(true);

            String enteredValues = cmd.getOptionValue(DONOR_ORGANS_ARG);
            ArrayList<OrganEnum> organs;

            try {
                organs = OrganController.extractOrgans(enteredValues);
            } catch (DoesNotExist doesNotExist) {
                throw new InvalidCommand(doesNotExist.getMessage());
            }

            profile.setDonorOrgans(organs);
        }

        // User enters list of organs they need as a receiver when creating the profile for the first time
        if (cmd.hasOption(RECEIVER_ORGANS_ARG)) {
            // Automatically set them as receiver
            profile.setIsReceiver(true);

            String enteredValue = cmd.getOptionValue(RECEIVER_ORGANS_ARG);
            ArrayList<OrganEnum> organs;

            try {
                organs = OrganController.extractOrgans(enteredValue);
            } catch (DoesNotExist doesNotExist) {
                throw new InvalidCommand(doesNotExist.getMessage());
            }

            for (OrganEnum organ : organs) {
                profile.addReceivingOrgan(organ);
            }

        }
        return profile;
    }

    /**
     * Display all the profiles that have been created.
     */
    private static void displayAllProfiles() {
        final int NO_PROFILES = 0;

        List<Profile> profiles = ProfileRequests.getInstance().searchProfiles(AuthToken.getInstance().getToken(),
                new HashMap<>()).getBody();

        if (profiles == null) {
            displayToTerminal("There are no profiles to display.");
        } else if (profiles.size() == NO_PROFILES) {
            displayToTerminal("There are no profiles to display.");
        } else {
            for (Profile profile : profiles) {
                displayToTerminal(profile.toString());
            }
        }
    }

    /**
     * Save an action that occurred.
     *
     * @param command    The command that was executed.
     * @param args       The arguments used during the execution of the command.
     * @param annotation Short message to describe what occurred.
     * @throws IOException There was an issue saving to the file.
     */
    private static void saveAction(CommandEnum command, String[] args, String annotation) throws IOException {

        Gson gson = new Gson();
        String username;

        Options usernameOpt = new Options();
        usernameOpt.addOption(CommandController.usernameOpt);

        try {
            CommandLine cmd = new DefaultParser().parse(usernameOpt, args);
            username = cmd.getOptionValue(USERNAME_ARG);
        } catch (Exception e) {
            username = "No Username";
        }

        Action action = new Action(command, args, annotation, username);
        ActionController.addAction(action);

        String toSave = gson.toJson(ActionController.getActions());

        FileWriter fileWriter = null;
        try {
            File file = new File(ACTION_HISTORY_FILE);
            fileWriter = new FileWriter(file);
            fileWriter.write(toSave);
            fileWriter.flush();
        } catch (IOException e) {
            LOGGER.severe(e.getMessage());
        } finally {
            if (fileWriter != null) {
                fileWriter.close();
            }
        }
    }

    /**
     * Display the message to the terminal. Will determine to use either the OS's terminal or the custom
     * JavaFX terminal.
     * <p>
     * This is a legacy issue. The application was originally created using the OS's terminal.
     *
     * @param message The message to be displayed.
     */
    private static void displayToTerminal(String message) {
        if (isUsingJavaFxTerminal) {
            ControllerAccess.getTerminalSceneController().display(message);
        } else {
            // Removed deprecated code, this weird if statement is an legacy issue
        }
    }

}
