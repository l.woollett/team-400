package seng302.ModelController;

import seng302.CustomException.DoesNotExist;
import seng302.Enum.GenderEnum;
import seng302.Enum.RegionEnum;

import java.time.LocalDate;
import java.util.Date;

/**
 * Will have functionality that uses all the child classes of User.
 */
public class UserController {

    // Used to tell if the Profile does not have a value for the field. These are stored in the controller so when
    // we save the Users to the JSON these default values are not saved.
    public static final String USERNAME_NOT_SET = "";
    public static final String ALIAS_NOT_SET = "";
    public static final String FIRST_NAME_NOT_SET = "";
    public static final String MIDDLE_NAME_NOT_SET = "";
    public static final String LAST_NAME_NOT_SET = "";
    public static final String ADDRESS_NOT_SET = "";
    public static final RegionEnum REGION_NOT_SET = RegionEnum.NOT_SET;
    public static final GenderEnum GENDER_NOT_SET = null;
    public static final GenderEnum BIRTH_GENDER_NOT_SET = null;
    public static final Date MODIFIED_DATE_NOT_SET = null;
    public static final Date CREATED_DATE_NOT_SET = null;
    public static final LocalDate DATE_OF_BIRTH_NOT_SET = null;
    public static final String HOSPITAL_ID_NOT_SET = null;
    public static final String CLINICIAN_NAME_NOT_SET = null;


    /**
     * Will search to see if a username has already been taken by any any profiles.
     *
     * @param username The username to search for.
     * @return True if the user name is used, otherwise false is returned.
     */
    public static boolean doesProfileExist(String username) {
        try {
            ProfileController.getProfile(username);
            return true; // Found the profile,
        } catch (DoesNotExist doesNotExist) {
            return false; // Did not find the profile
        }
    }

    /**
     * Will search to see if a username has already been used by any clinicians.
     *
     * @param username The username that identifies a clinician.
     * @return True if the username is used, false otherwise.
     */
    public static boolean doesClinicianExist(String username) {
        try {
            ClinicianController.getClinician(username);
            return true; // The clinician exists.
        } catch (DoesNotExist doesNotExist) {
            return false; // The clinician does not exist.
        }
    }

    /**
     * Will search to see if a this username has been used by any admins.
     *
     * @param username String of username to query.
     * @return boolean true if admin exists with this username.
     */
    public static boolean doesAdminExist(String username) {
        try {
            AdminController.getAdmin(username);
            return true; // The admin exists.
        } catch (DoesNotExist doesNotExist) {
            return false; // The admin does not exist.
        }
    }

    /**
     * Will search to see if the username has been used by any of the User child classes.
     *
     * @param username The username that will be checked.
     * @return True if the username is used, false otherwise.
     */
    public static boolean isUsernameUsed(String username) {


        boolean profileExists = doesProfileExist(username);
        boolean clinicianExists = doesClinicianExist(username);
        boolean adminExists = doesAdminExist(username);

        if (profileExists || clinicianExists || adminExists) { // Found it and can end the function early.
            return true;
        }
        return false;
    }

}
