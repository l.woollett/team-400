package seng302.ModelController;


import seng302.CustomException.DoesNotExist;
import seng302.Enum.GenderEnum;

/**
 * Abstract class to provide functionality to the GenderEnum
 */
public abstract class GenderController {

    /**
     * Function that takes a string and returns a GenderEnum to match the string. Avoids having to type cast objects to
     * GenderEnum objects
     *
     * @param gender: String that needs to match one of the gender enum options
     * @return a GenderEnum to replace the string value input
     * @throws DoesNotExist The entered gender is not recognized.
     */
    public static GenderEnum getGender(String gender) throws DoesNotExist {
        GenderEnum result;
        if (gender != null) {
            gender = gender.toLowerCase();
            switch (gender) {
                case "male":
                    result = GenderEnum.MALE;
                    break;
                case "female":
                    result = GenderEnum.FEMALE;
                    break;
                case "other":
                    result = GenderEnum.OTHER;
                    break;
                case (""): {
                    result = null;
                    break;
                }
                default:
                    throw new DoesNotExist("The specified gender: " + gender + " does not exist");
            }
        } else {
            result = null;
        }
        return result;
    }
}
