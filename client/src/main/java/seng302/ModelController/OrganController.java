package seng302.ModelController;

import seng302.CustomException.DoesNotExist;
import seng302.Enum.OrganEnum;

import java.util.ArrayList;

import static seng302.Enum.OrganEnum.*;

/**
 * Provides functionality that revolves around the OrganEnum.
 */
public abstract class OrganController {

    /**
     * Retrieve the OrganEnum which corresponds to the value that was given.
     *
     * @param s The string which represents the organ.
     * @return OrganEnum which corresponds to the value that was given.
     * @throws DoesNotExist Is thrown when the entered string does not represent an option for an organ.
     */
    public static OrganEnum getOrgan(String s) throws DoesNotExist {
        s = s.toLowerCase();

        if (s.equals(LIVER.toString().toLowerCase())) {
            return LIVER;
        } else if (s.equals(KIDNEY.toString().toLowerCase())) {
            return KIDNEY;
        } else if (s.equals(PANCREAS.toString().toLowerCase())) {
            return PANCREAS;
        } else if (s.equals(HEART.toString().toLowerCase())) {
            return HEART;
        } else if (s.equals("heart valves") || s.equals("heart_valves")) {
            return HEART_VALVES;
        } else if (s.equals(LUNG.toString().toLowerCase())) {
            return LUNG;
        } else if (s.equals(INTESTINE.toString().toLowerCase())) {
            return INTESTINE;
        } else if (s.equals(CORNEA.toString().toLowerCase())) {
            return CORNEA;
        } else if (s.equals("middle ear") || s.equals("middle_ear")) {
            return MIDDLE_EAR;
        } else if (s.equals(SKIN.toString().toLowerCase())) {
            return SKIN;
        } else if (s.equals(BONE.toString().toLowerCase())) {
            return BONE;
        } else if (s.equals("bone marrow") || s.equals("bone_marrow")) {
            return BONE_MARROW;
        } else if (s.equals("connective tissue") || s.equals("connective_tissue")) {
            return CONNECTIVE_TISSUE;
        } else {
            throw new DoesNotExist("The specified organ does not exist.");
        }
    }

    /**
     * Organs are listed in square brackets, e.g. [KIDNEY,PANCREAS,CORNEA]
     *
     * @param input All the organs listed in an array with a comma being the delimiter.
     * @return ArrayList that has the same organs.
     * @throws DoesNotExist Thrown when the specified organ does not exist.
     */
    public static ArrayList<OrganEnum> extractOrgans(String input) throws DoesNotExist {
        String[] enteredOrgans = input.split(",");

        ArrayList<OrganEnum> organs = new ArrayList<>();
        for (String organ : enteredOrgans) {
            organ = organ.trim(); //Remove any extra white spaces
            organs.add(getOrgan(organ));
        }
        return organs;
    }

    /**
     * Get all the available organs as a list separated by commas.
     *
     * @return String representing all the organs available.
     */
    public static String getOrganList() {
        String message = LIVER.toString() + ", " + KIDNEY.toString() + ", " + PANCREAS.toString() + ", " + HEART.toString() +
                ", " + HEART_VALVES.toString() + ", " + LUNG.toString() + ", " + INTESTINE.toString() + ", " +
                CORNEA.toString() + ", " + MIDDLE_EAR.toString() + ", " + SKIN.toString() + ", " + BONE.toString() +
                ", " + BONE_MARROW.toString() + ", " + CONNECTIVE_TISSUE.toString();
        return message;
    }

}
