package seng302.ModelController;

import seng302.Model.Action;

import java.util.ArrayList;

/**
 * Controller to manage the Action model class. Will also keep track of all actions that have occurred.
 */
public abstract class ActionController {

    private static ArrayList<Action> actions = new ArrayList<>();

    /**
     * Add an action that occurred.
     *
     * @param action Action that occurred.
     */
    public static void addAction(Action action) {
        actions.add(action);
    }

    public static ArrayList<Action> getActions() {
        return actions;
    }
}
