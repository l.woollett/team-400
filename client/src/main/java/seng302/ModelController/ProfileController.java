package seng302.ModelController;

import seng302.CustomException.DoesNotExist;
import seng302.Enum.AlcoholConsumptionEnum;
import seng302.Enum.BloodTypeEnum;
import seng302.Enum.GenderEnum;
import seng302.Enum.OrganEnum;
import seng302.Model.Death;
import seng302.Model.Disease;
import seng302.Model.Profile;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Controller to provide functionality which uses the Profile class as well as store all the created profiles.
 */
public abstract class ProfileController {

    public static final boolean IS_DONOR_NOT_SET = false; // By default they are not donors
    public static final boolean IS_RECEIVER_NOT_SET = false; // By default they are not receivers
    public static final boolean IS_SMOKER_NOT_SET = false; // By default they are not smokers
    public static final LocalDate DATE_OF_DEATH_NOT_SET = null;
    public static final Death DEATH_NOT_SET = null;
    public static final float WEIGHT_NOT_SET = -1;
    public static final float HEIGHT_NOT_SET = -1;
    public static final float BMI_NOT_SET = -1;
    public static final BloodTypeEnum BLOOD_TYPE_NOT_SET = null;
    public static final GenderEnum BIRTH_GENDER_NOT_SET = null;
    public static final String BLOOD_PRESSURE_NOT_SET = "";
    public static final AlcoholConsumptionEnum ALCOHOL_CONSUMPTION_NOT_SET = AlcoholConsumptionEnum.NOT_SET;
    public static final ArrayList<OrganEnum> ORGAN_NOT_SET = new ArrayList<>(); // By default they are not donors, so no organs set
    public static final ArrayList<Disease> DISEASES_NOT_SET = new ArrayList<>(); // By default they do not have any chronic diseases

    //Contains all the profiles created by the user. Key is the profile ID and the value is the profile.
    private static HashMap<String, Profile> profiles = new HashMap<>();


    /**
     * Add a profile to the ArrayList profiles; which contain all the created profiles by the user.
     *
     * @param profile  The new profile to be added.
     * @param username The unique username that will be associated with the profile.
     */
    public static void addProfile(String username, Profile profile) {
        profiles.put(username, profile);
    }

    /**
     * Retrieve all the profiles that are stored.
     *
     * @return All the profiles that have been created.
     */
    public static ArrayList<Profile> getProfiles() {
        ArrayList<Profile> createdProfiles = new ArrayList<>();

        for (Profile profile : profiles.values()) {
            createdProfiles.add(profile);
        }

        return createdProfiles;
    }

    /**
     * Function to add profiles from when profiles are imported from persistent storage.
     *
     * @param importedProfiles The profiles that were saved in persistent storage that will be loaded into the app.
     */
    public static void setProfiles(ArrayList<Profile> importedProfiles) {
        profiles.clear();
        for (Profile profile : importedProfiles) {
            profiles.put(profile.getUsername(), profile);
        }
    }

    /**
     * Clear all the profiles that have been created.
     */
    public static void clearProfiles() {
        profiles.clear();
    }

    /**
     * Retrieve the specified user profile.
     *
     * @param username The username of the desired user.
     * @return The profile of the user with the given profile_id.
     * @throws DoesNotExist Thrown if the profile does not exist.
     */
    public static Profile getProfile(String username) throws DoesNotExist {
        Profile profile = profiles.get(username);
        if (profile == null) {
            throw new DoesNotExist(String.format("Profile with username %s does not exist.", username));
        } else {
            return profile;
        }
    }

    /**
     * Search through the profiles and find the profile with the specified username. Profile username should be unique.
     *
     * @param profiles The profiles we need to search through.
     * @param username The username we want to find.
     * @return The profile which has the specified username.
     * @throws DoesNotExist The specified profile, using the username, does not exist.
     */
    public static Profile searchProfileByUsername(ArrayList<Profile> profiles, String username) throws DoesNotExist {
        for (Profile profile : profiles) {
            if (profile.getUsername().equals(username)) {
                return profile;
            }
        }

        throw new DoesNotExist("Profile with the username of " + username + " was not found.");
    }

    /**
     * Search through the profiles and find the profiles with the specified first name. Case insensitive.
     *
     * @param profiles         All the profiles that need to be searched through.
     * @param desiredFirstName The first name we want to search by.
     * @return All the profiles with the same first name that was specified.
     */
    public static ArrayList<Profile> searchProfileByFirstName(ArrayList<Profile> profiles, String desiredFirstName) {
        ArrayList<Profile> matchingProfiles = new ArrayList<>();
        for (Profile profile : profiles) {

            if (profile.getFirstName() == null) {
                continue; //Skip this comparison. There is no first name to compare
            }

            if (profile.getFirstName().toLowerCase().equals(desiredFirstName.toLowerCase())) {
                matchingProfiles.add(profile);
            }
        }

        return matchingProfiles;
    }

    /**
     * Search through the profiles and find the profiles with the specified last name. Case insensitive.
     *
     * @param profiles        All the profiles that need to be searched through.
     * @param desiredLastName The first name we want to search by.
     * @return All the profiles with the same first name that was specified.
     */
    public static ArrayList<Profile> searchProfileByLastName(ArrayList<Profile> profiles, String desiredLastName) {
        ArrayList<Profile> matchingProfiles = new ArrayList<>();
        for (Profile profile : profiles) {

            if (profile.getLastName() == null) {
                continue; //Skip this comparison there is no last name to compare.
            }

            if (profile.getLastName().toLowerCase().equals(desiredLastName.toLowerCase())) {
                matchingProfiles.add(profile);
            }
        }

        return matchingProfiles;
    }

    /**
     * Search through the profiles and find the profiles with the matching desired donor status.
     *
     * @param profiles           The profiles to search through.
     * @param desiredDonorStatus The desired donor status.
     * @return All the profiles with the matching donor status.
     */
    public static ArrayList<Profile> searchProfileByDonorStatus(ArrayList<Profile> profiles, boolean desiredDonorStatus) {
        ArrayList<Profile> matchingProfiles = new ArrayList<>();
        for (Profile profile : profiles) {

            if (profile.isDonor() == desiredDonorStatus) {
                matchingProfiles.add(profile);
            }
        }

        return matchingProfiles;
    }

    /**
     * Delete the specified user.
     *
     * @param username The username associated with the profile that will be deleted.
     * @return Profile that has been removed.
     * @throws DoesNotExist The profile specified to be deleted does not exist.
     */
    public static Profile deleteProfile(String username) throws DoesNotExist {
        Profile removeTest = profiles.remove(username);
        if (removeTest == null) {
            throw new DoesNotExist("The specified profile does not exist.");
        }

        return removeTest;
    }

}
