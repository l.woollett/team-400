package seng302.GUI.Chat;

import seng302.Utilities.Style;

import java.time.LocalDateTime;

public class OutgoingMessageController extends Messagable {

    public OutgoingMessageController(String message, String senderName, LocalDateTime sentTime) {
        super(message, senderName, sentTime, OutgoingMessageController.class.getResource("outgoingMessage.fxml"),
                Style.class.getResource("/seng302/Stylesheets/OutgoingMessage.css").toExternalForm(), -1, -1);
    }
}