package seng302.GUI.Chat;

import seng302.Utilities.Style;

import java.time.LocalDateTime;

public class IncomingMessageController extends Messagable {

    public IncomingMessageController(String message, String senderName, LocalDateTime sentTime) {
        super(message, senderName, sentTime, IncomingMessageController.class.getResource("incomingMessage.fxml"),
                Style.class.getResource("/seng302/Stylesheets/IncomingMessage.css").toExternalForm(), -1, -1);
    }
}