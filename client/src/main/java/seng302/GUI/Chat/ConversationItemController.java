package seng302.GUI.Chat;

import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import seng302.Model.Chat.Conversation;

import java.awt.event.MouseEvent;
import java.io.IOException;

public class ConversationItemController extends AnchorPane {

    @FXML private AnchorPane containerAnchorPane, conversationPane;
    @FXML private Text topic, creatorUsername;
    private ChatWindowController parentController;

    public ConversationItemController(Conversation convo, ChatWindowController parentController) {
        FXMLLoader fxmlLoader = new FXMLLoader(ConversationItemController.class.getResource("conversationItem.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        this.topic.setText(convo.getConversationTopic());
        this.creatorUsername.setText(convo.getClinicianUsername());
        this.parentController = parentController;

        containerAnchorPane.addEventHandler(javafx.scene.input.MouseEvent.MOUSE_CLICKED, (EventHandler) event -> {
            parentController.setChatId(convo.getConversationId());
            parentController.setLastSelected(this);
        });
    }
}
