package seng302.GUI.Chat;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.VBox;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import seng302.Enum.ConversationStatusEnum;
import seng302.GUI.Clinician.ClinicianSceneController;
import seng302.Model.Chat.Conversation;
import seng302.Model.Chat.ConversationClinicianJoin;
import seng302.Model.Chat.Message;
import seng302.Model.Clinician;
import seng302.ServerInteracton.ServerQueries.Authentication.AuthToken;
import seng302.ServerInteracton.ServerQueries.Chat.ConversationRequests;
import seng302.ServerInteracton.ServerQueries.Chat.MessageRequests;
import seng302.Utilities.AutoRequestTimer;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ChatWindowController {

    @FXML private VBox chatBox, conversationBox;
    @FXML private TextField sendMessageText;
    @FXML private ScrollPane chatScrollPane, conversationScrollPane;

    private String fullName;
    private ConversationItemController lastSelected = null;
    private ClinicianSceneController parentController;


    private final int DELAY = -1;
    private final int INTERVAL = -1;
    private Integer chatId = null;
    /**
     * Will be used to cut down the number of messages we request. Will be used to request messages with a higher
     * ID.
     */
    private Integer lastRetrievedMessageId = null;


    public ChatWindowController() {
    }

    /**
     * Initialize the controller and start the auto request timer for getting the current chat's messages.
     */
    @FXML
    private void initialize() {
        chatScrollPane.vvalueProperty().bind(chatBox.heightProperty());

        Platform.runLater(this::loadConversations);
        AutoRequestTimer.getInstance().createAutoRequestTimer(DELAY, INTERVAL, this::handleAutoRequest);

        sendMessageText.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                sendMessage();
            }
        });
    }

    /**
     * Retrieve all the conversations the clinician is part of.
     */
    private void loadConversations() {
        Platform.runLater(() -> {
            List<Integer> unreadConversations = new ArrayList<>();
            List<Conversation> conversations = new ArrayList<>();
            try {
                conversations = ConversationRequests.getInstance().getConversations(
                        parentController.getClinician().getUsername(), ConversationStatusEnum.ACTIVE,
                        AuthToken.getInstance().getToken()).getBody();
                unreadConversations = ConversationRequests.getInstance().getUnreadChats(parentController.getClinician().getUsername(),
                        AuthToken.getInstance().getToken()).getBody();
            } catch (HttpClientErrorException e) {
                // Check the status code of the request and handle accordingly
                switch (e.getStatusCode()) {
                    case UNAUTHORIZED:
                        parentController.kickUser();
                        break;
                    default:
                        parentController.handleCriticalError(e);
                }
            } catch (HttpServerErrorException | ResourceAccessException e) {
                parentController.handleCriticalError(e);
            }

            boolean currentChatIdExists = false;
            conversationBox.getChildren().clear();

            // Populate the conversations list
            if (conversations != null) {
                for (Conversation convo : conversations) {

                    if (convo.getConversationId().equals(chatId)) {
                        currentChatIdExists = true;
                    }

                    ConversationItemController ci = new ConversationItemController(convo, this);

                    if (unreadConversations.contains(convo.getConversationId())) {
                        setConversationUnread(ci);
                    } else {
                        setConversationUnselected(ci);
                    }

                    if(convo.getConversationId().equals(chatId)) {
                        setConversationSelected(ci);
                        setLastSelected(ci);
                    }

                    conversationBox.getChildren().add(ci);

                }

            }


            if(!currentChatIdExists) {
                chatId = null;
                chatBox.getChildren().clear();
            }
        });
    }

    /**
     * Define a reference to the parent controller.
     *
     * @param parentController The parent controller for this chat controller.
     */
    public void setParentController(ClinicianSceneController parentController) {
        this.parentController = parentController;
        initPostSettingParentController();
    }

    /**
     * Initialization that occurs after the parent controller has been instantiated.
     */
    private void initPostSettingParentController() {
        Clinician clinician = parentController.getClinician();
        fullName = String.format("%s %s %s", clinician.getFirstName(), clinician.getMiddleName(),
                clinician.getLastName());
    }


    /**
     * Will send a message to the current group chat. Will not send the message if the message content is empty
     * or there is no current chat.
     */
    @FXML
    private void sendMessage() {
        final int MAX_CHAR_LEN = 4000;
        String messageContent = sendMessageText.getText();

        if (messageContent.length() > MAX_CHAR_LEN) {
            messageContent = messageContent.substring(0, MAX_CHAR_LEN);
        }

        if(!messageContent.trim().equals("") && chatId != null) {
            Message message = new Message(null, LocalDateTime.now(), parentController.getClinician().getUsername(),
                    messageContent, chatId);

            try {
                MessageRequests.getInstance().postMessage(message, AuthToken.getInstance().getToken());
                loadMessages();
                sendMessageText.clear();
            } catch (HttpClientErrorException e) {
                // Check the status code of the request and handle accordingly
                switch (e.getStatusCode()) {
                    case UNAUTHORIZED:
                        parentController.kickUser();
                        break;
                    default:
                        parentController.handleCriticalError(e);
                }
            } catch(HttpServerErrorException | ResourceAccessException e) {
                parentController.handleCriticalError(e);
            }
        }
    }

    /**
     * Function will add an incoming message to the chat area.
     *
     * @param message The message to add.
     */
    private void addIncomingMessage(Message message) {
        IncomingMessageController incomingMessage = new IncomingMessageController(message.getMessageText(),
                message.getSenderUsername(), message.getTimestamp());
        chatBox.getChildren().add(incomingMessage);
    }

    /**
     * Function will add an outgoing message to the chat area.
     *
     * @param message The message to add.
     */
    private void addOutgoingMessage(Message message) {
        OutgoingMessageController outgoingMessage = new OutgoingMessageController(message.getMessageText(),
                message.getSenderUsername(), message.getTimestamp());
        chatBox.getChildren().add(outgoingMessage);
    }

    /**
     * Handler for the auto request of getting messages in a conversation.
     * Has a lambda function which calls the two methods to load up all the conversations and the messages for the
     * currently selected conversation.
     *
     * @return null as it is in the contract.
     */
    private Boolean handleAutoRequest() {
        Platform.runLater(() -> {
            if (chatId != null) {
                loadMessages();
            }
            loadConversations();
        });
        return null;
    }

    /**
     * Will retrieve all the messages from the current group chat.
     */
    private void loadMessages() {

        if(chatId != null) {

            try {
                List<Message> messages;

                if (lastRetrievedMessageId == null) {
                    messages = MessageRequests.getInstance().getMessages(chatId,
                            AuthToken.getInstance().getToken()).getBody();
                } else {
                    messages = MessageRequests.getInstance().getMessages(chatId, lastRetrievedMessageId,
                            AuthToken.getInstance().getToken()).getBody();
                }

                messages.sort(Comparator.comparing(Message::getMessageId));
                displayMessages(messages);
                updateLastRead();
            } catch (HttpClientErrorException e) {
                // Check the status code of the request and handle accordingly
                switch (e.getStatusCode()) {
                    case UNAUTHORIZED:
                        parentController.kickUser();
                        break;
                    default:
                        parentController.handleCriticalError(e);
                }
            } catch (HttpServerErrorException | ResourceAccessException e) {
                parentController.handleCriticalError(e);
            }
        } else {
            chatBox.getChildren().clear();
        }
    }

    /**
     * Will iterate through a list of message and display them in the chat area.
     *
     * @param messages A list of message.
     */
    private void displayMessages(List<Message> messages) {
        Platform.runLater(() -> {
            String currentClinicianUsername = parentController.getClinician().getUsername();
            for(Message message : messages) {
                if (message.getSenderUsername().equalsIgnoreCase(currentClinicianUsername)) {
                    addIncomingMessage(message);
                } else {
                    addOutgoingMessage(message);
                }
            }

            if (!messages.isEmpty()) {
                int lastMessageIndex = messages.size() - 1;
                lastRetrievedMessageId = messages.get(lastMessageIndex).getMessageId();
            }
        });
    }

    /**
     * Clears the vbox of messages and resets the lst viewed index.
     */
    private void clearMessages() {
        chatBox.getChildren().clear();
        lastRetrievedMessageId = -1;
    }

    /**
     * Sets the chat Id and loads the new messages.
     * This is called from the fxml controller being clicked.
     * @param id Integer of convoId.
     */
    public void setChatId (Integer id) {
        if (lastSelected != null){
            setConversationUnselected(lastSelected);
        }
        chatId = id;
        clearMessages();
        loadMessages();
    }

    /**
     * updates the last read field.
     */
    private void updateLastRead() {
        ConversationClinicianJoin conversationClinicianJoin = new ConversationClinicianJoin(chatId, parentController.getClinician().getUsername());
        try {
            ConversationRequests.getInstance().updateLastRead(conversationClinicianJoin, AuthToken.getInstance().getToken());
        } catch (HttpClientErrorException e) {
            // Check the status code of the request and handle accordingly
            switch (e.getStatusCode()) {
                case UNAUTHORIZED:
                    parentController.kickUser();
                    break;
                case BAD_REQUEST:
                    // Do nothing, conversaion may not exist anymore
                    break;
                default:
                    parentController.handleCriticalError(e);
            }
        } catch (HttpServerErrorException | ResourceAccessException e) {
            parentController.handleCriticalError(e);
        }
    }

    /**
     * sets the last selected.
     * @param conversationItemController the controller to set.
     */
    public void setLastSelected(ConversationItemController conversationItemController) {
        if (lastSelected != null) {
            setConversationUnselected(lastSelected);
        }
        this.lastSelected = conversationItemController;
        setConversationSelected(lastSelected);
    }

    /**
     * Will change the CSS to show the conversation is selected.
     *
     * @param conversationItemController The conversation for the CSS to be applied to.
     */
    private void setConversationSelected(ConversationItemController conversationItemController){
        conversationItemController.getStylesheets().clear();
        conversationItemController.getStylesheets().add("/seng302/Stylesheets/ConversationItemSelected.css");
    }

    /**
     * Will change the CSS to show the conversation is not selected.
     *
     * @param conversationItemController The conversation for the CSS to be applied to.
     */
    private void setConversationUnselected(ConversationItemController conversationItemController){
        conversationItemController.getStylesheets().clear();
        conversationItemController.getStylesheets().add("/seng302/Stylesheets/ConversationItem.css");
    }

    /**
     * Will change the CSS to show the conversation has not been read.
     * @param conversationItemController The conversation for the CSS to be applied to.
     */
    private void setConversationUnread(ConversationItemController conversationItemController){
        conversationItemController.getStylesheets().clear();
        conversationItemController.getStylesheets().add("/seng302/Stylesheets/ConversationItemUnread.css");
    }
}
