package seng302.GUI.Chat;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;

public abstract class Messagable extends AnchorPane {
    /**
     * Default value for the width of the container the whole message is contained in.
     */
    private static final int DEFAULT_CONTAINER_WIDTH = 500;
    /**
     * Default value for the width of the message contained inside of the container.
     */
    private static final int DEFAULT_MESSAGE_WIDTH = 250;

    /**
     * The AnchorPane that encapsulates the whole message.
     */
    @FXML protected AnchorPane containerAnchorPane;
    /**
     * The VBox that contains the actual message shown. This is separate from the containerAnchorPane so that incoming
     * and outgoing messages can be handled differently.
     */
    @FXML protected VBox mainVBox;
    /**
     * The text of the message being sent.
     */
    @FXML protected Text messageText;
    /**
     * The text element containing the name of the person who sent the message.
     */
    @FXML protected Text senderName;
    /**
     * The text element containing the time the message was sent.
     */
    @FXML protected Text sentTime;

    /**
     * A constructor that creates a message. It applies size constraints, css styling, and sets the text of
     * the message.
     *
     * @param message The text to be displayed in the message.
     * @param senderName The name of the sender of the message.
     * @param sentTime The time the message was sent.
     * @param urIfxmlName The name of the message .fxml file.
     * @param css The css style to be applied to the message.
     * @param containerWidth The width of the container the whole message is contained in.
     *                       Passing in -1 indicates that DEFAULT_CONTAINER_WIDTH should be used.
     * @param messageWidth The width of the message contained inside of the container.
     *                     Passing in -1 indicates that DEFAULT_MESSAGE_WIDTH should be used.
     */
    protected Messagable(String message, String senderName, LocalDateTime sentTime, URL urIfxmlName, String css, int containerWidth, int messageWidth) {
        FXMLLoader fxmlLoader = new FXMLLoader(urIfxmlName);
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        // Set the message fields
        this.messageText.setText(message);
        this.senderName.setText(senderName);
        this.sentTime.setText(getDateAsFormattedString(sentTime));

        // Setup the container size
        if (containerWidth == -1) {
            containerAnchorPane.setMaxWidth(DEFAULT_CONTAINER_WIDTH);
        } else {
            containerAnchorPane.setMaxWidth(containerWidth);
        }

        // Setup the message size
        if (messageWidth == -1) {
            mainVBox.setMaxWidth(DEFAULT_MESSAGE_WIDTH);
            messageText.setWrappingWidth(DEFAULT_MESSAGE_WIDTH - 20);
        } else {
            mainVBox.setMaxWidth(messageWidth);
            messageText.setWrappingWidth(messageWidth - 20);
        }


        // Style the VBox to look like a message
        mainVBox.getStylesheets().add(css);
    }

    /**
     * Returns a string representation of the given LocalDateTime given to one of the formats below:
     *     DD/MM/YY
     *     HH:MM:SS
     *
     * If the date the message was sent is the same as today then the time will be returned.
     * If the date th message was sent is before today then the date is returned.
     *
     * @param ldt The LocalDateTime to be converted.
     * @return A formatted string representing the date or the time of ltd.
     */
    protected String getDateAsFormattedString(LocalDateTime ldt) {
        LocalDateTime now = LocalDateTime.now();
        String time = "";

        String hoursString;
        String minuteString;

        int hours = ldt.getHour();
        int minutes = ldt.getMinute();
        if (hours < 10) {
            hoursString = "0" + hours;
        } else {
            hoursString = Integer.toString(hours);
        }

        if (minutes < 10) {
            minuteString = "0" + minutes;
        } else {
            minuteString = Integer.toString(minutes);
        }

        if (now.toLocalDate().equals(ldt.toLocalDate())) {
            // Only show the time
            time = String.format("%s:%s", hoursString, minuteString);
        } else {
            // Only show the date
            time = ldt.getDayOfMonth() + "/" + ldt.getMonthValue() + "/" + ldt.getYear();
        }
        return time;
    }
}
