package seng302.GUI.Notifications;

import javafx.scene.control.Alert;


/**
 * Generic class that will create a generic dialog to inform user of a string.
 */
public abstract class AlertDialog {

    /**
     * Displays an alert box with the given title and content. Will show until closed manually.
     *
     * @param title   The title of the alert box
     * @param message The content of the alert box
     */
    public static void showAndWait(String title, String message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);

        alert.showAndWait();
    }
}
