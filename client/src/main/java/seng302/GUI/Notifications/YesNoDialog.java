package seng302.GUI.Notifications;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;


/**
 * Generic class that will create a generic dialog to inform user of an action and gives them a chance to continue or
 * they can cancel the current action.
 */
public abstract class YesNoDialog {

    /**
     * Displays an alert box with the given title and content. Will show until closed manually.
     *
     * @param title   The title of the alert box
     * @param message The content of the alert box
     * @return boolean logout confirmed
     */
    public static boolean confirmLogout(String title, String message) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(title);

        alert.setHeaderText(null);
        alert.setContentText(message);

        alert.showAndWait();

        if (alert.getResult().equals(ButtonType.OK)) {
            return true;
        } else {
            return false;
        }
    }
}
