package seng302.GUI.Notifications;

import javafx.util.Duration;
import org.controlsfx.control.Notifications;

/**
 * Wrapper class for the ControlsFX Notifications. This way we can make changes globally and avoid having to constantly
 * build the notification each time we want to display something.
 */
public class PushNotification {

    private String title;
    private String message;
    private Duration duration;

    /**
     * Stores the values that will be used to display a Push Notification when calling the show() method.
     *
     * @param title   The title of the push notification.
     * @param message The message to be displayed in the notification.
     */
    public PushNotification(String title, String message) {
        this.title = title;
        this.message = message;
        this.duration = new Duration(2000); // The notification will last for one second
    }

    /**
     * Will show the push notification to the user.
     */
    public void show() {
        Notifications.create()
                .title(this.title)
                .text(this.message)
                .hideAfter(this.duration)
                .darkStyle()
                .showInformation();
    }

    /**
     * Will show an error notification to the user
     */
    public void showError(){
        Notifications.create()
                .title(this.title)
                .text(this.message)
                .hideAfter(this.duration)
                .darkStyle()
                .showError();
    }
}
