package seng302.GUI;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import seng302.Enum.GenderEnum;
import seng302.Enum.RegionEnum;
import seng302.GUI.Notifications.AlertDialog;
import seng302.Model.Clinician;
import seng302.Model.Hospital;
import seng302.ServerInteracton.ServerQueries.Authentication.AuthToken;
import seng302.ServerInteracton.ServerQueries.ClinicianRequests;
import seng302.Utilities.Undo_Redo.*;

import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import static seng302.Utilities.Style.*;
import static seng302.Utilities.Tooltips.*;
import static seng302.Utilities.Validator.*;

/**
 * Controls all functionality of the create a new user GUI interface. This includes error checking and validation.
 */
public class CreateClinicianController implements Initializable {

    private static final Logger LOGGER = Logger.getLogger(CreateClinicianController.class.getName());
    @FXML
    private AnchorPane createUserPane;
    @FXML
    private ChoiceBox<Enum> regionChoiceBox, genderChoiceBox;
    @FXML
    private ChoiceBox<Hospital> organisationChoiceBox;
    @FXML
    private DatePicker dateOfBirthDatePicker;
    @FXML
    private TextField usernameTextField, firstNameTextField, middleNameTextField, lastNameTextField,
            staffIdTextField, addressTextField;
    @FXML
    private Button createUserUndo, createUserRedo;
    private boolean isOrganisationValid, isStaffIdValid, isFirstNameValid, isMiddleNameValid, isLastNameValid,
            isDateOfBirthValid, isAddressValid, isUsernameValid, undoRedoEvent = true;
    private UndoRedoController undoRedoController;
    private List<Hospital> hospitals;

    public CreateClinicianController() {
        //Constructor
    }

    //to do add staff id and organisation

    /**
     * Calls all initialisation functions for the create user GUI.
     *
     * @param location  Unused parameter present so signature matches.
     * @param resources Unused parameter present so signature matches.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Initialise all GUI elements with tooltips and error checking listeners
        this.undoRedoController = new UndoRedoController();
        undoRedoEvent = true;
        initializeUsernameTextField();
        initializeFirstNameTextField();
        initializeDateOfBirthPicker();
        initializeStaffIdTextField();
        updateUndoRedoButtons();
        undoRedoEvent = false;
    }

    /**
     * Pass in the hospitals and initialise the choice box for the hospitals.
     *
     * @param hospitals List of existing hospitals.
     */
    public void initHospitals(List<Hospital> hospitals) {
        this.hospitals = hospitals;

        initializeOrganisationChoiceBox();
    }

    /**
     * Initialises the staff id text field. It adds a listener that visually changes the field to show if
     * correct/incorrect data has been input. Also adds an appropriate tooltip to match this. Only accepts staff id
     * that are 1-20 characters long. The middle name can only contain alphabet characters, hyphens, spaces, and
     * apostrophes. Any non-alphabet character must be between two alphabet characters.
     */
    private void initializeStaffIdTextField() {
        staffIdTextField.setTooltip(createNewTooltip(VALID_STAFF_ID_TOOLTIP_TEXT));
        staffIdTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    updateStaffIdTextField();
                    if (!oldValue.equals(newValue) && !undoRedoEvent) {
                        addTextToDeque(staffIdTextField, oldValue);
                    }
                })
        );
    }

    /**
     * Initializes the username text field
     */
    private void initializeUsernameTextField() {
        usernameTextField.setTooltip(createNewTooltip(VALID_USERNAME_TOOLTIP_TEXT));
        usernameTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    updateUsernameTextField();
                    if (!oldValue.equals(newValue) && !undoRedoEvent) {
                        addTextToDeque(usernameTextField, oldValue);
                    }
                })
        );
    }

    /**
     * Tests to see if the current username is valid and will apply appropriate css styles.
     */
    private void updateUsernameTextField() {
        String currentValue = usernameTextField.getText();
        isUsernameValid = isValidUsername(currentValue);
        applyTextFieldStyle(usernameTextField, isUsernameValid, true, VALID_USERNAME_TOOLTIP_TEXT,
                INVALID_USERNAME_TOOLTIP_TEXT, currentValue);
    }

    /**
     * Will test to see if the current value is valid and will apply an appropriate CSS style and tooltip.
     */
    private void updateStaffIdTextField() {
        String currentValue = staffIdTextField.getText();
        isStaffIdValid = isValidStaffId(currentValue);
        applyTextFieldStyle(staffIdTextField, isStaffIdValid, true, VALID_STAFF_ID_TOOLTIP_TEXT,
                INVALID_STAFF_ID_TOOLTIP_TEXT, currentValue);
    }


    /**
     * Initializes the ChoiceBox to select which hospital/organisation the clinician belongs to.
     */
    private void initializeOrganisationChoiceBox() {
        if (hospitals == null) {
            LOGGER.severe("The method initHospitals has not been called");
        }

        organisationChoiceBox.setItems(FXCollections.observableArrayList(hospitals));
        organisationChoiceBox.setConverter(new StringConverter<Hospital>() {
            @Override
            public String toString(Hospital hospital) {
                return hospital.getName();
            }

            @Override
            public Hospital fromString(String hospitalName) {
                for (Hospital hospital : hospitals) {
                    if (hospital.getName().equals(hospitalName)) {
                        return hospital;
                    }
                }

                LOGGER.severe("The provided name did not match any hospitals.");
                return null;
            }
        });


        organisationChoiceBox.valueProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    updateOrganisationChoiceBox();
                    if ((newValue != null && !undoRedoEvent) &&
                            (oldValue == null || (!oldValue.equals(newValue)))) {
                        pushHospitalChoiceBoxToUndoDeque(organisationChoiceBox, oldValue);

                    }
                }));

    }

    /**
     * Add the hospital choicebox to the undo redo stack.
     *
     * @param choiceBox The choicebox which has all the hospitals listed.
     * @param oldValue  The previous value in the choicebox.
     */
    private void pushHospitalChoiceBoxToUndoDeque(ChoiceBox<Hospital> choiceBox, Hospital oldValue) {
        undoRedoController.pushToUndoDeque(new UndoableHospitalChoiceBox(choiceBox, oldValue));
        updateUndoRedoButtons();
    }

    /**
     * Will test to see if the current value is valid and will apply an appropriate CSS style and tooltip.
     */
    private void updateOrganisationChoiceBox() {
        Hospital currentValue = organisationChoiceBox.getValue();
        isOrganisationValid = currentValue != null;
        applyChoiceBoxStyle(organisationChoiceBox, isOrganisationValid, false, currentValue);
    }

    /**
     * Initialises the first name text field. It adds a listener that visually changes the field to show if
     * correct/incorrect data has been input. Also adds an appropriate tooltip to match this.
     */
    private void initializeFirstNameTextField() {
        // Initialise tooltip
        firstNameTextField.setTooltip(createNewTooltip(INVALID_FIRST_NAME_TOOLTIP_TEXT));
        firstNameTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    updateFirstNameTextField();
                    if (!oldValue.equals(newValue) && !undoRedoEvent) {
                        addTextToDeque(firstNameTextField, oldValue);
                    }
                })
        );
    }

    /**
     * Will test to see if the first name text field holds a valid value, will then apply an appropriate CSS style and
     * tooltip.
     */
    private void updateFirstNameTextField() {
        String currentValue = firstNameTextField.getText();
        isFirstNameValid = isValidFirstName(currentValue);
        applyTextFieldStyle(firstNameTextField, isFirstNameValid, false, VALID_FIRST_NAME_TOOLTIP_TEXT,
                INVALID_FIRST_NAME_TOOLTIP_TEXT, currentValue);
    }

    /**
     * Initialises the middle name text field. It adds a listener that visually changes the field to show if
     * correct/incorrect data has been input. Also adds an appropriate tooltip to match this. Only accepts middle names
     * that are 1-20 characters long. The middle name can only contain alphabet characters, hyphens, spaces, and
     * apostrophes. Any non-alphabet character must be between two alphabet characters.
     */
    private void initializeMiddleNameTextField() {
        middleNameTextField.setTooltip(createNewTooltip(INVALID_MIDDLE_NAME_TOOLTIP_TEXT));
        middleNameTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    updateMiddleNameTextField();
                    if (!oldValue.equals(newValue) && !undoRedoEvent) {
                        addTextToDeque(middleNameTextField, oldValue);
                    }
                })
        );
    }

    /**
     * Will test to see if the current value is valid and will apply an appropriate CSS style and tooltip.
     */
    private void updateMiddleNameTextField() {
        String currentValue = middleNameTextField.getText();
        isMiddleNameValid = isValidMiddleName(currentValue);
        applyTextFieldStyle(middleNameTextField, isMiddleNameValid, true, VALID_MIDDLE_NAME_TOOLTIP_TEXT,
                INVALID_MIDDLE_NAME_TOOLTIP_TEXT, currentValue);
    }

    /**
     * Initialises the last name text field. It adds a listener that visually changes the field to show if
     * correct/incorrect data has been input. Also adds an appropriate tooltip to match this.
     */
    private void initializeLastNameTextField() {
        // Initialise tooltip
        lastNameTextField.setTooltip(createNewTooltip(INVALID_LAST_NAME_TOOLTIP_TEXT));
        lastNameTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    updateLastNameTextField();
                    if (!oldValue.equals(newValue) && !undoRedoEvent) {
                        addTextToDeque(lastNameTextField, oldValue);
                    }
                })
        );
    }

    /**
     * Will test to see if the current value for the last name is valid and will apply an appropriate CSS style and
     * tooltip.
     */
    private void updateLastNameTextField() {
        String currentValue = lastNameTextField.getText();
        isLastNameValid = isValidLastName(currentValue);
        applyTextFieldStyle(lastNameTextField, isLastNameValid, true, VALID_LAST_NAME_TOOLTIP_TEXT,
                INVALID_LAST_NAME_TOOLTIP_TEXT, currentValue);
    }

    /**
     * Initialises the address text field. It adds a listener that visually changes the field to show if
     * correct/incorrect data has been input. Also adds an appropriate tooltip to match this.
     */
    private void initializeAddressTextField() {
        // Initialise tooltip
        addressTextField.setTooltip(createNewTooltip(INVALID_ADDRESS_TOOLTIP_TEXT));
        addressTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    updateAddressTextField();
                    if (!oldValue.equals(newValue) && !undoRedoEvent) {
                        addTextToDeque(addressTextField, oldValue);
                    }
                })
        );
    }

    /**
     * Will test to see if the address value is currently valid and will apply appropriate CSS styles and tooltips.
     */
    private void updateAddressTextField() {
        String currentValue = addressTextField.getText();
        isAddressValid = isValidAddress(currentValue);
        applyTextFieldStyle(addressTextField, isAddressValid, true, VALID_ADDRESS_TOOLTIP_TEXT,
                INVALID_ADDRESS_TOOLTIP_TEXT, currentValue);
    }

    /**
     * Set the DOB date picker to only allow selection through the built in calendar and sets the default date to be
     * 01-01-1990. Also adds live error checking and visual feedback through a listener. DOB bust be between 01-01-1900
     * and today's date.
     */
    private void initializeDateOfBirthPicker() {
        // Create default date for the date of birth date picker
        LocalDate defaultDate = LocalDate.parse("01-01-1990", DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        isDateOfBirthValid = true; // We are initializing the date of birth to a valid date. So set isValid to be true
        dateOfBirthDatePicker.setTooltip(createNewTooltip(INVALID_DATE_OF_BIRTH_TOOLTIP_TEXT));
        dateOfBirthDatePicker.valueProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    updateDateOfBirthPickerStyle();
                    if (((oldValue == null) || (!oldValue.equals(newValue))) && !undoRedoEvent) {
                        addDatePickerToDeque(dateOfBirthDatePicker, oldValue);
                    }
                }
                ));
        // Set value after the listener was created so the date picker is green showing the default date of birth is valid.
        if (dateOfBirthDatePicker.getValue() == null) {
            undoRedoEvent = true;
            dateOfBirthDatePicker.setValue(defaultDate);
            undoRedoEvent = false;
        }
    }

    /**
     * Will test to see if the current value set in the dateOfBirth picker is valid, will apply the appropriate CSS
     * style and tooltip.
     */
    private void updateDateOfBirthPickerStyle() {
        LocalDate currentValue = dateOfBirthDatePicker.getValue();
        isDateOfBirthValid = isValidDateOfBirth(currentValue);
        applyDatePickerStyle(dateOfBirthDatePicker, isDateOfBirthValid, false,
                VALID_DATE_OF_BIRTH_TOOLTIP_TEXT, INVALID_DATE_OF_BIRTH_TOOLTIP_TEXT, currentValue);

        // The date of birth also affects the validity of the date of death, must update as well
    }


    /**
     * Populates the gender choice box with all possible genders and adds a listener that makes the box turn green to
     * indicate a gender has been selected.
     */
    private void initializeGenderChoiceBox() {
        genderChoiceBox.setItems(FXCollections.observableArrayList(
                GenderEnum.values()));
        genderChoiceBox.valueProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    updateGenderChoiceBox();
                    if (((oldValue == null) || (!oldValue.equals(newValue))) && !undoRedoEvent) {
                        addEnumChoiceBoxToDeque(genderChoiceBox, oldValue);
                    }
                })
        );
        genderChoiceBox.setValue(GenderEnum.NOT_SET);
    }

    /**
     * Test to see if the value is valid and set the appropriate CSS style and tooltip.
     */
    private void updateGenderChoiceBox() {
        Object currentValue = genderChoiceBox.getValue();
        applyChoiceBoxStyle(genderChoiceBox, true, true, currentValue);
    }


    /**
     * Update the style applied to the region choice box.
     */
    private void updateRegionChoiceBox() {
        Object currentValue = regionChoiceBox.getValue();
        applyChoiceBoxStyle(regionChoiceBox, true, true, currentValue);
    }

    /**
     * Populates the region choice box with all possible NZ regions and adds a listener that makes the box turn green to
     * indicate a region has been selected.
     */
    private void initializeRegionChoiceBox() {
        ObservableList<Enum> regionEnumOptions = FXCollections.observableArrayList();
        regionEnumOptions.addAll(RegionEnum.values());
        regionChoiceBox.setItems(regionEnumOptions);
        regionChoiceBox.valueProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    updateRegionChoiceBox();
                    if (((oldValue == null) || (!oldValue.equals(newValue))) && !undoRedoEvent) {
                        addEnumChoiceBoxToDeque(regionChoiceBox, oldValue);
                    }
                })
        );
        regionChoiceBox.setValue(RegionEnum.NOT_SET);
    }


    /**
     * Creates a clinician with the currently entered details. It requires at minimum a valid username, first name, and DOB
     * to successfully create a new user. If one of the mandatory fields is missing then the field will become red and
     * an alert box will pop-up telling the user they need to complete all mandatory fields. Will exit the create user
     * stage once the profile was successfully created and added to the rest of the profiles.
     */
    @FXML
    private void createClinicianButtonClicked() {
        if (isUsernameValid && isFirstNameValid && isDateOfBirthValid && isStaffIdValid && isOrganisationValid) {
            // Mandatory fields
            String username = usernameTextField.getText();
            String firstName = firstNameTextField.getText();
            LocalDate dateOfBirth = dateOfBirthDatePicker.getValue();
            Hospital hospital = organisationChoiceBox.getValue();
            String staffId = staffIdTextField.getText();
            Date createdDate = new Date();



            // Create the clinician and add it to the loaded profiles
            Clinician clinician =
                    new Clinician(firstName, "", "", username, "",
                            RegionEnum.NOT_SET, GenderEnum.NOT_SET, createdDate, dateOfBirth, hospital,
                            staffId);


            if (ControllerAccess.getAdminSceneController() != null) {
                ControllerAccess.getAdminRolesTabController().pushClinicianChangeToUndoDeque(null, clinician, null);
            }

            HttpStatus statusCode = HttpStatus.OK;
            try {
                ClinicianRequests.getInstance().postClinician(clinician, AuthToken.getInstance().getToken());
                close(); // Close this stage after the user was successfully created.
            } catch (HttpClientErrorException e) {
                LOGGER.severe(e.getMessage());
                statusCode = e.getStatusCode();
            }


            if (statusCode == HttpStatus.BAD_REQUEST || statusCode == HttpStatus.INTERNAL_SERVER_ERROR) {
                String title = "More detail required";
                String content = "The username is already taken/ The staff id and organisation is already used";
                AlertDialog.showAndWait(title, content);
            } else {

            }
        } else {
            String title = "More detail required";
            String content = "You must complete the mandatory fields/ Input proper value.";
            AlertDialog.showAndWait(title, content);
        }
    }


    /**
     * Exit the create user stage because they want to stop creating a user profile.
     */
    @FXML
    private void cancelButtonClicked() {
        close();
    }

    /**
     * Method called when the create user window is closed.
     */
    private void close() {
        Stage createUserStage = (Stage) createUserPane.getScene().getWindow();
        createUserStage.close();
    }


    /**
     * Function creates an UndoableTextField object and pushes to the undo deque.
     *
     * @param textField TextField: that we want to restore.
     * @param oldValue  String: the oldValue to restore to.
     */
    private void addTextToDeque(TextField textField, String oldValue) {
        undoRedoController.pushToUndoDeque(new UndoableTextField(textField, oldValue));
        updateUndoRedoButtons();
    }

    /**
     * Function creates an UndoableDatePicker object and pushes to the undo deque.
     *
     * @param datePicker DatePicker: that we want to restore.
     * @param oldValue   LocalDate: the oldValue to restore to.
     */
    private void addDatePickerToDeque(DatePicker datePicker, LocalDate oldValue) {
        undoRedoController.pushToUndoDeque(new UndoableDatePicker(datePicker, oldValue));
        updateUndoRedoButtons();
    }

    /**
     * Function creates an UndoableEnumChoiceBox object and pushes to the undo deque.
     *
     * @param choiceBox ChoiceBox: that we want to restore.
     * @param oldValue  Enum: the oldValue to restore to.
     */
    private void addEnumChoiceBoxToDeque(ChoiceBox<Enum> choiceBox, Enum oldValue) {
        undoRedoController.pushToUndoDeque(new UndoableEnumChoiceBox(choiceBox, oldValue));
        updateUndoRedoButtons();
    }


    /**
     * Redo event handler for when redo is pressed.
     */
    @FXML
    private void redoEventCreateUser() {
        undoRedoEvent = true;
        this.undoRedoController.redo();
        undoRedoEvent = false;
        updateUndoRedoButtons();
    }

    /**
     * Undo event handler for when undo is pressed.
     */
    @FXML
    private void undoEventCreateUser() {
        undoRedoEvent = true;
        this.undoRedoController.undo();
        undoRedoEvent = false;
        updateUndoRedoButtons();
    }

    /**
     * Updates the undo redo buttons to be usable if the undo redo stacks are not empty.
     */
    private void updateUndoRedoButtons() {
        createUserUndo.setVisible(true);
        createUserRedo.setVisible(true);
    }

    /**
     * Function to get the UndoRedoController, mainly used for testing
     *
     * @return UndoRedoController for this object
     */
    public UndoRedoController getUndoRedoController() {
        return undoRedoController;
    }
}
