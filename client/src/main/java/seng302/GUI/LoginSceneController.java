package seng302.GUI;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.springframework.web.client.HttpClientErrorException;
import seng302.GUI.Admin.AdminSceneController;
import seng302.GUI.Clinician.ClinicianSceneController;
import seng302.GUI.Notifications.PushNotification;
import seng302.GUI.Profile.ProfileSceneController;
import seng302.Model.Admin;
import seng302.Model.Clinician;
import seng302.Model.Profile;
import seng302.ServerInteracton.ServerQueries.AdminRequests;
import seng302.ServerInteracton.ServerQueries.Authentication.AuthToken;
import seng302.ServerInteracton.ServerQueries.Authentication.LoginRequest;
import seng302.ServerInteracton.ServerQueries.Authentication.LoginRequestBody;
import seng302.ServerInteracton.ServerQueries.Authentication.Role;
import seng302.ServerInteracton.ServerQueries.ClinicianRequests;
import seng302.ServerInteracton.ServerQueries.ProfileRequests;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * Controller used to control the user login screen.
 */
public class LoginSceneController {

    private static final int CLINICIAN_ADMIN_PAGE_WIDTH = 1200;
    private static final int CLINICIAN_PAGE_HEIGHT = 600;
    private static final int ADMIN_PAGE_HEIGHT = 670;
    private static Clinician activeClinician = null;
    private static Admin activeAdmin = null;
    private final Logger LOGGER = Logger.getLogger(getClass().getName());
    @FXML
    private Button loginButton;
    @FXML
    private GridPane loginPane;
    @FXML
    private TextField usernameField;
    @FXML
    private TextField passwordField;

    /**
     * Used to reset the active users so there is no conflict when a different user logs out then another logs in of
     * different type.
     */
    public static void resetActiveUsers() {
        activeClinician = null;
        activeAdmin = null;
    }

    /**
     * Returns whether the a clinician is currently active.
     *
     * @return boolean whether current user is clinician or otherwise.
     */
    public static boolean isActiveClinician() {
        return activeClinician != null;
    }

    /**
     * Sets the currently active clinician.
     *
     * @param clinician The Clinician to be set.
     */
    public static void setActiveClinician(Clinician clinician) {
        activeClinician = clinician;
    }

    /**
     * Returns whether the a admin is currently active.
     *
     * @return boolean whether current user is admin or otherwise.
     */
    public static boolean isActiveAdmin() {
        return activeAdmin != null;
    }

    /**
     * Sets the currently active admin.
     *
     * @param admin The Admin to be set.
     */
    private static void setActiveAdmin(Admin admin) {
        activeAdmin = admin;
    }

    /**
     * Opens up the create user window.
     */
    @FXML
    private void signUp() {
        Stage signUpStage = new Stage();
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("createUserLogin.fxml"));
            Parent signUpRoot = loader.load();
            Scene signUpScene = new Scene(signUpRoot);
            signUpStage.setScene(signUpScene);
            signUpStage.setTitle("Sign Up");
            signUpStage.setResizable(true);
            signUpStage.initModality(Modality.APPLICATION_MODAL);
            CreateUserLoginController controller = loader.getController();
            controller.setParentController(this);
            signUpStage.show();

        } catch (IOException e) {
            LOGGER.severe(e.getMessage());
        }
    }

    /**
     * Called after a profile is successfully executed, automatically logs in the user.
     *
     * @param username The username for the newly created profile.
     */
    public void postProfileCreation(String username) {
        try {
            LoginRequestBody loginRequestBody = new LoginRequestBody(username, null);
            LoginRequest loginRequest = new LoginRequest();

            AuthToken authToken = loginRequest.postLogin(loginRequestBody).getBody();
            assert authToken != null;

            AuthToken.getInstance().init(authToken.getToken(), authToken.getRole());
            Profile loggedInProfile = ProfileRequests.getInstance().getProfile(username, AuthToken.getInstance().getToken())
                    .getBody();

            swapToProfileScene(loggedInProfile, true);
            close();
        } catch (HttpClientErrorException e) {
            LOGGER.severe("Login after profile creation failed.");
        } catch (NullPointerException e){
            //Closing the pane had a nullpointer.
        }
    }

    /**
     * Method called when the create user window is closed.
     */
    private void close() {
        Stage createUserStage = (Stage) loginPane.getScene().getWindow();
        createUserStage.close();
    }

    /**
     * Called to show the login stage.
     */
    public void show() {
        Stage loginStage = (Stage) loginPane.getScene().getWindow();
        loginStage.show();
    }

    /**
     * Method will swap the login scene to the profile scene.
     *
     * @param profile          The profile of the logged in user.
     * @param isProfileCreated Is set to true when the profile has been created and need indicate the profile has not
     *                         been saved yet.
     */
    private void swapToProfileScene(Profile profile, boolean isProfileCreated) {
        Parent mainRoot;
        try {
            Stage mainStage = (Stage) loginPane.getScene().getWindow();

            FXMLLoader loader = new FXMLLoader(ProfileSceneController.class.getResource("profile.fxml"));
            mainRoot = loader.load();
            Scene scene = new Scene(mainRoot, 1025, 650);

            mainStage.setTitle("Profile");
            mainStage.setScene(scene);

            ProfileSceneController controller = loader.getController();
            controller.initParentStageReference();
            controller.setCurrentProfile(profile);
            controller.initStageTitle();
            controller.updateUnsavedChangesIndicator(false, false, isProfileCreated);

        } catch (IOException e) {
            LOGGER.severe(e.getMessage());
        }
    }

    /**
     * Method will swap the login scene to the clinician scene.
     *
     * @param clinician The logged in clinician.
     */
    private void swapToClinicianScene(Clinician clinician) {
        Parent mainRoot;
        try {
            Stage mainStage = (Stage) loginPane.getScene().getWindow();

            FXMLLoader loader = new FXMLLoader(ClinicianSceneController.class.getResource("clinician.fxml"));
            mainRoot = loader.load();
            Scene scene = new Scene(mainRoot, CLINICIAN_ADMIN_PAGE_WIDTH, CLINICIAN_PAGE_HEIGHT);

            mainStage.setTitle("Clinician");
            mainStage.setScene(scene);

            ClinicianSceneController controller = loader.getController();
            controller.initParentStageReference();
            controller.setClinician(clinician);
            controller.initStageTitle();

        } catch (IOException e) {

            LOGGER.severe(e.getMessage());
        }
    }

    //------- Admin / Clinician State methods ------------------------------------------------------------------------//

    /**
     * Method to swap the login scene ot the admin scene.
     */
    private void swapToAdminScene() {
        Parent mainRoot;
        try {
            Stage mainStage = (Stage) loginPane.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(AdminSceneController.class.getResource("admin.fxml"));
            mainRoot = loader.load();
            Scene scene = new Scene(mainRoot, CLINICIAN_ADMIN_PAGE_WIDTH, ADMIN_PAGE_HEIGHT);
            mainStage.setTitle("Admin");
            mainStage.setScene(scene);

            AdminSceneController controller = loader.getController();
            controller.initParentStageReference();
            controller.setAdmin(activeAdmin);
            controller.initStageTitle();

        } catch (IOException e) {

            LOGGER.severe(e.getMessage());
        }

    }

    /**
     * Will attempt to login the user. If successful, it will bring them to the main GUI. If not then a suitable message
     * will tell the user their login credentials were incorrect.
     */
    @FXML
    private void loginUser() {
        String enteredUsername = usernameField.getText().trim(); // Remove any additional white space

        // Note: We can not currently have the empty string as a password with this implementation
        String password = passwordField.getText();
        if (password.equals("")) {
            password = null;
        }

        AuthToken authToken;
        try {
            // Prepare and make the login post request
            LoginRequestBody loginRequestBody = new LoginRequestBody(enteredUsername, password);
            LoginRequest loginRequest = new LoginRequest();

            authToken = loginRequest.postLogin(loginRequestBody).getBody();
            assert authToken != null;

            AuthToken.getInstance().init(authToken.getToken(), authToken.getRole());

            // Check what user type is logging in and handle appropriately
            if (AuthToken.getInstance().getRole().equals(Role.USER)) {
                Profile loggedInProfile = ProfileRequests.getInstance().getProfile(enteredUsername,
                        AuthToken.getInstance().getToken()).getBody();

                swapToProfileScene(loggedInProfile, false);
            } else if (AuthToken.getInstance().getRole().equals(Role.CLINICIAN)) {
                Clinician loggedInClinician = ClinicianRequests.getInstance().getClinician(enteredUsername,
                        AuthToken.getInstance().getToken()).getBody();

                setActiveClinician(loggedInClinician);
                swapToClinicianScene(loggedInClinician);
            } else if (AuthToken.getInstance().getRole().equals(Role.ADMIN)) {
                Admin loggedInAdmin = AdminRequests.getInstance().getAdmin(enteredUsername,
                        AuthToken.getInstance().getToken()).getBody();

                setActiveAdmin(loggedInAdmin);
                swapToAdminScene();
            }
        } catch (HttpClientErrorException | NullPointerException e) {
            showLoginError();
        }
    }

    /**
     * The login attempt failed, display an appropriate login error message.
     */
    private void showLoginError() {
        new PushNotification("Login Error!", "Username Or Password Incorrect.").showError();
    }

    /**
     * Register this controller in the ControllerAccess abstract class so we can access this class's methods from other
     * areas in the code and set up the undo redo functionality.
     */
    @FXML
    public void initialize() {
        ControllerAccess.setLoginSceneController(this);

        usernameField.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                loginUser();
            }
        });

        passwordField.setOnKeyPressed(event -> {
            if (event.getCode() == KeyCode.ENTER) {
                loginUser();
            }
        });
    }

}
