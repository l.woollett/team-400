package seng302.GUI.Admin;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import seng302.GUI.Clinician.ClinicianSearchTabController;
import seng302.GUI.Clinician.ClinicianTransplantWaitingController;
import seng302.GUI.ControllerAccess;
import seng302.GUI.MainController;
import seng302.GUI.Notifications.AlertDialog;
import seng302.GUI.Terminal.TerminalSceneController;
import seng302.Model.Admin;
import seng302.Model.Hospital;
import seng302.Model.Profile;
import seng302.ServerInteracton.ServerQueries.AdminRequests;
import seng302.ServerInteracton.ServerQueries.Authentication.AuthToken;
import seng302.ServerInteracton.ServerQueries.ProfileRequests;
import seng302.Utilities.AutoRequestTimer;
import seng302.Utilities.CSV.CsvParser;
import seng302.Utilities.Undo_Redo.UndoRedoController;
import seng302.Utilities.Undo_Redo.UndoableTabChange;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Logger;

/**
 * The 'home' scene for all the tabs related to the Admin.
 */
public class AdminSceneController extends MainController implements Initializable {

    private final Logger LOGGER = Logger.getLogger(getClass().getName());
    // Undo and redo related
    @FXML
    public Button undo, redo;
    @FXML
    private MenuItem loadDataMenuItem, loadCsvMenuItem;
    @FXML
    private TabPane adminTabPane;
    @FXML
    private Tab adminProfileTab, clinicianTransplantWaitingTab, profileSearchTab, gMapTab, rolesTab;
    @FXML
    private AnchorPane mainAnchorPane;

    @FXML
    private GridPane clinicianSearchTab;

    @FXML
    private GridPane adminRolesTab;

    @FXML
    private AdminRolesTabController adminRolesTabController;
    @FXML
    private ClinicianTransplantWaitingController clinicianTransplantWaitingController;
    @FXML
    private ClinicianSearchTabController clinicianSearchTabController;
    private UndoRedoController undoRedoController;
    private SingleSelectionModel<Tab> adminTabPaneSelectionModel;
    private Tab currentTab;
    private boolean currentlyUndoing;
    private Stage terminalStage;
    private Admin admin;
    private List<Hospital> hospitals = new ArrayList<>();

    /**
     * Can only have one terminal at a time.
     */
    private boolean hasTerminal = false;

    public AdminSceneController() {
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ControllerAccess.setAdminSceneController(this);
        initializeUndoRedo();
        List<Hospital> hospitals = loadHospitals();
        adminRolesTabController.setParentController(this, hospitals);
        clinicianTransplantWaitingController.setParentController(this);
        AutoRequestTimer.getInstance().createAutoRequestTimer(-1, -1, this::handleAutoRequestEvent);
    }

    /**
     * Handles an auto-request event, updating all admin related details in the application.
     *
     * @return A boolean which is not currently relevant. It part of the function signature so we that can pass this
     * function as a parameter for the autoRequestTimer.
     */
    private Boolean handleAutoRequestEvent() {
        if (!autoRequestBlocked) {
            Platform.runLater(this::updateTimeStamp);
            Platform.runLater(() -> {
                try {
                    Admin requestTest = AdminRequests.getInstance().getAdmin(admin.getUsername(), AuthToken.getInstance().getToken()).getBody();
                } catch (HttpClientErrorException e) {
                    // Check the status code of the request and handle accordingly
                    switch (e.getStatusCode()) {
                        case UNAUTHORIZED:
                            kickUser();
                            break;
                        default:
                            handleCriticalError(e);
                    }
                } catch (HttpServerErrorException | ResourceAccessException e) {
                    handleCriticalError(e);
                }
            });
        }
        return null;
    }

    /**
     * Creates an UndoRedoController for the window and adds listeners to the undo and redo buttons so they trigger undo
     * and redo events when pressed. Also initialises tab change event listeners for undo/redo
     */
    private void initializeUndoRedo() {
        adminTabPaneSelectionModel = adminTabPane.getSelectionModel();
        currentTab = adminTabPaneSelectionModel.getSelectedItem();
        currentlyUndoing = false;

        undoRedoController = new UndoRedoController();
        setInjectedControllerUndoRedoControllers();

        undo.setOnAction(event -> {
            toggleCurrentlyUndoingForInjectedControllers();
            undoRedoController.undo();
            toggleCurrentlyUndoingForInjectedControllers();
            updateUndoRedoButtons();
        });
        redo.setOnAction(event -> {
            toggleCurrentlyUndoingForInjectedControllers();
            undoRedoController.redo();
            toggleCurrentlyUndoingForInjectedControllers();
            updateUndoRedoButtons();
        });

        updateUndoRedoButtons();
        createTabEventHandlers();
    }

    /**
     * Toggles the currentlyUndoing variable and updates all injected admin window controllers to match this.
     */
    private void toggleCurrentlyUndoingForInjectedControllers() {
        currentlyUndoing = !currentlyUndoing;
        adminRolesTabController.setCurrentlyUndoing(currentlyUndoing);
        clinicianSearchTabController.setCurrentlyUndoing(currentlyUndoing);
        clinicianTransplantWaitingController.setCurrentlyUndoing(currentlyUndoing);
    }

    /**
     * Sets all injected controllers to use the same UndoRedoController and isUndoing boolean as this class.
     */
    private void setInjectedControllerUndoRedoControllers() {
        adminRolesTabController.setUndoRedoController(undoRedoController);
        clinicianSearchTabController.setUndoRedoController(undoRedoController);
        clinicianTransplantWaitingController.setUndoRedoController(undoRedoController);
    }

    /**
     * Adds event handlers to all tabs in the Admin scene so that when they are clicked they push a Command onto the
     * undo stack.
     */
    private void createTabEventHandlers() {
        rolesTab.setOnSelectionChanged(event -> handleTabChange(rolesTab));
        clinicianTransplantWaitingTab.setOnSelectionChanged(event -> handleTabChange(clinicianTransplantWaitingTab));
        profileSearchTab.setOnSelectionChanged(event -> handleTabChange(profileSearchTab));
        gMapTab.setOnSelectionChanged(event -> handleTabChange(gMapTab));
    }

    /**
     * Handler for whenever a tab is selected in the Admin Scene that pushes the tab change event onto the undo Deque as
     * an UndoableTabChange. Note: It will not add these if the event was caused by and undo or redo.
     *
     * @param tab The new Tab that was selected.
     */
    private void handleTabChange(Tab tab) {
        if (tab.isSelected() && !currentlyUndoing) {
            undoRedoController.pushToUndoDeque(new UndoableTabChange(adminTabPaneSelectionModel, currentTab));
            currentTab = adminTabPaneSelectionModel.getSelectedItem();
            updateUndoRedoButtons();
        }
    }

    /**
     * Ensures that the undo/redo buttons are only enabled if there are available undo/redo.
     */
    public void updateUndoRedoButtons() {
        undo.setDisable(undoRedoController.isUndoEmpty());
        redo.setDisable(undoRedoController.isRedoEmpty());
    }

    /**
     * Sets the current admin that is using the scene.
     *
     * @param admin The admin to be set in place of the old admin.
     */
    public void setAdmin(Admin admin) {
        this.admin = admin;
    }

    @Override
    public void initParentStageReference() {
        this.parentStage = (Stage) mainAnchorPane.getScene().getWindow();
    }

    /**
     * Called when the stage for the first time and the FXML has already been loaded into the scene.
     */
    @Override
    public void initStageTitle() {
        stageTitle = String.format("Admin: %s", admin.getUsername());
    }

    /**
     * Will create a terminal for the admin to use.
     */
    @FXML
    private void openTerminal() {
        if (!hasTerminal) {
            try {
                FXMLLoader loader = new FXMLLoader(TerminalSceneController.class.getResource("terminal.fxml"));
                Parent root = loader.load();
                Scene terminalScene = new Scene(root);
                terminalStage = new Stage();
                terminalStage.setTitle("ODMS Terminal");
                terminalStage.setScene(terminalScene);
                terminalStage.setResizable(true);
                terminalStage.show();

                hasTerminal = true;

                TerminalSceneController terminalSceneController = loader.getController();
                terminalSceneController.initCurrentAdmin(admin);

                terminalStage.setOnCloseRequest(event -> hasTerminal = false);

            } catch (IOException e) {
                LOGGER.severe(e.getMessage());
                AlertDialog.showAndWait("Error", "Unable to open the terminal.");
            }
        }
    }

    /**
     * Bind the event to the event with the logout method.
     */
    @FXML
    public void logoutEvent() {
        logout();
        if (hasTerminal) {
            terminalStage.close();
        }
    }

    /**
     * Load the profiles from the CSV into the application and database.
     */
    @FXML
    private void loadCsvEvent() {
        Stage fileChooserStage = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("CSV", "*.csv"));
        File selectedFile = fileChooser.showOpenDialog(fileChooserStage);


        if (selectedFile != null) {

            CsvParser csvParser = new CsvParser(selectedFile);
            List<Profile> profiles = csvParser.parse();


            for (Profile profile : profiles) {
                try {
                    ProfileRequests.getInstance().postProfile(profile);
                } catch (HttpClientErrorException e) {
                    switch (e.getStatusCode()) {
                        case UNAUTHORIZED:
                            kickUser();
                            break;
                    }
                } catch (HttpServerErrorException | ResourceAccessException e) {
                    handleCriticalError(e);
                }
            }
            clinicianSearchTabController.updateProfiles();
        }
    }

    /**
     * Kicks user out if they are not authenticated
     */
    @Override
    public void kickUser() {
        if (hasTerminal) {
            terminalStage.close();
        }
        closeOpenStages();
        parentStage.close();
        AutoRequestTimer.getInstance().stopTimers();
        AuthToken.getInstance().clearToken();
        AlertDialog.showAndWait("Information", "You are being logged out because your credentials are no longer valid.");
        gotoLogin();
        Platform.runLater(() -> parentStage.setTitle("Login"));
        parentStage.show();
    }

    /**
     * Generic error handler for if anything in the application goes wrong. Basically just a more elegant way of
     * handling errors instead of the application crashing. It will log the user out so no harm to the system can
     * be done while it is in an unstable state.
     *
     * @param e Exception Any exception that can be thrown.
     */
    public void handleCriticalError(Exception e) {
        if (hasTerminal) {
            terminalStage.close();
        }
        closeOpenStages();
        parentStage.close();
        AutoRequestTimer.getInstance().stopTimers();
        AuthToken.getInstance().clearToken();
        AlertDialog.showAndWait("Error!", "Oops! Something went wrong. You are being logged out in order to keep your information safe.");
        gotoLogin();
        LOGGER.severe(e.getMessage());
        Platform.runLater(() -> parentStage.setTitle("Login"));
        parentStage.show();
    }

    public Admin getAdmin() {
        return admin;
    }
}
