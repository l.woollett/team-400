package seng302.GUI.Admin;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import seng302.GUI.ControllerAccess;
import seng302.GUI.Notifications.PushNotification;
import seng302.Model.Admin;
import seng302.ModelController.UserController;
import seng302.ServerInteracton.ServerQueries.AdminRequests;
import seng302.Utilities.Undo_Redo.UndoRedoController;
import seng302.Utilities.Undo_Redo.UndoableTextField;

import static seng302.Utilities.Style.applyTextFieldStyle;
import static seng302.Utilities.Tooltips.*;
import static seng302.Utilities.Validator.isValidPassword;
import static seng302.Utilities.Validator.isValidUsername;

public class CreateAdminController {

    @FXML public Button undoButton, redoButton;
    @FXML private AnchorPane createAdminPane;
    @FXML private TextField usernameTextField;
    @FXML private PasswordField passwordTextField;

    private boolean isUsernameValid = false;
    private boolean isPasswordValid = false;
    private UndoRedoController undoRedoController;
    private boolean currentlyUndoing;

    /**
     * Calls all initialisation functions for the create admin GUI.
     */
    public void initialize() {
        initializeUsernameField();
        initializePasswordField();
        initializeUndoRedo();
    }

    /**
     * Creates an UndoRedoController for the window and adds listeners to the undo and redo buttons so they trigger
     * undo and redo events when pressed.
     */
    private void initializeUndoRedo() {
        currentlyUndoing = false;
        undoRedoController = new UndoRedoController();

        undoButton.setOnAction(event -> {
            currentlyUndoing = true;
            undoRedoController.undo();
            currentlyUndoing = false;
            updateUndoRedoButtons();
        });
        redoButton.setOnAction(event -> {
            currentlyUndoing = true;
            undoRedoController.redo();
            currentlyUndoing = false;
            updateUndoRedoButtons();
        });

        updateUndoRedoButtons();
    }

    /**
     * Ensures that the undo/redo buttons are only enabled if there is an available undo/redo.
     */
    private void updateUndoRedoButtons() {
        undoButton.setDisable(undoRedoController.isUndoEmpty());
        redoButton.setDisable(undoRedoController.isRedoEmpty());
    }

    /**
     * Initialises the username TextField. Adds a listener that visually changes the field to show if
     * correct/incorrect data has been input and provides undo/redo functionality. Also adds an appropriate tooltip to
     * match this.
     */
    private void initializeUsernameField() {
        usernameTextField.setTooltip(createNewTooltip(INVALID_USERNAME_TOOLTIP_TEXT));
        updateUsernameTextFieldStyle();

        usernameTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    updateUsernameTextFieldStyle();
                    if (!oldValue.equals(newValue) && !currentlyUndoing) {
                        pushTextFieldChangeToUndoDeque(this.usernameTextField, oldValue);
                        updateUndoRedoButtons();
                    }
                })
        );
    }

    /**
     * Initialises the password TextField. Adds a listener that visually changes the field to show if correct/incorrect
     * data has been input and provides undo/redo functionality. Also adds an appropriate tooltip to
     * match this.
     */
    private void initializePasswordField() {
        passwordTextField.setTooltip(createNewTooltip(INVALID_PASSWORD_TOOLTIP_TEXT));
        updatePasswordTextFieldStyle();

        passwordTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    updatePasswordTextFieldStyle();
                    if (!oldValue.equals(newValue) && !currentlyUndoing) {
                        pushTextFieldChangeToUndoDeque(this.passwordTextField, oldValue);
                        updateUndoRedoButtons();
                    }
                })
        );
    }

    /**
     * Pushes an UndoableTextField onto the undo Deque.
     *
     * @param textField TextField that will be wrapped in an UndoableTextField.
     * @param oldValue  String representing the old value of the textField passed in. It is the value the textBox will
     *                  take if an undo event takes place.
     */
    private void pushTextFieldChangeToUndoDeque(TextField textField, String oldValue) {
        undoRedoController.pushToUndoDeque(new UndoableTextField(textField, oldValue));
    }

    /**
     * Will test to see if the username has a valid value and will apply an appropriate CSS style and tooltip.
     */
    private void updateUsernameTextFieldStyle() {
        String currentValue = usernameTextField.getText();
        isUsernameValid = isValidUsername(currentValue);
        applyTextFieldStyle(usernameTextField, isUsernameValid, false, VALID_USERNAME_TOOLTIP_TEXT,
                INVALID_USERNAME_TOOLTIP_TEXT, currentValue);

        // Check if the username is already used. If it is then apply a tooltip which says it is already taken.
        if (!isUsernameValid && UserController.isUsernameUsed(currentValue)) {
            applyTextFieldStyle(usernameTextField, isUsernameValid, false, VALID_USERNAME_TOOLTIP_TEXT,
                    INVALID_USERNAME_NON_UNIQUE_TOOLTIP_TEXT, currentValue);
        }
    }

    /**
     * Will test to see if the password field has a valid password value and will apply the appropriate CSS style and
     * tooltip text.
     */
    private void updatePasswordTextFieldStyle() {
        String currentValue = passwordTextField.getText();
        isPasswordValid = isValidPassword(currentValue);
        applyTextFieldStyle(passwordTextField, isPasswordValid, false, VALID_PASSWORD_TOOLTIP_TEXT,
                INVALID_PASSWORD_TOOLTIP_TEXT, currentValue);
    }

    /**
     * Exit the create user stage because they want to stop creating a user profile.
     */
    @FXML
    private void cancelButtonClicked() {
        close();
    }

    /**
     * Method called when the create user window is closed.
     */
    private void close() {
        Stage createUserStage = (Stage) createAdminPane.getScene().getWindow();
        createUserStage.close();
    }

    /**
     * Creates an admin with the given values if it has a valid username. The window is then closed.
     */
    @FXML
    private void createAdminButtonClicked() {
        if (isUsernameValid && isPasswordValid) {
            // Mandatory fields
            String username = usernameTextField.getText();
            String password = passwordTextField.getText();

            Admin admin = new Admin(username, password);

            try {
                ResponseEntity response = AdminRequests.getInstance().postAdmin(admin);

                if (response.getStatusCode() == HttpStatus.OK) {
                    PushNotification notification = new PushNotification("Success!", "Admin has been created!");
                    ControllerAccess.getAdminRolesTabController().pushAdminChangeToUndoDeque(null, admin, ControllerAccess.getAdminSceneController());
                    notification.show();
                    close(); // Close this stage after the user was successfully created.
                }
            } catch (HttpClientErrorException e) {
                if (e.getStatusCode() == HttpStatus.INTERNAL_SERVER_ERROR) {
                    PushNotification internalServerError = new PushNotification("Error!", "Please try again later.");
                    internalServerError.showError();
                    close();
                } else if (e.getStatusCode() == HttpStatus.BAD_REQUEST) {
                    PushNotification badRequest = new PushNotification ("Error!", "User already exists! Please select a different name.");
                    badRequest.showError();
                }
            } catch (HttpServerErrorException | ResourceAccessException e) {
                PushNotification genericError = new PushNotification("Error!", "An error occurred. Please try again later.");
                genericError.showError();
                close();
            }
        } else {
            PushNotification insufficientInfo = new PushNotification("More detail required!",
                    "You must complete the mandatory fields.");
            insufficientInfo.showError();
        }
    }
}
