package seng302.GUI.Admin;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.layout.Region;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import seng302.GUI.Clinician.ClinicianProfileTabController;
import seng302.GUI.ControllerAccess;
import seng302.GUI.CreateClinicianController;
import seng302.GUI.LoginSceneController;
import seng302.GUI.Notifications.AlertDialog;
import seng302.GUI.Notifications.PushNotification;
import seng302.GUI.Profile.ProfileSceneController;
import seng302.Model.Admin;
import seng302.Model.Clinician;
import seng302.Model.Hospital;
import seng302.Model.Profile;
import seng302.ModelController.AdminController;
import seng302.ModelController.ClinicianController;
import seng302.ServerInteracton.ServerQueries.AdminRequests;
import seng302.ServerInteracton.ServerQueries.Authentication.AuthToken;
import seng302.ServerInteracton.ServerQueries.ClinicianRequests;
import seng302.ServerInteracton.ServerQueries.ProfileRequests;
import seng302.Utilities.Undo_Redo.*;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

public class AdminRolesTabController {
    private final Logger LOGGER = Logger.getLogger(getClass().getName());
    protected List<Hospital> hospitals;
    @FXML
    private TextField deleteDonorTextField, deleteAdminTextField, deleteClinicianTextField;

    /**
     * Controls all undo/redo functionality for the class.
     */
    @FXML
    private UndoRedoController undoRedoController;
    private boolean currentlyUndoing;
    private AdminSceneController adminSceneController;
    private Alert sessionErrorAlert = new Alert(Alert.AlertType.ERROR, "Another session has been logged in, please log in again");

    /**
     * Called after the constructor (if there is one) and any @FXML fields are populated.
     */
    public void initialize() {
        initialiseUndoRedo();
        sessionErrorAlert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        ControllerAccess.setAdminRolesTabController(this);
    }

    /**
     * Sets the currently Undoing variables for the class.
     *
     * @param currentlyUndoing The value that currentlyUndoing will be set to.
     */
    public void setCurrentlyUndoing(boolean currentlyUndoing) {
        this.currentlyUndoing = currentlyUndoing;
    }

    /**
     * Initialise all undo/redo functionality for the class.
     */
    private void initialiseUndoRedo() {
        deleteDonorTextField.textProperty().addListener(((observable, oldValue, newValue) -> {
            if (!oldValue.equals(newValue) && !currentlyUndoing) {
                pushTextFieldChangeToUndoDeque(this.deleteDonorTextField, oldValue);
            }
        }));
        deleteAdminTextField.textProperty().addListener(((observable, oldValue, newValue) -> {
            if (!oldValue.equals(newValue) && !currentlyUndoing) {
                pushTextFieldChangeToUndoDeque(this.deleteAdminTextField, oldValue);
            }
        }));
        deleteClinicianTextField.textProperty().addListener(((observable, oldValue, newValue) -> {
            if (!oldValue.equals(newValue) && !currentlyUndoing) {
                pushTextFieldChangeToUndoDeque(this.deleteClinicianTextField, oldValue);
            }
        }));
    }

    /**
     * Push the admin to the undo redo stack when changes are made.
     * This can be a delete, create, or modify.
     *
     * @param oldAdmin             previous admin.
     * @param admin                current admin.
     * @param adminSceneController admin controller for calling methods from.
     */
    public void pushAdminChangeToUndoDeque(Admin oldAdmin, Admin admin, AdminSceneController adminSceneController) {
        undoRedoController.pushToUndoDeque(new UndoableAdmin(oldAdmin, admin, adminSceneController));
        updateUndoRedoButtons();
    }

    /**
     * Push the profile to the undo redo stack when changes are made.
     * This can be a delete, create, or modify.
     *
     * @param oldProfile previous profile.
     * @param profile    current profile.
     * @param controller profile controller for calling methods from.
     */
    public void pushProfileChangeToUndoDeque(Profile oldProfile, Profile profile, ProfileSceneController controller) {
        undoRedoController.pushToUndoDeque(new UndoableProfile(oldProfile, profile, controller, null, null));
        updateUndoRedoButtons();
    }

    /**
     * Push the Clinician to the undo redo stack when changes are made.
     * This can be a delete, create, or modify.
     *
     * @param oldClinician previous clinician.
     * @param clinician    current clinician.
     * @param controller   clinician controller for calling methods from.
     */
    public void pushClinicianChangeToUndoDeque(Clinician oldClinician, Clinician clinician, ClinicianProfileTabController controller) {
        undoRedoController.pushToUndoDeque(new UndoableClinician(oldClinician, clinician, controller));
        updateUndoRedoButtons();
    }

    /**
     * Pushes an UndoableTextField onto the undo Deque.
     *
     * @param textField TextField that will be wrapped in an UndoableTextField.
     * @param oldValue  String representing the old value of the textField passed in. It is the value the textBox will
     *                  take if an undo event takes place.
     */
    private void pushTextFieldChangeToUndoDeque(TextField textField, String oldValue) {
        undoRedoController.pushToUndoDeque(new UndoableTextField(textField, oldValue));
        updateUndoRedoButtons();
    }

    /**
     * Ensures that the undo/redo buttons are only enabled if there are available undo/redo.
     */
    private void updateUndoRedoButtons() {
        ControllerAccess.getAdminSceneController().updateUndoRedoButtons();
    }

    /**
     * Sets the undoRedoController for the class.
     *
     * @param undoRedoController The UndoRedoController to be set.
     */
    public void setUndoRedoController(UndoRedoController undoRedoController) {
        this.undoRedoController = undoRedoController;
    }

    public void setParentController(AdminSceneController adminSceneController, List<Hospital> hospitals) {
        this.hospitals = hospitals;
        this.adminSceneController = adminSceneController;
    }

    /**
     * Opens the create admin FXML.
     */
    @FXML
    private void createAdminButtonPressed() {
        Stage createAdminStage = new Stage();
        try {
            Parent createAdminRoot = FXMLLoader.load(getClass().getResource("createAdmin.fxml"));
            Scene createAdminScene = new Scene(createAdminRoot);
            createAdminStage.setScene(createAdminScene);
            createAdminStage.setTitle("Create admin");
            createAdminStage.setResizable(true);
            createAdminStage.initModality(Modality.APPLICATION_MODAL);
            createAdminStage.show();
            adminSceneController.addStage(createAdminStage);
        } catch (IOException e) {
            LOGGER.severe(e.getMessage());
        }
    }

    /**
     * Opens the create user FXML.
     */
    @FXML
    private void createDonorButtonPressed() {
        Stage signUpStage = new Stage();
        try {
            Parent signUpRoot = FXMLLoader.load(LoginSceneController.class.getResource("createUser.fxml"));
            Scene signUpScene = new Scene(signUpRoot);
            signUpStage.setScene(signUpScene);
            signUpStage.setTitle("Create Account");
            signUpStage.setResizable(true);
            signUpStage.initModality(Modality.APPLICATION_MODAL);
            signUpStage.show();
            adminSceneController.addStage(signUpStage);
        } catch (IOException e) {
            LOGGER.severe(e.getMessage());
        }
    }

    /**
     * Opens the create clinician FXML.
     */
    @FXML
    private void createClinicianButtonPressed() {
        Stage createClinicianStage = new Stage();
        try {
            FXMLLoader loader = new FXMLLoader(ClinicianProfileTabController.class.getResource("createClinician.fxml"));
            Parent createClinicianRoot = loader.load();
            Scene createClinicianScene = new Scene(createClinicianRoot);

            CreateClinicianController controller = loader.getController();
            createClinicianStage.setScene(createClinicianScene);
            createClinicianStage.setTitle("Create Clinician");
            createClinicianStage.setResizable(true);
            createClinicianStage.initModality(Modality.APPLICATION_MODAL);
            createClinicianStage.show();
            controller.initHospitals(adminSceneController.getHospitals());
            adminSceneController.addStage(createClinicianStage);
        } catch (IOException e) {
            LOGGER.severe(e.getMessage());
        }
    }

    /**
     * Deletes the admin with the given username if it exists.
     */
    @FXML
    private void deleteAdminButtonPressed() {
        String enteredUsername = deleteAdminTextField.getText().trim();
        Admin currentAdmin = ControllerAccess.getAdminSceneController().getAdmin();

        if (currentAdmin.getUsername().equals(enteredUsername)) {
            // Can not delete their own account
            PushNotification pushNotification = new PushNotification("Error!", "You can not delete your own account.");
            pushNotification.showError();
        } else if (enteredUsername.equals(AdminController.DEFAULT_ADMIN_USERNAME)) {
            PushNotification pushNotification = new PushNotification("Error!", "You can not delete the default admin.");
            pushNotification.showError();
        } else {
            try {
                // Injecting token into this controller doesn't seem to work from ASController.
                // SO have used controller access for the time being.
                String token = AuthToken.getInstance().getToken();
                ResponseEntity response = AdminRequests.getInstance().deleteAdmin(enteredUsername, token);

                if (response.getStatusCode() == HttpStatus.OK) {
                    PushNotification notification = new PushNotification("Success!", "Admin has been deleted");
                    notification.show();
                    deleteAdminTextField.setText("");
                }

            } catch (HttpClientErrorException e) {
                // Check the status code of the request and handle accordingly
                switch (e.getStatusCode()) {
                    case NOT_FOUND:
                        PushNotification notFoundNotification = new PushNotification("Error!", "Admin not found.");
                        notFoundNotification.showError();
                        break;
                    case UNAUTHORIZED:
                        adminSceneController.kickUser();
                        break;
                    case INTERNAL_SERVER_ERROR:
                        PushNotification internalErrorNotification = new PushNotification("Error!", "An error occurred. Please try again later.");
                        internalErrorNotification.showError();
                        break;
                    case METHOD_NOT_ALLOWED:
                        PushNotification methodNotAllowed = new PushNotification("Error!", "Enter an admin to remove");
                        methodNotAllowed.showError();
                        break;
                    default:
                        adminSceneController.handleCriticalError(e);
                }
            } catch (HttpServerErrorException | ResourceAccessException e) {
                adminSceneController.handleCriticalError(e);
            }
        }
    }

    /**
     * Deletes the user with the given username if it exists.
     */
    @FXML
    private void deleteClinicianButtonPressed() {
        String enteredUsername = deleteClinicianTextField.getText().trim();
        if (enteredUsername.equals(ClinicianController.getDefaultClinicianUsername())) {
            PushNotification deletingDefaultNotification = new PushNotification("Error!", "You can not delete the default clinician.");
            deletingDefaultNotification.showError();
        } else {
            try {
                Clinician clinicianToDelete = ClinicianRequests.getInstance().getClinician(enteredUsername,
                        AuthToken.getInstance().getToken()).getBody();
                pushClinicianChangeToUndoDeque(clinicianToDelete, null, null);
                ClinicianRequests.getInstance().deleteClinician(enteredUsername, AuthToken.getInstance().getToken());
                PushNotification notification = new PushNotification("Success!", "Clinician has been deleted");
                notification.show();
                deleteClinicianTextField.setText("");
            } catch (HttpClientErrorException e) {
                if (e.getStatusCode() == HttpStatus.UNAUTHORIZED) {
                    sessionErrorAlert.showAndWait();
                    adminSceneController.kickUser();
                } else if (e.getStatusCode() == HttpStatus.METHOD_NOT_ALLOWED) {
                    PushNotification pushNotification = new PushNotification("Error!", "Enter a clinician to remove");
                    pushNotification.showError();
                } else {
                    LOGGER.severe(e.getMessage());
                    PushNotification notification = new PushNotification("Error", "Clinician either did not exist or was unable to be deleted.");
                    notification.showError();
                }
            }
        }
    }

    /**
     * Deletes the user with the given username if it exists.
     */
    @FXML
    private void deleteDonorButtonPressed() {
        String enteredUsername = deleteDonorTextField.getText().trim();
        try {
            Profile profileToDelete = ProfileRequests.getInstance().getProfile(enteredUsername, AuthToken.getInstance().getToken()
            ).getBody();
            pushProfileChangeToUndoDeque(profileToDelete, null, null);
            ProfileRequests.getInstance().deleteProfile(enteredUsername, AuthToken.getInstance().getToken());
            PushNotification notification = new PushNotification("Success!", "User has been deleted");
            notification.show();
            deleteDonorTextField.setText("");
        } catch (HttpClientErrorException e) {
            // Check the status code of the request and handle accordingly
            switch (e.getStatusCode()) {
                case NOT_FOUND:
                    PushNotification donorNotFoundNotification = new PushNotification("Error!" , "Donor not found.");
                    donorNotFoundNotification.showError();
                    break;
                case UNAUTHORIZED:
                    adminSceneController.kickUser();
                    break;
                case INTERNAL_SERVER_ERROR:
                    PushNotification internalServerError = new PushNotification("Error!", "An error occurred. Please try again later.");
                    internalServerError.showError();
                    break;
                case METHOD_NOT_ALLOWED:
                    PushNotification donorErrorNotification = new PushNotification("Error!", "Enter a donor to remove");
                    donorErrorNotification.showError();
                    break;
                default:
                    adminSceneController.handleCriticalError(e);
            }
        } catch (HttpServerErrorException | ResourceAccessException e) {
            adminSceneController.handleCriticalError(e);
        }
    }
}

