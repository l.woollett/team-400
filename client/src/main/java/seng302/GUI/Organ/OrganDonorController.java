package seng302.GUI.Organ;

import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.util.Callback;
import seng302.CustomException.DoesNotExist;
import seng302.Enum.OrganEnum;
import seng302.GUI.ControllerAccess;
import seng302.GUI.Hideable;
import seng302.GUI.Profile.ProfileSceneController;
import seng302.Model.Profile;
import seng302.Utilities.CustomCheckboxListCell;
import seng302.Utilities.Undo_Redo.UndoRedoController;
import seng302.Utilities.Undo_Redo.UndoableOrganCheckListView;

import java.net.URL;
import java.util.*;

public class OrganDonorController implements Initializable, Hideable {

    @FXML
    private AnchorPane donorAnchorPane;
    @FXML
    private ListView<OrganEnum> donorOrgansTable;
    @FXML
    private Text organDonorText;

    private Profile currentProfile;
    private List<OrganEnum> checkedOrgans = new ArrayList<>();
    private ObservableList<OrganEnum> organsList = FXCollections.observableArrayList();
    private Map<OrganEnum, ObservableValue<Boolean>> organChecklist = new HashMap<>();
    private Callback<OrganEnum, ObservableValue<Boolean>> organToBoolean;
    private ProfileSceneController profileSceneController;

    /**
     * Controls all undo/redo functionality for the class.
     */
    @FXML
    private UndoRedoController undoRedoController;
    private boolean currentlyUndoing;

    /**
     * Constructor for the class. Creates the necessary objects for the list work correctly.
     */
    public OrganDonorController() {
        //callback for the setCellFactory to create a checklist
        organToBoolean = organ -> organChecklist.get(organ);
        ControllerAccess.setOrganDonorController(this);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Platform.runLater(this::hide);
    }


    /**
     * Sets the currently Undoing variables for the class.
     *
     * @param currentlyUndoing The value that currentlyUndoing will be set to.
     */
    public void setCurrentlyUndoing(boolean currentlyUndoing) {
        this.currentlyUndoing = currentlyUndoing;
    }

    /**
     * Sets the undoRedoController for the class.
     *
     * @param undoRedoController The UndoRedoController to be set.
     */
    public void setUndoRedoController(UndoRedoController undoRedoController) {
        this.undoRedoController = undoRedoController;
    }

    /**
     * Sets the ProfileSceneController for this class.
     *
     * @param profileSceneController The ProfileSceneController that is being set.
     */
    public void setProfileSceneController(ProfileSceneController profileSceneController) {
        this.profileSceneController = profileSceneController;
    }

    @FXML
    private void refresh() {
        donorOrgansTable.refresh();
    }

    /**
     * Function is used to pass a reference of the currentProfile from the ProfileSceneController so that the attributes
     * can be seen by this child controller
     *
     * @param currentProfile Profile of the current profile being viewed/logged into
     */
    public void setCurrentDonorProfile(Profile currentProfile) {
        this.currentProfile = currentProfile;
    }

    /**
     * Function populates the listView for current organs registered to donate. Resets the listView to be just a list
     * view with no checkboxes
     */
    @Override
    public void hide() {
        donorOrgansTable.setCellFactory(null);
        checkedOrgans.addAll(currentProfile.getDonorOrgans());
        organsList.setAll(currentProfile.getDonorOrgans());
        donorOrgansTable.setItems(organsList);
        donorOrgansTable.setCellFactory(new Callback<ListView<OrganEnum>, ListCell<OrganEnum>>() {
            @Override
            public ListCell<OrganEnum> call(ListView<OrganEnum> param) {
                return new ListCell<OrganEnum>() {
                    @Override
                    protected void updateItem(OrganEnum item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item == null || empty) {
                            setItem(null);
                        } else {
                            setText(item.toString());
                            if (currentProfile.getReceiverOrgans().contains(item)) {
                                setStyle("-fx-background-color: orange");
                            }
                        }
                    }
                };
            }
        });
    }

    /**
     * Function creates the Map that the checklist uses to track what the current checkbox selection model is. The map
     * contains HashMap of (OrganEnum, ObservableValue(Boolean)).
     */
    @Override
    public void reveal() {
        for (OrganEnum organ : OrganEnum.values()) {
            if (currentProfile.getDonorOrgans().contains(organ)) {
                organChecklist.put(organ, new SimpleBooleanProperty(true));
            } else {
                organChecklist.put(organ, new SimpleBooleanProperty(false));
            }
        }
    }

    private void pushCheckListToUndoDeque(SimpleBooleanProperty property) {
        undoRedoController.pushToUndoDeque(new UndoableOrganCheckListView(property));
        updateUndoRedoButtons();
    }

    /**
     * Ensures that the undo/redo buttons are only enabled if there are available undo/redo.
     */
    private void updateUndoRedoButtons() {
        profileSceneController.updateUndoRedoButtons();
    }

    /**
     * Function is called when the register button in the profileScene is clicked. Turns the list of donating organs
     * into a checklist displaying all the possible values that a user can select to donate
     *
     * @param undo Boolean indicating if an undo event is currently happening.
     */
    public void editDonorOrgansEvent(boolean undo) {
        organDonorText.setText("Select the organs you wish to donate");
        if (!undo) {
            reveal();
        }
        donorOrgansTable.setCellFactory(
                callBack -> new CustomCheckboxListCell(organToBoolean, currentProfile.getReceiverOrgans()));
        for (ObservableValue<Boolean> property : organChecklist.values()) {
            property.addListener(observable -> {
                if (!currentlyUndoing) {
                    pushCheckListToUndoDeque((SimpleBooleanProperty) property);
                }
            });
        }
        organsList.setAll(organChecklist.keySet());
    }


    /**
     * Function is called when the edit apply button is clicked in the Profile tab. Returns the listView to its original
     * state displaying only the organs that the user has selected to donate
     */
    public void editEndDonorOrgansEvent() {
        organDonorText.setText("Organs Listed to Donate");
        hide();
    }

    /**
     * Function checks if any organs have been selected in the checkBoxList
     *
     * @return boolean indicating whether anything has been selected in the list
     */
    public boolean hasDonorOrgansSelected() {
        boolean selected = false;
        for (OrganEnum organ : organChecklist.keySet()) {
            ObservableValue<Boolean> ticked = organChecklist.get(organ);
            if (ticked.getValue()) {
                selected = true;
            }
        }
        return selected;
    }

    /**
     * Function returns an array of selected organs from the CheckBoxList
     *
     * @return {@code ArrayList<OrganEnum>}
     * @throws DoesNotExist Thrown when trying to access an OrganEnum that is not in the list
     */
    public ArrayList<OrganEnum> getSelectedDonorOrgans() throws DoesNotExist {
        ArrayList<OrganEnum> selectedOrgans = new ArrayList<>();
        if (hasDonorOrgansSelected()) {
            for (OrganEnum organ : organChecklist.keySet()) {
                ObservableValue<Boolean> ticked = organChecklist.get(organ);
                if (ticked.getValue()) {
                    selectedOrgans.add(organ);
                }
            }
        } else {
            throw new DoesNotExist("No organs have been selected in the CheckBoxList for donor organs");
        }
        return selectedOrgans;
    }

    /**
     * Function to get the OrganCheckList (organs that have been checked). Currently only for testing purposes
     *
     * @return {@code Map<OrganEnum, ObservableValue<Boolean>>}
     */
    public Map<OrganEnum, ObservableValue<Boolean>> getOrganChecklist() {
        return organChecklist;
    }
}
