package seng302.GUI.Organ;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import seng302.Enum.OrganEnum;
import seng302.GUI.Hideable;
import seng302.GUI.Procedures.ProcedureRemovalPopUp;
import seng302.GUI.Profile.ProfileSceneController;
import seng302.Model.Death;
import seng302.Model.Disease;
import seng302.Model.Profile;
import seng302.Model.ReceiverOrgan;
import seng302.Utilities.Undo_Redo.UndoRedoController;
import seng302.Utilities.Undo_Redo.UndoableOrganCheckTableView;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

public class OrganReceiverController implements Hideable {

    @FXML
    private AnchorPane organReceiverAnchorPane;
    @FXML
    private TableView<ReceiverOrgan> receivingOrganList;

    @FXML
    private TableColumn<ReceiverOrgan, OrganEnum> receivingOrganColumn = new TableColumn<>
            ("Receiving Organ");

    @FXML
    private TableColumn<ReceiverOrgan, LocalDateTime> receivingOrganRegisteredColumn = new
            TableColumn<>("Date Registered");

    @FXML
    private Text organReceiverText;

    private Profile currentProfile;

    //This list is used to track the checkboxes. They are originally populated using this list and ticking them
    // changes this list
    private List<OrganEnum> checkedOrgans = new ArrayList<>();
    private ObservableList<ReceiverOrgan> currentReceivingOrgans = FXCollections.observableArrayList();
    private ObservableList<ReceiverOrgan> organsList = FXCollections.observableArrayList();
    private TableColumn<ReceiverOrgan, Boolean> checkBoxColumn = new TableColumn<>("Registered");
    private ProfileSceneController profileSceneController;
    private Map<OrganEnum, String> unregisteredOrgans = new HashMap<>();
    private Map<Disease, OrganEnum> curedDiseases = new HashMap<>();

    private Logger LOGGER = Logger.getLogger(OrganReceiverController.class.getName());


    /**
     * Controls all undo/redo functionality for the class.
     */
    @FXML
    private UndoRedoController undoRedoController;
    private boolean currentlyUndoing;
    private SimpleBooleanProperty observe;

    private ProfileSceneController parentController;

    /**
     * Called after the constructor (if there is one) and any @FXML fields are populated.
     */
    @FXML
    public void initialize() {
        //Platform.runLater(this::setupTable);
    }

    public void initParentController(ProfileSceneController profileSceneController) {
        this.parentController = profileSceneController;
    }

    /**
     * Sets the currently Undoing variables for the class.
     *
     * @param currentlyUndoing The value that currentlyUndoing will be set to.
     */
    public void setCurrentlyUndoing(boolean currentlyUndoing) {
        this.currentlyUndoing = currentlyUndoing;
    }

    /**
     * Sets the ProfileSceneController for this class.
     *
     * @param profileSceneController The ProfileSceneController that is being set.
     */
    public void setProfileSceneController(ProfileSceneController profileSceneController) {
        this.profileSceneController = profileSceneController;
    }

    /**
     * Ensures that the undo/redo buttons are only enabled if there are available undo/redo.
     */
    private void updateUndoRedoButtons() {
        profileSceneController.updateUndoRedoButtons();
    }

    /**
     * Sets the undoRedoController for the class.
     *
     * @param undoRedoController The UndoRedoController to be set.
     */
    public void setUndoRedoController(UndoRedoController undoRedoController) {
        this.undoRedoController = undoRedoController;
    }

    /**
     * Function is called by the profileSceneController to pass the current Profile information to this controller
     *
     * @param currentProfile The currently active profile being viewed
     */
    public void setCurrentReceiverProfile(Profile currentProfile) {
        this.currentProfile = currentProfile;
        setupTable();
    }

    /**
     * Function initializes the data table to display the currently registered organs for the profile to receive
     */
    private void setupTable() {
        checkedOrgans.clear();
        checkedOrgans.addAll(currentProfile.getReceiverOrgans());
        populateReceiverCheckList();
        receivingOrganColumn.setCellValueFactory(new PropertyValueFactory<>("registeredOrgan"));
        receivingOrganRegisteredColumn.setCellValueFactory(new PropertyValueFactory<>("dateRegistered"));
        currentReceivingOrgans.addAll(currentProfile.getReceiverOrgansWithTimes());
        receivingOrganList.setItems(currentReceivingOrgans);
        receivingOrganList.setEditable(false);
        setupCheckBoxColumn();
        setOrganClashIndicator();
        receivingOrganList.setOnSort(event -> receivingOrganList.refresh());
    }

    /**
     * Function sets the table to highlight rows where an organ clash has occurred.
     */
    private void setOrganClashIndicator() {
        receivingOrganList.setRowFactory(row -> new TableRow<ReceiverOrgan>() {
            @Override
            public void updateItem(ReceiverOrgan item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty) {
                    setText(null);
                    setStyle("");
                } else {
                    setItem(item);
                    ReceiverOrgan inRow = getTableView().getItems().get(getIndex());
                    if (currentProfile.getDonorOrgans().contains(inRow.getRegisteredOrgan())) {
                        setStyle("-fx-background-color: orange");
                    }
                }
            }
        });
    }

    /**
     * Function creates the Map that the checklist uses to track what the current checkbox selection model is. The
     * map contains HashMap of (OrganEnum, ObservableValue(Boolean)).
     */
    private void populateReceiverCheckList() {
        organsList.clear();
        for (OrganEnum organ : OrganEnum.values()) {
            organsList.add(new ReceiverOrgan(organ, LocalDate.now()));
        }
    }

    /**
     * Function updates the observable list that the table tracks
     */
    private void updateCurrentReceivingOrgans() {
        currentReceivingOrgans.clear();
        currentReceivingOrgans.addAll(currentProfile.getReceiverOrgansWithTimes());
    }

    /**
     * Function sets the table to be editable. Adds an additional tableColumn to the receiver table that consists of
     * checkboxes and removes the dateRegistered column.
     */
    @Override
    public void reveal() {
        organReceiverText.setText("Select organs to register");
        receivingOrganList.setItems(organsList);
        receivingOrganList.getColumns().remove(receivingOrganRegisteredColumn);
        if (!receivingOrganList.getColumns().contains(checkBoxColumn)) {
            receivingOrganList.getColumns().add(checkBoxColumn);
        }
        receivingOrganList.setEditable(true);
        receivingOrganList.refresh();
    }

    /**
     * Function restores the receivingOrgans table back to its original state. Removes the checkBox column and clears
     * the list so that only registered organs populate it. Makes the table uneditable and adds the date
     * Registered column back to the table
     */
    @Override
    public void hide() {
        organReceiverText.setText("Registered Organs to Receive");
        receivingOrganList.getColumns().remove(checkBoxColumn);
        if (!receivingOrganList.getColumns().contains(receivingOrganRegisteredColumn)) {
            receivingOrganList.getColumns().add(receivingOrganRegisteredColumn);
        }
        updateCurrentReceivingOrgans();
        receivingOrganList.setItems(currentReceivingOrgans); //repopulate the checkedOrganList using
        // the values from the profile
        receivingOrganList.setEditable(false);
    }

    /**
     * Function sets up the checkBoxColumn. It sets the initially checked organs that are already registered to
     * receive. Creates a boolean value that is observed, this value is determined by checking if the organ is in the
     * checkedOrgan ArrayList. Adds a changeListener to observableBoolean that updates the checkedOrgans list each
     * time an item is checked/un-checked.
     */
    private void setupCheckBoxColumn() {
        //Make the column a checkBoxColumn
        checkBoxColumn.setCellFactory(CheckBoxTableCell.forTableColumn(checkBoxColumn));
        //Setup the boolean that determines whether the box is checked initially, and bind a Listener to it
        checkBoxColumn.setCellValueFactory(cellDataFeatures -> {

            ReceiverOrgan selectedOrganWithDate =
                    cellDataFeatures.getValue(); //The value in the actual cell (ReceiverHelper)
            OrganEnum selectedOrgan = selectedOrganWithDate.getRegisteredOrgan();

            boolean v = checkedOrgans.contains(selectedOrgan);
            this.observe = new SimpleBooleanProperty(v); //Set the boolean to be observed
            //Bind the change Listener
            observe.addListener((observable, oldValue, newValue) -> {
                if (!currentlyUndoing) {
                    addOrganTableViewToDeque();
                }
                if (newValue) {
                    checkedOrgans.add(selectedOrgan);
                    unregisteredOrgans.remove(selectedOrgan);

                    curedDiseases.values().remove(selectedOrgan);
                } else {

                    // Only show the pop up if we're un-registering an organ that's in the current profile
                    if (currentProfile.getReceiverOrgans().contains(selectedOrgan)) {
                        try {
                            ProcedureRemovalPopUp procedureRemovalPopUp =
                                    new ProcedureRemovalPopUp(currentProfile, selectedOrganWithDate, parentController.getHospitals());
                            procedureRemovalPopUp.showAndWait();

                            String editMessage = procedureRemovalPopUp.getSelectedMessage();
                            unregisteredOrgans.put(selectedOrgan, editMessage);

                            if (procedureRemovalPopUp.isPatientDead()) {
                                Death newDeath = procedureRemovalPopUp.getDeath();
                                parentController.setNewEditDateOfDeath(newDeath.getDateOfDeath());

                                for (OrganEnum organ : checkedOrgans) {
                                    unregisteredOrgans.put(organ, editMessage);
                                }

                                clearCheckedOrgans();
                                receivingOrganList.refresh();


                            } else if (procedureRemovalPopUp.isDiseaseCured()) {
                                curedDiseases.put(procedureRemovalPopUp.getCuredDisease(), selectedOrgan);
                            }

                        } catch (IOException e) {
                            LOGGER.severe("Could not load the FXML for the dialog");
                        }
                    }
                    checkedOrgans.remove(cellDataFeatures.getValue().getRegisteredOrgan());

                }

            });
            return observe; //Tell the checkbox to be checked or not at the start
        });
        checkBoxColumn.setEditable(true);
    }

    /**
     * Function checks whether any organs have been selected to register to the receiver
     *
     * @return Boolean indicating if any organs are registered to receive
     */
    public boolean hasReceiverOrgansSelected() {
        return !checkedOrgans.isEmpty();
    }

    /**
     * Function returns an array of selected organs from the CheckBoxList
     *
     * @return ArrayList of OrganEnum
     */
    public List<OrganEnum> getSelectedReceiverOrgans() {
        return this.checkedOrgans;
    }

    public Map<Disease, OrganEnum> getCuredDiseases() {
        return curedDiseases;
    }

    /**
     * @return Map of the organs that have been unregistered and the reason for removal.
     */
    public Map<OrganEnum, String> getRemovedReceiverOrgans() {
        return unregisteredOrgans;
    }

    public void clearCheckedOrgans() {
        checkedOrgans.clear();
    }

    /**
     * Function creates an UndoableOrganCheckTableView object and pushes it onto the undo deque
     */
    private void addOrganTableViewToDeque() {
        undoRedoController.pushToUndoDeque(new UndoableOrganCheckTableView(checkedOrgans, receivingOrganList));
        updateUndoRedoButtons();
    }

    /**
     * Function to retrieve the checkBoxTableColumn
     *
     * @return {@code TableColumn<ReceiverOrgan, Boolean>}
     */
    public TableColumn<ReceiverOrgan, Boolean> getCheckBoxColumn() {
        return checkBoxColumn;
    }

    /**
     * Function to retrieve the TableView.
     *
     * @return {@code TableView<ReceiverOrgan>}
     */
    public TableView<ReceiverOrgan> getReceivingOrganList() {
        return receivingOrganList;
    }
}
