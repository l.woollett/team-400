package seng302.GUI.Clinician;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Region;
import javafx.scene.text.Text;
import javafx.util.StringConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import seng302.Enum.RegionEnum;
import seng302.GUI.ControllerAccess;
import seng302.GUI.Notifications.PushNotification;
import seng302.Model.Clinician;
import seng302.Model.Hospital;
import seng302.ServerInteracton.ServerQueries.Authentication.AuthToken;
import seng302.ServerInteracton.ServerQueries.ClinicianRequests;
import seng302.Utilities.Style;
import seng302.Utilities.Undo_Redo.*;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import static seng302.Utilities.Tooltips.*;
import static seng302.Utilities.Validator.*;

/**
 * The controller for the FXML that will be included in clinician.fxml under the Profile tab. Will display all the
 * clinician's details and allow them to edit them.
 */
public class ClinicianProfileTabController implements Initializable {

    //****************************************************************************************************************\\
    //                                          Declare all the FXML elements
    //****************************************************************************************************************\\

    private final Logger LOGGER = Logger.getLogger(this.getClass().getName());
    public ClinicianSceneController parentController;
    @FXML
    private Label organisationLabel, staffIdLabel, firstNameLabel, lastNameLabel, regionLabel, workAddressLabel, firstNameDisplayLabel;
    // Tabs and menus
    @FXML
    private AnchorPane mainAnchorPane;
    @FXML
    private Button editCancelButton, applyEditButton;
    // Original fields when page is loaded
    @FXML
    private Text firstNameText, middleNameText, lastNameText, usernameText, workAddressText, regionText, staffIdText,
            organisationText;
    // Editable fields
    @FXML
    private TextField firstNameTextField, middleNameTextField, lastNameTextField, workAddressTextField,
            staffIdTextField;
    @FXML
    private ChoiceBox<Hospital> organisationChoiceBox;
    @FXML
    private ChoiceBox<Enum> regionChoiceBox;
    // Non-FXML stuff
    private boolean acceptableFirstName = true, acceptableMiddleName = true, acceptableLastName = true,
            acceptableWorkAddress = true, acceptableRegion = true, acceptableStaffId = true, acceptableOrganisation =
            true, accept = true;
    // Currently null for comparison
    private Clinician currentClinician = null;
    private List<Hospital> hospitals;
    /**
     * Controls all undo/redo functionality for the class.
     */
    @FXML
    private UndoRedoController undoRedoController;
    private boolean currentlyUndoing;

    /**
     * Function is launched when the clinician GUI is loaded. This function handles setting up the clinician view when
     * it is loaded and sets the elements and events into their starting states.
     *
     * @param location  FXML file, handled automatically by FXMLLoader
     * @param resources Path to the FXML file, handled automatically by FXMLLoader
     */
    @FXML
    public void initialize(URL location, ResourceBundle resources) {
        ControllerAccess.setClinicianProfileTabController(this); // Want to be able to access controller methods
        // outside of this class

        setClinicianFieldsVisible(true);
        setEditFieldsVisible(false);
        setClinicianListeners();
        bindEventHandlers();
    }

    /**
     * Sets the currently Undoing variables for the class.
     *
     * @param currentlyUndoing The value that currentlyUndoing will be set to.
     */
    public void setCurrentlyUndoing(boolean currentlyUndoing) {
        this.currentlyUndoing = currentlyUndoing;
    }

    /**
     * Sets the undoRedoController for the class.
     *
     * @param undoRedoController The UndoRedoController to be set.
     */
    public void setUndoRedoController(UndoRedoController undoRedoController) {
        this.undoRedoController = undoRedoController;
    }

    /**
     * Create a reference to the main controller for the clinician related scenes/FXMLs and pass in the hospitals.
     *
     * @param parentController The main clinician controller.
     * @param hospitals        All the available hospitals.
     */
    public void setParentController(ClinicianSceneController parentController, List<Hospital> hospitals) {
        this.hospitals = hospitals;

        organisationChoiceBox.setItems(FXCollections.observableArrayList(hospitals));
        organisationChoiceBox.setConverter(new StringConverter<Hospital>() {
            @Override
            public String toString(Hospital hospital) {
                return hospital.getName();
            }

            @Override
            public Hospital fromString(String hospitalName) {
                for (Hospital hospital : hospitals) {
                    if (hospital.getName().equals(hospitalName)) {
                        return hospital;
                    }
                }
                LOGGER.severe("The provided hospital name did not match any existing hospitals.");
                return null;
            }
        });

        this.parentController = parentController;
    }

    /**
     * Set the fields based on whether the user can edit them or not.
     *
     * @param state true if they are viewing, false if they are editing.
     */
    public void setClinicianFieldsVisible(boolean state) {
        firstNameText.setVisible(state);
        middleNameText.setVisible(state);
        lastNameText.setVisible(state);
        usernameText.setVisible(state);
        workAddressText.setVisible(state);
        regionText.setVisible(state);
        staffIdText.setVisible(state);
        organisationText.setVisible(state);
    }

    /**
     * Set the fields based on whether they are editing.
     *
     * @param state true if they are editing, false if they are not.
     */
    public void setEditFieldsVisible(boolean state) {
        firstNameTextField.setVisible(state);
        middleNameTextField.setVisible(state);
        lastNameTextField.setVisible(state);
        workAddressTextField.setVisible(state);
        regionChoiceBox.setVisible(state);
        staffIdTextField.setVisible(state);
        organisationChoiceBox.setVisible(state);
        applyEditButton.setVisible(state);
        editCancelButton.setVisible(state);
    }

    /**
     * Adds the listeners to detect if the entered values are valid.
     */
    private void setClinicianListeners() throws NullPointerException {
        staffIdTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    acceptableStaffId = isValidUsername(newValue);
                    Style.applyTextFieldStyle(staffIdTextField, acceptableStaffId, false, VALID_USERNAME_TOOLTIP_TEXT,
                            INVALID_USERNAME_TOOLTIP_TEXT, newValue);

                    if (!oldValue.equals(newValue) && !currentlyUndoing) {
                        pushTextFieldChangeToUndoDeque(this.staffIdTextField, oldValue);
                    }
                }));

        firstNameTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    acceptableFirstName = isValidFirstName(newValue);
                    Style.applyTextFieldStyle(firstNameTextField, acceptableFirstName, false,
                            VALID_FIRST_NAME_TOOLTIP_TEXT, INVALID_FIRST_NAME_TOOLTIP_TEXT, newValue);

                    if (!oldValue.equals(newValue) && !currentlyUndoing) {
                        pushTextFieldChangeToUndoDeque(this.firstNameTextField, oldValue);
                    }
                }));

        middleNameTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    acceptableMiddleName = isValidMiddleName(newValue);
                    Style.applyTextFieldStyle(middleNameTextField, acceptableMiddleName, true,
                            VALID_MIDDLE_NAME_TOOLTIP_TEXT, INVALID_MIDDLE_NAME_TOOLTIP_TEXT, newValue);

                    if (!oldValue.equals(newValue) && !currentlyUndoing) {
                        pushTextFieldChangeToUndoDeque(this.middleNameTextField, oldValue);
                    }
                }));

        lastNameTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    acceptableLastName = isValidLastName(newValue);
                    Style.applyTextFieldStyle(lastNameTextField, acceptableLastName, true, VALID_LAST_NAME_TOOLTIP_TEXT,
                            INVALID_LAST_NAME_TOOLTIP_TEXT, newValue);

                    if (!oldValue.equals(newValue) && !currentlyUndoing) {
                        pushTextFieldChangeToUndoDeque(this.lastNameTextField, oldValue);
                    }
                }));

        regionChoiceBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (((oldValue == null) || (!oldValue.equals(newValue))) && !currentlyUndoing) {
                addEnumChoiceBoxToDeque(regionChoiceBox, oldValue);
            }
        });

        workAddressTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    acceptableWorkAddress = isValidAddress(newValue);
                    Style.applyTextFieldStyle(workAddressTextField, acceptableWorkAddress, false,
                            VALID_ADDRESS_TOOLTIP_TEXT, INVALID_ADDRESS_TOOLTIP_TEXT, newValue);

                    if (!oldValue.equals(newValue) && !currentlyUndoing) {
                        pushTextFieldChangeToUndoDeque(this.workAddressTextField, oldValue);
                    }
                }));
    }

    /**
     * Pushes an UndoableTextField onto the undo Deque.
     *
     * @param textField TextField that will be wrapped in an UndoableTextField.
     * @param oldValue  String representing the old value of the textField passed in. It is the value the textBox will
     *                  take if an undo event takes place.
     */
    private void pushTextFieldChangeToUndoDeque(TextField textField, String oldValue) {
        undoRedoController.pushToUndoDeque(new UndoableTextField(textField, oldValue));
        updateUndoRedoButtons();
    }

    /**
     * Function creates an UndoableEnumChoiceBox object and pushes to the undo deque.
     *
     * @param choiceBox ChoiceBox: that we want to restore.
     * @param oldValue  Enum: the oldValue to restore to.
     */
    private void addEnumChoiceBoxToDeque(ChoiceBox<Enum> choiceBox, Enum oldValue) {
        undoRedoController.pushToUndoDeque(new UndoableEnumChoiceBox(choiceBox, oldValue));
        updateUndoRedoButtons();
    }

    /**
     * Adds the profile onto the undo stack.
     */
    private void addEditEventToDeque() {
        undoRedoController.pushToUndoDeque(new UndoableClinician(currentClinician, currentClinician, this));
        updateUndoRedoButtons();
    }

    /**
     * Function creates an UndoableProfileCancel object and pushes it onto the undo Deque.
     */
    private void pushClinicianCancelEventToUndoDeque() {
        undoRedoController.pushToUndoDeque(new UndoableClinicianCancel(this, parentController));
        updateUndoRedoButtons();
    }

    /**
     * Ensures that the undo/redo buttons are only enabled if there are available undo/redo.
     */
    private void updateUndoRedoButtons() {
        ControllerAccess.getClinicianSceneController().updateUndoRedoButtons();
    }

    /**
     * Function initializes the event handlers or the editable components of the clinician scene
     */
    private void bindEventHandlers() {
        mainAnchorPane.setOnKeyPressed(event -> {
            KeyCode pressedKey = event.getCode();

            if (pressedKey.equals(KeyCode.BACK_QUOTE)) {
                ControllerAccess.getTerminalSceneController().show();

            }
        });
        applyEditButton.setOnAction(event -> applyEditEvent());
        editCancelButton.setOnAction(event -> cancelEditEvent(false));
    }

    /**
     * Function hides the original displayed information and brings forward the editable fields and buttons and
     * instantiates the fields with current clinician values.
     *
     * @param undo boolean indicating if an undo event is currently happening.
     */
    public void editClinicianEvent(boolean undo) {
        parentController.setAutoRequestBlocked(true);
        setClinicianFieldsVisible(false);
        setEditFieldsVisible(true);
        currentlyUndoing = true;

        if (!undo) {
            firstNameTextField.setText(currentClinician.getFirstName());

            if (currentClinician.getMiddleName() == null) {
                middleNameTextField.setText("");
            } else {
                middleNameTextField.setText(currentClinician.getMiddleName());
            }

            if (currentClinician.getLastName() == null) {
                lastNameTextField.setText("");
            } else {
                lastNameTextField.setText(currentClinician.getLastName());
            }

            if (currentClinician.getAddress() != null) {
                workAddressTextField.setText(currentClinician.getAddress());
            } else {
                workAddressTextField.setText("");
            }

            if (currentClinician.getStaffId() != null) {
                staffIdTextField.setText(currentClinician.getStaffId());
            } else {
                staffIdTextField.setText("");
            }

            Hospital cliniciansHospital = null;
            for (Hospital hospital : hospitals) {
                if (hospital.equals(currentClinician.getHospital())) {
                    cliniciansHospital = hospital;
                    break;
                }
            }
            organisationChoiceBox.setValue(cliniciansHospital);

            ObservableList<Enum> regionEnumOptions = FXCollections.observableArrayList();
            regionEnumOptions.addAll(RegionEnum.values());
            regionChoiceBox.setItems(regionEnumOptions);
            regionChoiceBox.getSelectionModel().select(currentClinician.getRegion());
        }


        parentController.setIsEditing(true);
        parentController.updateUnsavedChangesIndicator(false, true);

        currentlyUndoing = false;
    }

    /**
     * Function handles editing the clinician by retrieving the values from the editable clinician fields and applying
     * the changes to the active clinician
     */
    private void applyEditEvent() {
        editAcceptanceTest();
        if (accept) {
            addEditEventToDeque();
            int startingNumberOfEdits = this.currentClinician.getChanges().size();
            applyClinicianChanges();
            setClinicianFieldsVisible(true);
            setEditFieldsVisible(false);
            setClinicianFields();
            int endingNumberOfEdits = this.currentClinician.getChanges().size();
            parentController.setIsEditing(false);
            boolean hasChanges = (startingNumberOfEdits != endingNumberOfEdits);
            parentController.updateUnsavedChangesIndicator(true, false, hasChanges);

            try {
                ClinicianRequests.getInstance().putClinician(currentClinician, AuthToken.getInstance().getToken());
                PushNotification notification =
                        new PushNotification("Saved", "Changes to the profile were saved!");
                notification.show();
            } catch (HttpClientErrorException e) {
                // Check the status code of the request and handle accordingly
                switch (e.getStatusCode()) {
                    case UNAUTHORIZED:
                        parentController.kickUser();
                        break;
                    default:
                        parentController.handleCriticalError(e);
                }
            } catch (HttpServerErrorException | ResourceAccessException e) {
                parentController.handleCriticalError(e);
            }

            parentController.setAutoRequestBlocked(false);
        } else {
            Alert editProblemAlert = new Alert(Alert.AlertType.ERROR, "Some of the changes made are in an incorrect" +
                    " format. Please check red highlighted fields");
            editProblemAlert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
            editProblemAlert.showAndWait();
        }
    }

    /**
     * Function sets accept = true if all the editable fields are valid
     */
    private void editAcceptanceTest() {
        accept = acceptableFirstName && acceptableMiddleName && acceptableLastName && acceptableWorkAddress &&
                acceptableOrganisation && acceptableStaffId && acceptableRegion;
    }

    /**
     * Will set the new values for the current clinician from the text fields.
     */
    private void applyClinicianChanges() {
        currentClinician.setFirstName(firstNameTextField.getText().trim());
        currentClinician.setMiddleName(middleNameTextField.getText().trim());
        currentClinician.setLastName(lastNameTextField.getText().trim());
        currentClinician.setAddress(workAddressTextField.getText().trim());
        currentClinician.setRegion((RegionEnum) regionChoiceBox.getValue());
        currentClinician.setHospital(organisationChoiceBox.getValue());
        currentClinician.setStaffId(staffIdTextField.getText().trim());
    }

    /**
     * Function sets the text in the clinician GUI to display the clinicians details
     */
    public void setClinicianFields() {
        firstNameDisplayLabel.setText(" " + currentClinician.getFirstName() + "!");
        usernameText.setText(currentClinician.getUsername());
        firstNameText.setText(currentClinician.getFirstName());
        middleNameText.setText(currentClinician.getMiddleName());
        lastNameText.setText(currentClinician.getLastName());

        try {
            workAddressText.setText(currentClinician.getAddress());
        } catch (NullPointerException e) {
            workAddressText.setText("N/A");
        }
        String region = null;
        if (currentClinician.getRegion() != null) {
            region = currentClinician.getRegion().toString();
        }
        regionText.setText(region);
        organisationText.setText(currentClinician.getHospital().getName());
        staffIdText.setText(currentClinician.getStaffId());
    }

    /**
     * User does not want to continue editing, hide all the TextFields for editing and show all the Text nodes to
     * visible for displaying.
     *
     * @param undo whether to add to undo stack or not
     */
    public void cancelEditEvent(boolean undo) {
        setClinicianFieldsVisible(true);
        setEditFieldsVisible(false);

        parentController.setIsEditing(false);
        parentController.updateUnsavedChangesIndicator(false, false);

        if (!undo) {
            pushClinicianCancelEventToUndoDeque();
        }
        parentController.setAutoRequestBlocked(false);
    }

    /**
     * Set the current clinician to be displayed. Will update all the fields to show the clinicians's information.
     *
     * @param clinician The donor whose details are to be displayed.
     */
    public void setCurrentClinician(Clinician clinician) {
        currentClinician = clinician;
        setClinicianFields();
    }

    /**
     * Handles an auto-request event, updating all clinician related details in the clinician profile tab.
     */
    public void handleAutoRequestEvent() {
        try {
            Clinician newClinician = ClinicianRequests.getInstance().getClinician(currentClinician.getUsername(),
                    AuthToken.getInstance().getToken()).getBody();
            parentController.setClinician(newClinician);
            setCurrentClinician(newClinician);
        } catch (HttpClientErrorException e) {
            // Check the status code of the request and handle accordingly
            switch (e.getStatusCode()) {
                case UNAUTHORIZED:
                    Platform.runLater(parentController::kickUser);
                    break;
                default:
                    Platform.runLater(() -> parentController.handleCriticalError(e));
            }
        } catch (HttpServerErrorException | ResourceAccessException e) {
            Platform.runLater(() -> parentController.handleCriticalError(e));
        }
    }
}
