package seng302.GUI.Clinician;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import seng302.GUI.AvailableOrgans.AvailableOrganTabController;
import seng302.GUI.Chat.ChatWindowController;
import seng302.GUI.ControllerAccess;
import seng302.GUI.MainController;
import seng302.GUI.Notifications.AlertDialog;
import seng302.GUI.Notifications.PushNotification;
import seng302.Model.Clinician;
import seng302.Model.Hospital;
import seng302.ModelController.CacheController;
import seng302.ServerInteracton.ServerQueries.Authentication.AuthToken;
import seng302.ServerInteracton.ServerQueries.Chat.ConversationRequests;
import seng302.ServerInteracton.ServerQueries.ProfileRequests;
import seng302.Utilities.AutoRequestTimer;
import seng302.Utilities.Undo_Redo.UndoRedoController;
import seng302.Utilities.Undo_Redo.UndoableTabChange;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;


/**
 * The controller for the clinician.fxml
 */
public class ClinicianSceneController extends MainController {

    @FXML
    private Tab clinicianProfileTab, searchTab, clinicianTransplantWaitingTab, availableOrganTab, requestsTab;
    @FXML
    private Menu fileMenu, chatMenuItem;
    @FXML
    private MenuItem editMenuItem, saveMenuItem, logoutMenuItem, emptyCacheItem;
    @FXML
    private TabPane clinicianTabPane;
    @FXML
    private AnchorPane mainAnchorPane, clinicianProfile, clinicianTransplantWaiting;

    @FXML
    private GridPane clinicianSearchTab;

    @FXML
    private ClinicianProfileTabController clinicianProfileController;
    @FXML
    private ClinicianSearchTabController clinicianSearchTabController;
    @FXML
    private ClinicianTransplantWaitingController clinicianTransplantWaitingController;
    @FXML
    private AvailableOrganTabController organTablePaneController;
    @FXML
    private RequestController requestController;

    private Stage chatWindowStage = null;

    private Clinician clinician;

    private List<Hospital> hospitals = new ArrayList<>();

    // Undo and redo related
    @FXML
    public Button undo, redo;
    private UndoRedoController undoRedoController;
    private SingleSelectionModel<Tab> clinicianTabPaneSelectionModel;
    private Tab currentTab;
    private boolean currentlyUndoing;

    private final Logger LOGGER = Logger.getLogger(this.getClass().getName());

    /**
     * 'Register' this controller in ControllerAccess so we can access this controller from other areas in the code.
     */
    @FXML
    private void initialize() {
        hospitals = loadHospitals();
        ControllerAccess.setClinicianSceneController(this);
        clinicianProfileController.setParentController(this, hospitals);
        clinicianSearchTabController.setParentController(this);
        //requestsAnchorPaneController.setParentController(this);
        clinicianTransplantWaitingController.setParentController(this);
        organTablePaneController.setParentController(this);
        requestController.setParentController(this);


        initializeUndoRedo();
        AutoRequestTimer.getInstance().createAutoRequestTimer(-1, -1, this::handleAutoRequestEvent);
        AutoRequestTimer.getInstance().createAutoRequestTimer(1000, -1, () -> {Platform.runLater(this::handleChatNotificationAutoRequest);});
        autoRequestBlocked = false;

        chatMenuItem.setStyle("-fx-text-color: #000000");
    }

    /**
     * Handles an auto-request event, updating all clinician related details in the application.
     *
     * @return A boolean which is not currently relevant. It part of the function signature so we that can pass this
     * function as a parameter for the autoRequestTimer.
     */
    private Boolean handleAutoRequestEvent() {
        if (!autoRequestBlocked) {
            clinicianProfileController.handleAutoRequestEvent();
            Platform.runLater(this::updateTimeStamp);
        }
        return null;
    }

    /**
     * Creates an UndoRedoController for the window and adds listeners to the undo and redo buttons so they trigger
     * undo and redo events when pressed. Also initialises tab change event listeners for undo/redo
     */
    private void initializeUndoRedo() {
        clinicianTabPaneSelectionModel = clinicianTabPane.getSelectionModel();
        currentTab = clinicianTabPaneSelectionModel.getSelectedItem();
        currentlyUndoing = false;

        undoRedoController = new UndoRedoController();
        setInjectedControllerUndoRedoControllers();

        undo.setOnAction(event -> {
            toggleCurrentlyUndoingForInjectedControllers();
            undoRedoController.undo();
            toggleCurrentlyUndoingForInjectedControllers();
            updateUndoRedoButtons();
        });
        redo.setOnAction(event -> {
            toggleCurrentlyUndoingForInjectedControllers();
            undoRedoController.redo();
            toggleCurrentlyUndoingForInjectedControllers();
            updateUndoRedoButtons();
        });

        updateUndoRedoButtons();
        createTabEventHandlers();
    }

    /**
     * Toggles the currentlyUndoing variable and updates all injected admin window controllers to match this.
     */
    private void toggleCurrentlyUndoingForInjectedControllers() {
        currentlyUndoing = !currentlyUndoing;
        clinicianProfileController.setCurrentlyUndoing(currentlyUndoing);
        clinicianSearchTabController.setCurrentlyUndoing(currentlyUndoing);
        clinicianTransplantWaitingController.setCurrentlyUndoing(currentlyUndoing);
    }

    /**
     * Sets all injected controllers to use the same UndoRedoController as this class.
     */
    private void setInjectedControllerUndoRedoControllers() {
        clinicianProfileController.setUndoRedoController(undoRedoController);
        clinicianProfileController.setCurrentlyUndoing(currentlyUndoing);
        clinicianSearchTabController.setUndoRedoController(undoRedoController);
        clinicianSearchTabController.setCurrentlyUndoing(currentlyUndoing);
        clinicianTransplantWaitingController.setUndoRedoController(undoRedoController);
        clinicianTransplantWaitingController.setCurrentlyUndoing(currentlyUndoing);
    }

    /**
     * Adds event handlers to all tabs in the Clinician scene so that when they are clicked they push a Command onto the
     * undo stack.
     */
    private void createTabEventHandlers() {
        clinicianProfileTab.setOnSelectionChanged(event -> {
            Integer size = 0;
            try {
                size = ProfileRequests.getInstance().profileSearchCount(AuthToken.getInstance().getToken(), new HashMap<>()).getBody();
            } catch (HttpClientErrorException e) {
                // Check the status code of the request and handle accordingly
                switch (e.getStatusCode()) {
                    case UNAUTHORIZED:
                        kickUser();
                        break;
                    default:
                        handleCriticalError(e);
                }
            } catch (HttpServerErrorException | ResourceAccessException e) {
                handleCriticalError(e);
            }
            clinicianSearchTabController.setMaxSize(size);
            handleTabChange(clinicianProfileTab);
        });
        searchTab.setOnSelectionChanged(event -> handleTabChange(searchTab));
        clinicianTransplantWaitingTab.setOnSelectionChanged(event -> handleTabChange(clinicianTransplantWaitingTab));
        availableOrganTab.setOnSelectionChanged(event -> {
            organTablePaneController.postRender();
            handleTabChange(availableOrganTab);
        });
    }

    /**
     * Handler for whenever a tab is selected in the Clinician Scene that pushes the tab change event onto the undo Deque
     * as an UndoableTabChange. Note: It will not add these if the event was caused by and undo or redo.
     *
     * @param tab The new Tab that was selected.
     */
    private void handleTabChange(Tab tab) {
        if (tab.isSelected() && !currentlyUndoing) {
            undoRedoController.pushToUndoDeque(new UndoableTabChange(clinicianTabPaneSelectionModel, currentTab));
            currentTab = clinicianTabPaneSelectionModel.getSelectedItem();
            updateUndoRedoButtons();
        }
    }

    /**
     * Ensures that the undo/redo buttons are only enabled if there are available undo/redo.
     */
    public void updateUndoRedoButtons() {
        undo.setDisable(undoRedoController.isUndoEmpty());
        redo.setDisable(undoRedoController.isRedoEmpty());
    }

    public void setClinician(Clinician clinician) {
        this.clinician = clinician;
        clinicianProfileController.setCurrentClinician(clinician);
        requestController.setClinician(clinician);
    }

    /**
     * The user clicked the 'Edit' menu item.
     */
    @FXML
    private void editEvent() {
        clinicianProfileController.editClinicianEvent(false);
        clinicianTabPane.getSelectionModel().select(clinicianProfileTab);
    }

    @Override
    public void initParentStageReference() {
        this.parentStage = (Stage) mainAnchorPane.getScene().getWindow();
    }

    @Override
    public void initStageTitle() {
        stageTitle = String.format("Clinician: %s %s %s", clinician.getFirstName(), clinician.getMiddleName(), clinician.getLastName());
    }

    /**
     * Clears the cache that is storing drug interactions and shows a confirmation message.
     */
    @FXML
    private void emptyCache() {
        CacheController.emptyCache();
        PushNotification notification = new PushNotification("Cache Cleared", "Cache which was storing the medications has been cleared!");
        notification.show();
    }

    public UndoRedoController getUndoRedoController() {
        return this.undoRedoController;
    }

    public Clinician getClinician() {
        return clinician;
    }

    @FXML
    private void openChat() {
        if (chatWindowStage == null) {
            try {
                chatWindowStage = new Stage();
                chatWindowStage.setOnCloseRequest(event -> chatWindowStage = null);
                FXMLLoader loader = new FXMLLoader(ChatWindowController.class.getResource("chatWindow.fxml"));
                Parent root = loader.load();
                Scene scene = new Scene(root);
                chatWindowStage.setScene(scene);
                chatWindowStage.show();
                chatWindowStage.setResizable(false);

                ChatWindowController controller = loader.getController();
                controller.setParentController(this);
                addStage(chatWindowStage);
            } catch (IOException e) {
                LOGGER.severe(e.getMessage());
            }
        } else {
            chatWindowStage.toFront();
        }
    }

    /**
     * Kicks user out if they are not authenticated
     */
    public void kickUser() {
        closeOpenStages();
        parentStage.close();
        AutoRequestTimer.getInstance().stopTimers();
        organTablePaneController.stopTimer();
        AuthToken.getInstance().clearToken();
        AlertDialog.showAndWait("Information", "You are being logged out because your credentials are no longer valid.");
        gotoLogin();
        Platform.runLater(() -> parentStage.setTitle("Login"));
        parentStage.show();
    }

    /**
     * Generic error handler for if anything in the application goes wrong. Basically just a more elegant way of
     * handling errors instead of the application crashing. It will log the user out so no harm to the system can
     * be done while it is in an unstable state.
     *
     * @param e Exception Any exception that can be thrown.
     */
    public void handleCriticalError(Exception e) {
        e.printStackTrace();
        parentStage.close();
        AutoRequestTimer.getInstance().stopTimers();
        organTablePaneController.stopTimer();
        AuthToken.getInstance().clearToken();
        AlertDialog.showAndWait("Error!", "Oops! Something went wrong. You are being logged out in order to keep your information safe.");
        gotoLogin();
        LOGGER.severe(e.getMessage());
        Platform.runLater(() -> parentStage.setTitle("Login"));
        parentStage.show();
    }

    /**
     * Shows the total number of unread notifications.
     *
     * @param numNotifications The number of unread notifications.
     */
    public void setRequestsTabText(Integer numNotifications){
        if (numNotifications > 0) {
            requestsTab.setStyle("-fx-background-color: #FFA500; -fx-text-fill: #000000;");
            requestsTab.setText(String.format("Requests (%d)", numNotifications));
        } else{
            requestsTab.setStyle("-fx-background-color: null; -fx-text-fill: #000000;");
            requestsTab.setText("Requests");
        }

    }

    @FXML
    @Override
    protected void logout() {
        organTablePaneController.stopTimer();
        super.logout();
    }

    /**
     * Updates the menu item of chat to display new messages.
     */
    private void handleChatNotificationAutoRequest(){
        List<Integer> unreadConversations = new ArrayList<>();
        try {
            unreadConversations = ConversationRequests.getInstance().getUnreadChats(clinician.getUsername(),
                    AuthToken.getInstance().getToken()).getBody();
        } catch (HttpClientErrorException e) {
            // Check the status code of the request and handle accordingly
            switch (e.getStatusCode()) {
                case UNAUTHORIZED:
                    kickUser();
                    break;
                default:
                    handleCriticalError(e);
            }
        } catch (HttpServerErrorException | ResourceAccessException e) {
            handleCriticalError(e);
        }


        if (!unreadConversations.isEmpty()) {
            chatMenuItem.setText("Chat!");
            chatMenuItem.setStyle("-fx-background-color: #4CAF50; -fx-opacity: 0.7;" );

        } else {
            chatMenuItem.setText("Chat");
            chatMenuItem.setStyle("-fx-background-color: white" );
            /* There is some strange thing going on with the bootstrap CSS where the text fill
             turns white without the following line but the colour can be made to any value and the desired
            Result is still produced */
            chatMenuItem.setStyle("-fx-text-fill: black");
        }
    }
}
