package seng302.GUI.Clinician;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.javascript.event.UIEventType;
import com.lynden.gmapsfx.javascript.object.*;
import com.lynden.gmapsfx.shapes.Circle;
import com.lynden.gmapsfx.shapes.CircleOptions;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.Pair;
import netscape.javascript.JSObject;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import seng302.API.GoogleDistanceMatrix;
import seng302.Enum.AvailableOrganRequestStatus;
import seng302.Enum.ConversationStatusEnum;
import seng302.Enum.OrganEnum;
import seng302.GUI.MainController;
import seng302.GUI.Map.GMapController;
import seng302.Model.*;
import seng302.Model.Chat.Conversation;
import seng302.ServerInteracton.ServerQueries.Authentication.AuthToken;
import seng302.ServerInteracton.ServerQueries.AvailableOrganNotificationRequest;
import seng302.ServerInteracton.ServerQueries.AvailableOrganRequests;
import seng302.ServerInteracton.ServerQueries.Chat.ConversationRequests;
import seng302.ServerInteracton.ServerQueries.HospitalRequests;
import seng302.ServerInteracton.ServerQueries.ProfileRequests;
import seng302.Utilities.AutoRequestTimer;
import seng302.Utilities.Haversine;

import java.io.IOException;
import java.net.URL;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Supplier;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class AvailableOrgansPopupController implements Initializable {
    private GoogleMap map;
    private ArrayList<Hospital> hospitalArrayList = new ArrayList<>();

    protected StringProperty from = new SimpleStringProperty();
    protected StringProperty to = new SimpleStringProperty();

    private static final String UNSELECTED_HOSPITAL_MARKER = "https://i.imgur.com/gMuWBGP.png";
    private static final String SELECTED_HOSPITAL_MARKER = "https://i.imgur.com/EzJYipr.png";
    private static final String CURRENT_HOSPITAL_MARKER = "https://i.imgur.com/oueaASt.png";

    @FXML
    private GridPane mainGridPane;

    @FXML
    private TableView<AvailableOrgan> tblAvailableOrgans;

    @FXML
    private GoogleMapView googleMapView;

    @FXML
    private TableView<Hospital> tblSelected;

    @FXML
    private TableColumn colTimeToExpire, colUsername, colOrgan, colHospitalName;

    @FXML
    private Button send;

    // Use this because GMapsfx has inaccurate calculations >:(
    private static final int DELTA = 1000; // 1km/ 1000 metres
    // Or it could be Patrick's bad calculations about the earth, he was muttering something about the earth not being flat?
    // Im not really sure I think he is going crazy at this point saying the earth is round smh.
    // Like I have shown him that water would just fall off if the earth was round and he still wasn't listening.

    private ObservableList<Hospital> selectedHospitals = FXCollections.observableArrayList();
    private ObservableList<AvailableOrgan> organsList = FXCollections.observableArrayList();
    private MainController parentController;
    private Clinician clinician;
    private Circle helicopterBoundary;
    private Circle vehicleBoundary;

    private final Logger LOGGER = Logger.getLogger(getClass().getName());

    private Task showVehicleBoundaryTask;
    private Thread showVehicleBoundaryThread;

    /**
     * The initial call of this class, the URL and Resource bundle aren't used by us.
     *
     * @param url a url
     * @param rb  A resource bundle.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        googleMapView.addMapInializedListener(this::configureMap);
        googleMapView.setKey("AIzaSyB4pl6ve_wkVh3aow66r8OvxVqW0Eqwf1U");

        try {
            hospitalArrayList.addAll(Objects.requireNonNull(
                    HospitalRequests.getInstance().getHospitals(AuthToken.getInstance().getToken()).getBody()));
        } catch (HttpClientErrorException e) {
            switch (e.getStatusCode()) {
                case UNAUTHORIZED:
                    parentController.kickUser();
                    break;
                default:
                    parentController.handleCriticalError(e);
            }
        } catch (HttpServerErrorException | ResourceAccessException | NullPointerException e) {
            parentController.handleCriticalError(e);
        }

        initializeTableRowFactory();

        colUsername.setCellValueFactory((Callback<TableColumn.CellDataFeatures<AvailableOrgan, String>, ObservableValue<String>>) param ->
                new ReadOnlyObjectWrapper<>(param.getValue().getUsername()));

        colTimeToExpire.setCellValueFactory((Callback<TableColumn.CellDataFeatures<AvailableOrgan, String>, ObservableValue<String>>) param ->
                new ReadOnlyObjectWrapper<>(param.getValue().returnSecondsOfExpiryAsString()));

        colTimeToExpire.setCellFactory(new Callback<TableColumn<AvailableOrgan, String>, TableCell<AvailableOrgan, String>>() {
            @Override
            public TableCell<AvailableOrgan, String> call(TableColumn<AvailableOrgan, String> coverCol) {
                return new TableCell<AvailableOrgan, String>() {
                    @Override
                    public void updateItem(String value, boolean empty) {
                        super.updateItem(value, empty);
                        AvailableOrgan currentOrgan = (AvailableOrgan) this.getTableRow().getItem();
                        if (!empty && currentOrgan != null) {
                            setText(value);
                        }
                    }
                };
            }
        });

        colOrgan.setCellValueFactory((Callback<TableColumn.CellDataFeatures<AvailableOrgan, OrganEnum>, ObservableValue<OrganEnum>>) param ->
                new ReadOnlyObjectWrapper<>(param.getValue().getOrgan()));

        colHospitalName.setCellValueFactory((Callback<TableColumn.CellDataFeatures<Hospital, String>, ObservableValue<String>>) param ->
                new ReadOnlyObjectWrapper<>(param.getValue().getName()));

        selectedHospitals.addListener((ListChangeListener<Hospital>) c -> {
            if(c.getList().isEmpty()) {
                send.setDisable(true);
            } else {
                send.setDisable(false);
            }
        });

        tblSelected.setItems(selectedHospitals);
        AutoRequestTimer.getInstance().createAutoRequestTimer(1000, 1000, this::handleUpdateExpiry);

        send.setDisable(true);
    }

    /**
     * This is called after the map is Initialized, we could also call it after
     * the map is loaded, but this way we dont get pins jumping up after it loads,
     * instead doing it while there is a blank screen.
     */
    private void configureMap() {
        MapOptions mapOptions = new MapOptions();
        LatLong newZealand = new LatLong(-42.098786, 173.420904);

        mapOptions.center(newZealand)
                .mapType(MapTypeIdEnum.ROADMAP)
                .zoom(5)
                .overviewMapControl(false)
                .panControl(false)
                .rotateControl(false)
                .streetViewControl(false)
                .zoomControl(false)
                .scaleControl(false)
                .mapTypeControl(false);
        map = googleMapView.createMap(mapOptions, false);
    }

    /**
     * Setting the onclick event for the table of available organs.
     */
    private void initializeTableRowFactory() {
        tblAvailableOrgans.setRowFactory(tableView -> {
            TableRow<AvailableOrgan> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 1 && (!row.isEmpty())) {
                    //Limit Search to just this organ
                    displayReachableHospitals(row.getItem().getSecondsToExpiry(), clinician.getHospital());
                    selectedHospitals.clear();
                }
            });
            return row;
        });
    }

    /**
     * Remove all hospitals that we cannot get to in time at 220kph or less.
     *
     * @param currentSecondsToExpiry The seconds left for the organ to expire.
     * @param hospital               the hospital that the organ is at
     */
    private void displayReachableHospitals(long currentSecondsToExpiry, Hospital hospital) {
        final double METERS_IN_KM = 1000;
        List<Pair<Double, Hospital>> calculatedDistances = new ArrayList<>();
        Marker currentHospitalMarker = null;
        for (Hospital currentComparingHospital : hospitalArrayList) {

            // Do not include the original hospital
            if (!currentComparingHospital.getName().equals(hospital.getName())) {
                double distanceInMeters = new Haversine().calcDistance(hospital.getLatitude(), hospital.getLongitude(),
                        currentComparingHospital.getLatitude(), currentComparingHospital.getLongitude()) * METERS_IN_KM;
                calculatedDistances.add(new Pair<>(distanceInMeters, currentComparingHospital));
            }else{
                MarkerOptions markerOptions = new MarkerOptions();
                LatLong hospitalLocation = new LatLong(hospital.getLatitude(), hospital.getLongitude());
                markerOptions.position(hospitalLocation)
                        .visible(true)
                        .title(hospital.getName())
                        .icon(CURRENT_HOSPITAL_MARKER);

                currentHospitalMarker = new Marker(markerOptions);
                Marker finalCurrentHospitalMarker = currentHospitalMarker;
                map.addUIEventHandler(currentHospitalMarker, UIEventType.rightclick, (JSObject obj) -> {
                    InfoWindowOptions infoWindowOptions = new InfoWindowOptions();
                    infoWindowOptions.content(MessageFormat.format("<H2>{0}</H2>You Are Here!",
                            hospital.getName()));
                    InfoWindow hospitalInfo = new InfoWindow(infoWindowOptions);
                    hospitalInfo.open(map, finalCurrentHospitalMarker);
                });
            }
        }

        List<Pair<Double, Hospital>> reachableHospitalsByHelicopter = filterReachableHospitalsByHelicopter(currentSecondsToExpiry, calculatedDistances);

        List<Hospital> reachableHospitals = new ArrayList<>();
        reachableHospitalsByHelicopter.forEach(doubleHospitalPair -> reachableHospitals.add(doubleHospitalPair.getValue()));
        setHospitals(reachableHospitals, currentHospitalMarker);

        removeBoundaries();

        if(!reachableHospitalsByHelicopter.isEmpty()) {
            double maxHelicopterDistance = Collections.max(reachableHospitalsByHelicopter, Comparator.comparing(Pair<Double, Hospital>::getKey)).getKey();
            maxHelicopterDistance += DELTA;
            showHelicopterBoundary(maxHelicopterDistance, hospital);
        }

        if(showVehicleBoundaryTask != null) {
            try {
                showVehicleBoundaryTask.cancel();
                showVehicleBoundaryThread.join();
            } catch (InterruptedException e) {
                LOGGER.severe(e.getMessage());
            }
        }

        showVehicleBoundaryTask = new Task() {
            @Override
            protected Object call() throws Exception {

                // Assumes that if you can reach it be vehicle you can reach it be helicopter
                List<Pair<Double, Hospital>> reachableHospitalsByVehicle = filterReachableHospitalsByVehicle(currentSecondsToExpiry, hospital, reachableHospitalsByHelicopter, this::isCancelled);
                if (!reachableHospitalsByVehicle.isEmpty() && !isCancelled()) {
                    double maxVehicleDistance = Collections.max(reachableHospitalsByVehicle, Comparator.comparing(Pair<Double, Hospital>::getKey)).getKey();
                    Platform.runLater(() -> showVehicleBoundary(maxVehicleDistance + DELTA, hospital));
                }

                return null;
            }
        };

        showVehicleBoundaryThread = new Thread(showVehicleBoundaryTask);
        showVehicleBoundaryThread.start();
    }

    /**
     * Remove both the reachable vehicle and helicopter boundary from the map.
     */
    private void removeBoundaries() {
        if(helicopterBoundary != null) {
            map.removeMapShape(helicopterBoundary);
        }

        if (vehicleBoundary != null) {
            map.removeMapShape(vehicleBoundary);
        }
    }

    /**
     * Obtain all the hospitals that are reachable by a vehicle. This is called from within a Task because we are
     * querying the API and will take a noticeable amount of time.
     *
     * @param remainingSeconds The time until the organ expires.
     * @param start The starting hospital for the travel. Where the organ is curerntly located.
     * @param hospitals All the hospitals with a distance from the start.
     * @param isCancelled Method from a Task instance to exist the thread early.
     * @return The list of reachable hospitals using a vehicle.
     */
    private List<Pair<Double, Hospital>> filterReachableHospitalsByVehicle(double remainingSeconds, Hospital start, List<Pair<Double, Hospital>> hospitals, Supplier<Boolean> isCancelled) {
        GoogleDistanceMatrix distanceApi = new GoogleDistanceMatrix();
        List<Pair<Double, Hospital>> reachableHospitals = new ArrayList<>();

        for(Pair<Double, Hospital> hospitalPair : hospitals) {

            if(isCancelled.get()) {
                break;
            }

            try {
                double duration = distanceApi.getDuration(start, hospitalPair.getValue());
                if (duration < remainingSeconds) {
                    reachableHospitals.add(hospitalPair);
                }
            } catch (IOException e) {
                LOGGER.severe(e.getMessage());
            }
        }

        return reachableHospitals;
    }

    /**
     * Obtain the reachable hospitals if the we travelled by a helicopter.
     *
     * @param remainingSeconds The remaining seconds until the organ expires.
     * @param hospitals The hospitals with distances from the starting hospital.
     * @return All the reachable hospitals.
     */
    private List<Pair<Double, Hospital>> filterReachableHospitalsByHelicopter(double remainingSeconds, List<Pair<Double, Hospital>> hospitals) {
        List<Pair<Double, Hospital>> reachableHospitals = new ArrayList<>();
        hospitals.sort(Comparator.comparing(Pair::getKey));

        final double MAX_HELICOPTER_DISTANCE_METERS = GMapController.HELICOPTER_SPEED_KM_PER_HOUR * (remainingSeconds / 3600) * 1000;

        for (Pair<Double, Hospital> hospitalAndDistance : hospitals) {
            double distance = hospitalAndDistance.getKey();
            if (distance < MAX_HELICOPTER_DISTANCE_METERS) {
                reachableHospitals.add(hospitalAndDistance);
            }
        }

        return reachableHospitals;
    }


    /**
     * Display a circle showing the distance the helicopter can travel.
     *
     * @param distance         The radius for the circle in meters.
     * @param startingHospital The hospital where the helicopter travels from.
     */
    private void showHelicopterBoundary(double distance, Hospital startingHospital) {
        if (helicopterBoundary != null) {
            map.removeMapShape(helicopterBoundary);
        }

        CircleOptions circleOptions = new CircleOptions()
                .fillOpacity(0.4)
                .fillColor("purple")
                .radius(distance)
                .center(new LatLong(startingHospital.getLatitude(), startingHospital.getLongitude()));
        helicopterBoundary = new Circle(circleOptions);
        map.addMapShape(helicopterBoundary);
    }

    /**
     * Display a circle showing the distance the vehicle can travel.
     *
     * @param distance         The radius for the circle in meters.
     * @param startingHospital The hospital where the helicopter travels from.
     */
    private void showVehicleBoundary(double distance, Hospital startingHospital) {
        if (vehicleBoundary != null) {
            map.removeMapShape(vehicleBoundary);
        }

        CircleOptions circleOptions = new CircleOptions()
                .fillOpacity(0.4)
                .fillColor("orange")
                .radius(distance)
                .center(new LatLong(startingHospital.getLatitude(), startingHospital.getLongitude()));
        vehicleBoundary = new Circle(circleOptions);
        map.addMapShape(vehicleBoundary);
    }

    /**
     * Send a notification for each clinician in the hospital.
     *
     * @param cliniciansAtHospital The hospitals with the clinicians that work at each hospital.
     * @param organ The organ that will the clinicians are notified about.
     * @param donor The organ donor.
     * @param conversationId The unique identifier for the conversation.
     * @return A list of the notified clinicians that belong to the hospital.
     * @exception HttpClientErrorException Error occurred during HTTP request
     * @exception HttpServerErrorException Error occurred during HTTP request
     * @exception ResourceAccessException Error occurred during HTTP request
     * @exception NullPointerException Variable was not meant to be null
     */
    private void notifyHospital(Map<Hospital, List<Clinician>> cliniciansAtHospital, AvailableOrgan organ, Profile donor, int conversationId) throws HttpClientErrorException,
            HttpServerErrorException, ResourceAccessException, NullPointerException {

        for (Map.Entry<Hospital, List<Clinician>> entry : cliniciansAtHospital.entrySet()) {

            List<Clinician> clinicians = entry.getValue();
            Hospital hospital = entry.getKey();

            for (Clinician receiver : Objects.requireNonNull(clinicians)) {
                AvailableOrganNotification toSend = new AvailableOrganNotification(organ.getOrgan(), donor, clinician,
                        receiver, clinician.getHospital(), hospital, AvailableOrganRequestStatus.UNREAD, conversationId);
                AvailableOrganNotificationRequest.getInstance().sendNotification(toSend, AuthToken.getInstance().getToken());
            }
        }
    }

    /**
     * Great a group chat for the provided clinicians.
     *
     * @param clinicians All the clinicians that will be added to the group chat.
     * @return Returns the unique identifier for the created group chat.
     * @exception HttpClientErrorException Error occurred during HTTP request
     * @exception HttpServerErrorException Error occurred during HTTP request
     * @exception ResourceAccessException Error occurred during HTTP request
     * @exception NullPointerException Variable was not meant to be null
     */
    private Integer createGroupChat(List<Clinician> clinicians, AvailableOrgan organ) throws HttpClientErrorException,
            HttpServerErrorException, ResourceAccessException, NullPointerException {
        List<String> clinicianUsernames = clinicians.stream().map(Clinician::getUsername).collect(Collectors.toList());
        String chatCreatorUsername = clinician.getUsername();
        String topic = String.format("%s: %s", clinician.getHospital().getName(), organ.getOrgan().toString());
        Conversation conversation = new Conversation(null, LocalDateTime.now(), chatCreatorUsername, topic,
                null, ConversationStatusEnum.ACTIVE);
        Integer conversationId = ConversationRequests.getInstance().postConversation(conversation, AuthToken.getInstance().getToken()).getBody();

        if (conversationId == null) {
            throw new NullPointerException("The ID for the created conversation is null.");
        }

        ConversationRequests.getInstance().addParticipants(clinicianUsernames, conversationId, AuthToken.getInstance().getToken());

        return conversationId;
    }

    /**
     * Send the organ broadcast out to the selected hospitals which will happen in a background thread.
     */
    @FXML
    private void sendBroadcast() {
        Runnable runnable = () -> {
            try {
                List<Clinician> allClinicians = new ArrayList<>();
                Map<Hospital, List<Clinician>> cliniciansAtHospital = new HashMap<>();
                AvailableOrgan currentOrgan = tblAvailableOrgans.getSelectionModel().getSelectedItem();
                Profile donor = ProfileRequests.getInstance().getProfile(currentOrgan.getUsername(), AuthToken.getInstance().getToken()).getBody();

                for (Hospital h : selectedHospitals) {
                    List<Clinician> clinicians = HospitalRequests.getInstance().getCliniciansAtHospital(h.getName(),
                            AuthToken.getInstance().getToken()).getBody();
                    cliniciansAtHospital.put(h, clinicians);
                    assert clinicians != null;
                    allClinicians.addAll(clinicians);
                }

                if (!allClinicians.isEmpty()) {
                    Integer conversationId = createGroupChat(allClinicians, currentOrgan);
                    notifyHospital(cliniciansAtHospital, currentOrgan, donor, conversationId);
                }

            } catch (HttpClientErrorException e) {
                switch (e.getStatusCode()) {
                    case UNAUTHORIZED:
                        parentController.kickUser();
                        break;
                    default:
                        parentController.handleCriticalError(e);
                }
            } catch (HttpServerErrorException | ResourceAccessException | NullPointerException e) {
                parentController.handleCriticalError(e);
            }
        };

        if (!selectedHospitals.isEmpty()) {
            runnable.run();
            Stage stage = (Stage) tblAvailableOrgans.getScene().getWindow();

            stage.close();
        }
    }


    /**
     * Set the map markers to only the hospitals that we can get the organ to in time.
     *
     * @param newHospitalList the list of possible hospitals.
     */
    private void setHospitals(List<Hospital> newHospitalList, Marker currentHospitalMarker) {
        map.clearMarkers();
        map.addMarker(currentHospitalMarker);
        for (Hospital hospital : newHospitalList) {
            MarkerOptions markerOptions = new MarkerOptions();
            LatLong hospitalLocation = new LatLong(hospital.getLatitude(), hospital.getLongitude());
            markerOptions.position(hospitalLocation)
                    .visible(true)
                    .title(hospital.getName())
                    .icon(UNSELECTED_HOSPITAL_MARKER);

            Marker hospitalMarker = new Marker(markerOptions);
            map.addUIEventHandler(hospitalMarker, UIEventType.rightclick, (JSObject obj) -> {
                InfoWindowOptions infoWindowOptions = new InfoWindowOptions();
                infoWindowOptions.content(MessageFormat.format("<H2>{0}</H2>Address: {3} <br>Longitute: {2}<br>Latitude: {1}",
                        hospital.getName(), hospital.getLatitude(), hospital.getLongitude(), hospital.getAddress()));
                InfoWindow hospitalInfo = new InfoWindow(infoWindowOptions);
                hospitalInfo.open(map, hospitalMarker);
            });


            map.addUIEventHandler(hospitalMarker, UIEventType.click, (JSObject obj) -> {
                        if (!selectedHospitals.contains(hospital)) {
                            selectedHospitals.add(hospital);
                            markerOptions.icon(SELECTED_HOSPITAL_MARKER);
                        } else {
                            selectedHospitals.remove(hospital);
                            markerOptions.icon(UNSELECTED_HOSPITAL_MARKER);
                        }
                        hospitalMarker.setOptions(markerOptions);
                    }
            );
            map.addMarker(hospitalMarker);
        }
    }

    /**
     * Set the clinician that we're using
     *
     * @param clinician The clinician managing these organs.
     */
    public void setClinician(Clinician clinician) {
        this.clinician = clinician;
        String token = AuthToken.getInstance().getToken();
        try {
            int hospitalId = Objects.requireNonNull(HospitalRequests.getInstance().getHospitalIdFromName(token, clinician.getHospital().getName()).getBody());
            for (AvailableOrgan or : Objects.requireNonNull(AvailableOrganRequests.getInstance().getAvailableOrgans(Integer.toString(hospitalId), token).getBody())) {
                if (!(or.getSecondsSinceDeath() < 0)) {
                    organsList.add(or); //Organ is not expired d
                }
            }
        } catch (HttpClientErrorException e) {
            switch (e.getStatusCode()) {
                case UNAUTHORIZED:
                    parentController.kickUser();
                    break;
                default:
                    parentController.handleCriticalError(e);
            }
        } catch (HttpServerErrorException | ResourceAccessException | NullPointerException e) {
            parentController.handleCriticalError(e);
        }
    }

    /**
     * Update expiry time method we run on each organ in the table which is handled by the handleUpdateExpiry function.
     * Has a test method ot check removal of an item form the table.
     */
    private void updateExpiryTime() {
        Iterator<AvailableOrgan> organIterator = organsList.iterator();
        while (organIterator.hasNext()) {
            AvailableOrgan organ = organIterator.next();
            organ.updateExpiry();
            if (organ.getExpiryPercentage() >= 100) {
                organIterator.remove();
            }
        }
        tblAvailableOrgans.setItems(organsList);
        tblAvailableOrgans.refresh();
    }

    /**
     * Handler for updating the organ expiry time
     *
     * @return Not really sure why this is needed. I am just following what the AutoRequestTimer class uses.
     */
    private Boolean handleUpdateExpiry() {
        Platform.runLater(this::updateExpiryTime);
        return null;
    }

    /**
     * Define a reference to the main controller.
     *
     * @param parentController The main controller so we can kick the user and handle critical errors.
     */
    public void setParentController(MainController parentController) {
        this.parentController = parentController;
    }
}
