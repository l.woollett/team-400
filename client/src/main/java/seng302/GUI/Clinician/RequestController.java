package seng302.GUI.Clinician;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Tab;
import seng302.Enum.AvailableOrganRequestStatus;
import seng302.GUI.MainController;
import seng302.Model.AvailableOrgan;
import seng302.Model.AvailableOrganNotification;
import seng302.Model.Clinician;
import seng302.ServerInteracton.ServerQueries.Authentication.AuthToken;
import seng302.ServerInteracton.ServerQueries.AvailableOrganNotificationRequest;
import seng302.ServerInteracton.ServerQueries.NotificationRequests;

import java.util.ArrayList;
import java.util.List;

public class RequestController {

    @FXML
    private Tab incomingTabLabel, outgoingTabLabel;

    @FXML
    private IncomingTabController incomingTabController;
    @FXML
    private OutgoingTabController outgoingTabController;

    //The list keeps all incoming requests for a clinician.
    private List<AvailableOrganNotification> allIncomingOrganNotifications = null;
    private List<AvailableOrganNotification> allOutgoingOrganNotifications = null;

    private Clinician clinician;
    private ClinicianSceneController mainController;

    /**
     * Passed in value of the clinician as the current running user.
     *
     * @param clinician The given clinician.
     */
    public void setClinician(Clinician clinician) {
        this.clinician = clinician;
        outgoingTabController.setClinician(clinician);
        setupNotifications();
    }

    /**
     * Checks if a notification's organ has expired.
     * If it has, sets the status to expired.
     * @param notification the notification we're checking
     */
    private void checkRequestExpired(AvailableOrganNotification notification){
        AvailableOrgan curr = new AvailableOrgan(notification.getDonor().getUsername(), notification.getDonor().getDeath().getDateOfDeath(), notification.getDonor().getDeath().getTimeOfDeath(), notification.getOrgan());
        if(curr.getExpiryPercentage() >= 100){
            notification.setStatus(AvailableOrganRequestStatus.EXPIRED);
            NotificationRequests.getInstance().putAnIncomingNotification(notification, AuthToken.getInstance().getToken());
        }
    }

    public void setParentController(ClinicianSceneController mainController) {
        this.mainController = mainController;
        outgoingTabController.setParentController(mainController);
        incomingTabController.setParentController(mainController);
    }


    /**
     * Send request to server-side and get result for displaying.
     */
    private void setupNotifications() {
        allIncomingOrganNotifications = NotificationRequests.getInstance().getAllIncomingNotifications(
                AuthToken.getInstance().getToken(), this.clinician.getUsername()).getBody();

        allOutgoingOrganNotifications = AvailableOrganNotificationRequest.getInstance().getNotifications(
                this.clinician.getUsername(), AuthToken.getInstance().getToken()).getBody();

        //Checking if an organ has expired.
        for(AvailableOrganNotification notification: allIncomingOrganNotifications){
            checkRequestExpired(notification);
        }
        for(AvailableOrganNotification notification: allOutgoingOrganNotifications){
            checkRequestExpired(notification);
        }

        updateNotificationDisplay();
        setAllIncomingOrganNotificationsInTabs();
    }

    /**
     * Dynamically display the incoming notification tab based on the incoming notification items.
     */
    private void updateNotificationDisplay() {
        Platform.runLater(() -> {
            int totalSize = 0;
            String incomingSize = getIncomingNotification(allIncomingOrganNotifications);
            String outgoingSize = getOutgoingNotification(allOutgoingOrganNotifications);

            for (AvailableOrganNotification notif : allIncomingOrganNotifications){
                if (notif.getStatus() == AvailableOrganRequestStatus.UNREAD){
                    totalSize += 1;
                }
            }

            for (AvailableOrganNotification notif : allOutgoingOrganNotifications) {
                if (notif.getStatus() == AvailableOrganRequestStatus.INTERESTED) totalSize += 1;
            }

            mainController.setRequestsTabText(totalSize);

            incomingTabLabel.setText(incomingSize);
            outgoingTabLabel.setText(outgoingSize);
            if (outgoingSize.equals("Outgoing")) {
                outgoingTabLabel.setStyle("-fx-background-color: null; -fx-text-fill: #000000;");
            } else {
                outgoingTabLabel.setStyle("-fx-background-color: #CCFF99; -fx-text-fill: #000000;");
            }

            if (incomingSize.equals("Incoming") && allIncomingOrganNotifications.size() == 0) {
                incomingTabLabel.setDisable(true);
                incomingTabLabel.setStyle("-fx-background-color: null; -fx-text-fill: #000000;");
            } else if(!incomingSize.equals("Incoming")) {
                incomingTabLabel.setDisable(false);
                incomingTabLabel.setStyle("-fx-background-color: #CCFF99; -fx-text-fill: #000000;");
            } else{
                incomingTabLabel.setDisable(false);
                incomingTabLabel.setStyle("-fx-background-color: null; -fx-text-fill: #000000;");
            }
        });
    }

    /**
     * Creates the message to be displayed on the outgoing notifications tab. It will say the number of clinicians
     * that are interested in your notifications.
     *
     * @param allOutgoingOrganNotifications All the notifications you have created.
     * @return The message in the request tab to be displayed.
     */
    private String getOutgoingNotification(List<AvailableOrganNotification> allOutgoingOrganNotifications) {
        int size = 0;
        for(AvailableOrganNotification a : allOutgoingOrganNotifications){
            if(a.getStatus() == AvailableOrganRequestStatus.INTERESTED){
                size += 1;
            }
        }
        if (size >= 1) {
            return String.format("Outgoing (%d)", size);
        } else {
            return "Outgoing";
        }
    }

    /**
     * Gets the unread notifications for the incoming organs.
     *
     * @param allIncomingOrganNotifications the list of incoming organs.
     * @return a string saying Incoming (Num) or just incoming.
     */
    private String getIncomingNotification(List<AvailableOrganNotification> allIncomingOrganNotifications) {
        int size = 0;
        for (AvailableOrganNotification a : allIncomingOrganNotifications) {
            if (a.getStatus() == AvailableOrganRequestStatus.UNREAD) {
                size += 1;
            }
        }
        if (size >= 1) {
            return String.format("Incoming (%d)", size);
        } else {
            return "Incoming";
        }
    }


    /**
     * Set all incoming notifications to the incoming tab page.
     * We have to clone the list, as editing the base list will lock us out of the incoming tab
     * if we remove all the elements.
     */
    private void setAllIncomingOrganNotificationsInTabs() {
        List<AvailableOrganNotification> incomingNotifications = new ArrayList<>(allIncomingOrganNotifications);
        List<AvailableOrganNotification> outgoingNotifications = new ArrayList<>(allOutgoingOrganNotifications);
        incomingTabController.setAllIncomingOrganNotifications(incomingNotifications);
        outgoingTabController.setOutgoingOrganNotifications(outgoingNotifications);
    }

}
