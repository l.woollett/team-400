package seng302.GUI.Clinician;


import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.util.Callback;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import seng302.Enum.AvailableOrganRequestStatus;
import seng302.GUI.MainController;
import seng302.GUI.Notifications.PushNotification;
import seng302.Model.AvailableOrganNotification;
import seng302.ServerInteracton.ServerQueries.Authentication.AuthToken;
import seng302.ServerInteracton.ServerQueries.Chat.ConversationRequests;
import seng302.ServerInteracton.ServerQueries.NotificationRequests;
import seng302.Utilities.Style;

import java.util.ArrayList;
import java.util.List;

public class IncomingTabController {
    @FXML
    private TableView<AvailableOrganNotification> incomingRequestId;
    @FXML
    private TableColumn fromColumnId, organColumnId, msgStatusColumnId;
    @FXML
    private Label labelTitle, birthGenderId, ageId, dodId, bloodId, bmiId, alcholId, smokerId, hospitalFromId, OrganId;
    @FXML
    private ComboBox<AvailableOrganRequestStatus> comboFilter;
    @FXML
    private GridPane detailsPaneId;
    public Button interestedBtnId, notInterestedBtnId;

    private List<AvailableOrganNotification> allIncomingOrganNotifications = null;
    private ObservableList<AvailableOrganNotification> toDisplay = FXCollections.observableArrayList();

    private int rowIndex = -1;
    private AvailableOrganNotification currentObject;
    private ObservableList<AvailableOrganRequestStatus> comboItems = FXCollections.observableArrayList();

    private AvailableOrganRequestStatus selected = null;
    private ArrayList<AvailableOrganRequestStatus> interactableStatuses = new ArrayList<>();

    private MainController parentController;

    @FXML
    private void initialize() {
        comboItems.addAll(AvailableOrganRequestStatus.values());
        comboFilter.setItems(comboItems);
        interactableStatuses.add(AvailableOrganRequestStatus.UNREAD);
        interactableStatuses.add(AvailableOrganRequestStatus.READ);
        interactableStatuses.add(AvailableOrganRequestStatus.INTERESTED);
        interactableStatuses.add(AvailableOrganRequestStatus.UNINTERESTED);
    }


    /**
     * Clicking on a item in the combo box
     *
     * @param actionEvent The action on clicking on the combobox
     */
    public void comboFilterClicked(ActionEvent actionEvent) {
        selected = comboFilter.getSelectionModel().getSelectedItem();
        setUpTableView();
    }

    /**
     * Passed in value of the available incoming notification list for the current running user.
     *
     * @param notifications Given available incoming notification list.
     */
    public void setAllIncomingOrganNotifications(List<AvailableOrganNotification> notifications) {
        this.allIncomingOrganNotifications = null;
        this.allIncomingOrganNotifications = notifications;
        setUpTableView();
        displayIncomingMsgDetails();
    }


    /**
     * Opens the detail of an incoming notification when clicking on a row in the incoming table.
     */
    private void displayIncomingMsgDetails() {
        incomingRequestId.setRowFactory(tableView -> {
            TableRow<AvailableOrganNotification> row = new TableRow<>();
            row.setOnMouseClicked(event -> {

                detailsPaneId.setDisable(false);

                if (event.getClickCount() == 1 && (!row.isEmpty())) {
                    detailsPaneId.setVisible(true);
                    rowIndex = row.getIndex();
                    currentObject = this.allIncomingOrganNotifications.get(rowIndex);
                    if (currentObject.getStatus() == AvailableOrganRequestStatus.UNREAD) {
                        currentObject.setStatus(AvailableOrganRequestStatus.READ);
                        NotificationRequests.getInstance().putAnIncomingNotification(
                                currentObject,
                                AuthToken.getInstance().getToken());
                    }

                    if (currentObject.getDonor().getBirthGender() == null) {
                        birthGenderId.setText("");
                    } else {
                        birthGenderId.setText(currentObject.getDonor().getBirthGender().toString());
                    }
                    ageId.setText(String.valueOf(currentObject.getDonor().getAge()));
                    dodId.setText(String.valueOf(currentObject.getDonor().getDeath().getDateOfDeath()));
                    if (currentObject.getDonor().getBloodType() == null) {
                        bloodId.setText(String.valueOf(""));
                    } else {
                        bloodId.setText(String.valueOf(currentObject.getDonor().getBloodPressure()));
                    }
                    bmiId.setText(String.valueOf(currentObject.getDonor().getBmi()));
                    alcholId.setText(String.valueOf(currentObject.getDonor().getAlcoholConsumption()));
                    smokerId.setText(String.valueOf(currentObject.getDonor().getSmoker()));
                    hospitalFromId.setText(String.valueOf(currentObject.getOrigin().getName()));
                    OrganId.setText(String.valueOf(currentObject.getOrgan()));

                    if (interactableStatuses.contains(currentObject.getStatus())) {
                        interestedBtnId.setDisable(false);
                        notInterestedBtnId.setDisable(false);
                    } else {
                        interestedBtnId.setDisable(true);
                        notInterestedBtnId.setDisable(true);
                    }
                }
            });
            return row;
        });
    }


    /**
     * Bind value to the table for displaying.
     */
    private void setUpTableView() {

        AvailableOrganNotification currentlySelected = incomingRequestId.getSelectionModel().getSelectedItem();
        for (AvailableOrganNotification notification : toDisplay) {
            if (notification.equals(currentlySelected)) {
                currentlySelected = notification;
                break;
            }
        }

        toDisplay.clear();
        toDisplay.addAll(allIncomingOrganNotifications);
        toDisplay.removeIf(notification -> notification.getStatus() != selected && selected != null);
        incomingRequestId.setItems(toDisplay);
        incomingRequestId.getSelectionModel().select(currentlySelected);

        fromColumnId.setCellValueFactory((Callback<TableColumn.CellDataFeatures<AvailableOrganNotification, String>, ObservableValue<String>>) param ->
                new ReadOnlyObjectWrapper<>(param.getValue().getOrigin().getName()));
        organColumnId.setCellValueFactory((Callback<TableColumn.CellDataFeatures<AvailableOrganNotification, String>, ObservableValue<String>>) param ->
                new ReadOnlyObjectWrapper<>(param.getValue().getOrgan().toString()));
        msgStatusColumnId.setCellValueFactory((Callback<TableColumn.CellDataFeatures<AvailableOrganNotification, String>, ObservableValue<String>>) param ->
                new ReadOnlyObjectWrapper<>(param.getValue().getStatus().toString()));

        msgStatusColumnId.setCellFactory(new Callback<TableColumn<AvailableOrganNotification, String>, TableCell<AvailableOrganNotification, String>>() {
            @Override
            public TableCell<AvailableOrganNotification, String> call(TableColumn<AvailableOrganNotification, String> coverCol) {
                return new TableCell<AvailableOrganNotification, String>() {
                    @Override
                    public void updateItem(String value, boolean empty) {
                        AvailableOrganNotification currentOrgan = (AvailableOrganNotification) this.getTableRow().getItem();
                        if (!empty && currentOrgan != null) {
                            AvailableOrganRequestStatus status = currentOrgan.getStatus();
                            String cssSyle = Style.getStatusStyle(status);
                            setStyle(cssSyle);
                            setText(currentOrgan.getStatus().toString());
                        } else {
                            setStyle(Style.CLEAR_STATUS_CSS);
                        }
                    }
                };
            }
        });
    }

    /**
     * Click on Deny button will send an update request to the db, to change the response status to deny
     */
    public void clickNotInterestedBtn() {
        if (rowIndex > -1) {
            try {
                removeNotification(currentObject);

                currentObject.setStatus(AvailableOrganRequestStatus.UNINTERESTED);
                NotificationRequests.getInstance().putAnIncomingNotification(
                        currentObject,
                        AuthToken.getInstance().getToken());

                addNotification(currentObject);
                new PushNotification("Replied!", "Reply has been sent!").show();

                List<String> participant = new ArrayList<>();
                participant.add(currentObject.getReceiver().getUsername());
                int conversationId = currentObject.getConversationId();
                ConversationRequests.getInstance().removeParticipant(participant, conversationId,
                        AuthToken.getInstance().getToken());
            } catch (HttpClientErrorException e) {
                // Check the status code of the request and handle accordingly
                switch (e.getStatusCode()) {
                    case UNAUTHORIZED:
                        parentController.kickUser();
                        break;
                    default:
                        parentController.handleCriticalError(e);
                }
            } catch(HttpServerErrorException | ResourceAccessException e) {
                parentController.handleCriticalError(e);
            }

        }
    }

    /**
     * Click on Accept button will send an update request to the db, to change the response status to accept
     */
    public void clickInterestedBtn() {
        if (rowIndex > -1) {
            try {
                removeNotification(currentObject);

                currentObject.setStatus(AvailableOrganRequestStatus.INTERESTED);
                NotificationRequests.getInstance().putAnIncomingNotification(
                        currentObject,
                        AuthToken.getInstance().getToken());

                addNotification(currentObject);
                new PushNotification("Replied!", "Reply has been sent!").show();
            } catch (HttpClientErrorException e) {
                // Check the status code of the request and handle accordingly
                switch (e.getStatusCode()) {
                    case UNAUTHORIZED:
                        parentController.kickUser();
                        break;
                    default:
                        parentController.handleCriticalError(e);
                }
            } catch(HttpServerErrorException | ResourceAccessException e) {
                parentController.handleCriticalError(e);
            }

        }
    }

    /**
     * Remove a notification from the table.
     *
     * @param notification the notification to remove.
     */
    private void removeNotification(AvailableOrganNotification notification) {
        toDisplay.remove(notification);
    }

    /**
     * Add a notification to the table
     * @param notification the notification to add
     */
    private void addNotification(AvailableOrganNotification notification){
        toDisplay.add(notification);
        incomingRequestId.refresh();
    }

    /**
     * Clear the combo box
     *
     * @param mouseEvent The click that triggers this.
     */
    public void clearCombo(MouseEvent mouseEvent) {
        comboFilter.getSelectionModel().clearSelection();
        detailsPaneId.setDisable(false);
    }

    public void setParentController(MainController parentController) {
        this.parentController = parentController;
    }
}
