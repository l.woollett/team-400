package seng302.GUI.Clinician;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import javafx.util.StringConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import seng302.Enum.AvailableOrganRequestStatus;
import seng302.GUI.MainController;
import seng302.Model.AvailableOrganNotification;
import seng302.Model.Clinician;
import seng302.Model.Profile;
import seng302.ServerInteracton.ServerQueries.Authentication.AuthToken;
import seng302.ServerInteracton.ServerQueries.AvailableOrganNotificationRequest;
import seng302.ServerInteracton.ServerQueries.Chat.ConversationRequests;
import seng302.ServerInteracton.ServerQueries.ProfileRequests;
import seng302.Utilities.Style;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.logging.Logger;

public class OutgoingTabController {

    @FXML
    private Text birthGenderLbl, ageLbl, dateOfDeathLbl, bloodTypeLbl, smokerLbl, alcoholConsumptionLbl, bmiLbl,
            statusLbl, outgoingTimeLbl;
    @FXML
    private Button withdrawButton, transferButton, denyButton;
    @FXML
    private TableView<AvailableOrganNotification> outgoingNotificationsTable;
    @FXML

    private TableColumn<AvailableOrganNotification, String> recipientClinician;
    @FXML
    private TableColumn<AvailableOrganNotification, String> recipientHospital;
    @FXML
    private TableColumn<AvailableOrganNotification, String> organSent;
    @FXML
    private TableColumn<AvailableOrganNotification, String> statusCol;

    private MainController parentController;

    @FXML
    public ComboBox<AvailableOrganRequestStatus> comboFilter;

    private final Logger LOGGER = Logger.getLogger(getClass().getName());
    private ObservableList<AvailableOrganNotification> toDisplay = FXCollections.observableArrayList();
    private ObservableList<AvailableOrganRequestStatus> comboItems = FXCollections.observableArrayList();
    private AvailableOrganRequestStatus selected = null;
    private Clinician clinician;
    private List<AvailableOrganNotification> allNotifications = new ArrayList<>();
    private Stage availableOrgansPopup;

    private Timer updateOutgoingRequests;

    /**
     * Sets the parent controller so that we can pass vars through
     *
     * @param parentController The parent controller
     */
    public void setParentController(MainController parentController) {
        this.parentController = parentController;
    }

    @FXML
    private void initialize() {
        withdrawButton.setVisible(false);
        transferButton.setVisible(false);
        denyButton.setVisible(false);

        comboItems.clear();
        List<AvailableOrganRequestStatus> statuses = Arrays.asList(AvailableOrganRequestStatus.INTERESTED,
                AvailableOrganRequestStatus.UNINTERESTED, AvailableOrganRequestStatus.EXPIRED,
                AvailableOrganRequestStatus.WITHDRAWN, AvailableOrganRequestStatus.IN_PROGRESS,
                AvailableOrganRequestStatus.TRANSFERRED, AvailableOrganRequestStatus.DECLINED);

        comboItems.addAll(statuses);

        comboFilter.setConverter(new StringConverter<AvailableOrganRequestStatus>() {
            @Override
            public String toString(AvailableOrganRequestStatus status) {
                return status.toString();
            }

            @Override
            public AvailableOrganRequestStatus fromString(String statusStr) {
                for (AvailableOrganRequestStatus status : AvailableOrganRequestStatus.values()) {
                    if (status.toString().equals(statusStr)) {
                        return status;
                    }
                }

                LOGGER.severe("The value did not match any known notification statuses: " + statusStr);
                return null;
            }
        });


        comboFilter.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            selected = newValue;
            displayNotifications();
        });

        comboFilter.setItems(comboItems);
    }

    private void setupTableView() {
        recipientHospital.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getDestination().getName()));
        recipientClinician.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getReceiver().getUsername()));
        organSent.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getOrgan().toString()));
        statusCol.setCellValueFactory(param -> {
            AvailableOrganRequestStatus status = param.getValue().getStatus();
            String display;

            if (status.equals(AvailableOrganRequestStatus.UNREAD) ||
                    status.equals(AvailableOrganRequestStatus.READ)) {
                display = AvailableOrganRequestStatus.IN_PROGRESS.toString();
            } else {
                display = status.toString();
            }

            return new SimpleStringProperty(display);

        });

        statusCol.setCellFactory(new Callback<TableColumn<AvailableOrganNotification, String>, TableCell<AvailableOrganNotification, String>>() {
            @Override
            public TableCell<AvailableOrganNotification, String> call(TableColumn<AvailableOrganNotification, String> coverCol) {
                return new TableCell<AvailableOrganNotification, String>() {
                    @Override
                    public void updateItem(String value, boolean empty) {
                        AvailableOrganNotification currentNotification = (AvailableOrganNotification) this.getTableRow().getItem();
                        if (!empty && currentNotification != null) {
                            AvailableOrganRequestStatus status = currentNotification.getStatus();
                            String cssStyle = Style.getStatusStyle(status);
                            setStyle(cssStyle);

                            if (currentNotification.getStatus().equals(AvailableOrganRequestStatus.UNREAD) ||
                                    currentNotification.getStatus().equals(AvailableOrganRequestStatus.READ)) {
                                setText(AvailableOrganRequestStatus.IN_PROGRESS.toString());
                            } else {
                                setText(currentNotification.getStatus().toString());
                            }
                        } else {
                            setStyle(Style.CLEAR_STATUS_CSS);
                        }
                    }
                };
            }
        });

        outgoingNotificationsTable.getSelectionModel().selectedItemProperty().addListener(((observable, oldNotification, notification) -> {
            if (notification != null) {
                Profile donor = notification.getDonor();
                if (donor.getBirthGender() == null) {
                    birthGenderLbl.setText("");
                } else {
                    birthGenderLbl.setText(donor.getBirthGender().toString());
                }
                ageLbl.setText(Integer.toString(donor.getAge()));
                dateOfDeathLbl.setText(donor.getDeath().getDateOfDeath().toString());
                if (donor.getBloodType() == null) {
                    bloodTypeLbl.setText("");
                } else {
                    bloodTypeLbl.setText(donor.getBloodType().toString());
                }
                smokerLbl.setText(Boolean.toString(donor.getSmoker()));
                alcoholConsumptionLbl.setText(donor.getAlcoholConsumption().toString());
                bmiLbl.setText(Float.toString(donor.getBmi()));
                if(notification.getStatus() == AvailableOrganRequestStatus.READ || notification.getStatus() == AvailableOrganRequestStatus.UNREAD){
                    statusLbl.setText(AvailableOrganRequestStatus.IN_PROGRESS.toString());
                }else {
                    statusLbl.setText(notification.getStatus().toString());
                }
                outgoingTimeLbl.setText(notification.getSentTimeStamp().toString());

                if (notification.getStatus() == AvailableOrganRequestStatus.TRANSFERRED ||
                        notification.getStatus() == AvailableOrganRequestStatus.EXPIRED ||
                        notification.getStatus() == AvailableOrganRequestStatus.WITHDRAWN ||
                        notification.getStatus() == AvailableOrganRequestStatus.DECLINED) {
                    withdrawButton.setVisible(false);
                } else {
                    withdrawButton.setVisible(true);
                }

                if (notification.getStatus() == AvailableOrganRequestStatus.INTERESTED) {
                    transferButton.setVisible(true);
                    denyButton.setVisible(true);
                } else {
                    transferButton.setVisible(false);
                    denyButton.setVisible(false);
                }

            } else {
                transferButton.setVisible(false);
                denyButton.setVisible(false);
                birthGenderLbl.setText("");
                ageLbl.setText("");
                dateOfDeathLbl.setText("");
                bloodTypeLbl.setText("");
                smokerLbl.setText("");
                alcoholConsumptionLbl.setText("");
                bmiLbl.setText("");
                statusLbl.setText("");
                outgoingTimeLbl.setText("");
            }
        }));
    }

    /**
     * Will display all the outgoing notifications in the table which matches the filtered status.
     */
    private void displayNotifications() {
        List<AvailableOrganNotification> notifications = new ArrayList<>();
        AvailableOrganNotification currentlySelected = outgoingNotificationsTable.getSelectionModel().getSelectedItem();

        // Need to get the instance used in the list that will be displayed
        for (AvailableOrganNotification notification : allNotifications) {
            if (selected == null) {
                notifications.add(notification);
            } else if (selected.equals(AvailableOrganRequestStatus.IN_PROGRESS) &&
                    (notification.getStatus().equals(AvailableOrganRequestStatus.READ) ||
                            notification.getStatus().equals(AvailableOrganRequestStatus.UNREAD))) {
                notifications.add(notification);
            } else if (notification.getStatus().equals(selected)) {
                notifications.add(notification);
            }

            if (notification.equals(currentlySelected)) {
                currentlySelected = notification;
            }
        }

        outgoingNotificationsTable.setItems(FXCollections.observableArrayList(notifications));
        outgoingNotificationsTable.getSelectionModel().select(currentlySelected);
        int selectedRowIndex = outgoingNotificationsTable.getSelectionModel().getSelectedIndex();
        outgoingNotificationsTable.getFocusModel().focus(selectedRowIndex);
    }

    /**
     * This will open the popup used to create a new notification.
     */
    @FXML
    private void createNotification() {
        try {
            FXMLLoader loader = new FXMLLoader(AvailableOrgansPopupController.class.getResource("availableOrgansPopup.fxml"));
            Parent root = loader.load();
            AvailableOrgansPopupController controller = loader.getController();
            controller.setClinician(clinician);
            controller.setParentController(this.parentController);
            Scene scene = new Scene(root);
            availableOrgansPopup = new Stage();
            parentController.addStage(availableOrgansPopup);
            availableOrgansPopup.initModality(Modality.APPLICATION_MODAL);
            availableOrgansPopup.setScene(scene);
            availableOrgansPopup.show();
        } catch (IOException e) {
            LOGGER.severe(e.getMessage());
        }
    }

    /**
     * Sets the current clinician.
     *
     * @param clinician The clinician that was logged in.
     */
    public void setClinician(Clinician clinician) {
        this.clinician = clinician;
    }

    public void clearCombo() {
        comboFilter.getSelectionModel().clearSelection();
    }

    /**
     * Withdraw a organ from the list
     */
    @FXML
    public void withdrawNotification() {
        AvailableOrganNotification notification = outgoingNotificationsTable.getSelectionModel().getSelectedItem();
        removeNotification(notification);
        notification.setStatus(AvailableOrganRequestStatus.WITHDRAWN);
        try {
            AvailableOrganNotificationRequest.getInstance().updateNotification(notification, AuthToken.getInstance().getToken());
        } catch (HttpClientErrorException e) {
            // Check the status code of the request and handle accordingly
            switch (e.getStatusCode()) {
                case UNAUTHORIZED:
                    parentController.kickUser();
                    break;
                default:
                    parentController.handleCriticalError(e);
            }
        } catch (HttpServerErrorException | ResourceAccessException e) {
            parentController.handleCriticalError(e);
        }

        addNotification(notification);
        withdrawButton.setVisible(false);
        transferButton.setVisible(false);
        denyButton.setVisible(false);
    }

    /**
     * Accepting a transfer between two hospitals, denying others
     */
    public void transferNotification() {
        AvailableOrganNotification currentNotification = outgoingNotificationsTable.getSelectionModel().getSelectedItem();

        for (AvailableOrganNotification notification : allNotifications) {
            if (notification.getDonor().equals(currentNotification.getDonor()) && notification.getOrgan().equals(currentNotification.getOrgan())) {
                removeNotification(notification);
                notification.setStatus(AvailableOrganRequestStatus.DECLINED);
                addNotification(notification);
                try {
                    AvailableOrganNotificationRequest.getInstance().updateNotification(notification, AuthToken.getInstance().getToken());
                    Integer convoId = notification.getConversationId();
                    ConversationRequests.getInstance().endConversation(convoId, AuthToken.getInstance().getToken());
                } catch (HttpClientErrorException e) {
                    // Check the status code of the request and handle accordingly
                    switch (e.getStatusCode()) {
                        case UNAUTHORIZED:
                            parentController.kickUser();
                            break;
                        default:
                            parentController.handleCriticalError(e);
                    }
                } catch (HttpServerErrorException | ResourceAccessException e) {
                    parentController.handleCriticalError(e);
                }
            }
        }

        // Send organ away for transfer and now remove it from the donor's list of organs that they have available to donate.
        currentNotification.setStatus(AvailableOrganRequestStatus.TRANSFERRED);
        try {
            AvailableOrganNotificationRequest.getInstance().updateNotification(currentNotification, AuthToken.getInstance().getToken());
            ProfileRequests.getInstance().removeDonatingOrgan(AuthToken.getInstance().getToken(), currentNotification.getDonor().getUsername(), currentNotification.getOrgan());
        } catch (HttpClientErrorException e) {
            // Check the status code of the request and handle accordingly
            switch (e.getStatusCode()) {
                case UNAUTHORIZED:
                    parentController.kickUser();
                    break;
                default:
                    parentController.handleCriticalError(e);
            }
        } catch (HttpServerErrorException | ResourceAccessException e) {
            parentController.handleCriticalError(e);
        }

        transferButton.setVisible(false);
        denyButton.setVisible(false);
        withdrawButton.setVisible(false);
    }

    /**
     * Deny a clinicians request for an organ.
     */
    public void denyNotification() {
        AvailableOrganNotification notification = outgoingNotificationsTable.getSelectionModel().getSelectedItem();
        removeNotification(notification);
        notification.setStatus(AvailableOrganRequestStatus.DECLINED);
        try {
            AvailableOrganNotificationRequest.getInstance().updateNotification(notification, AuthToken.getInstance().getToken());
        } catch (HttpClientErrorException e) {
            // Check the status code of the request and handle accordingly
            switch (e.getStatusCode()) {
                case UNAUTHORIZED:
                    parentController.kickUser();
                    break;
                default:
                    parentController.handleCriticalError(e);
            }
        } catch (HttpServerErrorException | ResourceAccessException e) {
            parentController.handleCriticalError(e);
        }

        addNotification(notification);
        transferButton.setVisible(false);
        denyButton.setVisible(false);
        withdrawButton.setVisible(false);
    }

    /**
     * Remove a notification from the table
     * @param notification the notification to remove.
     */
    private void removeNotification(AvailableOrganNotification notification){
        ObservableList<AvailableOrganNotification> notificationList = outgoingNotificationsTable.getItems();
        notificationList.remove(notification);
        outgoingNotificationsTable.setItems(notificationList);
    }

    /**
     * Add a notification to the table
     * @param notification the notification to add
     */
    private void addNotification(AvailableOrganNotification notification){
        ObservableList<AvailableOrganNotification> notificationList = outgoingNotificationsTable.getItems();
        notificationList.add(notification);
        outgoingNotificationsTable.setItems(notificationList);
        setupTableView();
        Platform.runLater(this::displayNotifications);
    }

    public void setOutgoingOrganNotifications(List<AvailableOrganNotification> outgoingNotifications) {
        this.allNotifications = null;
        this.allNotifications = outgoingNotifications;
        setupTableView();
        Platform.runLater(this::displayNotifications);
    }
}
