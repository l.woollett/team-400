package seng302.GUI.Clinician;

import seng302.Enum.OrganEnum;
import seng302.Model.ReceiverOrgan;

import java.time.LocalDate;

/**
 * This class is the wrapper class of GUI - clinicianTransplantWaitingTab. The class keeps all fields need to be
 * displayed in this page, and sort all records according to the registerDate.
 */
public class ClinicianTransplantWaitingWrapper implements Comparable<ClinicianTransplantWaitingWrapper> {
    private OrganEnum receiveOrgan;
    private String registerDate;
    private String firstName;
    private String middleName;
    private String lastName;
    private String region;
    private String userName;
    private boolean hasOrganClash;


    public ClinicianTransplantWaitingWrapper(OrganEnum receiveOrgan,
                                             String registerDate,
                                             String firstName,
                                             String middleName,
                                             String lastName,
                                             String region,
                                             String userName,
                                             boolean hasOrganClash) {
        this.receiveOrgan = receiveOrgan;
        this.registerDate = registerDate;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.region = region;
        this.userName = userName;
        this.hasOrganClash = hasOrganClash;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getReceiveOrganString() {
        return receiveOrgan.toString();
    }

    public ReceiverOrgan getReceiveOrgan() {
        return new ReceiverOrgan(receiveOrgan, LocalDate.parse(registerDate));
    }

    public String getRegion() {
        return region;
    }

    public String getRegisterDate() {
        return registerDate;
    }

    public String getUserName() {
        return userName;
    }


    /**
     * Compare the registerDate from this record to a given record
     *
     * @param compareTo The given record needs to compare with
     * @return -1 if the registerDate in compareTo param is later than the record itself; 1 if the registerDate in
     * compareTo param is earlier than the record itself; 0 if the registerDate in compareTo param is equal to the
     * record itself.
     */
    @Override
    public int compareTo(ClinicianTransplantWaitingWrapper compareTo) {
        return LocalDate.parse(this.getRegisterDate()).compareTo(LocalDate.parse(compareTo.getRegisterDate()));
    }
}