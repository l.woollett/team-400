package seng302.GUI.Clinician;

import javafx.beans.binding.Bindings;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import seng302.Enum.GenderEnum;
import seng302.Enum.OrganEnum;
import seng302.Enum.RegionEnum;
import seng302.Enum.RoleEnum;
import seng302.GUI.ControllerAccess;
import seng302.GUI.LoginSceneController;
import seng302.GUI.Notifications.PushNotification;
import seng302.GUI.Profile.OpenProfilePage;
import seng302.Model.Profile;
import seng302.ServerInteracton.ServerQueries.Authentication.AuthToken;
import seng302.ServerInteracton.ServerQueries.ClinicianRequests;
import seng302.ServerInteracton.ServerQueries.ProfileRequests;
import seng302.Utilities.ProfileDisplayUtility;
import seng302.Utilities.Tooltips;
import seng302.Utilities.Undo_Redo.UndoRedoController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;


/**
 * The controller for the FXML that will be included under the search tab in the clinician.fxml. Will let the clinician
 * search through the registered users.
 */
public class ClinicianSearchTabController {

    private static final int DEFAULT_RESULTS_TO_SHOW = 50;
    private final Logger LOGGER = Logger.getLogger(getClass().getName());
    private ObservableList<Profile> toDisplay = FXCollections.observableArrayList();
    private TableColumn sortColumn;
    private TableColumn.SortType st = TableColumn.SortType.ASCENDING;
    private int maxSize;
    private int bottomInt = 0;
    private int page = 1;
    @FXML
    private TextField clinicianSearchTextField;
    @FXML
    private TableView<Profile> clinicianSearchTable;
    @FXML
    private TableColumn clinicianSearchTableUsername, clinicianSearchTableFirstName, clinicianSearchTableMiddleName,
            clinicianSearchTableLastName, clinicianSearchTablePreferredName, clinicianSearchTableDonorReceiver,
            clinicianSearchTableAge, clinicianSearchTableGender, clinicianSearchTableBirthGender,
            clinicianSearchTableRegion, clinicianSearchTableHospital, clinicianSearchTableClinician;
    @FXML
    private Label resultLbl, pageNumber;
    @FXML
    private TextField resultTxt;
    @FXML
    private Button clearFiltersButton, showAllResultsBtn;
    @FXML
    private AnchorPane mainAnchorPane;
    @FXML
    private CheckBox showMyPatientsCheckBox, showMyOrganisationCheckBox;
    //Filtering GUI Elements
    @FXML
    private TextField ageFilter;
    @FXML
    private ChoiceBox<Enum> cmbPreferredGenderItems, cmbBirthGenderItems, cmbRegionItems, cmbDonorItems,
            cmbDonorOrganItems, cmbReceivingOrganItems;
    // Pagination
    @FXML
    private Button pageLeft, pageRight;
    private Map<String, String> filters = new HashMap<>();
    private int resToDisplay = 50;
    private List<Profile> filteredProfiles = new ArrayList<>();
    private int currentMaxInt = resToDisplay;


    private ClinicianSceneController parentController;

    /**
     * Controls all undo/redo functionality for the class.
     */
    @FXML
    private UndoRedoController undoRedoController;
    private boolean currentlyUndoing;

    /**
     * Create the text to be displayed when a row in the search table is hovered over. The string will include
     * their full name and their organs to be donated (if they are a donor).
     *
     * @param profile The profile that is hovered over.
     * @return The summary of the profile.
     */
    private static String createHoverText(Profile profile) {
        StringBuilder summary = new StringBuilder(String.format("%s %s %s.", profile.getFirstName(), profile.getMiddleName(), profile.getLastName()));

        if (profile.isDonor()) {
            List<OrganEnum> organs = profile.getDonorOrgans();
            summary.append(" Donor: ");
            for (OrganEnum organ : organs) {
                summary.append(organ.toString()).append(", ");
            }
            //Remove the last comma
            summary = new StringBuilder(summary.toString().replaceAll(", $", ""));
        }

        return summary.toString();
    }

    /**
     * Initialises the FXML elements on the page to their initial values.
     * This includes getting all of the column data from file.
     */
    @FXML
    private void initialize() {
        ControllerAccess.setClinicianSearchTabController(this);

        initializeClinicianSearchTable();
        initialiseFilter();
        setViewPatientsCheckBox();
        setOrganisationsPatientsCheckBox();
        resultLbl.setText("There are " + maxSize + " profiles.");
        setPageButtons();

        clinicianSearchTableFirstName.setCellValueFactory((Callback<TableColumn.CellDataFeatures<Profile, String>, ObservableValue<String>>) param ->
                new ReadOnlyObjectWrapper<>(param.getValue().getFirstName()));

        clinicianSearchTableMiddleName.setCellValueFactory((Callback<TableColumn.CellDataFeatures<Profile, String>, ObservableValue<String>>) param ->
                new ReadOnlyObjectWrapper<>(param.getValue().getMiddleName()));

        clinicianSearchTableLastName.setCellValueFactory((Callback<TableColumn.CellDataFeatures<Profile, String>, ObservableValue<String>>) param ->
                new ReadOnlyObjectWrapper<>(param.getValue().getLastName()));


        clinicianSearchTableDonorReceiver.setCellValueFactory((Callback<TableColumn.CellDataFeatures<Profile, String>, ObservableValue<String>>) param -> {
            Profile profile = param.getValue();
            ArrayList<String> statuses = new ArrayList<>();
            if (profile.isDonor()) {
                statuses.add("Donor");
            }

            if (profile.isReceiver()) {
                statuses.add("Receiver");
            }

            return new ReadOnlyObjectWrapper<>(String.join(", ", statuses));
        });


        clinicianSearchTableAge.setCellValueFactory((Callback<TableColumn.CellDataFeatures<Profile, Integer>, ObservableValue<Integer>>) param ->
                new ReadOnlyObjectWrapper<>(param.getValue().getAge()));

        clinicianSearchTableBirthGender.setCellValueFactory((Callback<TableColumn.CellDataFeatures<Profile, GenderEnum>, ObservableValue<GenderEnum>>) param ->
                new ReadOnlyObjectWrapper<>(param.getValue().getBirthGender()));

        clinicianSearchTableRegion.setCellValueFactory((Callback<TableColumn.CellDataFeatures<Profile, RegionEnum>, ObservableValue<RegionEnum>>) param ->
                new ReadOnlyObjectWrapper<>(param.getValue().getRegion()));

        clinicianSearchTableHospital.setCellValueFactory((Callback<TableColumn.CellDataFeatures<Profile, String>, ObservableValue<String>>) param ->
                new ReadOnlyObjectWrapper<>(param.getValue().getOrganisationName()));

        clinicianSearchTableClinician.setCellValueFactory((Callback<TableColumn.CellDataFeatures<Profile, String>, ObservableValue<String>>) param ->
                new ReadOnlyObjectWrapper<>(param.getValue().getClinicianName()));


        ageFilter.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d+")) {
                ageFilter.setText(newValue.replaceAll("[^\\d]", ""));
            } else {
                filters.put("age", newValue);
            }

            if (newValue.equals("")) {
                filters.remove("age");
            }

            resetPageData();
            updateProfiles();
        });

        cmbBirthGenderItems.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                filters.put("birthGender", newValue.toString().toUpperCase());
            }

            resetPageData();
            updateProfiles();

        });

        cmbRegionItems.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                filters.put("region", newValue.toString());
            }
            resetPageData();
            updateProfiles();

        });

        cmbDonorItems.valueProperty().addListener((observable, oldValue, newValue) -> {
            String param;
            if (newValue != null) {
                switch (newValue.toString()) {
                    case "DONOR":
                        param = "0";
                        break;

                    case "RECEIVER":
                        param = "1";
                        break;

                    default:
                        param = "2";
                        break;

                }
                filters.put("donorStatus", param);
            }
            resetPageData();
            updateProfiles();

        });

        cmbDonorOrganItems.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                filters.put("donatingOrgan", newValue.toString());
            }
            resetPageData();
            updateProfiles();

        });

        cmbReceivingOrganItems.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                filters.put("receivingOrgan", newValue.toString());
            }
            resetPageData();
            updateProfiles();
        });

        clearFiltersButton.setOnAction(event -> {
            resetPageData();
            clearFilters();

        });

        updateProfiles();
    }

    /**
     * Sets the currently Undoing variables for the class.
     *
     * @param currentlyUndoing The value that currentlyUndoing will be set to.
     */
    public void setCurrentlyUndoing(boolean currentlyUndoing) {
        this.currentlyUndoing = currentlyUndoing;
    }

    /**
     * Sets the undoRedoController for the class.
     *
     * @param undoRedoController The UndoRedoController to be set.
     */
    public void setUndoRedoController(UndoRedoController undoRedoController) {
        this.undoRedoController = undoRedoController;
    }

    /**
     * Will add properties to the table view such that when hovering over a row, the profile's full name and organs
     * (if they are a donor). As well as when you double click on a row, a new stage will be created to display that
     * profile's information.
     */
    private void initializeClinicianSearchTable() {
        clinicianSearchTable.setRowFactory(tableView -> {
            TableRow<Profile> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == ProfileDisplayUtility.DOUBLE_CLICK_COUNT && (!row.isEmpty())) {
                    OpenProfilePage openProfilePage = new OpenProfilePage();
                    openProfilePage.openProfileFromDoubleClick(row.getItem().getUsername());
                }
            });
            row.hoverProperty().addListener(observable -> {
                final Profile profile = row.getItem();

                if (row.isHover() && profile != null) {
                    final String toolTipText = createHoverText(profile);
                    row.setTooltip(Tooltips.createNewTooltip(toolTipText));
                }
            });

            // Only displays the option to add to patients if the operator is a clinician.
            if (LoginSceneController.isActiveClinician()) {
                final ContextMenu contextMenu = new ContextMenu();
                final MenuItem addPatientMenuItem = new MenuItem("Add to my patients");
                addPatientMenuItem.setOnAction(event -> {
                    addPatientHandler(row.getItem());
                });
                contextMenu.getItems().add(addPatientMenuItem);


                row.contextMenuProperty().bind(
                        Bindings.when(row.emptyProperty())
                                .then((ContextMenu) null)
                                .otherwise(contextMenu)
                );
            }

            return row;
        });

        clinicianSearchTextField.textProperty().addListener((obj, oldValue, newValue) -> {

            if (!clinicianSearchTextField.getText().equals("")) {

                filters.put("q", clinicianSearchTextField.getText());
            }

            if (clinicianSearchTextField.getText().equals("")) {
                filters.remove("q");
            }
            resetPageData();
            updateProfiles();

        });

        resultTxt.textProperty().addListener((obj, oldValue, newValue) -> {
            if (!newValue.matches("\\d+")) {
                resultTxt.setText(newValue.replaceAll("[^\\d]", ""));
            } else {
                resToDisplay = Integer.parseInt(newValue);
            }

            resetPageData();
            updateProfiles();
        });

        showAllResultsBtn.setOnMouseClicked(event -> {
            showAll();
        });

        //Controls the visibility of Show All button
        showAllResultsBtn.setVisible(true);

        showMyPatientsCheckBox.setOnMouseClicked(event -> {
            resetPageData();
            addPatientFilter();
            updateProfiles();
        });

        showMyOrganisationCheckBox.setOnMouseClicked(event -> {
            resetPageData();
            addOrganisationFilter();
            updateProfiles();
        });

        clinicianSearchTable.setItems(toDisplay);
    }

    /**
     * Adds or removes the patients search param from the filters based on state.
     */
    private void addPatientFilter() {
        if (showMyPatientsCheckBox.isSelected()) {
            filters.put("clinician", parentController.getClinician().getUsername());
        } else {
            filters.remove("clinician");
        }
    }

    /**
     * Adds or removes the clinicians organisation param from the filters based on state.
     */
    private void addOrganisationFilter() {
        if (showMyOrganisationCheckBox.isSelected()) {
            filters.put("organisation", parentController.getClinician().getUsername());
        } else {
            filters.remove("organisation");
        }
    }

    /**
     * Initialises all of the filter fields with the values that they can be filtered on.
     */
    private void initialiseFilter() {
        cmbBirthGenderItems.getItems().add(GenderEnum.MALE);
        cmbBirthGenderItems.getItems().add(GenderEnum.FEMALE);
        cmbDonorOrganItems.getItems().addAll(OrganEnum.values());
        cmbReceivingOrganItems.getItems().addAll(OrganEnum.values());
        cmbRegionItems.getItems().addAll(RegionEnum.values());
        cmbRegionItems.getItems().remove(RegionEnum.NOT_SET);
        cmbDonorItems.getItems().addAll(RoleEnum.values());
    }

    /**
     * Refresh the clinicianSearchTable to show the most current information of the profiles in the data FXCollections
     * observable array list.
     */
    private void refreshTable() {
        clinicianSearchTable.refresh();
    }

    /**
     * Refreshes the table based on the search query (the name) that was entered into the search field
     *
     * @param profiles The query that will search for matching profile names.
     */
    private void updateSearch(List<Profile> profiles) {

        if (!clinicianSearchTable.getSortOrder().isEmpty()) {
            sortColumn = clinicianSearchTable.getSortOrder().get(0);
            st = sortColumn.getSortType();
        }

        toDisplay.clear();
        toDisplay = FXCollections.observableArrayList(profiles);

        if (sortColumn != null) {
            clinicianSearchTable.getSortOrder().add(sortColumn);
            sortColumn.setSortType(st);
            sortColumn.setSortable(true);
        }

        clinicianSearchTable.setItems(toDisplay);
    }


    /**
     * Handles the pressing of the show all button by setting count to max size.
     */
    private void showAll() {
        resetPageData();
        setProfileCount();
        resToDisplay = maxSize;
        resultTxt.setText(Integer.toString(maxSize));
        List<Profile> profiles = getProfiles();
        updateSearch(profiles);
        refreshTable();
        setPageButtons();
    }


    /**
     * Handles the updating of profiles and the refreshing of the table once the new profiles have been set.
     */
    public void updateProfiles() {
        List<Profile> profiles = getProfiles();
        updateSearch(profiles);
        setProfileCount();
        refreshTable();
        setPageButtons();
    }

    /**
     * Handles the updating of profiles after a change of page is done.
     * Essentially means the profile count query isn't repeated each page change.
     */
    private void pageSwitch() {
        List<Profile> profiles = getProfiles();
        updateSearch(profiles);
        refreshTable();
    }

    /**
     * Determines whether changing page is possible.
     *
     * @param up boolean to go page up or down/.
     */
    private void switchPage(boolean up) {

        if (up) {
            if ((bottomInt + resToDisplay) < maxSize) {
                bottomInt += resToDisplay;
                page++;
            }
        } else {
            if ((bottomInt - resToDisplay) < 0) {
                bottomInt = 0;
            } else {
                bottomInt -= resToDisplay;
                page--;
            }
        }

        filters.put("startIndex", Integer.toString(bottomInt));
        pageSwitch();
    }

    /**
     * Determines whether the switch page buttons are active or not.
     */
    private void setPageButtons() {
        if ((bottomInt + resToDisplay) < maxSize) {
            pageRight.setDisable(false);
        } else {
            pageRight.setDisable(true);
        }

        if ((bottomInt - resToDisplay) < 0) {
            pageLeft.setDisable(true);
        } else {
            pageLeft.setDisable(false);
        }
        pageNumber.setText("Page " + (bottomInt / resToDisplay + 1) + " of " + (((maxSize - 1) / resToDisplay) + 1));
    }

    /**
     * Calls the searchProfiles method and queries the Server.
     *
     * @return a list of profiles that match the search options selected in the gui.
     */
    private List<Profile> getProfiles() {
        List<Profile> profiles = null;
        filters.put("count", Integer.toString(resToDisplay));
        try {
            profiles = ProfileRequests.getInstance().searchProfiles(AuthToken.getInstance().getToken(), filters).getBody();
            setPageButtons();
        } catch (HttpClientErrorException e) {
            // Check the status code of the request and handle accordingly
            switch (e.getStatusCode()) {
                case UNAUTHORIZED:
                    parentController.kickUser();
                    break;
                default:
                    parentController.handleCriticalError(e);
            }
        } catch (HttpServerErrorException | ResourceAccessException e) {
            parentController.handleCriticalError(e);
        }
        return profiles;
    }

    /**
     * Sets the max profile size for the query params when not restricted by pagination.
     */
    private void setProfileCount() {
        try {
            maxSize = ProfileRequests.getInstance().profileSearchCount(AuthToken.getInstance().getToken(), filters).getBody();
            resultLbl.setText(maxSize + " profiles match your search.");
        } catch (HttpClientErrorException e) {
            // Check the status code of the request and handle accordingly
            switch (e.getStatusCode()) {
                case UNAUTHORIZED:
                    parentController.kickUser();
                    break;
                default:
                    parentController.handleCriticalError(e);
            }
        } catch (HttpServerErrorException | ResourceAccessException e) {
            parentController.handleCriticalError(e);
        }
    }

    /**
     * Clears all filters and refreshes the table
     */
    private void clearFilters() {
        ageFilter.setText("");
        resultTxt.setText(Integer.toString(DEFAULT_RESULTS_TO_SHOW));
        cmbBirthGenderItems.setValue(null);
        cmbRegionItems.setValue(null);
        cmbDonorItems.setValue(null);
        cmbDonorOrganItems.setValue(null);
        cmbReceivingOrganItems.setValue(null);
        clinicianSearchTextField.setText("");
        showMyPatientsCheckBox.setSelected(false);
        showMyOrganisationCheckBox.setSelected(false);
        resetPageData();
        try {
            filteredProfiles = ProfileRequests.getInstance().searchProfiles(AuthToken.getInstance().getToken(), new HashMap<>()).getBody();
            maxSize = filteredProfiles.size();
        } catch (HttpClientErrorException e) {
            // Check the status code of the request and handle accordingly
            switch (e.getStatusCode()) {
                case UNAUTHORIZED:
                    parentController.kickUser();
                    break;
                default:
                    parentController.handleCriticalError(e);
            }
        } catch (HttpServerErrorException | ResourceAccessException e) {
            parentController.handleCriticalError(e);
        }
        clinicianSearchTable.setItems(FXCollections.observableArrayList(filteredProfiles));
        filters.clear();
        updateProfiles();
    }

    /**
     * Calls the query to the server to link the two profiles.
     *
     * @param profile Profile to be linked.
     */
    private void addPatientHandler(Profile profile) {
        String cliniciansName = parentController.getClinician().getUsername();
        try {
            ClinicianRequests.getInstance().addPatient(cliniciansName, profile, AuthToken.getInstance().getToken());
            ClinicianRequests.getInstance().setOrganisation(cliniciansName, profile, AuthToken.getInstance().getToken());

            PushNotification notification = new PushNotification("Success!", "Patient has been added to your patients");
            notification.show();
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode() == HttpStatus.UNAUTHORIZED) {
                parentController.kickUser();
            }
            LOGGER.warning(e.toString());
        }
    }

    /**
     * Resets the page data to start from the bottom again.
     */
    private void resetPageData() {
        page = 1;
        bottomInt = 0;
        filters.remove("startIndex");
    }

    /**
     * Hides the my patients check box if the operator is an admin.
     * Ie not a clinician.
     */
    private void setViewPatientsCheckBox() {
        if (!LoginSceneController.isActiveClinician()) {
            showMyPatientsCheckBox.setVisible(false);
        }
    }

    /**
     * Hides the organisation check box if the operator is an admin.
     * Ie not a clinician.
     */
    private void setOrganisationsPatientsCheckBox() {
        if (!LoginSceneController.isActiveClinician()) {
            showMyOrganisationCheckBox.setVisible(false);
        }
    }

    @FXML
    private void pageLeft() {
        switchPage(false);
    }

    @FXML
    private void pageRight() {
        switchPage(true);
    }

    /**
     * Sets the total number of Profiles in the DB.
     *
     * @param maxSize The new max size to be set.
     */
    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
        setPageButtons();
    }

    /**
     * Create a reference to the main controller for the clinician related scenes/FXMLs.
     *
     * @param parentController The main clinician controller.
     */
    public void setParentController(ClinicianSceneController parentController) {
        this.parentController = parentController;
    }
}
