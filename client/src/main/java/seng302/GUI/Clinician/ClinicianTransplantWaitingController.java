package seng302.GUI.Clinician;

import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import seng302.CustomException.DoesNotExist;
import seng302.Enum.OrganEnum;
import seng302.Enum.RegionEnum;
import seng302.GUI.ControllerAccess;
import seng302.GUI.MainController;
import seng302.GUI.Procedures.ProcedureRemovalPopUp;
import seng302.GUI.Profile.OpenProfilePage;
import seng302.Model.Profile;
import seng302.Model.ReceiverOrgan;
import seng302.ServerInteracton.ServerQueries.Authentication.AuthToken;
import seng302.ServerInteracton.ServerQueries.ProfileRequests;
import seng302.Utilities.ProfileDisplayUtility;
import seng302.Utilities.Undo_Redo.Command;
import seng302.Utilities.Undo_Redo.UndoRedoController;
import seng302.Utilities.Undo_Redo.UndoableEnumChoiceBox;
import seng302.Utilities.Undo_Redo.UndoableListOfUndoableObjects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The controller for the FXML that will be included under the Transplant waiting list tab in the clinician.fxml.
 * It displays the records for the records.
 */
public class ClinicianTransplantWaitingController {

    private static final int ZERO = 0;
    @FXML
    private AnchorPane clinicianTransplantWaitingPane;
    @FXML
    private Button clearFilterButton;
    @FXML
    private TableColumn<ClinicianTransplantWaitingWrapper, String> clinicianTransplantWaitingOrganRegisteredDate,
            clinicianTransplantWaitingFirstName, clinicianTransplantWaitingMiddleName,
            clinicianTransplantWaitingLastName, clinicianTransplantWaitingOrgan, clinicianTransplantWaitingRegion;
    @FXML
    private TableView<ClinicianTransplantWaitingWrapper> clinicianTransplantWaitingList;
    @FXML
    private ChoiceBox<Enum> clinicianOrganComboBox, clinicianRegionComboBox;
    private ArrayList<Profile> profileList = null;
    private ObservableList<ClinicianTransplantWaitingWrapper> toDisplay = FXCollections.observableArrayList();
    private List<ClinicianTransplantWaitingWrapper> transplantWaitingListRecords = new ArrayList<>();
    private String userName;
    private String organSearchName, regionSearchName;
    private MainController parentController;

    /**
     * Controls all undo/redo functionality for the class.
     */
    @FXML
    private UndoRedoController undoRedoController;
    private boolean currentlyUndoing;

    /**
     * Prepare initial data set
     */
    @FXML
    private void initialize() {
        ControllerAccess.setClinicianTransplantWaitingController(this);
        setUpMouseEventsForRows();
        addRecordsForDisplay(null, null);
        display();
        setupChoiceBoxValues();
        initializeListeners();
    }

    /**
     * Sets the currently Undoing variables for the class.
     *
     * @param currentlyUndoing The value that currentlyUndoing will be set to.
     */
    public void setCurrentlyUndoing(boolean currentlyUndoing) {
        this.currentlyUndoing = currentlyUndoing;
    }

    /**
     * Sets the undoRedoController for the class.
     *
     * @param undoRedoController The UndoRedoController to be set.
     */
    public void setUndoRedoController(UndoRedoController undoRedoController) {
        this.undoRedoController = undoRedoController;
    }

    /**
     * Menu option to remove a transplant from the list.
     * Opens up a new scene as the popup
     *
     * @param transplantPatient The person / info that has been clicked on
     */
    private void removeTransplant(ClinicianTransplantWaitingWrapper transplantPatient) {
        ProcedureRemovalPopUp procedureRemovalPopUp = new ProcedureRemovalPopUp(transplantPatient, parentController.getHospitals());
        procedureRemovalPopUp.showAndWait();
        // If the window wasn't closed manually.
        if (procedureRemovalPopUp.isDiseaseCured() ||
                procedureRemovalPopUp.isPatientDead() ||
                procedureRemovalPopUp.isTransplantReceived() ||
                procedureRemovalPopUp.isRegistrationError()) {

            try {
                Profile currentUser = ProfileRequests.getInstance().getProfile(transplantPatient.getUserName(), AuthToken.getInstance().getToken()).getBody();
                if (procedureRemovalPopUp.isPatientDead()) {
                    assert currentUser != null;
                    currentUser.setDeath(procedureRemovalPopUp.getDeath());
                    currentUser.clearReceiverOrgans();
                } else {
                    assert currentUser != null;

                    if (procedureRemovalPopUp.isDiseaseCured()) {
                        currentUser.cureCurrentDisease(procedureRemovalPopUp.getCuredDisease());
                    }

                    currentUser.removeReceiverOrgan(transplantPatient.getReceiveOrgan().getRegisteredOrgan(),
                            procedureRemovalPopUp.getSelectedMessage());
                }
                ProfileRequests.getInstance().putProfile(currentUser, AuthToken.getInstance().getToken());

            } catch (HttpClientErrorException e) {
                // Check the status code of the request and handle accordingly
                switch (e.getStatusCode()) {
                    case UNAUTHORIZED:
                        parentController.kickUser();
                        break;
                    default:
                        parentController.handleCriticalError(e);
                }
            } catch (HttpServerErrorException | ResourceAccessException | DoesNotExist e) {
                parentController.handleCriticalError(e);
            }
            addRecordsForDisplay(null, null);
            display();
        }
    }

    /**
     * Add event listener to the two ComboBox: Filter by Organ and Filter by Region. Get the selected value to filter
     */
    private void initializeListeners() {
        clinicianOrganComboBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (((oldValue == null) || (!oldValue.equals(newValue))) && !currentlyUndoing) {
                addEnumChoiceBoxToDeque(clinicianOrganComboBox, oldValue);
            }
            updateSearchOrgan(newValue);
        });

        clinicianRegionComboBox.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (((oldValue == null) || (!oldValue.equals(newValue))) && !currentlyUndoing) {
                addEnumChoiceBoxToDeque(clinicianRegionComboBox, oldValue);
            }
            updateSearchRegion(newValue);
        });

        clearFilterButton.setOnAction(event -> {
            ArrayList<Command> undoableFilters = new ArrayList<>();
            boolean isValidClear = false;

            // Choice boxes
            ArrayList<ChoiceBox<Enum>> choiceBoxFilters = new ArrayList<>();
            choiceBoxFilters.add(clinicianOrganComboBox);
            choiceBoxFilters.add(clinicianRegionComboBox);

            for (ChoiceBox<Enum> choiceBox : choiceBoxFilters) {
                Enum choiceBoxValue = choiceBox.getValue();
                if (choiceBoxValue != null) {
                    undoableFilters.add(new UndoableEnumChoiceBox(choiceBox, choiceBoxValue));
                    isValidClear = true;
                }
            }

            // Adds clear event to the undo/redo Deque only if it actually cleared something
            if (isValidClear) {
                undoRedoController.pushToUndoDeque(new UndoableListOfUndoableObjects(undoableFilters));

                // Clearing the selection triggers the change listeners so we disable pushing to the undoRedoDeque
                // while clearing to stop things being pushed twice onto the undoRedoController
                currentlyUndoing = true;

                clinicianOrganComboBox.getSelectionModel().clearSelection();
                clinicianRegionComboBox.getSelectionModel().clearSelection();
                toDisplay.clear();
                toDisplay.addAll(transplantWaitingListRecords);

                currentlyUndoing = false;

                updateUndoRedoButtons();

            }
        });
    }

    /**
     * Accept the value from the event listener. Convert it to a string value if the accepted value is not null, else
     * pass the null value.
     *
     * @param organSearchValue The accepted organ value from the event listener.
     */
    private void updateSearchOrgan(Object organSearchValue) {
        if (organSearchValue != null) {
            organSearchName = organSearchValue.toString().toLowerCase();
        } else {
            organSearchName = null;
        }
        toFilter();
    }

    /**
     * Accept the value from the event listener. Convert it to a string value if the accepted value is not null, else
     * pass the null value.
     *
     * @param regionSearchValue The accepted region value from the event listener.
     */
    private void updateSearchRegion(Object regionSearchValue) {
        if (regionSearchValue != null) {
            regionSearchName = regionSearchValue.toString().toLowerCase();
        } else {
            regionSearchName = null;
        }
        toFilter();
    }

    /**
     * Perform filter action based on user's selection from the two combo box from the page
     */
    private void toFilter() {
        addRecordsForDisplay(organSearchName, regionSearchName);

        toDisplay.clear();
        List<ClinicianTransplantWaitingWrapper> wrappers = new ArrayList<>();

        filterBasedOnCriteria(wrappers);
        displayAfterFilter(wrappers);
        if (organSearchName == null && regionSearchName == null) {
            this.refreshTable();
        }

    }

    /**
     * Pass in a list of clinicianTransplantWaitingWrapper, based on different criteria to find related records for display
     *
     * @param wrappers A list of clinicianTransplantWaitingWrapper records
     */
    private void filterBasedOnCriteria(List<ClinicianTransplantWaitingWrapper> wrappers) {
        for (ClinicianTransplantWaitingWrapper wrapper : transplantWaitingListRecords) {
            if ((regionSearchName != null || organSearchName != null) &&
                    wrapper.getReceiveOrganString().equalsIgnoreCase(organSearchName) && wrapper.getRegion() != null &&
                    wrapper.getRegion().equalsIgnoreCase(regionSearchName)) {
                wrappers.add(wrapper);
            }
            if (regionSearchName != null && organSearchName == null && wrapper.getRegion() != null &&
                    wrapper.getRegion().equalsIgnoreCase(regionSearchName)) {
                wrappers.add(wrapper);
            }
            if (organSearchName != null && regionSearchName == null &&
                    wrapper.getReceiveOrganString().equalsIgnoreCase(organSearchName)) {
                wrappers.add(wrapper);
            }
        }
    }

    /**
     * Display the records after select value on the combo box. If no matching records found, display
     * the message to users.
     *
     * @param wrappers A list of clinicianTransplantWaitingWrapper records
     */
    private void displayAfterFilter(List<ClinicianTransplantWaitingWrapper> wrappers) {
        if (wrappers.size() > 0) {
            toDisplay.addAll(wrappers);
        } else {
            clinicianTransplantWaitingList.setPlaceholder(new Label("No matching records found."));
        }
    }

    /**
     * Set up values for display in combo box fields: OrganComboBox and RegionComboBox
     */
    private void setupChoiceBoxValues() {
        ObservableList<Enum> organEnumOptions = FXCollections.observableArrayList();
        organEnumOptions.addAll(OrganEnum.values());
        clinicianOrganComboBox.setItems(organEnumOptions);
        ObservableList<Enum> regionEnumsEnumOptions = FXCollections.observableArrayList();
        regionEnumsEnumOptions.addAll(RegionEnum.values());
        regionEnumsEnumOptions.remove(RegionEnum.NOT_SET);
        clinicianRegionComboBox.setItems(regionEnumsEnumOptions);
    }

    /**
     * If the TableView already has all records displayed, clear out all existing records and reload.
     * Otherwise, filter out only data needed for displaying. Since we need per organ per display in a TableView. In order to achieve
     * this, we need to bind a new data type for the displaying.
     * Note: TableView binds one object of a class per row. In a case to having a list of records in one object, for example,
     * a receiver profile object may have several organs registered. Then we create several ClinicianTransplantWaitingWrapper
     * object per registered organ.
     */
    private void addRecordsForDisplay(String organToSearch, String regionToSearch) {
        String region = null;
        int receiveCount = ZERO;

        try {
            profileList = ProfileRequests.getInstance().getTransplantProfiles(AuthToken.getInstance().getToken(), organToSearch, regionToSearch).getBody();
        } catch (HttpClientErrorException e) {
            // Check the status code of the request and handle accordingly
            switch (e.getStatusCode()) {
                case UNAUTHORIZED:
                    parentController.kickUser();
                    break;
                default:
                    parentController.handleCriticalError(e);
            }
        } catch (HttpServerErrorException | ResourceAccessException e) {
            parentController.handleCriticalError(e);
        }

        toDisplay.removeAll(transplantWaitingListRecords);
        transplantWaitingListRecords.clear();
        for (Profile everyProfile : profileList) {
            if (everyProfile.getRegion() != null) {
                region = everyProfile.getRegion().toString();
            }
            if (everyProfile.getReceiverOrgans().size() > ZERO) {
                receiveCount += everyProfile.getReceiverOrgans().size();
                for (ReceiverOrgan receiverOrgan : everyProfile.getReceiverOrgansWithTimes()) {
                    ClinicianTransplantWaitingWrapper wrapperRecord = new ClinicianTransplantWaitingWrapper(
                            receiverOrgan.getRegisteredOrgan(),
                            receiverOrgan.getDateRegistered().toString(),
                            everyProfile.getFirstName(), everyProfile.getMiddleName(),
                            everyProfile.getLastName(), region, everyProfile.getUsername(), everyProfile.hasOrganClash());
                    if (transplantWaitingListRecords.size() < receiveCount) {
                        transplantWaitingListRecords.add(wrapperRecord);
                    }
                }
            }
        }
        //Sort the array list in descending order. So the latest registered organ always at the top
        transplantWaitingListRecords.sort(Collections.reverseOrder());
        toDisplay.addAll(transplantWaitingListRecords);
        clinicianTransplantWaitingList.setItems(toDisplay);
    }

    /**
     * Display records to the table
     */
    private void display() {
        clinicianTransplantWaitingOrgan.setCellValueFactory(
                param -> new SimpleStringProperty(param.getValue().getReceiveOrganString()));
        clinicianTransplantWaitingOrganRegisteredDate.setCellValueFactory(new PropertyValueFactory<>("registerDate"));
        clinicianTransplantWaitingFirstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        clinicianTransplantWaitingMiddleName.setCellValueFactory(new PropertyValueFactory<>("middleName"));
        clinicianTransplantWaitingLastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        clinicianTransplantWaitingRegion.setCellValueFactory(new PropertyValueFactory<>("region"));
    }

    /**
     * Set up the mouse events for the table when a row is clicked.
     * Mouse event for when a double left click happens on a row  (the clicked profile is opened in a new tab).
     * Mouse event for when a right click happens on a row  (a context menu appears for removing the given transplant).
     */
    private void setUpMouseEventsForRows() {
        clinicianTransplantWaitingList.setRowFactory(tableView -> {
            // Initialise the context menu for removing a transplant
            ContextMenu contextMenu = new ContextMenu();
            MenuItem removeMenuItem = new MenuItem("Remove");
            contextMenu.getItems().add(removeMenuItem);
            TableRow<ClinicianTransplantWaitingWrapper> row = new TableRow<>();

            row.setOnMouseClicked(event -> {
                // Check if this was a right click on the row
                if (event.getButton() == MouseButton.SECONDARY) {
                    removeMenuItem.setOnAction(event2 -> removeTransplant(row.getItem()));
                    row.setContextMenu(contextMenu);
                }

                // Check if this was a double left click
                else if (event.getClickCount() == ProfileDisplayUtility.DOUBLE_CLICK_COUNT && (!row.isEmpty()) && event.getButton() == MouseButton.PRIMARY) {
                    ClinicianTransplantWaitingWrapper wrapper = row.getItem();
                    userName = wrapper.getUserName();
                    // Instantiate a profile stage, then open it by double clicking
                    OpenProfilePage openProfilePage = new OpenProfilePage();
                    openProfilePage.openProfileFromDoubleClick(userName);
                }
            });
            return row;
        });
    }

    /**
     * Refresh the clinicianSearchTable to show the most current information of the profiles in the data FXCollections
     * observable array list.
     */
    private void refreshTable() {
        addRecordsForDisplay(null, null);
        display();
        clinicianTransplantWaitingList.refresh();
    }

    /**
     * Function creates an UndoableEnumChoiceBox object and pushes to the undo deque.
     *
     * @param choiceBox ChoiceBox: that we want to restore.
     * @param oldValue  Enum: the oldValue to restore to.
     */
    private void addEnumChoiceBoxToDeque(ChoiceBox<Enum> choiceBox, Enum oldValue) {
        undoRedoController.pushToUndoDeque(new UndoableEnumChoiceBox(choiceBox, oldValue));
        updateUndoRedoButtons();
    }

    /**
     * Ensures that the undo/redo buttons are only enabled if there are available undo/redo.
     */
    private void updateUndoRedoButtons() {
        if (ControllerAccess.getClinicianSceneController() != null) {
            ControllerAccess.getClinicianSceneController().updateUndoRedoButtons();
        }
        if (ControllerAccess.getAdminSceneController() != null) {
            ControllerAccess.getAdminSceneController().updateUndoRedoButtons();
        }
    }

    /**
     * Create a reference to the main controller for the clinician related scenes/FXMLs.
     *
     * @param parentController The main clinician controller.
     */
    public void setParentController(MainController parentController) {
        this.parentController = parentController;
    }
}


