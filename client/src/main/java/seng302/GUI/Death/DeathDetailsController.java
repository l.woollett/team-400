package seng302.GUI.Death;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import static seng302.Utilities.Tooltips.INVALID_DATE_OF_DEATH_TOOLTIP_TEXT;
import static seng302.Utilities.Tooltips.createNewTooltip;

public class DeathDetailsController {
    @FXML
    private AnchorPane createDeathPane;

    @FXML
    private Button clearDateOfDeathButton;

    @FXML
    private Button createDeathUndo;

    @FXML
    private Button cancelButton;

    @FXML
    private TextArea reasonOfDeathTextArea;

    @FXML
    private DatePicker dateOfDeathDatePicker;

    @FXML
    private VBox dateOfDeathPicker;

    @FXML
    private Button createDeathButton;

    @FXML
    private Button createDeathRedo;

    @FXML
    private TextField hospitalTextField;

    @FXML
    void undoEventCreateDeath(ActionEvent event) {

    }

    @FXML
    void redoEventCreateDeath(ActionEvent event) {

    }

    /**
     * Clears the date of death date picker in case you accidentally clicked on it and were wanting to clear.
     */
    @FXML
    private void clearDateOfDeath() {
        dateOfDeathDatePicker.setValue(null);
        dateOfDeathDatePicker.setTooltip(createNewTooltip(INVALID_DATE_OF_DEATH_TOOLTIP_TEXT));
    }

    @FXML
    void createDeathButtonClicked(ActionEvent event) {

    }

    /**
     * Exit the create user stage because they want to stop creating a user profile.
     */
    @FXML
    private void cancelButtonClicked() {
        close();
    }

    /**
     * Method called when the create user window is closed.
     */
    private void close() {
        Stage createDeathStage = (Stage) createDeathPane.getScene().getWindow();
        createDeathStage.close();
    }

}

