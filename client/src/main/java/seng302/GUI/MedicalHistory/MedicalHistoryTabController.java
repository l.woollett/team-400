package seng302.GUI.MedicalHistory;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import seng302.GUI.ControllerAccess;
import seng302.GUI.Hideable;
import seng302.GUI.Notifications.AlertDialog;
import seng302.GUI.Profile.ProfileSceneController;
import seng302.Model.Disease;
import seng302.Model.Profile;
import seng302.Utilities.AscendingDiseaseDateComparator;
import seng302.Utilities.AscendingDiseaseNameComparator;
import seng302.Utilities.DescendingDiseaseDateComparator;
import seng302.Utilities.DescendingDiseaseNameComparator;
import seng302.Utilities.Undo_Redo.UndoRedoController;
import seng302.Utilities.Undo_Redo.UndoableCheckBox;
import seng302.Utilities.Undo_Redo.UndoableDatePicker;
import seng302.Utilities.Undo_Redo.UndoableLists.UndoableDiseaseDualList;
import seng302.Utilities.Undo_Redo.UndoableTextField;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.logging.Logger;

import static seng302.Utilities.Style.applyDatePickerStyle;
import static seng302.Utilities.Style.applyTextFieldStyle;
import static seng302.Utilities.Tooltips.*;
import static seng302.Utilities.Validator.isValidDiseaseDiagnosisDate;
import static seng302.Utilities.Validator.isValidDiseaseName;

public class MedicalHistoryTabController implements Hideable {

    private final Logger LOGGER = Logger.getLogger(getClass().getName());
    @FXML
    private AnchorPane mainDiseaseAnchorPane;
    @FXML
    private Button addNewDiseaseButton, moveDiseaseToCurrentDiseasesButton, moveDiseaseToPastDiseasesButton;
    @FXML
    private TextField diseaseNameTextField;
    @FXML
    private CheckBox isChronicDiseaseCheckBox;
    @FXML
    private DatePicker diseaseDatePicker;
    @FXML
    private Label diseaseNameLabel, diseaseDiagnosisDateLabel;
    @FXML
    private TableView<Disease> currentDiseaseTableView, pastDiseaseTableView;
    @FXML
    private TableColumn<Disease, String> currentDiseaseTypeColumn, currentDiseaseNameColumn, currentDiseaseDateColumn,
            pastDiseaseNameColumn, pastDiseaseDateColumn, pastDiseaseTypeColumn;
    private ProfileSceneController profileSceneController;
    private ObservableList<Disease> currentDiseases, pastDiseases, currentChronicDiseases, currentNonChronicDiseases = FXCollections.observableArrayList(new ArrayList<>());
    /**
     * Controls all undo/redo functionality for the class.
     */
    @FXML
    private UndoRedoController undoRedoController;
    private boolean currentlyUndoing;

    /**
     * Initialises the fields and tables of the scene. The fields will have default values put into them. The tables
     * will have event listeners bound to them.
     */
    @FXML
    private void initialize() {
        ControllerAccess.setMedicalHistoryTabController(this);

        currentDiseaseTypeColumn.setSortable(false);

        // --------------------------------- Current DISEASES ----------------------------------------------------------

        currentDiseaseDateColumn.prefWidthProperty().bind(currentDiseaseTableView.widthProperty().divide(3));
        currentDiseaseTypeColumn.prefWidthProperty().bind(currentDiseaseTableView.widthProperty().divide(3));
        currentDiseaseNameColumn.prefWidthProperty().bind(currentDiseaseTableView.widthProperty().divide(3));

        currentDiseaseTypeColumn.setCellValueFactory(
                param ->
                        new ReadOnlyObjectWrapper<>(param.getValue().getIsChronicDiseaseString()));

        currentDiseaseNameColumn.setCellValueFactory(
                param ->
                        new ReadOnlyObjectWrapper<>(param.getValue().getDiseaseName()));

        currentDiseaseDateColumn.setCellValueFactory(
                param ->
                        new ReadOnlyObjectWrapper<>(param.getValue().getDateOfDiagnosisString()));

        // --------------------------------- PAST DISEASES ----------------------------------------------------------

        pastDiseaseDateColumn.prefWidthProperty().bind(pastDiseaseTableView.widthProperty().divide(2));
        pastDiseaseNameColumn.prefWidthProperty().bind(pastDiseaseTableView.widthProperty().divide(2));

        pastDiseaseNameColumn.setCellValueFactory(
                param ->
                        new ReadOnlyObjectWrapper<>(param.getValue().getDiseaseName()));

        pastDiseaseDateColumn.setCellValueFactory(
                param ->
                        new ReadOnlyObjectWrapper<>(param.getValue().getDateOfDiagnosisString()));

        currentDiseaseTableView.setOnSort(sortEvent -> {
            sortEvent.consume();
            while (currentDiseaseTableView.getSortOrder().size() > 1) {
                currentDiseaseTableView.getSortOrder().remove(1);
            }
            updateTables();
        });

        pastDiseaseTableView.setOnSort(sortEvent -> {
            while (pastDiseaseTableView.getSortOrder().size() > 1) {
                pastDiseaseTableView.getSortOrder().remove(1);
            }
            updateTables();
        });

        isChronicDiseaseCheckBox.selectedProperty().addListener(((observable, oldValue, newValue) -> {
            if (!currentlyUndoing) {
                pushCheckBoxChangeToUndoDeque(isChronicDiseaseCheckBox);
            }
        }));
    }

    /**
     * Sets the currently Undoing variables for the class.
     *
     * @param currentlyUndoing The value that currentlyUndoing will be set to.
     */
    public void setCurrentlyUndoing(boolean currentlyUndoing) {
        this.currentlyUndoing = currentlyUndoing;
    }

    /**
     * Sets the undoRedoController for the class.
     *
     * @param undoRedoController The UndoRedoController to be set.
     */
    public void setUndoRedoController(UndoRedoController undoRedoController) {
        this.undoRedoController = undoRedoController;
    }

    /**
     * Adds the given disease to the list of current diseases and calls the tables to be updated in the GUI to reflect
     * this.
     * <p>
     * Call this function only if you are updating the disease. Use addDiseaseToCurrentDiseases() for a new disease
     *
     * @param disease The Disease to be added to the current disease list.
     */
    public void addCurrentDisease(Disease disease) {
        currentDiseases.add(disease);
        updateTables();
    }

    /**
     * Enables all of the clinician functionality by making clinician specific components visible and setting up styling
     * and listeners to these components.
     */
    public void enableClinicianFunctions() {
        // Initialise button states
        diseaseDatePicker.setValue(LocalDate.now());
        moveDiseaseToCurrentDiseasesButton.setDisable(true);
        moveDiseaseToPastDiseasesButton.setDisable(true);

        // Initialise the delete and update disease functionality
        currentDiseaseTableView.setRowFactory(tableView -> {
            TableRow<Disease> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getButton() == MouseButton.SECONDARY) {
                    initContextMenu(row);
                }
            });
            return row;
        });
        pastDiseaseTableView.setRowFactory(tableView -> {
            TableRow<Disease> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getButton() == MouseButton.SECONDARY) {
                    initContextMenu(row);
                }
            });
            return row;
        });

        // Initialise default styles
        applyTextFieldStyle(diseaseNameTextField, false, false, VALID_DISEASE_NAME_TOOLTIP_TEXT,
                INVALID_DISEASE_NAME_TOOLTIP_TEXT, diseaseNameTextField.getText());
        applyDatePickerStyle(diseaseDatePicker, true, false, VALID_DISEASE_DIAGNOSIS_DATE_TOOLTIP_TEXT,
                INVALID_DISEASE_DIAGNOSIS_DATE_TOOLTIP_TEXT, diseaseDatePicker.getValue());

        // Add listeners to signal user of input validity
        diseaseNameTextField.setTooltip(createNewTooltip(INVALID_DISEASE_NAME_TOOLTIP_TEXT));
        diseaseNameTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    Boolean acceptableDiseaseName = isValidDiseaseName(newValue);
                    if (!oldValue.equals(newValue) && !currentlyUndoing) {
                        pushTextFieldChangeToUndoDeque(diseaseNameTextField, oldValue);
                    }
                    applyTextFieldStyle(diseaseNameTextField, acceptableDiseaseName, false,
                            VALID_DISEASE_NAME_TOOLTIP_TEXT,
                            INVALID_DISEASE_NAME_TOOLTIP_TEXT, newValue);
                }));

        diseaseDatePicker.setTooltip(createNewTooltip(INVALID_DISEASE_DIAGNOSIS_DATE_TOOLTIP_TEXT));
        diseaseDatePicker.valueProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    if (((oldValue == null) || (!oldValue.equals(newValue))) && !currentlyUndoing) {
                        pushDatePickerChangeToUndoDeque(diseaseDatePicker, oldValue);
                    }
                    LocalDate dob = profileSceneController.getCurrentProfile().getDateOfBirth();
                    Boolean acceptableDiseaseDiagnosisDate = isValidDiseaseDiagnosisDate(newValue, dob);
                    applyDatePickerStyle(diseaseDatePicker, acceptableDiseaseDiagnosisDate, false,
                            VALID_DISEASE_DIAGNOSIS_DATE_TOOLTIP_TEXT,
                            INVALID_DISEASE_DIAGNOSIS_DATE_TOOLTIP_TEXT, newValue);
                }));
    }

    /**
     * Push changes to the undo-redo deque and update the undo redo buttons to either enable or disable based on whether
     * the stacks are empty or not. An empty stack would mean it is disabled and a non-empty one would mean the button
     * would be enabled.
     *
     * @param textField the textfield which the changes have come from.
     * @param oldValue  the old value that we are wanting to restore when an undo or redo event will occur.
     */
    private void pushTextFieldChangeToUndoDeque(TextField textField, String oldValue) {
        undoRedoController.pushToUndoDeque(new UndoableTextField(textField, oldValue));
        updateUndoRedoButtons();
    }

    /**
     * Push changes to the undo-redo deque and update the undo redo buttons to either enable or disable based on whether
     * the stacks are empty or not. An empty stack would mean it is disabled and a non-empty one would mean the button
     * would be enabled.
     *
     * @param datePicker the date picker which the changes have come from.
     * @param oldValue   the old value what we are wanting to restore when an undo or redo event will occur.
     */
    private void pushDatePickerChangeToUndoDeque(DatePicker datePicker, LocalDate oldValue) {
        undoRedoController.pushToUndoDeque(new UndoableDatePicker(datePicker, oldValue));
        updateUndoRedoButtons();
    }

    /**
     * Push changes to the undo-redo deque and update the undo redo buttons to either enable or disable based on whether
     * the stacks are empty or not. An empty stack would mean it is disabled and a non-empty one would mean the button
     * would be enabled.
     *
     * @param checkBox the checkbox which the changes have come from.
     */
    private void pushCheckBoxChangeToUndoDeque(CheckBox checkBox) {
        undoRedoController.pushToUndoDeque(new UndoableCheckBox(checkBox));
        updateUndoRedoButtons();
    }

    /**
     * Pushes two old lists to the undo redo stack so that way we can restore them when undo or redo is clicked.
     *
     * @param oldList1 the first list we want to restore (use currentDiseases).
     * @param oldList2 the second list we want to restore (use pastDiseases).
     */
    public void pushUndoableDiseaseList(ObservableList<Disease> oldList1, ObservableList<Disease> oldList2) {
        undoRedoController.pushToUndoDeque(new UndoableDiseaseDualList(oldList1, oldList2, this));
        updateUndoRedoButtons();
    }

    /**
     * Ensures that the undo/redo buttons are only enabled if there is an available undo/redo.
     */
    private void updateUndoRedoButtons() {
        profileSceneController.updateUndoRedoButtons();
    }

    /**
     * Creates a new disease from the current inputs. Also performs error checking to ensure disease is in the correct
     * format.
     */
    @FXML
    private void addDiseaseToCurrentDiseases() {
        // Retrieve disease values from GUI elements
        String diseaseName = diseaseNameTextField.getText().trim();
        Boolean isChronicDisease = isChronicDiseaseCheckBox.isSelected();
        LocalDate dateOfDiagnosis = diseaseDatePicker.getValue();

        // Retrieve currently logged in users dob
        Profile profile = profileSceneController.getCurrentProfile();
        LocalDate dob = profile.getDateOfBirth();

        // Don't add new disease if it is invalid
        if (!isValidDiseaseDiagnosisDate(dateOfDiagnosis, dob) || !isValidDiseaseName(diseaseName)) {
            AlertDialog.showAndWait("Error",
                    "You must complete the required fields marked in red to create a new disease.");
            return;
        }

        // Add disease if it is valid
        clearAddNewDiseaseFields();
        pushUndoableDiseaseList(currentDiseases, pastDiseases);
        Disease newDisease = new Disease(diseaseName, isChronicDisease, false, dateOfDiagnosis);
        currentDiseases.add(newDisease);

        // Ensure tables resemble changes by updating them
        updateTables();
    }

    /**
     * Moves the disease from the current list of diseases to the past list of diseases. The disease is then set as
     * cured, unless it is chronic. In such case the user will be alerted saying there is an error and they must change
     * the disease from chronic before it can be cured.
     */
    @FXML
    private void moveDiseaseFromCurrentToPastDiseases() {
        Disease diseaseToMove;

        diseaseToMove = currentDiseases.get(currentDiseaseTableView.getSelectionModel().getFocusedIndex());

        if (diseaseToMove.getIsChronicDisease()) {
            AlertDialog.showAndWait("Error. Cannot move disease!",
                    "Disease must not be chronic if it is to be moved to the past diseases.");
        } else {
            pushUndoableDiseaseList(currentDiseases, pastDiseases);
            diseaseToMove.setDiseaseCured(true);
            pastDiseases.add(diseaseToMove);
            currentDiseases.remove(currentDiseaseTableView.getSelectionModel().getFocusedIndex());
            pastDiseaseTableView.setItems(pastDiseases);

        }
        updateTables();
    }

    /**
     * Moves the currently selected disease from the past list of diseases to the list of current diseases.
     */
    @FXML
    private void moveDiseaseFromPastToCurrentDiseases() {
        Disease diseaseToMove;

        diseaseToMove = pastDiseases.get(pastDiseaseTableView.getSelectionModel().getFocusedIndex());
        pushUndoableDiseaseList(pastDiseases, currentDiseases);
        diseaseToMove.setDiseaseCured(false);
        currentDiseases.add(diseaseToMove);
        pastDiseases.remove(pastDiseaseTableView.getSelectionModel().getFocusedIndex());
        currentDiseaseTableView.setItems(currentDiseases);

        updateTables();
    }

    /**
     * Opens up the updateDisease window with auto-populated fields containing the current data of the disease to be
     * updated. This window must be closed before the parent window can be used again due to the modality settings.
     *
     * @param diseaseToUpdate The disease that is to be updated.
     */
    @FXML
    private void openUpdatePopUp(Disease diseaseToUpdate) {
        // Get stage and scene set-up
        FXMLLoader loader = new FXMLLoader(getClass().getResource("updateDiseaseDialog.fxml"));

        Stage updateDiseaseStage = new Stage();

        try {
            updateDiseaseStage.setScene(new Scene(loader.load()));
        } catch (IOException e) {
            LOGGER.severe(e.getMessage());
        }

        // Get the controller of the scene
        UpdateDiseaseDialogController controller = loader.getController();

        // Figure out what table and table data we are working with
        TableView table = currentDiseaseTableView;
        ObservableList<Disease> list = currentDiseases;
        if (pastDiseaseTableView.isFocused()) {
            table = pastDiseaseTableView;
            list = pastDiseases;
        }

        // Let the controller know the data we are working with
        controller.initDialog(diseaseToUpdate, table, list);

        // Set-up and show the update stage
        Stage mainStage = (Stage) mainDiseaseAnchorPane.getScene().getWindow();
        updateDiseaseStage.initOwner(mainStage);
        updateDiseaseStage.initModality(Modality.WINDOW_MODAL);
        updateDiseaseStage.setResizable(false);
        updateDiseaseStage.showAndWait();

        updateTables();
    }

    /**
     * Refreshes the tables and makes sure that if either table is empty, the appropriate buttons are disabled to match
     * this. Also updates the Profiles arrayList of current and past diseases to reflect the changes in the current
     * scene
     */
    @FXML
    public void updateTables() {

        if (currentDiseases.isEmpty()) {
            moveDiseaseToPastDiseasesButton.setDisable(true);
        } else {
            moveDiseaseToPastDiseasesButton.setDisable(false);
        }

        if (pastDiseases.isEmpty()) {
            moveDiseaseToCurrentDiseasesButton.setDisable(true);
        } else {
            moveDiseaseToCurrentDiseasesButton.setDisable(false);
        }

        if (getCurrentDiseaseSortColumn().equals(currentDiseaseNameColumn)) {
            sortCurrentDiseasesByName(getCurrentDiseaseSortType());
        } else {
            sortCurrentDiseasesByDate(getCurrentDiseaseSortType());
        }

        if (getPastDiseaseSortColumn().equals(pastDiseaseNameColumn)) {
            sortPastDiseasesByName(getPastDiseaseSortType());
        } else {
            sortPastDiseasesByDate(getPastDiseaseSortType());
        }

        profileSceneController.getCurrentProfile().setPastDiseases(new ArrayList<>(pastDiseases));
        profileSceneController.getCurrentProfile().setCurrentDiseases(new ArrayList<>(currentDiseases));
        profileSceneController.updateProfileHistoryOfChanges();

        currentDiseaseTableView.setItems(currentDiseases);
        pastDiseaseTableView.setItems(pastDiseases);

        currentDiseaseTableView.refresh();
        pastDiseaseTableView.refresh();
    }

    /**
     * Initialises the menu that appears when a right click event occurs with delete and update options. Sets up the
     * action listeners for each menu item as well.
     *
     * @param row The row of the disease to be deleted or updated.
     */
    private void initContextMenu(TableRow<Disease> row) {
        ContextMenu contextMenu = new ContextMenu();
        MenuItem updateMenuItem = new MenuItem("Update");
        MenuItem deleteMenuItem = new MenuItem("Delete");

        contextMenu.getItems().addAll(updateMenuItem, deleteMenuItem);
        row.setContextMenu(contextMenu);

        updateMenuItem.setOnAction(event -> openUpdatePopUp(row.getItem()));
        deleteMenuItem.setOnAction(event -> deleteSelectedDisease());
    }

    /**
     * Deletes the currently selected disease depending on what table is currently focused. If th current disease table
     * is focused, the selected current disease is deleted, and vice versa for the past disease table.
     */
    private void deleteSelectedDisease() {
        if (currentDiseaseTableView.isFocused()) {
            Disease diseaseToDelete =
                    currentDiseases.get(currentDiseaseTableView.getSelectionModel().getFocusedIndex());
            pushUndoableDiseaseList(currentDiseases, pastDiseases);
            currentDiseases.remove(diseaseToDelete);
        } else if (pastDiseaseTableView.isFocused()) {
            Disease diseaseToDelete = pastDiseases.get(pastDiseaseTableView.getSelectionModel().getFocusedIndex());
            pushUndoableDiseaseList(pastDiseases, currentDiseases);
            pastDiseases.remove(diseaseToDelete);
        }
        updateTables();
    }

    /**
     * Populates the current and past disease tables to be sorted in descending order by date. The current diseases
     * table will have chronic diseases at the top, followed up non-chronic.
     */
    @FXML
    public void populateTables() {
        pastDiseases = FXCollections.observableArrayList(profileSceneController.getCurrentProfile().getPastDiseases());
        currentDiseases = FXCollections.observableArrayList(profileSceneController.getCurrentProfile()
                .getCurrentDiseases());

        splitCurrentDiseases();

        currentChronicDiseases.sort(new DescendingDiseaseDateComparator());
        currentNonChronicDiseases.sort(new DescendingDiseaseDateComparator());
        pastDiseases.sort(new DescendingDiseaseDateComparator());

        currentDiseases = FXCollections.observableArrayList(new ArrayList<>());
        currentDiseases.addAll(currentChronicDiseases);
        currentDiseases.addAll(currentNonChronicDiseases);

        currentDiseaseTableView.setItems(currentDiseases);
        pastDiseaseTableView.setItems(pastDiseases);

        updateTables();
    }

    /**
     * Gets the current diseases.
     *
     * @return The current diseases.
     */
    public ObservableList<Disease> getCurrentDiseases() {
        return currentDiseases;
    }

    /**
     * Get the past diseases.
     *
     * @return The past diseases.
     */
    public ObservableList<Disease> getPastDiseases() {
        return pastDiseases;
    }

    /**
     * Splits up the current diseases into chronic and non-chronic which will later be used to sort them.
     */
    private void splitCurrentDiseases() {
        currentChronicDiseases = FXCollections.observableArrayList(new ArrayList<>());
        currentNonChronicDiseases = FXCollections.observableArrayList(new ArrayList<>());

        for (Disease disease : currentDiseases) {
            if (disease.getIsChronicDisease()) {
                currentChronicDiseases.add(disease);
            } else {
                currentNonChronicDiseases.add(disease);
            }
        }
    }

    /**
     * Gets the current sort type from the current diseases table
     *
     * @return ASCENDING or DESCENDING sort type
     */
    @FXML
    private TableColumn.SortType getCurrentDiseaseSortType() {
        TableColumn sortColumn;
        TableColumn.SortType sortType;
        if (currentDiseaseTableView.getSortOrder().size() > 0) {
            sortColumn = currentDiseaseTableView.getSortOrder().get(0);
            sortType = sortColumn.getSortType();
        } else {
            sortType = TableColumn.SortType.DESCENDING;
        }

        return sortType;
    }

    /**
     * Gets the column that needs to be sorted from the current diseases table
     *
     * @return name or date column which will be used to sort on
     */
    @FXML
    private TableColumn getCurrentDiseaseSortColumn() {
        TableColumn sortColumn = currentDiseaseDateColumn;
        if (currentDiseaseTableView.getSortOrder().size() > 0) {
            sortColumn = currentDiseaseTableView.getSortOrder().get(0);
        }

        return sortColumn;
    }

    /**
     * Gets the column that needs to be sorted from the past diseases table
     *
     * @return name or date column which will be used to sort on
     */
    @FXML
    private TableColumn getPastDiseaseSortColumn() {
        TableColumn sortColumn = pastDiseaseDateColumn;

        if (pastDiseaseTableView.getSortOrder().size() > 0) {
            sortColumn = pastDiseaseTableView.getSortOrder().get(0);
        }

        return sortColumn;
    }

    /**
     * Gets the current sort type from the past diseases table
     *
     * @return ASCENDING or DESCENDING sort type
     */
    @FXML
    private TableColumn.SortType getPastDiseaseSortType() {
        TableColumn sortColumn;
        TableColumn.SortType sortType;
        if (pastDiseaseTableView.getSortOrder().size() > 0) {
            sortColumn = pastDiseaseTableView.getSortOrder().get(0);
            sortType = sortColumn.getSortType();
        } else {
            sortType = TableColumn.SortType.DESCENDING;
        }

        return sortType;
    }

    /**
     * Sorts the past diseases base on the date and the sort type that is passed in.
     * <p>
     * Do not call this function on its own. updateTables() calls this so the diseases will be sorted when the tables
     * need to be updated. Failing to do so will results in an infinite loop.
     *
     * @param sortType The sort type that will be used (ASCENDING/ DESCENDING).
     */
    @FXML
    private void sortPastDiseasesByDate(TableColumn.SortType sortType) {

        if (sortType.equals(TableColumn.SortType.DESCENDING)) {
            pastDiseases.sort(new DescendingDiseaseDateComparator());
        } else {
            pastDiseases.sort(new AscendingDiseaseDateComparator());
        }
    }

    /**
     * Sorts the past diseases base on the name and the sort type that is passed in.
     * <p>
     * Do not call this function on its own. updateTables() calls this so the diseases will be sorted when the tables
     * need to be updated. Failing to do so will results in an infinite loop.
     *
     * @param sortType The sort type that will be used (ASCENDING/ DESCENDING).
     */
    @FXML
    private void sortPastDiseasesByName(TableColumn.SortType sortType) {

        if (sortType.equals(TableColumn.SortType.DESCENDING)) {
            pastDiseases.sort(new DescendingDiseaseNameComparator());
        } else {
            pastDiseases.sort(new AscendingDiseaseNameComparator());
        }
    }

    /**
     * Sorts the current diseases based on the date and the sort type that is passed in.
     * <p>
     * Do not call this function on its own. updateTables() calls this so the diseases will be sorted when the tables
     * need to be updated. Failing to do so will results in an infinite loop.
     *
     * @param sortType The sort type that will be used (ASCENDING/ DESCENDING).
     */
    @FXML
    private void sortCurrentDiseasesByDate(TableColumn.SortType sortType) {
        splitCurrentDiseases();

        if (sortType.equals(TableColumn.SortType.DESCENDING)) {
            currentChronicDiseases.sort(new DescendingDiseaseDateComparator());
            currentNonChronicDiseases.sort(new DescendingDiseaseDateComparator());
        } else {
            currentChronicDiseases.sort(new AscendingDiseaseDateComparator());
            currentNonChronicDiseases.sort(new AscendingDiseaseDateComparator());
        }

        currentDiseases.clear();
        currentDiseases.addAll(currentChronicDiseases);
        currentDiseases.addAll(currentNonChronicDiseases);
    }

    /**
     * Sorts the current diseases based on the name and the sort type that is passed in.
     * <p>
     * Do not call this function on its own. updateTables() calls this so the diseases will be sorted when the tables
     * need to be updated. Failing to do so will results in an infinite loop.
     *
     * @param sortType The sort type that will be used (ASCENDING/ DESCENDING).
     */
    @FXML
    private void sortCurrentDiseasesByName(TableColumn.SortType sortType) {
        splitCurrentDiseases();

        if (sortType.equals(TableColumn.SortType.DESCENDING)) {
            currentChronicDiseases.sort(new DescendingDiseaseNameComparator());
            currentNonChronicDiseases.sort(new DescendingDiseaseNameComparator());
        } else {
            currentChronicDiseases.sort(new AscendingDiseaseNameComparator());
            currentNonChronicDiseases.sort(new AscendingDiseaseNameComparator());
        }

        currentDiseases.clear();
        currentDiseases.addAll(currentChronicDiseases);
        currentDiseases.addAll(currentNonChronicDiseases);
    }

    /**
     * Resets the fields associated to adding a new disease to their default values
     */
    @FXML
    private void clearAddNewDiseaseFields() {
        diseaseDatePicker.setValue(LocalDate.now());
        diseaseNameTextField.clear();
        isChronicDiseaseCheckBox.setSelected(false);
    }

    /**
     * Stes the ProfileSceneController for the class.
     *
     * @param profileSceneController The ProfileSceneController that is to be set.
     */
    public void setProfileSceneController(ProfileSceneController profileSceneController) {
        this.profileSceneController = profileSceneController;
    }

    /**
     * Hides edit related GUI elements
     */
    @Override
    public void hide() {
        addNewDiseaseButton.setVisible(false);
        moveDiseaseToCurrentDiseasesButton.setVisible(false);
        moveDiseaseToPastDiseasesButton.setVisible(false);
        diseaseNameTextField.setVisible(false);
        diseaseDatePicker.setVisible(false);
        isChronicDiseaseCheckBox.setVisible(false);
        diseaseDiagnosisDateLabel.setVisible(false);
        diseaseNameLabel.setVisible(false);
    }

    /**
     * Reveals edit related GUI elements
     */
    @Override
    public void reveal() {
        addNewDiseaseButton.setVisible(true);
        moveDiseaseToCurrentDiseasesButton.setVisible(true);
        moveDiseaseToPastDiseasesButton.setVisible(true);
        diseaseNameTextField.setVisible(true);
        diseaseDatePicker.setVisible(true);
        isChronicDiseaseCheckBox.setVisible(true);
        diseaseDiagnosisDateLabel.setVisible(true);
        diseaseNameLabel.setVisible(true);
    }

    /**
     * Function refreshes that GUI tables so that they display the correct data after receiving some kind of updates.
     */
    public void refreshTables() {
        currentDiseaseTableView.refresh();
        pastDiseaseTableView.refresh();
    }
}
