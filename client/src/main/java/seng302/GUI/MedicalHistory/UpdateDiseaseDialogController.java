package seng302.GUI.MedicalHistory;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import seng302.GUI.ControllerAccess;
import seng302.GUI.Notifications.AlertDialog;
import seng302.GUI.Profile.ProfileSceneController;
import seng302.Model.Disease;
import seng302.Utilities.Undo_Redo.UndoRedoController;
import seng302.Utilities.Undo_Redo.UndoableCheckBox;
import seng302.Utilities.Undo_Redo.UndoableDatePicker;
import seng302.Utilities.Undo_Redo.UndoableTextField;
import seng302.Utilities.Validator;

import java.time.LocalDate;

import static seng302.Utilities.Style.applyDatePickerStyle;
import static seng302.Utilities.Style.applyTextFieldStyle;
import static seng302.Utilities.Tooltips.*;
import static seng302.Utilities.Validator.isValidDiseaseDiagnosisDate;
import static seng302.Utilities.Validator.isValidDiseaseName;

public class UpdateDiseaseDialogController {
    // Disease update related FXML components
    @FXML
    private Label updateDiseaseNameLabel, updateDateOfDiagnosisLabel;
    @FXML
    private TextField updateDiseaseNameTextField;
    @FXML
    private CheckBox updateChronicCheckBox;
    @FXML
    private DatePicker updateDateOfDiagnosisDatePicker;
    @FXML
    private Button updateSaveButton, updateCancelButton, undoButton, redoButton;
    @FXML
    private AnchorPane dialogAnchorPane;

    private Disease diseaseToUpdate;
    private ObservableList<Disease> originatingDiseaseList;
    private TableView originatingTableView;
    private UndoRedoController undoRedoController = new UndoRedoController();
    private boolean currentlyUndoing;

    /**
     * Populates the dialog box with the data from the selected disease
     *
     * @param disease                   The disease object we are looking at
     * @param givenOriginatingTableView the table that the disease came from
     * @param givenDiseaseList          the list that is linked to the table the disease came from
     */
    public void initDialog(Disease disease, TableView givenOriginatingTableView, ObservableList<Disease> givenDiseaseList) {
        diseaseToUpdate = disease;
        originatingDiseaseList = givenDiseaseList;
        originatingTableView = givenOriginatingTableView;

        updateDiseaseNameTextField.setText(disease.getDiseaseName());
        updateDateOfDiagnosisDatePicker.setValue(disease.getDateOfDiagnosis());
        updateChronicCheckBox.setSelected(disease.getIsChronicDisease());

        updateChronicCheckBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (!currentlyUndoing) {
                pushCheckBoxChangeToUndoDeque(updateChronicCheckBox);
            }
        });

        initializeUndoRedo();

        applyStyles();
    }

    /**
     * Saves the disease when the update button is clicked.
     * If fields are not filled out or there is some error in the input a dialog box is shown to inform the user.
     */
    @FXML
    private void saveDiseaseUpdateClicked() {
        MedicalHistoryTabController medicalHistoryTabController = ControllerAccess.getMedicalHistoryTabController();
        medicalHistoryTabController.pushUndoableDiseaseList(medicalHistoryTabController.getCurrentDiseases(), medicalHistoryTabController.getPastDiseases());

        // Get values from GUI
        String diseaseName = updateDiseaseNameTextField.getText();
        Boolean isChronic = updateChronicCheckBox.isSelected();
        LocalDate diagnosisDate = updateDateOfDiagnosisDatePicker.getValue();

        ProfileSceneController profile = ControllerAccess.getProfileSceneController();
        LocalDate dob = profile.getCurrentProfile().getDateOfBirth();

        if (!Validator.isValidDiseaseDiagnosisDate(diagnosisDate, dob)) {
            AlertDialog.showAndWait("Error", "Invalid date of diagnosis");
            return;
        }

        if (!Validator.isValidDiseaseName(diseaseName)) {
            AlertDialog.showAndWait("Error", "Invalid disease name");
            return;
        }

        // Create new disease to be added
        Disease updatedDisease = new Disease(diseaseName, isChronic, diagnosisDate);

        // Remove old version of disease
        originatingDiseaseList.remove(diseaseToUpdate);

        // If chronic move to current diseases, otherwise leave it in the original list
        if (isChronic) {
            medicalHistoryTabController.addCurrentDisease(updatedDisease);
        } else {
            originatingDiseaseList.add(updatedDisease);
        }

        originatingTableView.refresh();

        close();
    }

    /**
     * Creates an UndoRedoController for the window and adds listeners to the undo and redo buttons so they trigger
     * undo and redo events when pressed.
     */
    private void initializeUndoRedo() {
        currentlyUndoing = false;
        undoRedoController = new UndoRedoController();

        undoButton.setOnAction(event -> {
            currentlyUndoing = true;
            undoRedoController.undo();
            currentlyUndoing = false;
            updateUndoRedoButtons();
        });

        redoButton.setOnAction(event -> {
            currentlyUndoing = true;
            undoRedoController.redo();
            currentlyUndoing = false;
            updateUndoRedoButtons();
        });

        updateUndoRedoButtons();
    }

    /**
     * Push changes to the undo-redo deque and update the undo redo buttons to either enable or disable based on whether
     * the stacks are empty or not. An empty stack would mean it is disabled and a non-empty one would mean the button
     * would be enabled.
     *
     * @param textField the textfield which the changes have come from.
     * @param oldValue  the old value that we are wanting to restore when an undo or redo event will occur.
     */
    private void pushTextFieldChangeToUndoDeque(TextField textField, String oldValue) {
        undoRedoController.pushToUndoDeque(new UndoableTextField(textField, oldValue));
        updateUndoRedoButtons();
    }

    /**
     * Push changes to the undo-redo deque and update the undo redo buttons to either enable or disable based on whether
     * the stacks are empty or not. An empty stack would mean it is disabled and a non-empty one would mean the button
     * would be enabled.
     *
     * @param datePicker the date picker which the changes have come from.
     * @param oldValue   the old value what we are wanting to restore when an undo or redo event will occur.
     */
    private void pushDatePickerChangeToUndoDeque(DatePicker datePicker, LocalDate oldValue) {
        undoRedoController.pushToUndoDeque(new UndoableDatePicker(datePicker, oldValue));
        updateUndoRedoButtons();
    }

    /**
     * Push changes to the undo-redo deque and update the undo redo buttons to either enable or disable based on whether
     * the stacks are empty or not. An empty stack would mean it is disabled and a non-empty one would mean the button
     * would be enabled.
     *
     * @param checkBox the checkbox which the changes have come from.
     */
    private void pushCheckBoxChangeToUndoDeque(CheckBox checkBox) {
        undoRedoController.pushToUndoDeque(new UndoableCheckBox(checkBox));
        updateUndoRedoButtons();
    }

    /**
     * Ensures that the undo/redo buttons are only enabled if there is an available undo/redo.
     */
    private void updateUndoRedoButtons() {
        undoButton.setDisable(undoRedoController.isUndoEmpty());
        redoButton.setDisable(undoRedoController.isRedoEmpty());
    }

    /**
     * Applies the default styles to each of the fields in the update dialog.
     * Name and date are both valid as they must have been when the disease gets added.
     */
    private void applyStyles() {
        // Initialise default styles
        applyTextFieldStyle(updateDiseaseNameTextField, true, false, VALID_DISEASE_NAME_TOOLTIP_TEXT,
                INVALID_DISEASE_NAME_TOOLTIP_TEXT, updateDiseaseNameTextField.getText());
        applyDatePickerStyle(updateDateOfDiagnosisDatePicker, true, false, VALID_DISEASE_DIAGNOSIS_DATE_TOOLTIP_TEXT,
                INVALID_DISEASE_DIAGNOSIS_DATE_TOOLTIP_TEXT, updateDateOfDiagnosisDatePicker.getValue());

        // Add listeners to signal user of input validity
        updateDiseaseNameTextField.setTooltip(createNewTooltip(INVALID_DISEASE_NAME_TOOLTIP_TEXT));
        updateDiseaseNameTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    if (!oldValue.equals(newValue) && !currentlyUndoing) {
                        pushTextFieldChangeToUndoDeque(updateDiseaseNameTextField, oldValue);
                    }
                    Boolean acceptableDiseaseName = isValidDiseaseName(newValue);
                    applyTextFieldStyle(updateDiseaseNameTextField, acceptableDiseaseName, false, VALID_DISEASE_NAME_TOOLTIP_TEXT,
                            INVALID_DISEASE_NAME_TOOLTIP_TEXT, newValue);
                }));

        updateDateOfDiagnosisDatePicker.setTooltip(createNewTooltip(INVALID_DISEASE_DIAGNOSIS_DATE_TOOLTIP_TEXT));
        updateDateOfDiagnosisDatePicker.valueProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    if (((oldValue == null) || (!oldValue.equals(newValue))) && !currentlyUndoing) {
                        pushDatePickerChangeToUndoDeque(updateDateOfDiagnosisDatePicker, oldValue);
                    }
                    ProfileSceneController profile = ControllerAccess.getProfileSceneController();
                    LocalDate dob = profile.getCurrentProfile().getDateOfBirth();
                    Boolean acceptableDiseaseDiagnosisDate = isValidDiseaseDiagnosisDate(newValue, dob);
                    applyDatePickerStyle(updateDateOfDiagnosisDatePicker, acceptableDiseaseDiagnosisDate, false, VALID_DISEASE_DIAGNOSIS_DATE_TOOLTIP_TEXT,
                            INVALID_DISEASE_DIAGNOSIS_DATE_TOOLTIP_TEXT, newValue);
                }));
    }

    /**
     * Closes the dialog box if the cancel button is clicked
     */
    @FXML
    private void cancelDiseaseUpdateClicked() {
        close();
    }

    /**
     * Closes the current stage
     */
    private void close() {
        Stage updateDiseaseDialogStage = (Stage) dialogAnchorPane.getScene().getWindow();
        updateDiseaseDialogStage.close();
    }
}
