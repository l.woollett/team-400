package seng302.GUI;

/**
 * Interface for hiding different items on a page. 301 baby!
 */
public interface Hideable {

    void hide();

    void reveal();
}
