package seng302.GUI.Procedures;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import seng302.Enum.OrganEnum;
import seng302.GUI.Hideable;
import seng302.Model.Procedure;
import seng302.Model.Profile;
import seng302.Utilities.Undo_Redo.UndoRedoController;
import seng302.Utilities.Undo_Redo.UndoableDatePicker;
import seng302.Utilities.Undo_Redo.UndoableOrganCheckListView;
import seng302.Utilities.Undo_Redo.UndoableTextArea;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.*;
import java.util.logging.Logger;

import static seng302.Utilities.Style.applyStyle;

public class UserMedicalProceduresTabController implements Initializable, Hideable {


    private final Logger LOGGER = Logger.getLogger(getClass().getName());
    // Declarations of all the FXMl injected variables
    @FXML
    private AnchorPane procedureFunctionalityContainer, mainProcedureAnchorPane;
    @FXML
    private TableView<Procedure> pendingProceduresTableView;
    @FXML
    private TableView<Procedure> pastProceduresTableView;
    @FXML
    private TableColumn<Procedure, LocalDate> pastProceduresDateOfProceduresColumn,
            pendingProceduresDateOfProceduresColumn;
    @FXML
    private TableColumn<Procedure, String> pastProceduresSummaryColumn, pendingProceduresSummaryColumn;
    @FXML
    private TextArea summaryInput;
    @FXML
    private TextArea descriptionInput;
    @FXML
    private DatePicker procedureDateInput;
    private ChangeListener<String> summaryListener, descriptionListener;
    private ChangeListener<LocalDate> dateListener;
    //==================================================================================================================
    // CheckListView Stuff
    //==================================================================================================================
    @FXML
    private ListView<OrganEnum> donatedOrgansDisplay;
    private ObservableList<OrganEnum> organsForDonation;
    private List<OrganEnum> checkedOrgans = new ArrayList<>();
    private Map<OrganEnum, ObservableValue<Boolean>> organChecklist = new HashMap<>();
    private Callback<OrganEnum, ObservableValue<Boolean>> organToBoolean;

    //==================================================================================================================
    // Additional
    //==================================================================================================================
    private boolean isValidSummary = false;
    private boolean isValidDescription = false;
    private boolean isValidProcedureDate = false;
    private List<Procedure> addedProcedures = new ArrayList<>(), deletedProcedures = new ArrayList<>(),
            originalPastProcedures = new ArrayList<>(), originalPendingProcedures = new ArrayList<>();
    private Profile currentProfile;

    private ObservableList<Procedure> pendingProcedures, pastProcedures;
    /**
     * Controls all undo/redo functionality for the class.
     */
    @FXML
    private UndoRedoController undoRedoController;
    private boolean currentlyUndoing = false;

    public UserMedicalProceduresTabController() {
        organToBoolean = organ -> organChecklist.get(organ);
    }

    /**
     *
     * Initializes the medical procedures tab.
     *
     * @param location  Unused parameter present so signature matches.
     * @param resources Unused parameter present so signature matches.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        pastProceduresDateOfProceduresColumn.prefWidthProperty().bind(pastProceduresTableView.widthProperty().divide(2));
        pastProceduresSummaryColumn.prefWidthProperty().bind(pastProceduresTableView.widthProperty().divide(2));
        pendingProceduresDateOfProceduresColumn.prefWidthProperty().bind(pastProceduresTableView.widthProperty().divide(2));
        pendingProceduresSummaryColumn.prefWidthProperty().bind(pastProceduresTableView.widthProperty().divide(2));

        organsForDonation = FXCollections.observableArrayList();
        donatedOrgansDisplay.setItems(organsForDonation);
        summaryListener = (observable, oldValue, newValue) -> {
            isValidSummary = !summaryInput.getText().trim().equals("");
            applyStyle(summaryInput, isValidSummary);
            if (!oldValue.equals(newValue) && !currentlyUndoing) {
                undoRedoController.pushToUndoDeque(new UndoableTextArea(summaryInput, oldValue));
            }
        };

        descriptionListener = (observable, oldValue, newValue) -> {
            isValidDescription = !(descriptionInput.getText().trim().equals(""));
            applyStyle(descriptionInput, isValidDescription);
            if (!oldValue.equals(newValue) && !currentlyUndoing) {
                undoRedoController.pushToUndoDeque(new UndoableTextArea(descriptionInput, oldValue));
            }
        };

        dateListener = (observable, oldValue, newValue) -> {
            LocalDate dateOfBirth = currentProfile.getDateOfBirth();
            isValidProcedureDate = (!(procedureDateInput.getValue() == null) &&
                    (newValue.isAfter(dateOfBirth) || newValue.isEqual(dateOfBirth)));
            applyStyle(procedureDateInput, isValidProcedureDate);
            if (((oldValue == null) || (!oldValue.equals(newValue))) && !currentlyUndoing) {
                undoRedoController.pushToUndoDeque(new UndoableDatePicker(procedureDateInput, oldValue));
            }
        };
        init();
    }

    /**
     * Sets the undoRedoController for the class.
     *
     * @param undoRedoController The UndoRedoController to be set.
     */
    public void setUndoRedoController(UndoRedoController undoRedoController) {
        this.undoRedoController = undoRedoController;
    }

    /**
     * Sets the currently Undoing variables for the class.
     *
     * @param currentlyUndoing The value that currentlyUndoing will be set to.
     */
    public void setCurrentlyUndoing(boolean currentlyUndoing) {
        this.currentlyUndoing = currentlyUndoing;
    }

    /**
     * Function applies new, deleted and updated procedures when the apply button is clicked in edit mode for the
     * Profile. Apply flow works as follows: New procedures are added and deleted procedures are removed to
     * generate history entries, then the pending and past procedure lists are set using the clone entries in the GUI.
     */
    public void applyChanges() {
        addNewProcedures();
        removeProcedures();
        updateProcedures();
        hide();
    }

    /**
     * Function adds new procedures to the current Profile.
     */
    private void addNewProcedures() {
        for (Procedure newProcedure: addedProcedures) {
            currentProfile.addProcedure(newProcedure);
        }
    }

    /**
     * Function removes procedures to be deleted from the current Profile.
     */
    private void removeProcedures() {
        for (Procedure removedProcedure: deletedProcedures) {
            currentProfile.removeProcedure(removedProcedure);
        }

        for (Procedure removedProcedure : deletedProcedures) {
            Procedure toRemove = null;
            boolean isPending = false;
            for (Procedure procedure : originalPastProcedures) {
                if (procedure.equals(removedProcedure)) {
                    toRemove = procedure;
                }
            }
            for (Procedure procedure : originalPendingProcedures) {
                if (procedure.equals(removedProcedure)) {
                    toRemove = procedure;
                    isPending = true;
                }
            }
            if (isPending) {
                originalPendingProcedures.remove(toRemove);
            } else {
                originalPastProcedures.remove(toRemove);
            }
        }
    }

    /**
     * Function applies all the changes to any procedures that have been updated for the current Profile.
     */
    private void updateProcedures() {
        currentProfile.setPendingProcedures(originalPendingProcedures);
        currentProfile.setPastProcedures(originalPastProcedures);
    }

    /**
     * If all the input fields have valid values create the procedure and add it to the table. Otherwise do nothing
     * since there will be visual indication that there are invalid inputs.
     */
    @FXML
    private void addProcedure() {
        if (isValidProcedureDate && isValidDescription && isValidSummary) {
            String summary = summaryInput.getText();
            String description = descriptionInput.getText();
            LocalDate procedureDate = procedureDateInput.getValue();
            List<OrganEnum> newAffectedOrganList = new ArrayList<>(checkedOrgans);
            if (!isDuplicate()) {
                Procedure newProcedure = new Procedure(summary, description, procedureDate,
                        newAffectedOrganList);
                addedProcedures.add(newProcedure);
                if (procedureDate.isBefore(LocalDate.now())) {
                    pastProcedures.add(newProcedure);
                    originalPastProcedures.add(newProcedure);
                } else {
                    pendingProcedures.add(newProcedure);
                    originalPendingProcedures.add(newProcedure);
                }
            } else {
                Alert duplicateAlert = new Alert(Alert.AlertType.ERROR);
                duplicateAlert.setHeaderText("Duplicate Entry Error");
                Text alertText = new Text("You cannot schedule the same procedure twice in a single day");
                alertText.setWrappingWidth(400);
                duplicateAlert.getDialogPane().setContent(alertText);
                duplicateAlert.showAndWait();
            }
        }
    }

    /**
     * Function does a check to see if a procedure being added is a duplicate of an existing procedure.
     *
     * @return boolean indicating if there is a duplicate.
     */
    private boolean isDuplicate() {
        for (Procedure pendingProcedure : originalPendingProcedures) {
            if (pendingProcedure.isDuplicate(summaryInput.getText().trim(), procedureDateInput.getValue())) {
                return true;
            }
        }

        for (Procedure pastProcedure : originalPastProcedures) {
            if (pastProcedure.isDuplicate(summaryInput.getText().trim(), procedureDateInput.getValue())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Function sorts the table of procedures.
     * @param table TableView to be sorted
     * @param procedures The observable list of Procedures populating the TaleView table.
     */
    private void sortTable(TableView<Procedure> table, ObservableList<Procedure> procedures) {
        if (table.getSortOrder().size() == 0) {
            procedures.sort((o1, o2) -> {
                if (o1.getDateOfProcedure().isBefore(o2.getDateOfProcedure())) {
                    return 1;
                } else if (o1.getDateOfProcedure().isEqual(o2.getDateOfProcedure())) {
                    return 0;
                } else {
                    return -1;
                }
            });
        }
    }

    /**
     * Create the context menu for when the user right clicks on a row in the procedure table view.
     *
     * @param row            The row of the procedure to be deleted or updated.
     * @param procedureTable The table that will display the procedures.
     * @return The context menu.
     */
    private ContextMenu createContextMenu(TableRow<Procedure> row, TableView<Procedure> procedureTable) {
        ContextMenu contextMenu = new ContextMenu();
        MenuItem updateMenuItem = new MenuItem("Update");
        MenuItem deleteMenuItem = new MenuItem("Delete");
        contextMenu.getItems().addAll(updateMenuItem, deleteMenuItem);
        row.setContextMenu(contextMenu);

        updateMenuItem.setOnAction(event -> openUpdatePopUp(row.getItem(), procedureTable));
        deleteMenuItem.setOnAction(event -> deleteSelectedProcedure(row.getItem(), procedureTable));

        return contextMenu;
    }

    /**
     * The clinician wants to open a pop up to edit the procedure.
     *
     * @param procedure The procedure to be edited.
     * @param tableView The TableView which the procedure came from. Passed as an argument so TableView.refresh() can be
     *                  called, so the changes can be quickly showed in the TableView rather than waiting for the
     *                  TableView to observe the changes.
     */
    private void openUpdatePopUp(Procedure procedure, TableView<Procedure> tableView) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("updateProcedureDialog.fxml"));
            Parent root = loader.load();
            UpdateProcedureDialogController updateController = loader.getController();
            Scene scene = new Scene(root);
            Stage updateProcedureStage = new Stage();
            updateProcedureStage.setScene(scene);
            updateProcedureStage.initModality(Modality.APPLICATION_MODAL);
            updateController.initDialog(procedure, donatedOrgansDisplay.getItems(), tableView, this);
            updateProcedureStage.show();
            updateController.setParentUndoRedoController(this.undoRedoController);
        } catch (IOException e) {
            LOGGER.severe(e.getMessage());
        }
    }

    /**
     * The clinician wants to remove a procedure.
     *
     * @param procedureToRemove The procedure to remove.
     * @param tableReference    Reference to the table which is displaying the procedures. It is passed as an argument
     *                          so we can call Table.refresh() so the changes are quickly updated/represented in the
     *                          table.
     */
    private void deleteSelectedProcedure(Procedure procedureToRemove, TableView<Procedure> tableReference) {
        Procedure placeHolder = null;
        deletedProcedures.add(procedureToRemove);
        if (procedureToRemove.getDateOfProcedure().isBefore(LocalDate.now())) {
            pastProcedures.remove(procedureToRemove);
        } else {
            pendingProcedures.remove(procedureToRemove);
        }
        tableReference.getItems().remove(procedureToRemove);
        tableReference.refresh();
    }

    /**
     * Define what should should be displayed in each of the tables. Will enable the controls for editing a procedure
     * only if the clinician is logged in.
     */
    private void init() {


        applyStyle(summaryInput, isValidSummary);
        applyStyle(descriptionInput, isValidDescription);
        applyStyle(procedureDateInput, isValidProcedureDate);


        summaryInput.textProperty().addListener(summaryListener);

        descriptionInput.textProperty().addListener(descriptionListener);

        procedureDateInput.valueProperty().addListener(dateListener);

        pendingProceduresDateOfProceduresColumn.setCellValueFactory(new PropertyValueFactory<>("dateOfProcedure"));
        pendingProceduresSummaryColumn.setCellValueFactory(new PropertyValueFactory<>("summary"));

        pastProceduresDateOfProceduresColumn.setCellValueFactory(new PropertyValueFactory<>("dateOfProcedure"));
        pastProceduresSummaryColumn.setCellValueFactory(new PropertyValueFactory<>("summary"));

    }

    /**
     * Will populate the tables with the profile's procedures. Sorting of the tables will also happen here, by default
     * the sort will be based on the date. With the most recent procedure being at the top.
     */
    public void populateTables() {

        sortTable(pendingProceduresTableView, pendingProcedures);
        sortTable(pastProceduresTableView, pastProcedures);
        pendingProceduresTableView.setItems(pendingProcedures);
        pastProceduresTableView.setItems(pastProcedures);
        pastProceduresTableView.refresh();
        pendingProceduresTableView.refresh();
    }

    /**
     * Need to allow the clinician to select which organs might be affected from the procedure.
     *
     * @param organs The Profile's organs selected for donation.
     */
    public void setOrgansForDonation(List<OrganEnum> organs) {

        this.organsForDonation = FXCollections.observableArrayList(organs);
        donatedOrgansDisplay.setItems(organsForDonation);
        ArrayList<OrganEnum> toRemove = new ArrayList<>();
        for (OrganEnum organ : checkedOrgans) {
            if (!organsForDonation.contains(organ)) {
                toRemove.add(organ);
            }
        }
        for (OrganEnum organ : organs) {
            if (checkedOrgans.contains(organ)) {
                SimpleBooleanProperty prop = (SimpleBooleanProperty) organChecklist.get(organ);
                prop.setValue(true);
            } else if (toRemove.contains(organ)) {
                SimpleBooleanProperty prop = (SimpleBooleanProperty) organChecklist.get(organ);
                prop.setValue(false);
            } else {
                organChecklist.put(organ, new SimpleBooleanProperty(false));
            }
        }
        for (Map.Entry<OrganEnum, ObservableValue<Boolean>> entry : organChecklist.entrySet()) {
            entry.getValue().addListener(observable -> {
                if (!currentlyUndoing) {
                    undoRedoController.pushToUndoDeque(new UndoableOrganCheckListView((SimpleBooleanProperty)
                            entry.getValue()));
                }
                if (entry.getValue().getValue() && !checkedOrgans.contains(entry.getKey())) {
                    checkedOrgans.add(entry.getKey());
                } else if (!entry.getValue().getValue()) {
                    checkedOrgans.remove(entry.getKey());
                }
            });
        }
        donatedOrgansDisplay.setCellFactory(CheckBoxListCell.forListView(organToBoolean));
    }

    LocalDate getProfileDateOfBirth() {
        return this.currentProfile.getDateOfBirth();
    }

    /**
     * Function sets the classes* current Profile object.
     * @param currentProfile the current profile from the parent ProfileSceneController.
     */
    public void setCurrentProfile(Profile currentProfile) {
        this.currentProfile = currentProfile;
    }

    /**
     * Function makes a cloned list of the original Procedures for the currentProfile. This list is used to restore
     * the currentProfile to its original state in the situation when a user decides to cancel changes after changing
     * procedure details.
     */
    private void cloneCurrentProcedures() {
        //empty the lists of clones first
        originalPendingProcedures.clear();
        originalPastProcedures.clear();

        for (Procedure original: currentProfile.getPastProcedures()) {
            originalPastProcedures.add(new Procedure(original));
        }

        for (Procedure original: currentProfile.getPendingProcedures()) {
            originalPendingProcedures.add(new Procedure(original));
        }
    }

    /**
     * Hides edit related fields.
     */
    @Override
    public void hide() {
        pendingProcedures = FXCollections.observableArrayList(currentProfile.getPendingProcedures());
        pastProcedures = FXCollections.observableArrayList(currentProfile.getPastProcedures());
        procedureFunctionalityContainer.setVisible(false);
        pendingProceduresTableView.setRowFactory(null);
        pastProceduresTableView.setRowFactory(null);
        populateTables();
    }

    /**
     * Reveals edit related fields.
     */
    @Override
    public void reveal() {
        cloneCurrentProcedures();
        pastProcedures = FXCollections.observableArrayList(originalPastProcedures);
        pendingProcedures = FXCollections.observableArrayList(originalPendingProcedures);
        pastProceduresTableView.setRowFactory(tableView -> {
            TableRow<Procedure> row = new TableRow<>();
            row.setContextMenu(createContextMenu(row, tableView));
            return row;
        });
        pendingProceduresTableView.setRowFactory(tableView -> {
            TableRow<Procedure> row = new TableRow<>();
            row.setContextMenu(createContextMenu(row, tableView));
            return row;
        });
        populateTables();
        procedureFunctionalityContainer.setVisible(true);
    }

}