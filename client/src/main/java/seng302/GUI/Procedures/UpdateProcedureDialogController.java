package seng302.GUI.Procedures;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxListCell;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Callback;
import seng302.Enum.OrganEnum;
import seng302.GUI.ControllerAccess;
import seng302.Model.Procedure;
import seng302.Utilities.Undo_Redo.*;

import java.net.URL;
import java.time.LocalDate;
import java.util.*;

import static seng302.Utilities.Style.applyStyle;
import static seng302.Utilities.Tooltips.INVALID_PROCEDURE_DATE_TOOLTIP_TEXT;
import static seng302.Utilities.Tooltips.createNewTooltip;

/**
 * The controller for the updateProcedureDialog.fxml Used when the clinician wants to update a listed Procedure.
 */
public class UpdateProcedureDialogController implements Initializable {

    @FXML
    private AnchorPane mainContainer;
    @FXML
    private TextArea summaryInput;
    @FXML
    private TextArea descriptionInput;
    @FXML
    private DatePicker dateInput;
    @FXML
    private ListView<OrganEnum> affectedOrgansSelection;
    private List<OrganEnum> checkedOrgans = new ArrayList<>();
    private Map<OrganEnum, ObservableValue<Boolean>> organChecklist = new HashMap<>();
    private Callback<OrganEnum, ObservableValue<Boolean>> organToBoolean;

    private TableView<Procedure> originalTableViewReference;
    private Procedure originalProcedureReference;
    private UserMedicalProceduresTabController originalProceduresController;

    private boolean isValidSummary = true;
    private boolean isValidDescription = true;
    private boolean isValidProcedureDate = true;

    //==================================================================================================================
    // UndoRedo related stuff
    //==================================================================================================================
    private boolean undoRedoPushingBlocked;
    private UndoRedoController undoRedoController;
    private UndoRedoController parentUndoRedoController;
    @FXML
    private Button undoBtn, redoBtn;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        organToBoolean = organ -> organChecklist.get(organ);
        undoRedoController = new UndoRedoController();
        updateButtons();
    }

    /**
     * Initializes the scene and add listeners which applies the valid and invalid styles.
     */
    @FXML
    private void initialize() {
        dateInput.setTooltip(createNewTooltip(INVALID_PROCEDURE_DATE_TOOLTIP_TEXT));
    }

    /**
     * Sets the input fields to the current procedure's values.
     *
     * @param originalProcedure    The procedure that will be updated.
     * @param organsForDonation    All the organs that the profile has listed for donation.
     * @param tableView            Reference to the original table which displays the the procedures. This is so we can
     *                             call TableView.refresh() to show the latest changes. If we rely only on the Observable
     *                             lists there is a noticeable delay between closing the stage and the table view react to
     *                             the change.
     * @param proceduresController Controller object for the procedure tab.
     */
    void initDialog(Procedure originalProcedure, List<OrganEnum> organsForDonation,
                    TableView<Procedure> tableView, UserMedicalProceduresTabController proceduresController) {

        this.originalProcedureReference = originalProcedure;
        this.originalTableViewReference = tableView;
        this.originalProceduresController = proceduresController;

        this.summaryInput.setText(originalProcedure.getSummary());
        this.descriptionInput.setText(originalProcedure.getDescription());
        this.dateInput.setValue(originalProcedure.getDateOfProcedure());
        this.affectedOrgansSelection.setItems(FXCollections.observableArrayList(organsForDonation));

        this.summaryInput.textProperty().addListener((observable, oldValue, newValue) -> {
            isValidSummary = !summaryInput.getText().trim().equals("");
            applyStyle(summaryInput, isValidSummary);
            if (!oldValue.equals(newValue) && !undoRedoPushingBlocked) {
                this.undoRedoController.pushToUndoDeque(new UndoableTextArea(summaryInput, oldValue));
            }
        });
        this.descriptionInput.textProperty().addListener((observable, oldValue, newValue) -> {
            isValidDescription = !(descriptionInput.getText().trim().equals(""));
            applyStyle(descriptionInput, isValidDescription);
            if (!oldValue.equals(newValue) && !undoRedoPushingBlocked) {
                this.undoRedoController.pushToUndoDeque(new UndoableTextArea(descriptionInput, oldValue));
            }
        });

        this.dateInput.valueProperty().addListener((observable, oldValue, newValue) -> {
            LocalDate dateOfBirth = proceduresController.getProfileDateOfBirth();
            isValidProcedureDate = (!(dateInput.getValue() == null) &&
                    (newValue.isAfter(dateOfBirth) || newValue.isEqual(dateOfBirth)));
            applyStyle(dateInput, isValidProcedureDate);
            if (((oldValue == null) || (!oldValue.equals(newValue))) && !undoRedoPushingBlocked) {
                this.undoRedoController.pushToUndoDeque(new UndoableDatePicker(dateInput, oldValue));
            }
        });

        this.checkedOrgans = new ArrayList<>();
        this.checkedOrgans.addAll(originalProcedure.getAffectedOrgans());

        for (OrganEnum organ : organsForDonation) {
            if (originalProcedure.getAffectedOrgans().contains(organ)) {
                organChecklist.put(organ, new SimpleBooleanProperty(true));
            } else {
                organChecklist.put(organ, new SimpleBooleanProperty(false));
            }
        }

        for (Map.Entry<OrganEnum, ObservableValue<Boolean>> entry : organChecklist.entrySet()) {
            entry.getValue().addListener(observable -> {
                if (!undoRedoPushingBlocked) {
                    undoRedoController.pushToUndoDeque(new UndoableOrganCheckListView((SimpleBooleanProperty)
                            entry.getValue()));
                    updateButtons();
                }
                if (entry.getValue().getValue() && !checkedOrgans.contains(entry.getKey())) {
                    checkedOrgans.add(entry.getKey());
                } else if (!entry.getValue().getValue()) {
                    checkedOrgans.remove(entry.getKey());
                }
            });
        }
        affectedOrgansSelection.setCellFactory(CheckBoxListCell.forListView(organToBoolean));
    }

    /**
     * Will update the procedure's values. After updating call TableView.refresh() to quickly show the changes in the
     * table.
     */
    @FXML
    private void update() {
        String summary = summaryInput.getText().trim();
        String description = descriptionInput.getText().trim();

        if (isValidSummary && isValidDescription) {
            LocalDate date = dateInput.getValue();
            if (date.isAfter(ControllerAccess.getProfileSceneController().getCurrentProfile().getDateOfBirth())) {
                List<OrganEnum> affectedOrgans = checkedOrgans;
                this.parentUndoRedoController.pushToUndoDeque(new UndoableProcedureEdit(originalProcedureReference,
                        originalProcedureReference, originalProceduresController));
                this.originalProcedureReference.setSummary(summary);
                this.originalProcedureReference.setDescription(description);
                this.originalProcedureReference.setDateOfProcedure(date);
                this.originalProcedureReference.setAffectedOrgans(affectedOrgans);
                this.originalTableViewReference.refresh();

                ControllerAccess.getProfileSceneController().getCurrentProfile()
                        .updateProcedureList(this.originalProcedureReference);
                this.originalProceduresController.populateTables();

                Stage ownStage = (Stage) mainContainer.getScene().getWindow();
                ownStage.close();
            }
        }
    }

    /**
     * Close the stage without changing anything.
     */
    @FXML
    private void cancel() {
        Stage ownStage = (Stage) mainContainer.getScene().getWindow();
        ownStage.close();
    }

    @FXML
    private void undoBtnClicked() {
        undoRedoPushingBlocked = true;
        undoRedoController.undo();
        updateButtons();
        undoRedoPushingBlocked = false;
    }

    @FXML
    private void redoBtnClicked() {
        undoRedoPushingBlocked = true;
        undoRedoController.redo();
        updateButtons();
        undoRedoPushingBlocked = false;
    }

    private void updateButtons() {
        undoBtn.setDisable(undoRedoController.isUndoEmpty());
        redoBtn.setDisable(undoRedoController.isRedoEmpty());
    }

    /**
     * Function sets the parent undoRedoController so that the apply change can be added to the parent stack.
     *
     * @param parentUndoRedo UndoRedoController of the parent.
     */
    public void setParentUndoRedoController(UndoRedoController parentUndoRedo) {
        this.parentUndoRedoController = parentUndoRedo;
    }
}
