package seng302.GUI.Procedures;

import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import seng302.Model.*;
import seng302.Utilities.Style;

import java.net.URL;
import java.sql.Time;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Logger;

import static seng302.Utilities.Style.*;
import static seng302.Utilities.Tooltips.*;
import static seng302.Utilities.Validator.*;

public class ProcedureRemovalDialogController implements Initializable {
    @FXML private AnchorPane mainContainer;
    @FXML private Label lblName, lblOrgan;
    @FXML private Text lblDateOfDeath, lblHospitalOfDeath, lblTimeOfDeath, separatorTimeOfDeathText, separator2TimeOfDeathText;
    @FXML private TextField hrTimeOfDeathTextField, minTimeOfDeathTextField, secTimeOfDeathTextField;
    @FXML private RadioButton radioTransplant, radioError, radioDead, radioCured;
    @FXML private DatePicker dateOfDeathPicker;
    @FXML private Button btnOk, clearDateOfDeathButton;
    @FXML private Profile profile;
    @FXML private ComboBox<Disease> diseases;
    @FXML private ChoiceBox<Hospital> hospitalChoice;

    private final ToggleGroup RADIO_GROUP = new ToggleGroup();
    private boolean isDead = false;
    private boolean isDiseaseCured = false;
    private boolean transplantReceived = false;
    private boolean registrationError = false;
    private boolean isDeathDetailsValid = false;
    private Disease curedDisease;
    private List<Hospital> hospitals;

    private final Logger LOGGER = Logger.getLogger(getClass().getName());

    String getSelectedMessage() {
        RadioButton selectedBtn = (RadioButton) RADIO_GROUP.getSelectedToggle();
        return selectedBtn.getText();
    }

    /**
     * Handles what happens when the user clicks on the okay button. Will decide if the input is valid and if the
     * pop up can be closed.
     */
    private EventHandler<MouseEvent> clickedOkayButton = new EventHandler<MouseEvent>() {
        @Override
        public void handle(MouseEvent event) {
            boolean canClose = true;

            if (RADIO_GROUP.getSelectedToggle() == radioDead) {
                if (!isDeathDetailsValid) {
                    canClose = false;
                } else {
                    isDead = true;
                }
            } else if (RADIO_GROUP.getSelectedToggle() == radioCured) {
                curedDisease = diseases.getSelectionModel().getSelectedItem();
                if (curedDisease == null) {
                    Style.applyStyle(diseases, false);
                    canClose = false;
                } else {
                    canClose = true;
                    isDiseaseCured = true;
                }
            } else if (RADIO_GROUP.getSelectedToggle() == radioTransplant) {
                transplantReceived = true;
            } else if (RADIO_GROUP.getSelectedToggle() == radioError) {
                registrationError = true;
            }

            if (canClose) {
                ProcedureRemovalDialogController.this.close();
            }
        }
    };

    /**
     * Initializes the FXML with the given properties, date of death picker and label are hidden unless the death
     * radio button is selected.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        radioTransplant.setToggleGroup(RADIO_GROUP);
        radioError.setToggleGroup(RADIO_GROUP);
        radioCured.setToggleGroup(RADIO_GROUP);
        radioDead.setToggleGroup(RADIO_GROUP);

        diseases.setVisible(false);

        radioCured.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                diseases.setVisible(true);
            } else {
                diseases.setVisible(false);
            }
        });

        RADIO_GROUP.getToggles().get(0).setSelected(true);

        btnOk.addEventHandler(MouseEvent.MOUSE_CLICKED, clickedOkayButton);

        // Setting death details to invisible until clicked.
        setDeathDetailsVisible(false);
        dateOfDeathPicker.setValue(LocalDate.now());


        radioDead.selectedProperty().addListener(((observable, oldValue, newValue) -> {
            if (newValue) {
                setDeathCSS();
                setDeathDetailsVisible(true);
            } else {
                setDeathDetailsVisible(false);
            }
        }));

        hospitalChoice.setConverter(new StringConverter<Hospital>() {
            @Override
            public String toString(Hospital hospital) {
                return hospital.getName();
            }

            @Override
            public Hospital fromString(String hospitalName) {
                for (Hospital hospital : hospitals) {
                    if (hospital.getName().equals(hospitalName)) {
                        return hospital;
                    }
                }

                LOGGER.severe("The hospital wasn't found based on its name: " + hospitalName);
                return null;
            }
        });

        dateOfDeathPicker.valueProperty().addListener((observable, oldValue, newValue) -> setDeathCSS());
        hospitalChoice.valueProperty().addListener(((observable, oldValue, newValue) -> setDeathCSS()));

        hrTimeOfDeathTextField.setTooltip(createNewTooltip(INVALID_HR_TIME_OF_DEATH_TOOLTIP_TEXT));
        hrTimeOfDeathTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> setDeathCSS()));

        minTimeOfDeathTextField.setTooltip(createNewTooltip(INVALID_MIN_TIME_OF_DEATH_TOOLTIP_TEXT));
        minTimeOfDeathTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> setDeathCSS()));

        secTimeOfDeathTextField.setTooltip(createNewTooltip(INVALID_SEC_TIME_OF_DEATH_TOOLTIP_TEXT));
        secTimeOfDeathTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> setDeathCSS()));
    }

    /**
     * Sets the visibility of the death details items to the given boolean state.
     * @param state boolean of intended visibility.
     */
    private void setDeathDetailsVisible(boolean state) {
        lblDateOfDeath.setVisible(state);
        lblHospitalOfDeath.setVisible(state);
        lblTimeOfDeath.setVisible(state);
        separatorTimeOfDeathText.setVisible(state);
        separator2TimeOfDeathText.setVisible(state);
        clearDateOfDeathButton.setVisible(state);
        dateOfDeathPicker.setVisible(state);
        hospitalChoice.setVisible(state);
        hrTimeOfDeathTextField.setVisible(state);
        minTimeOfDeathTextField.setVisible(state);
        secTimeOfDeathTextField.setVisible(state);
    }

    /**
     * Checks the 3 time fields, the dod picker and the hospital choice box
     * and sets the appropriate CSS styling.
     */
    private void setDeathCSS() {

        LocalDate currentValue = dateOfDeathPicker.getValue();
        boolean hospitalSet = hospitalChoice.getValue() != null;

        String currentHrValue = hrTimeOfDeathTextField.getText();
        String currentMinValue = minTimeOfDeathTextField.getText();
        String currentSecValue = secTimeOfDeathTextField.getText();

        LocalDate currentDate = LocalDate.now();
        boolean validDate;
        boolean validHour;
        boolean validMinute;
        boolean validSecond;


        if (dateOfDeathPicker.getValue() != null) {
            validDate = ((dateOfDeathPicker.getValue().compareTo(profile.getDateOfBirth()) >= 0) && (dateOfDeathPicker.getValue().compareTo(currentDate) <= 0));

            applyDatePickerStyle(dateOfDeathPicker, validDate, false, VALID_DATE_OF_DEATH_TOOLTIP_TEXT,
                    INVALID_DATE_OF_DEATH_TOOLTIP_TEXT, currentValue);
        } else {
            applyDatePickerStyle(dateOfDeathPicker, false, false, VALID_DATE_OF_DEATH_TOOLTIP_TEXT,
                    INVALID_DATE_OF_DEATH_TOOLTIP_TEXT, currentValue);
            validDate = false;
        }

        applyChoiceBoxStyle(hospitalChoice, hospitalSet, false, hospitalChoice.getValue());

        validHour = isValidHourTimeOfDeath(currentHrValue);
        applyTextFieldStyle(hrTimeOfDeathTextField, validHour, false, VALID_HR_TIME_OF_DEATH_TOOLTIP_TEXT,
                INVALID_HR_TIME_OF_DEATH_TOOLTIP_TEXT, currentHrValue);

        validMinute = isValidMinuteTimeOfDeath(currentMinValue);
        applyTextFieldStyle(minTimeOfDeathTextField, validMinute, false, VALID_MIN_TIME_OF_DEATH_TOOLTIP_TEXT,
                INVALID_MIN_TIME_OF_DEATH_TOOLTIP_TEXT, currentMinValue);

        validSecond = isValidSecondTimeOfDeath(currentSecValue);
        applyTextFieldStyle(secTimeOfDeathTextField, validSecond, false, VALID_SEC_TIME_OF_DEATH_TOOLTIP_TEXT,
                INVALID_SEC_TIME_OF_DEATH_TOOLTIP_TEXT, currentSecValue);


        isDeathDetailsValid = validDate && hospitalSet && validHour && validMinute && validSecond;
    }

    /**
     * Initializes the controller/pop up.
     *
     * @param profile   The profile currently loaded.
     * @param organ     The organ that we're removing from the list.
     * @param hospitals The available hospitals.
     */
    void initializeDialog(Profile profile, ReceiverOrgan organ, List<Hospital> hospitals) {
        this.profile = profile;
        lblName.setText(String.format("Name: %s %s %S", profile.getFirstName(), profile.getMiddleName(),
                profile.getLastName()));
        lblOrgan.setText("Organ: " + organ.getRegisteredOrgan().toString());
        diseases.setItems(FXCollections.observableArrayList(profile.getCurrentDiseases()));
        this.hospitals = hospitals;
        hospitalChoice.setItems(FXCollections.observableArrayList(this.hospitals));
    }

    /**
     * Closes the popup
     */
    private void close() {
        Stage procedureRemovalDialog = (Stage) mainContainer.getScene().getWindow();
        procedureRemovalDialog.close();
    }

    boolean isDead() {
        return isDead;
    }

    public Death getDeath() {
        if (isDead) {

            Time timeOfDeath = new Time(Integer.parseInt(hrTimeOfDeathTextField.getText().trim()), Integer.parseInt(minTimeOfDeathTextField.getText().trim()),
                    Integer.parseInt(secTimeOfDeathTextField.getText().trim()));

            return new Death(dateOfDeathPicker.getValue(), hospitalChoice.getSelectionModel().getSelectedItem(), timeOfDeath);
        } else {
            LOGGER.severe("The patient has not died, should not be getting their death.");
            return null;
        }
    }

    Disease getCuredDisease() {
        return curedDisease;
    }

    boolean isDiseaseCured() {
        return isDiseaseCured;
    }

    boolean isRegistrationError() { return registrationError; }

    boolean isTransplantReceived() { return transplantReceived; }

    /**
     * Clears the date of death date picker, hospital of death and time of death,  in case you accidentally clicked on
     * it and were wanting to clear.
     */
    @FXML
    private void clearDateOfDeath() {
        dateOfDeathPicker.setValue(null);
        dateOfDeathPicker.setTooltip(createNewTooltip(INVALID_DATE_OF_DEATH_TOOLTIP_TEXT));
        hospitalChoice.setValue(null);
        hrTimeOfDeathTextField.setText("");
        minTimeOfDeathTextField.setText("");
        secTimeOfDeathTextField.setText("");
    }
}
