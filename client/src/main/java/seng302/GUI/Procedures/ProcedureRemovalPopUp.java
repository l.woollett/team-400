package seng302.GUI.Procedures;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.springframework.web.client.HttpClientErrorException;
import seng302.GUI.Clinician.ClinicianTransplantWaitingWrapper;
import seng302.Model.*;
import seng302.ServerInteracton.ServerQueries.Authentication.AuthToken;
import seng302.ServerInteracton.ServerQueries.ProfileRequests;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;


public class ProcedureRemovalPopUp {

    private final Logger LOGGER = Logger.getLogger(getClass().getName());
    private Stage updateProcedureStage;
    private ProcedureRemovalDialogController controller;

    public ProcedureRemovalPopUp(Profile profile, ReceiverOrgan receiverOrgan, List<Hospital> hospitals) throws IOException {
        FXMLLoader loader =
                new FXMLLoader(ProcedureRemovalDialogController.class.getResource("procedureRemovalDialog.fxml"));
        Parent root = loader.load();
        controller = loader.getController();
        Scene scene = new Scene(root);
        updateProcedureStage = new Stage();
        updateProcedureStage.initModality(Modality.APPLICATION_MODAL);
        updateProcedureStage.setScene(scene);
        controller.initializeDialog(profile, receiverOrgan, hospitals);
    }

    public ProcedureRemovalPopUp(ClinicianTransplantWaitingWrapper pendingTransplantPatient, List<Hospital> hospitals) {
        try {
            Profile profile = ProfileRequests.getInstance().getProfile(pendingTransplantPatient.getUserName(), AuthToken.getInstance().getToken()).getBody();
            FXMLLoader loader =
                    new FXMLLoader(ProcedureRemovalDialogController.class.getResource("procedureRemovalDialog.fxml"));
            Parent root = loader.load();
            controller = loader.getController();
            Scene scene = new Scene(root);
            updateProcedureStage = new Stage();
            updateProcedureStage.initModality(Modality.APPLICATION_MODAL);
            updateProcedureStage.setScene(scene);
            controller.initializeDialog(profile, pendingTransplantPatient.getReceiveOrgan(), hospitals);

        } catch (HttpClientErrorException | IOException e) {
            LOGGER.severe(e.getMessage());
        }
    }

    public void showAndWait() {
        updateProcedureStage.showAndWait();
    }

    public String getSelectedMessage() {
        return controller.getSelectedMessage();
    }

    public boolean isPatientDead() {
        return controller.isDead();
    }

    public Death getDeath() {
        return controller.getDeath();
    }

    public boolean isDiseaseCured() {
        return controller.isDiseaseCured();
    }

    public Disease getCuredDisease() {
        return controller.getCuredDisease();
    }

    public boolean isTransplantReceived() {
        return controller.isTransplantReceived();
    }

    public boolean isRegistrationError() {
        return controller.isRegistrationError();
    }

}
