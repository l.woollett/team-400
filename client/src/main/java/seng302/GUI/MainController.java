package seng302.GUI;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import seng302.GUI.Notifications.YesNoDialog;
import seng302.Model.Hospital;
import seng302.ServerInteracton.ServerQueries.Authentication.AuthToken;
import seng302.ServerInteracton.ServerQueries.Authentication.LogoutQuery;
import seng302.ServerInteracton.ServerQueries.HospitalRequests;
import seng302.Utilities.AutoRequestTimer;
import seng302.Utilities.ProfileDisplayUtility;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

/**
 * Will share common functionality to FXML scenes that inherits this class.
 */
public abstract class MainController {
    private Set<Stage> openedStages = new HashSet<>();
    private final Logger LOGGER = Logger.getLogger(MainController.class.getName());
    protected Stage parentStage;
    protected String stageTitle = "";
    /**
     * Signifies whether timed auto requests should be allowed or blocked.
     */
    protected boolean autoRequestBlocked;
    /**
     * Keep track if the user is currently editing their profiles.
     */
    protected boolean isEditing = false;
    private LocalDateTime lastUpdated = LocalDateTime.now();
    private boolean hasUnsavedChangesIndicator = false;
    private List<Hospital> hospitals;
    // ***************************************************************************************************************\\
    //                                  HANDLES INDICATING ANY UNSAVED CHANGES IN THE TITLE
    // ***************************************************************************************************************\\
    private boolean hasUnsavedChanges = false; // Initially there are no changes to be saved

    /**
     * Sets the autoRequestBlocked variable which is a control flag for the autoRequestTimer. If it is set to true then
     * no automatic updates from the server take place. If it is false then automatic updates will work as normal
     * provided the timer has been created.
     *
     * @param autoRequestBlocked The new value to set for the automatic request timer flag.
     */
    public void setAutoRequestBlocked(boolean autoRequestBlocked) {
        this.autoRequestBlocked = autoRequestBlocked;
    }

    /**
     * Updates the stage using the provided title.
     * <p>
     * Precondition: The reference to the parent stage must already be set.
     */
    private void setStageTitle() {

        resetStageTitle(); // Get the most up to date value

        String dateAndTime = String.format("Last Updated: %s/%s/%s at %s:%s:%s", lastUpdated.getDayOfMonth(),
                lastUpdated.getMonthValue(),
                lastUpdated.getYear(),
                lastUpdated.getHour(),
                lastUpdated.getMinute(),
                lastUpdated.getSecond());

        String indicator = "";
        if (hasUnsavedChangesIndicator) {
            indicator += "*";
        }

        parentStage.setTitle(String.format("%s | %s%s", dateAndTime, stageTitle, indicator));
    }

    public boolean isEditing() {
        return this.isEditing;
    }

    public void setIsEditing(boolean value) {
        this.isEditing = value;
    }

    /**
     * Kicks user out if they are not authenticated
     */
    public void kickUser() {
        AutoRequestTimer.getInstance().stopTimers();
        if (parentStage != null) { // The stage hasn't finished loading yet
            parentStage.setTitle("Login");
        }
        AuthToken.getInstance().clearToken();
        gotoLogin();
    }

    /**
     * Generic error handler for if anything in the application goes wrong. Basically just a more elegant way of
     * handling errors instead of the application crashing. It will log the user out so no harm to the system can
     * be done while it is in an unstable state.
     *
     * @param e Exception Any exception that can be thrown.
     */
    public abstract void handleCriticalError(Exception e);

    /**
     * Will force each controller to define a reference to the parent stage.
     */
    protected abstract void initParentStageReference();

    /**
     * Sets the stage's custom title. e.g. For the clinician stage it will be 'Clinician: {clinician's full name}'
     * Will not add the unsaved changes indicator
     */
    protected abstract void initStageTitle();

    /**
     * Prompts user to confirm if they want to logout. Logging out happens by swapping the current
     * scene with login scene. Closes any opened secondary stages and resets the status of the logged in users.
     */
    @FXML
    protected void logout() {
        if (YesNoDialog.confirmLogout("Confirm Logout", "Are you sure you want to logout?")) {
            LogoutQuery logoutQuery = new LogoutQuery();
            ResponseEntity response = logoutQuery.postLogout(AuthToken.getInstance().getToken());

            if (response.getStatusCode() == HttpStatus.CREATED) {
                AutoRequestTimer.getInstance().stopTimers();
                AuthToken.getInstance().clearToken();
                parentStage.setTitle("Login");
                gotoLogin();
            } else {
                LOGGER.severe("Error logging out");
            }
        }
    }

    /**
     * Swap to the login scene.
     */
    protected void gotoLogin() {
        closeOpenStages();
        try {
            Parent root = FXMLLoader.load(getClass().getResource("/seng302/Main/login.fxml"));
            Scene scene = new Scene(root);
            parentStage.setScene(scene);
            parentStage.setTitle("Login");

            // Used to get rid of the black screen when the user logs out
            parentStage.hide();
            parentStage.show();


            LoginSceneController.resetActiveUsers();
            ProfileDisplayUtility.closeStages();

        } catch (IOException e) {
            LOGGER.severe(e.getMessage());
        }
    }

    /**
     * Updates the title with the astrix if there are unsaved changes.
     *
     * @param showIndicator If true show the indicator
     */
    private void setUnsavedChangesIndicator(boolean showIndicator) {
        // In case the user's name is used as part of the title and has been edited, we will retrieve their most
        // recent applied name
        hasUnsavedChangesIndicator = showIndicator;

    }

    /**
     * Will reset the stage title (i.e. remove the time stamp and the unsaved changes indicator).
     */
    private void resetStageTitle() {
        initStageTitle();
    }

    protected void updateTimeStamp() {
        lastUpdated = LocalDateTime.now();
        setStageTitle();
    }

    /**
     * Used to update the status to show if there are unsaved changes when a Profile has no possibility of changing.
     * This method determines the status based on on the user has clicked on the save option and if the user is
     * currently editing their profile.
     *
     * @param attemptedToSave True if the user has clicked on the Save option.
     * @param isEditing       True if the user is currently editing their profile/in edit mode.
     */
    public void updateUnsavedChangesIndicator(boolean attemptedToSave, boolean isEditing) {
        if (isEditing) {
            setUnsavedChangesIndicator(true);
        } else {
            // If the user is not currently in editing mode and has saved to file then there are no more changes to save
            if (attemptedToSave) {
                hasUnsavedChanges = false;
            }

            setUnsavedChangesIndicator(hasUnsavedChanges);
        }

    }

    /**
     * Used to update the status to show if there are unsaved changes.
     * <p>
     * This method determines the status based on if the user has clicked the save option, if the user is currently
     * editing their profile, and if the profile has any changes.
     *
     * @param attemptedToSave True if the user has clicked on the save option.
     * @param isEditing       True if the user is currently editing their profile.
     * @param hasChanges      True if the there are changes related to the most recent call to this method. Does not
     *                        relate to any prior changes made.
     */
    public void updateUnsavedChangesIndicator(boolean attemptedToSave, boolean isEditing, boolean hasChanges) {
        if (isEditing) {
            // Regardless of what happens, if the user is still currently editing their profile then we should indicate
            // that there are unsaved changes.

            // Do not re-assign a value to hasUnsavedChanges because a previous edit could have not changed anything
            // Do not want to overwrite this even though another edit event occurred
            setUnsavedChangesIndicator(false);
        } else if (attemptedToSave) {
            // Regardless of what happens, if the user has saved (and not currently editing)
            // then there are no unsaved changes
            hasUnsavedChanges = false;
            setUnsavedChangesIndicator(hasUnsavedChanges);

        } else if (hasChanges) {
            // We have detected a change that are unsaved
            hasUnsavedChanges = true;
            setUnsavedChangesIndicator(hasUnsavedChanges);

        } else if (!hasChanges) {
            // Do not re-assign a value to hasUnsavedChanges because a previous edit could have been applied
            // Keep the old title status
            setUnsavedChangesIndicator(hasUnsavedChanges);

        }
    }

    /**
     * Load the hospitals from the database
     */
    protected List<Hospital> loadHospitals() {
        try {
            hospitals = HospitalRequests.getInstance().getHospitals(AuthToken.getInstance().getToken()).getBody();
            assert hospitals != null;
            Collections.sort(hospitals);
        } catch (HttpClientErrorException e) {
            switch (e.getStatusCode()) {
                case UNAUTHORIZED:
                    LOGGER.warning("The user is not authorized to retrieve the hospital list, will kick them" +
                            "out of their session now.");
                    kickUser();
                    break;
                default:
                    // handle critical error when this is implemented by Connor.
                    LOGGER.severe(e.getMessage());
            }
        } catch (HttpServerErrorException | ResourceAccessException | NullPointerException e) {
            LOGGER.severe(e.getMessage());
            // Handle critical error which this is implemented by Connor.
        }


        return hospitals;
    }

    public List<Hospital> getHospitals() {
        if (hospitals == null) {
            LOGGER.severe("The hospitals have not been loaded yet.");
        }

        return hospitals;
    }

    /**
     * Loops through all open stages
     */
    protected void closeOpenStages() {
        for (Stage stage : openedStages) {
            stage.close();
        }
    }

    /**
     * Adds a stage to the set of opened stages.
     *
     * @param stage The stage which we need ot add and keep track of.
     */
    public void addStage(Stage stage) {
        openedStages.add(stage);
    }
}
