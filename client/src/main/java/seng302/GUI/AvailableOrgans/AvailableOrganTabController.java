package seng302.GUI.AvailableOrgans;

import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import seng302.Enum.OrganEnum;
import seng302.GUI.Clinician.ClinicianSceneController;
import seng302.GUI.Profile.OpenProfilePage;
import seng302.Model.AvailableOrgan;
import seng302.Model.Hospital;
import seng302.Model.OrganMatchQueryResult;
import seng302.ServerInteracton.ServerQueries.Authentication.AuthToken;
import seng302.ServerInteracton.ServerQueries.AvailableOrganRequests;
import seng302.ServerInteracton.ServerQueries.HospitalRequests;
import seng302.ServerInteracton.ServerQueries.ProfileRequests;
import seng302.Utilities.AutoRequestTimer;
import seng302.Utilities.ProfileDisplayUtility;

import java.time.LocalDate;
import java.util.*;
import java.util.logging.Logger;

public class AvailableOrganTabController {

    private final Logger LOGGER = Logger.getLogger(getClass().getName());

    @FXML
    private TableColumn usernameColumn, organNameColumn, dateOfDeathColumn, expiryPercentageColumn, boundsColumn, expiryStringColumn, progressColumn;

    @FXML
    private TableColumn receiverUsernameColumn, receiverHospital;

    @FXML
    private TableView<AvailableOrgan> organExpiryTable;
    @FXML
    private TableView<OrganMatchQueryResult> organMatchesTable;
    @FXML
    private AnchorPane availableOrganPane;
    @FXML
    private CheckBox autoRequestCheck;

    private ObservableList<AvailableOrgan> organs = FXCollections.observableArrayList();
    private ObservableList<OrganMatchQueryResult> receivers = FXCollections.observableArrayList();
    private ClinicianSceneController parentController;
    private Hospital hospital;

    private static final double ONE_HUNDRED = 100.0;
    private static final double GREEN_BOUND = 50.0;
    private static final double YELLOW_BOUND = 75.0;
    private static final int BAD_HOSPITAL_ID = -1;

    private final static String LIGHT_GREEN_CSS = "-fx-background-color: linear-gradient(to right, #66BB6A %f%%, white %f%%, white %f%%, black %f%%, black %f%%, white %f%%, white 100%%); -fx-border-color:black; -fx-border-radius: 3;";
    private final static String LIGHT_YELLOW_CSS = "-fx-background-color: linear-gradient(to right, #FFEE58 %f%%, white %f%%, white %f%%, black %f%%, black %f%%, white %f%%, white 100%%); -fx-border-color:black; -fx-border-radius: 3;";
    private final static String LIGHT_RED_CSS = "-fx-background-color: linear-gradient(to right, #EF5350 %f%%, white %f%%, white %f%%, black %f%%, black %f%%, white %f%%, white 100%%); -fx-border-color:black; -fx-border-radius: 3;";
    private final static String DARK_GREEN_CSS = "-fx-background-color: linear-gradient(to right, #66BB6A %f%%, black %f%%, black %f%%, #2E7D32 %f%%, #2E7D32 %f%%, white %f%%, white 100%%); -fx-border-color:black; -fx-border-radius: 3;";
    private final static String DARK_YELLOW_CSS = "-fx-background-color: linear-gradient(to right, #FFEE58 %f%%, black %f%%, black %f%%, #FBC02D %f%%, #FBC02D %f%%, white %f%%, white 100%%); -fx-border-color:black; -fx-border-radius: 3;";
    private final static String DARK_RED_CSS = "-fx-background-color: linear-gradient(to right, #EF5350 %f%%, black %f%%, black %f%%, #C62828 %f%%, #C62828 %f%%, white %f%%, white 100%%); -fx-border-color:black; -fx-border-radius: 3;";
    private final static String LIGHT_RED_NO_LOWER_BOUND_CSS = "-fx-background-color: linear-gradient(to right, #EF5350 %f%%, white %f%%, white 100%%); -fx-border-color:black; -fx-border-radius: 3;";

    @FXML
    private Button usernameSorter;
    @FXML
    private Button organNameSorter;
    @FXML
    private Button dateOfDeathSorter;
    @FXML
    private Button organAgeSorter;
    @FXML
    private Button expiryPercentageSorter;
    @FXML
    private Button expiryBoundsSorter;
    private Button currentSorterButton;

    private List<Button> sorterButtons;
    private List<TableColumn> columns;

    @FXML
    private void initialize() {
        initializeTableRowFactory();

        this.columns = new ArrayList<>(Arrays.asList(usernameColumn, organNameColumn, dateOfDeathColumn, boundsColumn, expiryStringColumn, expiryPercentageColumn, progressColumn));
        this.sorterButtons = new ArrayList<>(Arrays.asList(usernameSorter, organNameSorter, dateOfDeathSorter, organAgeSorter, expiryPercentageSorter, expiryBoundsSorter));

        Platform.runLater(this::postRender);
        Platform.runLater(() -> sortPercentageHandler(TableColumn.SortType.DESCENDING));
        currentSorterButton = expiryPercentageSorter;

        // Disables the default column sorting mechanism
        for (TableColumn column : columns) {
            column.setSortable(false);
        }

        usernameColumn.setCellValueFactory((Callback<TableColumn.CellDataFeatures<AvailableOrgan, String>, ObservableValue<String>>) param ->
                new ReadOnlyObjectWrapper<>(param.getValue().getUsername()));

        dateOfDeathColumn.setCellValueFactory((Callback<TableColumn.CellDataFeatures<AvailableOrgan, LocalDate>, ObservableValue<LocalDate>>) param ->
                new ReadOnlyObjectWrapper<>(param.getValue().getDateOfDeath()));

        organNameColumn.setCellValueFactory((Callback<TableColumn.CellDataFeatures<AvailableOrgan, OrganEnum>, ObservableValue<OrganEnum>>) param ->
                new ReadOnlyObjectWrapper<>(param.getValue().getOrgan()));

        expiryPercentageColumn.setCellValueFactory((Callback<TableColumn.CellDataFeatures<AvailableOrgan, String>, ObservableValue<String>>) param ->
                new ReadOnlyObjectWrapper<>(param.getValue().getExpiryPercentage().toString()));

        expiryStringColumn.setCellValueFactory((Callback<TableColumn.CellDataFeatures<AvailableOrgan, String>, ObservableValue<String>>) param ->
                new ReadOnlyObjectWrapper<>(param.getValue().returnDayHourMinSecondString()));

        boundsColumn.setCellValueFactory((Callback<TableColumn.CellDataFeatures<AvailableOrgan, String>, ObservableValue<String>>) param ->
                new ReadOnlyObjectWrapper<>(param.getValue().returnBoundsAsString()));

        receiverUsernameColumn.setCellValueFactory((Callback<TableColumn.CellDataFeatures<OrganMatchQueryResult, String>, ObservableValue<String>>) param ->
                new ReadOnlyObjectWrapper<>(param.getValue().getUsername()));

        receiverHospital.setCellValueFactory((Callback<TableColumn.CellDataFeatures<OrganMatchQueryResult, String>, ObservableValue<String>>) param ->
                new ReadOnlyObjectWrapper<>(param.getValue().getRecHospitalName()));

        expiryPercentageColumn.setCellFactory(new Callback<TableColumn<AvailableOrgan, String>, TableCell<AvailableOrgan, String>>() {
            @Override
            public TableCell<AvailableOrgan, String> call(TableColumn<AvailableOrgan, String> coverCol) {
                return new TableCell<AvailableOrgan, String>() {
                    @Override
                    public void updateItem(String value, boolean empty) {
                        super.updateItem(value, empty);
                        AvailableOrgan currentOrgan = (AvailableOrgan) this.getTableRow().getItem();
                        if (!empty && currentOrgan != null) {
                            setText("");
                            double percentage;
                            double lowerBound;
                            percentage = ((double) currentOrgan.getSecondsSinceDeath() / (double) currentOrgan.getUpperBoundSeconds()) * ONE_HUNDRED;
                            currentOrgan.setExpiryPercentage(percentage);
                            if (currentOrgan.getUpperBoundSeconds() == AvailableOrgan.NO_EXPIRY) {
                                lowerBound = ONE_HUNDRED;
                            } else {
                                lowerBound = ((double) currentOrgan.getLowerBoundSeconds() / (double) currentOrgan.getUpperBoundSeconds() * ONE_HUNDRED);
                            }

                            double rightFill = ONE_HUNDRED - percentage;

                            if (percentage < GREEN_BOUND) {
                                double greenEnd = ONE_HUNDRED - rightFill;
                                String cssGreen = String.format(LIGHT_GREEN_CSS, greenEnd, greenEnd, lowerBound, lowerBound + 1, lowerBound + 1, lowerBound);
                                if (percentage > lowerBound) {
                                    cssGreen = String.format(DARK_GREEN_CSS, lowerBound, lowerBound, lowerBound + 1, lowerBound + 1, percentage, percentage);
                                }
                                setStyle(cssGreen);
                            } else if (percentage < YELLOW_BOUND) {
                                String cssYellow = String.format(LIGHT_YELLOW_CSS, percentage, percentage, lowerBound, lowerBound, lowerBound + 1, lowerBound + 1);
                                if (percentage > lowerBound) {
                                    cssYellow = String.format(DARK_YELLOW_CSS, lowerBound, lowerBound, lowerBound + 1, lowerBound + 1, percentage, percentage);
                                }
                                setStyle(cssYellow);
                            } else {
                                String cssRed = String.format(LIGHT_RED_CSS, percentage, percentage, rightFill, lowerBound, lowerBound + 1, lowerBound + 1);
                                if (currentOrgan.getUpperBoundSeconds() == currentOrgan.getLowerBoundSeconds()) {
                                    cssRed = String.format(LIGHT_RED_NO_LOWER_BOUND_CSS, percentage, percentage);
                                }
                                if (percentage > lowerBound) {
                                    cssRed = String.format(DARK_RED_CSS, lowerBound, lowerBound, lowerBound + 1, lowerBound + 1, percentage, percentage);
                                }
                                setStyle(cssRed);
                            }
                        }
                    }
                };
            }
        });


        expiryStringColumn.setCellFactory(new Callback<TableColumn<AvailableOrgan, String>, TableCell<AvailableOrgan, String>>() {
            @Override
            public TableCell<AvailableOrgan, String> call(TableColumn<AvailableOrgan, String> coverCol) {
                return new TableCell<AvailableOrgan, String>() {
                    @Override
                    public void updateItem(String value, boolean empty) {
                        super.updateItem(value, empty);
                        AvailableOrgan currentOrgan = (AvailableOrgan) this.getTableRow().getItem();
                        if (!empty && currentOrgan != null) {
                            setText(value);
                        }
                    }
                };
            }
        });

        usernameSorter.setOnMouseClicked(event -> {
            handleSortButtonClicked(usernameColumn, usernameSorter, "Username -", "Username ▲", "Username ▼");
        });
        organNameSorter.setOnMouseClicked(event -> {
            handleSortButtonClicked(organNameColumn, organNameSorter, "Organ Name -", "Organ Name ▲", "Organ Name ▼");
        });
        dateOfDeathSorter.setOnMouseClicked(event -> {
            handleSortButtonClicked(dateOfDeathColumn, dateOfDeathSorter, "Date of Death -", "Date of Death ▲", "Date of Death ▼");
        });
        organAgeSorter.setOnMouseClicked(event -> {
            handleSortButtonClicked(expiryStringColumn, organAgeSorter, "Organ Age -", "Organ Age ▲", "Organ Age ▼");
        });
        expiryPercentageSorter.setOnMouseClicked(event -> {
            handleSortButtonClicked(expiryPercentageColumn, expiryPercentageSorter, "Expiry -", "Expiry ▲", "Expiry ▼");
        });
        expiryBoundsSorter.setOnMouseClicked(event -> {
            handleSortButtonClicked(boundsColumn, expiryBoundsSorter, "Expiry Bounds -", "Expiry Bounds ▲", "Expiry Bounds ▼");
        });

        progressColumn.setCellValueFactory((Callback<TableColumn.CellDataFeatures<AvailableOrgan, Double>, ObservableValue<Double>>) param ->
                new ReadOnlyObjectWrapper<>(param.getValue().getExpiryPercentage()));

        sortPercentageHandler(TableColumn.SortType.DESCENDING);
        AutoRequestTimer.getInstance().createAutoRequestTimer(1000, 1000, this::handleUpdateExpiry);
        AutoRequestTimer.getInstance().createAutoRequestTimer( -1, -1, this::handleAutoRequest);
    }

    /**
     * Sorts the given column and resets all other columns to not be sorted.
     *
     * @param column           The column that need to be sorted. It is where the button is located.
     * @param button           The button in the header of the column passed in.
     * @param defaultHeader    The header displayed when the column is not being sorted.
     * @param ascendingHeader  The header displayed when the column is being sorted in ascending order.
     * @param descendingHeader The header displayed when the column is being sorted in descending order.
     */
    private void handleSortButtonClicked(TableColumn column, Button button, String defaultHeader, String ascendingHeader, String descendingHeader) {
        currentSorterButton = button;

        if (button.getText().equals(defaultHeader)) {
            button.setText(ascendingHeader);
            resetSortButtons(button);
            if (button.equals(expiryPercentageSorter)) {
                sortPercentageHandler(TableColumn.SortType.ASCENDING);
            } else {
                handleOrganSort(TableColumn.SortType.ASCENDING, column);
            }

        } else if (button.getText().equals(ascendingHeader)) {
            button.setText(descendingHeader);
            resetSortButtons(button);
            if (button.equals(expiryPercentageSorter)) {
                sortPercentageHandler(TableColumn.SortType.DESCENDING);
            } else {
                handleOrganSort(TableColumn.SortType.DESCENDING, column);
            }

        } else if (button.getText().equals(descendingHeader)) {
            button.setText(defaultHeader);
        } else {
            LOGGER.severe("Something went wrong...oops.");
        }
    }

    /**
     * Sorts the organs based on the sort type and the column that the button is in.
     *
     * @param st     The sort type of the column.
     * @param column The column that needs to be sorted.
     */
    private void handleOrganSort(TableColumn.SortType st, TableColumn column) {

        if (column.equals(usernameColumn)) {
            organs.sort((o1, o2) -> {
                if (st.equals(TableColumn.SortType.ASCENDING)) {
                    return o1.getUsername().compareTo(o2.getUsername());
                } else {
                    return o2.getUsername().compareTo(o1.getUsername());
                }
            });
        }

        if (column.equals(organNameColumn)) {
            organs.sort((o1, o2) -> {
                if (st.equals(TableColumn.SortType.ASCENDING)) {
                    return o1.getOrgan().toString().compareTo(o2.getOrgan().toString());
                } else {
                    return o2.getOrgan().toString().compareTo(o1.getOrgan().toString());
                }
            });
        }

        if (column.equals(dateOfDeathColumn)) {
            organs.sort((o1, o2) -> {
                if (st.equals(TableColumn.SortType.ASCENDING)) {
                    return o1.getDateOfDeath().compareTo(o2.getDateOfDeath());
                } else {
                    return o2.getDateOfDeath().compareTo(o1.getDateOfDeath());
                }
            });
        }

        if (column.equals(boundsColumn)) {
            organs.sort((o1, o2) -> {
                if (st.equals(TableColumn.SortType.ASCENDING)) {
                    return Long.compare(o1.getUpperBoundSeconds(), o2.getUpperBoundSeconds());
                } else {
                    return Long.compare(o2.getUpperBoundSeconds(), o1.getUpperBoundSeconds());
                }
            });
        }

        if (column.equals(expiryStringColumn)) {
            organs.sort((o1, o2) -> {
                if (st.equals(TableColumn.SortType.ASCENDING)) {
                    return o1.getCurrentSecondsToExpiryAsString().compareTo(o2.getCurrentSecondsToExpiryAsString());
                } else {
                    return o2.getCurrentSecondsToExpiryAsString().compareTo(o1.getCurrentSecondsToExpiryAsString());
                }
            });
        }
    }

    /**
     * Resets all of the buttons in column headers that are not being sorted to have a dash.
     *
     * @param buttonSortToKeep The button that will not be reset as it is the column that is being currently sorted.
     */
    private void resetSortButtons(Button buttonSortToKeep) {
        for (Button button : sorterButtons) {
            if (!button.equals(buttonSortToKeep)) {
                button.setText(button.getText().substring(0, button.getText().length() - 1) + "-");
            }
        }
    }

    /**
     * Performs the necessary sorting actions on the table
     *
     * @param st The sort type that you want to use.
     */
    private void sortPercentageHandler(TableColumn.SortType st) {
        organs.sort((o1, o2) -> {
            if (st.equals(TableColumn.SortType.ASCENDING)) {
                return o1.getExpiryPercentage().compareTo(o2.getExpiryPercentage());
            } else if (st.equals(TableColumn.SortType.DESCENDING)) {
                return o2.getExpiryPercentage().compareTo(o1.getExpiryPercentage());
            } else {
                return -1;
            }
        });
        organExpiryTable.setItems(organs);
    }

    /**
     * Initializes the row properties of the organExpiryTable.
     */
    private void initializeTableRowFactory() {
        organExpiryTable.setRowFactory(tableView -> {
            TableRow<AvailableOrgan> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 1 && (!row.isEmpty())) {
                    AvailableOrgan selected = organs.get(row.getIndex());
                    ArrayList<OrganMatchQueryResult> result = ProfileRequests.getInstance().getMatchingProfiles(AuthToken.getInstance().getToken(),
                            selected.getUsername(), selected.getOrgan().toString()).getBody();
                    if (result == null) {
                        receivers.clear();
                    } else {
                        receivers.clear();
                        receivers.addAll(result);
                        organMatchesTable.setItems(receivers);
                    }

                } else if (event.getClickCount() == ProfileDisplayUtility.DOUBLE_CLICK_COUNT && (!row.isEmpty())) {
                    OpenProfilePage openProfilePage = new OpenProfilePage();
                    openProfilePage.openProfileFromDoubleClick(row.getItem().getUsername());
                }
            });

            return row;
        });

        organMatchesTable.setRowFactory(tableView -> {
            TableRow<OrganMatchQueryResult> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == ProfileDisplayUtility.DOUBLE_CLICK_COUNT && (!row.isEmpty())) {
                    OpenProfilePage openProfilePage = new OpenProfilePage();
                    openProfilePage.openProfileFromDoubleClick(row.getItem().getUsername());
                }
            });

            return row;
        });
    }

    /**
     * Handler for updating the organ expiry time
     *
     * @return Not really sure why this is needed. I am just following what the AutoRequestTimer class uses.
     */
    private Boolean handleUpdateExpiry() {
        Platform.runLater(this::updateExpiryTime);
        return null;
    }

    private Boolean handleAutoRequest() {
        if (autoRequestCheck.isSelected()) {
            Platform.runLater(this::postRender);
        }
        return null;
    }

    /**
     * Update expiry time method we run on each organ in the table which is handled by the handleUpdateExpiry function.
     * Has a test method ot check removal of an item form the table.
     */
    private void updateExpiryTime() {
        removeExpiredOrgans();
        organExpiryTable.setItems(organs);
        organExpiryTable.refresh();
    }

    /**
     * Makes a request to the server to get the current id of the hospital from the name.
     *
     * @param hospitalName The name of the hospital we want to get id of.
     * @return The id of the hospital or BAD_HOSPITAL_ID if something went wrong.
     */
    private Integer getCurrentHospitalId(String hospitalName) {

        try {
            return HospitalRequests.getInstance().getHospitalIdFromName(AuthToken.getInstance().getToken(), hospitalName).getBody();
        } catch (HttpClientErrorException e) {
            switch (e.getStatusCode()) {
                case UNAUTHORIZED:
                    parentController.kickUser();
                    break;
                default:
                    parentController.handleCriticalError(e);
            }
        } catch (HttpServerErrorException | ResourceAccessException | NullPointerException e) {
            parentController.handleCriticalError(e);
        }
        return BAD_HOSPITAL_ID;
    }

    /**
     * Makes a request to the server to get the available organs for a single hospital.
     *
     * @param hospitalId The id of the hospital we want to get the organs from.
     * @return The list of available organs at the hospital. Can be empty.
     */
    private ObservableList<AvailableOrgan> getAvailableOrgans(int hospitalId) {
        ObservableList<AvailableOrgan> organs = FXCollections.observableArrayList(new ArrayList<>());
        try {
            organs = FXCollections.observableArrayList(AvailableOrganRequests.getInstance()
                    .getAvailableOrgans(Integer.toString(hospitalId), AuthToken.getInstance().getToken()).getBody());

            return organs;
        } catch (HttpClientErrorException e) {
            switch (e.getStatusCode()) {
                case UNAUTHORIZED:
                    parentController.kickUser();
                    break;
                case NOT_FOUND:
                    break;
                default:
                    parentController.handleCriticalError(e);
            }
        } catch (HttpServerErrorException | ResourceAccessException e) {
            parentController.handleCriticalError(e);
        }

        return organs;
    }

    /**
     * Sets the parent controller to be the ClinicianSceneController class.
     * This will be used later for the critical error handling.
     *
     * @param clinicianSceneController the parent controller
     */
    public void setParentController(ClinicianSceneController clinicianSceneController) {
        parentController = clinicianSceneController;
    }

    /**
     * Tasks to run after the render has occurred.
     */
    public void postRender() {
        int currentHospitalId = getCurrentHospitalId(parentController.getClinician().getHospital().getName());
        assert currentHospitalId != BAD_HOSPITAL_ID;
        organs = getAvailableOrgans(currentHospitalId);

        removeExpiredOrgans();

        resetSortButtons(expiryPercentageSorter);
        expiryPercentageSorter.setText("Expiry ▼");
        sortPercentageHandler(TableColumn.SortType.DESCENDING);
    }

    /**
     * Stops the timer(s) only if it they have actually been set.
     */
    public void stopTimer() {
        AutoRequestTimer.getInstance().stopTimers();
    }

    /**
     * Removes expired organs.
     */
    private void removeExpiredOrgans() {
        Iterator<AvailableOrgan> organIterator = organs.iterator();
        while (organIterator.hasNext()) {
            AvailableOrgan organ = organIterator.next();
            organ.updateExpiry();
            if (organ.getExpiryPercentage() >= 100) {
                organIterator.remove();
            }
        }
    }

}
