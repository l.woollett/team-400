package seng302.GUI;

import seng302.GUI.Admin.AdminRolesTabController;
import seng302.GUI.Admin.AdminSceneController;
import seng302.GUI.Clinician.ClinicianProfileTabController;
import seng302.GUI.Clinician.ClinicianSceneController;
import seng302.GUI.Clinician.ClinicianSearchTabController;
import seng302.GUI.Clinician.ClinicianTransplantWaitingController;
import seng302.GUI.MedicalHistory.MedicalHistoryTabController;
import seng302.GUI.MedicalHistory.UpdateDiseaseDialogController;
import seng302.GUI.Organ.OrganDonorController;
import seng302.GUI.Procedures.UserMedicalProceduresTabController;
import seng302.GUI.Profile.ProfileSceneController;
import seng302.GUI.Terminal.TerminalSceneController;

/**
 * Give access to other files of controllers they can not normally get.
 */
public class ControllerAccess {
    private static TerminalSceneController terminalSceneController;
    private static ProfileSceneController profileSceneController;
    private static LoginSceneController loginSceneController;
    private static ClinicianProfileTabController clinicianProfileTabController;
    private static ClinicianSceneController clinicianSceneController;
    private static ClinicianSearchTabController clinicianSearchTabController;
    private static UpdateDiseaseDialogController updateDiseaseDialogController;
    private static MedicalHistoryTabController medicalHistoryTabController;
    private static AdminSceneController adminSceneController;
    private static AdminRolesTabController adminRolesTabController;
    private static OrganDonorController organDonorController;
    private static ClinicianTransplantWaitingController clinicianTransplantWaitingController;
    private static UserMedicalProceduresTabController userMedicalProceduresTabController;

    public static AdminSceneController getAdminSceneController() {
        return adminSceneController;
    }

    public static void setAdminSceneController(AdminSceneController adminSceneController) {
        ControllerAccess.adminSceneController = adminSceneController;
    }

    public static AdminRolesTabController getAdminRolesTabController() {
        return adminRolesTabController;
    }

    public static void setAdminRolesTabController(AdminRolesTabController adminRolesTabController) {
        ControllerAccess.adminRolesTabController = adminRolesTabController;
    }

    public static OrganDonorController getOrganDonorController() {
        return organDonorController;
    }

    public static void setOrganDonorController(OrganDonorController organDonorController) {
        ControllerAccess.organDonorController = organDonorController;
    }

    public static ClinicianSceneController getClinicianSceneController() {
        return clinicianSceneController;
    }

    public static void setClinicianSceneController(ClinicianSceneController clinicianSceneController) {
        ControllerAccess.clinicianSceneController = clinicianSceneController;
    }

    public static LoginSceneController getLoginSceneController() {
        return loginSceneController;
    }

    // Login
    public static void setLoginSceneController(LoginSceneController loginSceneController) {
        ControllerAccess.loginSceneController = loginSceneController;
    }


    // Profile

    public static ProfileSceneController getProfileSceneController() {
        return profileSceneController;
    }

    public static void setProfileSceneController(ProfileSceneController profileSceneController) {
        ControllerAccess.profileSceneController = profileSceneController;
    }

    // Terminal

    public static TerminalSceneController getTerminalSceneController() {
        return terminalSceneController;
    }

    public static void setTerminalSceneController(TerminalSceneController controller) {
        terminalSceneController = controller;
    }

    // Clinician

    public static ClinicianProfileTabController getClinicianProfileTabController() {
        return clinicianProfileTabController;
    }

    public static void setClinicianProfileTabController(ClinicianProfileTabController clinicianProfileTabController) {
        ControllerAccess.clinicianProfileTabController = clinicianProfileTabController;
    }

    public static ClinicianSearchTabController getClinicianSearchTabController() {
        return clinicianSearchTabController;
    }

    public static void setClinicianSearchTabController(ClinicianSearchTabController clinicianSearchTabController) {
        ControllerAccess.clinicianSearchTabController = clinicianSearchTabController;
    }

    public static ClinicianTransplantWaitingController getClinicianTransplantWaitingController() {
        return clinicianTransplantWaitingController;
    }

    public static void setClinicianTransplantWaitingController(
            ClinicianTransplantWaitingController clinicianTransplantWaitingController) {
        ControllerAccess.clinicianTransplantWaitingController = clinicianTransplantWaitingController;
    }

    public static UpdateDiseaseDialogController getUpdateDiseaseDialogController() {
        return updateDiseaseDialogController;
    }

    public static void setUpdateDiseaseDialogController(UpdateDiseaseDialogController updateDiseaseDialogController) {
        ControllerAccess.updateDiseaseDialogController = updateDiseaseDialogController;
    }

    public static MedicalHistoryTabController getMedicalHistoryTabController() {
        return medicalHistoryTabController;
    }

    public static void setMedicalHistoryTabController(MedicalHistoryTabController medicalHistoryTabController) {
        ControllerAccess.medicalHistoryTabController = medicalHistoryTabController;
    }

    /**
     * Returns the currently set userMedicalProceduresTabController.
     *
     * @return The currently set userMedicalProceduresTabController.
     */
    public static UserMedicalProceduresTabController getUserMedicalProceduresTabController() {
        return userMedicalProceduresTabController;
    }

    /**
     * Sets the userMedicalProceduresTabController to be the currently open instance of
     * UserMedicalProceduresTabController.
     *
     * @param userMedicalProceduresTabController The controller to be set as userMedicalProceduresTabController.
     */
    public static void setUserMedicalProceduresTabController(
            UserMedicalProceduresTabController userMedicalProceduresTabController) {
        ControllerAccess.userMedicalProceduresTabController = userMedicalProceduresTabController;
    }
}
