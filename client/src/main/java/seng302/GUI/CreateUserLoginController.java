package seng302.GUI;


import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import seng302.Enum.BloodTypeEnum;
import seng302.Enum.GenderEnum;
import seng302.Enum.OrganEnum;
import seng302.Enum.RegionEnum;
import seng302.GUI.Notifications.AlertDialog;
import seng302.Model.Death;
import seng302.Model.Hospital;
import seng302.Model.Profile;
import seng302.ModelController.ProfileController;
import seng302.ModelController.UserController;
import seng302.ServerInteracton.ServerQueries.Authentication.AuthToken;
import seng302.ServerInteracton.ServerQueries.Authentication.Role;
import seng302.ServerInteracton.ServerQueries.ProfileRequests;
import seng302.Utilities.Style;
import seng302.Utilities.Undo_Redo.UndoRedoController;
import seng302.Utilities.Validator;

import java.net.URL;
import java.sql.Time;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

import static seng302.Utilities.Style.applyChoiceBoxStyle;
import static seng302.Utilities.Style.applyTextFieldStyle;
import static seng302.Utilities.Tooltips.*;
import static seng302.Utilities.Validator.*;

public class CreateUserLoginController implements Initializable {

    @FXML
    private AnchorPane createUserPane;

    @FXML
    private TextField usernameTextField;

    @FXML
    private Button createUserButton;

    @FXML
    private ChoiceBox<GenderEnum> genderChoiceBox;

    @FXML
    private DatePicker dateOfBirthDatePicker;

    @FXML
    private TextField firstNameTextField;

    @FXML
    private TextField middleNameTextField;

    @FXML
    private TextField lastNameTextField;

    private boolean isUsernameValid = false;
    private boolean isFirstNameValid = false;
    private boolean isMiddleNameValid = false;
    private boolean isLastNameValid = false;
    private boolean isDateOfBirthValid = false;

    private LoginSceneController parentController;

    /**
     * Calls all initialisation functions for the create user GUI.
     *
     * @param location  Unused parameter present so signature matches.
     * @param resources Unused parameter present so signature matches.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Initialise all GUI elements with tooltips and error checking listeners
        initializeUsernameField();
        initializeFirstNameTextField();
        initializeMiddleNameTextField();
        initializeLastNameTextField();
        initializeDateOfBirthPicker();
        initializeBirthGenderChoiceBox();
    }

    public void setParentController(LoginSceneController parentController){
        this.parentController = parentController;
    }

    /**
     * Creates a user with the currently entered details. It requires at minimum a valid username, first name, and DOB
     * to successfully create a new user. If one of the mandatory fields is missing then the field will become red and
     * an alert box will pop-up telling the user they need to complete all mandatory fields. Will exit the create user
     * stage once the profile was successfully created and added to the rest of the profiles.
     */
    @FXML
    private void createUserButtonClicked() {
        if (isUsernameValid && isFirstNameValid && isDateOfBirthValid) {
            // Mandatory fields
            String username = usernameTextField.getText();
            String firstName = firstNameTextField.getText();
            LocalDate dateOfBirth = dateOfBirthDatePicker.getValue();

            // Note: By default getText() will return "" if no value entered so checks aren't necessary
            String middleName = middleNameTextField.getText();
            String lastName = lastNameTextField.getText();
            Date createdDate = new Date();
            Date modifiedDate = new Date();
            GenderEnum gender = genderChoiceBox.getValue();

            // Create the profile and add it to the loaded profiles
            Profile prof = new Profile(firstName, dateOfBirth, createdDate, username);
            prof.setMiddleName(middleName);
            prof.setLastName(lastName);
            prof.setGender(gender);
            prof.setModifiedDate(modifiedDate);

            HttpStatus statusCode = HttpStatus.OK;
            try {
                ProfileRequests.getInstance().postProfile(prof);
            } catch (HttpClientErrorException e) {
                statusCode = e.getStatusCode();
            }


            if (statusCode == HttpStatus.BAD_REQUEST || statusCode == HttpStatus.INTERNAL_SERVER_ERROR) {
                String title = "More detail required";
                String content = "You must complete all fields / the username is already taken.";
                AlertDialog.showAndWait(title, content);
            } else {
                if (AuthToken.getInstance().getRole() == null) {
                    parentController.postProfileCreation(username);
                    Stage createUserStage = (Stage) createUserPane.getScene().getWindow();
                    createUserStage.close();
                }
            }
        } else {
            AlertDialog.showAndWait("Failure", "You must complete the mandatory fields");
        }
    }


    /**
     * Initialises the username text field. It adds a listener that visually changes the field to show if
     * correct/incorrect data has been input. Also adds an appropriate tooltip to match this.
     */
    private void initializeUsernameField() {
        usernameTextField.setTooltip(createNewTooltip(INVALID_USERNAME_TOOLTIP_TEXT));
        usernameTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    updateUsernameTextFieldStyle();
                }
                ));
    }

    /**
     * Will test to see if the username has a valid value and will apply an appropriate CSS style and tooltip.
     */
    private void updateUsernameTextFieldStyle() {
        String currentValue = usernameTextField.getText();
        isUsernameValid = isValidUsername(currentValue);

        applyTextFieldStyle(usernameTextField, isUsernameValid, false, VALID_USERNAME_TOOLTIP_TEXT,
                INVALID_USERNAME_TOOLTIP_TEXT, currentValue);

        // Check if the username is already used. If it is then apply a tooltip which says it is already taken.
        if (!isUsernameValid && UserController.isUsernameUsed(currentValue)) {
            applyTextFieldStyle(usernameTextField, isUsernameValid, false, VALID_USERNAME_TOOLTIP_TEXT,
                    INVALID_USERNAME_NON_UNIQUE_TOOLTIP_TEXT, currentValue);
        }
    }

    /**
     * Initialises the first name text field. It adds a listener that visually changes the field to show if
     * correct/incorrect data has been input. Also adds an appropriate tooltip to match this.
     */
    private void initializeFirstNameTextField() {
        // Initialise tooltip
        firstNameTextField.setTooltip(createNewTooltip(INVALID_FIRST_NAME_TOOLTIP_TEXT));
        firstNameTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    updateFirstNameTextField();
                })
        );
    }

    /**
     * Will test to see if the first name text field holds a valid value, will then apply an appropriate CSS style and
     * tooltip.
     */
    private void updateFirstNameTextField() {
        String currentValue = firstNameTextField.getText();
        isFirstNameValid = isValidFirstName(currentValue);
        applyTextFieldStyle(firstNameTextField, isFirstNameValid, false, VALID_FIRST_NAME_TOOLTIP_TEXT,
                INVALID_FIRST_NAME_TOOLTIP_TEXT, currentValue);
    }

    /**
     * Initialises the middle name text field. It adds a listener that visually changes the field to show if
     * correct/incorrect data has been input. Also adds an appropriate tooltip to match this. Only accepts middle names
     * that are 1-20 characters long. The middle name can only contain alphabet characters, hyphens, spaces, and
     * apostrophes. Any non-alphabet character must be between two alphabet characters.
     */
    private void initializeMiddleNameTextField() {
        middleNameTextField.setTooltip(createNewTooltip(INVALID_MIDDLE_NAME_TOOLTIP_TEXT));
        middleNameTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    updateMiddleNameTextField();
                })
        );
    }

    /**
     * Will test to see if the current value is valid and will apply an appropriate CSS style and tooltip.
     */
    private void updateMiddleNameTextField() {
        String currentValue = middleNameTextField.getText();
        isMiddleNameValid = isValidMiddleName(currentValue);
        applyTextFieldStyle(middleNameTextField, isMiddleNameValid, true, VALID_MIDDLE_NAME_TOOLTIP_TEXT,
                INVALID_MIDDLE_NAME_TOOLTIP_TEXT, currentValue);
    }

    /**
     * Initialises the last name text field. It adds a listener that visually changes the field to show if
     * correct/incorrect data has been input. Also adds an appropriate tooltip to match this.
     */
    private void initializeLastNameTextField() {
        // Initialise tooltip
        lastNameTextField.setTooltip(createNewTooltip(INVALID_LAST_NAME_TOOLTIP_TEXT));
        lastNameTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    updateLastNameTextField();
                })
        );
    }

    /**
     * Will test to see if the current value for the last name is valid and will apply an appropriate CSS style and
     * tooltip.
     */
    private void updateLastNameTextField() {
        String currentValue = lastNameTextField.getText();
        isLastNameValid = isValidLastName(currentValue);
        applyTextFieldStyle(lastNameTextField, isLastNameValid, true, VALID_LAST_NAME_TOOLTIP_TEXT,
                INVALID_LAST_NAME_TOOLTIP_TEXT, currentValue);
    }

    /**
     * Populates the birth gender choice box with all possible genders and adds a listener that makes the box turn green
     * to indicate a birth gender has been selected.
     */
    private void initializeBirthGenderChoiceBox() {
        genderChoiceBox.setItems(FXCollections.observableArrayList(
                GenderEnum.NOT_SET, GenderEnum.MALE, GenderEnum.FEMALE));
        genderChoiceBox.valueProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    updateBirthGenderChoiceBox();
                })
        );
        genderChoiceBox.setValue(GenderEnum.NOT_SET);
    }

    /**
     * Test to see if the value is valid and set the appropriate CSS style and tooltip.
     */
    private void updateBirthGenderChoiceBox() {
        Object currentValue = genderChoiceBox.getValue();
        applyChoiceBoxStyle(genderChoiceBox, true, true, currentValue);
    }

    /**
     * Set the DOB date picker to only allow selection through the built in calendar and sets the default date to be
     * 01-01-1990. Also adds live error checking and visual feedback through a listener. DOB bust be between 01-01-1900
     * and today's date.
     */
    private void initializeDateOfBirthPicker() {
        // Create default date for the date of birth date picker
        LocalDate defaultDate = LocalDate.parse("01-01-1990", DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        Style.applyDatePickerStyle(dateOfBirthDatePicker, true, false, VALID_DATE_OF_BIRTH_TOOLTIP_TEXT, INVALID_DATE_OF_BIRTH_TOOLTIP_TEXT, defaultDate);

        isDateOfBirthValid = true; // We are initializing the date of birth to a valid date. So set isValid to be true
        dateOfBirthDatePicker.setTooltip(createNewTooltip(INVALID_DATE_OF_BIRTH_TOOLTIP_TEXT));

        // Set value after the listener was created so the date picker is green showing the default date of birth is valid.
        if (dateOfBirthDatePicker.getValue() == null) {
            dateOfBirthDatePicker.setValue(defaultDate);
        }

        dateOfBirthDatePicker.valueProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    if(!isValidDateOfBirth(newValue)){
                        isDateOfBirthValid = false;
                        Style.applyDatePickerStyle(dateOfBirthDatePicker, false, false, VALID_DATE_OF_BIRTH_TOOLTIP_TEXT, INVALID_DATE_OF_BIRTH_TOOLTIP_TEXT, newValue);
                    }else{
                        isDateOfBirthValid = true;
                        Style.applyDatePickerStyle(dateOfBirthDatePicker, true, false, VALID_DATE_OF_BIRTH_TOOLTIP_TEXT, INVALID_DATE_OF_BIRTH_TOOLTIP_TEXT, newValue);
                    }
                })
        );
    }
}
