package seng302.GUI.Map;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.javascript.event.UIEventType;
import com.lynden.gmapsfx.javascript.object.*;
import com.lynden.gmapsfx.service.directions.*;
import com.lynden.gmapsfx.shapes.Polyline;
import com.lynden.gmapsfx.shapes.PolylineOptions;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import netscape.javascript.JSObject;
import seng302.API.GoogleDistanceMatrix;
import seng302.Model.Hospital;
import seng302.ServerInteracton.ServerQueries.Authentication.AuthToken;
import seng302.ServerInteracton.ServerQueries.HospitalRequests;
import seng302.Utilities.Haversine;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Objects;
import java.util.ResourceBundle;

/**
 * The Google Maps controller
 * This is the map that we use to show hospitals and their information.
 */
public class GMapController implements Initializable, DirectionsServiceCallback {

    public static final double HELICOPTER_SPEED_KM_PER_HOUR = 222;


    protected StringProperty from = new SimpleStringProperty();
    protected StringProperty to = new SimpleStringProperty();
    //Directions shit we need
    private DirectionsService directionsService;
    private DirectionsPane directionsPane;
    private DirectionsRenderer directionsRenderer = null;
    private GoogleMap map;
    private ArrayList<Hospital> hospitalArrayList = new ArrayList<>();
    private Hospital hospital1;
    private Hospital hospital2;

    private DecimalFormat df = new DecimalFormat("####0.00");
    private Polyline helicopterRoute;

    @FXML
    private GoogleMapView googleMapView;

    @FXML
    private TextArea hospital1Details, hospital2Details, travelDetails;

    @FXML
    private CheckBox isHelicopter;

    @FXML
    private Label h1Clear, h2Clear;

    /**
     * The initial call of this class, the URL and Resource bundle aren't used by us.
     *
     * @param url a url
     * @param rb  A resource bundle.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        googleMapView.addMapInializedListener(this::configureMap);
        googleMapView.setKey("AIzaSyB4pl6ve_wkVh3aow66r8OvxVqW0Eqwf1U");
        hospital1Details.setWrapText(true);
        hospital2Details.setWrapText(true);
        travelDetails.setWrapText(true);
        hospitalArrayList.addAll(Objects.requireNonNull(
                HospitalRequests.getInstance().getHospitals(AuthToken.getInstance().getToken()).getBody()));

        //==================================//
        //   Onclick Listeners for things   //
        //==================================//

        h1Clear.setOnMouseClicked((MouseEvent event) -> {
            hospital1 = null;
            hospital1Details.setText("");
            configureMap(map.getCenter(), map.getZoom());
            if (hospital1 == null && hospital2 == null) {
                travelDetails.setText("");
            }
        });


        h2Clear.setOnMouseClicked(event -> {
            hospital2 = null;
            hospital2Details.setText("");
            configureMap(map.getCenter(), map.getZoom());
            if (hospital1 == null && hospital2 == null) {
                travelDetails.setText("");
            }
        });

        isHelicopter.selectedProperty().addListener(event -> {
            if (isHelicopter.isSelected()) {
                directionsRenderer.clearDirections();
            } else {
                directionsRenderer.setMap(map);
                if (helicopterRoute != null) {
                    map.removeMapShape(helicopterRoute);
                }
            }

            if (hospital1 != null && hospital2 != null) {
                sendRequest(hospital1, hospital2);
            }
        });
    }

    /**
     * Our own way of clearing the map since ClearDirections currently breaks the directionsRenderer and you cannot
     * add routes anymore
     *
     * @param centre The centre of the map currently
     * @param zoom   The current zoom level
     */
    private void configureMap(LatLong centre, int zoom) {
        MapOptions mapOptions = new MapOptions();

        mapOptions.center(centre)
                .mapType(MapTypeIdEnum.ROADMAP)
                .zoom(zoom)
                .overviewMapControl(false)
                .panControl(false)
                .rotateControl(false)
                .streetViewControl(false)
                .zoomControl(false)
                .scaleControl(false)
                .mapTypeControl(false);
        map = googleMapView.createMap(mapOptions, false);
        directionsService = new DirectionsService();
        directionsPane = googleMapView.getDirec();
        directionsRenderer = new DirectionsRenderer(true, googleMapView.getMap(), directionsPane);
        configureHospitals(map);
    }

    /**
     * This is called after the map is Initialized, we could also call it after
     * the map is loaded, but this way we dont get pins jumping up after it loads,
     * instead doing it while there is a blank screen.
     */
    private void configureMap() {
        MapOptions mapOptions = new MapOptions();
        LatLong newZealand = new LatLong(-42.098786, 173.420904);

        mapOptions.center(newZealand)
                .mapType(MapTypeIdEnum.ROADMAP)
                .zoom(5)
                .overviewMapControl(false)
                .panControl(false)
                .rotateControl(false)
                .streetViewControl(false)
                .zoomControl(false)
                .scaleControl(false)
                .mapTypeControl(false);
        map = googleMapView.createMap(mapOptions, false);
        directionsService = new DirectionsService();
        directionsPane = googleMapView.getDirec();
        directionsRenderer = new DirectionsRenderer(true, googleMapView.getMap(), directionsPane);
        configureHospitals(map);
    }

    /**
     * So for every hospital1 that we get back from the server, we want to add it's pin to the map.
     * We get it's lat and long from the model to set its location, and its name to set a title for the
     * pin.
     * <p>
     * We add a onclick handler to it so that when it is clicked, we show a popup with the name and address.
     *
     * @param map The map that we're using
     */
    private void configureHospitals(GoogleMap map) {
        for (Hospital hospital : hospitalArrayList) {
            MarkerOptions markerOptions = new MarkerOptions();
            LatLong hospitalLocation = new LatLong(hospital.getLatitude(), hospital.getLongitude());
            markerOptions.position(hospitalLocation)
                    .visible(true)
                    .title(hospital.getName())
                    .icon("https://i.imgur.com/gMuWBGP.png");

            if (hospital1 == hospital || hospital2 == hospital) {
                markerOptions.icon("https://i.imgur.com/EzJYipr.png");
            }

            Marker hospitalMarker = new Marker(markerOptions);
            map.addUIEventHandler(hospitalMarker, UIEventType.rightclick, (JSObject obj) -> {
                InfoWindowOptions infoWindowOptions = new InfoWindowOptions();
                infoWindowOptions.content(MessageFormat.format("<H2>{0}</H2>Address: {3} <br>Longitute: {2}<br>Latitude: {1}",
                        hospital.getName(), hospital.getLatitude(), hospital.getLongitude(), hospital.getAddress()));
                InfoWindow hospitalInfo = new InfoWindow(infoWindowOptions);
                hospitalInfo.open(map, hospitalMarker);
            });


            map.addUIEventHandler(hospitalMarker, UIEventType.click, (JSObject obj) -> {
                if (this.hospital1 == null && hospital != hospital2) {
                    markerOptions.icon("https://i.imgur.com/EzJYipr.png");
                    hospitalMarker.setOptions(markerOptions);
                    this.hospital1 = hospital;
                    hospital1Details.setText(hospital.getName() + "\n" + hospital.getAddress() + "\n" + hospital.getRegion());
                    if (hospital2 != null) {
                        sendRequest(this.hospital1, hospital2);
                    }

                } else if (hospital != hospital1 && hospital2 == null) {
                    markerOptions.icon("https://i.imgur.com/EzJYipr.png");
                    hospitalMarker.setOptions(markerOptions);
                    hospital2 = hospital;
                    hospital2Details.setText(hospital.getName() + "\n" + hospital.getAddress() + "\n" + hospital.getRegion());
                    if (this.hospital1 != null) {
                        sendRequest(this.hospital1, hospital2);
                    }
                }
            });

            map.addMarker(hospitalMarker);
        }
    }

    /**
     * The button to clear the map and markers
     *
     * @param event OnClick event from clicking the button.
     */
    @FXML
    private void clearDirections(ActionEvent event) {
        hospital1 = null;
        hospital2 = null;
        hospital1Details.setText("");
        hospital2Details.setText("");
        travelDetails.setText("");
        configureMap();
    }

    /**
     * Function for staring the calculations and displaying the directions between 2 hospitals.
     *
     * @param from The hospital1 that you're coming from
     * @param to   The hospital1 that you're going to.
     *             See also directionsRecieved, as this is called when we get a result.
     */
    private void sendRequest(Hospital from, Hospital to) {
        final double MINUTES_IN_AN_HOUR = 60.0;

        boolean isHelicopterTrip = isHelicopter.isSelected();

        if (!isHelicopterTrip) {
            DirectionsRequest request = new DirectionsRequest(from.getAddress(), to.getAddress(), TravelModes.DRIVING);
            directionsService.getRoute(request, this, directionsRenderer);
        } else {
            if (helicopterRoute != null) {
                map.removeMapShape(helicopterRoute);
            }

            Double distanceKm = new Haversine().calcDistance(hospital1.getLatitude(), hospital1.getLongitude(),
                    hospital2.getLatitude(), hospital2.getLongitude());
            Double duration = distanceKm / HELICOPTER_SPEED_KM_PER_HOUR * MINUTES_IN_AN_HOUR;
            displayTravelDetails(hospital1, hospital2, df.format(distanceKm), df.format(duration));


            LatLong origin = new LatLong(hospital1.getLatitude(), hospital1.getLongitude());
            LatLong destination = new LatLong(hospital2.getLatitude(), hospital2.getLongitude());
            LatLong[] coordinates = new LatLong[]{origin, destination};
            MVCArray points = new MVCArray(coordinates);
            PolylineOptions polyOpts = new PolylineOptions()
                    .path(points).strokeColor("blue").strokeWeight(3);

            helicopterRoute = new Polyline(polyOpts);

            map.addMapShape(helicopterRoute);
        }
    }

    /**
     * This method is called when a request comes back with a result. Used to displaying the results if the mode of
     * transportation was driving.
     * <p>
     * We use this to populate the travel details box
     *
     * @param directionsResult The whole result
     * @param directionStatus  Ok or fail
     */
    @Override
    public void directionsReceived(DirectionsResult directionsResult, DirectionStatus directionStatus) {
        DirectionsLeg ourRoute = directionsResult.getRoutes().get(0).getLegs().get(0);
        String time = timeToTravel(hospital1, hospital2);
        String distanceKms = Double.toString(ourRoute.getDistance().getValue() / 1000);
        displayTravelDetails(hospital1, hospital2, distanceKms, time);

    }

    /**
     * Shows the travel details in the traveldetails text box
     *
     * @param hospital1 The first hospital
     * @param hospital2 The second hospital
     * @param distance  Distance between the two hospitals
     * @param duration  The time to travel the aforementioned distance
     */
    private void displayTravelDetails(Hospital hospital1, Hospital hospital2, String distance, String duration) {
        String travel = MessageFormat.format("Travel Details Between {0} and {1}:\nDistance: {2} km\nTravel Time: {3} minutes",
                hospital1.getName(), hospital2.getName(), distance, duration);
        travelDetails.setText(travel);
    }

    /**
     * Calculation for time to travel from point A to point B
     *
     * @param origin      The starting hospital for the destination
     * @param destination The hospital you finish the travel at
     * @return A time in minutes that the trip will take.
     */
    private String timeToTravel(Hospital origin, Hospital destination) {
        final double SECONDS_IN_MINUTE = 60;

        try {
            double duration = new GoogleDistanceMatrix().getDuration(origin, destination);
            return df.format(duration / SECONDS_IN_MINUTE);
        } catch (Exception e) {
            return "N/A";
        }
    }
}
