package seng302.GUI;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import seng302.Enum.BloodTypeEnum;
import seng302.Enum.GenderEnum;
import seng302.Enum.OrganEnum;
import seng302.Enum.RegionEnum;
import seng302.GUI.Notifications.AlertDialog;
import seng302.Model.Death;
import seng302.Model.Hospital;
import seng302.Model.Profile;
import seng302.ModelController.ProfileController;
import seng302.ModelController.UserController;
import seng302.ServerInteracton.ServerQueries.Authentication.AuthToken;
import seng302.ServerInteracton.ServerQueries.Authentication.Role;
import seng302.ServerInteracton.ServerQueries.HospitalRequests;
import seng302.ServerInteracton.ServerQueries.ProfileRequests;
import seng302.Utilities.Undo_Redo.*;

import java.net.URL;
import java.sql.Time;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Logger;

import static seng302.Utilities.Style.*;
import static seng302.Utilities.Tooltips.*;
import static seng302.Utilities.Validator.*;

/**
 * Controls all functionality of the create a new user GUI interface. This includes error checking and validation.
 */
public class CreateUserController implements Initializable {

    @FXML
    private AnchorPane createUserPane;

    @FXML
    private CheckBox boneCheckBox, boneMarrowCheckBox, connectiveTissueCheckBox, corneaCheckBox, heartCheckBox,
            heartValveCheckBox, intestineCheckBox, kidneyCheckBox, liverCheckBox, lungCheckBox, middleEarCheckBox,
            pancreasCheckBox, skinCheckBox;

    @FXML
    private ChoiceBox<Enum> regionChoiceBox, genderChoiceBox, birthGenderChoiceBox, bloodTypeChoiceBox;

    @FXML
    private ChoiceBox<Hospital> hospitalOfDeathChoiceBox;

    @FXML
    private DatePicker dateOfBirthDatePicker, dateOfDeathDatePicker;

    @FXML
    private TextField usernameTextField, aliasTextField, firstNameTextField, middleNameTextField, lastNameTextField,
            heightTextField, weightTextField, addressTextField, hrTimeOfDeathTextField, minTimeOfDeathTextField, secTimeOfDeathTextField;

    @FXML
    private Button createUserUndo, createUserRedo, clearDateOfDeathButton, createUserButton, cancelButton;

    @FXML
    private Text dateOfDeathText, hospitalOfDeathText, timeOfDeathText, separatorTimeOfDeathText, separator2TimeOfDeathText;

    private boolean isUsernameValid, isAliasValid, isFirstNameValid, isMiddleNameValid, isLastNameValid,
            isDateOfBirthValid, isDeathDetailsValid, isHeightValid, isWeightValid, isAddressValid, undoRedoEvent = true, nothingSet;

    private List<Hospital> hospitals = new ArrayList<>();

    /**
     * Reference to the LoginSceneController
     **/
    private LoginSceneController loginController;
    private UndoRedoController undoRedoController;

    private static final Logger LOGGER = Logger.getLogger(CreateUserController.class.getName());

    public CreateUserController() {
        //Constructor
    }

    /**
     * Calls all initialisation functions for the create user GUI.
     *
     * @param location  Unused parameter present so signature matches.
     * @param resources Unused parameter present so signature matches.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Initialise all GUI elements with tooltips and error checking listeners
        this.undoRedoController = new UndoRedoController();
        undoRedoEvent = true;
        initializeUsernameField();
        initializeAliasTextField();
        initializeFirstNameTextField();
        initializeMiddleNameTextField();
        initializeLastNameTextField();
        initializeHeightTextField();
        initializeWeightTextField();
        initializeAddressTextField();
        initializeDateOfBirthPicker();
        initializeGenderChoiceBox();
        initializeHrTimeOfDeathField();
        initializeMinTimeOfDeathField();
        initializeSecTimeOfDeathField();

        Role role = AuthToken.getInstance().getRole();
        if (role == Role.ADMIN || role == Role.CLINICIAN) {
            initializeDeathNodes();
        } else {
            dateOfDeathDatePicker.setVisible(false);
            hospitalOfDeathChoiceBox.setVisible(false);
        }
        initializeBirthGenderChoiceBox();
        initializeRegionChoiceBox();
        initializeBloodTypeChoiceBox();
        initializeCheckBoxes();
        updateUndoRedoButtons();
        undoRedoEvent = false;
    }

    /**
     * Initialises the time text field. It adds a listener that visually changes the field to show if
     * correct/incorrect data has been input. Also adds an appropriate tooltip to match this.
     */
    private void initializeHrTimeOfDeathField() {
        hrTimeOfDeathTextField.setTooltip(createNewTooltip(INVALID_HR_TIME_OF_DEATH_TOOLTIP_TEXT));
        hrTimeOfDeathTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    setDeathCSS();
                    if (!oldValue.equals(newValue) && !undoRedoEvent) {
                        addTextToDeque(hrTimeOfDeathTextField, oldValue);
                    }
                }
                ));
    }

    /**
     * Initialises the time text field. It adds a listener that visually changes the field to show if
     * correct/incorrect data has been input. Also adds an appropriate tooltip to match this.
     */
    private void initializeMinTimeOfDeathField() {
        minTimeOfDeathTextField.setTooltip(createNewTooltip(INVALID_MIN_TIME_OF_DEATH_TOOLTIP_TEXT));
        minTimeOfDeathTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    setDeathCSS();
                    if (!oldValue.equals(newValue) && !undoRedoEvent) {
                        addTextToDeque(minTimeOfDeathTextField, oldValue);
                    }
                }
                ));
    }

    /**
     * Initialises the time text field. It adds a listener that visually changes the field to show if
     * correct/incorrect data has been input. Also adds an appropriate tooltip to match this.
     */
    private void initializeSecTimeOfDeathField() {
        secTimeOfDeathTextField.setTooltip(createNewTooltip(INVALID_SEC_TIME_OF_DEATH_TOOLTIP_TEXT));
        secTimeOfDeathTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    setDeathCSS();
                    if (!oldValue.equals(newValue) && !undoRedoEvent) {
                        addTextToDeque(secTimeOfDeathTextField, oldValue);
                    }
                }
                ));
    }


    /**
     * Initialises the username text field. It adds a listener that visually changes the field to show if
     * correct/incorrect data has been input. Also adds an appropriate tooltip to match this.
     */
    private void initializeUsernameField() {
        // Initialise tooltip
        usernameTextField.setTooltip(createNewTooltip(INVALID_USERNAME_TOOLTIP_TEXT));
        usernameTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    updateUsernameTextFieldStyle();
                    if (!oldValue.equals(newValue) && !undoRedoEvent) {
                        addTextToDeque(usernameTextField, oldValue);
                    }
                }
                ));
    }

    /**
     * Checks the 3 time fields, the dod picker and the hospital choice box
     * and sets the appropriate CSS styling.
     */
    private void setDeathCSS() {

        LocalDate currentValue = dateOfDeathDatePicker.getValue();
        boolean hospitalSet = hospitalOfDeathChoiceBox.getValue() != null;

        String currentHrValue = hrTimeOfDeathTextField.getText();
        String currentMinValue = minTimeOfDeathTextField.getText();
        String currentSecValue = secTimeOfDeathTextField.getText();
        boolean currentDOBSet = dateOfBirthDatePicker.getValue() != null;

        nothingSet = (currentValue == null &&
                !hospitalSet &&
                currentHrValue.equals("") &&
                currentMinValue.equals("") &&
                currentSecValue.equals("")) &&
                currentDOBSet;

        LocalDate currentDate = LocalDate.now();
        boolean validDate = true;
        boolean validHour = false;
        boolean validMinute = false;
        boolean validSecond = false;

        if (nothingSet) {
            dateOfDeathDatePicker.setValue(null);
            dateOfDeathDatePicker.getStylesheets().clear();
            hospitalOfDeathChoiceBox.setValue(null);
            hospitalOfDeathChoiceBox.getStylesheets().clear();
            hrTimeOfDeathTextField.getStylesheets().clear();
            minTimeOfDeathTextField.getStylesheets().clear();
            secTimeOfDeathTextField.getStylesheets().clear();
            dateOfBirthDatePicker.getStylesheets().clear();
        } else {

            if (dateOfDeathDatePicker.getValue() != null) {
                validDate = ((dateOfDeathDatePicker.getValue().compareTo(dateOfBirthDatePicker.getValue()) >= 0) && (dateOfDeathDatePicker.getValue().compareTo(currentDate) <= 0));

                applyDatePickerStyle(dateOfDeathDatePicker, validDate, false, VALID_DATE_OF_DEATH_TOOLTIP_TEXT,
                        INVALID_DATE_OF_DEATH_TOOLTIP_TEXT, currentValue);
            } else {
                applyDatePickerStyle(dateOfDeathDatePicker, false, false, VALID_DATE_OF_DEATH_TOOLTIP_TEXT,
                        INVALID_DATE_OF_DEATH_TOOLTIP_TEXT, currentValue);
                validDate = false;
            }


            applyChoiceBoxStyle(hospitalOfDeathChoiceBox, hospitalSet, false, hospitalOfDeathChoiceBox.getValue());

            validHour = isValidHourTimeOfDeath(currentHrValue);
            applyTextFieldStyle(hrTimeOfDeathTextField, validHour, false, VALID_HR_TIME_OF_DEATH_TOOLTIP_TEXT,
                    INVALID_HR_TIME_OF_DEATH_TOOLTIP_TEXT, currentHrValue);

            validMinute = isValidMinuteTimeOfDeath(currentMinValue);
            applyTextFieldStyle(minTimeOfDeathTextField, validMinute, false, VALID_MIN_TIME_OF_DEATH_TOOLTIP_TEXT,
                    INVALID_MIN_TIME_OF_DEATH_TOOLTIP_TEXT, currentMinValue);

            validSecond = isValidSecondTimeOfDeath(currentSecValue);
            applyTextFieldStyle(secTimeOfDeathTextField, validSecond, false, VALID_SEC_TIME_OF_DEATH_TOOLTIP_TEXT,
                    INVALID_SEC_TIME_OF_DEATH_TOOLTIP_TEXT, currentSecValue);


        }
        isDeathDetailsValid = (validDate && hospitalSet && validHour && validMinute && validSecond);
    }

    /**
     * Will test to see if the username has a valid value and will apply an appropriate CSS style and tooltip.
     */
    private void updateUsernameTextFieldStyle() {
        String currentValue = usernameTextField.getText();
        isUsernameValid = isValidUsername(currentValue);

        applyTextFieldStyle(usernameTextField, isUsernameValid, false, VALID_USERNAME_TOOLTIP_TEXT,
                INVALID_USERNAME_TOOLTIP_TEXT, currentValue);

        // Check if the username is already used. If it is then apply a tooltip which says it is already taken.
        if (!isUsernameValid && UserController.isUsernameUsed(currentValue)) {
            applyTextFieldStyle(usernameTextField, isUsernameValid, false, VALID_USERNAME_TOOLTIP_TEXT,
                    INVALID_USERNAME_NON_UNIQUE_TOOLTIP_TEXT, currentValue);
        }
    }

    /**
     * Initialises the alias name text field. It adds a listener that visually changes the field to show if
     * correct/incorrect data has been input. Also adds an appropriate tooltip to match this. Only accepts middle names
     * that are 1-20 characters long. The middle name can only contain alphabet characters, hyphens, spaces, and
     * apostrophes. Any non-alphabet character must be between two alphabet characters.
     */
    private void initializeAliasTextField() {
        aliasTextField.setTooltip(createNewTooltip(INVALID_MIDDLE_NAME_TOOLTIP_TEXT));
        aliasTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    updateAliasTextField();
                    if (!oldValue.equals(newValue) && !undoRedoEvent) {
                        addTextToDeque(aliasTextField, oldValue);
                    }
                })
        );
    }

    /**
     * Will test to see if the current value is valid and will apply an appropriate CSS style and tooltip.
     */
    private void updateAliasTextField() {
        String currentValue = aliasTextField.getText();
        isAliasValid = isValidAlias(currentValue);
        applyTextFieldStyle(aliasTextField, isAliasValid, true, VALID_ALIAS_TOOLTIP_TEXT,
                INVALID_ALIAS_TOOLTIP_TEXT, currentValue);
    }

    /**
     * Initialises the first name text field. It adds a listener that visually changes the field to show if
     * correct/incorrect data has been input. Also adds an appropriate tooltip to match this.
     */
    private void initializeFirstNameTextField() {
        // Initialise tooltip
        firstNameTextField.setTooltip(createNewTooltip(INVALID_FIRST_NAME_TOOLTIP_TEXT));
        firstNameTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    updateFirstNameTextField();
                    if (!oldValue.equals(newValue) && !undoRedoEvent) {
                        addTextToDeque(firstNameTextField, oldValue);
                    }
                })
        );
    }

    /**
     * Will test to see if the first name text field holds a valid value, will then apply an appropriate CSS style and
     * tooltip.
     */
    private void updateFirstNameTextField() {
        String currentValue = firstNameTextField.getText();
        isFirstNameValid = isValidFirstName(currentValue);
        applyTextFieldStyle(firstNameTextField, isFirstNameValid, false, VALID_FIRST_NAME_TOOLTIP_TEXT,
                INVALID_FIRST_NAME_TOOLTIP_TEXT, currentValue);
    }

    /**
     * Initialises the middle name text field. It adds a listener that visually changes the field to show if
     * correct/incorrect data has been input. Also adds an appropriate tooltip to match this. Only accepts middle names
     * that are 1-20 characters long. The middle name can only contain alphabet characters, hyphens, spaces, and
     * apostrophes. Any non-alphabet character must be between two alphabet characters.
     */
    private void initializeMiddleNameTextField() {
        middleNameTextField.setTooltip(createNewTooltip(INVALID_MIDDLE_NAME_TOOLTIP_TEXT));
        middleNameTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    updateMiddleNameTextField();
                    if (!oldValue.equals(newValue) && !undoRedoEvent) {
                        addTextToDeque(middleNameTextField, oldValue);
                    }
                })
        );
    }

    /**
     * Will test to see if the current value is valid and will apply an appropriate CSS style and tooltip.
     */
    private void updateMiddleNameTextField() {
        String currentValue = middleNameTextField.getText();
        isMiddleNameValid = isValidMiddleName(currentValue);
        applyTextFieldStyle(middleNameTextField, isMiddleNameValid, true, VALID_MIDDLE_NAME_TOOLTIP_TEXT,
                INVALID_MIDDLE_NAME_TOOLTIP_TEXT, currentValue);
    }

    /**
     * Initialises the last name text field. It adds a listener that visually changes the field to show if
     * correct/incorrect data has been input. Also adds an appropriate tooltip to match this.
     */
    private void initializeLastNameTextField() {
        // Initialise tooltip
        lastNameTextField.setTooltip(createNewTooltip(INVALID_LAST_NAME_TOOLTIP_TEXT));
        lastNameTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    updateLastNameTextField();
                    if (!oldValue.equals(newValue) && !undoRedoEvent) {
                        addTextToDeque(lastNameTextField, oldValue);
                    }
                })
        );
    }

    /**
     * Will test to see if the current value for the last name is valid and will apply an appropriate CSS style and
     * tooltip.
     */
    private void updateLastNameTextField() {
        String currentValue = lastNameTextField.getText();
        isLastNameValid = isValidLastName(currentValue);
        applyTextFieldStyle(lastNameTextField, isLastNameValid, true, VALID_LAST_NAME_TOOLTIP_TEXT,
                INVALID_LAST_NAME_TOOLTIP_TEXT, currentValue);
    }

    /**
     * Initialises the height text field. It adds a listener that visually changes the field to show if
     * correct/incorrect data has been input. Also adds an appropriate tooltip to match this.
     */
    private void initializeHeightTextField() {
        // Initialise tooltip
        heightTextField.setTooltip(createNewTooltip(INVALID_HEIGHT_TOOLTIP_TEXT));
        heightTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    updateHeightTextField();
                    if (!oldValue.equals(newValue) && !undoRedoEvent) {
                        addTextToDeque(heightTextField, oldValue);
                    }
                })
        );
    }

    /**
     * Test to see if the current value for the height is valid and will apply an appropriate CSS style and tooltip.
     */
    private void updateHeightTextField() {
        String currentValue = heightTextField.getText();
        isHeightValid = isValidHeight(currentValue);
        applyTextFieldStyle(heightTextField, isHeightValid, true, VALID_HEIGHT_TOOLTIP_TEXT,
                INVALID_HEIGHT_TOOLTIP_TEXT, currentValue);
    }

    /**
     * Initialises the weight text field. It adds a listener that visually changes the field to show if
     * correct/incorrect data has been input. Also adds an appropriate tooltip to match this.
     */
    private void initializeWeightTextField() {
        // Initialise tooltip
        weightTextField.setTooltip(createNewTooltip(INVALID_WEIGHT_TOOLTIP_TEXT));
        weightTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    updateWeightTextField();
                    if (!oldValue.equals(newValue) && !undoRedoEvent) {
                        addTextToDeque(weightTextField, oldValue);
                    }
                })
        );
    }

    /**
     * Will test to see if the current value is valid and will apply an appropriate CSS style and tooltip.
     */
    private void updateWeightTextField() {
        String currentValue = weightTextField.getText();
        isWeightValid = isValidWeight(currentValue);
        applyTextFieldStyle(weightTextField, isWeightValid, true, VALID_WEIGHT_TOOLTIP_TEXT,
                INVALID_WEIGHT_TOOLTIP_TEXT, currentValue);
    }

    /**
     * Initialises the address text field. It adds a listener that visually changes the field to show if
     * correct/incorrect data has been input. Also adds an appropriate tooltip to match this.
     */
    private void initializeAddressTextField() {
        // Initialise tooltip
        addressTextField.setTooltip(createNewTooltip(INVALID_ADDRESS_TOOLTIP_TEXT));
        addressTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    updateAddressTextField();
                    if (!oldValue.equals(newValue) && !undoRedoEvent) {
                        addTextToDeque(addressTextField, oldValue);
                    }
                })
        );
    }

    /**
     * Will test to see if the address value is currently valid and will apply appropriate CSS styles and tooltips.
     */
    private void updateAddressTextField() {
        String currentValue = addressTextField.getText();
        isAddressValid = isValidAddress(currentValue);
        applyTextFieldStyle(addressTextField, isAddressValid, true, VALID_ADDRESS_TOOLTIP_TEXT,
                INVALID_ADDRESS_TOOLTIP_TEXT, currentValue);
    }

    /**
     * Set the DOB date picker to only allow selection through the built in calendar and sets the default date to be
     * 01-01-1990. Also adds live error checking and visual feedback through a listener. DOB bust be between 01-01-1900
     * and today's date.
     */
    private void initializeDateOfBirthPicker() {
        // Create default date for the date of birth date picker
        LocalDate defaultDate = LocalDate.parse("01-01-1990", DateTimeFormatter.ofPattern("dd-MM-yyyy"));
        isDateOfBirthValid = true; // We are initializing the date of birth to a valid date. So set isValid to be true
        dateOfBirthDatePicker.setTooltip(createNewTooltip(INVALID_DATE_OF_BIRTH_TOOLTIP_TEXT));
        dateOfBirthDatePicker.valueProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    setDeathCSS();
                    if (((oldValue == null) || (!oldValue.equals(newValue))) && !undoRedoEvent) {
                        addDatePickerToDeque(dateOfBirthDatePicker, oldValue);
                    }
                }
                ));
        // Set value after the listener was created so the date picker is green showing the default date of birth is valid.
        if (dateOfBirthDatePicker.getValue() == null) {
            undoRedoEvent = true;
            dateOfBirthDatePicker.setValue(defaultDate);
            undoRedoEvent = false;
        }
    }

    /**
     * Populates the gender choice box with all possible genders and adds a listener that makes the box turn green to
     * indicate a gender has been selected.
     */
    private void initializeGenderChoiceBox() {
        genderChoiceBox.setItems(FXCollections.observableArrayList(
                GenderEnum.values()));
        genderChoiceBox.valueProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    updateGenderChoiceBox();
                    if (((oldValue == null) || (!oldValue.equals(newValue))) && !undoRedoEvent) {
                        addEnumChoiceBoxToDeque(genderChoiceBox, oldValue);
                    }
                })
        );
        genderChoiceBox.setValue(GenderEnum.NOT_SET);
    }

    /**
     * Populates the hospital choicebox.
     * <p>
     * Add listeners to the death date picker and hospital choicebox so we know if the values are valid or not.
     */
    private void initializeDeathNodes() {
        dateOfDeathDatePicker.setEditable(false);
        dateOfDeathDatePicker.setTooltip(createNewTooltip(INVALID_DATE_OF_DEATH_TOOLTIP_TEXT));

        dateOfDeathDatePicker.valueProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    setDeathCSS();
                    if (((oldValue == null) || (!oldValue.equals(newValue))) && !undoRedoEvent) {
                        addDatePickerToDeque(dateOfDeathDatePicker, oldValue);
                    }
                }));

        try {
            hospitals = HospitalRequests.getInstance().getHospitals(AuthToken.getInstance().getToken()).getBody();
            Collections.sort(hospitals);
        } catch (HttpClientErrorException e) {
            switch (e.getStatusCode()) {
                case UNAUTHORIZED:
                    throw e;
                default:
                    // TODO Connor, handleCriticalError not yet implemented
                    // handleCriticalError(e);
            }
        } catch (HttpServerErrorException | ResourceAccessException e) {
            // TODO Connor, handleCriticalError not yet implemented
            // handleCriticalError(e);
        }

        hospitalOfDeathChoiceBox.setItems(FXCollections.observableArrayList(hospitals));
        hospitalOfDeathChoiceBox.setConverter(new StringConverter<Hospital>() {
            @Override
            public String toString(Hospital hospital) {
                return hospital.getName();
            }

            @Override
            public Hospital fromString(String hospitalName) {

                for (Hospital hospital : hospitals) {
                    if (hospital.getName().equals(hospitalName)) {
                        return hospital;
                    }
                }
                LOGGER.severe(String.format("Hospital '%s' doesn't exist.", hospitalName));
                return null;
            }
        });
        hospitalOfDeathChoiceBox.valueProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    setDeathCSS();
                    if (((oldValue == null) || (!oldValue.equals(newValue))) && !undoRedoEvent) {
                        addHospitalChoiceBoxToDeque(hospitalOfDeathChoiceBox, oldValue);
                        addHospitalChoiceBoxToDeque(hospitalOfDeathChoiceBox, oldValue);
                    }
                })
        );
    }

    /**
     * Test to see if the value is valid and set the appropriate CSS style and tooltip.
     */
    private void updateGenderChoiceBox() {
        Object currentValue = genderChoiceBox.getValue();
        applyChoiceBoxStyle(genderChoiceBox, true, true, currentValue);
    }

    /**
     * Populates the birth gender choice box with all possible genders and adds a listener that makes the box turn green
     * to indicate a birth gender has been selected.
     */
    private void initializeBirthGenderChoiceBox() {
        birthGenderChoiceBox.setItems(FXCollections.observableArrayList(
                GenderEnum.NOT_SET, GenderEnum.MALE, GenderEnum.FEMALE));
        birthGenderChoiceBox.valueProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    updateBirthGenderChoiceBox();
                    if (((oldValue == null) || (!oldValue.equals(newValue))) && !undoRedoEvent) {
                        addEnumChoiceBoxToDeque(birthGenderChoiceBox, oldValue);
                    }
                })
        );
        birthGenderChoiceBox.setValue(GenderEnum.NOT_SET);
    }

    /**
     * Test to see if the value is valid and set the appropriate CSS style and tooltip.
     */
    private void updateBirthGenderChoiceBox() {
        Object currentValue = birthGenderChoiceBox.getValue();
        applyChoiceBoxStyle(birthGenderChoiceBox, true, true, currentValue);
    }

    private void addHospitalChoiceBoxToDeque(ChoiceBox<Hospital> choiceBox, Hospital oldValue) {
        undoRedoController.pushToUndoDeque(new UndoableHospitalChoiceBox(choiceBox, oldValue));
        updateUndoRedoButtons();
    }

    /**
     * Populates the region choice box with all possible NZ regions and adds a listener that makes the box turn green to
     * indicate a region has been selected.
     */
    private void initializeRegionChoiceBox() {
        ObservableList<Enum> regionEnumOptions = FXCollections.observableArrayList();
        regionEnumOptions.addAll(RegionEnum.values());
        regionChoiceBox.setItems(regionEnumOptions);
        regionChoiceBox.valueProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    updateRegionChoiceBox();
                    if (((oldValue == null) || (!oldValue.equals(newValue))) && !undoRedoEvent) {
                        addEnumChoiceBoxToDeque(regionChoiceBox, oldValue);
                    }
                })
        );
        regionChoiceBox.setValue(RegionEnum.NOT_SET);
    }

    /**
     * Update the style applied to the region choice box.
     */
    private void updateRegionChoiceBox() {
        Object currentValue = regionChoiceBox.getValue();
        applyChoiceBoxStyle(regionChoiceBox, true, true, currentValue);
    }

    /**
     * Populates the blood type choice box with all possible blood types and adds a listener that makes the box turn
     * green to indicate a blood type has been selected.
     */
    private void initializeBloodTypeChoiceBox() {
        bloodTypeChoiceBox.setItems(FXCollections.observableArrayList(
                BloodTypeEnum.values()));
        bloodTypeChoiceBox.valueProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    updateBloodTypeChoiceBox();
                    if ((newValue != null && !undoRedoEvent) && (oldValue == null || (!oldValue.equals(newValue)))) {
                        addEnumChoiceBoxToDeque(bloodTypeChoiceBox, oldValue);
                    }
                })
        );
        bloodTypeChoiceBox.setValue(BloodTypeEnum.NOT_SET);
    }

    /**
     * Update the style applied to the blood type choice box.
     */
    private void updateBloodTypeChoiceBox() {
        Object currentValue = bloodTypeChoiceBox.getValue();
        applyChoiceBoxStyle(bloodTypeChoiceBox, true, true, currentValue);
    }

    /**
     * Function initializes the checkboxes with relevant listeners
     */
    private void initializeCheckBoxes() {
        ArrayList<CheckBox> checkBoxArrayList = new ArrayList<>(Arrays.asList(boneCheckBox, boneMarrowCheckBox,
                connectiveTissueCheckBox, corneaCheckBox, heartCheckBox, heartValveCheckBox, intestineCheckBox,
                kidneyCheckBox, liverCheckBox, lungCheckBox, middleEarCheckBox, pancreasCheckBox, skinCheckBox));
        for (CheckBox checkBox : checkBoxArrayList) {
            addCheckBoxListener(checkBox);
        }
    }

    /**
     * Function adds a listener to the checkboxes so that they can have undo/redo functionality
     *
     * @param checkBox CheckBox: that you want to attach listener to
     */
    private void addCheckBoxListener(CheckBox checkBox) {
        checkBox.selectedProperty().addListener(((observable, oldValue, newValue) -> {
            if (!undoRedoEvent) {
                addCheckBoxToDeque(checkBox);
            }
        }));
    }


    /**
     * Creates a user with the currently entered details. It requires at minimum a valid username, first name, and DOB
     * to successfully create a new user. If one of the mandatory fields is missing then the field will become red and
     * an alert box will pop-up telling the user they need to complete all mandatory fields. Will exit the create user
     * stage once the profile was successfully created and added to the rest of the profiles.
     */
    @FXML
    private void createUserButtonClicked() {
        if (isUsernameValid && isFirstNameValid && isDateOfBirthValid && (isDeathDetailsValid || nothingSet)) {
            // Mandatory fields
            String username = usernameTextField.getText();
            String firstName = firstNameTextField.getText();
            LocalDate dateOfBirth = dateOfBirthDatePicker.getValue();

            // Note: By default getText() will return "" if no value entered so checks aren't necessary
            String alias = aliasTextField.getText();
            String middleName = middleNameTextField.getText();
            String lastName = lastNameTextField.getText();
            String address = addressTextField.getText();

            RegionEnum region = (RegionEnum) regionChoiceBox.getValue();

            Death death = ProfileController.DEATH_NOT_SET;

            LocalDate dateOfDeath = dateOfDeathDatePicker.getValue();
            Hospital hospital = hospitalOfDeathChoiceBox.getSelectionModel().getSelectedItem();

            if (isDeathDetailsValid) {
                Time timeOfDeath = new Time(Integer.parseInt(hrTimeOfDeathTextField.getText().trim()), Integer.parseInt(minTimeOfDeathTextField.getText().trim()),
                        Integer.parseInt(secTimeOfDeathTextField.getText().trim()));
                death = new Death(dateOfDeath, hospital, timeOfDeath);
            } else if (dateOfDeath == null && hospital == null) {
                // Leave the death as null
            } else {
                LOGGER.severe("Unexpected flow in logic, date and hospital must either both be null or" +
                        "have values. Date of death value: " + dateOfDeath + " Hospital value: " + hospital);
            }

            float height = 0;
            if (isHeightValid) {
                height = Float.parseFloat(heightTextField.getText());
            }
            if (dateOfDeathDatePicker.getValue() != null) {
                // Force the dateOfDeathDatePicker listener to update its CSS style since DOB has been updated
                LocalDate date = dateOfDeathDatePicker.getValue();
                dateOfDeathDatePicker.setValue(LocalDate.now());
                dateOfDeathDatePicker.setValue(date);
            }


            float weight = 0;
            if (isWeightValid) {
                weight = Float.parseFloat(weightTextField.getText());
            }

            Date createdDate = new Date();
            Date modifiedDate = new Date();

            // Finding what gender they selected
            GenderEnum gender = (GenderEnum) genderChoiceBox.getValue();

            // Finding what birth gender they selected
            GenderEnum birthGender = (GenderEnum) birthGenderChoiceBox.getValue();

            // Finding what blood type they selected
            BloodTypeEnum bloodType = (BloodTypeEnum) bloodTypeChoiceBox.getValue();

            // Finding what organs they selected to donate (if any)
            boolean isDonor;
            ArrayList<OrganEnum> organs = getSelectedOrgans();
            isDonor = organs.size() > 0;

            ArrayList<OrganEnum> receiveOrganList = new ArrayList<>();

            // Create the profile and add it to the loaded profiles
            Profile prof =
                    new Profile(firstName, middleName, lastName, dateOfBirth, death, gender, birthGender, height,
                            weight, bloodType,
                            address, region, createdDate, modifiedDate, isDonor, organs, username, alias, false,
                            receiveOrganList, ProfileController.BLOOD_PRESSURE_NOT_SET,
                            ProfileController.ALCOHOL_CONSUMPTION_NOT_SET, ProfileController.IS_SMOKER_NOT_SET);

            //if death details visible set it
            if (hrTimeOfDeathTextField.isVisible()) {
                if (dateOfDeathDatePicker.getValue() != null) {
                    if (!isDeathDetailsValid) {
                        AlertDialog.showAndWait("Incorrect Information Entered", "Incorrect details for time of death");
                        return;
                    }
                    int hour = Integer.parseInt(hrTimeOfDeathTextField.getText().trim());
                    int min = Integer.parseInt(minTimeOfDeathTextField.getText().trim());
                    int sec = Integer.parseInt(secTimeOfDeathTextField.getText().trim());
                    Time timeOfDeath2 = new Time(hour, min, sec);
                    prof.getDeath().setTimeOfDeath(timeOfDeath2);
                }
            }

            if (ControllerAccess.getAdminSceneController() != null) {
                ControllerAccess.getAdminRolesTabController().pushProfileChangeToUndoDeque(null, prof, null);
            }

            HttpStatus statusCode = HttpStatus.OK;
            try {
                ProfileRequests.getInstance().postProfile(prof);
                close(); // Close this stage after the user was successfully created.
            } catch (HttpClientErrorException e) {
                LOGGER.severe(e.getMessage());
                statusCode = e.getStatusCode();
            }


            if (statusCode == HttpStatus.BAD_REQUEST || statusCode == HttpStatus.INTERNAL_SERVER_ERROR) {
                String title = "More detail required";
                String content = "You must complete the mandatory fields/ the username is already taken.";
                AlertDialog.showAndWait(title, content);
            } else {
                if (AuthToken.getInstance().getRole() == null) {
                    loginController.postProfileCreation(username);
                }
            }
        } else {
            AlertDialog.showAndWait("Failure", "You must complete the mandatory fields");
        }
    }

    /**
     * Initialize a reference to the LoginSceneController
     *
     * @param controller The controller used in the login stage.
     */
    public void initLoginControllerReference(LoginSceneController controller) {
        this.loginController = controller;
    }

    /**
     * Exit the create user stage because they want to stop creating a user profile.
     */
    @FXML
    private void cancelButtonClicked() {
        close();
    }

    /**
     * Method called when the create user window is closed.
     */
    private void close() {
        Stage createUserStage = (Stage) createUserPane.getScene().getWindow();
        createUserStage.close();
    }

    /**
     * Retrieves all the selected organs that a user has opted to donate on the create user screen..
     *
     * @return The organs that the user has opted to donate
     */
    private ArrayList<OrganEnum> getSelectedOrgans() {
        ArrayList<OrganEnum> organs = new ArrayList<>();
        if (boneCheckBox.isSelected()) {
            organs.add(OrganEnum.BONE);
        }
        if (boneMarrowCheckBox.isSelected()) {
            organs.add(OrganEnum.BONE_MARROW);
        }
        if (connectiveTissueCheckBox.isSelected()) {
            organs.add(OrganEnum.CONNECTIVE_TISSUE);
        }
        if (corneaCheckBox.isSelected()) {
            organs.add(OrganEnum.CORNEA);
        }
        if (heartCheckBox.isSelected()) {
            organs.add(OrganEnum.HEART);
        }
        if (intestineCheckBox.isSelected()) {
            organs.add(OrganEnum.INTESTINE);
        }
        if (kidneyCheckBox.isSelected()) {
            organs.add(OrganEnum.KIDNEY);
        }
        if (liverCheckBox.isSelected()) {
            organs.add(OrganEnum.LIVER);
        }
        if (lungCheckBox.isSelected()) {
            organs.add(OrganEnum.LUNG);
        }
        if (middleEarCheckBox.isSelected()) {
            organs.add(OrganEnum.MIDDLE_EAR);
        }
        if (pancreasCheckBox.isSelected()) {
            organs.add(OrganEnum.PANCREAS);
        }
        if (skinCheckBox.isSelected()) {
            organs.add(OrganEnum.SKIN);
        }
        if (heartValveCheckBox.isSelected()) {
            organs.add(OrganEnum.HEART_VALVES);
        }
        return organs;
    }

    /**
     * Clears the date of death date picker in case you accidentally clicked on it and were wanting to clear.
     */
    @FXML
    private void clearDateOfDeath() {
        if (dateOfDeathDatePicker.getValue() != null && hospitalOfDeathChoiceBox.getValue() != null &&
                !hrTimeOfDeathTextField.getText().equals(null) && !minTimeOfDeathTextField.getText().equals(null)
                && !secTimeOfDeathTextField.getText().equals(null)) {
            ArrayList<Command> undoableObjects = new ArrayList<>();
            undoableObjects.add(new UndoableDatePicker(dateOfDeathDatePicker, dateOfDeathDatePicker.getValue()));
            undoableObjects.add(new UndoableHospitalChoiceBox(hospitalOfDeathChoiceBox, hospitalOfDeathChoiceBox.getValue()));
            undoRedoController.pushToUndoDeque(new UndoableListOfUndoableObjects(undoableObjects));

            undoRedoEvent = true;

            dateOfDeathDatePicker.setValue(null);
            dateOfDeathDatePicker.setTooltip(createNewTooltip(INVALID_DATE_OF_DEATH_TOOLTIP_TEXT));
            hospitalOfDeathChoiceBox.setValue(null);
            hrTimeOfDeathTextField.setText("");
            minTimeOfDeathTextField.setText("");
            secTimeOfDeathTextField.setText("");

            undoRedoEvent = false;
        } else if (dateOfDeathDatePicker.getValue() != null) {
            dateOfDeathDatePicker.setValue(null);
        } else if (hospitalOfDeathChoiceBox.getValue() != null) {
            hospitalOfDeathChoiceBox.setValue(null);
        } else {
            hrTimeOfDeathTextField.setText("");
            minTimeOfDeathTextField.setText("");
            secTimeOfDeathTextField.setText("");
        }
    }

    /**
     * Hides the elements of death details
     */
    public void hideDeathDetails() {
        dateOfDeathDatePicker.setVisible(false);
        clearDateOfDeathButton.setVisible(false);
        timeOfDeathText.setVisible(false);
        hospitalOfDeathChoiceBox.setVisible(false);
        hospitalOfDeathText.setVisible(false);
        dateOfDeathText.setVisible(false);
        minTimeOfDeathTextField.setVisible(false);
        separatorTimeOfDeathText.setVisible(false);
        separator2TimeOfDeathText.setVisible(false);
        hrTimeOfDeathTextField.setVisible(false);
        secTimeOfDeathTextField.setVisible(false);
    }

    /**
     * Function creates an UndoableTextField object and pushes to the undo deque.
     *
     * @param textField TextField: that we want to restore.
     * @param oldValue  String: the oldValue to restore to.
     */
    private void addTextToDeque(TextField textField, String oldValue) {
        undoRedoController.pushToUndoDeque(new UndoableTextField(textField, oldValue));
        updateUndoRedoButtons();
    }

    /**
     * Function creates an UndoableDatePicker object and pushes to the undo deque.
     *
     * @param datePicker DatePicker: that we want to restore.
     * @param oldValue   LocalDate: the oldValue to restore to.
     */
    private void addDatePickerToDeque(DatePicker datePicker, LocalDate oldValue) {
        undoRedoController.pushToUndoDeque(new UndoableDatePicker(datePicker, oldValue));
        updateUndoRedoButtons();
    }

    /**
     * Function creates an UndoableEnumChoiceBox object and pushes to the undo deque.
     *
     * @param choiceBox ChoiceBox: that we want to restore.
     * @param oldValue  Enum: the oldValue to restore to.
     */
    private void addEnumChoiceBoxToDeque(ChoiceBox<Enum> choiceBox, Enum oldValue) {
        undoRedoController.pushToUndoDeque(new UndoableEnumChoiceBox(choiceBox, oldValue));
        updateUndoRedoButtons();
    }

    /**
     * Function creates an UndoableCheckBox object and pushes to the undo deque.
     *
     * @param checkBox CheckBox: that we want to restore.
     */
    private void addCheckBoxToDeque(CheckBox checkBox) {
        undoRedoController.pushToUndoDeque(new UndoableCheckBox(checkBox));
        updateUndoRedoButtons();
    }

    /**
     * Redo event handler for when redo is pressed.
     */
    @FXML
    private void redoEventCreateUser() {
        undoRedoEvent = true;
        this.undoRedoController.redo();
        undoRedoEvent = false;
        updateUndoRedoButtons();
    }

    /**
     * Undo event handler for when undo is pressed.
     */
    @FXML
    private void undoEventCreateUser() {
        undoRedoEvent = true;
        this.undoRedoController.undo();
        undoRedoEvent = false;
        updateUndoRedoButtons();
    }

    /**
     * Updates the undo redo buttons to be usable if the undo redo stacks are not empty.
     */
    private void updateUndoRedoButtons() {
        createUserUndo.setVisible(!this.undoRedoController.isUndoEmpty());
        createUserRedo.setVisible(!this.undoRedoController.isRedoEmpty());
    }

    /**
     * Function to get the UndoRedoController, mainly used for testing
     *
     * @return UndoRedoController for this object
     */
    public UndoRedoController getUndoRedoController() {
        return undoRedoController;
    }
}
