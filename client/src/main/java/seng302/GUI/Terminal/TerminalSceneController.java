package seng302.GUI.Terminal;

import com.sun.javafx.tk.FontMetrics;
import com.sun.javafx.tk.Toolkit;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import seng302.GUI.ControllerAccess;
import seng302.GUI.Terminal.Shells.Bash;
import seng302.GUI.Terminal.Shells.Shell;
import seng302.GUI.Terminal.Shells.Sql;
import seng302.Model.Admin;

/**
 * Controls the terminal that is being displayed.
 */
public class TerminalSceneController {

    private final String SQL_SHELL_COMMAND = "sql";
    @FXML
    private TextArea terminalDisplay;
    @FXML
    private TextField terminalInput;
    @FXML
    private PasswordField terminalPasswordInput;
    @FXML
    private AnchorPane terminalPane;
    private TextField currentInput;
    private Admin currentAdmin;
    private Shell currentShell;
    // System dependent line separator
    private String ls = System.getProperty("line.separator");

    /**
     * Will show the JavaFX terminal.
     */
    public void show() {
        currentInput.requestFocus();
        Stage terminalStage = (Stage) terminalPane.getScene().getWindow();
        terminalStage.show();
    }

    /**
     * If the value entered into the terminal is sensitive information we can choose to hide it.
     *
     * @param isMasked If true, the value entered into the terminal will be hidden.
     */
    public void setMaskedInput(boolean isMasked) {
        terminalInput.setVisible(!isMasked);
        terminalPasswordInput.setVisible(isMasked);

        if (isMasked) {
            currentInput = terminalPasswordInput;
            terminalPasswordInput.requestFocus();
        } else {
            currentInput = terminalInput;
            terminalInput.requestFocus();
        }
    }


    /**
     * Sets a reference to the current admin.
     *
     * @param admin The admin that is currently logged in.
     */
    public void initCurrentAdmin(Admin admin) {
        this.currentAdmin = admin;
    }

    /**
     * @return The admin that is currently using the terminal.
     */
    public Admin getAdminUser() {
        return currentAdmin;
    }

    /**
     * Listener to the TextField which will retrieve whatever the user entered. Will display what they entered on the
     * screen with a prompt, easily distinguish their input to the app's response.
     * <p>
     * Will parse the input into the terminal and execute the command.
     */
    @FXML
    public void processTerminalInput() {
        String enteredValue = currentInput.getText();
        currentInput.clear();

        // See if we need to swap the shells
        if (SQL_SHELL_COMMAND.equals(enteredValue) && !currentShell.getCurrentShell().equals(Sql.class.getName())) {
            swapToSqlShell();
        } else {
            currentShell.processInput(enteredValue);
        }

    }

    public void swapToSqlShell() {
        currentShell.cleanUp();
        currentShell = new Sql(this);
        currentShell.setUp();
    }

    public void swapToBashShell() {
        currentShell.cleanUp();
        currentShell = new Bash(this);
        currentShell.setUp();
    }


    public void clearInput() {
        currentInput.clear();
    }

    public void setInputText(String value) {
        currentInput.setText(value);
    }

    /**
     * Quit the application
     */
    public void quitTerminal() {
        // This will avoid the core being dumped for some reason.
        System.exit(0);
    }

    /**
     * Append the message to the current output. This way all previous messages are still displayed as well as the new
     * one. The display will also be 'refreshed' to display the new message.
     *
     * @param message The message to be displayed to the custom terminal.
     */
    public void display(String message) {
        terminalDisplay.appendText(message + ls); // Auto scroll to the bottom
    }

    /**
     * Used for wrapping the text in the terminal display. Since the display can change its size dynamically we use this
     * to get it's current size so we know length we can wrap the contents to be displayed to the terminal.
     *
     * @return Current width of the terminal.
     */
    public int getTerminalDisplayWidth() {
        final int OFFSET = 5; // The scroll bar ends up covering some of the screen, so it counts as we're
        // outside of the display area despite getting its width directly.

        Font font = terminalDisplay.getFont();
        FontMetrics fontMetrics = Toolkit.getToolkit().getFontLoader().getFontMetrics(font);
        // Should be using monospace as the font so all the characters should have the same width
        double fontSizePx = fontMetrics.computeStringWidth("T");
        double displayWidthPpx = terminalDisplay.getWidth();
        return ((int) Math.round(displayWidthPpx / fontSizePx)) - OFFSET;
    }

    /**
     * Initialize all the Terminal Stage and set the required listeners.
     */
    @FXML
    public void initialize() {
        ControllerAccess.setTerminalSceneController(this);

        // Add listeners to all the nodes so it can detect when the user wants to cycle through their command history
        terminalInput.setOnKeyPressed(event -> {
            KeyCode keyPressed = event.getCode();
            if (keyPressed.equals(KeyCode.UP)) {
                currentShell.traceBackCommandHistory();
                currentInput.end();
                event.consume();
            } else if (keyPressed.equals(KeyCode.DOWN)) {
                currentShell.traceForwardCommandHistory();
                currentInput.end();
            }
        });

        currentInput = terminalInput;


        currentShell = new Bash(this);
    }

}
