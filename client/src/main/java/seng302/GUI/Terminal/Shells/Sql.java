package seng302.GUI.Terminal.Shells;

import org.springframework.web.client.HttpClientErrorException;
import seng302.Database.DatabaseManager;
import seng302.GUI.Terminal.TerminalSceneController;
import seng302.ServerInteracton.ServerQueries.Authentication.AuthToken;
import seng302.ServerInteracton.ServerQueries.Authentication.LoginRequest;
import seng302.ServerInteracton.ServerQueries.Authentication.LoginRequestBody;

import java.sql.*;
import java.util.logging.Logger;

import static org.apache.commons.lang.StringUtils.repeat;

/**
 * The sql shell will be used to execute queries written by the ODMS administrator.
 */
public class Sql extends Shell {

    private final String EXIT_COMMAND = "exit";
    private final Logger LOGGER = Logger.getLogger(getClass().getName());
    private boolean isLoggedIn = false;
    private boolean isAskingForUsername = true;
    private String enteredUsername;

    public Sql(TerminalSceneController terminal) {
        super(terminal);

        prompt = "SQL>";
    }

    /**
     * Executes the given command
     *
     * @param enteredValue the command that is needed to be executed.
     */
    @Override
    protected void execute(String enteredValue) {

        if (!isLoggedIn) {
            promptLogin(enteredValue);
        } else if (EXIT_COMMAND.equals(enteredValue)) {
            terminal.swapToBashShell();
            return;
        } else {
            terminal.display(prompt + " " + enteredValue);
            executeQuery(enteredValue);
        }
    }

    /**
     * Execute the provided query using a read only connection.
     *
     * @param query The entered query by the admin.
     */
    private void executeQuery(String query) {

        DatabaseManager db = new DatabaseManager();

        try (Connection conn = db.getConnection()) {
            conn.setReadOnly(true);

            PreparedStatement statement = conn.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            printHeaders(resultSet.getMetaData());
            while (resultSet.next()) {
                printResults(resultSet);
            }

        } catch (SQLException e) {
            terminal.display(e.getMessage());
        }
    }


    /**
     * Asking the user to login. Their must be logged in before they can connect to the database.
     *
     * @param credential The value entered in the command line.
     */
    private void promptLogin(String credential) {

        DatabaseManager dbManager = new DatabaseManager();

        if (isAskingForUsername) {
            terminal.display(prompt + " " + credential);
            enteredUsername = credential;
            terminal.display("Enter Password: ");
            terminal.setMaskedInput(true);
            isAskingForUsername = false; // Now check for the password the next time they input a value

        } else {
            // Show a bullet unicode character instead of their actual password
            terminal.display(prompt + " " + repeat("\u2022", credential.length()));

            try {
                LoginRequest loginRequest = new LoginRequest();
                LoginRequestBody loginRequestBody = new LoginRequestBody(enteredUsername, credential);
                AuthToken authToken = loginRequest.postLogin(loginRequestBody).getBody();
                // Ensure we actually get a token back from the database
                assert authToken != null;
                AuthToken.getInstance().init(authToken.getToken(), authToken.getRole());
                isLoggedIn = true;
                terminal.display("Authenticated.");
            } catch (HttpClientErrorException e) {
                terminal.display("Invalid login credentials.");
                terminal.swapToBashShell();
            }
            terminal.setMaskedInput(false);
        }
    }

    /**
     * Prompt the user to login.
     */
    @Override
    public void setUp() {
        terminal.display("Enter Username: ");
    }

    @Override
    public void cleanUp() {
        terminal.display("SQL session terminated.");
    }


    @Override
    public void initTerminalReference(TerminalSceneController mainTerminal) {
        terminal = mainTerminal;
    }

    @Override
    public String getCurrentShell() {
        return getClass().getName();
    }

    /**
     * Prints out the column labels to act as table headers.
     *
     * @param metaData the meta data which is taken from the query to the database.
     * @throws SQLException if the headers are malformed.
     */
    private void printHeaders(ResultSetMetaData metaData) throws SQLException {
        StringBuilder headersBuilder = new StringBuilder();
        for (int i = 1; i <= metaData.getColumnCount(); i++) {
            if (i == 1) {
                headersBuilder.append("|");
            }
            headersBuilder.append(String.format(" %-30s |", metaData.getColumnLabel(i)));
        }
        String headers = headersBuilder.toString();
        terminal.display(headers);
        terminal.display(repeat("-", headers.length()));
    }

    /**
     * Prints out the results from the SQL query to the terminal.
     *
     * @param resultSet the result set that we got back from the database with our query.
     * @throws SQLException if the result set is malformed.
     */
    private void printResults(ResultSet resultSet) throws SQLException {
        StringBuilder row = new StringBuilder();
        for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
            if (i == 1) {
                row.append("|");
            }
            row.append(String.format(" %-30s |", resultSet.getString(i)));
        }

        terminal.display(row.toString());
    }
}
