package seng302.GUI.Terminal.Shells;

import org.apache.tools.ant.types.Commandline;
import seng302.CustomException.DoesNotExist;
import seng302.CustomException.InvalidCommand;
import seng302.Enum.CommandEnum;
import seng302.GUI.Terminal.TerminalSceneController;
import seng302.Model.CliInput;
import seng302.ModelController.CommandController;

import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Logger;

/**
 * The bash shell will be loaded as the default in the terminal. It will process the normal commands and will be able
 * to load different shells.
 */
public class Bash extends Shell {

    private final Logger LOGGER = Logger.getLogger(getClass().getName());

    public Bash(TerminalSceneController terminal) {
        super(terminal);

        prompt = "ODMS$";
    }


    /**
     * Parse the input from the JavaFX terminal. Identify the command and the arguments.
     *
     * @param enteredValue The value entered into the JavaFX terminal
     * @return A CliInput object which represents the entered valid command.
     * @throws InvalidCommand Thrown when the command entered does not exist.
     */
    private CliInput parseInput(String enteredValue) throws InvalidCommand {
        final int COMMAND_INDEX = 0;
        final int FIRST_ARGUMENT_INDEX = 1;

        enteredValue = enteredValue.trim(); //Remove any leading/trailing white spaces

        if (enteredValue.length() == 0) {
            throw new InvalidCommand("The command entered does not exist.");
        }


        String[] enteredInputParts = Commandline.translateCommandline(enteredValue);

        CommandEnum command;
        try {
            command = CommandController.retrieveCommand(enteredInputParts[COMMAND_INDEX]);
        } catch (DoesNotExist e) {
            throw new InvalidCommand(e.getMessage());
        }


        if (command == null) {
            throw new InvalidCommand("The command entered does not exist.");
        }

        String[] arguments = Arrays.copyOfRange(enteredInputParts, FIRST_ARGUMENT_INDEX, enteredInputParts.length);


        CommandController.checkArguments(command, arguments);

        CliInput cliInput = new CliInput(command, arguments);

        return cliInput;
    }

    public void execute(String enteredValue) {
        terminal.display(prompt + " " + enteredValue); // Only the user's input will have the prompt.

        CliInput cliInput;

        try {
            cliInput = parseInput(enteredValue);
            CommandController.execute(cliInput);
            terminal.display(""); // Add a blank line for formatting
        } catch (InvalidCommand invalidCommand) {
            terminal.display(invalidCommand.getMessage()); // Something went wrong, display error to the user
        } catch (IOException ioException) {
            LOGGER.severe(ioException.getMessage());
        }
    }

    @Override
    public void setUp() {
        // Do nothing
    }

    @Override
    public void cleanUp() {
        // Do nothing
    }


    @Override
    public void initTerminalReference(TerminalSceneController mainTerminal) {
        terminal = mainTerminal;
    }

    @Override
    public String getCurrentShell() {
        return getClass().getName();
    }
}
