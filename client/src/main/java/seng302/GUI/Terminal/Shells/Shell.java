package seng302.GUI.Terminal.Shells;

import seng302.GUI.Terminal.TerminalSceneController;

import java.util.ArrayList;

public abstract class Shell {

    protected TerminalSceneController terminal;
    protected String prompt;
    /**
     * Used to 'point' at the command to be displayed in the terminal. This is used when the user wants to trace through
     * their command history.
     */
    private int commandHistoryIndex = 0;
    private ArrayList<String> commandHistory = new ArrayList<>();


    public Shell(TerminalSceneController terminal) {
        initTerminalReference(terminal);
    }

    /**
     * Will clear the terminal input and replace it with the command that is pointed at by the commandHistoryIndex. If
     * commandHistoryIndex is the same as the number of commands, then it isn't pointing at a command, meaning that the
     * user has no more recent entered commands to display so clear the terminal.
     */
    public void displayCommand() {
        terminal.clearInput();
        if (commandHistoryIndex == commandHistory.size()) {
            // Do nothing, no next recent entered command to display
        } else {
            terminal.setInputText(commandHistory.get(commandHistoryIndex));
        }
    }


    /**
     * When the user presses the UP key, it will display the next previous command they've entered. When they've reached
     * the oldest/first command entered it'll keep displaying that command in the input of the console.
     */
    public void traceBackCommandHistory() {
        if (commandHistoryIndex == 0) {
            // Already pointing at the first entered command, do nothing
        } else {
            commandHistoryIndex -= 1; // Move to the next previous entered command
        }

        displayCommand();
    }

    /**
     * When the user presses the DOWN key, it will display the next recent command they've entered. When they've gone
     * past the most recent/latest command entered it'll just show an empty input in the console.
     */
    public void traceForwardCommandHistory() {
        if (commandHistoryIndex == commandHistory.size()) {
            // Already gone past all the entered commands. Can't increase the index to point to the next
            // recent command entered. Do nothing. This value will be used to tell the program to clear the
            // input in the console.
        } else {
            commandHistoryIndex += 1;
        }

        displayCommand();
    }


    /**
     * Template method which will add the command to the history and then execute command.
     *
     * @param enteredValue What the user typed into the terminal.
     */
    public void processInput(String enteredValue) {
        // Add their input to the history of commands, so the user can trace through their command history
        commandHistory.add(enteredValue);

        // Will reset the index to the size of the ArrayList, so that when the user presses UP it will point
        // to the most recently entered command, because commandHistoryIndex -= 1 will be executed.
        commandHistoryIndex = commandHistory.size();

        execute(enteredValue);
    }


    protected abstract void execute(String enteredValue);

    public abstract void setUp();

    public abstract void cleanUp();

    public abstract void initTerminalReference(TerminalSceneController terminal);

    /**
     * Returns the name of the Shell so the terminal knows what is the current shell.
     *
     * @return The class name of the current shell.
     */
    public abstract String getCurrentShell();
}
