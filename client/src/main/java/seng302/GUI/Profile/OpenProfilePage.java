package seng302.GUI.Profile;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.http.ResponseEntity;
import seng302.CustomException.DoesNotExist;
import seng302.Model.Profile;
import seng302.ServerInteracton.ServerQueries.Authentication.AuthToken;
import seng302.ServerInteracton.ServerQueries.ProfileRequests;
import seng302.Utilities.ProfileDisplayUtility;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * Call this class opens up a profile page with related information loaded. This class could be a good candidates for
 * interface, if there are another similar method we applied in to openProfileFromClick, then we should make an
 * interface.
 */
public class OpenProfilePage {

    private final Logger LOGGER = Logger.getLogger(getClass().getName());

    /**
     * Method called to create a new stage to display a profile's details.
     *
     * @param username The username of the profile to be opened.
     */
    public void openProfileFromDoubleClick(String username) {
        Stage profileStage = null;

        if (!ProfileDisplayUtility.isStageCreated(username)) {
            profileStage = new Stage();

            try {
                ResponseEntity<Profile> responseEntity = ProfileRequests.getInstance().getProfile(username, AuthToken.getInstance().getToken());
                Profile profile = responseEntity.getBody();
                FXMLLoader loader = new FXMLLoader(getClass().getResource("profile.fxml"));
                Parent root = loader.load();
                Scene scene = new Scene(root, 1025, 650);
                profileStage.setScene(scene);
                profileStage.show(); // Show the stage first before setting the donor, needs the fields visible
                // so they can be queried

                ProfileSceneController controller = loader.getController();
                controller.initParentStageReference();
                controller.setCurrentProfile(profile);
                controller.initStageTitle();

                ProfileDisplayUtility.displayProfileStage(username, profileStage, controller);
            } catch (IOException e) {
                LOGGER.severe(e.getMessage());
            }


        } else {

            try {
                profileStage = ProfileDisplayUtility.retrieveProfileStage(username);
                profileStage.toFront();
            } catch (DoesNotExist doesNotExist) {
                LOGGER.severe(doesNotExist.getMessage());
            }
        }
    }
}
