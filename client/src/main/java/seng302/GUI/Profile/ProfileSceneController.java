package seng302.GUI.Profile;

import impl.org.controlsfx.autocompletion.AutoCompletionTextFieldBinding;
import impl.org.controlsfx.autocompletion.SuggestionProvider;
import javafx.animation.PauseTransition;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Region;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import javafx.util.StringConverter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import seng302.API.ApiCache;
import seng302.API.Mapi;
import seng302.CustomException.DoesNotExist;
import seng302.Enum.*;
import seng302.GUI.Admin.AdminSceneController;
import seng302.GUI.Clinician.ClinicianSceneController;
import seng302.GUI.ControllerAccess;
import seng302.GUI.Hideable;
import seng302.GUI.LoginSceneController;
import seng302.GUI.MainController;
import seng302.GUI.MedicalHistory.MedicalHistoryTabController;
import seng302.GUI.Notifications.AlertDialog;
import seng302.GUI.Notifications.PushNotification;
import seng302.GUI.Organ.OrganDonorController;
import seng302.GUI.Organ.OrganReceiverController;
import seng302.GUI.Procedures.UserMedicalProceduresTabController;
import seng302.Model.Death;
import seng302.Model.Disease;
import seng302.Model.Hospital;
import seng302.Model.Profile;
import seng302.ModelController.CacheController;
import seng302.ModelController.ProfileController;
import seng302.ModelController.UserController;
import seng302.ServerInteracton.ServerQueries.Authentication.AuthToken;
import seng302.ServerInteracton.ServerQueries.Authentication.Role;
import seng302.ServerInteracton.ServerQueries.ProfileRequests;
import seng302.Utilities.AutoRequestTimer;
import seng302.Utilities.Undo_Redo.*;

import java.net.URL;
import java.sql.Time;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Logger;

import static java.time.temporal.ChronoUnit.YEARS;
import static seng302.Utilities.Style.*;
import static seng302.Utilities.Tooltips.*;
import static seng302.Utilities.Validator.*;

/**
 * Handles the display of donor profiles and their editing events =============================================== L I C
 * E N S E S =================================================<br> ControlsFX was used for their controls that allow
 * multiple selection boxes and lists in JavaFX GUI's <br> Copyright (c) 2013, 2014, ControlsFX <br> licensed under the
 * ControlsFX license. You may not use this file except in compliance with their license. A copy of the license is
 * obtainable from here. Alternatively: https://bitbucket.org/controlsfx/controlsfx/src/default/license.txt <br> Unless
 * required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS
 * IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific
 * language governing permissions and limitations under the License. <br>
 */
public class ProfileSceneController extends MainController implements Hideable, Initializable {
    //==================================================================================================================
    /* General class variables                                                                                        */
    //==================================================================================================================

    /**
     * Tab pane containing all children tabs for the profile scene.
     */
    @FXML
    private TabPane profileTabPane;
    @FXML
    private Tab profileTab, // 0 - These numbers are here for reference indicating Tab indexes in the profile TabPane.
            donationTab, // 1
            receiverTab, // 2
            medicationsTab, // 3
            historyTab, // 4
            medicalHistoryTab, // 5
            procedureHistoryTab; // 6

    /**
     * The current profile being displayed in the Profile window.
     */
    private Profile currentProfile;

    private List<Hospital> hospitals = new ArrayList<Hospital>();

    /**
     * Logger for error reporting rather than printing stack traces, etc. to the terminal
     */
    private Logger LOGGER = Logger.getLogger(ProfileSceneController.class.getName());

    //------------------------------------------------------------------------------------------------------------------
    /* Injected FXML controllers                                                                                      */
    //------------------------------------------------------------------------------------------------------------------

    @FXML
    private OrganDonorController donorAnchorPaneController;
    @FXML
    private OrganReceiverController organReceiverAnchorPaneController;
    @FXML
    private MedicalHistoryTabController medicalHistoryController;
    @FXML
    private UserMedicalProceduresTabController userMedicalProceduresTabController;

    //------------------------------------------------------------------------------------------------------------------
    /* Undo/redo related variables                                                                                    */
    //------------------------------------------------------------------------------------------------------------------

    @FXML
    private Button undo, redo;

    /**
     * True if no events are allowed to be pushed onto the UndoRedoController Deque, false otherwise.
     */
    private boolean undoRedoPushingBlocked;
    /**
     * The classes undo/redo functionality controller
     */
    private UndoRedoController undoRedoController;

    // Tab change related undo/redo variables
    private SingleSelectionModel<Tab> profileTabPaneSelectionModel;
    private Tab currentTab;

    //------------------------------------------------------------------------------------------------------------------
    /* Menu related variables                                                                                         */
    //------------------------------------------------------------------------------------------------------------------

    @FXML
    private Menu donorFileMenu, helpMenu;
    @FXML
    private MenuItem editMenuItem, saveMenuItem, profileLogoutMenuItem, emptyCacheItem;

    //==================================================================================================================
    /* Profile tab related variables                                                                                  */
    //==================================================================================================================

    @FXML
    private AnchorPane mainAnchorPane;
    @FXML
    private Button editCancelButton, applyEditButton, btnRegisterDonor, btnRegisterReceiver, clearDateOfDeathButton;
    @FXML
    private CheckBox smokerCheckBox;
    @FXML
    private ChoiceBox<Enum> preferredGenderChoiceBox, birthGenderChoiceBox, bloodTypeChoiceBox, regionChoiceBox,
            alcoholConsumptionChoiceBox;

    @FXML
    private ChoiceBox<Hospital> hospitalOfDeathChoiceBox;

    @FXML
    private DatePicker dateOfBirthPicker, dateOfDeathPicker;
    @FXML
    private Text userName, alias, firstName, middleName, lastName, dateOfBirth, gender, birthGender, bloodType,
            donorStatus, dateOfDeath, weight, height, bmi, smoker, bloodPressure, alcohol, address, region, dateCreated,
            lastModified, historyTabTitle, ageText, ageAtDeathText, receiverStatusText, hospitalOfDeath, clinicianText, hrTimeOfDeathText, minTimeOfDeathText,
            secTimeOfDeathText;

    @FXML
    private Text separatorTimeOfDeathText, separator2TimeOfDeathText;
    @FXML
    private TextField aliasEdit, firstNameEdit, middleNameEdit, lastNameEdit, weightEdit, heightEdit, bloodPressureEdit,
            addressEdit, hrTimeOfDeathTextField, minTimeOfDeathTextField, secTimeOfDeathTextField;

    private boolean acceptableFirstName = true, acceptableMiddleName = true, acceptableLastName = true, acceptableDob =
            true, acceptableDod = true, setDod = false, acceptableWeight = true, acceptableHeight = true, acceptableBloodPressure =
            true, acceptableAddress = true, acceptableAlias = true;

    //==================================================================================================================
    /* Medications tab related variables                                                                              */
    //==================================================================================================================

    @FXML
    private Button newMedBtn, deleteMedBtn, moveToCurrentMedBtn, moveToPastMedBtn;
    @FXML
    private TextArea drugIngredients, drugInteractions;
    @FXML
    private TextField medicationSearch;
    @FXML
    private ListView<String> previousMedication, currentMedication;
    @FXML
    private Label interactions, ingredients;

    private ArrayList<String> autoCompleteResults = new ArrayList<>();
    private int N_THREADS = 2; // Number of threads to use. The VM has 2, so we use 2.
    private ObservableList<String> donorCurrentMedications = FXCollections.observableArrayList(),
            donorPreviousMedications = FXCollections.observableArrayList();
    private String medicationToGetIngredient, medicationSearchNewValue;
    private ThreadPoolExecutor threadPool = (ThreadPoolExecutor) Executors.newFixedThreadPool(N_THREADS);
    /**
     * Thread for getting the drug interactions between drug A and drug B.
     */
    private Task compare = new Task() {
        @Override
        protected Object call() {
            run();
            return null;
        }

        public void run() {
            ApiCache cacheManager = new ApiCache();
            GenderEnum gender = currentProfile.getGender();
            if (gender == UserController.GENDER_NOT_SET) gender = GenderEnum.OTHER;
            ArrayList<String> interactions;
            ObservableList<String> currentSelected = currentMedication.getSelectionModel().getSelectedItems();
            ObservableList<String> previousSelected = previousMedication.getSelectionModel().getSelectedItems();
            if (currentSelected.isEmpty()) {
                interactions = cacheManager
                        .getDrugInteractions(previousSelected.get(0), previousSelected.get(1), gender.toString(),
                                currentProfile.getAge());
            } else if (previousSelected.isEmpty()) {
                interactions = cacheManager
                        .getDrugInteractions(currentSelected.get(0), currentSelected.get(1), gender.toString(),
                                currentProfile.getAge());
            } else {
                interactions = cacheManager
                        .getDrugInteractions(previousSelected.get(0), currentSelected.get(0), gender.toString(),
                                currentProfile.getAge());
            }
            drugInteractions.setText(String.join("\n", interactions));
        }
    };
    /**
     * Thread for getting the active ingredients for a drug.
     */
    private Task activeIngredients = new Task() {
        protected Object call() {
            run();
            return null;
        }

        public void run() {
            ArrayList<String> res = Mapi.getIngredients(medicationToGetIngredient);
            if (res.isEmpty()) {
                drugIngredients.setText("There are no active ingredients.");
            } else {
                drugIngredients.setText(String.join("\n", res));
            }
        }
    };
    @FXML
    private TextArea historyTextArea;

    //==================================================================================================================
    /* History tab related variables                                                                                  */
    //==================================================================================================================

    /**
     * Constructor for the ProfileSceneController class. Currently just sets itself inside ControllerAccess.
     */
    public ProfileSceneController() {
        ControllerAccess.setProfileSceneController(this);
    }

    //==================================================================================================================
    /* General                                                                                                        */
    //==================================================================================================================

    /**
     * Used when we want to set the date in the picker during editing.
     *
     * @param newDateOfDeath The new value for date of death during edit.
     */
    public void setNewEditDateOfDeath(LocalDate newDateOfDeath) {
        dateOfDeathPicker.setValue(newDateOfDeath);
    }

    /**
     * Clears the date of death date picker, hospital of death and time of death,  in case you accidentally clicked on
     * it and were wanting to clear.
     */
    @FXML
    private void clearDateOfDeath() {
        if (dateOfDeathPicker.getValue() != null && hospitalOfDeathChoiceBox.getValue() != null &&
                !hrTimeOfDeathTextField.getText().equals(null) && !minTimeOfDeathTextField.getText().equals(null) &&
                !secTimeOfDeathTextField.getText().equals(null)) {
            // Both death date picker and hospital choice box have values selected, want to be able to undo it in one
            // click

            ArrayList<Command> undoableObjects = new ArrayList<>();
            undoableObjects.add(new UndoableDatePicker(dateOfDeathPicker, dateOfDeathPicker.getValue()));
            undoableObjects.add(new UndoableHospitalChoiceBox(hospitalOfDeathChoiceBox, hospitalOfDeathChoiceBox.getValue()));
            undoRedoController.pushToUndoDeque(new UndoableListOfUndoableObjects(undoableObjects));

            undoRedoPushingBlocked = true;

            dateOfDeathPicker.setValue(null);
            dateOfDeathPicker.setTooltip(createNewTooltip(INVALID_DATE_OF_DEATH_TOOLTIP_TEXT));
            hospitalOfDeathChoiceBox.setValue(null);
            hrTimeOfDeathTextField.setText("");
            minTimeOfDeathTextField.setText("");
            secTimeOfDeathTextField.setText("");

            undoRedoPushingBlocked = false;
        } else if (dateOfDeathPicker.getValue() != null) {
            dateOfDeathPicker.setValue(null);
        } else if (hospitalOfDeathChoiceBox.getValue() != null) {
            hospitalOfDeathChoiceBox.setValue(null);
        } else {
            hrTimeOfDeathTextField.setText("");
            minTimeOfDeathTextField.setText("");
            secTimeOfDeathTextField.setText("");
        }

    }

    /**
     * Called after the constructor (if there is one) and any @FXML fields are populated.
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Platform.runLater(this::setStuffThatCausesNull);
        initTabReferences();
        setAllListeners();
        btnRegisterDonor.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.equals("Register")) {
                donationTab.setDisable(true);
            } else if (newValue.equals("Unregister")) {
                donationTab.setDisable(false);
            }
        });
        btnRegisterReceiver.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.equals("Register")) {
                receiverTab.setDisable(true);
            } else if (newValue.equals("Unregister")) {
                receiverTab.setDisable(false);
            }
        });

        currentMedication.focusedProperty().addListener((observable, oldValue, newValue) ->
        {
            if (!isEditing) {
                setAutoRequestBlocked(newValue);
            }
        });
        previousMedication.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!isEditing) {
                setAutoRequestBlocked(newValue);
            }
        });
        initializeMenuEventHandlers();
        initializeUndoRedo();
        AutoRequestTimer.getInstance().createAutoRequestTimer(-1, -1, this::handleAutoRequestEvent);
        hospitals = loadHospitals();
        hospitalOfDeathChoiceBox.setItems(FXCollections.observableArrayList(hospitals));
        hospitalOfDeathChoiceBox.setConverter(new StringConverter<Hospital>() {
            @Override
            public String toString(Hospital hospital) {
                return hospital.getName();
            }

            @Override
            public Hospital fromString(String hospitalName) {

                for (Hospital hospital : hospitals) {
                    if (hospital.getName().equals(hospitalName)) {
                        return hospital;
                    }
                }

                LOGGER.severe(String.format("Hospital '%s' doesn't exist.", hospitalName));
                return null;
            }
        });
    }

    /**
     * Function is called at the start. Stuff that causes null pointer exceptions when initializes from the start. Now
     * runs after the FXML loader loads.
     */
    private void setStuffThatCausesNull() {
        hide();
        initializeMedicationLists();
    }

    @Override
    public void initParentStageReference() {
        this.parentStage = (Stage) mainAnchorPane.getScene().getWindow();
    }

    /**
     * Define references and other necessary initialization methods relating to included FXML in the tabs.
     */
    private void initTabReferences() {
        donorAnchorPaneController.setProfileSceneController(this);
        organReceiverAnchorPaneController.setProfileSceneController(this);
        medicalHistoryController.setProfileSceneController(this);
        organReceiverAnchorPaneController.initParentController(this);
    }

    /**
     * Sets the title of the Profile window to match the user profile that is open.
     */
    @Override
    public void initStageTitle() {
        stageTitle = String.format("User: %s %s %s", currentProfile.getFirstName(), currentProfile.getMiddleName(),
                currentProfile.getLastName());
    }

    /**
     * Handles an auto-request event, updating all admin related details in the application.
     *
     * @return A boolean which is not currently relevant. It part of the function signature so we that can pass this
     * function as a parameter for the autoRequestTimer.
     */
    private Boolean handleAutoRequestEvent() {
        if (!autoRequestBlocked) {
            Platform.runLater(this::updateAllProfileInformation);
            Platform.runLater(this::updateTimeStamp);
        }
        return null;
    }


    /**
     * Updates all profile related
     */
    private void updateAllProfileInformation() {
        try {
            Profile profile = ProfileRequests.getInstance().getProfile(currentProfile.getUsername(),
                    AuthToken.getInstance().getToken()).getBody();
            setCurrentProfile(profile); // This also updates the history tab
        } catch (HttpClientErrorException e) {
            // Check the status code of the request and handle accordingly
            switch (e.getStatusCode()) {
                case UNAUTHORIZED:
                    kickUser();
                    break;
                default:
                    handleCriticalError(e);
            }
        } catch (HttpServerErrorException | ResourceAccessException e) {
            handleCriticalError(e);
        }
        donorAnchorPaneController.editEndDonorOrgansEvent();
        organReceiverAnchorPaneController.hide();
        medicalHistoryController.populateTables();
        userMedicalProceduresTabController.populateTables();
    }

    /**
     * Hides the logout button in the Menu.
     */
    public void hideLogoutButton() {
        profileLogoutMenuItem.setVisible(false);
    }

    //to
    //------------------------------------------------------------------------------------------------------------------
    /* Getters and setters                                                                                            */
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Returns the current profile.
     *
     * @return The current profile.
     */
    public Profile getCurrentProfile() {
        return this.currentProfile;
    }

    /**
     * Set the current donor to be displayed. Will update all the fields to show the user's information.
     *
     * @param donor The donor whose details are to be displayed.
     */
    public void setCurrentProfile(Profile donor) {
        currentProfile = donor;

        setProfileFieldsText();
        populateGuiElements(); // Some nodes rely on the attributes of the current profile
        updateProfileHistoryOfChanges();
    }

    /**
     * Returns the UndoRedoController for this class.
     *
     * @return The UndoRedoController for this class.
     */
    public UndoRedoController getUndoRedoController() {
        return this.undoRedoController;
    }

    /**
     * Returns the UndoRedoController for this class.
     *
     * @return The OrganDonorController for this class.
     */
    public OrganDonorController getDonorAnchorPaneController() {
        return this.donorAnchorPaneController;
    }

    /**
     * Returns the OrganReceiverController for this class.
     *
     * @return The OrganReceiverController for this class.
     */
    public OrganReceiverController getOrganReceiverAnchorPaneController() {
        return this.organReceiverAnchorPaneController;
    }

    /**
     * Sets the isEditing flag.
     *
     * @param value The value that isEditing will be set to.
     */
    @Override
    public void setIsEditing(boolean value) {
        super.setIsEditing(value);
    }

    //==================================================================================================================
    /* All listeners                                                                                                  */
    //==================================================================================================================

    /**
     * Sets all of the listeners for the class.
     */
    private void setAllListeners() throws NullPointerException {
        setCheckBoxListeners();
        setChoiceBoxListeners();
        setMedicationsTabListeners();
        setTextFieldListeners();
    }

    /**
     * Sets all CheckBox listeners for this class.
     */
    private void setCheckBoxListeners() {
        smokerCheckBox.selectedProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    if (!oldValue.equals(newValue) && !undoRedoPushingBlocked) {
                        pushCheckBoxToUndoDeque(smokerCheckBox);
                    }
                })
        );
    }

    /**
     * Sets all ChoiceBox listeners for the class.
     */
    private void setChoiceBoxListeners() {
        preferredGenderChoiceBox.setItems(FXCollections.observableArrayList(GenderEnum.values()));
        preferredGenderChoiceBox.valueProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    if ((newValue != null && !undoRedoPushingBlocked) &&
                            (oldValue == null || (!oldValue.equals(newValue)))) {
                        pushEnumChoiceBoxToUndoDeque(preferredGenderChoiceBox, oldValue);
                    }
                }));

        bloodTypeChoiceBox.setItems(FXCollections.observableArrayList(BloodTypeEnum.values()));
        bloodTypeChoiceBox.valueProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    if ((newValue != null && !undoRedoPushingBlocked) &&
                            (oldValue == null || (!oldValue.equals(newValue)))) {
                        pushEnumChoiceBoxToUndoDeque(bloodTypeChoiceBox, oldValue);
                    }
                }));

        ObservableList<Enum> regionEnumOptions = FXCollections.observableArrayList();
        regionEnumOptions.addAll(RegionEnum.values());
        regionChoiceBox.setItems(regionEnumOptions);
        regionChoiceBox.valueProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    if ((newValue != null && !undoRedoPushingBlocked) &&
                            (oldValue == null || (!oldValue.equals(newValue)))) {
                        pushEnumChoiceBoxToUndoDeque(regionChoiceBox, oldValue);
                    }
                }));

        birthGenderChoiceBox.setItems(FXCollections.observableArrayList(GenderEnum.FEMALE, GenderEnum.MALE));
        birthGenderChoiceBox.valueProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    if ((newValue != null && !undoRedoPushingBlocked) &&
                            (oldValue == null || (!oldValue.equals(newValue)))) {
                        pushEnumChoiceBoxToUndoDeque(birthGenderChoiceBox, oldValue);
                    }
                }));

        alcoholConsumptionChoiceBox.setItems(FXCollections.observableArrayList(AlcoholConsumptionEnum.values()));
        alcoholConsumptionChoiceBox.valueProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    if ((newValue != null && !undoRedoPushingBlocked) &&
                            (oldValue == null || (!oldValue.equals(newValue)))) {
                        pushEnumChoiceBoxToUndoDeque(alcoholConsumptionChoiceBox, oldValue);

                    }
                }));

        hospitalOfDeathChoiceBox.valueProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    setDeathCSS();
                    if ((newValue != null && !undoRedoPushingBlocked) &&
                            (oldValue == null || (!oldValue.equals(newValue)))) {
                        pushHospitalChoiceBoxToUndoDeque(hospitalOfDeathChoiceBox, oldValue);

                    }
                }));
    }

    /**
     * Set the listener for the change of value in the medication search field. Also binds the suggestion list to the
     * field, and a listener for the enter key to add the drug to the current medication table.
     */
    private void setMedicationsTabListeners() {
        SuggestionProvider<String> provider = SuggestionProvider.create(new ArrayList<>());
        new AutoCompletionTextFieldBinding<>(medicationSearch, provider);
        PauseTransition pauseTransition = new PauseTransition(Duration.seconds(0.5));

        medicationSearch.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!oldValue.equals(newValue) && !undoRedoPushingBlocked && (medicationSearchNewValue == null || !newValue
                    .equals(medicationSearchNewValue))) {
                pushTextFieldChangeToUndoDeque(medicationSearch, oldValue);
            }
            medicationSearchNewValue = medicationSearch.getText();
            pauseTransition.setOnFinished(ae -> {
                if (!newValue.equals("")) {
                    provider.clearSuggestions();
                    autoCompleteResults = Mapi.getAutoComplete(newValue);
                    provider.addPossibleSuggestions(autoCompleteResults);
                }
            });
            pauseTransition.playFromStart();

        });
    }

    /**
     * Sets all TextField listeners for the class.
     */
    private void setTextFieldListeners() {
        aliasEdit.setTooltip(createNewTooltip(INVALID_ALIAS_TOOLTIP_TEXT));
        aliasEdit.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    acceptableAlias = isValidAlias(newValue);
                    applyTextFieldStyle(aliasEdit, acceptableAlias, true, VALID_ALIAS_TOOLTIP_TEXT,
                            INVALID_ALIAS_TOOLTIP_TEXT, newValue);

                    if (!oldValue.equals(newValue) && !undoRedoPushingBlocked) {
                        pushTextFieldChangeToUndoDeque(aliasEdit, oldValue);
                    }
                }));

        firstNameEdit.setTooltip(createNewTooltip(INVALID_FIRST_NAME_TOOLTIP_TEXT));
        firstNameEdit.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    acceptableFirstName = isValidFirstName(newValue);
                    applyTextFieldStyle(firstNameEdit, acceptableFirstName, false, VALID_FIRST_NAME_TOOLTIP_TEXT,
                            INVALID_FIRST_NAME_TOOLTIP_TEXT, newValue);
                    if (!oldValue.equals(newValue) && !undoRedoPushingBlocked) {
                        pushTextFieldChangeToUndoDeque(firstNameEdit, oldValue);
                    }

                }));

        middleNameEdit.setTooltip(createNewTooltip(INVALID_MIDDLE_NAME_TOOLTIP_TEXT));
        middleNameEdit.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    acceptableMiddleName = isValidMiddleName(newValue);
                    applyTextFieldStyle(middleNameEdit, acceptableMiddleName, true, VALID_MIDDLE_NAME_TOOLTIP_TEXT,
                            INVALID_MIDDLE_NAME_TOOLTIP_TEXT, newValue);

                    if (!oldValue.equals(newValue) && !undoRedoPushingBlocked) {
                        pushTextFieldChangeToUndoDeque(middleNameEdit, oldValue);
                    }
                }));

        lastNameEdit.setTooltip(createNewTooltip(INVALID_LAST_NAME_TOOLTIP_TEXT));
        lastNameEdit.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    acceptableLastName = isValidLastName(newValue);
                    applyTextFieldStyle(lastNameEdit, acceptableLastName, true, VALID_LAST_NAME_TOOLTIP_TEXT,
                            INVALID_LAST_NAME_TOOLTIP_TEXT, newValue);

                    if (!oldValue.equals(newValue) && !undoRedoPushingBlocked) {
                        pushTextFieldChangeToUndoDeque(lastNameEdit, oldValue);
                    }
                }));

        dateOfBirthPicker.setTooltip(createNewTooltip(INVALID_DATE_OF_BIRTH_TOOLTIP_TEXT));
        dateOfBirthPicker.valueProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    LocalDate currentValue = dateOfBirthPicker.getValue();
                    acceptableDob = isValidDateOfBirth(currentValue);
                    applyDatePickerStyle(dateOfBirthPicker, acceptableDob, false,
                            VALID_DATE_OF_BIRTH_TOOLTIP_TEXT, INVALID_DATE_OF_BIRTH_TOOLTIP_TEXT, currentValue);

                    // The date of birth also affects the validity of the date of death, must update as well
                    setDeathCSS();

                    if (((oldValue == null) || (!oldValue.equals(newValue))) && !undoRedoPushingBlocked) {
                        pushDatePickerChangeToUndoDeque(dateOfBirthPicker, oldValue);
                    }

                    ageText.setText("" + YEARS.between(newValue, LocalDate.now()));
                    if (dateOfDeathPicker.getValue() != null) {
                        ageAtDeathText.setText(
                                "" + YEARS.between(dateOfBirthPicker.getValue(), dateOfDeathPicker.getValue()));
                    }
                }
                ));

        dateOfDeathPicker.setTooltip(createNewTooltip(INVALID_DATE_OF_DEATH_TOOLTIP_TEXT));
        dateOfDeathPicker.valueProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    setDeathCSS();
                    if (((oldValue == null) || (!oldValue.equals(newValue))) && !undoRedoPushingBlocked) {
                        pushDatePickerChangeToUndoDeque(dateOfDeathPicker, oldValue);
                    }
                }));

        weightEdit.setTooltip(createNewTooltip(INVALID_WEIGHT_TOOLTIP_TEXT));
        weightEdit.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    acceptableWeight = isValidWeight(newValue);
                    applyTextFieldStyle(weightEdit, acceptableWeight, true, VALID_WEIGHT_TOOLTIP_TEXT,
                            INVALID_WEIGHT_TOOLTIP_TEXT, newValue);
                }));

        heightEdit.setTooltip(createNewTooltip(INVALID_HEIGHT_TOOLTIP_TEXT));
        heightEdit.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    acceptableHeight = isValidHeight(newValue);
                    applyTextFieldStyle(heightEdit, acceptableHeight, true, VALID_HEIGHT_TOOLTIP_TEXT,
                            INVALID_HEIGHT_TOOLTIP_TEXT, newValue);
                }));

        bloodPressureEdit.setTooltip(createNewTooltip(INVALID_BLOOD_PRESSURE_TOOLTIP_TEXT));
        bloodPressureEdit.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    acceptableBloodPressure = isValidBloodPressure(newValue);
                    applyTextFieldStyle(bloodPressureEdit, acceptableBloodPressure, true,
                            VALID_BLOOD_PRESSURE_TOOLTIP_TEXT,
                            INVALID_BLOOD_PRESSURE_TOOLTIP_TEXT, newValue);
                }));

        addressEdit.setTooltip(createNewTooltip(INVALID_ADDRESS_TOOLTIP_TEXT));
        addressEdit.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    acceptableAddress = isValidAddress(newValue);
                    applyTextFieldStyle(addressEdit, acceptableAddress, true, VALID_ADDRESS_TOOLTIP_TEXT,
                            INVALID_ADDRESS_TOOLTIP_TEXT, newValue);
                }));

        heightEdit.setTooltip(createNewTooltip(INVALID_HEIGHT_TOOLTIP_TEXT));
        heightEdit.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    if (!oldValue.equals(newValue) && !undoRedoPushingBlocked) {
                        pushTextFieldChangeToUndoDeque(heightEdit, oldValue);
                    }

                    if (isValidWeight(weightEdit.getText()) && isValidHeight(heightEdit.getText())) {
                        float weight = Float.parseFloat(weightEdit.getText());
                        float height = Float.parseFloat(heightEdit.getText());
                        if (height >= 1 && weight >= 1) {
                            bmi.setText(Float.toString((weight / height) / height));
                        }
                    }
                }));

        weightEdit.setTooltip(createNewTooltip(INVALID_WEIGHT_TOOLTIP_TEXT));
        weightEdit.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    if (!oldValue.equals(newValue) && !undoRedoPushingBlocked) {
                        pushTextFieldChangeToUndoDeque(weightEdit, oldValue);
                    }

                    if (isValidWeight(weightEdit.getText()) && isValidHeight(heightEdit.getText())) {
                        float weight = Float.parseFloat(weightEdit.getText());
                        float height = Float.parseFloat(heightEdit.getText());
                        if (height >= 1 && weight >= 1) {
                            bmi.setText(Float.toString((weight / height) / height));
                        }
                    }
                }));

        bloodPressureEdit.setTooltip(createNewTooltip(INVALID_BLOOD_PRESSURE_TOOLTIP_TEXT));
        bloodPressureEdit.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    String currentValue = bloodPressureEdit.getText();
                    acceptableBloodPressure = isValidBloodPressure(currentValue);
                    applyTextFieldStyle(bloodPressureEdit, acceptableBloodPressure, true,
                            VALID_BLOOD_PRESSURE_TOOLTIP_TEXT, INVALID_BLOOD_PRESSURE_TOOLTIP_TEXT, currentValue);
                    if (!oldValue.equals(newValue) && !undoRedoPushingBlocked) {
                        pushTextFieldChangeToUndoDeque(bloodPressureEdit, oldValue);
                    }
                }));

        addressEdit.setTooltip(createNewTooltip(INVALID_ADDRESS_TOOLTIP_TEXT));
        addressEdit.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    String currentValue = addressEdit.getText();
                    acceptableAddress = isValidAddress(currentValue);
                    applyTextFieldStyle(addressEdit, acceptableAddress, true, VALID_ADDRESS_TOOLTIP_TEXT,
                            INVALID_ADDRESS_TOOLTIP_TEXT, currentValue);
                    if (!oldValue.equals(newValue) && !undoRedoPushingBlocked) {
                        pushTextFieldChangeToUndoDeque(addressEdit, oldValue);
                    }
                }));

        hrTimeOfDeathTextField.setTooltip(createNewTooltip(INVALID_HR_TIME_OF_DEATH_TOOLTIP_TEXT));
        hrTimeOfDeathTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    setDeathCSS();
                    if (!oldValue.equals(newValue) && !undoRedoPushingBlocked) {
                        pushTextFieldChangeToUndoDeque(hrTimeOfDeathTextField, oldValue);
                    }
                }
                ));

        minTimeOfDeathTextField.setTooltip(createNewTooltip(INVALID_MIN_TIME_OF_DEATH_TOOLTIP_TEXT));
        minTimeOfDeathTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    setDeathCSS();
                    if (!oldValue.equals(newValue) && !undoRedoPushingBlocked) {
                        pushTextFieldChangeToUndoDeque(minTimeOfDeathTextField, oldValue);
                    }
                }
                ));

        secTimeOfDeathTextField.setTooltip(createNewTooltip(INVALID_SEC_TIME_OF_DEATH_TOOLTIP_TEXT));
        secTimeOfDeathTextField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    setDeathCSS();
                    if (!oldValue.equals(newValue) && !undoRedoPushingBlocked) {
                        pushTextFieldChangeToUndoDeque(secTimeOfDeathTextField, oldValue);
                    }
                }
                ));


    }

    /**
     * Checks the 3 time fields, the dod picker and the hospital choice box
     * and sets the appropriate CSS styling.
     */
    private void setDeathCSS() {

        LocalDate currentValue = dateOfDeathPicker.getValue();
        boolean hospitalSet = hospitalOfDeathChoiceBox.getValue() != null;

        String currentHrValue = hrTimeOfDeathTextField.getText();
        String currentMinValue = minTimeOfDeathTextField.getText();
        String currentSecValue = secTimeOfDeathTextField.getText();

        boolean nothingSet = (currentValue == null &&
                !hospitalSet &&
                currentHrValue.equals("") &&
                currentMinValue.equals("") &&
                currentSecValue.equals(""));

        LocalDate currentDate = LocalDate.now();
        boolean validDate = true;
        boolean validTimeStamp = false;

        if (nothingSet) {
            dateOfDeathPicker.setValue(null);
            dateOfDeathPicker.getStylesheets().clear();
            dateOfDeathPicker.getStylesheets().clear();
            hospitalOfDeathChoiceBox.setValue(null);
            hospitalOfDeathChoiceBox.getStylesheets().clear();
            hrTimeOfDeathTextField.getStylesheets().clear();
            minTimeOfDeathTextField.getStylesheets().clear();
            secTimeOfDeathTextField.getStylesheets().clear();
            acceptableDod = true;
            setDod = false;
        } else {

            if (dateOfDeathPicker.getValue() != null) {
                validDate = ((dateOfDeathPicker.getValue().compareTo(dateOfBirthPicker.getValue()) >= 0) && (dateOfDeathPicker.getValue().compareTo(currentDate) <= 0));

                applyDatePickerStyle(dateOfDeathPicker, validDate, false, VALID_DATE_OF_DEATH_TOOLTIP_TEXT,
                        INVALID_DATE_OF_DEATH_TOOLTIP_TEXT, currentValue);
            } else {
                applyDatePickerStyle(dateOfDeathPicker, false, false, VALID_DATE_OF_DEATH_TOOLTIP_TEXT,
                        INVALID_DATE_OF_DEATH_TOOLTIP_TEXT, currentValue);
                validDate = false;
            }

            applyChoiceBoxStyle(hospitalOfDeathChoiceBox, hospitalSet, false, hospitalOfDeathChoiceBox.getValue());

            validTimeStamp = isValidTimeStamp(currentHrValue, currentMinValue, currentSecValue);

            applyTextFieldStyle(hrTimeOfDeathTextField, validTimeStamp, false, VALID_HR_TIME_OF_DEATH_TOOLTIP_TEXT,
                    INVALID_HR_TIME_OF_DEATH_TOOLTIP_TEXT, currentHrValue);
            applyTextFieldStyle(minTimeOfDeathTextField, validTimeStamp, false, VALID_MIN_TIME_OF_DEATH_TOOLTIP_TEXT,
                    INVALID_MIN_TIME_OF_DEATH_TOOLTIP_TEXT, currentMinValue);
            applyTextFieldStyle(secTimeOfDeathTextField, validTimeStamp, false, VALID_SEC_TIME_OF_DEATH_TOOLTIP_TEXT,
                    INVALID_SEC_TIME_OF_DEATH_TOOLTIP_TEXT, currentSecValue);

            acceptableDod = validDate && hospitalSet && validTimeStamp; //&& validHour && validMinute && validSecond;
            setDod = true;
        }
    }


    //==================================================================================================================
    /* Undo/redo related                                                                                              */
    //==================================================================================================================

    /**
     * Creates an UndoRedoController and tracker variables for the window. Adds listeners to the undo and redo buttons
     * so they trigger undo and redo events when pressed. Also initialises tab change event listeners for undo/redo.
     */
    private void initializeUndoRedo() {
        // Tab change related
        profileTabPaneSelectionModel = profileTabPane.getSelectionModel();
        currentTab = profileTabPaneSelectionModel.getSelectedItem();

        // Initialising tracker variables in this class and injected classes
        undoRedoPushingBlocked = false;
        updateUndoRedoPushingBlockedForInjectedControllers();

        // Initialise UndoRedoController and set all injected controller to use the same one
        undoRedoController = new UndoRedoController();
        setUndoRedoControllerForInjectedControllers();

        // Undo/redo button listeners
        undo.setOnAction(event -> {
            undoRedoPushingBlocked = true;
            updateUndoRedoPushingBlockedForInjectedControllers();
            undoRedoController.undo();
            undoRedoPushingBlocked = false;
            updateUndoRedoPushingBlockedForInjectedControllers();
            updateUndoRedoButtons();
        });
        redo.setOnAction(event -> {
            undoRedoPushingBlocked = true;
            updateUndoRedoPushingBlockedForInjectedControllers();
            undoRedoController.redo();
            undoRedoPushingBlocked = false;
            updateUndoRedoPushingBlockedForInjectedControllers();
            updateUndoRedoButtons();
        });

        updateUndoRedoButtons();
        createTabEventHandlers();

//        initializeDateOfBirthPicker(); - This cannot be called here because it uses the currentProfile but at this point the current profile is not set.
        //initializeDateOfDeathPicker();
    }

    /**
     * Ensures that the undo/redo buttons are only enabled if there is an available undo/redo.
     */
    public void updateUndoRedoButtons() {
        undo.setDisable(undoRedoController.isUndoEmpty());
        redo.setDisable(undoRedoController.isRedoEmpty());
    }

    /**
     * Updates the undoRedoPushingBlocked variable for all injected profile window controllers to match the
     * undoRedoPushingBlocked value in this class.
     */
    private void updateUndoRedoPushingBlockedForInjectedControllers() {
        donorAnchorPaneController.setCurrentlyUndoing(undoRedoPushingBlocked);
        organReceiverAnchorPaneController.setCurrentlyUndoing(undoRedoPushingBlocked);
        medicalHistoryController.setCurrentlyUndoing(undoRedoPushingBlocked);
        userMedicalProceduresTabController.setCurrentlyUndoing(undoRedoPushingBlocked);
    }

    /**
     * Sets all injected controllers to use the same UndoRedoController as this class.
     */
    private void setUndoRedoControllerForInjectedControllers() {
        donorAnchorPaneController.setUndoRedoController(undoRedoController);
        organReceiverAnchorPaneController.setUndoRedoController(undoRedoController);
        medicalHistoryController.setUndoRedoController(undoRedoController);
        userMedicalProceduresTabController.setUndoRedoController(undoRedoController);
    }

    //------------------------------------------------------------------------------------------------------------------
    /* Undo/redo related - Tab change related                                                                         */
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Adds event handlers to all tabs in the Profile scene so that when they are clicked they push a Command onto the
     * undo stack.
     */
    private void createTabEventHandlers() {
        profileTab.setOnSelectionChanged(event -> handleTabChange(profileTab));
        donationTab.setOnSelectionChanged(event -> handleTabChange(donationTab));
        receiverTab.setOnSelectionChanged(event -> handleTabChange(receiverTab));
        medicationsTab.setOnSelectionChanged(event -> handleTabChange(medicationsTab));
        historyTab.setOnSelectionChanged(event -> handleTabChange(historyTab));
        medicalHistoryTab.setOnSelectionChanged(event -> {
            propagatedProfileData();
            handleTabChange(medicalHistoryTab);
        });
        procedureHistoryTab.setOnSelectionChanged(event -> {
            propagatedProfileData();
            handleTabChange(procedureHistoryTab);
        });
    }

    /**
     * Handler for whenever a tab is selected in the Profile Scene that pushes the tab change event onto the undo Deque
     * as an UndoableTabChange. Note: It will not add these if the event was caused by an undo or redo.
     *
     * @param tab The new Tab that was selected.
     */
    private void handleTabChange(Tab tab) {
        if (tab.isSelected() && !undoRedoPushingBlocked) {
            undoRedoController.pushToUndoDeque(new UndoableTabChange(profileTabPaneSelectionModel, currentTab));
            updateUndoRedoButtons();
        }
        currentTab = profileTabPaneSelectionModel.getSelectedItem();
    }


    //------------------------------------------------------------------------------------------------------------------
    /* Undo/redo related - Pushing to the undo Deque related                                                          */
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Function creates an UndoableCheckBox object and pushes it onto the undo Deque.
     *
     * @param checkBox CheckBox: that we want to restore.
     */
    private void pushCheckBoxToUndoDeque(CheckBox checkBox) {
        undoRedoController.pushToUndoDeque(new UndoableCheckBox(checkBox));
        updateUndoRedoButtons();
    }

    /**
     * Function creates an UndoableDatePicker object and pushes it onto the undo Deque.
     *
     * @param datePicker DatePicker: that we want to restore.
     * @param oldValue   LocalDate: the oldValue to restore to.
     */
    private void pushDatePickerChangeToUndoDeque(DatePicker datePicker, LocalDate oldValue) {
        undoRedoController.pushToUndoDeque(new UndoableDatePicker(datePicker, oldValue));
        updateUndoRedoButtons();
    }

    /**
     * Function creates an UndoableEnumChoiceBox object and pushes it onto the undo Deque.
     *
     * @param choiceBox ChoiceBox: that we want to restore.
     * @param oldValue  Enum: the oldValue to restore to.
     */
    private void pushEnumChoiceBoxToUndoDeque(ChoiceBox<Enum> choiceBox, Enum oldValue) {
        undoRedoController.pushToUndoDeque(new UndoableEnumChoiceBox(choiceBox, oldValue));
        updateUndoRedoButtons();
    }

    private void pushHospitalChoiceBoxToUndoDeque(ChoiceBox<Hospital> choiceBox, Hospital oldValue) {
        undoRedoController.pushToUndoDeque(new UndoableHospitalChoiceBox(choiceBox, oldValue));
        updateUndoRedoButtons();
    }

    /**
     * Function creates an UndoableProfileCancel object and pushes it onto the undo Deque.
     */
    private void pushProfileCancelEventToUndoDeque() {
        undoRedoController.pushToUndoDeque(new UndoableProfileCancel(this));
        updateUndoRedoButtons();
    }

    /**
     * Function creates an UndoableProfile object and pushes it onto the undo Deque.
     */
    private void pushProfileEditEventToUndoDeque() {
        undoRedoController.pushToUndoDeque(new UndoableProfile(currentProfile, currentProfile, this, new
                UndoableButton(btnRegisterDonor, new UndoableTabChange(profileTabPaneSelectionModel,
                profileTabPaneSelectionModel.getSelectedItem()), undoRedoPushingBlocked),
                new UndoableButton(btnRegisterReceiver, new UndoableTabChange(profileTabPaneSelectionModel,
                        profileTabPaneSelectionModel.getSelectedItem()), undoRedoPushingBlocked)));
        updateUndoRedoButtons();
    }

    /**
     * Function creates an UndoableStringDualListAndProfile object and pushes it onto the undo Deque.
     *
     * @param list1   ObservableList of Strings:
     * @param list2   ObservableList of Strings:
     * @param profile Profile:
     */
    private void pushStringDualListAndProfileToUndoDeque(ObservableList<String> list1, ObservableList<String> list2,
                                                         Profile profile) {
        undoRedoController.pushToUndoDeque(new UndoableStringDualListAndProfile(list1, list2, profile));
        updateUndoRedoButtons();
    }

    /**
     * Function creates an UndoableTextField object and pushes it onto the undo Deque.
     *
     * @param textField TextField: that we want to restore.
     * @param oldValue  String: the oldValue to restore to.
     */
    private void pushTextFieldChangeToUndoDeque(TextField textField, String oldValue) {
        undoRedoController.pushToUndoDeque(new UndoableTextField(textField, oldValue));
        updateUndoRedoButtons();
    }

    //==================================================================================================================
    /* @FXML related                                                                                                  */
    //==================================================================================================================

    /**
     * Clears the cache which stores the drug interactions and displays a confirmation alert.
     */
    @FXML
    private void emptyCache() {
        CacheController.emptyCache();
        AlertDialog.showAndWait("Cache Cleared", "Cache which was storing the medications has been cleared!");
    }

    /**
     * Bind the event to the event with the logout method.
     */
    @FXML
    private void logoutEvent() {
        logout();
    }

    /**
     * Function handles when the registerDonor button is clicked.
     */
    @FXML
    private void registerDonorButtonClick() {
        undoRedoPushingBlocked = true;
        registerButtonClicked(btnRegisterDonor, donationTab);
        undoRedoPushingBlocked = false;
        updateUndoRedoButtons();
    }

    /**
     * Function handles when the registerReceiver button is clicked.
     */
    @FXML
    private void registerReceiverButtonClick() {
        undoRedoPushingBlocked = true;
        registerButtonClicked(btnRegisterReceiver, receiverTab);
        undoRedoPushingBlocked = false;
        updateUndoRedoButtons();
    }

    //==================================================================================================================
    /* Profile related                                                                                                */
    //==================================================================================================================

    /**
     * Set the values in the edit fields to the related profile fields.
     */
    private void applyProfileChanges() {
        this.currentProfile.setFirstName(firstNameEdit.getText().trim());
        this.currentProfile.setMiddleName(middleNameEdit.getText().trim());
        this.currentProfile.setLastName(lastNameEdit.getText().trim());
        this.currentProfile.setDateOfBirth(dateOfBirthPicker.getValue());
        this.currentProfile.setAlias(aliasEdit.getText().trim());

        GenderEnum preferredGender = (GenderEnum) preferredGenderChoiceBox.getValue();
        if (preferredGender == UserController.GENDER_NOT_SET) {
            this.currentProfile.setGender(UserController.GENDER_NOT_SET);
        } else {
            this.currentProfile.setGender(preferredGender);
        }

        GenderEnum birthGender = (GenderEnum) birthGenderChoiceBox.getValue();
        if (birthGender != ProfileController.BIRTH_GENDER_NOT_SET) {
            this.currentProfile.setBirthGender(birthGender);
        } else {
            this.currentProfile.setBirthGender(ProfileController.BIRTH_GENDER_NOT_SET);
        }

        this.currentProfile.setBloodType(((BloodTypeEnum) bloodTypeChoiceBox.getValue()));

        applyDonorOrgans();
        applyReceivingOrgans();
        applyProcedures();

        LocalDate dateOfDeath = dateOfDeathPicker.getValue();
        Hospital hospital = hospitalOfDeathChoiceBox.getSelectionModel().getSelectedItem();
        Time timeOfDeath = null;
        //so we can now remove the death time
        if (!hrTimeOfDeathTextField.getText().equals("") && !minTimeOfDeathTextField.getText().equals("") && !secTimeOfDeathTextField.getText().equals("")) {
            timeOfDeath = new Time(Integer.parseInt(hrTimeOfDeathTextField.getText().trim()), Integer.parseInt(minTimeOfDeathTextField.getText().trim()),
                    Integer.parseInt(secTimeOfDeathTextField.getText().trim()));
        }

        Death death = ProfileController.DEATH_NOT_SET;
        if (setDod) {
            death = new Death(dateOfDeath, hospital, timeOfDeath);
        } else if (dateOfDeath == null && hospital == null) {
            // Death was not selected
        } else {
            LOGGER.severe("Unexpected flow in logic, date and hospital must either both be null or" +
                    "have values. Date of death value: " + dateOfDeath + " Hospital value: " + hospital);
        }

        this.currentProfile.setDeath(death);
        this.currentProfile.setWeight(Float.parseFloat(weightEdit.getText()));
        this.currentProfile.setHeight(Float.parseFloat(heightEdit.getText()));
        this.currentProfile.setSmoker(smokerCheckBox.isSelected());
        this.currentProfile.setAddress(addressEdit.getText());

        RegionEnum region = (RegionEnum) regionChoiceBox.getValue();
        if (region == UserController.REGION_NOT_SET) {
            this.currentProfile.setRegion(UserController.REGION_NOT_SET);
        } else {
            this.currentProfile.setRegion(region);
        }

        this.currentProfile.setAlcoholConsumption((AlcoholConsumptionEnum) alcoholConsumptionChoiceBox.getValue());
        this.currentProfile.setBloodPressure(bloodPressureEdit.getText());

    }

    /**
     * Function applies the edits of the profile by retrieving the values from the editable profile fields and applying
     * the changes to the active profile.
     */
    private void applyProfileEditEvent() {
        Alert editProblemAlert = new Alert(Alert.AlertType.ERROR, "Some of the changes made are in an incorrect" +
                " format. Please check red highlighted fields");
        editProblemAlert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        if (isValidProfileEdit()) {
            try {
                applyProfileChanges();
                ResponseEntity responseEntity = ProfileRequests.getInstance()
                        .putProfile(this.currentProfile, AuthToken.getInstance().getToken());
                if (responseEntity.getStatusCode() == HttpStatus.NO_CONTENT) {
                    pushProfileEditEventToUndoDeque();
                    int startingNumChanges = currentProfile.getChanges().size();
                    editMenuItem.setVisible(true);
                    setProfileTextElementsVisible();
                    hide();
                    setProfileFieldsText();
                    updateProfileHistoryOfChanges();
                    isEditing = false;
                    int endingNumChanges = currentProfile.getChanges().size();
                    donorAnchorPaneController.editEndDonorOrgansEvent();
                    organReceiverAnchorPaneController.hide();
                    // Relying on the history of changes to determine if the profile has been edited
                    boolean profileHasBeenEdited = startingNumChanges != endingNumChanges;
                    updateUnsavedChangesIndicator(true, isEditing, profileHasBeenEdited);
                    PushNotification pushNotification = new PushNotification("Changes Applied",
                            "Changes to the profile have been saved.");
                    pushNotification.show();
                    setAutoRequestBlocked(false);
                }
            } catch (HttpClientErrorException e) {
                switch (e.getStatusCode()) {
                    case UNAUTHORIZED:
                        editProblemAlert.setContentText("Another session has been logged in, please log in again");
                        editProblemAlert.showAndWait();
                        LOGGER.severe(e.getMessage());
                        gotoLogin();
                        ClinicianSceneController clinicianSceneController =
                                ControllerAccess.getClinicianSceneController();
                        AdminSceneController adminSceneController = ControllerAccess.getAdminSceneController();
                        if (clinicianSceneController == null && adminSceneController != null) {
                            adminSceneController.kickUser();
                        } else if (adminSceneController == null && clinicianSceneController != null) {
                            clinicianSceneController.kickUser();
                        } else {
                            kickUser();
                        }
                        break;
                    default:
                        editProblemAlert.setContentText(e.getMessage());
                        editProblemAlert.showAndWait();
                        LOGGER.severe(e.getMessage());
                }
            } catch (HttpServerErrorException | ResourceAccessException e) {
                handleCriticalError(e);
            }
        } else {
            if (btnRegisterDonor.getText().equals("Unregister") &&
                    !donorAnchorPaneController.hasDonorOrgansSelected()) {
                editProblemAlert.setContentText("You have registered as a donor but have no organs listed, please " +
                        "list the organs you wish to donate");
            } else if (btnRegisterReceiver.getText().equals("Unregister") && !organReceiverAnchorPaneController
                    .hasReceiverOrgansSelected()) {
                editProblemAlert.setContentText("You have registered as an organ receiver but have no organs listed, " +
                        "please list the organs you wish to receive");
            }
            editProblemAlert.showAndWait();
        }
    }


    /**
     * Function disregards any changes made in the edit TextFields and makes them invisible, then makes the profile
     * details Text elements visible again.
     *
     * @param blockStackPush boolean that indicates whether objects are being pushed to the undo deque when changes
     *                       occur
     */
    public void cancelProfileEditEvent(boolean blockStackPush) {
        undoRedoPushingBlocked = blockStackPush;
        editMenuItem.setVisible(true);
        isEditing = false;

        hide();
        setProfileTextElementsVisible();

        updateUnsavedChangesIndicator(false, isEditing);

        if (!undoRedoPushingBlocked) {
            pushProfileCancelEventToUndoDeque();
        }
        initializeButtons();
        undoRedoPushingBlocked = false;
        setAutoRequestBlocked(false);
    }

    /**
     * Function hides the original displayed information and brings forward the editable fields and buttons and
     * instantiates the fields with current donor values.
     *
     * @param undo boolean indicating whether an undo event is currently happening.
     */
    public void editProfileEvent(boolean undo) {
        setAutoRequestBlocked(true);
        editMenuItem.setVisible(false);
        setProfileTextElementsInvisible();
        reveal();

        ObservableList<Enum> bloodTypeOptions = FXCollections.observableArrayList();
        bloodTypeOptions.addAll(BloodTypeEnum.values());

        ObservableList<Enum> genderOptions = FXCollections.observableArrayList();
        genderOptions.addAll(GenderEnum.values());

        ObservableList<Enum> birthGenderOptions = FXCollections.observableArrayList();
        birthGenderOptions.addAll(GenderEnum.NOT_SET, GenderEnum.MALE, GenderEnum.FEMALE);

        ObservableList<OrganEnum> donatableOrgans = FXCollections.observableArrayList();
        Collections.addAll(donatableOrgans, OrganEnum.values());

        undoRedoPushingBlocked = true;
        updateUndoRedoPushingBlockedForInjectedControllers();

        if (!undo) {
            firstNameEdit.setText(currentProfile.getFirstName());

            if (currentProfile.getMiddleName() == null) {
                middleNameEdit.setText("");
            } else {
                middleNameEdit.setText(currentProfile.getMiddleName());
            }

            if (currentProfile.getAlias() == null) {
                aliasEdit.setText("");
            } else {
                aliasEdit.setText(currentProfile.getAlias());
            }

            if (currentProfile.getLastName() == null) {
                lastNameEdit.setText("");
            } else {
                lastNameEdit.setText(currentProfile.getLastName());
            }

            if (currentProfile.getDateOfBirth() != null) {
                dateOfBirthPicker.setValue(currentProfile.getDateOfBirth());
            }

            preferredGenderChoiceBox.setItems(genderOptions);
            birthGenderChoiceBox.setItems(birthGenderOptions);

            preferredGenderChoiceBox.setValue(currentProfile.getGender());
            birthGenderChoiceBox.setValue(currentProfile.getBirthGender());

            bloodTypeChoiceBox.setItems(bloodTypeOptions);
            bloodTypeChoiceBox.setValue(currentProfile.getBloodType());

            initializeButtons();
            if (currentProfile.getDeath() != null) {
                dateOfDeathPicker.setValue(currentProfile.getDeath().getDateOfDeath());

                if (currentProfile.getDeath().getTimeOfDeath() != null) {
                    hrTimeOfDeathTextField.setText(String.valueOf(currentProfile.getDeath().getTimeOfDeath().getHours()));
                    minTimeOfDeathTextField.setText(String.valueOf(currentProfile.getDeath().getTimeOfDeath().getMinutes()));
                    secTimeOfDeathTextField.setText(String.valueOf(currentProfile.getDeath().getTimeOfDeath().getSeconds()));
                }

                for (Hospital hospital : hospitals) {
                    if (hospital.equals(currentProfile.getDeath().getHospital())) {
                        hospitalOfDeathChoiceBox.setValue(hospital);
                    }
                }


            } else {
                dateOfDeathPicker.setValue(null);
                hospitalOfDeathChoiceBox.setValue(null);
            }

            try {
                String weightEditString = String.format("%.2f", currentProfile.getWeight());
                weightEdit.setText(weightEditString);
            } catch (NullPointerException e) {
                weightEdit.setText("");
            }

            try {
                String heightEditString = String.format("%.2f", currentProfile.getHeight());
                heightEdit.setText(heightEditString);
            } catch (NullPointerException e) {
                heightEdit.setText("");
            }

            if (currentProfile.isSmoker()) {
                smokerCheckBox.setSelected(true);
            } else {
                smokerCheckBox.setSelected(false);
            }

            if (currentProfile.getBloodPressure() == null) {
                bloodPressureEdit.setText("");
            } else {
                try {
                    bloodPressureEdit.setText(currentProfile.getBloodPressure());
                } catch (NullPointerException e) {
                    bloodPressureEdit.setText("");
                }
            }

            alcoholConsumptionChoiceBox.setValue(currentProfile.getAlcoholConsumption());

            if (currentProfile.getAddress() != null) {
                try {
                    addressEdit.setText(currentProfile.getAddress());
                } catch (NullPointerException e) {
                    addressEdit.setText("");
                }
            } else {
                addressEdit.setText("");
            }

            ObservableList<Enum> regionEnumOptions = FXCollections.observableArrayList();
            regionEnumOptions.addAll(RegionEnum.values());
            regionChoiceBox.setItems(regionEnumOptions);
            regionChoiceBox.setValue(currentProfile.getRegion());
        }

        // Just started editing, no edits made yet to the original profile
        isEditing = true;

        updateUnsavedChangesIndicator(false, isEditing);
        undoRedoPushingBlocked = false;
        updateUndoRedoPushingBlockedForInjectedControllers();
    }

    /**
     * Setup the buttons with their event handlers.
     */
    private void initializeButtons() {
        initializeMedicationsTabButtonEventHandlers();
        if (currentProfile.isDonor()) {
            btnRegisterDonor.setText("Unregister");
            donorStatus.setText("Registered Donor");
        } else {
            btnRegisterDonor.setText("Register");
            donorStatus.setText("Not Registered");
        }

        if (currentProfile.isReceiver()) {
            btnRegisterReceiver.setText("Unregister");
            receiverStatusText.setText("Registered Receiver");
        } else {
            btnRegisterReceiver.setText("Register");
            receiverStatusText.setText("Not Registered");
        }
    }

    /**
     * Initializes the event handlers for the menu components of the profile scene.
     */
    private void initializeMenuEventHandlers() {
        editMenuItem.setOnAction(event -> editProfileEvent(false));
        applyEditButton.setOnAction(event -> applyProfileEditEvent());
        editCancelButton.setOnAction(event -> cancelProfileEditEvent(false));
    }

    /**
     * Returns true if all editable profile fields are valid.
     *
     * @return Returns True if the profile's changes can be saved.
     */
    private boolean isValidProfileEdit() {
        if (LoginSceneController.isActiveClinician() || LoginSceneController.isActiveAdmin()) {
            return acceptableFirstName && acceptableLastName && acceptableDob && acceptableDod && acceptableWeight &&
                    acceptableAlias && acceptableHeight && acceptableBloodPressure && acceptableAddress &&
                    ((donorAnchorPaneController.hasDonorOrgansSelected() &&
                            btnRegisterDonor.getText().equals("Unregister")) ||  (btnRegisterDonor.getText().equals(
                                    "Register"))) && ((btnRegisterReceiver.getText() .equals("Unregister") &&
                    organReceiverAnchorPaneController.hasReceiverOrgansSelected()) || btnRegisterReceiver.getText().equals("Register"));
        } else {
            return acceptableFirstName && acceptableLastName && acceptableDob && acceptableWeight &&
                    acceptableAlias && acceptableHeight && acceptableBloodPressure && acceptableAddress &&
                    ((donorAnchorPaneController.hasDonorOrgansSelected() &&
                            btnRegisterDonor.getText().equals("Unregister")) ||
                            (btnRegisterDonor.getText().equals("Register")));
        }
    }

    private void populateGuiElements() {
        organReceiverAnchorPaneController.setCurrentReceiverProfile(currentProfile);
        donorAnchorPaneController.setCurrentDonorProfile(currentProfile);
        userMedicalProceduresTabController.setCurrentProfile(currentProfile);
        setProfileTextElementsVisible();
        hide();
//        initializeMedicationLists();
        initializeButtons();
    }

    /**
     * Toggles the text of the clicked button between 'Register' and 'Unregister' and disables the appropriate tab if
     * the button is set to 'Unregister'. When the text is changed to 'Unregister' the passed in tab is shown.
     *
     * @param button The button clicked.
     * @param tab    The related tab to the button click.
     */
    private void registerButtonClicked(Button button, Tab tab) {
        this.undoRedoController
                .pushToUndoDeque(new UndoableButton(button, new UndoableTabChange(profileTabPaneSelectionModel,
                        profileTabPaneSelectionModel.getSelectedItem()), undoRedoPushingBlocked));
        switch (button.getText()) {
            case "Unregister":
                button.setText("Register");
                receiverStatusText.setText("Not Registered");
                tab.setDisable(true);
                break;
            case "Register":
                button.setText("Unregister");
                receiverStatusText.setText("Registered Receiver");
                profileTabPane.getSelectionModel().select(tab);
                tab.setDisable(false);
                break;
        }
    }

    /**
     * Function that sets the Text elements in the profile GUI to display the profile's details.
     */
    public void setProfileFieldsText() {
        donorCurrentMedications.clear();
        donorPreviousMedications.clear();
        donorCurrentMedications.addAll(currentProfile.getCurrentMedications());
        donorPreviousMedications.addAll(currentProfile.getPreviousMedications());

        if (currentProfile.getClinicianName() != null) {
            clinicianText.setText(currentProfile.getClinicianName());
        } else {
            clinicianText.setText("None");
        }
        userName.setText(currentProfile.getUsername());
        firstName.setText(currentProfile.getFirstName());

        try {
            alias.setText(currentProfile.getAlias());
        } catch (NullPointerException e) {
            alias.setText("N/A");
        }

        try {
            middleName.setText(currentProfile.getMiddleName());
        } catch (NullPointerException e) {
            middleName.setText("N/A");
        }

        try {
            lastName.setText(currentProfile.getLastName());
        } catch (NullPointerException e) {
            lastName.setText("N/A");
        }

        dateOfBirth.setText(currentProfile.getDateOfBirth().toString());

        try {
            gender.setText(currentProfile.getGender().toString());
        } catch (NullPointerException e) {
            gender.setText("N/A");
        }

        try {
            birthGender.setText(currentProfile.getBirthGender().toString());
        } catch (NullPointerException e) {
            birthGender.setText("N/A");
        }

        try {
            bloodType.setText(currentProfile.getBloodType().toString());
        } catch (NullPointerException e) {
            bloodType.setText("N/A");
        }

        try {
            hrTimeOfDeathText.setText(String.valueOf(currentProfile.getDeath().getTimeOfDeath().getHours()));
            minTimeOfDeathText.setText(String.valueOf(currentProfile.getDeath().getTimeOfDeath().getMinutes()));
            secTimeOfDeathText.setText(String.valueOf(currentProfile.getDeath().getTimeOfDeath().getSeconds()));
            separatorTimeOfDeathText.setVisible(true);
            separator2TimeOfDeathText.setVisible(true);

        } catch (NullPointerException e) {
            hrTimeOfDeathText.setText("");
            minTimeOfDeathText.setText("");
            secTimeOfDeathText.setText("");
            separatorTimeOfDeathText.setVisible(false);
            separator2TimeOfDeathText.setVisible(false);
        }


        if (currentProfile.isDonor() || btnRegisterDonor.getText().equals("Unregister")) {
            donorStatus.setText("Registered Donor");
        } else {
            donorStatus.setText("Not Registered");
            donationTab.setDisable(true);
        }

        if (currentProfile.isReceiver() || btnRegisterReceiver.getText().equals("Unregister")) {
            receiverStatusText.setText("Registered Receiver");
        } else {
            receiverStatusText.setText("Not Registered");
            receiverTab.setDisable(true);
        }

        try {
            dateOfDeath.setText(currentProfile.getDeath().getDateOfDeath().toString());
            hospitalOfDeath.setText(currentProfile.getDeath().getHospital().getName());
        } catch (NullPointerException e) {
            dateOfDeath.setText("N/A");
            hospitalOfDeath.setText("N/A");
        }

        try {
            String weightString = String.format("%.2f", currentProfile.getWeight());
            weight.setText(weightString);
        } catch (NullPointerException e) {
            weight.setText("N/A");
        }

        try {
            String heightString = String.format("%.2f", currentProfile.getHeight());
            height.setText(heightString);
        } catch (NullPointerException e) {
            height.setText("N/A");
        }

        try {
            bmi.setText(String.valueOf(currentProfile.getBmi()));
        } catch (NullPointerException e) {
            bmi.setText("N/A");
        }
        if (currentProfile.isSmoker()) {
            smoker.setText("YES");
        } else {
            smoker.setText("NO");
        }
        try {
            bloodPressure.setText(currentProfile.getBloodPressure());
        } catch (NullPointerException e) {
            bloodPressure.setText("N/A");
        }
        try {
            alcohol.setText(String.valueOf(currentProfile.getAlcoholConsumption()));
        } catch (NullPointerException e) {
            alcohol.setText("N/A");
        }

        try {
            address.setText(currentProfile.getAddress());
        } catch (NullPointerException e) {
            address.setText("N/A");
        }
        try {
            region.setText(currentProfile.getRegion().toString());
        } catch (NullPointerException e) {
            region.setText("N/A");
        }

        dateCreated.setText(currentProfile.getCreatedDate().toString());

        try {
            lastModified.setText(currentProfile.getModifiedDate().toString());
        } catch (NullPointerException e) {
            lastModified.setText("N/A");
        }

        if (currentProfile.getDeath() == null) {
            ageText.setText("" + (currentProfile.getAge()));
            ageText.setVisible(true);
            ageAtDeathText.setVisible(false);
        } else {
            ageAtDeathText.setVisible(true);
            ageText.setVisible(false);
            ageAtDeathText.setText("" + currentProfile.getAge());
        }
    }

    /**
     * Function sets all the profile detail Text elements invisible.
     */
    public void setProfileTextElementsInvisible() {
        alias.setVisible(false);
        firstName.setVisible(false);
        middleName.setVisible(false);
        lastName.setVisible(false);
        dateOfBirth.setVisible(false);
        gender.setVisible(false);
        birthGender.setVisible(false);
        bloodType.setVisible(false);
        donorStatus.setVisible(false);
        dateOfDeath.setVisible(false);
        weight.setVisible(false);
        height.setVisible(false);
        smoker.setVisible(false);
        bloodPressure.setVisible(false);
        alcohol.setVisible(false);
        address.setVisible(false);
        region.setVisible(false);
        hospitalOfDeath.setVisible(false);
        if (LoginSceneController.isActiveClinician() || LoginSceneController.isActiveAdmin()) {
            receiverStatusText.setVisible(false);
        }
    }

    /**
     * Function sets the death function elements visible based on role.
     */
    private void setDeathElementsVisibleBasedOnRole() {
        dateOfDeathPicker.setVisible(false);
        dateOfDeath.setVisible(true);
        hospitalOfDeathChoiceBox.setVisible(false);
        hospitalOfDeath.setVisible(true);
        clearDateOfDeathButton.setVisible(false);
        hrTimeOfDeathTextField.setVisible(false);
        hrTimeOfDeathText.setVisible(true);
        minTimeOfDeathTextField.setVisible(false);
        minTimeOfDeathText.setVisible(true);
        secTimeOfDeathTextField.setVisible(false);
        secTimeOfDeathText.setVisible(true);

        if (LoginSceneController.isActiveClinician() || LoginSceneController.isActiveAdmin()) {
            dateOfDeath.setVisible(false);
            dateOfDeathPicker.setVisible(true);
            hospitalOfDeathChoiceBox.setVisible(true);
            hospitalOfDeath.setVisible(false);
            clearDateOfDeathButton.setVisible(true);
            hrTimeOfDeathTextField.setVisible(true);
            hrTimeOfDeathText.setVisible(false);
            minTimeOfDeathTextField.setVisible(true);
            minTimeOfDeathText.setVisible(false);
            secTimeOfDeathTextField.setVisible(true);
            secTimeOfDeathText.setVisible(false);
        }
    }


    /**
     * Function sets all the profile detail Text elements visible.
     */
    private void setProfileTextElementsVisible() {
        userName.setVisible(true);
        firstName.setVisible(true);
        middleName.setVisible(true);
        lastName.setVisible(true);
        alias.setVisible(true);
        dateOfBirth.setVisible(true);
        gender.setVisible(true);
        birthGender.setVisible(true);
        bloodType.setVisible(true);
        donorStatus.setVisible(true);
        dateOfDeath.setVisible(true);
        weight.setVisible(true);
        height.setVisible(true);
        smoker.setVisible(true);
        bloodPressure.setVisible(true);
        alcohol.setVisible(true);
        address.setVisible(true);
        region.setVisible(true);
        receiverStatusText.setVisible(true);
        hospitalOfDeath.setVisible(true);
        hrTimeOfDeathText.setVisible(true);
        minTimeOfDeathText.setVisible(true);
        secTimeOfDeathText.setVisible(true);
        if (!currentProfile.isReceiver()) {
            receiverTab.setDisable(true);
        }
    }

    /**
     * Function sets all the profile details edit TextFields invisible.
     */
    @Override
    public void hide() {
        firstNameEdit.setVisible(false);
        middleNameEdit.setVisible(false);
        lastNameEdit.setVisible(false);
        aliasEdit.setVisible(false);
        dateOfBirthPicker.setVisible(false);
        preferredGenderChoiceBox.setVisible(false);
        birthGenderChoiceBox.setVisible(false);
        btnRegisterDonor.setVisible(false);
        btnRegisterReceiver.setVisible(false);
        dateOfDeathPicker.setVisible(false);
        weightEdit.setVisible(false);
        heightEdit.setVisible(false);
        bloodPressureEdit.setVisible(false);
        addressEdit.setVisible(false);
        regionChoiceBox.setVisible(false);
        bloodTypeChoiceBox.setVisible(false);
        btnRegisterDonor.setVisible(false);
        btnRegisterReceiver.setVisible(false);
        smokerCheckBox.setVisible(false);
        alcoholConsumptionChoiceBox.setVisible(false);
        editCancelButton.setVisible(false);
        applyEditButton.setVisible(false);
        donorAnchorPaneController.editEndDonorOrgansEvent();
        organReceiverAnchorPaneController.hide();
        userMedicalProceduresTabController.hide();
        medicalHistoryController.hide();
        newMedBtn.setVisible(false);
        deleteMedBtn.setVisible(false);
        moveToCurrentMedBtn.setVisible(false);
        moveToPastMedBtn.setVisible(false);
        medicationSearch.setVisible(false);
        currentMedication.setEditable(false);
        previousMedication.setEditable(false);
        hospitalOfDeathChoiceBox.setVisible(false);
        clearDateOfDeathButton.setVisible(false);
        hrTimeOfDeathTextField.setVisible(false);
        minTimeOfDeathTextField.setVisible(false);
        secTimeOfDeathTextField.setVisible(false);

        if (currentProfile.getDeath() == null) {
            separatorTimeOfDeathText.setVisible(false);
            separator2TimeOfDeathText.setVisible(false);
        }
    }

    /**
     * Function sets all the profile details edit TextFields visible.
     */
    @Override
    public void reveal() {
        hrTimeOfDeathTextField.setVisible(true);
        minTimeOfDeathTextField.setVisible(true);
        secTimeOfDeathTextField.setVisible(true);
        hospitalOfDeathChoiceBox.setVisible(true);
        clearDateOfDeathButton.setVisible(true);
        firstNameEdit.setVisible(true);
        middleNameEdit.setVisible(true);
        lastNameEdit.setVisible(true);
        aliasEdit.setVisible(true);
        dateOfBirthPicker.setVisible(true);
        preferredGenderChoiceBox.setVisible(true);
        birthGenderChoiceBox.setVisible(true);
        setDeathElementsVisibleBasedOnRole();
        weightEdit.setVisible(true);
        heightEdit.setVisible(true);
        bloodPressureEdit.setVisible(true);
        addressEdit.setVisible(true);
        regionChoiceBox.setVisible(true);
        bloodTypeChoiceBox.setVisible(true);
        btnRegisterDonor.setVisible(true);
        if (LoginSceneController.isActiveClinician() || LoginSceneController.isActiveAdmin()) {
            btnRegisterReceiver.setVisible(true);
            organReceiverAnchorPaneController.reveal();
            userMedicalProceduresTabController.reveal();
            medicalHistoryController.reveal();
        }
        smokerCheckBox.setVisible(true);
        alcoholConsumptionChoiceBox.setVisible(true);
        editCancelButton.setVisible(true);
        applyEditButton.setVisible(true);
        donorAnchorPaneController.editDonorOrgansEvent(undoRedoPushingBlocked);
        newMedBtn.setVisible(true);
        deleteMedBtn.setVisible(true);
        moveToCurrentMedBtn.setVisible(true);
        moveToPastMedBtn.setVisible(true);
        medicationSearch.setVisible(true);
        currentMedication.setEditable(true);
        previousMedication.setEditable(true);

        separatorTimeOfDeathText.setVisible(true);
        separator2TimeOfDeathText.setVisible(true);
    }

    //==================================================================================================================
    /* Donating organs related                                                                                        */
    //==================================================================================================================

    /**
     * Function applies changes made to the donating organs of the profile.
     */
    private void applyDonorOrgans() {
        switch (btnRegisterDonor.getText()) {
            case "Unregister":
                currentProfile.setIsDonor(true);
                break;
            case "Register":
                currentProfile.setIsDonor(false);
                break;
            default:
                throw new IllegalStateException("The donor button should always display either Unregister or " +
                        "Register, currently it is set to: " + btnRegisterDonor.getText() + ". Please debug why");
        }

        boolean isDonor = btnRegisterDonor.getText().equals("Unregister");
        currentProfile.setIsDonor(isDonor);

        try {
            if (isDonor) {
                currentProfile.setDonorOrgans(donorAnchorPaneController.getSelectedDonorOrgans());
            } else {
                currentProfile.setDonorOrgans(new ArrayList<>());
            }
        } catch (DoesNotExist e) {
            LOGGER.severe(e.getMessage());
        }
    }

    //==================================================================================================================
    /* Receiving organs related                                                                                       */
    //==================================================================================================================

    /**
     * Function applies changes made to the receiving organs of the profile.
     */
    private void applyReceivingOrgans() {
        switch (btnRegisterReceiver.getText()) {
            case "Unregister":
                currentProfile.setIsReceiver(true);
                break;
            case "Register":
                currentProfile.setIsReceiver(false);
                break;
            default:
                throw new IllegalStateException("The receiver button should always display either Unregister or " +
                        "Register, currently it is set to: " + btnRegisterReceiver.getText() + ". Please debug why");
        }

        boolean isReceiver = btnRegisterReceiver.getText().equals("Unregister");
        currentProfile.setIsReceiver(isReceiver);


        List<OrganEnum> selectedOrgans = organReceiverAnchorPaneController.getSelectedReceiverOrgans();
        Map<OrganEnum, String> removedOrgans = organReceiverAnchorPaneController.getRemovedReceiverOrgans();

        for (Map.Entry<Disease, OrganEnum> cured : organReceiverAnchorPaneController.getCuredDiseases().entrySet()) {
            try {
                currentProfile.cureCurrentDisease(cured.getKey());
            } catch (DoesNotExist doesNotExist) {
                LOGGER.severe(doesNotExist.getMessage());
            }
        }


        for (OrganEnum organ : selectedOrgans) {
            currentProfile.addReceivingOrgan(organ);
        }

        for (Map.Entry<OrganEnum, String> entry : removedOrgans.entrySet()) {
            OrganEnum organ = entry.getKey();
            String message = entry.getValue();

            try {
                currentProfile.removeReceiverOrgan(organ, message);
            } catch (DoesNotExist doesNotExist) {
                LOGGER.severe(doesNotExist.getMessage());
            }
        }

        if (!isReceiver) {

            currentProfile.setReceiverOrgans(new ArrayList<>());
            organReceiverAnchorPaneController.clearCheckedOrgans();
        }

    }

    //==================================================================================================================
    /* Medications tab related                                                                                        */
    //==================================================================================================================

    /**
     * Initialises all medications tab button event handlers.
     */
    private void initializeMedicationsTabButtonEventHandlers() {
        deleteMedBtn.setOnAction(event -> {
            ObservableList<String> cDrugs = currentMedication.getSelectionModel().getSelectedItems();
            ObservableList<String> pDrugs = previousMedication.getSelectionModel().getSelectedItems();

            ArrayList<String> toDelete = new ArrayList<>();

            toDelete.addAll(cDrugs);
            toDelete.addAll(pDrugs);

            if (!undoRedoPushingBlocked && medicationSearchNewValue != null) {
                pushStringDualListAndProfileToUndoDeque(this.donorCurrentMedications, this.donorPreviousMedications,
                        this.currentProfile);
            }

            currentMedication.getItems().removeAll(cDrugs);
            previousMedication.getItems().removeAll(pDrugs);

            currentProfile.deleteMedication(toDelete);

            updateProfileHistoryOfChanges();
        });

        newMedBtn.setOnAction(event -> {
            if (medicationSearchNewValue == null) {
                AlertDialog.showAndWait("Invalid Drug", "Error. Not a valid drug.");
            }
            if (!currentMedication.getItems().contains(medicationSearchNewValue) &&
                    (medicationSearchNewValue != null) &&
                    !previousMedication.getItems().contains(medicationSearchNewValue)) {
                if (!undoRedoPushingBlocked && medicationSearchNewValue != null) {
                    pushStringDualListAndProfileToUndoDeque(this.donorCurrentMedications, this
                            .donorPreviousMedications, this.currentProfile);
                }
                if (autoCompleteResults.contains(medicationSearchNewValue)) {
                    currentMedication.getItems().add(medicationSearchNewValue);
                    currentProfile.addCurrentMedication(medicationSearchNewValue);
                    updateProfileHistoryOfChanges();
                } else {
                    AlertDialog.showAndWait("Invalid Drug", "Error. Not a valid drug.");
                }
            }
            previousMedication.refresh();
            currentMedication.refresh();
        });

        moveToPastMedBtn.setOnAction(event -> {
            ArrayList<String> toChange = new ArrayList<>(currentMedication.getSelectionModel().getSelectedItems());

            if (!undoRedoPushingBlocked && medicationSearchNewValue != null) {
                pushStringDualListAndProfileToUndoDeque(this.donorCurrentMedications, this.donorPreviousMedications,
                        this.currentProfile);
            }

            previousMedication.getItems().addAll(currentMedication.getSelectionModel().getSelectedItems());
            currentMedication.getItems().removeAll(currentMedication.getSelectionModel().getSelectedItems());

            currentProfile.addPreviousMedication(toChange);
            previousMedication.refresh();
            currentMedication.refresh();
            updateProfileHistoryOfChanges();
        });

        moveToCurrentMedBtn.setOnAction(event -> {
            ArrayList<String> toChange = new ArrayList<>(currentMedication.getSelectionModel().getSelectedItems());

            if (!undoRedoPushingBlocked && medicationSearchNewValue != null) {
                pushStringDualListAndProfileToUndoDeque(this.donorCurrentMedications, this.donorPreviousMedications,
                        this.currentProfile);
            }
            currentMedication.getItems().addAll(previousMedication.getSelectionModel().getSelectedItems());
            previousMedication.getItems().removeAll(previousMedication.getSelectionModel().getSelectedItems());

            currentProfile.moveToCurrentMedication(toChange);
            previousMedication.refresh();
            currentMedication.refresh();
            updateProfileHistoryOfChanges();
        });
    }

    /**
     * Setup the lists that show the current and previous medications.
     */
    private void initializeMedicationLists() {
        previousMedication.setItems(donorPreviousMedications);
        previousMedication.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        previousMedication.setOnMouseClicked(event -> selection(previousMedication.getSelectionModel()));

        currentMedication.setItems(donorCurrentMedications);
        currentMedication.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        currentMedication.setOnMouseClicked(event -> selection(currentMedication.getSelectionModel()));
    }

    /**
     * Loads the diseases tables when the user clicks on the Medical History Disease tab Loads the procedures tables
     * when the user clicks on the Medical History Procedure tab
     * <p>
     * Calls the populateTables() function inside the associated tab to populate the tables of medical history and
     * procedures
     * <p>
     * In the Medical History Procedure tab, will also pass information about the organs for donation that might be
     * affected by the procedure.
     */
    private void propagatedProfileData() {
        previousMedication.refresh();
        currentMedication.refresh();
        medicalHistoryController.populateTables();
        userMedicalProceduresTabController.setOrgansForDonation(currentProfile.getDonorOrgans());
        userMedicalProceduresTabController.populateTables();
    }

    /**
     * Given a selectionModel (Just a model that stores what is currently selected in a listView) it will decide if
     * active Ingredients needs to be shown or if the drugs selected need to be compared.
     *
     * @param selectionModel selection model of currently selected medications
     */
    private void selection(MultipleSelectionModel<String> selectionModel) {
        int numSelected = currentMedication.getSelectionModel().getSelectedItems().size();
        numSelected += previousMedication.getSelectionModel().getSelectedItems().size();
        if (numSelected == 0) {
            //This should never trigger, but just in case..
            drugIngredients.setText("");
            drugInteractions.setText("");
        } else if (numSelected == 1) {
            drugInteractions.setText("");
            medicationToGetIngredient = selectionModel.getSelectedItem();
            threadPool.submit(activeIngredients);
        } else if (numSelected == 2) {
            drugIngredients.setText("");
            threadPool.submit(compare);
        } else {
            drugInteractions.setText("");
            drugIngredients.setText("");
        }
    }

    //==================================================================================================================
    /* History tab related                                                                                            */
    //==================================================================================================================

    /**
     * Method will retrieve all the changes that has been made to the current donor and will update the display showing
     * the history.
     */
    public void updateProfileHistoryOfChanges() {
        StringBuilder history = new StringBuilder();
        String newLineChar = System.getProperty("line.separator");
        for (String entry : currentProfile.getChanges()) {
            history.append(entry).append(newLineChar);
        }

        historyTabTitle.setText("History of changes for " + currentProfile.getFirstName() + " " +
                currentProfile.getLastName());
        historyTextArea.setWrapText(true);
        historyTextArea.setText(history.toString());
    }


    //==================================================================================================================
    /* Procedures tab related                                                                                         */
    //==================================================================================================================

    /**
     * Function applies procedure changes made when the user clicks the apply button after editing.
     */
    private void applyProcedures() {
        userMedicalProceduresTabController.applyChanges();
    }

    //==================================================================================================================

    /**
     * Kicks user out if they are not authenticated
     */
    public void kickUser() {
        parentStage.close();
        AutoRequestTimer.getInstance().stopTimers();
        if (AuthToken.getInstance().getRole().equals(Role.USER)) {
            AlertDialog.showAndWait("Information", "You are being logged out because your credentials are no longer valid.");
            gotoLogin();
            Platform.runLater(() -> parentStage.setTitle("Login"));
            parentStage.show();
        }
        AuthToken.getInstance().clearToken();
    }

    /**
     * Generic error handler for if anything in the application goes wrong. Basically just a more elegant way of
     * handling errors instead of the application crashing. It will log the user out so no harm to the system can
     * be done while it is in an unstable state.
     * @param e Exception Any exception that can be thrown.
     */
    public void handleCriticalError(Exception e) {
        parentStage.close();
        AutoRequestTimer.getInstance().stopTimers();
        if (AuthToken.getInstance().getRole().equals(Role.USER)) {
            AlertDialog.showAndWait("Information", "You are being logged out because your credentials are no longer valid.");
            gotoLogin();
            Platform.runLater(() -> parentStage.setTitle("Login"));
            parentStage.show();
        }
        AuthToken.getInstance().clearToken();
        LOGGER.severe(e.getMessage());
    }
}
