package seng302.Model;

import seng302.Enum.AvailableOrganRequestStatus;
import seng302.Enum.OrganEnum;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Encapsulates the information that is included in the request that is sent to nearby hospitals to notify them
 * about available organs.
 */
public class AvailableOrganNotification {

    private OrganEnum organ;
    private Profile donor;
    private Clinician sender;
    private Clinician receiver;
    private Hospital origin;
    private Hospital destination;
    private AvailableOrganRequestStatus status;
    private LocalDateTime sentTimeStamp;
    private Integer notificationId;
    private Integer conversationId;

    public AvailableOrganNotification() {
        // Used by SprintBoot
    }

    /**
     * Constructor for an available organ notification.
     *
     * @param organ       the organ that is being offered out
     * @param donor       the donor that the organ was a part of
     * @param sender      the clinician that is sending this organ
     * @param receiver    the receiving clinician for this organ
     * @param origin      The origin hospital that the organ is coming from
     * @param destination The hospital that the organ is being sent to
     * @param status      The status of the transfer.
     */
    public AvailableOrganNotification(OrganEnum organ, Profile donor, Clinician sender, Clinician receiver,
                                      Hospital origin, Hospital destination, AvailableOrganRequestStatus status,
                                      Integer conversationId) {

        if (destination.equals(origin)) {
            throw new IllegalArgumentException("The origin and destination hospitals can not be the same.");
        }

        if (sender.equals(receiver)) {
            throw new IllegalArgumentException("The sender and receiver can not be the same clinician.");
        }

        this.organ = organ;
        this.donor = donor;
        this.sender = sender;
        this.receiver = receiver;
        this.origin = origin;
        this.destination = destination;
        this.status = status;
        this.sentTimeStamp = LocalDateTime.now();
        this.conversationId = conversationId;

    }

    /**
     * Constructor for an available organ notification.
     *
     * @param organ            The organ to be sent
     * @param donor            The donor the organ is coming out of
     * @param sender           The clinician managing the organ
     * @param receiver         The receiving clinician
     * @param origin           The hospital the organ is coming from
     * @param destination      The hospital the organ is going to
     * @param status           The status of the notification
     * @param createdTimeStamp The timestamp of when the notification was created
     * @param notificationId               The ID of the notification
     */
    public AvailableOrganNotification(OrganEnum organ, Profile donor, Clinician sender, Clinician receiver,
                                      Hospital origin, Hospital destination, AvailableOrganRequestStatus status,
                                      LocalDateTime createdTimeStamp, Integer notificationId, Integer conversationId) {

        if (destination.equals(origin)) {
            throw new IllegalArgumentException("The origin and destination hospitals can not be the same.");
        }

        if (sender.equals(receiver)) {
            throw new IllegalArgumentException("The sender and receiver can not be the same clinician.");
        }

        this.organ = organ;
        this.donor = donor;
        this.sender = sender;
        this.receiver = receiver;
        this.origin = origin;
        this.destination = destination;
        this.status = status;
        this.sentTimeStamp = createdTimeStamp;
        this.notificationId = notificationId;
        this.conversationId = conversationId;

    }


    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }

    public void setStatus(AvailableOrganRequestStatus status) {
        this.status = status;
    }

    public Integer getNotificationId() {
        return notificationId;
    }

    public OrganEnum getOrgan() {
        return organ;
    }

    public Profile getDonor() {
        return donor;
    }

    public Clinician getSender() {
        return sender;
    }

    public Clinician getReceiver() {
        return receiver;
    }

    public Hospital getOrigin() {
        return origin;
    }

    public Hospital getDestination() {
        return destination;
    }

    public AvailableOrganRequestStatus getStatus() {
        return status;
    }

    public LocalDateTime getSentTimeStamp() {
        return sentTimeStamp;
    }

    public Integer getConversationId() {
        return conversationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AvailableOrganNotification)) return false;
        AvailableOrganNotification that = (AvailableOrganNotification) o;
        return Objects.equals(notificationId, that.notificationId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(notificationId);
    }
}
