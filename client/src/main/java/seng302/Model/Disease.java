package seng302.Model;

import java.time.LocalDate;

public class Disease {

    private String diseaseName;
    private boolean isChronicDisease;
    private boolean isDiseaseCured;
    private LocalDate dateOfDiagnosis;

    public Disease() {
        //default constructor, DO NOT DELETE THIS COMMENT. SONARQUBE COMPLAINS AND SO DOES KIRILL!!!!
    }

    public Disease(String diseaseName, boolean isChronicDisease, LocalDate dateOfDiagnosis) {
        this.diseaseName = diseaseName;
        this.isChronicDisease = isChronicDisease;
        this.isDiseaseCured = false;
        this.dateOfDiagnosis = dateOfDiagnosis;
    }

    public Disease(String diseaseName, boolean isChronicDisease, boolean isDiseaseCured, LocalDate dateOfDiagnosis) {
        this.diseaseName = diseaseName;
        this.isChronicDisease = isChronicDisease;
        this.isDiseaseCured = isDiseaseCured;
        this.dateOfDiagnosis = dateOfDiagnosis;
    }

    public String getDiseaseName() {
        return diseaseName;
    }

    public void setDiseaseName(String diseaseName) {
        this.diseaseName = diseaseName;
    }

    public boolean getIsChronicDisease() {
        return isChronicDisease;
    }

    public void setChronicDisease(boolean chronicDisease) {
        isChronicDisease = chronicDisease;
    }

    public boolean getIsDiseaseCured() {
        return isDiseaseCured;
    }

    public void setDiseaseCured(boolean diseaseCured) {
        isDiseaseCured = diseaseCured;
    }

    public LocalDate getDateOfDiagnosis() {
        return dateOfDiagnosis;
    }

    public void setDateOfDiagnosis(LocalDate dateOfDiagnosis) {
        this.dateOfDiagnosis = dateOfDiagnosis;
    }

    public String getDateOfDiagnosisString() {
        return getDateOfDiagnosis().toString();
    }

    public String getIsChronicDiseaseString() {
        if (getIsChronicDisease()) {
            return "Chronic";
        } else {
            return "";
        }
    }

    @Override
    public boolean equals(Object diseaseObj) {
        if (diseaseObj != null) {
            if (this.getClass() != diseaseObj.getClass()) {
                return false;
            } else {
                Disease otherDisease = (Disease) diseaseObj;
                return (otherDisease.getDiseaseName().equals(this.diseaseName) &&
                        otherDisease.getIsChronicDisease() == this.isChronicDisease);
            }
        } else {
            return false;
        }
    }


    @Override
    public int hashCode() {
        return this.diseaseName.hashCode() + (isChronicDisease ? 1 : 2);
    }

    @Override
    public String toString() {
        String outputString = "";

        if (isChronicDisease) {
            outputString = "(Chronic) ";
        }

        outputString += getDiseaseName() + ". Diagnosed on: " + getDateOfDiagnosis();
        return outputString.trim();
    }
}
