package seng302.Model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static java.time.temporal.ChronoUnit.DAYS;

public class CachedItem {
    private LocalDate dateStamp;
    private ArrayList<String> data;
    private List<String> params;

    /**
     * Constructor for a Cached Item.
     *
     * @param data   The arrayList of interactions between drug 1 and 2
     * @param params The drugs, age and gender of the user
     */
    public CachedItem(ArrayList<String> data, List<String> params) {
        this.dateStamp = LocalDate.now();
        this.data = data;
        this.params = params;
    }

    /**
     * Checking if a cached item has expired.
     *
     * @return true if it has, false if it hasn't
     */
    public boolean hasExpired() {
        LocalDate now = LocalDate.now();
        long daysBetween = DAYS.between(dateStamp, now);
        return daysBetween > 7;
    }

    /**
     * @return the data from the cached item This is an arrayList of the interactions between two drugs.
     */
    public ArrayList<String> getData() {
        return this.data;
    }

    /**
     * This checks to see if two sets of parameters are the same
     * The reason that we have a for i loop is that when we used to have
     * The params set  as a list of objects we had to go through each one and use .equals.
     *
     * @param params the user's params
     * @return boolean if it is a match or not. True if match, false otherwise.
     */
    public boolean matchParams(List<String> params) {
        boolean isMatch = true;
        for (int i = 0; i < params.size(); i++) {
            if (!params.get(i).equals(this.params.get(i))) {
                isMatch = false;
            }
        }
        return isMatch;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CachedItem that = (CachedItem) o;
        return Objects.equals(data, that.data) && Objects.equals(params, that.params);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dateStamp, data, params);
    }
}
