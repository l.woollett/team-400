package seng302.Model;

import seng302.Enum.OrganEnum;

import java.sql.Time;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

/**
 * This class is used to encompass different class attributes to have one class fit into the available organ table.
 * This may not be the best way to currently do this, but I am just trying what I think is the best.
 */
public class AvailableOrgan {

    private Double expiryPercentage;
    private String username;
    private LocalDate dateOfDeath;
    private Time timeOfDeath;
    private LocalDateTime dateTimeOfDeath;
    private OrganEnum organ;
    private long secondsSinceDeath;
    private long secondsToExpiry;
    private long lowerBoundSeconds;
    private long upperBoundSeconds;
    private String lowerBoundString;
    private String upperBoundString;

    private final static long YEAR_IN_SECONDS = 31536000;
    private final static long HOUR_IN_SECONDS = 3600;
    private final static long DAY_IN_SECONDS = 86400;
    private final static long MINUTE_IN_SECONDS = 60;
    private final static long NO_LOWER_BOUND = -1;
    public final static long NO_EXPIRY = -2;

    public AvailableOrgan() {
        // Default constructor for spring boot.
    }

    /**
     * Constructor for an Available Organ object.
     *
     * @param username    The username of the person that the organ has come from. Will be used to open the profile.
     * @param dateOfDeath The date which the person has died. Will be used to calculate organ expiry time.
     * @param organ       The organ that is available for donation/ transplant.
     */
    public AvailableOrgan(String username, LocalDate dateOfDeath, Time deathTime, OrganEnum organ) {
        this.username = username;
        this.dateOfDeath = dateOfDeath;
        this.timeOfDeath = deathTime;
        this.organ = organ;
        this.dateTimeOfDeath = LocalDateTime.of(dateOfDeath, timeOfDeath.toLocalTime());
        this.secondsSinceDeath = Duration.between(dateTimeOfDeath, LocalDateTime.now()).getSeconds();
        setLowerBoundSeconds();
        setUpperBoundSeconds();
        this.expiryPercentage = ((double) getSecondsSinceDeath() / (double) getUpperBoundSeconds()) * 100;
    }

    public Double getExpiryPercentage() {
        return expiryPercentage;
    }

    public void setExpiryPercentage(Double expiryPercentage) {
        this.expiryPercentage = expiryPercentage;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public LocalDate getDateOfDeath() {
        return dateOfDeath;
    }

    public void setDateOfDeath(LocalDate dateOfDeath) {
        this.dateOfDeath = dateOfDeath;
    }

    public OrganEnum getOrgan() {
        return organ;
    }

    public void setOrgan(OrganEnum organ) {
        this.organ = organ;
    }

    public LocalDateTime getDateTimeOfDeath() {
        return dateTimeOfDeath;
    }

    public Time getTimeOfDeath() {
        return timeOfDeath;
    }

    public String getLowerBoundString() {
        return lowerBoundString;
    }

    public String getUpperBoundString() {
        return upperBoundString;
    }

    /**
     * Gets the seconds that the user has been dead for, and the upper bound of the expiring time, and then
     * calculates how long the organ has left till it expires.
     */
    private void setSecondsToExpiry() {
        if (this.upperBoundSeconds == NO_EXPIRY) {
            this.secondsToExpiry = Long.MAX_VALUE; // Cant expire so MAX VALUE all the way bb
        } else {
            long expiryTime = -this.secondsSinceDeath + this.upperBoundSeconds; //Get how long the organ actually has left
            if (expiryTime < 0) { //If its already expired
                this.secondsToExpiry = Long.MIN_VALUE; //Has expired, we should make absolutely sure its never showed.
            } else {
                this.secondsToExpiry = expiryTime;
            }
        }
    }


    /**
     * Sets the lower bound for the time an organs has until it is expired (in seconds).
     * <p>
     * WARNING: Do not use this at a regular getter/ setter. This is here because we do not have an organ object, but
     * are using ENUMS instead.
     */
    private void setLowerBoundSeconds() {
        switch (this.organ) {
            case HEART:
            case LUNG:
                this.lowerBoundSeconds = 4 * HOUR_IN_SECONDS;
                this.lowerBoundString = "4 - ";
                break;
            case PANCREAS:
                this.lowerBoundSeconds = 12 * HOUR_IN_SECONDS;
                this.lowerBoundString = "12 - ";
                break;
            case LIVER:
                this.lowerBoundSeconds = 24 * HOUR_IN_SECONDS;
                this.lowerBoundString = "";
                break;
            case KIDNEY:
                this.lowerBoundSeconds = 48 * HOUR_IN_SECONDS;
                this.lowerBoundString = "48 - ";
                break;
            case CORNEA:
                this.lowerBoundSeconds = 5 * DAY_IN_SECONDS;
                this.lowerBoundString = "5 - ";
                break;
            case HEART_VALVES:
            case SKIN:
            case BONE:
                this.lowerBoundSeconds = 3 * YEAR_IN_SECONDS;
                this.lowerBoundString = "3 - ";
                break;
            default:
                this.lowerBoundSeconds = NO_LOWER_BOUND;
                this.lowerBoundString = "";
        }
    }

    /**
     * Sets the upper bound for the time an organs has until it is expired (in seconds).
     * <p>
     * WARNING: Do not use this at a regular getter/ setter. This is here because we do not have an organ object, but
     * are using ENUMS instead.
     */
    private void setUpperBoundSeconds() {
        switch (this.organ) {
            case HEART:
            case LUNG:
                this.upperBoundSeconds = 6 * HOUR_IN_SECONDS;
                this.upperBoundString = "6 hours";
                break;
            case PANCREAS:
                this.upperBoundSeconds = 24 * HOUR_IN_SECONDS;
                this.upperBoundString = "24 hours";
                break;
            case LIVER:
                this.upperBoundSeconds = 24 * HOUR_IN_SECONDS;
                this.upperBoundString = "24 hours";
                break;
            case KIDNEY:
                this.upperBoundSeconds = 72 * HOUR_IN_SECONDS;
                this.upperBoundString = "72 hours";
                break;
            case CORNEA:
                this.upperBoundSeconds = 7 * DAY_IN_SECONDS;
                this.upperBoundString = "7 days";
                break;
            case HEART_VALVES:
            case SKIN:
            case BONE:
                this.upperBoundSeconds = 10 * YEAR_IN_SECONDS;
                this.upperBoundString = "10 years";
                break;
            default:
                this.upperBoundSeconds = NO_EXPIRY;
                this.upperBoundString = "None";
        }
    }

    public long getLowerBoundSeconds() {
        return this.lowerBoundSeconds;
    }

    public long getUpperBoundSeconds() {
        return this.upperBoundSeconds;
    }

    /**
     * Gives a string representation of the time than an organ has been dead for.
     *
     * @return The string version of the organ expiry time.
     */
    public String returnDayHourMinSecondString() {
        String baseString = "%dd:%dh:%dm:%ds";
        int days = (int) TimeUnit.SECONDS.toDays(this.secondsSinceDeath);
        long hours = TimeUnit.SECONDS.toHours(this.secondsSinceDeath) - (days * 24);
        long mins = TimeUnit.SECONDS.toMinutes(this.secondsSinceDeath) - (TimeUnit.SECONDS.toHours(this.secondsSinceDeath) * MINUTE_IN_SECONDS);
        long seconds = TimeUnit.SECONDS.toSeconds(this.secondsSinceDeath) - (TimeUnit.SECONDS.toMinutes(this.secondsSinceDeath) * MINUTE_IN_SECONDS);

        return String.format(baseString, days, hours, mins, seconds);
    }

    /**
     * Returns a string of how long the organ has left to expire
     *
     * @return a string of how long a organ has left to expire
     */
    public String returnSecondsOfExpiryAsString() {
        String baseString = "%dd:%dh:%dm:%ds";
        int days = (int) TimeUnit.SECONDS.toDays(this.secondsToExpiry);
        long hours = TimeUnit.SECONDS.toHours(this.secondsToExpiry) - (days * 24);
        long minutes = TimeUnit.SECONDS.toMinutes(this.secondsToExpiry) - (TimeUnit.SECONDS.toHours(this.secondsToExpiry) * MINUTE_IN_SECONDS);
        long seconds = TimeUnit.SECONDS.toSeconds(this.secondsToExpiry) - (TimeUnit.SECONDS.toMinutes(this.secondsToExpiry) * MINUTE_IN_SECONDS);

        if (secondsToExpiry == Long.MAX_VALUE) {
            return "Does Not Expire";
        } else {
            return String.format(baseString, days, hours, minutes, seconds);
        }
    }


    public long getSecondsSinceDeath() {
        return this.secondsSinceDeath;
    }

    public String getCurrentSecondsToExpiryAsString() {
        return String.valueOf(this.secondsSinceDeath);
    }

    public String returnBoundsAsString() {
        return String.format("%s%s", this.lowerBoundString, this.upperBoundString);
    }

    /**
     * Add one tick to the seconds of expiry.
     */
    public void updateExpiry() {
        this.secondsSinceDeath++;
        this.setSecondsToExpiry();
    }

    public long getSecondsToExpiry() {
        return secondsToExpiry;
    }
}
