package seng302.Model;

/**
 * Admin class that extends clinician uses while adding extra functionality.
 */
public class Admin {

    private String username;
    private String password;

    /**
     * Dummy constructor used to make the request for admins work in AdminQueries file.
     */
    public Admin() {
    }

    public Admin(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public Admin(Admin adminToCopy) {
        this.password = adminToCopy.getPassword();
        this.username = adminToCopy.getUsername();
    }


    public String getUsername() {
        return this.username;
    }

    public void setUsername(String newName) {
        this.username = newName;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(String newPassword) {
        this.password = newPassword;
    }
}


