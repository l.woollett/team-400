package seng302.Model;

import seng302.Enum.OrganEnum;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Procedure {
    private String summary;
    private String description;
    private LocalDate dateOfProcedure;
    private List<OrganEnum> affectedOrgans;

    /**
     * Dummy constructor for de-serializing objects from server.
     */
    public Procedure() {

    }

    public Procedure(String summary, String description, LocalDate dateOfProcedure, List<OrganEnum> affectedOrgans) {
        this.summary = summary;
        this.description = description;
        this.dateOfProcedure = dateOfProcedure;
        this.affectedOrgans = affectedOrgans;
    }

    /**
     * Constructor for creating deep copies of the Procedure
     *
     * @param toCopy Procedure object to copy
     */
    public Procedure(Procedure toCopy) {
        this.affectedOrgans = new ArrayList<>(toCopy.affectedOrgans);
        this.description = toCopy.getDescription();
        this.summary = toCopy.getSummary();
        this.dateOfProcedure = toCopy.getDateOfProcedure();
    }

    public List<OrganEnum> getAffectedOrgans() {
        return affectedOrgans;
    }

    public void setAffectedOrgans(List<OrganEnum> affectedOrgans) {
        this.affectedOrgans = affectedOrgans;
    }

    public LocalDate getDateOfProcedure() {
        return dateOfProcedure;
    }

    public void setDateOfProcedure(LocalDate dateOfProcedure) {
        this.dateOfProcedure = dateOfProcedure;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Function checks whether a user is adding a duplicate procedure to the procedures lists. A Procedure is veiwed
     * as a duplicate if the summary and date of procedure.
     * @param procedureSummary String summary of what the procedure is for.
     * @param dateOfProcedure LocalDate of when the procedure will occur.
     * @return boolean indicating if the procedure is a duplicate.
     */
    public boolean isDuplicate(String procedureSummary, LocalDate dateOfProcedure) {
        return this.summary.equals(procedureSummary) && this.dateOfProcedure.equals(dateOfProcedure);
    }

    @Override
    public String toString() {
        return String
                .format("Summary: %s\nDescription: %s\nDate: %s", summary, description, dateOfProcedure.toString());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null) {
            if (this.getClass() != obj.getClass()) {
                return false;
            } else {
                Procedure otherProcedure = (Procedure) obj;
                return this.summary.equals(otherProcedure.getSummary()) &&
                        this.description.equals(otherProcedure.getDescription()) &&
                        this.dateOfProcedure.equals(otherProcedure.getDateOfProcedure()) &&
                        this.affectedOrgans.equals(otherProcedure.getAffectedOrgans());
            }
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return this.summary.hashCode() + this.description.hashCode() + this.dateOfProcedure.hashCode() +
                this.affectedOrgans.hashCode();
    }

}
