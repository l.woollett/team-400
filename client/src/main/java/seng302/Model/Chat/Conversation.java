package seng302.Model.Chat;

import seng302.Enum.ConversationStatusEnum;

import java.time.LocalDateTime;

public class Conversation {

    private Integer conversationId;
    private LocalDateTime createdTimestamp;
    private String clinicianUsername;
    private String conversationTopic;
    private LocalDateTime endTime;
    private ConversationStatusEnum status;

    public Conversation() {
        // Spring Boot constructor
    }

    public Conversation(Integer id, LocalDateTime timestamp, String creatorUsername, String topic,
                        LocalDateTime endTimestamp, ConversationStatusEnum conversationStatus) {
        this.conversationId = id;
        this.createdTimestamp = timestamp;
        this.clinicianUsername = creatorUsername;
        this.conversationTopic = topic;
        this.endTime = endTimestamp;
        this.status = conversationStatus;
    }

    public Integer getConversationId() {
        return conversationId;
    }

    public void setConversationId(Integer conversationId) {
        this.conversationId = conversationId;
    }

    public LocalDateTime getCreatedTimestamp() {
        return createdTimestamp;
    }

    public void setCreatedTimestamp(LocalDateTime createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }

    public String getClinicianUsername() {
        return clinicianUsername;
    }

    public void setClinicianUsername(String clinicianUsername) {
        this.clinicianUsername = clinicianUsername;
    }

    public String getConversationTopic() {
        return conversationTopic;
    }

    public void setConversationTopic(String topic) {
        this.conversationTopic = topic;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public ConversationStatusEnum getStatus() {
        return status;
    }

    public void setStatus(ConversationStatusEnum status) {
        this.status = status;
    }
}
