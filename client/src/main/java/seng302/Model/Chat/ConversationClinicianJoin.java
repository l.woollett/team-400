package seng302.Model.Chat;

/**
 * Class is used to send the conversation ID and associated clinician in a HTTP Request from the server to the
 * client. This class is just bootstrap can serialize and deserialize the information.
 */
public class ConversationClinicianJoin {

    private Integer conversationId;
    private String clinicianUsername;

    public ConversationClinicianJoin(){
        //Spring boot constructor
    }

    public ConversationClinicianJoin(Integer convoId, String username) {
        conversationId = convoId;
        clinicianUsername = username;
    }

    public Integer getConversationId() {
        return conversationId;
    }

    public String getClinicianUsername() {
        return clinicianUsername;
    }


}
