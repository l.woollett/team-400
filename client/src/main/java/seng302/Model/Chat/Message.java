package seng302.Model.Chat;

import java.time.LocalDateTime;

public class Message {
    private Integer messageId;
    private LocalDateTime timestamp;
    private String senderUsername;
    private String messageText;
    private int conversationId;

    public Message() {
        // Spring boot constructor
    }

    public Message(Integer id, LocalDateTime timestamp, String username, String messageText, int conversationId) {
        this.messageId = id;
        this.timestamp = timestamp;
        this.senderUsername = username;
        this.messageText = messageText;
        this.conversationId = conversationId;
    }

    public Integer getMessageId() {
        return messageId;
    }

    public void setMessageId(Integer messageId) {
        this.messageId = messageId;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getSenderUsername() {
        return senderUsername;
    }

    public void setSenderUsername(String sender) {
        this.senderUsername = sender;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public int getConversationId() {
        return conversationId;
    }

    public void setConversationId(int conversationId) {
        this.conversationId = conversationId;
    }
}
