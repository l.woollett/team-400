package seng302.Model;

import seng302.Enum.RegionEnum;

import java.util.Objects;

/**
 * Model class which contains information for a specific hospital within New Zealand. This will be used to mark
 * hospital on the map (hence why we have latitude and longitude as attributes).
 */

public class Hospital implements Comparable<Hospital> {

    private String name;
    private RegionEnum region;
    private String address;
    private float latitude;
    private float longitude;

    public Hospital() {
        // Default constructor for SpringBoot wizardry
    }

    /**
     * Used to create a deep copy of a hospital. This is used in undo redo.
     *
     * @param hospital The original hospital.
     */
    public Hospital(Hospital hospital) {
        this.name = hospital.getName();
        this.region = hospital.getRegion();
        this.address = hospital.getAddress();
        this.latitude = hospital.getLatitude();
        this.longitude = hospital.getLongitude();
    }

    /**
     * Creates a hospital instance.
     *
     * @param name      The name of the hospital.
     * @param region    The hospital's region within New Zealand.
     * @param address   The hospital's address.
     * @param latitude  Latitude for the hospital.
     * @param longitude Longitude for the hospital.
     */
    public Hospital(String name, RegionEnum region, String address, float latitude, float longitude) {
        this.name = name;
        this.region = region;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public RegionEnum getRegion() {
        return region;
    }

    public String getAddress() {
        return address;
    }

    public float getLatitude() {
        return latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    /**
     * Compare the hospital name from this record to a given record
     *
     * @param compareTo The given record needs to compare with
     * @return -1 if the hospital name in compareTo param is later than the record itself; 1 if the hospital name  in
     * compareTo param is earlier than the record itself; 0 if the hospital name  in compareTo param is equal to the
     * record itself.
     */
    @Override
    public int compareTo(Hospital compareTo) {
        return this.getName().compareTo(compareTo.getName());

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Hospital)) return false;
        Hospital hospital = (Hospital) o;
        return Float.compare(hospital.latitude, latitude) == 0 &&
                Float.compare(hospital.longitude, longitude) == 0 &&
                Objects.equals(name, hospital.name) &&
                region == hospital.region &&
                Objects.equals(address, hospital.address);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, region, address, latitude, longitude);
    }


    @Override
    public String toString() {
        return String.format("%s %s %s %f %f", name, region.toString(), address, latitude, longitude);
    }
}
