package seng302.Model;

import seng302.Enum.OrganEnum;

import java.time.LocalDate;

public class ReceiverOrgan {
    private OrganEnum registeredOrgan;
    private LocalDate dateRegistered;

    public ReceiverOrgan() {

    }

    public ReceiverOrgan(OrganEnum registeredOrgan, LocalDate dateRegistered) {
        this.registeredOrgan = registeredOrgan;
        this.dateRegistered = dateRegistered;
    }

    public OrganEnum getRegisteredOrgan() {
        return registeredOrgan;
    }

    public LocalDate getDateRegistered() {
        return dateRegistered;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }

        if (!(obj instanceof ReceiverOrgan)) {
            return false;
        }

        ReceiverOrgan compare = (ReceiverOrgan) obj;

        return this.dateRegistered.equals(compare.dateRegistered) && this.registeredOrgan.equals(compare.registeredOrgan);
    }

    @Override
    public int hashCode() {
        return this.registeredOrgan.hashCode() + this.dateRegistered.hashCode();
    }

}
