package seng302.Model;

import seng302.Enum.AlcoholConsumptionEnum;
import seng302.Enum.BloodTypeEnum;
import seng302.Enum.GenderEnum;
import seng302.Enum.RegionEnum;
import seng302.ModelController.HistoryController;
import seng302.ModelController.UserController;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * This is the abstraction class of users, the parent class of classes: donor, clinician &amp; receivers
 */
public abstract class User {


    // Default values are assigned to optional values for the situation where values are not set. So we can avoid
    // having NullPointerExceptions and have a value to compare to when checking if the value is not set.
    protected String firstName = UserController.FIRST_NAME_NOT_SET; // Mandatory attribute
    protected String middleName = UserController.MIDDLE_NAME_NOT_SET;
    protected String lastName = UserController.LAST_NAME_NOT_SET;
    protected String username = UserController.USERNAME_NOT_SET; // Mandatory attribute
    protected String address = UserController.ADDRESS_NOT_SET;
    protected RegionEnum region = UserController.REGION_NOT_SET;
    protected Date createdDate = UserController.CREATED_DATE_NOT_SET; // It will be assigned when creating the profile.
    protected GenderEnum gender = UserController.GENDER_NOT_SET;
    protected Date modifiedDate = UserController.MODIFIED_DATE_NOT_SET;
    protected LocalDate dateOfBirth = UserController.DATE_OF_BIRTH_NOT_SET; // Mandatory field.
    protected ArrayList<String> changes = new ArrayList<>();

    public User(String firstName,
                String middleName,
                String lastName,
                String username,
                String address,
                RegionEnum region,
                GenderEnum gender,
                Date createdDate,
                Date modifiedDate,
                LocalDate dateOfBirth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.username = username;
        this.address = address;
        if (region == null) {
            region = RegionEnum.NOT_SET;
        }
        this.region = region;
        this.gender = gender;
        this.createdDate = createdDate;
        if (modifiedDate == null) {
            this.modifiedDate = createdDate;
        } else {
            this.modifiedDate = modifiedDate;
        }
        this.dateOfBirth = dateOfBirth;
    }

    public User() {
    }

    public User(String firstName, LocalDate dob, Date createdDate, String username) {
        this.firstName = firstName;
        this.dateOfBirth = dob;
        this.createdDate = createdDate;
        this.username = username;
    }


    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Set the date of birth. Will create a description of the change and tie it to the profile's history.
     *
     * @param dateOfBirth The new date of birth. in YYYY-MM-DD format.
     */
    public void setDateOfBirth(LocalDate dateOfBirth) {
        if (hasChanged(this.dateOfBirth, dateOfBirth)) {
            this.changes.add(HistoryController.createDateOfBirthChangeDescription(new Date(), this.dateOfBirth,
                    dateOfBirth));
            this.modifiedDate = new Date();
        }
        this.dateOfBirth = dateOfBirth;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    /**
     * Set the modifiedDate. ONLY CALL WHEN CREATING UNDOABLE!
     *
     * @param modifiedDate Date when last modified
     */
    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public GenderEnum getGender() {
        return gender;
    }

    /**
     * Set the gender. Will create a description of the change and tie it to the profile's history.
     *
     * @param gender The new gender.
     */
    public void setGender(GenderEnum gender) {
        if (hasChanged(this.gender, gender)) {
            this.changes.add(HistoryController.createGenderChangeDescription(new Date(), this.gender, gender));
            this.modifiedDate = new Date();
        }
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    /**
     * Set this profile's address and change it. Setting the new address will create a new description of the change
     * that will be tied to the address.
     *
     * @param address The new address.
     */
    public void setAddress(String address) {
        if (hasChanged(this.address, address)) {
            this.changes.add(HistoryController.createAddressChangeDescription(new Date(), this.address, address));
            this.modifiedDate = new Date();
        }
        this.address = address;
    }

    public String getFirstName() {
        return firstName;
    }

    /**
     * Set the first name. Will create a description of the change and tie it to the profile's history.
     *
     * @param firstName The new first name.
     */
    public void setFirstName(String firstName) {
        if (hasChanged(this.firstName, firstName)) {
            this.changes.add(HistoryController.createFirstNameChangeDescription(new Date(), this.firstName, firstName));
            this.modifiedDate = new Date();
        }
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    /**
     * Set the profile's middle name and change it. Setting the new middle name will create a new description of the
     * change that will be tied to the middle name.
     *
     * @param middleName The middle name
     */
    public void setMiddleName(String middleName) {
        if (hasChanged(this.middleName, middleName)) {
            this.changes
                    .add(HistoryController.createMiddleNameChangeDescription(new Date(), this.middleName, middleName));
            this.modifiedDate = new Date();
        }
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    /**
     * Set the last name. Will create a description of the change and tie it to the profile's history.
     *
     * @param lastName The new last name.
     */
    public void setLastName(String lastName) {
        if (hasChanged(this.lastName, lastName)) {
            this.changes.add(HistoryController.createLastNameChangeDescription(new Date(), this.lastName, lastName));
            this.modifiedDate = new Date();
            this.lastName = lastName;
        }
    }

    public String getUsername() {
        return username;
    }

    /**
     * Set the new username. Will create a description of the change and tie it to the profile's history.
     *
     * @param username The new username.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    public RegionEnum getRegion() {
        return region;
    }

    /**
     * Set the new region. Will create a description of the change and tie it to the profile's history.
     *
     * @param region The new region.
     */
    public void setRegion(RegionEnum region) {
        if (region == null) {
            region = RegionEnum.NOT_SET;
        }

        if (region == this.region) {
            return;
        } else if (this.region == RegionEnum.NOT_SET) {
            this.changes.add(HistoryController.createRegionChangeDescription(new Date(), "none", region.toString()));
        } else if (region == RegionEnum.NOT_SET) {
            this.changes
                    .add(HistoryController.createRegionChangeDescription(new Date(), this.region.toString(), "none"));
        } else {
            this.changes.add(HistoryController
                    .createRegionChangeDescription(new Date(), this.region.toString(), region.toString()));
        }
        this.modifiedDate = new Date();
        this.region = region;
    }

    public ArrayList<String> getChanges() {
        return changes;
    }

    /**
     * Set the changes. ONLY CALL WHEN CREATING UNDOABLE!
     *
     * @param changes ArrayList of changes
     */
    public void setChanges(ArrayList<String> changes) {
        this.changes = changes;
    }

    /**
     * Test to see if the value has changed.
     *
     * @param oldValue The old/current value of the user.
     * @param newValue The new value passed in the setter.
     * @return True if the value has changed, false otherwise.
     */
    protected boolean hasChanged(Object oldValue, Object newValue) {
        // Must deal with null objects to avoid NullPointerExceptions
        // First check to see if they are both null
        if (oldValue == null && newValue == null) {
            return false;
        }

        // Check if only one is null
        if (oldValue == null || newValue == null) {
            return true;
        }

        // Not null so can use String comparison
        return !oldValue.equals(newValue);
    }

    /**
     * Test to see if the value has changed.
     *
     * @param oldValue The old/current value of the user.
     * @param newValue The new value passed in the setter.
     * @return True if the value has changed, false otherwise.
     */
    protected boolean hasChanged(GenderEnum oldValue, GenderEnum newValue) {
        // Must deal with null objects to avoid NullPointerExceptions
        // First check to see if they are both null
        if (oldValue == null && newValue == null) {
            return false;
        }

        // Check if only one is null
        if (oldValue == null || newValue == null) {
            return true;
        }

        // Not null so can use String comparison
        return !oldValue.equals(newValue);
    }

    /**
     * Test to see if the value has changed.
     *
     * @param oldValue The old/current value of the user.
     * @param newValue The new value passed in the setter.
     * @return True if the value has changed, false otherwise.
     */
    protected boolean hasChanged(LocalDate oldValue, LocalDate newValue) {
        // Must deal with null objects to avoid NullPointerExceptions
        // First check to see if they are both null
        if (oldValue == null && newValue == null) {
            return false;
        }

        // Check if only one is null
        if (oldValue == null || newValue == null) {
            return true;
        }

        // Not null so can use String comparison
        return !oldValue.equals(newValue);
    }

    /**
     * Test to see if the value has changed.
     *
     * @param oldValue The old/current value of the user.
     * @param newValue The new value passed in the setter.
     * @return True if the value has changed, false otherwise.
     */
    protected boolean hasChanged(Date oldValue, Date newValue) {
        // Must deal with null objects to avoid NullPointerExceptions
        // First check to see if they are both null
        if (oldValue == null && newValue == null) {
            return false;
        }

        // Check if only one is null
        if (oldValue == null || newValue == null) {
            return true;
        }


        return !oldValue.equals(newValue);

    }

    /**
     * Test to see if the value has changed.
     *
     * @param oldValue The old/current value of the user.
     * @param newValue The new value passed in the setter.
     * @return True if the value has changed, false otherwise.
     */
    protected boolean hasChanged(BloodTypeEnum oldValue, BloodTypeEnum newValue) {
        // Must deal with null objects to avoid NullPointerExceptions
        // First check to see if they are both null
        if (oldValue == null && newValue == null) {
            return false;
        }

        // Check if only one is null
        if (oldValue == null || newValue == null) {
            return true;
        }


        return !oldValue.equals(newValue);

    }

    /**
     * Test to see if the value has changed
     *
     * @param oldValue The old/current value of the user.
     * @param newValue The new value passed in the setter.
     * @return True if the value has changed, false otherwise.
     */
    protected boolean hasChanged(AlcoholConsumptionEnum oldValue, AlcoholConsumptionEnum newValue) {
        // Must deal with null objects to avoid NullPointerExceptions
        // First check to see if they are both null
        if (oldValue == null && newValue == null) {
            return false;
        }

        // Check if only one is null
        if (oldValue == null || newValue == null) {
            return true;
        }

        return !oldValue.equals(newValue);

    }

    /**
     * Test to see if the value has changed.
     *
     * @param oldValue The old/current value of the user.
     * @param newValue The new value passed in the setter.
     * @return True if the value has changed, false otherwise.
     */
    protected boolean hasChanged(float oldValue, float newValue) {
        return !(Math.abs(newValue - oldValue) < 0.000001);
    }

    /**
     * Test to see if the value has changed.
     *
     * @param oldValue The old/current value of the user.
     * @param newValue The new value passed in the setter.
     * @return True if the value has changed, false otherwise.
     */
    protected boolean hasChanged(boolean oldValue, boolean newValue) {
        return !(oldValue == newValue);
    }

    /**
     * Will check if there is a difference between two ArrayList objects.
     *
     * @param oldList    The old list.
     * @param newList    The new list.
     * @param comparator The comparator used to sort the lists. Used to make sure we are comparing the same items.
     * @return True if the value has changed, false otherwise.
     */
    protected boolean hasListChanged(List<?> oldList, List<?> newList, Comparator comparator) {
        int oldSize = oldList.size();
        int newSize = newList.size();

        if (oldSize != newSize) {
            return true;
        }

        oldList.sort(comparator);
        newList.sort(comparator);

        // oldSize and newSize have the same value right now
        for (int index = 0; index < oldSize; index++) {
            if (oldList.get(index) != newList.get(index)) {
                return true;
            }
        }

        // No changes have been detected.
        return false;
    }


}
