package seng302.Model;

import seng302.Enum.GenderEnum;
import seng302.Enum.RegionEnum;
import seng302.ModelController.HistoryController;

import java.time.LocalDate;
import java.util.Date;

/**
 * Child class that will represent a clinician in the system.
 */
public class Clinician extends User {

    // The staff ID is unique within an hospital. In the ODMS system the staff ID is not unique by itself.
    // The hospital and the staff ID combined will be unique
    private Hospital hospital;
    private String staffId;


    public Clinician() {
    }

    /**
     * Constructor to create a clinician.
     *
     * @param firstName   The first name of the clinician.
     * @param middleName  The middle name of the clinician.
     * @param lastName    The last name of the clinician.
     * @param username    The last name of the clinician.
     * @param address     The address for the clinician.
     * @param region      The region for the clinician.
     * @param gender      The gender for the clinician.
     * @param createdDate The date the clinician account was created.
     * @param dateOfBirth The date of birth of the clinician.
     * @param hospital    The hospital the clinician belongs to.
     * @param staffId     The clinician's staff ID from the hospital.
     */
    public Clinician(String firstName, String middleName, String lastName, String username, String address,
                     RegionEnum region, GenderEnum gender, Date createdDate, LocalDate dateOfBirth, Hospital hospital,
                     String staffId) {
        super(firstName, middleName, lastName, username, address, region, gender, createdDate, null, dateOfBirth);
        this.hospital = hospital;
        this.staffId = staffId;
    }

    public Hospital getHospital() {
        return hospital;
    }

    public void setHospital(Hospital hos) {
        if (hasChanged(this.hospital, hos)) {
            this.changes.add(HistoryController.createOrganisationChangeDescription(new Date(), hos));
            this.modifiedDate = new Date();
            this.hospital = hos;
        }
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String id) {
        if (hasChanged(this.staffId, id)) {
            this.changes.add(HistoryController.createStaffIdChangeDescription(new Date(), id));
            this.modifiedDate = new Date();
            this.staffId = id;
        }
    }

}
