package seng302.Model;

import java.sql.Time;
import java.time.LocalDate;
import java.util.Objects;

/**
 * This class keeps the information of a profile after death. It contains a date of death, createdDate and a hospital
 */
public class Death {
    private LocalDate dateOfDeath;
    private Hospital hospital;
    private Time timeOfDeath;

    public Death() {
        // Needed for SpringBoot to work
    }

    /**
     * Constructor for this class to assign the created date to a pass-in date
     *
     * @param dateOfDeath, the date of death of this profile user
     * @param hospital,    the hospital of this profile user at when dead
     */
    public Death(LocalDate dateOfDeath, Hospital hospital, Time timeOfDeath) {
        this.dateOfDeath = dateOfDeath;
        this.hospital = hospital;
        this.timeOfDeath = timeOfDeath;
    }

    public LocalDate getDateOfDeath() {
        return this.dateOfDeath;
    }

    public void setDateOfDeath(LocalDate dateOfDeath) {
        this.dateOfDeath = dateOfDeath;
    }

    public Hospital getHospital() {
        return this.hospital;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }

    public Time getTimeOfDeath() {
        return timeOfDeath;
    }

    public void setTimeOfDeath(Time timeOfDeath) {
        this.timeOfDeath = timeOfDeath;
    }

    public String toString() {
        return "date of death: " + this.dateOfDeath + ", hospital: " + this.hospital.getName() + "time of death: " + this.timeOfDeath;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Death death = (Death) o;
        return Objects.equals(dateOfDeath, death.dateOfDeath) &&
                Objects.equals(hospital, death.hospital) &&
                Objects.equals(timeOfDeath, death.timeOfDeath);
    }

    @Override
    public int hashCode() {

        return Objects.hash(dateOfDeath, hospital, timeOfDeath);
    }
}
