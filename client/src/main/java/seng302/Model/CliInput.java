package seng302.Model;

import seng302.Enum.CommandEnum;

/**
 * Used to represent an input into the command line. Made up of the command the user wants to execute and any additional
 * arguments they may have entered.
 */
public class CliInput {
    private CommandEnum commandEnum;
    private String[] arguments;

    public CliInput(CommandEnum commandEnum, String[] arguments) {
        this.commandEnum = commandEnum;
        this.arguments = arguments;
    }

    public String[] getArguments() {
        return arguments;
    }

    public CommandEnum getCommandEnum() {
        return commandEnum;
    }
}
