package seng302.Model;

import seng302.CustomException.AlreadyExists;
import seng302.CustomException.DoesNotExist;
import seng302.Enum.*;
import seng302.ModelController.HistoryController;
import seng302.ModelController.ProfileController;
import seng302.ModelController.UserController;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 * Will represent a user's profile; will contain all the needed information about them.
 * <p>
 * Date of birth and the first name are mandatory fields to be entered by the user. The created and modified timestamp
 * will be automatically entered. Everything else is optional and can be left null.
 */
public class Profile extends User {

    // Default values are assigned to optional values for the situation where values are not set. So we can avoid
    // having NullPointerExceptions and have a value to compare to when checking if the value is not set.
    private float height = ProfileController.HEIGHT_NOT_SET;
    private float weight = ProfileController.WEIGHT_NOT_SET;
    private float bmi = ProfileController.BMI_NOT_SET;
    private BloodTypeEnum bloodType = ProfileController.BLOOD_TYPE_NOT_SET;
    private boolean isDonor = ProfileController.IS_DONOR_NOT_SET;
    private boolean isReceiver = false;
    private boolean organClash = false;
    private List<OrganEnum> organs = new ArrayList<>();
    private boolean isSmoker = ProfileController.IS_SMOKER_NOT_SET;
    private String bloodPressure = ProfileController.BLOOD_PRESSURE_NOT_SET;
    private AlcoholConsumptionEnum alcoholConsumption = ProfileController.ALCOHOL_CONSUMPTION_NOT_SET;
    protected GenderEnum birthGender = UserController.BIRTH_GENDER_NOT_SET;
    protected String alias = UserController.ALIAS_NOT_SET;
    private List<Disease> currentDiseases = new ArrayList<>();
    private List<Disease> pastDiseases = new ArrayList<>();
    private List<ReceiverOrgan> receiverOrgans = new ArrayList<>();
    private List<Procedure> pendingProcedures = new ArrayList<>();
    private List<Procedure> pastProcedures = new ArrayList<>();
    private List<String> currentMedications = new ArrayList<>();
    private List<String> previousMedications = new ArrayList<>();
    private Death death = ProfileController.DEATH_NOT_SET;
    private String clinicianUserName; // This is the clinician of this profile
    private String organisationName; // This is the hospital they're at.

    private static final Logger LOGGER = Logger.getLogger(Profile.class.getName());

    /**
     * Dummy constructor for SPRING SERIALIZATION ONLY. DO NOT USE ANYWHERE ELSE!.
     */
    public Profile() {
        //default constructor, used in serialization
    }

    /**
     * Create a new user profile with the provided details.
     *
     * @param firstName      The first name of the user, mandatory field the profile to be created.
     * @param middleName     The middle name of the user.
     * @param lastName       The last name of the user.
     * @param alias          The alias of the user
     * @param dateOfBirth    The date of birth of the person, mandatory field for the profile to be created.
     * @param death          Detail's about the user's death, null if the user is not deceased.
     * @param gender         The genderEnum of the user.
     * @param birthGender    The birthGenderEnum of the user.
     * @param height         The height of the user.
     * @param weight         The weight of the user.
     * @param bloodType      The blood types of the user.
     * @param address        The address of the user.
     * @param region         The region of the user.
     * @param createdDate    The date the profile was created, mandatory field for the profile to be created.
     * @param modifiedDate   The profile being created is counted as being modified.
     * @param isDonor        true if the user is a donor, false if they are not.
     * @param organs         List of organs the user have selected to donate.
     * @param username       The unique identifier for the profile.
     * @param isReceiver     true if the user is a receiver, false if they are not.
     * @param receiverOrgans An ArrayList of OrganEnum holding the list of organs that this profile is registered to
     *                       receive
     */
    public Profile(String firstName, String middleName, String lastName, LocalDate dateOfBirth, Death death,
                   GenderEnum gender, GenderEnum birthGender, float height, float weight, BloodTypeEnum bloodType,
                   String address, RegionEnum region, Date createdDate, Date modifiedDate, boolean isDonor,
                   ArrayList<OrganEnum> organs, String username, String alias, boolean isReceiver,
                   ArrayList<OrganEnum> receiverOrgans, String bloodPressure, AlcoholConsumptionEnum alcoholConsumption, boolean isSmoker) {
        super(firstName, middleName, lastName, username, address, region, gender, createdDate, modifiedDate,
                dateOfBirth);
        this.bloodPressure = bloodPressure;
        this.alcoholConsumption = alcoholConsumption;
        this.isSmoker = isSmoker;
        this.alias = alias;
        this.birthGender = birthGender;
        this.death = death;
        this.height = height;
        this.weight = weight;
        this.bloodType = bloodType;
        this.isDonor = isDonor;
        calcBMI(this.height, this.weight);
        this.isReceiver = isReceiver;
        if (organs != null) {
            this.organs = organs;
        }

        if (receiverOrgans != null) {
            for (OrganEnum organ : receiverOrgans) {
                ReceiverOrgan receiverOrgan = new ReceiverOrgan(organ, LocalDate.now());
                this.receiverOrgans.add(receiverOrgan);
            }
        }
        checkOrganClash();
    }

    /**
     * Create a new user profile with the provided details.
     *
     * @param firstName      The first name of the user, mandatory field the profile to be created.
     * @param middleName     The middle name of the user.
     * @param lastName       The last name of the user.
     * @param dateOfBirth    The date of birth of the person, mandatory field for the profile to be created.
     * @param death          Detail's about the user's death, null if the user is not deceased.
     * @param gender         The genderEnum of the user.
     * @param height         The height of the user.
     * @param weight         The weight of the user.
     * @param bloodType      The blood types of the user.
     * @param address        The address of the user.
     * @param region         The region of the user.
     * @param createdDate    The date the profile was created, mandatory field for the profile to be created.
     * @param modifiedDate   The profile being created is counted as being modified.
     * @param isDonor        true if the user is a donor, false if they are not.
     * @param organs         List of organs the user have selected to donate.
     * @param username       The unique identifier for the profile.
     * @param isReceiver     true if the user is a receiver, false if they are not.
     * @param receiverOrgans An ArrayList of OrganEnum holding the list of organs that this profile is registered to
     *                       receive
     */
    public Profile(String firstName, String middleName, String lastName, LocalDate dateOfBirth, Death death,
                   GenderEnum gender, float height, float weight, BloodTypeEnum bloodType, String address,
                   RegionEnum region, Date createdDate, Date modifiedDate, boolean isDonor, ArrayList<OrganEnum> organs,
                   String username,
                   boolean isReceiver, ArrayList<OrganEnum> receiverOrgans) {
        super(firstName, middleName, lastName, username, address, region, gender, createdDate, modifiedDate,
                dateOfBirth);
        this.alias = "";
        this.birthGender = null;
        this.death = death;
        this.height = height;
        this.weight = weight;
        this.bloodType = bloodType;
        this.isDonor = isDonor;
        this.organs = organs;
        calcBMI(this.height, this.weight);
        this.isReceiver = isReceiver;
        if (organs != null) {
            this.organs = organs;
        }

        if (receiverOrgans != null) {
            for (OrganEnum organ : receiverOrgans) {
                ReceiverOrgan receiverOrgan = new ReceiverOrgan(organ, LocalDate.now());
                this.receiverOrgans.add(receiverOrgan);
            }
        }
        checkOrganClash();
    }

    /**
     * Create a new user profile with the provided mandatory details.
     *
     * @param firstName   The first name of a person, mandatory field the profile to be created.
     * @param dob         The date of birth of the person, mandatory field for the profile to be created.
     * @param createdDate The date the profile was created, mandatory field for the profile to be created.
     * @param username    The profile's username mandatory field.
     */
    public Profile(String firstName, LocalDate dob, Date createdDate, String username) {
        super(firstName, dob, createdDate, username);
    }

    /**
     * Create a deep copy of the profile provided.
     *
     * @param toClone Profile that is to be copied.
     */
    public Profile(Profile toClone) {
        if (toClone != null) {
            this.firstName = toClone.getFirstName();
            this.dateOfBirth = toClone.getDateOfBirth();
            this.createdDate = (Date) toClone.getCreatedDate().clone();
            this.username = toClone.getUsername();
            this.middleName = toClone.getMiddleName();
            this.lastName = toClone.getLastName();
            this.address = toClone.getAddress();
            this.region = toClone.getRegion();
            this.gender = toClone.getGender();
            this.death = (toClone.getDeath());
            this.height = (toClone.getHeight());
            this.weight = (toClone.getWeight());
            this.bloodType = (toClone.getBloodType());
            this.isDonor = (toClone.isDonor());
            this.organs = (new ArrayList<>(toClone.getDonorOrgans()));
            this.isSmoker = (toClone.isSmoker());
            this.bloodPressure = (toClone.getBloodPressure());
            this.alcoholConsumption = ((AlcoholConsumptionEnum) toClone.getAlcoholConsumption());
            this.birthGender = (toClone.getBirthGender());
            this.alias = (toClone.getAlias());
            this.currentDiseases = (new ArrayList<>(toClone.getCurrentDiseases()));
            this.pastDiseases = (new ArrayList<>(toClone.getPastDiseases()));
            this.receiverOrgans = (new ArrayList<>(toClone.getReceiverOrgansWithTimes()));
            this.pendingProcedures = (new ArrayList<>(toClone.getPendingProcedures()));
            this.pastProcedures = (new ArrayList<>(toClone.getPastProcedures()));
            this.currentMedications = (new ArrayList<>(toClone.getCurrentMedications()));
            this.previousMedications = (new ArrayList<>(toClone.getPreviousMedications()));
            this.isReceiver = (toClone.isReceiver());
            if (modifiedDate == null) {
                this.modifiedDate = null;
            } else {
                this.modifiedDate = ((Date) toClone.getModifiedDate().clone());
            }
            this.changes = (new ArrayList<>(toClone.getChanges()));
        }
    }

    public boolean getSmoker() {
        return this.isSmoker;
    }

    public String getClinicianName() {
        return this.clinicianUserName;
    }

    public void setClinicianName(String newClinicianName) {
        this.clinicianUserName = newClinicianName;
    }

    public String getOrganisationName() {
        return organisationName;
    }

    public void setOrganisationName(String organisationName) {
        this.organisationName = organisationName;
    }

    public List<Procedure> getPastProcedures() {
        return pastProcedures;
    }

    public void setPastProcedures(List<Procedure> pastProcedures) {
        this.pastProcedures = pastProcedures;
    }

    public List<Procedure> getPendingProcedures() {
        return pendingProcedures;
    }

    public void setPendingProcedures(List<Procedure> pendingProcedures) {
        this.pendingProcedures = pendingProcedures;
    }

    public List<OrganEnum> getDonorOrgans() {
        if (organs == null) {
            return new ArrayList<>();
        } else {
            return organs;
        }
    }

    public BloodTypeEnum getBloodType() {
        return bloodType;
    }

    public float getHeight() {
        return height;
    }

    public float getWeight() {
        return weight;
    }

    public float getBmi() {
        return bmi;
    }

    public String getAlias() {
        return alias;
    }

    public GenderEnum getBirthGender() {
        return birthGender;
    }

    public Death getDeath() {
        return death;
    }

    /**
     * Donor status based on the donor organs registered
     *
     * @return true if registered organ number bigger than 0. Otherwise returns false.
     */
    public boolean isDonor() {
        if (getDonorOrgans().size() > 0) {
            isDonor = true;
        } else {
            isDonor = false;
        }
        return isDonor;
    }

    public List<String> getCurrentMedications() {
        return currentMedications;
    }

    public List<String> getPreviousMedications() {
        return previousMedications;
    }

    /**
     * Receiver status based on the receiver organs registered
     *
     * @return true if registered organ number bigger than 0. Otherwise returns false.
     */
    public boolean isReceiver() {
        if (receiverOrgans.size() > 0) {
            isReceiver = true;
        } else {
            isReceiver = false;
        }
        return isReceiver;
    }

    /**
     * Function retrieves the list of registered organs and their time stamps. Registered organs contain the organ, and
     * the date it was registered
     *
     * @return List of ReceiverOrgan
     */
    public List<ReceiverOrgan> getReceiverOrgansWithTimes() {
        return this.receiverOrgans;
    }

    /**
     * Function returns the list of receiver organs in their OrganEnum format excluding the date
     *
     * @return ArrayList containing OrganEnum of receiver organs
     */
    public List<OrganEnum> getReceiverOrgans() {
        ArrayList<OrganEnum> receivingOrgans = new ArrayList<>();
        for (ReceiverOrgan organ : this.receiverOrgans) {
            receivingOrgans.add(organ.getRegisteredOrgan());
        }
        return receivingOrgans;
    }

    /**
     * Takes a medication and adds it to the current medication Then adds the change to the history list
     *
     * @param medication String describing the medication to be added to the currentMedications list
     */
    public void addCurrentMedication(String medication) {
        if (!currentMedications.contains(medication)) {
            currentMedications.add(medication);
            changes.add(HistoryController.addCurrentMedicationDescription(medication));
            modifiedDate = new Date();
        }
    }

    /**
     * Given a list of items to move, move the items from Current medications to previous medications. Then add the
     * change to the history tab.
     *
     * @param toMove the medications to remove from current medication out
     */
    public void addPreviousMedication(ArrayList<String> toMove) {
        for (String medication : toMove) {
            if (!previousMedications.contains(medication)) {
                previousMedications.add(medication);
                currentMedications.remove(medication);
                changes.add(HistoryController.moveFromCurrentToPreviousDescription(medication));
                modifiedDate = new Date();
            }
        }
    }

    /**
     * Given a list of items to move, take the items from the previous medications list and add them into the current
     * medications list. Also add the change to the history list
     *
     * @param toMove the medications to move to the current medication
     */
    public void moveToCurrentMedication(ArrayList<String> toMove) {
        for (String medication : toMove) {
            if (!currentMedications.contains(medication)) {
                currentMedications.add(medication);
                previousMedications.remove(medication);
                changes.add(HistoryController.moveFromPreviousToCurrentDescription(medication));
                modifiedDate = new Date();
            }
        }
    }

    /**
     * Given a list, will delete the items inside the list from both the current and previous medication and then log
     * the change in the history list.
     *
     * @param toDelete the medications to delete
     */
    public void deleteMedication(ArrayList<String> toDelete) {
        for (String medication : toDelete) {
            if (currentMedications.contains(medication)) {
                currentMedications.remove(medication);
                changes.add(HistoryController.deleteMedicationDescription(medication));
                modifiedDate = new Date();
            } else {
                previousMedications.remove(medication);
                changes.add(HistoryController.deleteMedicationDescription(medication));
            }
        }
    }

    /**
     * Set a new death detail for this profile. This will also create a new description of the change and tie it to the profile's history.
     *
     * @param newDeath The patient's new death details, if they're still alive this is set to null.
     */
    public void setDeath(Death newDeath) {
        if (hasChanged(this.death, newDeath)) {
            if (this.death == ProfileController.DEATH_NOT_SET) {
                this.changes.add(HistoryController.createDeathChangeDescription(new Date(), null, this.death));
            } else {
                this.changes.add(HistoryController.createDeathChangeDescription(new Date(), this.death, newDeath));
            }
        }
        this.death = newDeath;
    }

    /**
     * Set this profile's blood type. Setting the new blood type will create a new description of the change that will
     * be tied to the address.
     *
     * @param bloodType The new blood type.
     */
    public void setBloodType(BloodTypeEnum bloodType) {
        if (hasChanged(this.bloodType, bloodType)) {
            this.changes.add(HistoryController.createBloodTypeChangeDescription(new Date(), this.bloodType, bloodType));
            this.modifiedDate = new Date();
        }
        this.bloodType = bloodType;
    }


    /**
     * Set the height. Will create a description of the change and tie it to the profile's history.
     *
     * @param height The new height.
     */
    public void setHeight(float height) {
        if (hasChanged(this.height, height)) {
            this.changes.add(HistoryController.createChangeHeightDescription(new Date(), this.height, height));
            this.modifiedDate = new Date();
            calcBMI(this.height, this.weight);
        }
        this.height = height;
    }

    private void checkOrganClash() {
        if (!this.organs.isEmpty()) {
            boolean clash = false;
            for (ReceiverOrgan organ : this.receiverOrgans) {
                if (this.organClash = this.organs.contains(organ.getRegisteredOrgan())) {
                    clash = true;
                }
            }
            this.organClash = clash;
        }
    }

    /**
     * Set the new donor organs. Will create a description of the change and tie it to the profile's history.
     *
     * @param organs The new organs.
     */
    public void setDonorOrgans(List<OrganEnum> organs) {

        if (this.organs == null) {
            this.organs = new ArrayList<>();
        }

        if (organs == null) {
            organs = new ArrayList<>();
        }

        if (hasOrgansChanged(this.organs, organs)) {
            this.changes.add(HistoryController.createOrganDonorListChangeDescription(new Date(), organs));
            this.modifiedDate = new Date();
        }
        this.organs = organs;
        checkOrganClash();
    }

    /**
     * Function takes a list of OrganEnum that you want register in bulk as ReceiverOrgans, then create a ReceiverOrgan
     * for each OrganEnum, and add them to the Profile's receiverOrgans list. Erases the previous entries.
     *
     * @param receiverOrgans ArrayList containing the intended organs we want to register
     */
    public void setReceiverOrgans(List<OrganEnum> receiverOrgans) {
        ArrayList<ReceiverOrgan> receivingOrgans = new ArrayList<>();
        for (OrganEnum receiverOrgan1 : receiverOrgans) {
            ReceiverOrgan receiverOrgan = new ReceiverOrgan(receiverOrgan1, LocalDate.now());
            receivingOrgans.add(receiverOrgan);
        }
        if (hasReceiverOrgansChanged(this.receiverOrgans, receivingOrgans)) {
            this.modifiedDate = new Date();
            this.receiverOrgans = receivingOrgans;
            this.changes.add(HistoryController.createOrganReceiverListChangeDescription(new Date(), getReceiverOrgans()));
        }
        this.receiverOrgans = receivingOrgans;
        checkOrganClash();
    }

    /**
     * Function explicitly sets the list of receiverOrgans. Use this when retrieving the receiverOrgans from the
     * database for example
     *
     * @param receiverOrgansWithTimes The List of receiverOrgans to set for the profile.
     */
    public void setReceiverOrgansWithTimes(List<ReceiverOrgan> receiverOrgansWithTimes) {
        this.receiverOrgans = receiverOrgansWithTimes;
        checkOrganClash();
    }

    /**
     * Set the new weight. Will create a description of the change and tie it to the profile's history.
     *
     * @param weight The new weight
     */
    public void setWeight(float weight) {
        if (hasChanged(this.weight, weight)) {
            this.changes.add(HistoryController.createChangeWeightDescription(new Date(), this.weight, weight));
            this.modifiedDate = new Date();
            calcBMI(this.height, this.weight);
        }
        this.weight = weight;
    }

    /**
     * Set the new donor status. Will create a description of the change and tie it to the profile's history.
     *
     * @param donor The new donor status.
     */
    public void setIsDonor(boolean donor) {
        if (hasChanged(this.isDonor, donor)) {
            this.changes.add(HistoryController.createDonorStatusChangeDescription(new Date(), donor));
            this.modifiedDate = new Date();
            isDonor = donor;
        }
    }

    public boolean isSmoker() {
        return isSmoker;
    }

    /**
     * Set the new smoker. Will create a description of the change and tie it to the profile's history.
     *
     * @param smoker The new smoker.
     */
    public void setSmoker(boolean smoker) {
        if (hasChanged(this.isSmoker, smoker)) {
            this.changes.add(HistoryController.createSmokerChangeDescription(new Date(), smoker));
            this.modifiedDate = new Date();
            isSmoker = smoker;
        }
    }

    /**
     * Set the new blood pressure. Will create a description of the change and tie it to the profile's history.
     *
     * @param bloodPressure The new blood pressure.
     */
    public void setBloodPressure(String bloodPressure) {
        if (hasChanged(this.bloodPressure, bloodPressure)) {
            this.changes.add(HistoryController
                    .createBloodPressureChangeDescription(new Date(), this.bloodPressure, bloodPressure));
            this.modifiedDate = new Date();
        }
        this.bloodPressure = bloodPressure;
    }

    /**
     * Set the new alcohol consumption. Will create a description of the change and tie it to the profile's history.
     *
     * @param alcoholConsumption The new alcohol assumption.
     */
    public void setAlcoholConsumption(AlcoholConsumptionEnum alcoholConsumption) {
        if (hasChanged(this.alcoholConsumption, alcoholConsumption)) {
            this.changes.add(HistoryController
                    .createAlcoholConsumptionChangeDescription(new Date(), this.alcoholConsumption,
                            alcoholConsumption));
            this.modifiedDate = new Date();
        }
        this.alcoholConsumption = alcoholConsumption;
    }

    /**
     * Sets the currentDiseases to reflect what the MedicalHistoryTabController has stored. This gets called each time
     * the tables get refreshed.
     * <p>
     * Ensure that every method that involves the tables calls updateTables() in MedicalHistoryController.
     *
     * @param currentDiseases the ArrayList of current diseases from the MedicalHistoryController
     */
    public void setCurrentDiseases(List<Disease> currentDiseases) {
        if (hasDiseasesChanged(this.currentDiseases, currentDiseases)) {
            this.changes.add(HistoryController.createCurrentDiseasesChangeDescription(currentDiseases));
            this.modifiedDate = new Date();
        }
        this.currentDiseases = currentDiseases;
    }

    /**
     * Set the birth gender. Will create a description of the change and tie it to the profile's history.
     *
     * @param birthGender the new gender
     */
    public void setBirthGender(GenderEnum birthGender) {
        if (hasChanged(this.birthGender, birthGender)) {
            this.changes.add(HistoryController
                    .createBirthGenderChangeDescription(new Date(), this.birthGender, birthGender));
            this.modifiedDate = new Date();
        }
        this.birthGender = birthGender;
    }

    /**
     * Set the new alias.
     *
     * @param alias The new alias.
     */
    public void setAlias(String alias) {
        if (hasChanged(this.alias, alias)) {
            this.changes.add(HistoryController.createAliasChangeDescription(new Date(), this.alias, alias));
            this.modifiedDate = new Date();
            this.alias = alias;
        }
    }

    /**
     * Set current medications for the profile
     *
     * @param currentMedications ArrayList of the medications
     */
    public void setCurrentMedications(List<String> currentMedications) {
        this.currentMedications = currentMedications;
    }

    /**
     * Set previous medications for the profile
     *
     * @param previousMedications ArrayList of the medications
     */
    public void setPreviousMedications(List<String> previousMedications) {
        this.previousMedications = previousMedications;
    }

    /**
     * Will add the Procedure to either the pending procedure list or the past procedure list depending on the date.
     *
     * @param procedure The Procedure to add to the profile.
     */
    public void addProcedure(Procedure procedure) {
        LocalDate date = procedure.getDateOfProcedure();
        if (date.isBefore(LocalDate.now())) {
            changes.add(HistoryController.addPastProcedureChangeDescription(new Date(), procedure));
            pastProcedures.add(procedure);
        } else {
            changes.add(HistoryController.addPendingProcedureChangeDescription(new Date(), procedure));
            pendingProcedures.add(procedure);
        }
    }

    /**
     * Will remove the procedure.
     *
     * @param procedure The procedure to be removed.
     */
    public void removeProcedure(Procedure procedure) {
        LocalDate date = procedure.getDateOfProcedure();
        if (date.isBefore(LocalDate.now())) {
            pastProcedures.remove(procedure);
        } else {
            pendingProcedures.remove(procedure);
        }
        changes.add(HistoryController.deleteProcedureChangeDescription(new Date(), procedure));
    }

    public void updateProcedureList(Procedure procedure) {
        LocalDate date = procedure.getDateOfProcedure();

        // Remove procedure from list that contains it
        pastProcedures.remove(procedure);
        pendingProcedures.remove(procedure);

        if (date.isBefore(LocalDate.now())) {
            pastProcedures.add(procedure);
        } else {
            pendingProcedures.add(procedure);
        }
        changes.add(HistoryController.updateProcedureChangeDescription(new Date(), procedure));
    }

    /**
     * Sets the pastDiseases to reflect what the MedicalHistoryTabController has stored. This gets called each time the
     * tables get refreshed.
     * <p>
     * Ensure that every method that involves the tables calls updateTables() in MedicalHistoryController.
     *
     * @param pastDiseases the ArrayList of past diseases from the MedicalHistoryController
     */
    public void setPastDiseases(List<Disease> pastDiseases) {
        if (hasDiseasesChanged(this.pastDiseases, pastDiseases)) {
            this.changes.add(HistoryController.createPastDiseasesChangeDescription(pastDiseases));
            this.modifiedDate = new Date();
            this.pastDiseases = pastDiseases;
        }
    }

    /**
     * Returns the list of past diseases
     *
     * @return list of past diseases
     */
    public List<Disease> getPastDiseases() {
        return pastDiseases;
    }

    /**
     * Returns the list of current diseases
     *
     * @return list of current diseases
     */
    public List<Disease> getCurrentDiseases() {
        return currentDiseases;
    }

    /**
     * Set the profile to be an organ receiver
     *
     * @param receiver: Boolean
     */
    public void setIsReceiver(boolean receiver) {
        if (hasChanged(this.isReceiver, receiver)) {
            this.changes.add(HistoryController.createReceiverStatusChangeDescription(new Date(), receiver));
            this.modifiedDate = new Date();
            isReceiver = receiver;
        }
    }


    public String getBloodPressure() {
        return bloodPressure;
    }

    public Enum getAlcoholConsumption() {
        return alcoholConsumption;
    }

    /**
     * Must check all attributes excluding first name and date of birth since all others are optional to enter in.
     *
     * @return The summary of all the attributes of the user's profile.
     */
    @Override
    public String toString() {
        //System dependent line separator.
        final String ls = System.lineSeparator();

        String message = "";
        message += "username: " + username + ls;
        message += "Name: " + this.firstName;

        if (!alias.equals(UserController.ALIAS_NOT_SET)) {
            message += " " + this.alias + ls;
        } else {
            message += ls;
        }

        if (!middleName.equals(UserController.MIDDLE_NAME_NOT_SET)) {
            message += " " + this.middleName + ls;
        } else {
            message += ls;
        }

        if (!lastName.equals(UserController.LAST_NAME_NOT_SET)) {
            message += " " + this.lastName + ls;
        } else {
            message += ls;
        }

        message += "Date of Birth: " + this.dateOfBirth.toString() + ls;

        if (death != ProfileController.DEATH_NOT_SET) {
            message += "Date of Death: " + this.death.getDateOfDeath().toString() + ls;
            message += "Death location: " + this.death.getHospital().getName() + ls;
        } else {
            message += "Death details: has not died" + ls;
        }


        if (gender != UserController.GENDER_NOT_SET) {
            message += "Gender: " + this.gender.toString() + ls;
        } else {
            message += "Gender: not set" + ls;
        }

        if (birthGender != UserController.BIRTH_GENDER_NOT_SET) {
            message += "Birth Gender: " + this.birthGender.toString() + ls;
        } else {
            message += "Birth Gender: not set" + ls;
        }

        if (height != ProfileController.HEIGHT_NOT_SET) {
            message += "Height (m): " + this.height + ls;
        } else {
            message += "Height (m): not set" + ls;
        }

        if (weight != ProfileController.WEIGHT_NOT_SET) {
            message += "Weight (kg): " + this.weight + ls;
        } else {
            message += "Weight (kg): not set" + ls;
        }

        if (bmi != ProfileController.BMI_NOT_SET) {
            message += "BMI: " + this.bmi + ls;
        } else {
            message += "BMI: not set" + ls;
        }

        if (bloodType != ProfileController.BLOOD_TYPE_NOT_SET) {
            message += "Blood type: " + this.bloodType.toString() + ls;
        } else {
            message += "Blood type: not set" + ls;
        }

        if (!address.equals(UserController.ADDRESS_NOT_SET)) {
            message += "Address: " + this.address + ls;
        } else {
            message += "Address: not set" + ls;
        }

        if (region != UserController.REGION_NOT_SET) {
            message += "Region: " + this.region + ls;
        } else {
            message += "Region: not set" + ls;
        }

        if (createdDate != UserController.CREATED_DATE_NOT_SET) {
            message += "Date created: " + this.createdDate.toString() + ls;
        } else {
            message += "Date created: not set" + ls;
        }

        if (modifiedDate != UserController.MODIFIED_DATE_NOT_SET) {
            message += "Date last modified: " + this.modifiedDate.toString() + ls;
        } else {
            message += "Date last modified: not set" + ls;
        }

        if (isDonor) {
            message += "Is a donor: true" + ls;

            if (hasOrgansChanged(organs, ProfileController.ORGAN_NOT_SET)) {
                message += "Organs selected: ";
                for (OrganEnum organEnum : organs) {
                    message += organEnum.toString() + ", ";
                }

                //Remove the last comma
                message = message.replaceAll(", $", "");
            } else {
                message += "Organs selected: No organs have been selected for donation." + ls;
            }

        } else {
            message += "Is a donor: false" + ls;
        }

        if (isReceiver) {
            message += "Is a receiver: true" + ls;

            if (!receiverOrgans.isEmpty()) {
                for (ReceiverOrgan organ : this.receiverOrgans) {
                    message += "receiver organ register date: " + organ.getDateRegistered().toString() + ", " + ls;
                    message += "receiver organ: " + organ.getRegisteredOrgan().toString() + ", " + ls;
                }
            }
        } else {
            message += "Is a receiver: false" + ls;
        }

        if (hasDiseasesChanged(currentDiseases, ProfileController.DISEASES_NOT_SET)) {
            message += "Current Diseases: ";
            for (Disease disease : currentDiseases) {
                message += disease.toString() + ", ";
            }
            //Remove the last comma
            message = message.replaceAll(", $", "");
            message += ls;
        } else {
            message += "User has no Current Diseases." + ls;
        }

        if (hasDiseasesChanged(pastDiseases, ProfileController.DISEASES_NOT_SET)) {
            message += "Past Diseases: ";
            for (Disease disease : currentDiseases) {
                message += disease.toString() + ", ";
            }
            //Remove the last comma
            message = message.replaceAll(", $", "");
            message += ls;
        } else {
            message += "User has no Past Diseases." + ls;
        }

        message += "Is a smoker: " + isSmoker + ls;

        if (bloodPressure.equals(ProfileController.BLOOD_PRESSURE_NOT_SET)) {
            message += "Blood Pressure: " + this.bloodPressure + ls;
        } else {
            message += "Blood Pressure: not set" + ls;
        }
        if (alcoholConsumption.equals(ProfileController.ALCOHOL_CONSUMPTION_NOT_SET)) {
            message += "Alcohol Consumption: " + this.alcoholConsumption + ls;
        } else {
            message += "Alcohol Consumption: not set" + ls;
        }

        return message;
    }

    /**
     * Function removes a particular organ from the receiver list based on the registered date.
     *
     * @param organ  The OrganEnum that is being removed from the receiverOrgan list.
     * @param reason The reason why the organ is being removed.
     * @throws DoesNotExist Thrown when trying to access an organ that does not exist in the list.
     */
    public void removeReceiverOrgan(OrganEnum organ, String reason) throws DoesNotExist {
        boolean exists = false;
        OrganEnum removedOrgan = null;
        ReceiverOrgan toRemove = null;
        for (ReceiverOrgan receiverOrgan : this.receiverOrgans) {
            if (receiverOrgan.getRegisteredOrgan().equals(organ)) {
                toRemove = receiverOrgan;
                removedOrgan = receiverOrgan.getRegisteredOrgan();
                exists = true;
            }
        }
        if (!exists) {
            throw new DoesNotExist("The given organ has not been registered yet.");
        } else {
            checkOrganClash();
            this.receiverOrgans.remove(toRemove);
            this.changes
                    .add(HistoryController
                            .createReceiverOrganRemovalDescription(new Date(), removedOrgan.toString(), reason));
        }
    }

    /**
     * Function removes a particular organ from the receiver list based on the registered date.
     * <p>
     * Same as removeReceiverOrgan(OrganEnum organ, String reason) but reason is the empty string
     *
     * @param organ The OrganEnum that is being removed from the receiverOrgan list.
     * @throws DoesNotExist Thrown when trying to access an organ that does not exist in the list.
     */
    public void removeReceiverOrgan(OrganEnum organ) throws DoesNotExist {
        removeReceiverOrgan(organ, "");
    }

    /**
     * Removed the specified organ. If the organ wasn't in the ArrayList in the first place do not throw error.
     *
     * @param organ The organ to remove.
     */
    public void removeDonorOrgan(OrganEnum organ) {
        this.modifiedDate = new Date();
        this.organs.remove(organ);
        this.changes.add(HistoryController.createOrganDonorListChangeDescription(new Date(), this.organs));
        checkOrganClash();
    }

    /**
     * Adds the specified organ into the receiver list.
     *
     * @param organ The organ the function is going to add.
     */
    public void addReceivingOrgan(OrganEnum organ) {
        this.isReceiver = true;
        boolean contains = false;
        if (!this.receiverOrgans.isEmpty()) {
            for (ReceiverOrgan receiverOrgan : this.receiverOrgans) {
                if (receiverOrgan.getRegisteredOrgan().equals(organ)) {
                    contains = true;
                }
            }
        }
        if (!contains) {
            this.receiverOrgans.add(new ReceiverOrgan(organ, LocalDate.now()));
            this.changes.add(HistoryController.createReceiverOrganAdditionDescription(new Date(), organ));
        }

        checkOrganClash();
    }

    /**
     * Add the specified organ to the user's selected organs.
     *
     * @param organ The organ to add.
     * @throws AlreadyExists The organ has already be selected.
     */
    public void addDonorOrgan(OrganEnum organ) throws AlreadyExists {
        this.isDonor = true;
        if (this.organs.contains(organ)) {
            throw new AlreadyExists(organ.toString() + " has already been added.");
        }
        this.modifiedDate = new Date();
        this.organs.add(organ);
        this.changes.add(HistoryController.createOrganDonorListChangeDescription(new Date(), this.organs));
        checkOrganClash();
    }

    /**
     * Replaces the selected organs with a new (and empty) array.
     */
    public void clearDonorOrgans() {
        this.modifiedDate = new Date();
        this.organs = new ArrayList<>();
        this.changes.add(HistoryController.createOrganDonorListChangeDescription(new Date(), this.organs));
    }

    /**
     * Replaces the organs for a receiver with a new (and empty) array.
     */
    public void clearReceiverOrgans() {
        this.modifiedDate = new Date();
        this.changes.add(HistoryController.createOrganReceiverListChangeDescription(new Date(), new ArrayList<>()));
        this.receiverOrgans.clear();
        checkOrganClash();
    }

    private void calcBMI(float height, float weight) {
        if (height >= 1 && weight >= 1) {
            this.bmi = (weight / height) / height;
        } else {
            this.bmi = ProfileController.BMI_NOT_SET;
        }
    }

    public ArrayList<String> getChanges() {
        return changes;
    }


    @Override
    public boolean equals(Object other) {
        if (other != null) {
            if (this.getClass() != other.getClass()) {
                return false;
            } else {
                Profile otherProfile = (Profile) other;
                return this.username.equals(otherProfile.getUsername());
            }
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + username.hashCode();
        return result;
    }


    /**
     * Calculate the age.
     *
     * @param birthDate     The date the person was born.
     * @param referenceDate The date we are basing the age calculation on. If they are still alive the current date will
     *                      be passed. If they have died then their date of death will be passed.
     * @return Their age as an integer.
     */
    private int calcAge(LocalDate birthDate, LocalDate referenceDate) {
        Month referenceMonth = referenceDate.getMonth();
        int referenceDay = referenceDate.getDayOfMonth();

        Month birthMonth = birthDate.getMonth();
        int birthDay = birthDate.getDayOfMonth();

        if (referenceMonth.getValue() > birthMonth.getValue()) {
            // If the current month has gone past the birth month, then we must have passed their birthday
            return referenceDate.getYear() - birthDate.getYear();
        } else if (birthMonth.getValue() == referenceMonth.getValue() && referenceDay >= birthDay) {
            // If the months are the same, then we must check if the current day in the month is after or equal
            // to their birthday. If so then we must have passed their birthday
            return referenceDate.getYear() - birthDate.getYear();
        } else {
            // Their birthday hasn't passed yet, must take this into account by subtracting a year
            return referenceDate.getYear() - birthDate.getYear() - 1;
        }
    }

    /**
     * Will return the age of the person.
     *
     * @return An integer representing the age of person.
     */
    public int getAge() {
        LocalDate today = LocalDate.now();

        if (death == ProfileController.DEATH_NOT_SET) {
            return calcAge(dateOfBirth, today);
        } else {
            return calcAge(dateOfBirth, death.getDateOfDeath());
        }
    }

    /**
     * Test to see if the array of diseases has changed.
     *
     * @param oldList The old/current value of the user.
     * @param newList The new value passed in the setter.
     * @return True if the value has changed, false otherwise.
     */
    private boolean hasDiseasesChanged(List<Disease> oldList, List<Disease> newList) {
        Comparator<Disease> comparator = Comparator.comparing(Disease::toString);
        return hasListChanged(oldList, newList, comparator);
    }

    /**
     * Test to see if the array of organs has changed.
     *
     * @param oldList The old/current value of the user.
     * @param newList The new value passed in the setter.
     * @return True if the value has changed, false otherwise.
     */
    private boolean hasOrgansChanged(List<OrganEnum> oldList, List<OrganEnum> newList) {
        Comparator<OrganEnum> comparator = Comparator.comparing(OrganEnum::toString);
        return hasListChanged(oldList, newList, comparator);
    }

    /**
     * Function checks if two lists of ReceiverOrgan are different or the same.
     * @param oldList List of original ReceiverOrgan.
     * @param newList List of new ReceiverOrgan.
     * @return boolean indicating if the two list are different
     */
    private boolean hasReceiverOrgansChanged(List<ReceiverOrgan> oldList, List<ReceiverOrgan> newList) {
        if (newList.size() != oldList.size()) {
            return true;
        } else {
            for (ReceiverOrgan fromOldList: oldList) {
                for (ReceiverOrgan fromNewList: newList) {
                    if (fromOldList.equals(fromNewList)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public boolean hasOrganClash() {
        return organClash;
    }

    /**
     * Will move the current disease to the past diseases.
     *
     * @param disease The current disease that has been cured, to be moved to past diseases.
     * @throws DoesNotExist It is not a current disease.
     */
    public void cureCurrentDisease(Disease disease) throws DoesNotExist {
        if (!currentDiseases.contains(disease)) {
            throw new DoesNotExist(disease.toString() + " is not a current disease");
        }

        currentDiseases.remove(disease);
        pastDiseases.add(disease);
    }

}

