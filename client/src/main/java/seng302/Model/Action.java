package seng302.Model;

import seng302.Enum.CommandEnum;

import java.util.Arrays;
import java.util.Date;

/**
 * Class used to represent an action that occurred in the application.
 */
public class Action {
    private final String username;
    private CommandEnum command;
    private String[] args;
    private Date timestamp;
    private String annotation;

    /**
     * @param command    The command that was executed.
     * @param args       The arguments used during the command execution.
     * @param annotation Sentence describing what happened.
     * @param username   The username of the the profile that made the action.
     */
    public Action(CommandEnum command, String[] args, String annotation, String username) {
        this.command = command;
        this.args = args;
        this.timestamp = new Date();
        this.annotation = annotation;
        this.username = username;
    }

    public CommandEnum getCommand() {
        return command;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public String getAnnotation() {
        return annotation;
    }

    public String[] getArgs() {
        return args;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public String toString() {
        return username + " " + this.annotation + "-> Command: " + this.command.toString() + " args: " +
                Arrays.deepToString(this.args) + " TimeStamp: " + this.timestamp.toString();
    }
}
