package seng302.Model;

import seng302.Utilities.Haversine;

import java.util.Objects;

public class OrganMatchQueryResult {
    private String username;
    private String recHospitalName;
    private Double donHospitalLat;
    private Double donHospitalLong;
    private Double recHospitalLat;
    private Double recHospitalLong;
    private int dateDiff;
    private Double distance;

    /**
     * Constructor for netbeans to not complain
     */
    public OrganMatchQueryResult() {
        //NetBeans
    }

    /**
     * Constructor for this class
     *
     * @param username        Receiver username
     * @param hospitalName    Receiver hospital name
     * @param donHospitalLat  Donor hospital Latitude
     * @param donHospitalLong Donor hospital Longitude
     * @param recHospitalLat  Receiver hospital Latitude
     * @param recHospitalLong Receiver hospital Longitude
     * @param dateDiff        The Amount of time they've been waiting
     */
    public OrganMatchQueryResult(String username, String hospitalName, Double donHospitalLat,
                                 Double donHospitalLong, Double recHospitalLat, Double recHospitalLong, int dateDiff) {

        this.username = username;
        this.recHospitalName = hospitalName;
        this.donHospitalLat = donHospitalLat;
        this.donHospitalLong = donHospitalLong;
        this.recHospitalLat = recHospitalLat;
        this.recHospitalLong = recHospitalLong;
        this.dateDiff = dateDiff;
    }

    /**
     * If two OrganMatchQueriesResults are equal.
     *
     * @param o The other object to compare to
     * @return bool if its equal or not.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrganMatchQueryResult result = (OrganMatchQueryResult) o;
        return dateDiff == result.dateDiff &&
                Objects.equals(username, result.username) &&
                Objects.equals(recHospitalName, result.recHospitalName) &&
                Objects.equals(donHospitalLat, result.donHospitalLat) &&
                Objects.equals(donHospitalLong, result.donHospitalLong) &&
                Objects.equals(recHospitalLat, result.recHospitalLat) &&
                Objects.equals(recHospitalLong, result.recHospitalLong);
    }


    /**
     * For hashing the object
     *
     * @return an int for hashing
     */
    @Override
    public int hashCode() {
        return Objects.hash(username, recHospitalName, donHospitalLat, donHospitalLong, recHospitalLat, recHospitalLong, dateDiff);
    }

    /**
     * Custom comparator for comparing two OrganMatchQueryResult's
     * Compares their date registered, then the distance between them and the Donor hospital.
     *
     * @param that The other object to compare to
     * @return int, -1 for before, 0 for equal, 1 for after.
     */
    public int compareTo(OrganMatchQueryResult that) {
        final int BEFORE = -1;
        final int EQUAL = 0;
        final int AFTER = 1;

        //Sorting by date being registered
        if (this == that) return EQUAL;
        if (this.dateDiff > that.dateDiff) return BEFORE;
        else if (this.dateDiff < that.dateDiff) return AFTER;

        else {
            //Both are the same
            //Now sort by distance
            //This is not used anymore, but will still be here in case we
            //Want to add functionality back in.
            if (this.getDistance() < that.getDistance()) {
                //Shorter distance to travel than the other
                return BEFORE;
            } else if (this.getDistance() > that.getDistance()) {
                return AFTER;
            } else {
                return EQUAL;
            }
        }
    }

    /**
     * Get the distance between the Donor hospital and the Receiver hospital.
     *
     * @return a double of the aforementioned distance.
     */
    public Double getDistance() {
        if (this.distance != null) {
            return this.distance;
        } else {
            this.distance = new Haversine().calcDistance(this.donHospitalLat, this.donHospitalLong, this.recHospitalLat, this.recHospitalLong);
            return this.distance;
        }
    }

    //All methods below are needed for NetBeans to not cry.

    /**
     * Returns the username of the receiver
     *
     * @return a username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Returns the name of the hospital receiver is getting treatment at
     *
     * @return
     */
    public String getRecHospitalName() {
        return recHospitalName;
    }

    /**
     * Returns the latitude of the hospital the donor is at
     *
     * @return Donation hospital latitude
     */
    public Double getDonHospitalLat() {
        return donHospitalLat;
    }

    /**
     * Returns the longitude of the hospital the donor is at
     *
     * @return Donation hospital longitude
     */
    public Double getDonHospitalLong() {
        return donHospitalLong;
    }

    /**
     * Returns the latitude of the hospital the receiver is at
     *
     * @return Receiver hospital latitude
     */
    public Double getRecHospitalLat() {
        return recHospitalLat;
    }

    /**
     * Returns the longitude of the hospital the receiver is at
     *
     * @return Receiver hospital longitude
     */
    public Double getRecHospitalLong() {
        return recHospitalLong;
    }

    /**
     * Returns the date difference between the Donor and Receiver
     *
     * @return int difference
     */
    public int getDateDiff() {
        return dateDiff;
    }
}
