package seng302.Enum;

/**
 * Predefined constants to represent the different blood types.
 */
public enum BloodTypeEnum {
    NOT_SET(""),
    O_NEGATIVE("O-"),
    O_POSITIVE("O+"),
    A_NEGATIVE("A-"),
    A_POSITIVE("A+"),
    B_NEGATIVE("B-"),
    B_POSITIVE("B+"),
    AB_NEGATIVE("AB-"),
    AB_POSITIVE("AB+");

    private final String bloodTypeValue;

    BloodTypeEnum(String value) {
        this.bloodTypeValue = value;
    }

    /**
     * Function converts a string to a BloodTypeEnum.
     *
     * @param value String to be converted.
     * @return BloodTypeEnum related tio the input string.
     * @throws IllegalArgumentException thrown when the string doesn't match a BloodTypeEnum.
     */
    public static BloodTypeEnum getBloodType(String value) throws IllegalArgumentException {

        for (BloodTypeEnum bloodType : BloodTypeEnum.values()) {
            if (bloodType.bloodTypeValue.equalsIgnoreCase(value)) {
                return bloodType;
            }
        }
        throw new IllegalArgumentException("No BloodTypeEnum constant with text: " + value);
    }

    /**
     * Function returns the string representation of BloodTypeEnum.
     *
     * @return String representing BloodTypeEnum.
     */
    @Override
    public String toString() {
        return this.bloodTypeValue;
    }
}
