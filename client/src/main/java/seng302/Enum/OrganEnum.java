package seng302.Enum;

/**
 * Predefined constants for the different organs a donor can donate.
 */
public enum OrganEnum {
    LIVER("Liver"),
    KIDNEY("Kidney"),
    PANCREAS("Pancreas"),
    HEART("Heart"),
    HEART_VALVES("Heart Valves"),
    LUNG("Lung"),
    INTESTINE("Intestine"),
    CORNEA("Cornea"),
    MIDDLE_EAR("Middle Ear"),
    SKIN("Skin"),
    BONE("Bone"),
    BONE_MARROW("Bone Marrow"),
    CONNECTIVE_TISSUE("Connective Tissue");

    private final String readableOrgan;

    OrganEnum(String readableOrgan) {
        this.readableOrgan = readableOrgan;
    }

    /**
     * Function converts a string value to the enum.
     *
     * @param value String representation of the value.
     * @return OrganEnum that relates to the input text.
     * @throws IllegalArgumentException Thrown when the string doesn't match any OrganEnum.
     */
    public static OrganEnum getOrgan(String value) throws IllegalArgumentException {
        for (OrganEnum organ : OrganEnum.values()) {
            if (organ.readableOrgan.equalsIgnoreCase(value)) {
                return organ;
            }
        }
        throw new IllegalArgumentException("No OrganEnum constant with text: " + value);
    }

    /**
     * Gets the id corresponding to the passed in organ enum.
     *
     * @param any An organ enum.
     * @return The id representation of the passed in organ.
     * @deprecated (when, why, refactoring advice)
     */
    public int getOrganId(OrganEnum any) {
        switch (any) {
            case LIVER:
                return 1;
            case CORNEA:
                return 2;
            case KIDNEY:
                return 3;
            case BONE:
                return 4;
            case PANCREAS:
                return 5;
            case LUNG:
                return 6;
            case SKIN:
                return 7;
            case HEART:
                return 8;
            case INTESTINE:
                return 9;
            case MIDDLE_EAR:
                return 10;
            case BONE_MARROW:
                return 11;
            case CONNECTIVE_TISSUE:
                return 12;
            case HEART_VALVES:
                return 13;
            default:
                throw new IllegalArgumentException();
        }
    }

    /**
     * Function returns the string representation of the OrganEnum
     *
     * @return String representation of the OrganEnum
     */
    @Override
    public String toString() {
        return this.readableOrgan;
    }
}
