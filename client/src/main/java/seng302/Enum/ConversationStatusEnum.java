package seng302.Enum;

public enum ConversationStatusEnum {
    ALL("All"),
    ACTIVE("Active"),
    INACTIVE("Inactive");

    private final String statusStringValue;

    ConversationStatusEnum(String value) {
        this.statusStringValue = value;
    }

    public static ConversationStatusEnum getStatus(String value) throws IllegalArgumentException {
        if (value == null) {
            throw new IllegalArgumentException("The value was null");
        }

        for (ConversationStatusEnum status : ConversationStatusEnum.values()) {
            if (status.statusStringValue.equalsIgnoreCase(value)) {
                return status;
            }
        }

        throw new IllegalArgumentException("No status matches the provided value: " + value);
    }
}
