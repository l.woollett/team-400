package seng302.Enum;

/**
 * Predefined constants for the different regions a user can choose
 */
public enum RegionEnum {
    NOT_SET(""),
    AUCKLAND("Auckland"),
    BAY_OF_PLENTY("Bay of Plenty"),
    CANTERBURY("Canterbury"),
    GISBOURNE("Gisborne"),
    HAWKES_BAY("Hawke's Bay"),
    MANAWATU_WANGANUI("Manawatu-Wanganui"),
    MARLBOROUGH("Marlborough"),
    NELSON("Nelson"),
    NORTHLAND("Northland"),
    OTAGO("Otago"),
    SOUTHLAND("Southland"),
    TARANAKI("Taranaki"),
    TASMAN("Tasman"),
    WAIKATO("Waikato"),
    WELLINGTION("Wellington"),
    WEST_COAST("West Coast");

    private final String readableRegion;

    RegionEnum(String readableRegion) {
        this.readableRegion = readableRegion;
    }

    /**
     * Function converts a string to a RegionEnum.
     *
     * @param value String to be converted.
     * @return RegionEnum representing the input string.
     * @throws IllegalArgumentException thrown when the input string doesn't match any RegionEnum.
     */
    public static RegionEnum getRegion(String value) throws IllegalArgumentException {
        for (RegionEnum region : RegionEnum.values()) {
            if (region.readableRegion.equalsIgnoreCase(value)) {
                return region;
            }
        }
        throw new IllegalArgumentException("No RegionEnum constant with text: " + value);
    }

    /**
     * Function returns the string representation of the RegionEnum.
     *
     * @return String representing the RegionEnum.
     */
    @Override
    public String toString() {
        return this.readableRegion;
    }

}
