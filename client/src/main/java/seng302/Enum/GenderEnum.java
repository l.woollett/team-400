package seng302.Enum;

/**
 * Predefined constants to represent the different options for gender.
 */
public enum GenderEnum {
    NOT_SET(""),
    FEMALE("Female"),
    MALE("Male"),
    OTHER("Other");

    private final String readableGender;

    GenderEnum(String readableGender) {
        this.readableGender = readableGender;
    }

    /**
     * Function converts a string to a matching GenderEnum.
     *
     * @param value String value to be converted.
     * @return GenderEnum relating to the input string.
     * @throws IllegalArgumentException Thrown when the string doesn't match any OrganEum.
     */
    public static GenderEnum getGender(String value) throws IllegalArgumentException {

        if (value == null) {
            return NOT_SET;
        }

        for (GenderEnum gender : GenderEnum.values()) {
            if (gender.readableGender.equalsIgnoreCase(value)) {
                return gender;
            }
        }
        throw new IllegalArgumentException("No GenderEnum constant with text: " + value);
    }

    /**
     * Function returns the string representation of the GenderEnum.
     *
     * @return String representing the GenderEnum.
     */
    @Override
    public String toString() {
        return this.readableGender;
    }
}
