package seng302.Enum;

/**
 * Enum used to indicate the status of an available organ request.
 */
public enum AvailableOrganRequestStatus {
    IN_PROGRESS("In Progress"),
    TRANSFERRED("Transferred"),
    DECLINED("Declined"),
    READ("Read"),
    UNREAD("Unread"),
    INTERESTED("Interested"),
    UNINTERESTED("UnInterested"),
    EXPIRED("Expired"),
    WITHDRAWN("Withdrawn");

    private final String statusStringValue;

    AvailableOrganRequestStatus(String status) {
        this.statusStringValue = status;
    }

    public static AvailableOrganRequestStatus getStatus(String value) throws IllegalArgumentException {
        if (value == null) {
            throw new IllegalArgumentException("The value was null");
        }

        for (AvailableOrganRequestStatus status : AvailableOrganRequestStatus.values()) {
            if (status.statusStringValue.equalsIgnoreCase(value)) {
                return status;
            }
        }

        throw new IllegalArgumentException("No status matches the provided value: " + value);
    }

    @Override
    public String toString() {
        return this.statusStringValue;
    }
}
