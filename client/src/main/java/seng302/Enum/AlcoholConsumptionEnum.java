package seng302.Enum;

public enum AlcoholConsumptionEnum {
    NOT_SET(""),
    NONE("NONE"),
    LOW("LOW"),
    MODERATE("MODERATE"),
    HIGH("HIGH"),
    VERY_HIGH("VERY HIGH");

    private final String readableAlcoholConsumption;

    AlcoholConsumptionEnum(String readableAlcoholConsumption) {
        this.readableAlcoholConsumption = readableAlcoholConsumption;
    }

    /**
     * Function converts a string into a AlcoholConsumptionEnum.
     *
     * @param value String to be converted.
     * @return AlcoholConsumptionEnum relating to the input String.
     * @throws IllegalArgumentException thrown when the string doesn't match a AlcoholConsumptionEnum.
     */
    public static AlcoholConsumptionEnum getAlcoholConsumption(String value) throws IllegalArgumentException {
        for (AlcoholConsumptionEnum alcoholConsumption : AlcoholConsumptionEnum.values()) {
            if (alcoholConsumption.readableAlcoholConsumption.equalsIgnoreCase(value)) {
                return alcoholConsumption;
            }
        }
        throw new IllegalArgumentException("No AlcoholConsumptionEnum constant with text: " + value);
    }

    /**
     * Function returns the string representation of the AlcoholConsumptionEnum.
     *
     * @return String representing the AlcoholConsumptionEnum.
     */
    @Override
    public String toString() {
        return this.readableAlcoholConsumption;
    }
}
