package seng302.Enum;

/**
 * Predefined constants for the different Chronic Diseases a donor can have.
 */

public enum ChronicDiseaseEnum {
    ASTHMA("Asthma"),
    BIPOLAR("Bipolar"),
    CARDIAC_FAILURE("Cardiac Failure"),
    CHRONIC_KIDNEY("Chronic Kidney"),
    CHRONIC_ARTERY("Chronic Artery"),
    CROHNS("Crohns"),
    DIABETES("Diabetes"),
    DYSRHYTHMIA("Dysrythmia"),
    EPILEPSY("Epilepsy"),
    HIV("HIV"),
    HYPERLIPIDAEMIA("Hyperlipidemia"),
    HYPERTENSION("Hypertension"),
    HYPOTHYROIDISM("Hypothyroidism"),
    MULTIPLE_SCIEROSIS("Multiple Scierosis"),
    PARKINSONS("Parkinsons"),
    ARTHRITIS(
            "Arthritis"), //This is actually Rheumatoid arthritis, dont know if we need to distinguish the difference though.
    SCHIZOPHRENIA("Shizophrenia"),
    LUPUS_ERYTHEMATOSIS("Lupus Erythematosis");

    private final String readableDisease;

    ChronicDiseaseEnum(String readableDisease) {
        this.readableDisease = readableDisease;
    }

    /**
     * Function converts a string into a ChronicDiseaseEnum.
     *
     * @param value String to be converted.
     * @return ChronicDiseaseEnum relating to the input string.
     * @throws IllegalArgumentException Thrown when no ChronicDiseaseEnum matches the string.
     */
    public static ChronicDiseaseEnum getChronicDisease(String value) throws IllegalArgumentException {
        for (ChronicDiseaseEnum disease : ChronicDiseaseEnum.values()) {
            if (disease.readableDisease.equalsIgnoreCase(value)) {
                return disease;
            }
        }
        throw new IllegalArgumentException("No ChronicDiseaseEnum constant with text: " + value);
    }

    /**
     * Function returns the string representation of ChronicDiseaseEnum.
     *
     * @return String representing the ChronicDiseaseEnum.
     */
    @Override
    public String toString() {
        return this.readableDisease;
    }
}
