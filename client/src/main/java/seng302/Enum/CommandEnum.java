package seng302.Enum;

/**
 * Enums which each represent a command the user can execute.
 */
public enum CommandEnum {
    QUIT,
    CREATE_PROFILE,
    CREATE_CLINICIAN,
    DISPLAY_PROFILE,
    HELP,
    EDIT_PROFILE,
    EDIT_CLINICIAN,
    DELETE_PROFILE,
    DELETE_CLINICIAN,
    SQL,
    DELETE_CACHE
}