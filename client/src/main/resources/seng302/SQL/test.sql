DROP TABLE IF EXISTS Notification;
DROP TABLE IF EXISTS History;
DROP TABLE IF EXISTS Admin;
DROP TABLE IF EXISTS Disease;
DROP TABLE IF EXISTS ReceivingOrgans;
DROP TABLE IF EXISTS Medication;
DROP TABLE IF EXISTS ProcedureOrgans;
DROP TABLE IF EXISTS ConversationClinicianJoin;
DROP TABLE IF EXISTS Messages;
DROP TABLE IF EXISTS Conversation;
DROP TABLE IF EXISTS DonatingOrgans;
DROP TABLE IF EXISTS MedicalProcedure;
DROP TABLE IF EXISTS Profile;
DROP TABLE IF EXISTS Clinician;
DROP TABLE IF EXISTS User;
DROP TABLE IF EXISTS Hospitals;
DROP TABLE IF EXISTS Messages;
DROP TABLE IF EXISTS ConversationClinicianJoin;
DROP TABLE IF EXISTS Conversation;

CREATE TABLE User (
  firstName    VARCHAR(20),
  middleName   VARCHAR(40),
  lastName     VARCHAR(40),
  username     VARCHAR(20) NOT NULL UNIQUE,
  address      VARCHAR(120),
  region       VARCHAR(100),
  gender       VARCHAR(10),
  createdDate  DATE        NOT NULL DEFAULT '1970-01-01',
  dateOfBirth  DATE        NOT NULL DEFAULT '1970-01-01',
  modifiedDate DATETIME,
  token        VARCHAR(36),
  password     VARCHAR(40),
  isAdmin      BOOLEAN     NOT NULL DEFAULT 0,
  PRIMARY KEY (username)
);

CREATE TABLE History (
  historyId   INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  username    VARCHAR(20),
  createdTime TIMESTAMP,
  description VARCHAR(250),
  FOREIGN KEY (username) REFERENCES User (username)
);

CREATE TABLE `Hospitals` (
  `HospitalID`      INT AUTO_INCREMENT PRIMARY KEY,
  `Name`            VARCHAR(80) NOT NULL,
  `Hospital_Region` VARCHAR(20) NOT NULL,
  `StreetAddress`   VARCHAR(40) NOT NULL,
  `Latitude`        FLOAT       NOT NULL,
  `Longitude`       FLOAT       NOT NULL
);

CREATE TABLE Clinician (
  staffId      VARCHAR(100),
  organisation INT,
  username     VARCHAR(20) NOT NULL PRIMARY KEY,
  FOREIGN KEY (username) REFERENCES User (username),
  FOREIGN KEY (organisation) REFERENCES Hospitals (HospitalID)
);

CREATE TABLE Profile (
  dateOfDeath        DATE,
  timeOfDeath        Time,
  height             FLOAT,
  weight             FLOAT,
  bloodType          VARCHAR(4),
  isDonor            BOOLEAN,
  isReceiver         BOOLEAN,
  isSmoker           BOOLEAN,
  bloodPressure      VARCHAR(6),
  alcoholConsumption VARCHAR(10),
  birthGender        VARCHAR(10),
  alias              VARCHAR(40),
  username           VARCHAR(20) PRIMARY KEY,
  hospitalId         INT,
  clinicianUserName  VARCHAR(20),
  FOREIGN KEY (username) REFERENCES User (username),
  FOREIGN KEY (hospitalId) REFERENCES Hospitals (HospitalID),
  FOREIGN KEY (clinicianUserName) REFERENCES Clinician (username)
);

CREATE TABLE Disease (
  diseaseName      VARCHAR(100),
  isChronicDisease BOOLEAN,
  isDiseaseCured   BOOLEAN,
  dateOfDiagnosis  DATE,
  isCurrent        BOOLEAN,
  profileUsername  VARCHAR(20),
  FOREIGN KEY (profileUsername) REFERENCES Profile (username),
  PRIMARY KEY (profileUsername, diseaseName, dateOfDiagnosis)
);

CREATE TABLE DonatingOrgans (
  organId  INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  organ    VARCHAR(20),
  username VARCHAR(20),
  FOREIGN KEY (username) REFERENCES Profile (username)
);

CREATE TABLE ReceivingOrgans (
  organ          VARCHAR(20),
  dateRegistered DATE,
  username       VARCHAR(20),
  FOREIGN KEY (username) REFERENCES Profile (username),
  PRIMARY KEY (username, organ)
);

CREATE TABLE Medication (
  isCurrent       BOOLEAN,
  name            VARCHAR(100),
  profileUsername VARCHAR(20),
  FOREIGN KEY (profileUsername) REFERENCES Profile (username),
  PRIMARY KEY (profileUsername, name, isCurrent)
);

CREATE TABLE MedicalProcedure (
  procedureId     INT AUTO_INCREMENT UNIQUE,
  dateOfProcedure DATE,
  summary         VARCHAR(255),
  description     VARCHAR(255),
  username        VARCHAR(20),
  FOREIGN KEY (username) REFERENCES Profile (username),
  PRIMARY KEY (username, summary, dateOfProcedure)
);

CREATE TABLE ProcedureOrgans (
  procedureOrganId INT AUTO_INCREMENT PRIMARY KEY,
  procedureId      INT NOT NULL,
  organ            VARCHAR(20),
  username         VARCHAR(20),
  FOREIGN KEY (procedureId) REFERENCES MedicalProcedure (procedureId),
  FOREIGN KEY (username) REFERENCES Profile (username)
);

CREATE TABLE Conversation (
  conversationId           INT      AUTO_INCREMENT PRIMARY KEY,
  creationTimestamp        DATETIME DEFAULT NOW()       NOT NULL,
  conversationStatus       VARCHAR(20) DEFAULT 'ACTIVE' NOT NULL,
  conversationEndTimestamp DATETIME DEFAULT NULL,
  creatorUsername          VARCHAR(20)                  NOT NULL,
  topic                    VARCHAR(256)                 NOT NULL,
  FOREIGN KEY (creatorUsername) REFERENCES Clinician (username)
);

CREATE TABLE Notification (
  requestId          INT AUTO_INCREMENT PRIMARY KEY, -- An identifier for the request
  conversationId     INT                    NOT NULL,
  organ              VARCHAR(20)            NOT NULL, -- The organ that is being broadcast as available
  originClinician    VARCHAR(20)            NOT NULL, -- The clinician who sent the request
  originHospital     INT                    NOT NULL, -- The hospital the request was made from
  recipientClinician VARCHAR(20)            NOT NULL, -- The clinician who received the request
  recipientHospital  INT                    NOT NULL, -- The hospital the request was sent to
  requestStatus      VARCHAR(20)            NOT NULL, -- The status of the message. Can be: In progress, Accepted, Declined, Expired, or Withdrawn
  donor              VARCHAR(20)            NOT NULL, -- The donor the organ belongs to
  createdTime        DATETIME DEFAULT NOW() NOT NULL, -- The date the request was created
  FOREIGN KEY (donor) REFERENCES Profile (username),
  FOREIGN KEY (originClinician) REFERENCES Clinician (username),
  FOREIGN KEY (originHospital) REFERENCES Hospitals (HospitalID),
  FOREIGN KEY (recipientHospital) REFERENCES Hospitals (HospitalID),
  FOREIGN KEY (recipientClinician) REFERENCES Clinician (username),
  FOREIGN KEY (conversationId) REFERENCES Conversation (conversationId)
);

CREATE TABLE Messages (
  messageId      INT AUTO_INCREMENT PRIMARY KEY,
  sentTimestamp  DATETIME DEFAULT NOW() NOT NULL,
  messageText    VARCHAR(4096)          NOT NULL,
  sender         VARCHAR(20)            NOT NULL,
  conversationId INT                    NOT NULL,
  FOREIGN KEY (sender) REFERENCES Clinician (username),
  FOREIGN KEY (conversationId) REFERENCES Conversation (conversationId)
);

CREATE TABLE ConversationClinicianJoin (
  conversationId INT,
  clinician      VARCHAR(20),
  lastRead       DATETIME DEFAULT NULL,
  FOREIGN KEY (conversationId) REFERENCES Conversation (conversationId),
  FOREIGN KEY (clinician) REFERENCES Clinician (username),
  PRIMARY KEY (conversationId, clinician)
);

INSERT INTO `Hospitals` (`Name`, `Hospital_Region`, `StreetAddress`, `Latitude`, `Longitude`) VALUES
  ('Whangarei Hospital', 'Northland', 'Maunu Road Whangarei 0110', -35.734998, 174.303139),
  ('Kaitaia Hospital', 'Northland', '29 Redan Road Kaitaia', -35.118930, 173.261132),
  ('Dargaville Hospital', 'Northland', 'Awakino Road Dargaville', -35.928584, 173.874939),
  ('Bay of Islands Hospital', 'Northland', 'Hospital Road Kawakawa', -35.385278, 174.069524),
  ('Auckland City Hospital', 'Auckland', '2 Park Road Grafton Auckland 1023', -36.859875, 174.769890),
  ('Starship Children’s Hospital', 'Auckland', '2 Park Road Grafton Auckland 1023', -36.859652, 174.769853),
  ('National Women’s Health', 'Auckland', 'Claude Road Epsom Auckland 1023', -36.895701, 174.779209),
  ('Tauranga Hospital', 'Bay of Plenty', 'Cameron Road Tauranga 3110', -37.707914, 176.147926),
  ('Whakatane Hospital', 'Bay of Plenty', 'Stewart Street Whakatane 3120', -37.963979, 176.978290),
  ('Christchurch Hospital', 'Canterbury', 'Christchurch Hospital, 2 Riccarton Ave, Christchurch Central, Christchurch 8011', -43.533886, 172.625804),
  ('Christchurch Women’s Hospital', 'Canterbury', '2 Riccarton Avenue Christchurch 8011', -43.533865, 172.624333),
  ('Burwood Hospital', 'Canterbury', '225 Mairehau Road Christchurch 8083', -43.480203, 172.683073),
  ('The Princess Margaret Hospital', 'Canterbury', '95 Cashmere Ave Christchurch 8022', -43.570719, 172.620677),
  ('Ashburton Hospital', 'Canterbury', 'Elizabeth Street Ashburton 7700', -43.894148, 171.745323),
  ('Hillmorton Hospital', 'Canterbury', 'Hillmorton Hospital, Annex Road South, Middleton, Christchurch 8024', -43.547901, 172.590940),
  ('Gisborne Hospital', 'Gisborne', '421 Ormond Road Gisborne 4010', -38.637923, 178.005116),
  ('Hawke\'s Bay Hospital', 'Hawke\'s Bay', 'Omahu Road Hastings 4120', -39.627504, 176.824890),
  ('Nelson Hospital', 'Nelson', 'Tipahi Street Nelson 7010', -41.286896, 173.272319),
  ('Wairau Hospital', 'Marlborough', 'Hospital Road Blenheim 7021', -41.535867, 173.943947),
  ('Dunedin Hospital', 'Southland', '201 Great King Street Dunedin 9016', -45.869257, 170.508848),
  ('Southland Hospital', 'Southland', 'Kew Road Invercargill 9812', -46.437518, 168.358625),
  ('Lakes District Hospital', 'Southland', '20 Douglas Street Frankton Queenstown 93', -45.023192, 168.735755),
  ('Taranaki Base Hospital', 'Taranaki', '23 David Street Westown New Plymouth 431', -39.072726, 174.056258),
  ('Hawera Hospital', 'Taranaki', 'Hunter Street Hawera 4610', -39.587565, 174.266391),
  ('Waikato Hospital', 'Waikato', 'Pembroke Street Hamilton', -37.804888, 175.282067),
  ('Wellington Hospital', 'Wellington', 'Riddiford Street Wellington South 6021', -41.307982, 174.779107),
  ('Kenepuru Hospital', 'Wellington', 'Rahia Street Porirua', -41.146625, 174.834256),
  ('Hutt Hospital', 'Wellington', 'High Street Lower Hutt 5010', -41.204353, 174.924677),
  ('Grey Base Hospital', 'West Coast', 'High Street Greymouth 7805', -42.463399, 171.192579);


INSERT INTO User (`username`, `address`, `createdDate`, `region`, `dateOfBirth`, `lastName`, `firstName`,
                  `middleName`, `gender`)
VALUES ('abc', 'abc', '2018-05-09', 'CANTERBURY', '1998-04-13', 'last', 'first', 'middle', 'FEMALE');

INSERT INTO User (`username`, isAdmin, password)
VALUES ('admin', 1, 'admin');

INSERT INTO User (`username`, `address`, `createdDate`, `region`, `dateOfBirth`, `lastName`, `firstName`, `middleName`, `gender`, `modifiedDate`)
VALUES ('defaultClinician', 'abc', '2018-05-09', 'CANTERBURY', '1998-04-13', 'last', 'first', 'middle', 'FEMALE', NULL);

INSERT INTO Clinician (`staffId`, `organisation`, `username`) VALUES ('0', '5', 'defaultClinician');

INSERT INTO History (`createdTime`, `description`, `username`) VALUES (CURRENT_TIMESTAMP, 'Created profile', 'abc');

INSERT INTO Profile (`bloodPressure`, `height`, `isSmoker`, `bloodType`, `birthGender`, `alias`, `weight`, `isReceiver`, `dateOfDeath`, `isDonor`, `alcoholConsumption`, `username`, `hospitalId`, `clinicianUserName`, `timeOfDeath`)
VALUES ('80/20', '1.75', '1', 'A-', 'FEMALE', 'batwoman', '80.50', '1', '2000-05-05', '1', 'HIGH', 'abc', '1',
        'defaultClinician', '12:00:00');

INSERT INTO MedicalProcedure (`dateOfProcedure`, `summary`, `description`, username)
VALUES ('2018-07-24', 'Heart Transplant', 'Medical transplant to ensure heart attack does not happen again', 'abc');

INSERT INTO DonatingOrgans (organ, username) VALUES ('Heart Valves', 'abc');

INSERT INTO DonatingOrgans (organ, username) VALUES ('Heart', 'abc');

INSERT INTO ReceivingOrgans (organ, dateRegistered, username) VALUES ('Kidney', '2018-07-10', 'abc');

INSERT INTO MedicalProcedure (dateOfProcedure, summary, description, username) VALUES ('2018-07-10', 'A procedure to
fix the broken Kidney', 'A surgeon will dominate that kidney into submission', 'abc');

INSERT INTO ProcedureOrgans (procedureId, organ, username) VALUES ((SELECT procedureId
                                                                    FROM MedicalProcedure
                                                                    WHERE
                                                                      MedicalProcedure.username = 'abc' AND summary = 'A procedure to
fix the broken Kidney' AND dateOfProcedure = '2018-07-10'), 'Kidney', 'abc');

INSERT INTO Medication (`isCurrent`, `name`, `profileUsername`) VALUES ('1', 'Palcozal', 'abc');

INSERT INTO Disease (`isChronicDisease`, `diseaseName`, `isDiseaseCured`, `dateOfDiagnosis`, `isCurrent`, `profileUsername`)
VALUES ('1', 'Cancer', '0', '2018-07-10', '1', 'abc');