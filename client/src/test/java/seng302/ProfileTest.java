package seng302;

import org.junit.Before;
import org.junit.Test;
import seng302.CustomException.AlreadyExists;
import seng302.CustomException.DoesNotExist;
import seng302.Enum.*;
import seng302.Model.Death;
import seng302.Model.Hospital;
import seng302.Model.Procedure;
import seng302.Model.Profile;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static junit.framework.TestCase.*;
import static org.junit.Assert.fail;
import static seng302.CustomAsserts.assertDateEquals;

public class ProfileTest {
    private LocalDate dobDate = LocalDate.parse("02-02-1939", DateTimeFormatter.ofPattern("dd-MM-yyyy"));
    private Profile testReceiver1;

    /**
     * Add one receiver to a list of receiver profiles
     */
    @Before
    public void setUp() {
        testReceiver1 = new Profile("fName", "mName",
                "lName", dobDate, null,
                GenderEnum.MALE, 1.78f, 70f, BloodTypeEnum.A_NEGATIVE, "test address",
                RegionEnum.AUCKLAND, getCreatedDate(), null, false, new ArrayList<>(),
                "uName1", true, getReceiverOrganList());
    }


    /**
     * Set up a tuple of receiver organs and a timestamp on the date registered this organ
     *
     * @return the tuple of donated organs and the timestamp of the registration
     */
    private ArrayList<OrganEnum> getReceiverOrganList() {
        return new ArrayList<>(Arrays.asList(OrganEnum.LIVER, OrganEnum.CORNEA));
    }


    /**
     * Set up created date
     *
     * @return the created date
     */
    private Date getCreatedDate() {
        String createdDateString = "2018-04-28";
        Date createdDateDate = null;
        try {
            createdDateDate = new SimpleDateFormat("yyyy-MM-dd").parse(createdDateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return createdDateDate;
    }

    /**
     * Assert if the description for the profile changes are valid.
     *
     * @param modelSolution What the description should be.
     * @param result        What the description was.
     */
    private void assertChangeDescriptionEqual(String modelSolution, String result) {
        final int END_OF_TIMESTAMP = 29;

        Date modelSolutionDate = null;
        Date resultDate = null;

        String modelSolutionDateStr = modelSolution.substring(0, modelSolution.indexOf(":", END_OF_TIMESTAMP));
        String resultDateStr = result.substring(0, modelSolution.indexOf(":", END_OF_TIMESTAMP));


        try {
            DateFormat df = new SimpleDateFormat("EEE MMM dd kk:mm:ss zzz yyyy");
            modelSolutionDate = df.parse(modelSolutionDateStr);
            resultDate = df.parse(resultDateStr);
        } catch (ParseException e) {
            fail();
            e.printStackTrace();
        }

        assertDateEquals(resultDate, modelSolutionDate); // This will check that the description has the correct
        // timestamp/date

    }

    /**
     * Will test if the description for changing the address will be accurate and added to their history.
     */
    @Test
    public void changedAddressDescription() {
        Profile profile = new Profile("John", LocalDate.now(), new Date(), "username");
        profile.setAddress("123 Fake Street");

        String changeDesc = profile.getChanges().get(0);
        Date currentDate = new Date();

        String modelSolution = currentDate.toString() + ": Address was set to 123 Fake Street";

        assertChangeDescriptionEqual(modelSolution, changeDesc);
    }


    /**
     * Will test if the description for changing the first name will be accurate and added to their history.
     */
    @Test
    public void changedFirstNameDescription() {
        Profile profile = new Profile("John", LocalDate.now(), new Date(), "username");
        profile.setFirstName("Matt");

        String changeDesc = profile.getChanges().get(0);
        Date currentDate = new Date();

        String modelSolution = currentDate.toString() + ": Changed first name from John to Matt";

        assertChangeDescriptionEqual(modelSolution, changeDesc);
    }

    /**
     * Will test if the description for changing the middle name will be accurate and added to their history.
     */
    @Test
    public void changedMiddleNameDescription() {
        Profile profile = new Profile("John", LocalDate.now(), new Date(), "username");
        profile.setMiddleName("Kylo");

        String changeDesc = profile.getChanges().get(0);
        Date currentDate = new Date();

        String modelSolution = currentDate.toString() + ": Middle name was set to Kylo";

        assertChangeDescriptionEqual(modelSolution, changeDesc);
    }

    /**
     * Will test if the description for changing the last name will be accurate and added to their history.
     */
    @Test
    public void changedLastNameDescriptionNull() {
        Profile profile = new Profile("John", LocalDate.now(), new Date(), "username");
        profile.setLastName("Snow");

        String changeDesc = profile.getChanges().get(0);
        Date currentDate = new Date();

        String modelSolution = currentDate.toString() + ": Last name was set to Snow";

        assertChangeDescriptionEqual(modelSolution, changeDesc);
    }

    /**
     * Will test if the description for changing the last name will be accurate and added to their history.
     */
    @Test
    public void changedLastNameDescription() {
        Profile profile = new Profile("John", LocalDate.now(), new Date(), "username");
        profile.setLastName("Henry");
        profile.setLastName("Snow");

        String changeDesc = profile.getChanges().get(1);
        Date currentDate = new Date();

        String modelSolution = currentDate.toString() + ": Changed last name from Henry to Snow";

        assertChangeDescriptionEqual(modelSolution, changeDesc);
    }

    /**
     * Will test if the description for changing the date of birth  will be accurate and added to their history.
     */
    @Test
    public void changedDobDescription() {
        Profile profile = new Profile("John", LocalDate.parse("1996-12-10"), new Date(), "username");
        profile.setDateOfBirth(LocalDate.of(1997, 12, 10));

        String changeDesc = profile.getChanges().get(0);
        Date currentDate = new Date();

        String modelSolution = currentDate.toString() + ": Changed date of birth from 1996-12-10 to 1997-12-10";

        assertChangeDescriptionEqual(modelSolution, changeDesc);
    }

    /**
     * Will test if the description for changing the date of death will be accurate and added to their history.
     */
    @Test
    public void changedDodDescription() {
        Profile profile = new Profile("John", LocalDate.parse("1996-12-10"), new Date(), "username");
        profile.setDeath(new Death(LocalDate.of(2005, 12, 10),
                new Hospital("Canterbury Hospital", RegionEnum.CANTERBURY, "123 Fake Street", 1, 1), null));

        String changeDesc = profile.getChanges().get(0);
        Date currentDate = new Date();

        String modelSolution = currentDate.toString() + ": Date of death was set to 2005-12-10";

        assertChangeDescriptionEqual(modelSolution, changeDesc);
    }

    /**
     * Will test if the description for changing the gender will be accurate and added to their history.
     */
    @Test
    public void changedGenderDescription() {
        Profile profile = new Profile("John", LocalDate.parse("1996-12-10"), new Date(), "username");
        profile.setGender(GenderEnum.MALE);

        String changeDesc = profile.getChanges().get(0);
        Date currentDate = new Date();

        String modelSolution = currentDate.toString() + ": Gender was set to MALE";

        assertChangeDescriptionEqual(modelSolution, changeDesc);
    }

    /**
     * Will test if the description for changing the blood type will be accurate and added to their history.
     */
    @Test
    public void changedBloodTypeDescription() {
        Profile profile = new Profile("John", LocalDate.parse("1996-12-10"), new Date(), "username");
        profile.setBloodType(BloodTypeEnum.A_NEGATIVE);

        String changeDesc = profile.getChanges().get(0);
        Date currentDate = new Date();

        String modelSolution = currentDate.toString() + ": Blood type was set to A-";

        assertChangeDescriptionEqual(modelSolution, changeDesc);
    }

    /**
     * Will test if the description for changing the donor status will be accurate and added to their history.
     */
    @Test
    public void changedDonorStatusDescriptionRegister() {
        Profile profile = new Profile("John", LocalDate.parse("1996-12-10"), new Date(), "username");
        profile.setIsDonor(true);

        String changeDesc = profile.getChanges().get(0);
        Date currentDate = new Date();

        String modelSolution = currentDate.toString() + ": Registered as an organ donor";

        assertChangeDescriptionEqual(modelSolution, changeDesc);
    }


    /**
     * Will test if the description for changing the organs will be accurate and added to their history.
     */
    @Test
    public void changeOrganListDescriptionSingle() {
        Profile profile = new Profile("John", LocalDate.parse("1996-12-10"), new Date(), "username");
        profile.setDonorOrgans(new ArrayList<>(Collections.singletonList(OrganEnum.BONE)));

        String changeDesc = profile.getChanges().get(0);
        Date currentDate = new Date();

        String modelSolution = currentDate.toString() + ": Changed the organs to donate to: Bone.";

        assertChangeDescriptionEqual(modelSolution, changeDesc);
    }

    /**
     * Will test if the description for changing the organs will be accurate and added to their history.
     */
    @Test
    public void changeOrganListDescriptionMultiple() {
        Profile profile = new Profile("John", LocalDate.parse("1996-12-10"), new Date(), "username");
        profile.setDonorOrgans(
                new ArrayList<>(Arrays.asList(OrganEnum.BONE, OrganEnum.LIVER, OrganEnum.KIDNEY)));

        String changeDesc = profile.getChanges().get(0);
        Date currentDate = new Date();

        String modelSolution = currentDate.toString() + ": Changed the organs to donate to: Bone, Liver, Kidney.";

        assertChangeDescriptionEqual(modelSolution, changeDesc);
    }

    /**
     * Test if we can accurately add a procedure that happened in the past.
     */
    @Test
    public void addProcedureToPastProceduresList() {
        Profile profile = new Profile("John", LocalDate.parse("1996-12-10"), new Date(), "username");
        LocalDate procedureDate = LocalDate.parse("1990-01-01");
        assertEquals(0, profile.getPastProcedures().size());
        profile.addProcedure(new Procedure("Summary", "Description", procedureDate, new ArrayList<>()));
        assertEquals(1, profile.getPastProcedures().size());
        assertEquals(new Procedure("Summary", "Description", procedureDate, new ArrayList<>()),
                profile.getPastProcedures().get(0));
    }

    /**
     * Test if we can accurately add a procedure that has yet to happen.
     */
    @Test
    public void addProcedureToPendingProceduresList() {
        Profile profile = new Profile("John", LocalDate.parse("1996-12-10"), new Date(), "username");
        LocalDate procedureDate = LocalDate.now().plusMonths(1);
        assertEquals(0, profile.getPendingProcedures().size());
        profile.addProcedure(new Procedure("Summary", "Description", procedureDate, new ArrayList<>()));
        assertEquals(1, profile.getPendingProcedures().size());
        assertEquals(new Procedure("Summary", "Description", procedureDate, new ArrayList<>()),
                profile.getPendingProcedures().get(0));
    }


    /**
     * Will test if the description for changing the weight will be accurate and added to their history.
     */
    @Test
    public void changedWeightDescriptionInitialise() {
        Profile profile = new Profile("John", LocalDate.parse("1996-12-10"), new Date(), "username");
        profile.setWeight(54);

        String changeDesc = profile.getChanges().get(0);
        Date currentDate = new Date();

        String modelSolution = currentDate.toString() + ": Weight was set to 54.0";

        assertChangeDescriptionEqual(modelSolution, changeDesc);
    }

    /**
     * Will test if the description for changing the weight will be accurate and added to their history.
     */
    @Test
    public void changedWeightDescription() {
        Profile profile = new Profile("John", LocalDate.parse("1996-12-10"), new Date(), "username");
        profile.setWeight(54);
        profile.setWeight(55);

        String changeDesc = profile.getChanges().get(1);
        Date currentDate = new Date();

        String modelSolution = currentDate.toString() + ": Changed weight from 54.0 to 55.0";

        assertChangeDescriptionEqual(modelSolution, changeDesc);
    }

    /**
     * Will test if the description for changing the height will be accurate and added to their history.
     */
    @Test
    public void changedHeightDescriptionInitialise() {
        Profile profile = new Profile("John", LocalDate.parse("1996-12-10"), new Date(), "username");
        profile.setHeight(154);

        String changeDesc = profile.getChanges().get(0);
        Date currentDate = new Date();

        String modelSolution = currentDate.toString() + ": Height was set to 154.0";

        assertChangeDescriptionEqual(modelSolution, changeDesc);
    }

    /**
     * Will test if the description for changing the height will be accurate and added to their history.
     */
    @Test
    public void changedHeightDescription() {
        Profile profile = new Profile("John", LocalDate.parse("1996-12-10"), new Date(), "username");
        profile.setHeight(154);
        profile.setHeight(155);

        String changeDesc = profile.getChanges().get(1);
        Date currentDate = new Date();

        String modelSolution = currentDate.toString() + ": Changed height from 154.0 to 155.0";

        assertChangeDescriptionEqual(modelSolution, changeDesc);
    }

    /**
     * Will test if the description for changing the weight will be accurate and added to their history.
     */
    @Test
    public void changedSmokerDescriptionTrue() {
        Profile profile = new Profile("John", LocalDate.parse("1996-12-10"), new Date(), "username");
        profile.setSmoker(true);

        String changeDesc = profile.getChanges().get(0);
        Date currentDate = new Date();

        String modelSolution = currentDate.toString() + ": Changed smoker status to YES";

        assertChangeDescriptionEqual(modelSolution, changeDesc);
    }


    /**
     * Will test if the description for changing the blood pressure will be accurate and added to their history.
     */
    @Test
    public void changedRegionDescription() {
        Profile profile = new Profile("John", LocalDate.parse("1996-12-10"), new Date(), "username");
        profile.setRegion(RegionEnum.CANTERBURY);

        String changeDesc = profile.getChanges().get(0);
        Date currentDate = new Date();

        String modelSolution = currentDate.toString() + ": Region was set to Canterbury";

        assertChangeDescriptionEqual(modelSolution, changeDesc);
    }

    /**
     * Will test if the description for changing the alcohol consumption will be accurate and added to their history.
     */
    @Test
    public void changedAlcoholConsumptionDescription() {
        Profile profile = new Profile("John", LocalDate.parse("1996-12-10"), new Date(), "username");
        profile.setAlcoholConsumption(AlcoholConsumptionEnum.MODERATE);

        String changeDesc = profile.getChanges().get(0);
        Date currentDate = new Date();

        String modelSolution = currentDate.toString() + ": Alcohol consumption was set to medium";

        assertChangeDescriptionEqual(modelSolution, changeDesc);
    }

    /**
     * Will test if the description for changing the blood pressure will be accurate and added to their history.
     */
    @Test
    public void changedBloodPressureDescription() {
        Profile profile = new Profile("John", LocalDate.parse("1996-12-10"), new Date(), "username");
        profile.setBloodPressure("150/80");

        String changeDesc = profile.getChanges().get(0);
        Date currentDate = new Date();

        String modelSolution = currentDate.toString() + ": Blood pressure was set to 150/80";

        assertChangeDescriptionEqual(modelSolution, changeDesc);
    }


    /**
     * Test the edge case. Profiles are considered different only if they have different ID's even if all the other
     * attributes are the same.
     */
    @Test
    public void testNotEqual() {
        Profile profile1 = new Profile("fname", LocalDate.now(), new Date(), "user1");
        Profile profile2 = new Profile("fname", LocalDate.now(), new Date(), "user2");
        assertFalse(profile1.equals(profile2));
    }

    /**
     * Test the edge case. Profiles are considered different only if they have different ID's even if all the other
     * attributes are the same.
     */
    @Test
    public void testEqual() {
        Profile profile1 = new Profile("fname", LocalDate.now(), new Date(), "user1");
        Profile profile2 = new Profile("fname", LocalDate.now(), new Date(), "user1");
        assertTrue(profile1.equals(profile2));
    }

    /**
     * Test if an organ the user has selected gets removed.
     */
    @Test
    public void removeOrganTest() {
        Profile profile = new Profile("fname", LocalDate.now(), new Date(), "user1");
        profile.setDonorOrgans(new ArrayList<>(Arrays.asList(OrganEnum.LIVER, OrganEnum.CORNEA)));
        profile.removeDonorOrgan(OrganEnum.LIVER);

        List<OrganEnum> remainingOrgans = profile.getDonorOrgans();

        assertEquals(remainingOrgans.size(), 1);
        assertEquals(remainingOrgans.get(0), OrganEnum.CORNEA);
    }

    /**
     * Test if an organ the user has  NOT selected gets does not cause an error when being attmped to be removed..
     */
    @Test
    public void removeOrganTest2() {
        Profile profile = new Profile("fname", LocalDate.now(), new Date(), "user1");
        profile.setDonorOrgans(new ArrayList<>(Arrays.asList(OrganEnum.LIVER, OrganEnum.CORNEA)));
        profile.removeDonorOrgan(OrganEnum.BONE);

        List<OrganEnum> remainingOrgans = profile.getDonorOrgans();

        assertEquals(remainingOrgans.size(), 2);
        assertEquals(remainingOrgans.get(0), OrganEnum.LIVER);
        assertEquals(remainingOrgans.get(1), OrganEnum.CORNEA);
    }

    /**
     * Test if an organ the user has not selected before gets added to the list.
     */
    @Test
    public void addOrgan() {
        Profile profile = new Profile("fname", LocalDate.now(), new Date(), "user1");
        profile.setDonorOrgans(new ArrayList<>(Collections.singletonList(OrganEnum.LIVER)));
        try {
            profile.addDonorOrgan(OrganEnum.BONE);
        } catch (AlreadyExists alreadyExists) {
            alreadyExists.printStackTrace();
            fail();
        }

        List<OrganEnum> remainingOrgans = profile.getDonorOrgans();

        assertEquals(remainingOrgans.size(), 2);
        assertEquals(remainingOrgans.get(0), OrganEnum.LIVER);
        assertEquals(remainingOrgans.get(1), OrganEnum.BONE);

    }

    /**
     * Test if an organ that the user has already added throws an exception which will carry the msessage to be
     * displayed to the user.
     *
     * @throws AlreadyExists the organ already exists
     */
    @Test(expected = AlreadyExists.class)
    public void addOrgan2() throws AlreadyExists {
        Profile profile = new Profile("fname", LocalDate.now(), new Date(), "user1");
        profile.setDonorOrgans(new ArrayList<>(Collections.singletonList(OrganEnum.LIVER)));
        profile.addDonorOrgan(OrganEnum.LIVER);

    }

    /**
     * Test if an organ the user has selected can be cleared out.
     */
    @Test
    public void clearOrganTest() {
        Profile profile = new Profile("fname", LocalDate.now(), new Date(), "user1");
        profile.setDonorOrgans(new ArrayList<>(Arrays.asList(OrganEnum.LIVER, OrganEnum.CORNEA)));
        profile.clearDonorOrgans();

        assertEquals(profile.getDonorOrgans().size(), 0);
    }

    /**
     * Test if we can accurately calculate the age of a person if we have gone past their birthday.
     */
    @Test
    public void calcAgeTestPassedBirthDay() {
        LocalDate dateOfBirth = LocalDate.parse("1998-04-13");
        LocalDate currentDate = LocalDate.parse("2018-04-14");
        Profile profile = new Profile("Sora", dateOfBirth, new Date(), "sora");

        try {
            Method calcAgeMethod = Profile.class.getDeclaredMethod("calcAge", LocalDate.class, LocalDate.class);
            calcAgeMethod.setAccessible(true);
            int age = (int) calcAgeMethod.invoke(profile, dateOfBirth, currentDate);
            assertEquals(20, age);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test if we can accurately calculate the age of a person if we have haven't gotten passed their birthday yet.
     *
     * @throws InvocationTargetException exception
     */
    @Test
    public void calcAgeTestBeforeBirthDay() throws InvocationTargetException {
        LocalDate dateOfBirth = LocalDate.parse("1998-04-13");
        LocalDate currentDate = LocalDate.parse("2018-04-04");
        Profile profile = new Profile("Sora", dateOfBirth, new Date(), "sora");

        try {
            Method calcAgeMethod = Profile.class.getDeclaredMethod("calcAge", LocalDate.class, LocalDate.class);
            calcAgeMethod.setAccessible(true);
            int age = (int) calcAgeMethod.invoke(profile, dateOfBirth, currentDate);
            assertEquals(19, age);
        } catch (NoSuchMethodException | IllegalAccessException e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test if we can accurately calculate the age of a person if we have haven't gotten passed their birthday yet.
     */
    @Test
    public void calcAgeTestBeforeBirthDay2() {
        LocalDate dateOfBirth = LocalDate.parse("1992-04-14");
        LocalDate currentDate = LocalDate.parse("2018-04-04");
        Profile profile = new Profile("Sora", dateOfBirth, new Date(), "sora");

        try {
            Method calcAgeMethod = Profile.class.getDeclaredMethod("calcAge", LocalDate.class, LocalDate.class);
            calcAgeMethod.setAccessible(true);
            int age = (int) calcAgeMethod.invoke(profile, dateOfBirth, currentDate);
            assertEquals(25, age);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test if we can accurately calculate the age of a person if we the current date if their birthday.
     */
    @Test
    public void calcAgeTestSameDay() {
        LocalDate dateOfBirth = LocalDate.parse("1998-04-13");
        LocalDate currentDate = LocalDate.parse("2018-04-13");
        Profile profile = new Profile("Sora", dateOfBirth, new Date(), "sora");

        try {
            Method calcAgeMethod = Profile.class.getDeclaredMethod("calcAge", LocalDate.class, LocalDate.class);
            calcAgeMethod.setAccessible(true);
            int age = (int) calcAgeMethod.invoke(profile, dateOfBirth, currentDate);
            assertEquals(20, age);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Add a new organ into the receiver organ list. The size of the organ list from 2 to 3.
     */
    @Test
    public void addUniqueReceivingOrganTest() {
        testReceiver1.addReceivingOrgan(OrganEnum.SKIN);
        assertEquals(3, testReceiver1.getReceiverOrgansWithTimes().size());
    }

    /**
     * Attempt to add a pre-existing organ into the receiver organ list in another time It should not create an
     * additional entry so the list should remain at size 2.
     */
    @Test
    public void addExistingReceivingOrganTest() {
        testReceiver1.addReceivingOrgan(OrganEnum.LIVER);
        assertEquals(2, testReceiver1.getReceiverOrgansWithTimes().size());
    }

    /**
     * Remove an existing organ list from a receiver. The size of the organ list from 2 to 1.
     *
     * @throws DoesNotExist thrown when trying to remove ReceiverOrgan not in list
     */
    @Test
    public void removeExistingReceiverOrganTest() throws DoesNotExist {
        testReceiver1.removeReceiverOrgan(OrganEnum.LIVER);
        assertEquals(1, testReceiver1.getReceiverOrgansWithTimes().size());
    }


    /**
     * Remove not existing organ from the receiver list by a given organ and time which not yet registered. After the
     * test, the register organ list keeps unchanged. It throws DoesNotExist exception.
     *
     * @throws DoesNotExist Thrown when trying to remove ReceiverOrgan that is not in the list
     */
    @Test(expected = DoesNotExist.class)
    public void removeNonRegisteredReceiverOrganTest() throws DoesNotExist {
        testReceiver1.removeReceiverOrgan(OrganEnum.HEART);
        assertTrue(2 == testReceiver1.getReceiverOrgansWithTimes().size());
    }

}
