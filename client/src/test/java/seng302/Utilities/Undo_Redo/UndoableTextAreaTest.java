package seng302.Utilities.Undo_Redo;

import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UndoableTextAreaTest extends UndoRedoTestSetupClass {

    private TextArea testArea;
    private UndoableTextArea testable;

    @Override
    public void start(Stage stage) {

    }

    @Before
    public void setup() {
        testArea = new TextArea("This is the original text");
        this.testable = new UndoableTextArea(testArea, testArea.getText());
        this.testArea.setText("Not the original text");
    }

    @Test
    public void execute() {
        testable.execute();
        assertEquals("This is the original text", testArea.getText());
    }

    @Test
    public void revert() {
        testable.execute();
        assertEquals("This is the original text", testArea.getText());
        testable.revert();
        assertEquals("Not the original text", testArea.getText());
    }
}