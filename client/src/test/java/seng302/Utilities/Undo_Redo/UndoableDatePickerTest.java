package seng302.Utilities.Undo_Redo;

import javafx.scene.control.DatePicker;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class UndoableDatePickerTest extends UndoRedoTestSetupClass {

    private DatePicker testPicker;
    private UndoableDatePicker testable;

    @Before
    public void setup() {
        testPicker = new DatePicker(LocalDate.of(1992, 2, 15));
        testable = new UndoableDatePicker(this.testPicker, this.testPicker.getValue());
    }

    @Test
    public void execute() {
        this.testPicker.setValue(LocalDate.now());
        this.testable.execute();
        assertEquals(this.testPicker.getValue().toString(), LocalDate.of(1992, 2, 15).toString());
    }

    @Test
    public void revert() {
        this.testPicker.setValue(LocalDate.now());
        this.testable.revert();
        assertEquals(this.testPicker.getValue().toString(), LocalDate.of(1992, 2, 15).toString());
    }
}