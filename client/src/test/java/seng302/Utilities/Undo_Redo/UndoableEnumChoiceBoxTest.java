package seng302.Utilities.Undo_Redo;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ChoiceBox;
import org.junit.Before;
import org.junit.Test;
import seng302.Enum.OrganEnum;

import static org.junit.Assert.assertEquals;

public class UndoableEnumChoiceBoxTest extends UndoRedoTestSetupClass {

    private ChoiceBox<Enum> testBox;
    private UndoableEnumChoiceBox testable;


    @Before
    public void setup() {
        ObservableList<Enum> temp = FXCollections.observableArrayList(OrganEnum.values());
        this.testBox = new ChoiceBox<>(temp);
        this.testBox.setValue(OrganEnum.LIVER);
        this.testable = new UndoableEnumChoiceBox(this.testBox, this.testBox.getValue());
    }

    @Test
    public void execute() {
        this.testBox.setValue(OrganEnum.HEART);
        this.testable.execute();
        assertEquals(OrganEnum.LIVER, this.testBox.getValue());
    }

    @Test
    public void revert() {
        this.testBox.setValue(OrganEnum.LUNG);
        this.testable.execute();
        assertEquals(OrganEnum.LIVER, this.testBox.getValue());
    }


}