package seng302.Utilities.Undo_Redo;

import javafx.scene.control.CheckBox;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.testfx.api.FxToolkit;

import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertTrue;

public class UndoableCheckBoxTest extends UndoRedoTestSetupClass {

    private CheckBox testBox;
    private UndoableCheckBox testable;

    @Before
    public void setup() {
        this.testBox = new CheckBox();
        this.testBox.setSelected(true);
        this.testable = new UndoableCheckBox(this.testBox);
    }

    @After
    public void teardown() throws TimeoutException {
        FxToolkit.cleanupStages();
        release(new KeyCode[]{});
        release(new MouseButton[]{});
    }

    @Test
    public void execute() {
        this.testBox.setSelected(false);
        this.testable.execute();
        assertTrue(this.testBox.isSelected());
    }

    @Test
    public void revert() {
        this.testBox.setSelected(false);
        this.testable.revert();
        assertTrue(this.testBox.isSelected());
    }
}