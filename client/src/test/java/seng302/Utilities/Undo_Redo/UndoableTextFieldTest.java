package seng302.Utilities.Undo_Redo;

import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class UndoableTextFieldTest extends UndoRedoTestSetupClass {

    private TextField testField;
    private UndoableTextField testable;

    @Override
    public void start(Stage stage) {

    }

    @Before
    public void setup() {
        testField = new TextField("This is the correct text");
        this.testable = new UndoableTextField(testField, testField.getText());
        this.testField.setText("Not the correct text");
    }

    @Test
    public void execute() {
        testable.execute();
        assertEquals("This is the correct text", testField.getText());
    }

    @Test
    public void revert() {
        testable.revert();
        assertEquals("This is the correct text", testField.getText());
    }

}