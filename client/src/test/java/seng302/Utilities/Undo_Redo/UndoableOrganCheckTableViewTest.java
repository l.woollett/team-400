package seng302.Utilities.Undo_Redo;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.control.TableView;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import seng302.Model.ReceiverOrgan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class UndoableOrganCheckTableViewTest extends UndoRedoTestSetupClass {

    private SimpleBooleanProperty testProperty;
    private TableView<ReceiverOrgan> table;

    private UndoableOrganCheckTableView testable;

    @Before
    public void setup() {
        testProperty = new SimpleBooleanProperty(false);
        //testable = new UndoableOrganCheckTableView(testProperty, table);
    }

    @Test
    @Ignore //Completely changed implementation
    public void execute() {
        testProperty.setValue(true);
        testable.execute();
        assertFalse(testProperty.getValue());
    }

    @Test
    @Ignore //Completely changed implementation
    public void revert() {
        testProperty.setValue(true);
        testable.execute();
        testable.revert();
        assertTrue(testProperty.getValue());
    }
}