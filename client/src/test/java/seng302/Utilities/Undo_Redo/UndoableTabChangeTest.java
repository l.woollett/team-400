package seng302.Utilities.Undo_Redo;

import javafx.scene.Scene;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit.ApplicationTest;

import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertEquals;

public class UndoableTabChangeTest extends ApplicationTest {

    private SingleSelectionModel<Tab> selectionModel;
    private Tab tab1, tab2;
    private UndoableTabChange testable;

    /**
     * Creates a basic Scene consisting of an AnchorPane that contains a TabPane with two Tabs.
     *
     * @param stage The stage that is being set up.
     */
    @Override
    public void start(Stage stage) {
        // Setup tab pane
        TabPane testTabPane = new TabPane();
        selectionModel = testTabPane.getSelectionModel();
        tab1 = new Tab();
        tab2 = new Tab();
        testTabPane.getTabs().addAll(tab1, tab2);

        // Set up main container and add tab pane
        AnchorPane root = new AnchorPane();
        root.getChildren().add(testTabPane);

        // Setup the Scene
        Scene scene = new Scene(root, 50, 50);
        stage.setScene(scene);
    }

    /**
     * Ensures that the first tab is selected to mimic a user clicking the tab and this event is created in it's
     * undoable form.
     */
    @Before
    public void setup() {
        selectionModel.select(tab1);
        this.testable = new UndoableTabChange(selectionModel, tab1);
    }

    /**
     * Performs an undo.
     */
    @Test
    public void execute() {
        this.selectionModel.select(tab2);
        testable.execute();
        assertEquals(tab1, selectionModel.getSelectedItem());
    }

    /**
     * Performs a redo
     */
    @Test
    public void revert() {
        this.selectionModel.select(tab2);
        testable.revert();
        assertEquals(tab1, selectionModel.getSelectedItem());
    }

    /**
     * Releases variable used int the tests. This may be removable.
     *
     * @throws TimeoutException Thrown if it times out.
     */
    @After
    public void teardown() throws TimeoutException {
        FxToolkit.cleanupStages();
        release(new KeyCode[]{});
        release(new MouseButton[]{});
    }
}