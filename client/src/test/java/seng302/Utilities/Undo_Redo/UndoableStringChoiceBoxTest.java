package seng302.Utilities.Undo_Redo;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ChoiceBox;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UndoableStringChoiceBoxTest extends UndoRedoTestSetupClass {

    private ChoiceBox<String> testBox;
    private UndoableStringChoiceBox testAble;

    @Before
    public void setup() {
        ObservableList<String> temp = FXCollections.observableArrayList("Test1", "Test2", "Test3");
        this.testBox = new ChoiceBox<>(temp);
        this.testBox.setValue("Test1");
        this.testAble = new UndoableStringChoiceBox(testBox, testBox.getValue());
    }

    @Test
    public void execute() {
        this.testBox.setValue("Test2");
        this.testAble.execute();
        assertEquals("Test1", this.testBox.getValue());
    }

    @Test
    public void revert() {
        this.testBox.setValue("Test2");
        this.testAble.execute();
        assertEquals("Test1", this.testBox.getValue());
    }
}