package seng302.Utilities.Undo_Redo;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import seng302.CustomException.DoesNotExist;
import seng302.Enum.*;
import seng302.GUI.Profile.ProfileSceneController;
import seng302.Model.Death;
import seng302.Model.Hospital;
import seng302.Model.Profile;
import seng302.ModelController.ProfileController;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.Assert.*;

public class UndoableProfileTest {

    private Profile testProfile;
    private ArrayList<OrganEnum> donorOrgans = new ArrayList<>();
    private ArrayList<OrganEnum> receivingOrgans = new ArrayList<>();
    private UndoableProfile testable;
    private Death death;
    private ProfileSceneController controller = new ProfileSceneController();

    @Before
    public void setup() {
        this.donorOrgans.add(OrganEnum.LIVER);
        this.donorOrgans.add(OrganEnum.LUNG);
        this.receivingOrgans.add(OrganEnum.BONE);
        this.testProfile = new Profile("fName", "mName",
                "lName", LocalDate.of(1992, 2, 16), null,
                GenderEnum.MALE, 1.78f, 70f, BloodTypeEnum.A_NEGATIVE, "test address",
                RegionEnum.AUCKLAND, new Date(), null, true, this.donorOrgans,
                "uName1", true, this.receivingOrgans);
    }

    @Test
    @Ignore
    public void executeCreateEvent() {
        boolean testPass = false;
        this.testable = new UndoableProfile(null, this.testProfile, controller, null, null);
        ProfileController.addProfile(this.testProfile.getUsername(), this.testProfile);
        this.testable.execute();
        try {
            ProfileController.getProfile(this.testProfile.getUsername());
        } catch (DoesNotExist e) {
            testPass = true;
        }
        assertTrue(testPass);
    }

    @Test
    @Ignore
    public void executeDeleteEvent() {
        this.testable = new UndoableProfile(this.testProfile, null, controller, null, null);
        ProfileController.addProfile(this.testProfile.getUsername(), this.testProfile);
        try {
            ProfileController.deleteProfile(this.testProfile.getUsername());
        } catch (DoesNotExist e) {
            fail(e.getMessage());
        }
        this.testable.execute();
        assertTrue(ProfileController.getProfiles().contains(testProfile));
    }

    /**
     * Yes I know there are many asserts in this test, they are all necessary since we are dealing with copying an
     * object and restoring values using the copy, need to ensure that we have a successful deep copy.
     */
    @Test
    @Ignore //!!!!!!Doesn't like the edit profile event!!!!!!!!!
    public void execute() {
        this.testable = new UndoableProfile(this.testProfile, this.testProfile, controller, null, null);
        makeAndApplyChanges();
        this.testable.execute();
        assertEquals("fName", this.testProfile.getFirstName());
        assertEquals("mName", this.testProfile.getMiddleName());
        assertEquals("lName", this.testProfile.getLastName());
        assertEquals("uName1", this.testProfile.getUsername());
        assertEquals("test address", this.testProfile.getAddress());
        assertEquals(RegionEnum.AUCKLAND, this.testProfile.getRegion());
        assertEquals(GenderEnum.MALE, this.testProfile.getGender());
        assertEquals(LocalDate.of(1992, 2, 16), this.testProfile.getDateOfBirth());
        assertNull(this.testProfile.getDeath().getDateOfDeath());
        assertEquals(1.78f, this.testProfile.getHeight(), 0.0001);
        assertEquals(70f, this.testProfile.getWeight(), 0.0001);
        assertEquals(BloodTypeEnum.A_NEGATIVE, this.testProfile.getBloodType());
        assertTrue(this.testProfile.isDonor());
        assertEquals(2, this.testProfile.getDonorOrgans().size());
        assertFalse(this.testProfile.isSmoker());
        assertEquals("", this.testProfile.getBloodPressure());
        assertEquals(AlcoholConsumptionEnum.NOT_SET, this.testProfile.getAlcoholConsumption());
        assertNull(this.testProfile.getBirthGender());
        assertEquals("", this.testProfile.getAlias());
        assertEquals(1, this.testProfile.getReceiverOrgans().size());
        assertTrue(this.testProfile.isReceiver());
        assertEquals(testProfile.getCreatedDate().getTime(), testProfile.getModifiedDate().getTime());
    }

    /**
     * Undo a create event, then redo it
     */
    @Test
    @Ignore //!!!!!! Stage title? !!!!!!!!!
    public void revertCreateEventUndo() {
        this.testable = new UndoableProfile(null, this.testProfile, controller, null, null);
        ProfileController.addProfile(this.testProfile.getUsername(), this.testProfile);
        this.testable.execute();
        this.testable.revert();
        assertTrue(ProfileController.getProfiles().contains(testProfile));
    }

    /**
     * Undo a delete event, then redo it
     */
    @Test
    @Ignore //!!!!!! Stage title? !!!!!!!!!
    public void revertDeleteEvent() {
        this.testable = new UndoableProfile(this.testProfile, null, controller, null, null);
        boolean testPass = false;
        ProfileController.addProfile(this.testProfile.getUsername(), this.testProfile);
        try {
            ProfileController.deleteProfile(this.testProfile.getUsername());
        } catch (DoesNotExist e) {
            fail(e.getMessage());
        }
        this.testable.execute();
        this.testable.revert();
        try {
            ProfileController.getProfile(this.testProfile.getUsername());
        } catch (DoesNotExist e) {
            testPass = true;
        }
        assertTrue(testPass);
    }

    /**
     * Undo a customization then redo it. Yes I know like 20+ asserts, again it's necessary since we are dealing with
     * copying objects.
     */
    @Test
    @Ignore //!!!!!!Doesn't like the edit profile event!!!!!!!!!
    public void revert() {
        this.testable = new UndoableProfile(this.testProfile, this.testProfile, controller, null, null);
        makeAndApplyChanges();
        this.testable.execute();
        this.testable.revert();

        assertEquals(death, this.testProfile.getDeath().getDateOfDeath());
        assertEquals(0, this.testProfile.getReceiverOrgans().size());
        assertEquals("WRONG", this.testProfile.getFirstName());
        assertEquals("WRONG", this.testProfile.getMiddleName());
        assertEquals("WRONG", this.testProfile.getLastName());
        assertEquals("WRONG", this.testProfile.getUsername());
        assertEquals("WRONG", this.testProfile.getAddress());
        assertEquals(RegionEnum.CANTERBURY, this.testProfile.getRegion());
        assertEquals(GenderEnum.FEMALE, this.testProfile.getGender());
        assertEquals(LocalDate.of(1992, 2, 15), this.testProfile.getDateOfBirth());
        assertEquals(death, this.testProfile.getDeath().getDateOfDeath());
        assertEquals(1.9, this.testProfile.getHeight(), 0.0001);
        assertEquals(90, this.testProfile.getWeight(), 0.0001);
        assertEquals(BloodTypeEnum.AB_POSITIVE, this.testProfile.getBloodType());
        assertFalse(this.testProfile.isDonor());
        assertEquals(0, this.testProfile.getDonorOrgans().size());
        assertTrue(this.testProfile.isSmoker());
        assertEquals("120/90", this.testProfile.getBloodPressure());
        assertEquals(AlcoholConsumptionEnum.HIGH, this.testProfile.getAlcoholConsumption());
        assertEquals(GenderEnum.MALE, this.testProfile.getBirthGender());
        assertEquals("WRONG", this.testProfile.getAlias());
        assertEquals(0, this.testProfile.getReceiverOrgans().size());
        assertFalse(this.testProfile.isReceiver());
    }

    private void makeAndApplyChanges() {
        death = new Death(LocalDate.now(), new Hospital("Canterbury Hospital", RegionEnum.CANTERBURY, "123 Fake Street", 1, 1), null);
        try {
            this.testProfile.removeReceiverOrgan(OrganEnum.BONE);
        } catch (DoesNotExist e) {
            fail();
        }
        this.testProfile.setFirstName("WRONG");
        this.testProfile.setMiddleName("WRONG");
        this.testProfile.setLastName("WRONG");
        this.testProfile.setUsername("WRONG");
        this.testProfile.setAddress("WRONG");
        this.testProfile.setRegion(RegionEnum.CANTERBURY);
        this.testProfile.setGender(GenderEnum.FEMALE);
        this.testProfile.setDateOfBirth(LocalDate.of(1992, 2, 15));
        this.testProfile.setDeath(death);
        this.testProfile.setHeight(1.9f);
        this.testProfile.setWeight(90f);
        this.testProfile.setBloodType(BloodTypeEnum.AB_POSITIVE);
        this.testProfile.setIsDonor(false);
        this.testProfile.setDonorOrgans(new ArrayList<>());
        this.testProfile.setSmoker(true);
        this.testProfile.setBloodPressure("120/90");
        this.testProfile.setAlcoholConsumption(AlcoholConsumptionEnum.HIGH);
        this.testProfile.setBirthGender(GenderEnum.MALE);
        this.testProfile.setAlias("WRONG");
        this.testProfile.setReceiverOrgans(new ArrayList<>());
        this.testProfile.setIsReceiver(false);
    }
}