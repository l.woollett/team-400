package seng302.Utilities.Undo_Redo;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import org.junit.Before;
import org.junit.Test;
import seng302.Enum.OrganEnum;

import java.util.HashMap;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class UndoableOrganCheckListViewTest {

    private HashMap<OrganEnum, ObservableValue<Boolean>> testMap;
    private UndoableOrganCheckListView testable;
    private ObservableValue<Boolean> obs;

    @Before
    public void setup() {
        obs = new SimpleBooleanProperty(false);
        testMap = new HashMap<>();
        testMap.put(OrganEnum.LIVER, obs);
        testable = new UndoableOrganCheckListView((SimpleBooleanProperty) obs);
    }

    @Test
    public void execute() {
        SimpleBooleanProperty test = (SimpleBooleanProperty) obs;
        test.setValue(true);
        testable.execute();
        assertFalse(testMap.get(OrganEnum.LIVER).getValue());
    }

    @Test
    public void revert() {
        SimpleBooleanProperty test = (SimpleBooleanProperty) obs;
        test.setValue(true);
        testable.execute();
        testable.revert();
        assertTrue(testMap.get(OrganEnum.LIVER).getValue());
    }
}
