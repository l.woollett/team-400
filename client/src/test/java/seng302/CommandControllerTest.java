package seng302;

import org.apache.commons.cli.Options;
import org.apache.tools.ant.types.Commandline;
import org.junit.Before;
import org.junit.Test;
import seng302.CustomException.DoesNotExist;
import seng302.CustomException.InvalidCommand;
import seng302.Enum.*;
import seng302.Model.Clinician;
import seng302.Model.Hospital;
import seng302.Model.Profile;
import seng302.ModelController.ClinicianController;
import seng302.ModelController.CommandController;
import seng302.ModelController.ProfileController;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;

import static org.junit.Assert.*;
import static seng302.Enum.GenderEnum.FEMALE;
import static seng302.Enum.OrganEnum.LIVER;

public class CommandControllerTest {

    @Before
    public void clearUsers() {
        ProfileController.clearProfiles();
        ClinicianController.clearClinicians();
    }

    /**
     * Extract only the arguments.
     *
     * @param parsedInput The command input which contains the command and all arguments.
     * @return Array of just the arguments.
     */
    private String[] getArgs(String[] parsedInput) {
        return Arrays.copyOfRange(parsedInput, 1, parsedInput.length);
    }

    /**
     * Test if the if the quit command will correctly throw an error if arguments are added.
     *
     * @throws InvalidCommand quit should not take any arguments
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsQuitInvalid() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("quit -invalid");
        CommandController.checkArguments(CommandEnum.QUIT, getArgs(parsedInput));
    }

    /**
     * Test if the if the quit command will correctly throw an error if arguments are added.
     *
     * @throws InvalidCommand quit should not take any arguments
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsQuitInvalid2() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("quit invalid");
        CommandController.checkArguments(CommandEnum.QUIT, getArgs(parsedInput));
    }

    /**
     * Test if the quit command will be correctly assessed to be valid. The quit command does not take any arguments.
     */
    @Test
    public void checkArgumentsQuitValid() {

        try {
            String[] parsedInput = Commandline.translateCommandline("quit");
            CommandController.checkArguments(CommandEnum.QUIT, getArgs(parsedInput));
        } catch (InvalidCommand invalidCommand) {
            invalidCommand.printStackTrace();
            fail();
        }

        //If it reaches here without throwing an exception it passes.=
    }


    /**
     * Test if commands can be assessed to be correct with even a lot of spaces.
     */
    @Test
    public void checkArgumentsSpaceValid() {

        try {
            String[] parsedInput = Commandline.translateCommandline("   quit    ");
            CommandController.checkArguments(CommandEnum.QUIT, getArgs(parsedInput));
        } catch (InvalidCommand invalidCommand) {
            invalidCommand.printStackTrace();
            fail();
        }

        //If it reaches here without throwing an exception it passes.=
    }

    /**
     * Test if commands can be assessed to be correct with even a lot of spaces.
     */
    @Test
    public void checkArgumentsSpaceValid2() {

        try {
            String[] parsedInput = Commandline.translateCommandline("help           -all");
            CommandController.checkArguments(CommandEnum.HELP, getArgs(parsedInput));
        } catch (InvalidCommand invalidCommand) {
            invalidCommand.printStackTrace();
            fail();
        }

        //If it reaches here without throwing an exception it passes.=
    }

    /**
     * Test if the correct arguments for the help command can be identified.
     */
    @Test
    public void checkArgumentsHelpValid() {
        try {
            String[] parsedInput = Commandline.translateCommandline("help -all");
            CommandController.checkArguments(CommandEnum.HELP, getArgs(parsedInput));
        } catch (InvalidCommand e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test if the correct arguments for the help command can be identified.
     */
    @Test
    public void checkArgumentsHelpValid2() {
        try {
            String[] parsedInput = Commandline.translateCommandline("help -command=help");
            CommandController.checkArguments(CommandEnum.HELP, getArgs(parsedInput));
        } catch (InvalidCommand e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test if the correct arguments for the help command can be identified.
     */
    @Test
    public void checkArgumentsHelpValid3() {
        try {
            String[] parsedInput = Commandline.translateCommandline("help -all");
            CommandController.checkArguments(CommandEnum.HELP, getArgs(parsedInput));
        } catch (InvalidCommand e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test if the incorrect arguments for the help command can be identified. Only one argument can be applied at a
     * time.
     *
     * @throws InvalidCommand Can only take one argument.
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsHelpInvalid() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("help -all -command=help");
        CommandController.checkArguments(CommandEnum.HELP, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect arguments for the help command can be identified. Arguments needs to be preceded by one or
     * two hyphens.
     *
     * @throws InvalidCommand Needs hyphen.
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsHelpInvalid2() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("help all");
        CommandController.checkArguments(CommandEnum.HELP, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect arguments for the help command can be identified. Must have one argument.
     *
     * @throws InvalidCommand Command does not exist
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsHelpInvalid3() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("help -command=asdf");
        CommandController.checkArguments(CommandEnum.HELP, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect arguments for the help command can be identified. Must have one argument.
     *
     * @throws InvalidCommand Needs arguments.
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsHelpInvalid4() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("help");
        CommandController.checkArguments(CommandEnum.HELP, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect arguments for the help command can be identified. Must have one argument.
     *
     * @throws InvalidCommand Argument needs a value
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsHelpInvalid5() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("help -command=");
        CommandController.checkArguments(CommandEnum.HELP, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect arguments for the help command can be identified. Must have one argument.
     *
     * @throws InvalidCommand Argument needs a value.
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsHelpInvalid6() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("help -command");
        CommandController.checkArguments(CommandEnum.HELP, getArgs(parsedInput));
    }


    /**
     * Test if the correct usage of the display profile command can be identified.
     */
    @Test
    public void checkArgumentsDisplayProfileValid() {
        try {
            String[] parsedInput = Commandline.translateCommandline("display_profile -all");
            CommandController.checkArguments(CommandEnum.DISPLAY_PROFILE, getArgs(parsedInput));
        } catch (InvalidCommand e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test if the correct usage of the display profile command can be identified.
     */
    @Test
    public void checkArgumentsDisplayProfileValid2() {
        try {
            String[] parsedInput = Commandline.translateCommandline("display_profile -username xd");
            CommandController.checkArguments(CommandEnum.DISPLAY_PROFILE, getArgs(parsedInput));
        } catch (InvalidCommand e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test if the incorrect usage of the display profile command can be identified. Can only accept one argument at a
     * time.
     *
     * @throws InvalidCommand Incorrect usage. Can only take one of the arguments.
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsDisplayProfileInvalid() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("display_profile -all -id=12");
        CommandController.checkArguments(CommandEnum.DISPLAY_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect usage of the display profile command can be identifies. Needs hyphens.
     *
     * @throws InvalidCommand needs hyphen
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsDisplayProfileInvalid2() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("display_profile all");
        CommandController.checkArguments(CommandEnum.DISPLAY_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect usage of the display profile command can be identifies. Needs an argument.
     *
     * @throws InvalidCommand needs arguments
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsDisplayProfileInvalid3() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("display_profile");
        CommandController.checkArguments(CommandEnum.DISPLAY_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect usage of the display profile command can be identifies. Will detect the extra junk and
     * throw an exception.
     *
     * @throws InvalidCommand Not recognized argument/value
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsDisplayProfileInvalid4() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("display_profile -all asdf");
        CommandController.checkArguments(CommandEnum.DISPLAY_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect usage of the display profile command can be identifies. Will detect the extra junk and
     * throw an exception.
     *
     * @throws InvalidCommand Wrong argument.
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsDisplayProfileInvalid5() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("display_profile asdf");
        CommandController.checkArguments(CommandEnum.DISPLAY_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect usage of the display profile command can be identifies.
     *
     * @throws InvalidCommand argument needs value
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsDisplayProfileInvalid6() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("display_profile -id=");
        CommandController.checkArguments(CommandEnum.DISPLAY_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect usage of the display profile command can be identifies.
     *
     * @throws InvalidCommand argument needs value
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsDisplayProfileInvalid7() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("display_profile -id ");
        CommandController.checkArguments(CommandEnum.DISPLAY_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect usage of the display profile command can be identifies.
     *
     * @throws InvalidCommand Invalid value for the argument.
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsDisplayProfileInvalid8() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("display_profile -id=asdf");
        CommandController.checkArguments(CommandEnum.DISPLAY_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect usage of the display profile command can be identifies.
     *
     * @throws InvalidCommand Invalid value for the argument.
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsDisplayProfileInvalid9() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("display_profile -id asdf");
        CommandController.checkArguments(CommandEnum.DISPLAY_PROFILE, getArgs(parsedInput));
    }


    /**
     * Test if the incorrect usage of the create profile command can be recognized.
     *
     * @throws InvalidCommand Missing arguments.
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsCreateProfileInvalid() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("create_profile");
        CommandController.checkArguments(CommandEnum.CREATE_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect usage of the create profile command can be recognized. The firstName is mandatory
     *
     * @throws InvalidCommand First name is a mandatory field.
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsCreateProfileInvalid2() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("create_profile -lastName=Smith " +
                "-dateOfBirth=1997-05-12 -gender=Male -height=1.2 -weight=34.5 -bloodType=O- -currentAddress=\"Blah Street\" " +
                "-region=Canterbury");
        CommandController.checkArguments(CommandEnum.CREATE_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect usage of the create profile command can be recognized. Need hyphens.
     *
     * @throws InvalidCommand needs hyphen
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsCreateProfileInvalid3() throws InvalidCommand {
        String[] parsedInput =
                Commandline.translateCommandline("create_profile firstName=John -dateOfBirth=1997-09-30");
        CommandController.checkArguments(CommandEnum.CREATE_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect usage of the create profile command can be recognized. If a value needs a space it needs to
     * be put in quotation marks.
     *
     * @throws InvalidCommand If the first name has a space, then needs to be surrounded by quotation marks
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsCreateProfileInvalid4() throws InvalidCommand {
        String[] parsedInput =
                Commandline.translateCommandline("create_profile -firstName=John Smith -dateOfBirth=1997-12-30");
        CommandController.checkArguments(CommandEnum.CREATE_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect usage of the create_profile can be identified. First name is mandatory.
     *
     * @throws InvalidCommand Need value for argument.
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsCreateProfileInvalid6() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("create_profile -firstName= -dateOfBirth=1997-05-12");
        CommandController.checkArguments(CommandEnum.CREATE_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect usage of the create_profile can be identified. First name is mandatory.
     *
     * @throws InvalidCommand Need value for argument.
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsCreateProfileInvalid7() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("create_profile -firstName -dateOfBirth=1997-05-12");
        CommandController.checkArguments(CommandEnum.CREATE_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect usage of the create_profile can be identified. First name is mandatory.
     *
     * @throws InvalidCommand Incorrect format for date of birth
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsCreateProfileInvalid8() throws InvalidCommand {
        String[] parsedInput =
                Commandline.translateCommandline("create_profile -firstName Hello -dateOfBirth=1997/05/12");
        CommandController.checkArguments(CommandEnum.CREATE_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect usage of the create_profile can be identified. Invalid date.
     *
     * @throws InvalidCommand Invalid date
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsCreateProfileInvalid9() throws InvalidCommand {
        String[] parsedInput =
                Commandline.translateCommandline("create_profile -firstName=John -dateOfBirth=1997-99-99");
        CommandController.checkArguments(CommandEnum.CREATE_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect usage of the create_profile can be identified. Date of birth is mandatory
     *
     * @throws InvalidCommand Date of birth is a mandatory field.
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsCreateProfileInvalid10() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("create_profile -firstName=John");
        CommandController.checkArguments(CommandEnum.CREATE_PROFILE, getArgs(parsedInput));
    }


    /**
     * Test if the incorrect usage of the create_profile can be identified. invalid date of date of death
     *
     * @throws InvalidCommand Missing value for argument
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsCreateProfileInvalid11() throws InvalidCommand {
        String[] parsedInput = Commandline
                .translateCommandline("create_profile -firstName=John -dateOfBirth=1997-09-30 -dateOfDeath=");
        CommandController.checkArguments(CommandEnum.CREATE_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect usage of the create_profile can be identified. invalid date of date of death
     *
     * @throws InvalidCommand wrong format for date
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsCreateProfileInvalid12() throws InvalidCommand {
        String[] parsedInput = Commandline
                .translateCommandline("create_profile -firstName=John -dateOfBirth=1997-09-30 -dateOfDeath=1998/12/20");
        CommandController.checkArguments(CommandEnum.CREATE_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect usage of the create_profile can be identified. invalid is donor argument.
     *
     * @throws InvalidCommand value missing
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsCreateProfileInvalid13() throws InvalidCommand {
        String[] parsedInput =
                Commandline.translateCommandline("create_profile -firstName=John -dateOfBirth=1997-09-30 -isDonor=");
        CommandController.checkArguments(CommandEnum.CREATE_PROFILE, getArgs(parsedInput));
    }


    /**
     * Test if the incorrect usage of the create_profile can be identified. invalid is donor argument.
     *
     * @throws InvalidCommand incorrect value
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsCreateProfileInvalid14() throws InvalidCommand {
        String[] parsedInput = Commandline
                .translateCommandline("create_profile -firstName=John -dateOfBirth=1997-09-30 -isDonor=asdf");
        CommandController.checkArguments(CommandEnum.CREATE_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect usage of the create_profile can be identified. invalid bloodtype.
     *
     * @throws InvalidCommand invalid blood type
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsCreateProfileInvalid15() throws InvalidCommand {
        String[] parsedInput =
                Commandline.translateCommandline("create_profile -firstName=John -dateOfBirth=1997-09-30 -bloodType=A");
        CommandController.checkArguments(CommandEnum.CREATE_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect usage of the create_profile can be identified. invalid bloodtype.
     *
     * @throws InvalidCommand missing value for blood type
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsCreateProfileInvalid16() throws InvalidCommand {
        String[] parsedInput =
                Commandline.translateCommandline("create_profile -firstName=John -dateOfBirth=1997-09-30 -bloodType=");
        CommandController.checkArguments(CommandEnum.CREATE_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect usage of the create_profile can be identified. invalid gender
     *
     * @throws InvalidCommand invalid gender
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsCreateProfileInvalid17() throws InvalidCommand {
        String[] parsedInput =
                Commandline.translateCommandline("create_profile -firstName=John -dateOfBirth=1997-09-30 -gender=q");
        CommandController.checkArguments(CommandEnum.CREATE_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect usage of the create_profile can be identified. invalid gender
     *
     * @throws InvalidCommand Missing value for gender
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsCreateProfileInvalid18() throws InvalidCommand {
        String[] parsedInput =
                Commandline.translateCommandline("create_profile -firstName=John -dateOfBirth=1997-09-30 -gender=");
        CommandController.checkArguments(CommandEnum.CREATE_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect usage of the create_profile can be identified. invalid height
     *
     * @throws InvalidCommand Innvalid height
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsCreateProfileInvalid19() throws InvalidCommand {
        String[] parsedInput =
                Commandline.translateCommandline("create_profile -firstName=John -dateOfBirth=1997-09-30 -height=asdf");
        CommandController.checkArguments(CommandEnum.CREATE_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect usage of the create_profile can be identified. invalid height
     *
     * @throws InvalidCommand missing value for height
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsCreateProfileInvalid20() throws InvalidCommand {
        String[] parsedInput =
                Commandline.translateCommandline("create_profile -firstName=John -dateOfBirth=1997-09-30 -height=");
        CommandController.checkArguments(CommandEnum.CREATE_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect usage of the create_profile can be identified. invalid weight
     *
     * @throws InvalidCommand Invalid value for height
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsCreateProfileInvalid21() throws InvalidCommand {
        String[] parsedInput =
                Commandline.translateCommandline("create_profile -firstName=John -dateOfBirth=1997-09-30 -weight=asdf");
        CommandController.checkArguments(CommandEnum.CREATE_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect usage of the create_profile can be identified. invalid weight
     *
     * @throws InvalidCommand Missing value for height
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsCreateProfileInvalid22() throws InvalidCommand {
        String[] parsedInput =
                Commandline.translateCommandline("create_profile -firstName=John -dateOfBirth=1997-09-30 -weight=");
        CommandController.checkArguments(CommandEnum.CREATE_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect usage of the create_profile can be identified. invalid donorOrgans
     *
     * @throws InvalidCommand Missing value for donorOrgans
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsCreateProfileInvalid23() throws InvalidCommand {
        String[] parsedInput = Commandline
                .translateCommandline("create_profile -firstName=John -dateOfBirth=1997-09-30 -donorOrgans=");
        CommandController.checkArguments(CommandEnum.CREATE_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the incorrect usage of the create_profile can be identified. invalid donorOrgans
     *
     * @throws InvalidCommand Invalid value for donorOrgans
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsCreateProfileInvalid24() throws InvalidCommand {
        String[] parsedInput = Commandline
                .translateCommandline("create_profile -firstName=John -dateOfBirth=1997-09-30 -donorOrgans=ASDF");
        CommandController.checkArguments(CommandEnum.CREATE_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the arguments of the command for edit profile command can be correctly identified.
     */
    @Test
    public void checkArgumentsEditProfileValid() {
        try {
            String[] parsedInput = Commandline.translateCommandline(
                    "edit_profile -username xd -firstName=John -middleName=Johny -lastName=Smith " +
                            "-dateOfBirth=1997-05-12 -gender=Male -height=1.2 -weight=34.5 " +
                            "-bloodType=O- -currentAddress=\"Blah Street\" -region=Canterbury " +
                            "-addDonorOrgan=LIVER -removeDonorOrgan=BONE");
            CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
        } catch (InvalidCommand invalidCommand) {
            invalidCommand.printStackTrace();
            fail();
        }
    }

    /**
     * Test if the arguments of the command for edit profile command can be correctly identified.
     */
    @Test
    public void checkArgumentsEditProfileValid2() {
        try {
            String[] parsedInput = Commandline.translateCommandline("edit_profile -username xd -firstName=John");
            CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
        } catch (InvalidCommand invalidCommand) {
            invalidCommand.printStackTrace();
            fail();
        }
    }

    /**
     * Test if the arguments of the command for edit profile command can be correctly identified.
     */
    @Test
    public void checkArgumentsEditProfileValidIsReceiver() {
        try {
            String[] parsedInput =
                    Commandline.translateCommandline("edit_profile -username john_smith -addReceiverOrgan=Liver");
            CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
        } catch (InvalidCommand invalidCommand) {
            invalidCommand.printStackTrace();
            fail();
        }
    }

    /**
     * Test if the arguments of the command for edit profile command can be correctly identified.
     */
    @Test
    public void checkArgumentsEditProfileValidAddReceiverOrgans() {
        try {
            String[] parsedInput =
                    Commandline.translateCommandline("edit_profile -username john_smith -addReceiverOrgan=LIVER");
            CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
        } catch (InvalidCommand invalidCommand) {
            invalidCommand.printStackTrace();
            fail();
        }
    }

    /**
     * Test if the arguments of the command for edit profile command can be correctly identified.
     */
    @Test
    public void checkArgumentsEditProfileValidRemoveReceiverOrgans() {
        try {
            String[] parsedInput =
                    Commandline.translateCommandline("edit_profile -username john_smith -removeReceiverOrgan=LIVER");
            CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
        } catch (InvalidCommand invalidCommand) {
            invalidCommand.printStackTrace();
            fail();
        }
    }

    /**
     * Test if the arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand Thrown when inputting a command that doesn't exist
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalidIsReceiver() throws InvalidCommand {
        String[] parsedInput =
                Commandline.translateCommandline("edit_profile -username john_smith -isReceiver=invalid");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand thrown when inputting a command that doesn't exist.
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalidAddReceiverOrgans() throws InvalidCommand {
        String[] parsedInput =
                Commandline.translateCommandline("edit_profile -username john_smith -addReceiverOrgans=INVALID");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand thrown when inputting a command that doesn't exist.
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalidRemoveReceiverOrgans() throws InvalidCommand {
        String[] parsedInput =
                Commandline.translateCommandline("edit_profile -username john_smith -removeReceiverOrgans=INVALID");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }


    /**
     * Test if the improper use of the command can be identified.
     *
     * @throws InvalidCommand Need arguments
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("edit_profile");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the improper use of the command can be identified.
     *
     * @throws InvalidCommand need to specify attributes of the profile to edit
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid2() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("edit_profile -id=1");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the improper use of the command can be identified.
     *
     * @throws InvalidCommand Missing argument for value
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid3() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("edit_profile -id=1 asdf");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the improper use of the command can be identified.
     *
     * @throws InvalidCommand Missing the ID for the profile to be edited
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid4() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("edit_profile -firstName=Fail");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand Missing value for id
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid5() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline(
                "edit_profile -id -firstName=John -lastName=Smith -dateOfBirth=1997-05-12 -dateOfDeath=2019-12-25 -gender=Male -height=1.2 -weight=34.5 -bloodType=O- -currentAddress=\"Blah Street\" -region=Canterbury -isDonor=true -addDonorOrgan=LIVER -removeDonorOrgan=BONE");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand missing value for id
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid6() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline(
                "edit_profile -id= -firstName=John -lastName=Smith -dateOfBirth=1997-05-12 -dateOfDeath=2019-12-25 -gender=Male -height=1.2 -weight=34.5 -bloodType=O- -currentAddress=\"Blah Street\" -region=Canterbury -isDonor=true -addDonorOrgan=KIDNEY -removeDonorOrgan=BONE_MARROW");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand invalid value for id
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid7() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline(
                "edit_profile -id=asdf -firstName=John -lastName=Smith -dateOfBirth=1997-05-12 -dateOfDeath=2019-12-25 -gender=Male -height=1.2 -weight=34.5 -bloodType=O- -currentAddress=\"Blah Street\" -region=Canterbury -isDonor=true -addDonorOrgan=PANCREAS");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand firstname is a mandatory field can't be left blank or be spaces
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid8() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline(
                "edit_profile -id=1 -firstName=\" \" -lastName=Smith -dateOfBirth=1997-05-12 -dateOfDeath=2019-12-25 -gender=Male -height=1.2 -weight=34.5 -bloodType=O- -currentAddress=\"Blah Street\" -region=Canterbury -isDonor=true -removeDonorOrgan=LIVER");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand first name is a mandatory field can't be left blank, or be the empty string
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid9() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline(
                "edit_profile -id=1 -firstName=\"\" -lastName=Smith -dateOfBirth=1997-05-12 -dateOfDeath=2019-12-25 -gender=Male -height=1.2 -weight=34.5 -bloodType=O- -currentAddress=\"Blah Street\" -region=Canterbury -isDonor=true -addDonorOrgan=CONNECTIVE_TISSUE");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand wrong format for date
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid10() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("edit_profile -id=1 -dateOfDeath=2019/12/25");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand Missing value for date
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid11() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("edit_profile -id=1 -dateOfDeath=");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand Wrong format for date
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid12() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("edit_profile -id=1 -dateOfBirth=1997/05/12");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand missing value for argument
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid13() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("edit_profile -id=1 -dateOfBirth=");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand missing value for date
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid14() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("edit_profile -id=1 -dateOfBirth");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand invalid blood type
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid15() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("edit_profile -id=1 -bloodType=A");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand missing value for blood type
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid16() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("edit_profile -id=1 -bloodType=");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand missing value for blood type
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid17() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("edit_profile -id=1 -bloodType");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand invalid gender
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid18() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("edit_profile -id=1 -gender=q");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand missing value for gender
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid19() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("edit_profile -id=1 -gender=");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand missing value for gender
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid20() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("edit_profile -id=1 -gender");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand invalid value for donor
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid21() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("edit_profile -id=1 -isDonor=asdf");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand missing value for donor
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid22() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("edit_profile -id=1 -isDonor=");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand missing value for donor
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid23() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("edit_profile -id=1 -isDonor ");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand invalid value for height
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid24() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("edit_profile -id=1 -height=asdf ");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand missing value for height
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid25() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("edit_profile -id=1 -height= ");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand missing valule for height
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid26() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("edit_profile -id=1 -height ");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand invalid value for height
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid27() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("edit_profile -id=1 -weight=asdf ");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand missing value for weight
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid28() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("edit_profile -id=1 -weight= ");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand missing value for the weight
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid29() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("edit_profile -id=1 -weight ");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand invalid organ to be added
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid30() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("edit_profile -id=1 -addDonorOrgan=asdf ");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand missing value for the argument
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid31() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("edit_profile -id=1 -addDonorOrgan= ");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand missing value for the organ
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid32() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("edit_profile -id=1 -addDonorOrgan ");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand missing value for the organ
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid33() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("edit_profile -id=1 -removeDonorOrgan=asdf ");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand Missing value for the organ
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid34() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("edit_profile -id=1 -removeDonorOrgan=");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the mis-use of arguments of the command for edit profile command can be correctly identified.
     *
     * @throws InvalidCommand missing value for the organ
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditProfileInvalid35() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("edit_profile -id=1 -removeDonorOrgan");
        CommandController.checkArguments(CommandEnum.EDIT_PROFILE, getArgs(parsedInput));
    }


    /**
     * Test if the proper use of the delete command can be identified.
     */
    @Test
    public void checkArgumentsDeleteProfileValid() {
        try {
            String[] parsedInput = Commandline.translateCommandline("delete_profile -username xd");
            CommandController.checkArguments(CommandEnum.DELETE_PROFILE, getArgs(parsedInput));
        } catch (InvalidCommand e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test if the improper use of the command can be identified.
     *
     * @throws InvalidCommand extra argument
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsDeleteProfileInvalid() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("delete_profile -id=1 asdf");
        CommandController.checkArguments(CommandEnum.DELETE_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the improper use of the command can be identified.
     *
     * @throws InvalidCommand missing argument
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsDeleteProfileInvalid2() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("delete_profile");
        CommandController.checkArguments(CommandEnum.DELETE_PROFILE, getArgs(parsedInput));
    }


    /**
     * Test if the improper use of the command can be identified.
     *
     * @throws InvalidCommand wrong argument
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsDeleteProfileInvalid3() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("delete_profile -id=1 -isDonor=true");
        CommandController.checkArguments(CommandEnum.DELETE_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the improper use of the command can be identified.
     *
     * @throws InvalidCommand missing value for argument
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsDeleteProfileInvalid4() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("delete_profile -id=");
        CommandController.checkArguments(CommandEnum.DELETE_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the improper use of the command can be identified.
     *
     * @throws InvalidCommand invalid value for argument
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsDeleteProfileInvalid5() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("delete_profile -id=asdf");
        CommandController.checkArguments(CommandEnum.DELETE_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test if the improper use of the command can be identified.
     *
     * @throws InvalidCommand missing value for argument
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsDeleteProfileInvalid6() throws InvalidCommand {
        String[] parsedInput = Commandline.translateCommandline("delete_profile -id ");
        CommandController.checkArguments(CommandEnum.DELETE_PROFILE, getArgs(parsedInput));
    }

    /**
     * Test that the commands can be successfully retrieved.
     */
    @Test
    public void retrieveCommandTest() {
        try {
            assertEquals(CommandController.retrieveCommand("quit"), CommandEnum.QUIT);
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }


    /**
     * Test that the commands can be successfully retrieved.
     */
    @Test
    public void retrieveCommandTest2() {
        try {
            assertEquals(CommandController.retrieveCommand("create_profile"), CommandEnum.CREATE_PROFILE);
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    /**
     * Test that the commands can be successfully retrieved.
     */
    @Test
    public void retrieveCommandTest3() {
        try {
            assertEquals(CommandController.retrieveCommand("display_profile"), CommandEnum.DISPLAY_PROFILE);
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    /**
     * Test that the commands can be successfully retrieved.
     */
    @Test
    public void retrieveCommandTest6() {
        try {
            assertEquals(CommandController.retrieveCommand("help"), CommandEnum.HELP);
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    /**
     * Test that the commands can be successfully retrieved.
     */
    @Test
    public void retrieveCommandTest7() {
        try {
            assertEquals(CommandController.retrieveCommand("edit_profile"), CommandEnum.EDIT_PROFILE);
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    /**
     * Test that the commands can be successfully retrieved.
     */
    @Test
    public void retrieveCommandTest9() {
        try {
            assertEquals(CommandController.retrieveCommand("delete_profile"), CommandEnum.DELETE_PROFILE);
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    /**
     * Test that it can be identified that the entered value is not a command.
     *
     * @throws DoesNotExist Command does not exist.
     */
    @Test(expected = DoesNotExist.class)
    public void retrieveCommandInvalidTest() throws DoesNotExist {
        CommandController.retrieveCommand("no_such_command");
    }

    /**
     * Test that the correct command input can be retrieved from a command enumerator.
     */
    @Test
    public void retrieveCommandInputTest() {
        try {
            assertEquals("quit", CommandController.retrieveCommandInput(CommandEnum.QUIT));
        } catch (DoesNotExist e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test that the correct command input can be retrieved from a command enumerator.
     */
    @Test
    public void retrieveCommandInputTest2() {
        try {
            assertEquals("create_profile", CommandController.retrieveCommandInput(CommandEnum.CREATE_PROFILE));
        } catch (DoesNotExist e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test that the correct command input can be retrieved from a command enumerator.
     */
    @Test
    public void retrieveCommandInputTest3() {
        try {
            assertEquals("display_profile", CommandController.retrieveCommandInput(CommandEnum.DISPLAY_PROFILE));
        } catch (DoesNotExist e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test that the correct command input can be retrieved from a command enumerator.
     */
    @Test
    public void retrieveCommandInputTest6() {
        try {
            assertEquals("help", CommandController.retrieveCommandInput(CommandEnum.HELP));
        } catch (DoesNotExist e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test that the correct command input can be retrieved from a command enumerator.
     */
    @Test
    public void retrieveCommandInputTest7() {
        try {
            assertEquals("edit_profile", CommandController.retrieveCommandInput(CommandEnum.EDIT_PROFILE));
        } catch (DoesNotExist e) {
            e.printStackTrace();
            fail();
        }
    }


    /**
     * Test that the correct command input can be retrieved from a command enumerator.
     */
    @Test
    public void retrieveCommandInputTest9() {
        try {
            assertEquals("delete_profile", CommandController.retrieveCommandInput(CommandEnum.DELETE_PROFILE));
        } catch (DoesNotExist e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test if all the defined commands enumerators are included in the arguments HashMap.
     */
    @Test
    public void createArgumentsHashMapTest() {

        //Inherit the private methods of the abstract class CommandController so the method can be invoked
        class CommandControllerChild extends CommandController {
        }

        try {
            Method method = CommandController.class.getDeclaredMethod("createArgumentsHashMap");
            method.setAccessible(true);

            HashMap<CommandEnum, Options> retrieved =
                    (HashMap<CommandEnum, Options>) method.invoke(new CommandControllerChild());

            Set<CommandEnum> retrievedKeys = retrieved.keySet();

            for (CommandEnum command : CommandEnum.values()) {
                assertTrue(retrievedKeys.contains(command));
            }

            assertEquals(retrieved.size(), CommandEnum.values().length);

        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test if all the defined command enumerators are included in the command HashMap
     */
    @Test
    public void createCommandsHashMapTest() {
        //Inherit the private methods of the abstract class CommandController so the method can be invoked
        class CommandControllerChild extends CommandController {
        }

        try {
            Method method = CommandController.class.getDeclaredMethod("createCommandsHashMap");
            method.setAccessible(true);

            HashMap<CommandEnum, Options> retrieved =
                    (HashMap<CommandEnum, Options>) method.invoke(new CommandControllerChild());

            Set<CommandEnum> retrievedKeys = retrieved.keySet();

            for (CommandEnum command : CommandEnum.values()) {
                assertTrue(retrievedKeys.contains(command.toString().toLowerCase()));
            }

            assertEquals(retrieved.size(), CommandEnum.values().length);

        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test if a valid donor profile can be created from an array of arguments.
     */
    @Test
    public void createDonorProfileTest() {
        //Inherit the private methods of the abstract class CommandController so the method can be invoked
        class CommandControllerChild extends CommandController {
        }

        String[] args = Commandline.translateCommandline(
                "-firstName=John -lastName=Smith -username xd -dateOfBirth=1997-05-12 -gender=Male -height=1.2 -weight=34.5 -bloodType=O- -currentAddress=Street -region=Canterbury -isDonor=true -donorOrgans=LIVER,KIDNEY");

        try {
            Method method = CommandController.class.getDeclaredMethod("createProfile", String[].class);
            method.setAccessible(true);

            Profile profile = (Profile) method.invoke(new CommandControllerChild(), (Object) args);

            assertEquals("xd", profile.getUsername());
            assertEquals("John", profile.getFirstName());
            assertEquals("Smith", profile.getLastName());

            LocalDate dob = LocalDate.parse("1997-05-12");
            assertEquals(dob, profile.getDateOfBirth());

            assertEquals(GenderEnum.MALE, profile.getGender());
            assertEquals(1.2, profile.getHeight(), 0.00001);
            assertEquals(34.5, profile.getWeight(), 0.00001);
            assertEquals(BloodTypeEnum.O_NEGATIVE, profile.getBloodType());
            assertEquals("Street", profile.getAddress());
            assertEquals(profile.getRegion(), RegionEnum.CANTERBURY);
            assertTrue(profile.isDonor());
            assertEquals(LIVER, profile.getDonorOrgans().get(0));
            assertEquals(OrganEnum.KIDNEY, profile.getDonorOrgans().get(1));

        } catch (NoSuchMethodException | IllegalAccessException e) {
            e.printStackTrace();
            fail();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }

    /**
     * Test if a valid receiver profile can be created from an array of arguments.
     */
    @Test
    public void createReceiverProfileTest() {
        //Inherit the private methods of the abstract class CommandController so the method can be invoked
        class CommandControllerChild extends CommandController {
        }

        String[] args = Commandline.translateCommandline(
                "-firstName=John -lastName=Smith -username=john_smith -dateOfBirth=1997-05-12 -gender=Male -height=1.2 -weight=34.5 -bloodType=O- -currentAddress=Street -region=Canterbury -isReceiver=true -receiverOrgans=LIVER,KIDNEY");

        try {
            Method method = CommandController.class.getDeclaredMethod("createProfile", String[].class);
            method.setAccessible(true);

            Profile profile = (Profile) method.invoke(new CommandControllerChild(), (Object) args);

            assertEquals("john_smith", profile.getUsername());
            assertEquals("John", profile.getFirstName());
            assertEquals("Smith", profile.getLastName());

            LocalDate dob = LocalDate.parse("1997-05-12");
            assertEquals(dob, profile.getDateOfBirth());

            assertEquals(GenderEnum.MALE, profile.getGender());
            assertEquals(1.2, profile.getHeight(), 0.00001);
            assertEquals(34.5, profile.getWeight(), 0.00001);
            assertEquals(BloodTypeEnum.O_NEGATIVE, profile.getBloodType());
            assertEquals("Street", profile.getAddress());
            assertEquals(RegionEnum.CANTERBURY, profile.getRegion());
            assertTrue(profile.isReceiver());
        } catch (NoSuchMethodException | IllegalAccessException e) {
            e.printStackTrace();
            fail();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }


    /**
     * Test if we can identify the incorrect usage of the create_clinician command. No arguments.
     *
     * @throws InvalidCommand thrown when inputting a command that doesn't exist.
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsCreateClinicianInvalidMissingAllArgs() throws InvalidCommand {
        String[] parsedInputs = Commandline.translateCommandline("create_clinician");
        CommandController.checkArguments(CommandEnum.CREATE_CLINICIAN, getArgs(parsedInputs));
    }

    /**
     * Test if we can identify the incorrect usage of the create_clinician command. Missing the username.
     *
     * @throws InvalidCommand thrown when inputting a command that doesn't exist.
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsCreateClinicianInvalidMissingUsername() throws InvalidCommand {
        String[] parsedInputs = Commandline.translateCommandline(
                "create_clinician -organisation=Save -id=abc123 -firstName=John -dateOfBirth=1997-09-20 -region=Canterbury");
        CommandController.checkArguments(CommandEnum.CREATE_CLINICIAN, getArgs(parsedInputs));
    }

    /**
     * Test if we can identify the incorrect usage of the create_clinician command. Missing the clinician staff ID.
     *
     * @throws InvalidCommand thrown when inputting a command that doesn't exist.
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsCreateClinicianInvalidMissingStaffId() throws InvalidCommand {
        String[] parsedInputs = Commandline.translateCommandline(
                "create_clinician -username=clinician0 -organisation=save -firstName=John -dateOfBirth=1997-09-20 -region=Canterbury");
        CommandController.checkArguments(CommandEnum.CREATE_CLINICIAN, getArgs(parsedInputs));
    }

    /**
     * Test if we can identify the incorrect usage of the create_clinician command. Missing the first name.
     *
     * @throws InvalidCommand thrown when inputting a command that doesn't exist.
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsCreateClinicianInvalidMissingFirstname() throws InvalidCommand {
        String[] parsedInputs = Commandline.translateCommandline(
                "create_clinician -username=clinician0 -id=abc123 -organisation=save -dateOfBirth=1997-09-20 -region=Canterbury");
        CommandController.checkArguments(CommandEnum.CREATE_CLINICIAN, getArgs(parsedInputs));
    }

    /**
     * Test if we can identify the incorrect usage of the create_clinician command. Missing the date of birth.
     *
     * @throws InvalidCommand thrown when inputting a command that doesn't exist.
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsCreateClinicianInvalidMissingDateOfBirth() throws InvalidCommand {
        String[] parsedInputs = Commandline.translateCommandline(
                "create_clinician -username=clinician0 -organisation=save -id=abc123 -firstName=John-region=Canterbury");
        CommandController.checkArguments(CommandEnum.CREATE_CLINICIAN, getArgs(parsedInputs));
    }

    /**
     * Test if we can identify the incorrect usage of the create_clinician command. Missing the region.
     *
     * @throws InvalidCommand thrown when inputting a command that doesn't exist.
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsCreateClinicianInvalidMissingRegion() throws InvalidCommand {
        String[] parsedInputs = Commandline.translateCommandline(
                "create_clinician -username=clinician0 -organisation=save -id=abc123 -firstName=John -dateOfBirth=1997-09-20");
        CommandController.checkArguments(CommandEnum.CREATE_CLINICIAN, getArgs(parsedInputs));
    }

    /**
     * Test if a valid clinician can be created from an array of arguments.
     */
    @Test
    public void createClinicianTest() {
        //Inherit the private methods of the abstract class CommandController so the method can be invoked
        class CommandControllerChild extends CommandController {
        }

        String[] args = Commandline.translateCommandline(
                "-username=clinician0 -id=abc123 -firstName=John -middleName=Johny -lastName=Smith -currentAddress=\"123 Fake Street\" -dateOfBirth=1997-09-20 -gender=MALE -region=Canterbury");

        try {
            Method method = CommandController.class.getDeclaredMethod("createClinician", String[].class);
            method.setAccessible(true);

            Clinician clinician = (Clinician) method.invoke(new CommandControllerChild(), (Object) args);

            assertEquals("clinician0", clinician.getUsername());
            assertEquals("abc123", clinician.getStaffId());
            assertEquals("John", clinician.getFirstName());
            assertEquals("Johny", clinician.getMiddleName());
            assertEquals("Smith", clinician.getLastName());

            LocalDate dob = LocalDate.parse("1997-09-20");
            assertEquals(dob, clinician.getDateOfBirth());


            assertEquals(GenderEnum.MALE, clinician.getGender());
            assertEquals("123 Fake Street", clinician.getAddress());
            assertEquals(clinician.getRegion(), RegionEnum.CANTERBURY);

        } catch (NoSuchMethodException | IllegalAccessException e) {
            e.printStackTrace();
            fail();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }


    /**
     * Test if the incorrect usage of the command can be identified. Missing the required username field used for
     * searching for the clinician.
     *
     * @throws InvalidCommand thrown when inputting a command that doesn't exist.
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsEditClinicianInvalidMissingUsername() throws InvalidCommand {
        String[] parsedInputs = Commandline.translateCommandline(
                "edit_clinician -id=abc123 -firstName=John -middleName=Johny -lastName=Smith -currentAddress=\"123 Fake Street\" -dateOfBirth=1997-09-20 -gender=MALE -region=Canterbury");
        CommandController.checkArguments(CommandEnum.EDIT_CLINICIAN, getArgs(parsedInputs));
    }

    /**
     * Check if we can correctly identify the arguments passed to the delete clinician argument.
     */
    @Test
    public void checkArgumentsDeleteClinician() {
        String[] parsedInputs = Commandline.translateCommandline("delete_clinician -username=clinician0");
        try {
            CommandController.checkArguments(CommandEnum.DELETE_CLINICIAN, getArgs(parsedInputs));
        } catch (InvalidCommand invalidCommand) {
            invalidCommand.printStackTrace();
            fail();
        }
    }

    /**
     * Check if we can correctly identify the incorrect use of the  delete clinician command. Can not have the empty
     * string as the username argument.
     *
     * @throws InvalidCommand thrown when inputting a command that doesn't exist.
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsDeleteClinicianInvalidEmptyString() throws InvalidCommand {
        String[] parsedInputs = Commandline.translateCommandline("delete_clinician -username=\"\"");
        CommandController.checkArguments(CommandEnum.DELETE_CLINICIAN, getArgs(parsedInputs));
    }

    /**
     * Check if we can correctly identify the incorrect use of the  delete clinician command. Must have the username
     * argument.
     *
     * @throws InvalidCommand thrown when inputting a command that doesn't exist.
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsDeleteClinicianInvalidMissingUsernameArg() throws InvalidCommand {
        String[] parsedInputs = Commandline.translateCommandline("delete_clinician");
        CommandController.checkArguments(CommandEnum.DELETE_CLINICIAN, getArgs(parsedInputs));
    }

    /**
     * Check if we can correctly identify the incorrect use of the  delete clinician command. Has an extra unrecognized
     * argument
     *
     * @throws InvalidCommand thrown when inputting a command that doesn't exist.
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsDeleteClinicianInvalidExtraArg() throws InvalidCommand {
        String[] parsedInputs = Commandline.translateCommandline("delete_clinician -username=clinician0 -extra=blah");
        CommandController.checkArguments(CommandEnum.DELETE_CLINICIAN, getArgs(parsedInputs));
    }

    /**
     * The SQL command to starting a the SQL shell shouldn't have any arguments.
     */
    @Test
    public void checkArgumentsSqlPass() {
        String[] parsedInputs = "sql".split(" ");
        try {
            CommandController.checkArguments(CommandEnum.SQL, getArgs(parsedInputs));
        } catch (InvalidCommand invalidCommand) {
            invalidCommand.printStackTrace();
            fail();
        }
    }

    /**
     * The SQL command to starting a the SQL shell shouldn't have any arguments.
     *
     * @throws InvalidCommand The command is not expecting an argument.
     */
    @Test(expected = InvalidCommand.class)
    public void checkArgumentsSqlFail() throws InvalidCommand {
        String[] parsedInputs = Commandline.translateCommandline("sql -fail");
        CommandController.checkArguments(CommandEnum.SQL, getArgs(parsedInputs));
    }


    /**
     * Test if a clinician can be deleted using the command.
     */
    @Test
    public void deleteClinicianTest() {
        //Inherit the private methods of the abstract class CommandController so the method can be invoked
        class CommandControllerChild extends CommandController {
        }

        Clinician clinician = new Clinician("Jeff", "Jeffy", "Jefferson", "clinician0", "112a Not Real Street",
                RegionEnum.WELLINGTION, FEMALE, new Date(), LocalDate.parse("1990-02-02"), new Hospital("New Organisation", RegionEnum.CANTERBURY, "New Address", 0, 0),
                "staff0");
        ClinicianController.addClinician(clinician.getUsername(), clinician);

        String[] args = Commandline.translateCommandline("-username=clinician0");

        try {

            // Check we actually have a single clinician that we can remove
            assertEquals(1, ClinicianController.getClinicians().size());

            Method method = CommandController.class.getDeclaredMethod("deleteClinician", String[].class);
            method.setAccessible(true);

            method.invoke(new CommandControllerChild(), (Object) args);

            // Check that we actually have removed the clinician
            assertEquals(0, ClinicianController.getClinicians().size());

        } catch (NoSuchMethodException | IllegalAccessException e) {
            e.printStackTrace();
            fail();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
//            fail(e.getMessage());
        }
    }

}

