package seng302;

import org.junit.Before;
import org.junit.Test;
import seng302.CustomException.AlreadyExists;
import seng302.CustomException.DoesNotExist;
import seng302.Enum.OrganEnum;
import seng302.Model.Profile;
import seng302.ModelController.ProfileController;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ProfileControllerTest {

    @Test
    public void searchProfileByIdValid() {
        Profile profile = new Profile("fname", LocalDate.now(), new Date(), "user1");
        Profile profile2 = new Profile("fname", LocalDate.now(), new Date(), "user2");
        Profile profile3 = new Profile("fname", LocalDate.now(), new Date(), "user3");

        ArrayList<Profile> profiles = new ArrayList<>(Arrays.asList(profile, profile2, profile3));

        Profile retrievedProfile;
        try {
            retrievedProfile = ProfileController.searchProfileByUsername(profiles, "user2");

            assertTrue(retrievedProfile.equals(profile2));

        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test(expected = DoesNotExist.class)
    public void searchProfileByIdInvalid() throws DoesNotExist {
        Profile profile = new Profile("fname", LocalDate.now(), new Date(), "user1");
        Profile profile2 = new Profile("fname", LocalDate.now(), new Date(), "user2");
        Profile profile3 = new Profile("fname", LocalDate.now(), new Date(), "user3");

        ArrayList<Profile> profiles = new ArrayList<>(Arrays.asList(profile, profile2, profile3));
        ProfileController.searchProfileByUsername(profiles, "100");

    }

    @Test
    public void searchProfileByFirstName() {
        Profile profile = new Profile("A", LocalDate.now(), new Date(), "user1");
        Profile profile2 = new Profile("B", LocalDate.now(), new Date(), "user2");
        Profile profile3 = new Profile("C", LocalDate.now(), new Date(), "user3");

        ArrayList<Profile> profiles = new ArrayList<>(Arrays.asList(profile, profile2, profile3));
        ArrayList<Profile> solution = ProfileController.searchProfileByFirstName(profiles, "B");

        assertEquals(solution.size(), 1);
        assertTrue(solution.get(0).equals(profile2));
    }

    @Test
    public void searchProfileByFirstName2() {
        Profile profile = new Profile("A", LocalDate.now(), new Date(), "user1");
        Profile profile2 = new Profile("B", LocalDate.now(), new Date(), "user2");
        Profile profile3 = new Profile("C", LocalDate.now(), new Date(), "user3");

        ArrayList<Profile> profiles = new ArrayList<>(Arrays.asList(profile, profile2, profile3));
        ArrayList<Profile> solution = ProfileController.searchProfileByFirstName(profiles, "b");

        assertEquals(solution.size(), 1);
        assertTrue(solution.get(0).equals(profile2));
    }

    @Test
    public void searchProfileByFirstName3() {
        Profile profile = new Profile("A", LocalDate.now(), new Date(), "user1");
        Profile profile2 = new Profile("B", LocalDate.now(), new Date(), "user2");
        Profile profile3 = new Profile("C", LocalDate.now(), new Date(), "user3");

        ArrayList<Profile> profiles = new ArrayList<>(Arrays.asList(profile, profile2, profile3));
        ArrayList<Profile> solution = ProfileController.searchProfileByFirstName(profiles, "D");

        assertEquals(solution.size(), 0);
    }

    @Test
    public void searchProfileByLastName() {
        Profile profile = new Profile("A", LocalDate.now(), new Date(), "user1");
        Profile profile2 = new Profile("B", LocalDate.now(), new Date(), "user2");
        Profile profile3 = new Profile("C", LocalDate.now(), new Date(), "user3");
        profile.setLastName("Q");
        profile2.setLastName("W");
        profile3.setLastName("E");

        ArrayList<Profile> profiles = new ArrayList<>(Arrays.asList(profile, profile2, profile3));
        ArrayList<Profile> solution = ProfileController.searchProfileByLastName(profiles, "W");

        assertEquals(solution.size(), 1);
        assertTrue(solution.get(0).equals(profile2));
    }

    @Test
    public void searchProfileByLastName2() {
        Profile profile = new Profile("A", LocalDate.now(), new Date(), "user1");
        Profile profile2 = new Profile("B", LocalDate.now(), new Date(), "user2");
        Profile profile3 = new Profile("C", LocalDate.now(), new Date(), "user3");
        profile.setLastName("Q");
        profile2.setLastName("W");
        profile3.setLastName("E");

        ArrayList<Profile> profiles = new ArrayList<>(Arrays.asList(profile, profile2, profile3));
        ArrayList<Profile> solution = ProfileController.searchProfileByLastName(profiles, "w");

        assertEquals(solution.size(), 1);
        assertTrue(solution.get(0).equals(profile2));
    }

    @Test
    public void searchProfileByLastName3() {
        Profile profile = new Profile("A", LocalDate.now(), new Date(), "user1");
        Profile profile2 = new Profile("B", LocalDate.now(), new Date(), "user2");
        Profile profile3 = new Profile("C", LocalDate.now(), new Date(), "user3");
        profile.setLastName("Q");
        profile2.setLastName("W");
        profile3.setLastName("E");

        ArrayList<Profile> profiles = new ArrayList<>(Arrays.asList(profile, profile2, profile3));
        ArrayList<Profile> solution = ProfileController.searchProfileByLastName(profiles, "J");

        assertEquals(solution.size(), 0);
    }

    @Test
    public void searchProfileByDonorStatus() throws AlreadyExists {
        Profile profile = new Profile("A", LocalDate.now(), new Date(), "user1");
        Profile profile2 = new Profile("B", LocalDate.now(), new Date(), "user2");
        Profile profile3 = new Profile("C", LocalDate.now(), new Date(), "user3");
        profile.addDonorOrgan(OrganEnum.HEART);
        profile2.setIsDonor(false);
        profile3.addDonorOrgan(OrganEnum.HEART);

        ArrayList<Profile> profiles = new ArrayList<>(Arrays.asList(profile, profile2, profile3));
        ArrayList<Profile> solution = ProfileController.searchProfileByDonorStatus(profiles, false);

        assertEquals(solution.size(), 1);
        assertTrue(solution.get(0).equals(profile2));
    }

    @Test
    public void searchProfileByDonorStatus2() throws AlreadyExists {
        Profile profile = new Profile("A", LocalDate.now(), new Date(), "user1");
        Profile profile2 = new Profile("B", LocalDate.now(), new Date(), "user2");
        Profile profile3 = new Profile("C", LocalDate.now(), new Date(), "user3");
        profile.addDonorOrgan(OrganEnum.LIVER);
        profile2.addDonorOrgan(OrganEnum.LIVER);
        profile3.addDonorOrgan(OrganEnum.LIVER);

        ArrayList<Profile> profiles = new ArrayList<>(Arrays.asList(profile, profile2, profile3));
        ArrayList<Profile> solution = ProfileController.searchProfileByDonorStatus(profiles, false);

        assertEquals(0, solution.size());
    }

    @Test
    public void deleteProfileValid() {
        Profile profile = new Profile("A", LocalDate.now(), new Date(), "user1");
        Profile profile2 = new Profile("B", LocalDate.now(), new Date(), "user2");

        ArrayList<Profile> profiles = new ArrayList<>(Arrays.asList(profile, profile2));
        ProfileController.setProfiles(profiles);

        try {
            ProfileController.deleteProfile("user2");
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }


        ArrayList<Profile> profilesAfterDeletion = ProfileController.getProfiles();
        assertEquals(1, profilesAfterDeletion.size());
        assertTrue(profilesAfterDeletion.get(0).equals(profile));
    }

    @Test(expected = DoesNotExist.class)
    public void deleteProfileInvalid() throws DoesNotExist {
        Profile profile = new Profile("A", LocalDate.now(), new Date(), "user1");
        Profile profile2 = new Profile("B", LocalDate.now(), new Date(), "user2");

        ArrayList<Profile> profiles = new ArrayList<>(Arrays.asList(profile, profile2));
        ProfileController.setProfiles(profiles);

        ProfileController.deleteProfile("user3");

    }

    @Before
    public void setUp() {
        ProfileController.setProfiles(new ArrayList());
    }

}
