package seng302;

import org.junit.Before;
import org.junit.Test;
import seng302.Utilities.CSV.CsvParser;

import java.time.DateTimeException;
import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class CsvParserTest {

    private CsvParser parser;

    @Before
    public void setUp() {
        parser = new CsvParser(null);
    }

    /**
     * Test to see if the method can accurately parse dates with the format MM/DD/YYYY.
     */
    @Test
    public void dateFormatTest() {
        String date = "12/10/2017";
        LocalDate result = parser.extractDate(date);
        assertEquals(LocalDate.of(2017, 12, 10), result);
    }

    /**
     * Test to see if we can identify the wrong format.
     */
    @Test(expected = NumberFormatException.class)
    public void wrongFormatTest() {
        String date = "12-10-2017";
        parser.extractDate(date);
    }

    /**
     * Test to see if we can recognize invalid dates
     */
    @Test(expected = DateTimeException.class)
    public void invalidDate() {
        String date = "15/10/2017";
        parser.extractDate(date);
    }
}
