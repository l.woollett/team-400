package seng302;

import org.junit.Test;
import seng302.CustomException.DoesNotExist;
import seng302.ModelController.BloodTypeController;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static seng302.Enum.BloodTypeEnum.*;

public class BloodTypeControllerTest {

    /**
     * Test if it correctly throws an exception if it asks to find a non-sense blood typed.
     *
     * @throws DoesNotExist The specified blood type does not exist.
     */
    @Test(expected = DoesNotExist.class)
    public void invalidBloodTypeTest() throws DoesNotExist {
        assertEquals(O_NEGATIVE, BloodTypeController.getBloodType("non-existent blood type"));
    }

    @Test
    public void oNegTest() {
        try {
            assertEquals(O_NEGATIVE, BloodTypeController.getBloodType("O-"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void oPosTest() {
        try {
            assertEquals(O_POSITIVE, BloodTypeController.getBloodType("O+"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }


    @Test
    public void aNegTest() {
        try {
            assertEquals(A_NEGATIVE, BloodTypeController.getBloodType("A-"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void aPosTest() {
        try {
            assertEquals(A_POSITIVE, BloodTypeController.getBloodType("A+"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void bNegTest() {
        try {
            assertEquals(B_NEGATIVE, BloodTypeController.getBloodType("B-"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void bPosTest() {
        try {
            assertEquals(B_POSITIVE, BloodTypeController.getBloodType("B+"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void abNegTest() {
        try {
            assertEquals(AB_NEGATIVE, BloodTypeController.getBloodType("AB-"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void abPosTest() {
        try {
            assertEquals(AB_POSITIVE, BloodTypeController.getBloodType("AB+"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }
}
