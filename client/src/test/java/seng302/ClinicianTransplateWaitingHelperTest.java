package seng302;

import org.junit.Before;
import org.junit.Test;
import seng302.Enum.OrganEnum;
import seng302.GUI.Clinician.ClinicianTransplantWaitingWrapper;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * This is the test class for ClinicianTransplantWaitingHelper
 */
public class ClinicianTransplateWaitingHelperTest {
    List<ClinicianTransplantWaitingWrapper> ClinicianTransplateWaitingHelperList =
            new ArrayList<>();

    /**
     * Add three records to the list of ClinicianTransplateWaitingHelper to compare with
     */
    @Before
    public void setUp() {
        LocalDate localDateTime =
                LocalDate.of(2015, 11, 26);
        ClinicianTransplantWaitingWrapper originalRecord =
                new ClinicianTransplantWaitingWrapper(OrganEnum.LIVER,
                        localDateTime.toString(), "firstName", "middleName", "lastName", "CHCH", "a", true);
        ClinicianTransplantWaitingWrapper toCompareNotEqualLatter =
                new ClinicianTransplantWaitingWrapper(OrganEnum.LIVER,
                        localDateTime.plusDays(1).toString(), "firstName", "middleName", "lastName", "CHCH", "b", true);
        ClinicianTransplantWaitingWrapper toCompareEqual =
                new ClinicianTransplantWaitingWrapper(OrganEnum.LIVER,
                        localDateTime.toString(), "firstName", "middleName", "lastName", "CHCH", "c", false);
        ClinicianTransplantWaitingWrapper toCompareNotEqualEarilier =
                new ClinicianTransplantWaitingWrapper(OrganEnum.LIVER,
                        localDateTime.minusDays(1).toString(), "firstName", "middleName", "lastName", "CHCH", "d",
                        true);
        ClinicianTransplateWaitingHelperList.add(originalRecord);
        ClinicianTransplateWaitingHelperList.add(toCompareNotEqualLatter);
        ClinicianTransplateWaitingHelperList.add(toCompareEqual);
        ClinicianTransplateWaitingHelperList.add(toCompareNotEqualEarilier);
    }

    /**
     * Compare the registerDate and returns -1, as the given record has a latter date different registerDate than the
     * orginal one
     */
    @Test
    public void compareNotEqualLatterTest() {
        assertEquals(-1,
                ClinicianTransplateWaitingHelperList.get(0).compareTo(ClinicianTransplateWaitingHelperList.get(1)));
    }

    /**
     * Compare the registerDate and returns 0, as the two records have the same registerDate
     */
    @Test
    public void compareEqualTest() {
        assertEquals(0,
                ClinicianTransplateWaitingHelperList.get(0).compareTo(ClinicianTransplateWaitingHelperList.get(2)));
    }

    /**
     * Compare the registerDate and returns 1, as the given record has a earlier date different registerDate than the
     * orginal one
     */
    @Test
    public void compareNotEqualEarilerTest() {
        assertEquals(1,
                ClinicianTransplateWaitingHelperList.get(0).compareTo(ClinicianTransplateWaitingHelperList.get(3)));
    }

}