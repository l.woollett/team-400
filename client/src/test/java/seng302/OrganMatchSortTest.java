package seng302;

import org.junit.Test;
import seng302.Model.OrganMatchQueryResult;

import static junit.framework.TestCase.assertEquals;

public class OrganMatchSortTest {
    /**
     * Tests if two of the same organs return 0 -- Equal
     */
    @Test
    public void testSameResult() {
        OrganMatchQueryResult organ1 = new OrganMatchQueryResult("organ1", "Doesnt Matter"
                , -35.735, 174.303, -41.2869, 173.272, 1);

        OrganMatchQueryResult organ2 = new OrganMatchQueryResult("organ1", "Doesnt Matter"
                , -35.735, 174.303, -41.2869, 173.272, 1);

        assertEquals(0, organ1.compareTo(organ2));
    }

    /**
     * Organ 1 has a datediff smaller than organ2, so should go first (-1) -- Before
     */
    @Test
    public void testDifferentDateDiffs() {
        OrganMatchQueryResult organ1 = new OrganMatchQueryResult("organ1", "Doesnt Matter"
                , -35.735, 174.303, -41.2869, 173.272, 0);

        OrganMatchQueryResult organ2 = new OrganMatchQueryResult("organ1", "Doesnt Matter"
                , -35.735, 174.303, -41.2869, 173.272, 1);

        assertEquals(1, organ1.compareTo(organ2));
    }

    /**
     * Organ 1 has a datediff larger than organ2, so should go last (1) -- After
     */
    @Test
    public void testDifferentDateDiffsAfter() {
        OrganMatchQueryResult organ1 = new OrganMatchQueryResult("organ1", "Doesnt Matter"
                , -35.735, 174.303, -41.2869, 173.272, 2);

        OrganMatchQueryResult organ2 = new OrganMatchQueryResult("organ1", "Doesnt Matter"
                , -35.735, 174.303, -41.2869, 173.272, 1);

        assertEquals(-1, organ1.compareTo(organ2));
    }

    /**
     * Organ 1 has a closer lat and long than organ2, so should go First(1)
     */
    @Test
    public void testCloserToHospital() {
        OrganMatchQueryResult organ1 = new OrganMatchQueryResult("organ1", "Doesnt Matter"
                , -35.735, 174.303, -35.735, 174.303, 1);

        OrganMatchQueryResult organ2 = new OrganMatchQueryResult("organ1", "Doesnt Matter"
                , -35.735, 174.303, -41.2869, 173.272, 1);
        assertEquals(-1, organ1.compareTo(organ2));
    }

    /**
     * Organ 1 has a further lat and long than organ2, so should go Last(-1)
     */
    @Test
    public void testFurtherFromHospital() {
        OrganMatchQueryResult organ1 = new OrganMatchQueryResult("organ1", "Doesnt Matter"
                , -35.735, 174.303, -35.735, 173.272, 1);

        OrganMatchQueryResult organ2 = new OrganMatchQueryResult("organ1", "Doesnt Matter"
                , -35.735, 174.303, -35.735, 174.303, 1);

        assertEquals(1, organ1.compareTo(organ2));
    }
}
