package seng302;

import org.junit.Test;
import seng302.ModelController.AdminController;

import java.util.ArrayList;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AdminSceneControllerTest {

    /**
     * Can we detect a default Clinician.
     */
    @Test
    public void hasDefaultAdminTest() {
        AdminController.setAdmins(new ArrayList<>()); // Ensure there are no admins
        AdminController.addDefaultAdmin();
        assertTrue(AdminController.hasDefaultAdmin());
    }

    /**
     * Is the testing for default admin giving false positives.
     */
    @Test
    public void hasDefaultAdminTestFailure() {
        AdminController.setAdmins(new ArrayList<>());
        assertFalse(AdminController.hasDefaultAdmin());
    }

    /**
     * Can we add a default admin successfully.
     */
    @Test
    public void addDefaultAdminTest() {
        AdminController.setAdmins(new ArrayList<>());
        assertFalse(AdminController.hasDefaultAdmin());
        AdminController.addDefaultAdmin();
        assertTrue(AdminController.hasDefaultAdmin());
    }
}
