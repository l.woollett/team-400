package seng302;

import org.junit.Test;
import seng302.Model.Disease;

import java.time.LocalDate;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DiseaseTest {

    @Test
    public void equalsBothChronicTestPass() {
        Disease obj1 = new Disease("Test", true, LocalDate.now());
        Disease obj2 = new Disease("Test", true, LocalDate.now());
        assertTrue(obj1.equals(obj2));
    }

    @Test
    public void equalsBothNotChronicTestPass() {
        Disease obj1 = new Disease("Test", false, LocalDate.now());
        Disease obj2 = new Disease("Test", false, LocalDate.now());
        assertTrue(obj1.equals(obj2));
    }

    @Test
    public void equalsOneNotChronicTestFail() {
        Disease obj1 = new Disease("Test", true, LocalDate.now());
        Disease obj2 = new Disease("Test", false, LocalDate.now());
        assertFalse(obj1.equals(obj2));
    }

    @Test
    public void equalsDifferentNamesTestFail() {
        Disease obj1 = new Disease("Test", true, LocalDate.now());
        Disease obj2 = new Disease("Test2", true, LocalDate.now());
        assertFalse(obj1.equals(obj2));
    }

    @Test
    public void equalsDifferentEverythingTestFail() {
        Disease obj1 = new Disease("Test", true, LocalDate.now());
        Disease obj2 = new Disease("Test2", false, LocalDate.now());
        assertFalse(obj1.equals(obj2));
    }

    @Test
    public void equalsSameObjectTestFail() {
        Disease obj1 = new Disease("Test", true, LocalDate.now());
        Disease obj2 = new Disease("Test", true, LocalDate.now());
        assertFalse(obj1 == obj2);
    }

    @Test
    public void equalsSameObjectTestPass() {
        Disease obj1 = new Disease("Test", true, LocalDate.now());
        Disease obj2 = obj1;
        assertTrue(obj1.equals(obj2));
    }

}
