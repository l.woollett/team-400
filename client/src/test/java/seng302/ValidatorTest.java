package seng302;


import org.junit.Test;
import seng302.Enum.OrganEnum;
import seng302.Model.Disease;
import seng302.Model.Procedure;
import seng302.Model.Profile;
import seng302.Utilities.Validator;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.apache.commons.lang.StringUtils.repeat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static seng302.Utilities.Validator.*;

public class ValidatorTest {


    /**
     * See if we can accurately validate a change in th date of birth for a user that doesn't have any
     * procedures.
     */
    @Test
    public void isValidChangeOfDateOfBirthTest() {
        Profile profile = new Profile("Test Profile", LocalDate.of(1990, 1, 1), new Date(), "testProfile");
        LocalDate newDateOfBirth = LocalDate.of(2000, 10, 10);
        boolean result = Validator.isValidChangeOfDateOfBirth(newDateOfBirth, profile);
        assertTrue(result);
    }

    /**
     * See if we can accurately validate a date of birth that meets all the requirements. The profile has
     * a current disease.
     */
    @Test
    public void isValidChangeOfDateOfBirthTestWithCurrentDiseasePass() {
        Profile profile = new Profile("Test Profile", LocalDate.of(1990, 1, 1), new Date(), "testProfile");
        List<Disease> diseases = new ArrayList<>();
        diseases.add(new Disease("Bad Code", true, LocalDate.of(1995, 1, 1)));
        profile.setCurrentDiseases(diseases);
        LocalDate newDateOfBirth = LocalDate.of(1994, 10, 10);
        boolean result = Validator.isValidChangeOfDateOfBirth(newDateOfBirth, profile);
        assertTrue(result);
    }

    /**
     * See if we can accurately validate a date of birth that meets all the requirements. The profile has
     * a past disease.
     */
    @Test
    public void isValidChangeOfDateOfBirthTestWithPastDiseasePass() {
        Profile profile = new Profile("Test Profile", LocalDate.of(1990, 1, 1), new Date(), "testProfile");
        List<Disease> diseases = new ArrayList<>();
        diseases.add(new Disease("Bad Code", true, LocalDate.of(1995, 1, 1)));
        profile.setPastDiseases(diseases);
        LocalDate newDateOfBirth = LocalDate.of(1994, 10, 10);
        boolean result = Validator.isValidChangeOfDateOfBirth(newDateOfBirth, profile);
        assertTrue(result);
    }

    /**
     * See if we can accurately validate a date of birth that doesn't meet all the requirements. The profile has
     * a current disease.
     */
    @Test
    public void isValidChangeOfDateOfBirthTestWithCurrentDiseaseFail() {
        Profile profile = new Profile("Test Profile", LocalDate.of(1990, 1, 1), new Date(), "testProfile");
        List<Disease> diseases = new ArrayList<>();
        diseases.add(new Disease("Bad Code", true, LocalDate.of(1995, 1, 1)));
        profile.setCurrentDiseases(diseases);
        LocalDate newDateOfBirth = LocalDate.of(2000, 10, 10);
        boolean result = Validator.isValidChangeOfDateOfBirth(newDateOfBirth, profile);
        assertFalse(result);
    }

    /**
     * See if we can accurately validate a date of birth that doesn't meets all the requirements. The profile has
     * a past disease.
     */
    @Test
    public void isValidChangeOfDateOfBirthTestWithPastDiseaseFail() {
        Profile profile = new Profile("Test Profile", LocalDate.of(1990, 1, 1), new Date(), "testProfile");
        List<Disease> diseases = new ArrayList<>();
        diseases.add(new Disease("Bad Code", true, LocalDate.of(1995, 1, 1)));
        profile.setPastDiseases(diseases);
        LocalDate newDateOfBirth = LocalDate.of(2000, 10, 10);
        boolean result = Validator.isValidChangeOfDateOfBirth(newDateOfBirth, profile);
        assertFalse(result);
    }

    /**
     * See if we can accurately validate a date of birth that meets all the requirements. The profile has
     * a procedure.
     */
    @Test
    public void isValidChangeOfDateOfBirthTestWithProceduresPass() {
        Profile profile = new Profile("Test Profile", LocalDate.of(1990, 1, 1), new Date(), "testProfile");
        List<OrganEnum> affectedOrgans = new ArrayList<>();
        affectedOrgans.add(OrganEnum.HEART);
        profile.addProcedure(new Procedure("Test procedure", "Test", LocalDate.of(1995, 1, 1), affectedOrgans));
        LocalDate newDateOfBirth = LocalDate.of(1994, 10, 10);
        boolean result = Validator.isValidChangeOfDateOfBirth(newDateOfBirth, profile);
        assertTrue(result);
    }


    /**
     * See if we can accurately validate a date of birth that is after a procedure date. The profile has
     * a procedure.
     */
    @Test
    public void isValidChangeOfDateOfBirthTestOneProceduresFail() {
        Profile profile = new Profile("Test Profile", LocalDate.of(1990, 1, 1), new Date(), "testProfile");
        List<OrganEnum> affectedOrgans = new ArrayList<>();
        affectedOrgans.add(OrganEnum.HEART);
        profile.addProcedure(new Procedure("Test procedure", "Test", LocalDate.of(1995, 1, 1), affectedOrgans));
        LocalDate newDateOfBirth = LocalDate.of(2000, 10, 10);
        boolean result = Validator.isValidChangeOfDateOfBirth(newDateOfBirth, profile);
        assertFalse(result);
    }

    /**
     * See if we can accurately validate a date of birth that is before our minimum date of birth.
     */
    @Test
    public void isValidChangeOfDateOfBirthTestBeforeMinDateOfBirth() {
        Profile profile = new Profile("Test Profile", LocalDate.of(1990, 1, 1), new Date(), "testProfile");
        LocalDate newDateOfBirth = LocalDate.of(1000, 10, 10);
        boolean result = Validator.isValidChangeOfDateOfBirth(newDateOfBirth, profile);
        assertFalse(result);
    }


    /**
     * If the procedure date is after their date of birth and before the current date the procedure date is valid.
     */
    @Test
    public void procedureDateTestValid() {
        LocalDate dateOfBirth = LocalDate.of(2000, 1, 1);
        LocalDate procedureDate = LocalDate.of(2010, 1, 1);
        boolean result = Validator.isValidProcedureDate(procedureDate, dateOfBirth);
        assertTrue(result);
    }

    /**
     * If the procedure date is before the date of birth, it is invalid.
     */
    @Test
    public void procedureDateTestInvalidBeforeDateOfBirth() {
        LocalDate dateOfBirth = LocalDate.of(2000, 1, 1);
        LocalDate procedureDate = LocalDate.of(1990, 1, 1);
        boolean result = Validator.isValidProcedureDate(procedureDate, dateOfBirth);
        assertFalse(result);
    }

    /**
     * Date is valid is the procedure is on the same day as their birthday.
     */
    @Test
    public void procedureDateTestValidSameDay() {
        LocalDate dateOfBirth = LocalDate.of(2000, 1, 1);
        LocalDate procedureDate = LocalDate.of(2000, 1, 1);
        boolean result = Validator.isValidProcedureDate(procedureDate, dateOfBirth);
        assertTrue(result);
    }

    /**
     * Should fail since the procedure test is still before the birthday.
     */
    @Test
    public void procedureDateTestMarginallyBefore() {
        LocalDate dateOfBirth = LocalDate.of(2000, 1, 2);
        LocalDate procedureDate = LocalDate.of(2000, 1, 1);
        boolean result = Validator.isValidProcedureDate(procedureDate, dateOfBirth);
        assertFalse(result);
    }

    /**
     * Should pass since the procedure is still after the birthday.
     */
    @Test
    public void procedureDateTestMarginallyAfter() {
        LocalDate dateOfBirth = LocalDate.of(2000, 1, 2);
        LocalDate procedureDate = LocalDate.of(2000, 1, 3);
        boolean result = Validator.isValidProcedureDate(procedureDate, dateOfBirth);
        assertTrue(result);
    }

    // ------------------ USERNAME ----------------------------------------------

    @Test
    public void isValidUsernameTestPass() {
        assertTrue(isValidUsername("John"));
    }

    @Test
    public void isValidUsernameTestPassNumbers() {
        assertTrue(isValidUsername("clinician0"));
    }

    @Test
    public void isValidUsernameTestPassMaxLen() {
        assertTrue(isValidUsername("JohnJohnJohnJohn"));
    }

    @Test
    public void isValidUsernameTestFailSpace() {
        assertFalse(isValidUsername("John Smith"));
    }

    @Test
    public void isValidUsernameTestFailBadCharacter() {
        assertFalse(isValidUsername("John$"));
    }

    @Test
    public void isValidGenericDateInputPass() {
        assertTrue(isValidGenericDateInput("1997-09-30"));
    }

    @Test
    public void isValidGenericDateInputFail() {
        assertFalse(isValidGenericDateInput("not a date"));
    }

    @Test
    public void isValidUsernameTestFailLength() {
        assertFalse(
                isValidUsername("JohnJohnJohnJohnJohnJohnJohnJohnJohnJohnJohnJohnJohnJohnJohnJohnJohnJohnJohnJohn"));
    }

    @Test
    public void isValidUsernameTestFailEmpty() {
        assertFalse(isValidUsername(""));
    }


    // ------------------ FIRST NAME ----------------------------------------------
    @Test
    public void isValidFirstNameTestPass() {
        assertTrue(isValidFirstName("Kirill"));
    }

    @Test
    public void isValidFirstNameTestPassMaxLen() {
        assertTrue(isValidFirstName("JohnJohnJohnJohnJohn"));
    }

    @Test
    public void isValidFirstNameTestFailLength() {
        assertFalse(isValidFirstName("JohnJohnJohnJohnJohns"));
    }

    @Test
    public void isValidFirstNameTestFailBadCharacter() {
        assertFalse(isValidFirstName("John$"));
    }

    @Test
    public void isValidFirstNameTestPassSpace() {
        assertTrue(isValidFirstName("Kirill Pee"));
    }

    @Test
    public void isValidFirstNameTestPassHyphen() {
        assertTrue(isValidFirstName("Kirill-xD"));
    }

    @Test
    public void isValidFirstNameTestPassApostrophe() {
        assertTrue(isValidFirstName("Kirill'ov"));
    }

    @Test
    public void isValidFirstNameTestFailEmpty() {
        assertFalse(isValidFirstName(""));
    }


    // ------------------ MIDDLE NAME ----------------------------------------------
    @Test
    public void isValidMiddleNameTestPass() {
        assertTrue(isValidMiddleName("Kirill"));
    }

    @Test
    public void isValidMiddleNameTestPassMaxLen() {
        assertTrue(isValidMiddleName("JohnJohnJohnJohnJoh"));
    }

    @Test
    public void isValidMiddleNameTestFailLength() {
        assertFalse(isValidMiddleName("JohnJohnJohnJohnJohnJohnJohnJohnJohnJohn"));
    }

    @Test
    public void isValidMiddleNameTestFailBadCharacter() {
        assertFalse(isValidMiddleName("John$"));
    }

    @Test
    public void isValidMiddleNameTestPassSpace() {
        assertTrue(isValidMiddleName("Kirill Pee"));
    }

    @Test
    public void isValidMiddleNameTestPassHyphen() {
        assertTrue(isValidMiddleName("Kirill-xD"));
    }

    @Test
    public void isValidMiddleNameTestPassApostrophe() {
        assertTrue(isValidMiddleName("Kirill'ov"));
    }

    // ------------------ LAST NAME ----------------------------------------------
    @Test
    public void isValidLastNameTestPass() {
        assertTrue(isValidLastName("Kirill"));
    }

    @Test
    public void isValidLastNameTestPassMaxLen() {
        assertTrue(isValidLastName("JohnJohnJohnJohnJoh"));
    }

    @Test
    public void isValidLastNameTestFailLength() {
        assertFalse(isValidLastName("JohnJohnJohnJohnJohnJohnJohnJohnJohnJohn"));
    }

    @Test
    public void isValidLastNameTestFailBadCharacter() {
        assertFalse(isValidLastName("John$"));
    }

    @Test
    public void isValidLastNameTestPassSpace() {
        assertTrue(isValidLastName("Kirill Pee"));
    }

    @Test
    public void isValidLastNameTestPassHyphen() {
        assertTrue(isValidLastName("Kirill-xD"));
    }

    @Test
    public void isValidLastNameTestPassApostrophe() {
        assertTrue(isValidLastName("Kirill'ov"));
    }

    // ------------------ HEIGHT ----------------------------------------------
    @Test
    public void isValidHeightTestPass() {
        assertTrue(isValidHeight("1.85"));
    }

    @Test
    public void isValidHeightTestFailNotANumber() {
        assertFalse(isValidHeight("a"));
    }

    @Test
    public void isValidHeightTestFailBadCharacter() {
        assertFalse(isValidHeight("1,85"));
    }

    @Test
    public void isValidHeightTestFailWrongFormat1() {
        assertFalse(isValidHeight("185"));
    }

    @Test
    public void isValidHeightTestFormat2() {
        assertFalse(isValidHeight("1.885"));
    }

    @Test
    public void isValidHeightTestFailZero() {
        assertFalse(isValidHeight("0"));
    }

    // ------------------ WEIGHT ----------------------------------------------
    @Test
    public void isValidWeightTestPass() {
        assertTrue(isValidWeight("80.50"));
    }

    @Test
    public void isValidWeightTestPassBoundary499() {
        assertTrue(isValidWeight("499.99"));
    }

    @Test
    public void isValidWeightTestPassBoundaryOne() {
        assertTrue(isValidWeight("1.00"));
    }

    @Test
    public void isValidWeightTestFailNegative() {
        assertFalse(isValidWeight("-12"));
    }

    @Test
    public void isValidWeightTestFailBoundary501() {
        assertFalse(isValidWeight("501"));
    }

    @Test
    public void isValidWeightTestFailBoundaryZero() {
        assertFalse(isValidWeight("0"));
    }

    @Test
    public void isValidWeightTestFailOver500() {
        assertFalse(isValidWeight("552"));
    }

    @Test
    public void isValidWeightTestFailNotANumber() {
        assertFalse(isValidWeight("as"));
    }

    @Test
    public void isValidWeightTestFailBadCharacter() {
        assertFalse(isValidWeight("50-0"));
    }

    @Test
    public void isValidWeightTestFailLength() {
        assertFalse(isValidWeight("504540"));
    }

    // ------------------ ADDRESS ----------------------------------------------
    @Test
    public void isValidAddressTestPass() {
        assertTrue(isValidAddress("112 Clarence street, Riccarton, 8024"));
    }

    @Test
    public void isValidAddressTestPassDigits() {
        assertTrue(isValidAddress("112abc"));
    }

    @Test
    public void isValidAddressTestPassHyphen() {
        assertTrue(isValidAddress("ab-c"));
    }

    @Test
    public void isValidAddressTestPassApostrophe() {
        assertTrue(isValidAddress("g'day"));
    }

    @Test
    public void isValidAddressTestPassSpace() {
        assertTrue(isValidAddress("A Big Space"));
    }

    @Test
    public void isValidAddressTestFailLength() {

        String testAddress = repeat("A", Validator.MAX_ADDRESS_LENGTH + 10);
        assertFalse(isValidAddress(testAddress));
    }

    @Test
    public void isValidAddressTestFailBoundaryLength() {
        String testAddress = repeat("A", Validator.MAX_ADDRESS_LENGTH + 1);
        assertFalse(isValidAddress(testAddress));
    }

    @Test
    public void isValidAddressTestFailEmptyString() {
        assertFalse(isValidAddress(""));
    }

    @Test
    public void isValidAddressTestFailNonAlphabetCharacterNotBetweenTwoAlphabetCharactersOrDigits() {
        assertFalse(isValidAddress("A...A"));
    }

    // ------------------ Date of Birth ----------------------------------------------
    @Test
    public void isValidDateOfBirthTestPass() {
        assertTrue(isValidDateOfBirth(LocalDate.now()));
    }

    @Test
    public void isValidDateOfBirthTestFail() {
        LocalDate localDate = LocalDate.MAX;
        assertFalse(isValidDateOfBirth(localDate));
    }

    // ------------------ Date of Death ----------------------------------------------
    @Test
    public void isValidDateOfDeathTestPass() {
        assertTrue(isValidDateOfDeath(LocalDate.now(), LocalDate.now(), true, null, null, null));
    }

    @Test
    public void isValidDateOfDeathTestFailDoDBeforeDob() {
        LocalDate dob = LocalDate.parse("2000-01-01");
        LocalDate dod = LocalDate.parse("1999-01-01");
        assertFalse(isValidDateOfDeath(dod, dob, true, null, null, null));
    }

    @Test
    public void isValidDeathFailHospitalNotSelected() {
        assertFalse(isValidDateOfDeath(LocalDate.now(), LocalDate.now(), false, null, null, null));
    }

    // ------------------ BLOOD PRESSURE ----------------------------------------------
    @Test
    public void isValidBloodPressurePass() {
        assertTrue(isValidBloodPressure("60/80"));
    }

    @Test
    public void isValidBloodPressurePassBoundaryOneNumberEach() {
        assertTrue(isValidBloodPressure("1/9"));
    }

    @Test
    public void isValidBloodPressurePassBoundaryThreeNumbersEach() {
        assertTrue(isValidBloodPressure("600/999"));
    }

    @Test
    public void isValidBloodPressureFailEmpty() {
        assertTrue(isValidBloodPressure(""));
    }

    @Test
    public void isValidBloodPressureFailNotANumber() {
        assertFalse(isValidBloodPressure("a/b"));
    }

    @Test
    public void isValidBloodPressureFailWrongFormatSlash() {
        assertFalse(isValidBloodPressure("20-80"));
    }

    @Test
    public void isValidBloodPressureFailBadCharacter() {
        assertFalse(isValidBloodPressure("20/]90"));
    }

    @Test
    public void isValidBloodPressureFailBackSlash() {
        assertFalse(isValidBloodPressure("89\\76"));
    }

}
