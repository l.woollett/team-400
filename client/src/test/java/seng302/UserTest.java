package seng302;

import org.junit.BeforeClass;
import org.junit.Test;
import seng302.Enum.RegionEnum;
import seng302.Model.Hospital;
import seng302.Model.Profile;
import seng302.Model.User;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

import static org.junit.Assert.*;

public class UserTest {

    private static User user;
    private static Method hasChangedMethod;

    @BeforeClass
    public static void setUp() {
        user = new Profile("username", LocalDate.now(), new Date(), "test");

        try {
            hasChangedMethod = User.class.getDeclaredMethod("hasChanged", Object.class, Object.class);
            hasChangedMethod.setAccessible(true);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Return a date object from a string representation. Must follow the format yyyy-MM-dd
     *
     * @param dateStr The String representation of the Date.
     * @return A Date object for the passed String.
     */
    private Date parseDate(String dateStr) {

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;

        try {
            date = simpleDateFormat.parse(dateStr);
        } catch (ParseException e) {
            e.printStackTrace();
            fail();
        }

        return date;
    }

    /**
     * Test to see if we can detect if the organisation has changed.
     *
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    @Test
    public void hasChangedOrganisationTrue() throws IllegalAccessException, InvocationTargetException {
        Hospital hospital1 = new Hospital("Hospital Name", RegionEnum.CANTERBURY, "Hospital Address", 1, 1);
        Hospital hospital2 = new Hospital("Hospital Name2", RegionEnum.CANTERBURY, "Hospital Address", 1, 1);
        boolean result = (boolean) hasChangedMethod.invoke(user, hospital1, hospital2);
        assertTrue(result);
    }

    /**
     * Test to see if we can detect if the organisation has changed.
     *
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     */
    @Test
    public void hasChangedOrganisationFalse() throws IllegalAccessException, InvocationTargetException {
        Hospital hospital1 = new Hospital("Hospital Name", RegionEnum.CANTERBURY, "Hospital Address", 1, 1);
        Hospital hospital2 = new Hospital("Hospital Name", RegionEnum.CANTERBURY, "Hospital Address", 1, 1);
        boolean result = (boolean) hasChangedMethod.invoke(user, hospital1, hospital2);
        assertFalse(result);
    }

    /**
     * Test if we can detect if a string has changed.
     */
    @Test
    public void hasChangedStringTrue() {
        try {
            boolean result = (boolean) hasChangedMethod.invoke(user, "Original String", "Changed String");
            assertTrue(result);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test if we can detect if a string has changed.
     */
    @Test
    public void hasChangedStringTrueOriginalValueNull() {
        try {
            boolean result = (boolean) hasChangedMethod.invoke(user, null, "Changed String");
            assertTrue(result);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test if we can detect if a string has changed.
     */
    @Test
    public void hasChangedStringTrueNewValueNull() {
        try {
            boolean result = (boolean) hasChangedMethod.invoke(user, "Original", null);
            assertTrue(result);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test if we can detect if a string has not been changed.
     */
    @Test
    public void hasChangedStringFalse() {
        try {
            boolean result = (boolean) hasChangedMethod.invoke(user, "Original String", "Original String");
            assertFalse(result);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test if we can detect if a string has not been changed.
     */
    @Test
    public void hasChangedStringFalseNulls() {
        try {
            boolean result = (boolean) hasChangedMethod.invoke(user, null, null);
            assertFalse(result);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test if we can detect if a Date has changed.
     */
    @Test
    public void hasChangedDateTrue() {
        Date originalDate = parseDate("2000-01-01");
        Date newDate = parseDate("2000-02-02");
        try {
            boolean result = (boolean) hasChangedMethod.invoke(user, originalDate, newDate);
            assertTrue(result);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test if we can detect if a Date has changed.
     */
    @Test
    public void hasChangedDateTrueOriginalNull() {
        Date newDate = parseDate("2000-02-02");
        try {
            boolean result = (boolean) hasChangedMethod.invoke(user, null, newDate);
            assertTrue(result);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test if we can detect if a Date has changed.
     */
    @Test
    public void hasChangedDateTrueNewNull() {
        Date originalDate = parseDate("2000-01-01");
        try {
            boolean result = (boolean) hasChangedMethod.invoke(user, originalDate, null);
            assertTrue(result);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test if we can detect if a Date has not changed.
     */
    @Test
    public void hasChangedDateFalse() {
        Date date = new Date();
        try {
            boolean result = (boolean) hasChangedMethod.invoke(user, date, date.clone());
            assertFalse(result);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Test if we can detect if a Date has not changed.
     */
    @Test
    public void hasChangedDateFalseNulls() {
        try {
            boolean result = (boolean) hasChangedMethod.invoke(user, null, null);
            assertFalse(result);
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            fail();
        }
    }


}
