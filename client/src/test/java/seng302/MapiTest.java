package seng302;

import org.junit.Ignore;
import org.junit.Test;
import seng302.API.Mapi;

import java.util.ArrayList;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

@Ignore
public class MapiTest {

    /**
     * Test if we can get can correctly retrieve the ingredients for a medication.
     */
    @Test
    public void testIngredientsValid() {
        String toTest = "Aspirin";
        assertTrue(Mapi.getIngredients(toTest).toString().equals("[Aspirin, Aspirin; dipyridamole]"));
    }

    @Test
    public void testOtherIngredientsValid() {
        String toTest = "Doca";
        assertTrue(Mapi.getIngredients(toTest).toString().equals("[Desoxycorticosterone acetate]"));
    }

    /**
     * Test if we can determine that a medication exists.
     */
    @Test
    public void checkMedicationExists() {
        String toTest = "Robaxin";
        assertTrue(Mapi.medicineExists(toTest));
    }

    /**
     * Test that we can determine that a medication doesn't exist.
     */
    @Test
    public void checkMedicationExists2() {
        String toTest = "Roba";
        assertFalse(Mapi.medicineExists(toTest));
    }

    /**
     * Test we can search for matching results for 'Roba'
     */

    @Test
    public void checkSpaceInMedication() {
        String toTest = "Asellacrin 10";
        assertTrue(Mapi.medicineExists(toTest));
    }

    @Test
    public void testValidSearch() {
        ArrayList<String> requested = Mapi.getAutoComplete("Roba");
        ArrayList<String> wanted = new ArrayList<>();
        wanted.add("Robaxin");
        wanted.add("Robaxin-750");
        wanted.add("Robaxisal");
        assertTrue(requested.equals(wanted));
    }

    /**
     * Test that we get no results if nothing since 'Roba' doesn't match anything.
     */
    @Test
    public void testInvalidSearch() {
        ArrayList<String> requested = Mapi.getAutoComplete("Robo");
        assertTrue(requested.get(0).isEmpty());
    }

    /**
     * Test that we get no results if we pass the empty string. This will time out the connection.
     */
    @Test
    public void testEmptySearch() {
        ArrayList<String> requested = Mapi.getAutoComplete("");
        assertTrue(requested.get(0).isEmpty()); //Should have hit a 503 error, because it times out.
    }

}
