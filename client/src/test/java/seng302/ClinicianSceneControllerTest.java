package seng302;

import org.junit.Test;
import seng302.Enum.GenderEnum;
import seng302.Enum.RegionEnum;
import seng302.Model.Clinician;
import seng302.Model.Hospital;
import seng302.ModelController.ClinicianController;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.Assert.assertFalse;
import static seng302.CustomAsserts.assertChangeDescriptionEqual;

/**
 * Test the clinician controller class.
 */
public class ClinicianSceneControllerTest {

    /**
     * Test if we can detect a missing default clinician.
     */
    @Test
    public void hasDefaultClinicianTestMissingClinician() {
        ClinicianController.setClinicians(new ArrayList<>()); // Ensure there are no clinicians
        assertFalse(ClinicianController.hasDefaultClinician());
    }

    /**
     * Test if the correct description for the organisation will be added to the clinician's history.
     */
    @Test
    public void changedOrganisationTest() {
        Clinician clinician = new Clinician("Jake", "Jackson", "Jacked",
                "jackie123", "123 Fake Street", RegionEnum.CANTERBURY, GenderEnum.MALE, new Date(),
                LocalDate.now(), new Hospital("ODMS", RegionEnum.CANTERBURY, "Address", 0, 0), "staff0");
        clinician.setHospital(new Hospital("OrgaNZ", RegionEnum.CANTERBURY, "Address", 0, 0));

        String changeDesc = clinician.getChanges().get(0);
        Date currentDate = new Date();

        String modelSolution = currentDate.toString() + ": Changed organisation to OrgaNZ";

        assertChangeDescriptionEqual(modelSolution, changeDesc);
    }

    /**
     * Test if the correct description for the staff ID will be added to the clinician's history.
     */
    @Test
    public void changedStaffIdTest() {
        Clinician clinician = new Clinician("Jake", "Jackson", "Jacked",
                "jackie123", "123 Fake Street", RegionEnum.CANTERBURY, GenderEnum.MALE, new Date(),
                LocalDate.now(), new Hospital("ODMS", RegionEnum.CANTERBURY, "Address", 0, 0), "staff0");
        clinician.setStaffId("staff1");

        String changeDesc = clinician.getChanges().get(0);
        Date currentDate = new Date();

        String modelSolution = currentDate.toString() + ": Changed staff ID to staff1";

        assertChangeDescriptionEqual(modelSolution, changeDesc);
    }
}
