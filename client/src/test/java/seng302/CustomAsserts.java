package seng302;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.fail;

public class CustomAsserts {

    /**
     * Compare the dates with a one second precision. This will handle the case where the two values are generated at
     * slightly different times but are essentially the same. E.g. getting the modified date of a profile and then
     * getting the current time. There will be slight difference but for all intent and purposes they are the same.
     *
     * @param date1 First date
     * @param date2 Second date
     */
    public static void assertDateEquals(Date date1, Date date2) {
        long unixTime1 = date1.getTime();
        long unixTime2 = date2.getTime();

        assertEquals(unixTime1, unixTime2, 1000);
    }


    /**
     * Assert if the description for the profile changes are valid.
     *
     * @param modelSolution What the description should be.
     * @param result        What the description was.
     */
    public static void assertChangeDescriptionEqual(String modelSolution, String result) {
        final int END_OF_TIMESTAMP = 29;

        Date modelSolutionDate = null;
        Date resultDate = null;

        String modelSolutionDateStr = modelSolution.substring(0, modelSolution.indexOf(":", END_OF_TIMESTAMP));
        String resultDateStr = result.substring(0, modelSolution.indexOf(":", END_OF_TIMESTAMP));

        String modelSolutionDesc = modelSolution.substring(modelSolution.indexOf(":", END_OF_TIMESTAMP));
        String resultDesc = result.substring(modelSolution.indexOf(":", END_OF_TIMESTAMP));


        try {
            DateFormat df = new SimpleDateFormat("EEE MMM dd kk:mm:ss zzz yyyy");
            modelSolutionDate = df.parse(modelSolutionDateStr);
            resultDate = df.parse(resultDateStr);
        } catch (ParseException e) {
            fail();
            e.printStackTrace();
        }

        assertDateEquals(resultDate, modelSolutionDate); // This will check that the description has the correct
        // timestamp/date

        assertTrue(modelSolutionDesc.equals(resultDesc));

    }

}
