package seng302;

import org.junit.Test;
import seng302.CustomException.DoesNotExist;
import seng302.Enum.OrganEnum;
import seng302.ModelController.OrganController;

import java.util.ArrayList;
import java.util.Arrays;

import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static seng302.Enum.OrganEnum.*;

/**
 * Test the OrganController class.
 *
 * @author Patrick Ma
 */
public class OrganControllerTest {

    @Test
    public void liverTest() {
        try {
            assertEquals(LIVER, OrganController.getOrgan("LIVER"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void liverTest2() {
        try {
            assertEquals(LIVER, OrganController.getOrgan("liver"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void kidneyTest() {
        try {
            assertEquals(KIDNEY, OrganController.getOrgan("KIDNEY"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void kidneyTest2() {
        try {
            assertEquals(KIDNEY, OrganController.getOrgan("kidney"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void pancreasTest() {
        try {
            assertEquals(PANCREAS, OrganController.getOrgan("PANCREAS"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void pancreasTest2() {
        try {
            assertEquals(PANCREAS, OrganController.getOrgan("pancreas"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void heartTest() {
        try {
            assertEquals(HEART, OrganController.getOrgan("HEART"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void heartTest2() {
        try {
            assertEquals(HEART, OrganController.getOrgan("heart"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void heartValveTest() {
        try {
            assertEquals(HEART_VALVES, OrganController.getOrgan("HEART_VALVES"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void heartValveTest2() {
        try {
            assertEquals(HEART_VALVES, OrganController.getOrgan("heart_valves"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void heartValveTest3() {
        try {
            assertEquals(HEART_VALVES, OrganController.getOrgan("Heart Valves"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void lungTest() {
        try {
            assertEquals(LUNG, OrganController.getOrgan("LUNG"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void lungTest2() {
        try {
            assertEquals(LUNG, OrganController.getOrgan("lung"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void intestineTest() {
        try {
            assertEquals(INTESTINE, OrganController.getOrgan("INTESTINE"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void intestineTest2() {
        try {
            assertEquals(INTESTINE, OrganController.getOrgan("intestine"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void corneaTest() {
        try {
            assertEquals(CORNEA, OrganController.getOrgan("CORNEA"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void corneaTest2() {
        try {
            assertEquals(CORNEA, OrganController.getOrgan("cornea"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void middleEarTest() {
        try {
            assertEquals(MIDDLE_EAR, OrganController.getOrgan("MIDDLE_EAR"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void middleEarTest2() {
        try {
            assertEquals(MIDDLE_EAR, OrganController.getOrgan("middle_ear"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void middleEarTest3() {
        try {
            assertEquals(MIDDLE_EAR, OrganController.getOrgan("Middle Ear"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void skinTest() {
        try {
            assertEquals(SKIN, OrganController.getOrgan("SKIN"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void skinTest2() {
        try {
            assertEquals(SKIN, OrganController.getOrgan("skin"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void boneTest() {
        try {
            assertEquals(BONE, OrganController.getOrgan("BONE"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void boneTest2() {
        try {
            assertEquals(BONE, OrganController.getOrgan("bone"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void boneMarrowTest() {
        try {
            assertEquals(BONE_MARROW, OrganController.getOrgan("BONE_MARROW"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void boneMarrowTest2() {
        try {
            assertEquals(BONE_MARROW, OrganController.getOrgan("bone_marrow"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void boneMarrowTest3() {
        try {
            assertEquals(BONE_MARROW, OrganController.getOrgan("Bone Marrow"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void connectiveTissueTest() {
        try {
            assertEquals(CONNECTIVE_TISSUE, OrganController.getOrgan("CONNECTIVE_TISSUE"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void connectiveTissueTest2() {
        try {
            assertEquals(CONNECTIVE_TISSUE, OrganController.getOrgan("connective_tissue"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    @Test
    public void connectiveTissueTest3() {
        try {
            assertEquals(CONNECTIVE_TISSUE, OrganController.getOrgan("Connective Tissue"));
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }
    }

    /**
     * @throws DoesNotExist entered value specifying an organ does not match any predefined organs
     */
    @Test(expected = DoesNotExist.class)
    public void invalidOrganTest() throws DoesNotExist {
        OrganController.getOrgan("not_valid");
        fail(); //See if it reaches here
    }

    @Test
    public void extractOrgansValidTest() {
        ArrayList<OrganEnum> modelSolution = new ArrayList<>(Arrays.asList(KIDNEY, PANCREAS, CORNEA));
        ArrayList<OrganEnum> retrievedSolution = null;
        try {
            retrievedSolution = OrganController.extractOrgans("KIDNEY,PANCREAS,CORNEA");
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }

        assertEquals(retrievedSolution.size(), modelSolution.size());

        for (int i = 0; i < retrievedSolution.size(); i++) {
            assertTrue(modelSolution.get(i).equals(retrievedSolution.get(i)));
        }
    }

    /**
     * If extra spaces are added they should not affect the result.
     */
    @Test
    public void extractOrgansValidTest2() {
        ArrayList<OrganEnum> modelSolution = new ArrayList<>(Arrays.asList(KIDNEY, PANCREAS, CORNEA));
        ArrayList<OrganEnum> retrievedSolution = null;
        try {
            retrievedSolution = OrganController.extractOrgans("KIDNEY, PANCREAS, CORNEA");
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }

        assertEquals(retrievedSolution.size(), modelSolution.size());

        for (int i = 0; i < retrievedSolution.size(); i++) {
            assertTrue(modelSolution.get(i).equals(retrievedSolution.get(i)));
        }
    }

    /**
     * If extra spaces are added they should not affect the result.
     */
    @Test
    public void extractOrgansValidTest3() {
        ArrayList<OrganEnum> modelSolution = new ArrayList<>(Arrays.asList(KIDNEY, PANCREAS, CORNEA));
        ArrayList<OrganEnum> retrievedSolution = null;
        try {
            retrievedSolution = OrganController.extractOrgans(" KIDNEY, PANCREAS, CORNEA  ");
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
            fail();
        }

        assertEquals(retrievedSolution.size(), modelSolution.size());

        for (int i = 0; i < retrievedSolution.size(); i++) {
            assertTrue(modelSolution.get(i).equals(retrievedSolution.get(i)));
        }
    }

    /**
     * Should throw error if one of the organs listed does not exist.
     *
     * @throws DoesNotExist specified organ doesn't match with any predefined organs
     */
    @Test(expected = DoesNotExist.class)
    public void extractOrgansInvalidTest() throws DoesNotExist {
        OrganController.extractOrgans("KIDNEY,PANCREAS,NOT_AN_ORGAN");
    }

    /**
     * Should throw error if one of the organs listed does not exist.
     *
     * @throws DoesNotExist specified organ doesn't match with any predefined organs
     */
    @Test(expected = DoesNotExist.class)
    public void extractOrgansInvalidTest2() throws DoesNotExist {
        OrganController.extractOrgans("NOT_AN_ORGAN,KIDNEY,PANCREAS");
    }

    /**
     * Should throw error if one of the organs listed does not exist.
     *
     * @throws DoesNotExist specified organ doesn't match with any predefined organs
     */
    @Test(expected = DoesNotExist.class)
    public void extractOrgansInvalidTest3() throws DoesNotExist {
        OrganController.extractOrgans("KIDNEY,NOT_AN_ORGAN,PANCREAS");
    }

    /**
     * Should throw error if one of the organs listed does not exist.
     *
     * @throws DoesNotExist specified organ doesn't match with any predefined organs
     */
    @Test(expected = DoesNotExist.class)
    public void extractOrgansInvalidTest4() throws DoesNotExist {
        OrganController.extractOrgans("KIDNEY , NOT_AN_ORGAN , PANCREAS");
    }

    /**
     * If no organs listed should be counted as an error..
     *
     * @throws DoesNotExist Missing value
     */
    @Test(expected = DoesNotExist.class)
    public void extractOrgansInvalidTest5() throws DoesNotExist {
        OrganController.extractOrgans("");
    }

    /**
     * If no organs listed return an empty ArrayList.
     *
     * @throws DoesNotExist missing value, white spaces are trimmed
     */
    @Test(expected = DoesNotExist.class)
    public void extractOrgansInvalidTest6() throws DoesNotExist {
        OrganController.extractOrgans("   ");
    }


}
