package seng302;

import org.junit.Test;
import seng302.API.EhealthMe;

import java.util.ArrayList;

import static junit.framework.TestCase.assertTrue;

public class EhealthMeTest {

    @Test
    public void testOneProperAndOneImproperDrug() {
        ArrayList<String> results = EhealthMe.getDrugInteractions("Escitalopram", "Diapam", "FEMALE", 32);
        assertTrue(results.contains("Error. No drug interactions"));
    }

    @Test
    public void testTwoImproperDrugs() {
        ArrayList<String> results = EhealthMe.getDrugInteractions("life", "D4NKKUSH", "FEMALE", 32);
        assertTrue(results.contains("Error. No drug interactions"));
    }


    @Test
    public void testBlankArgs() {
        ArrayList<String> results = EhealthMe.getDrugInteractions("", "", "", null);
        assertTrue(results.contains("Error. No drug interactions"));
    }
}
