package seng302;

import org.junit.Test;
import seng302.Model.Procedure;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static seng302.Enum.OrganEnum.HEART;
import static seng302.Enum.OrganEnum.LIVER;

public class ProcedureTest {

    /**
     * Check if we can correctly identify the same procedure.
     */
    @Test
    public void equalProcedurePass() {
        LocalDate procedureDate = LocalDate.parse("1990-01-01");
        LocalDate procedureDate2 = LocalDate.parse("1990-01-01");
        Procedure procedure =
                new Procedure("Summary", "Description", procedureDate, new ArrayList<>(Arrays.asList(LIVER, HEART)));
        Procedure procedure2 =
                new Procedure("Summary", "Description", procedureDate2, new ArrayList<>(Arrays.asList(LIVER, HEART)));
        assertTrue(procedure.equals(procedure2));
    }

    /**
     * Check if we can correctly identify different procedures when the difference is only the procedure date.
     */
    @Test
    public void equalProcedureFailDate() {
        LocalDate procedureDate = LocalDate.parse("1990-01-01");
        LocalDate procedureDate2 = LocalDate.parse("1990-01-02");
        Procedure procedure =
                new Procedure("Summary", "Description", procedureDate, new ArrayList<>(Arrays.asList(LIVER, HEART)));
        Procedure procedure2 =
                new Procedure("Summary", "Description", procedureDate2, new ArrayList<>(Arrays.asList(LIVER, HEART)));
        assertFalse(procedure.equals(procedure2));
    }

    /**
     * Check if we can correctly identify different procedures when the difference is only the summary.
     */
    @Test
    public void equalProcedureFailSummary() {
        LocalDate procedureDate = LocalDate.parse("1990-01-01");
        Procedure procedure =
                new Procedure("Summary", "Description", procedureDate, new ArrayList<>(Arrays.asList(LIVER, HEART)));
        Procedure procedure2 =
                new Procedure("Summary2", "Description", procedureDate, new ArrayList<>(Arrays.asList(LIVER, HEART)));
        assertFalse(procedure.equals(procedure2));
    }

    /**
     * Check if we can correctly identify different procedures when the difference is only the description.
     */
    @Test
    public void equalProcedureFailDescription() {
        LocalDate procedureDate = LocalDate.parse("1990-01-01");
        Procedure procedure =
                new Procedure("Summary", "Description", procedureDate, new ArrayList<>(Arrays.asList(LIVER, HEART)));
        Procedure procedure2 =
                new Procedure("Summary", "Description2", procedureDate, new ArrayList<>(Arrays.asList(LIVER, HEART)));
        assertFalse(procedure.equals(procedure2));
    }

    /**
     * Check if we can correctly identify different procedures when the difference are the affected organs for
     * donation.
     */
    @Test
    public void equalProcedureFailOrgans() {
        LocalDate procedureDate = LocalDate.parse("1990-01-01");
        Procedure procedure =
                new Procedure("Summary", "Description", procedureDate, new ArrayList<>(Arrays.asList(LIVER)));
        Procedure procedure2 =
                new Procedure("Summary", "Description2", procedureDate, new ArrayList<>(Arrays.asList(LIVER, HEART)));
        assertFalse(procedure.equals(procedure2));
    }

}
