package GUITests;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import seng302.Enum.RegionEnum;
import seng302.GUI.CreateUserController;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.*;

/**
 * Sometimes exceptions will occur while running this test (mostly looks to be caused by not passing arguments into
 * main,  we can't from here) sometimes this is because testFX is not being run traditionally because it takes too
 * long for 'write' and 'clickon' events to happen. Instead testFX is being used to retrieve the relevant fields and
 * assigning them to private local variables, these local fields' values are being set and retrieved directly and set
 * using Platform.runLater. This class purely checks that the undo and redo works as it should (event triggering etc)
 * but will not check if styles etc are being handled properly. For this reason it is advised to also run manual
 * tests to accurately see what is happening to the GUI when undo and redo is implemented.
 */
@Ignore
public class UndoRedoCreateUserTest extends TestFXBase {

    @FXML
    private TextField firstNameTextField, middleNameTextField, lastNameTextField, usernameTextField, aliasTextField,
            heightTextField, weightTextField, addressTextField;
    @FXML
    private CreateUserController controller;
    @FXML
    private DatePicker dateOfBirthDatePicker, dateOfDeathDatePicker;
    @FXML
    private ChoiceBox<String> genderChoiceBox;
    @FXML
    private ChoiceBox<String> birthGenderChoiceBox;
    @FXML
    private ChoiceBox<String> bloodTypeChoiceBox;
    @FXML
    private ChoiceBox<RegionEnum> regionChoiceBox;

    @FXML
    private CheckBox boneCheckBox, boneMarrowCheckBox, connectiveTissueCheckBox, corneaCheckBox, heartCheckBox,
            heartValveCheckBox, intestineCheckBox, kidneyCheckBox, liverCheckBox, lungCheckBox, middleEarCheckBox,
            pancreasCheckBox, skinCheckBox;

    private List<TextField> textFields;
    private List<DatePicker> datePickers;
    private List<CheckBox> checkBoxes;

    private Button createUserUndo, createUserRedo;

    @Before
    public void setupGuiElements() {
        bindTextFields();
        bindDatePickers();
        bindChoiceBoxes();
        bindCheckBoxes();
        bindButtons();
        controller.getUndoRedoController().reset();
    }

    /**
     * Function binds all the GUI textFields to private textFields for easy access and editing/querying.
     */
    private void bindTextFields() {
        firstNameTextField = lookup("#firstNameTextField").query();
        middleNameTextField = lookup("#middleNameTextField").query();
        lastNameTextField = lookup("#lastNameTextField").query();
        usernameTextField = lookup("#usernameTextField").query();
        aliasTextField = lookup("#aliasTextField").query();
        heightTextField = lookup("#heightTextField").query();
        weightTextField = lookup("#weightTextField").query();
        addressTextField = lookup("#addressTextField").query();
        textFields = new ArrayList<>(
                Arrays.asList(firstNameTextField, middleNameTextField, lastNameTextField, usernameTextField,
                        aliasTextField,
                        heightTextField, weightTextField, addressTextField));
    }

    /**
     * Function binds all the GUI datePickers to private datePickers for easy access and editing/querying.
     */
    private void bindDatePickers() {
        dateOfBirthDatePicker = lookup("#dateOfBirthDatePicker").query();
        dateOfDeathDatePicker = lookup("#dateOfDeathDatePicker").query();
        datePickers = new ArrayList<>(Arrays.asList(dateOfBirthDatePicker, dateOfDeathDatePicker));
    }

    /**
     * Function binds all the GUI choiceBoxes to private choiceBoxes for easy access and editing/querying.
     */
    private void bindChoiceBoxes() {
        genderChoiceBox = lookup("#genderChoiceBox").query();
        birthGenderChoiceBox = lookup("#birthGenderChoiceBox").query();
        bloodTypeChoiceBox = lookup("#bloodTypeChoiceBox").query();
        regionChoiceBox = lookup("#regionChoiceBox").query();
    }

    /**
     * Function binds all the GUI checkBoxes to private checkBoxes for easy access and editing/querying.
     */
    private void bindCheckBoxes() {
        boneCheckBox = lookup("#boneCheckBox").query();
        boneMarrowCheckBox = lookup("#boneMarrowCheckBox").query();
        connectiveTissueCheckBox = lookup("#connectiveTissueCheckBox").query();
        corneaCheckBox = lookup("#corneaCheckBox").query();
        heartCheckBox = lookup("#heartCheckBox").query();
        heartValveCheckBox = lookup("#heartValveCheckBox").query();
        intestineCheckBox = lookup("#intestineCheckBox").query();
        kidneyCheckBox = lookup("#kidneyCheckBox").query();
        liverCheckBox = lookup("#liverCheckBox").query();
        lungCheckBox = lookup("#lungCheckBox").query();
        middleEarCheckBox = lookup("#middleEarCheckBox").query();
        pancreasCheckBox = lookup("#pancreasCheckBox").query();
        skinCheckBox = lookup("#skinCheckBox").query();
        checkBoxes = new ArrayList<>(
                Arrays.asList(boneCheckBox, boneMarrowCheckBox, connectiveTissueCheckBox, corneaCheckBox, heartCheckBox,
                        heartValveCheckBox, intestineCheckBox, kidneyCheckBox, liverCheckBox, lungCheckBox,
                        middleEarCheckBox,
                        pancreasCheckBox, skinCheckBox));
    }

    private void bindButtons() {
        createUserUndo = lookup("#createUserUndo").query();
        createUserRedo = lookup("#createUserRedo").query();
    }

    @Override
    public void start(Stage stage) {
        FXMLLoader loader = new FXMLLoader(CreateUserController.class.getResource("createUserLogin.fxml"));
        Parent mainRoot = null;
        try {
            mainRoot = loader.load();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        if (mainRoot != null) {
            Scene scene = new Scene(mainRoot);
            stage.setScene(scene);
            stage.show();
        }
        controller = loader.getController();
    }


    /**
     * Tests undo/redo functionality for the TextFields in the create user scene. Many asserts are required to
     * ensure that the undo/redo feature works, especially in long chains of events.
     */
    @Test
    public void textFieldUndoRedoTest() {
        for (TextField testField : textFields) {
            testField.setText("correct");
        }
        assertEquals(8, controller.getUndoRedoController().getUndoDequeSize());

        //check that undo works and that original values are restored, even when they start as empty
        for (int i = 0; i < 8; i++) {
            createUserUndo.fire();
        }
        assertEquals(0, controller.getUndoRedoController().getUndoDequeSize());
        assertEquals(8, controller.getUndoRedoController().getRedoDequeSize());
        for (TextField testField : textFields) {
            assertEquals("", testField.getText());
        }

        //check that redo works
        for (int i = 0; i < 8; i++) {
            createUserRedo.fire();
        }
        assertEquals(8, controller.getUndoRedoController().getUndoDequeSize());
        assertEquals(0, controller.getUndoRedoController().getRedoDequeSize());
        for (TextField testField : textFields) {
            assertEquals("correct", testField.getText());
        }

        //check that a change clears the redo deque
        createUserUndo.fire();
        firstNameTextField.setText("This should cause the redo deque to empty");
        assertEquals(0, controller.getUndoRedoController().getRedoDequeSize());
    }

    /**
     * Tests the undo/redo functionality for the DatePickers in the create user scene. Many asserts are required to
     * ensure that the undo/redo feature works, especially in long chains of events.
     */
    @Test
    public void datePickerUndoRedoTest() {
        LocalDate firstDate = LocalDate.of(1990, 1, 2);
        LocalDate secondDate = LocalDate.of(1990, 1, 3);
        assertEquals(LocalDate.of(1990, 1, 1), dateOfBirthDatePicker.getValue());
        for (DatePicker testPicker : datePickers) {
            testPicker.setValue(firstDate);
        }
        assertEquals(2, controller.getUndoRedoController().getUndoDequeSize());

        //check that undo works and that original values are restored, even when they start as null
        for (int i = 0; i < 2; i++) {
            createUserUndo.fire();
        }
        assertEquals(0, controller.getUndoRedoController().getUndoDequeSize());
        assertEquals(2, controller.getUndoRedoController().getRedoDequeSize());
        assertEquals(LocalDate.of(1990, 1, 1), dateOfBirthDatePicker.getValue());
        assertNull(dateOfDeathDatePicker.getValue());

        //check that redo works
        for (int i = 0; i < 2; i++) {
            createUserRedo.fire();
        }
        assertEquals(2, controller.getUndoRedoController().getUndoDequeSize());
        assertEquals(0, controller.getUndoRedoController().getRedoDequeSize());
        for (DatePicker testPicker : datePickers) {
            assertEquals(firstDate, testPicker.getValue());
        }

        //check that a change clears the redo deque
        createUserUndo.fire();
        dateOfBirthDatePicker.setValue(secondDate);
        assertEquals(0, controller.getUndoRedoController().getRedoDequeSize());
    }

    /**
     * Tests the undo/redo functionality for the ChoiceBoxes in the create user scene. Many asserts are required to
     * ensure that the undo/redo feature works, especially in long chains of events.
     */
    @Test
    public void choiceBoxUndoRedoTest() {
        genderChoiceBox.setValue("Male");
        birthGenderChoiceBox.setValue("Male");
        bloodTypeChoiceBox.setValue("A+");
        regionChoiceBox.setValue(RegionEnum.AUCKLAND);
        assertEquals(4, controller.getUndoRedoController().getUndoDequeSize());

        //check that undo works and that original values are restored, even when they start as null
        for (int i = 0; i < 4; i++) {
            createUserUndo.fire();
        }
        assertEquals(0, controller.getUndoRedoController().getUndoDequeSize());
        assertEquals(4, controller.getUndoRedoController().getRedoDequeSize());
        assertNull(genderChoiceBox.getValue());
        assertNull(birthGenderChoiceBox.getValue());
        assertNull(bloodTypeChoiceBox.getValue());
        assertNull(regionChoiceBox.getValue());

        //check that redo works
        for (int i = 0; i < 4; i++) {
            createUserRedo.fire();
        }
        assertEquals(4, controller.getUndoRedoController().getUndoDequeSize());
        assertEquals(0, controller.getUndoRedoController().getRedoDequeSize());
        assertEquals("Male", genderChoiceBox.getValue());
        assertEquals("Male", birthGenderChoiceBox.getValue());
        assertEquals("A+", bloodTypeChoiceBox.getValue());
        assertEquals(RegionEnum.AUCKLAND, regionChoiceBox.getValue());

        //check that a change clears the redo deque
        createUserUndo.fire();
        genderChoiceBox.setValue("Other"); //should clear the redoDeque
        assertEquals(0, controller.getUndoRedoController().getRedoDequeSize());
    }

    /**
     * Tests the undo/redo functionality for the CheckBoxes in the create user scene. Many asserts are required to
     * ensure that the undo/redo feature works, especially in long chains of events.
     */
    @Test
    public void checkBoxUndoRedoTest() {
        for (CheckBox testBox : checkBoxes) {
            testBox.setSelected(true);
        }
        assertEquals(checkBoxes.size(), controller.getUndoRedoController().getUndoDequeSize());

        //check that undo works and that original values are restored, even when they start as null
        for (int i = 0; i < checkBoxes.size(); i++) {
            createUserUndo.fire();
        }
        assertEquals(0, controller.getUndoRedoController().getUndoDequeSize());
        assertEquals(checkBoxes.size(), controller.getUndoRedoController().getRedoDequeSize());
        for (CheckBox testBox : checkBoxes) {
            assertFalse(testBox.isSelected());
        }

        //check that redo works
        for (int i = 0; i < checkBoxes.size(); i++) {
            createUserRedo.fire();
        }
        assertEquals(checkBoxes.size(), controller.getUndoRedoController().getUndoDequeSize());
        assertEquals(0, controller.getUndoRedoController().getRedoDequeSize());
        for (CheckBox testBox : checkBoxes) {
            assertTrue(testBox.isSelected());
        }

        //check that a change clears the redo deque
        createUserUndo.fire();
        liverCheckBox.setSelected(false);
        assertEquals(0, controller.getUndoRedoController().getRedoDequeSize());
    }

    @Test
    public void mixedTest() {
        String text1 = "first";
        String text2 = "second";
        LocalDate date1 = LocalDate.of(1990, 1, 2);
        LocalDate date2 = LocalDate.of(1990, 2, 2);
        LocalDate death1 = LocalDate.of(1998, 1, 2);
        LocalDate death2 = LocalDate.of(1998, 2, 2);

        firstNameTextField.setText(text1);
        lastNameTextField.setText(text2);
        createUserUndo.fire();
        dateOfBirthDatePicker.setValue(date1);
        createUserUndo.fire();
        createUserUndo.fire();
        assertEquals(2, controller.getUndoRedoController().getRedoDequeSize());
        assertEquals(0, controller.getUndoRedoController().getUndoDequeSize());
        assertEquals("", firstNameTextField.getText());
        createUserRedo.fire();
        createUserRedo.fire();

        dateOfBirthDatePicker.setValue(date2);
        dateOfDeathDatePicker.setValue(death1);
        dateOfDeathDatePicker.setValue(death2);
        createUserUndo.fire();
        genderChoiceBox.setValue("Male");
        createUserUndo.fire();
        assertEquals(death1, dateOfDeathDatePicker.getValue());

        liverCheckBox.setSelected(true);
        dateOfDeathDatePicker.setValue(death2);
        boneCheckBox.setSelected(true);
        createUserUndo.fire();
        createUserUndo.fire();
        createUserUndo.fire();
        assertFalse(boneCheckBox.isSelected());
        assertFalse(liverCheckBox.isSelected());
        assertEquals(death1, dateOfDeathDatePicker.getValue());

        for (int i = 0; i < 4; i++) {
            createUserUndo.fire();
        }
        assertEquals(0, controller.getUndoRedoController().getUndoDequeSize());
        assertEquals(7, controller.getUndoRedoController().getRedoDequeSize());
    }
}
