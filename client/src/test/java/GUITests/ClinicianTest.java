package GUITests;

import javafx.scene.input.KeyCode;
import org.junit.Ignore;
import org.junit.Test;
import seng302.Enum.RegionEnum;

import static org.junit.Assert.*;
import static org.testfx.api.FxAssert.verifyThat;
import static org.testfx.matcher.base.NodeMatchers.isVisible;

/**
 * A class to test with default clinician, with positive and negative tests
 */
@Ignore
public class ClinicianTest extends TestFXBase {

    /**
     * Test with a valid clinician name. The value has been set up in the parent class - TextFxBase
     */
    @Test
    public void loginAsDefaultClinicianSuccess() {
        assertEquals(getDefaultClinician().getUsername(), "defaultClinician");
        String defaultClinicianLoginName = getDefaultClinician().getUsername();

        if (defaultClinicianLoginName != null) {
            clickOn("#usernameTxtField").write(defaultClinicianLoginName);
            clickOn("#loginBtn");
            verifyThat("#mainAnchorPane", isVisible());
        }
    }

    /**
     * Edit the clinician with valid input and checks if it saves This test provide a valid input.
     */
    @Test
    public void editDefaultClinicianStaffIdSuccess() {
        assertTrue(editDefaultClinicianStaffId("normal"));
    }

    /**
     * Edit the clinician with invalid input and checks if it saves This test provide an invalid input.
     */
    @Test
    public void editDefaultClinicianStaffIdFailure() {
        assertFalse(editDefaultClinicianStaffId("..."));
    }

    /**
     * Edits the clinician with a VALID input of organisation and checks if it saves.
     */
    @Test
    public void editDefaultClinicianOrganisationSuccess() {
        assertTrue(editDefaultClinicianOrganisation("Dr MOE"));
    }

    /**
     * Edits the clinician with an INVALID input of organisation and checks if it saves.
     */
    @Test
    public void editDefaultClinicianOrganisationFailure() {
        assertFalse(editDefaultClinicianOrganisation("Invalid char ^"));
    }

    /**
     * Edits the clinician with a VALID input of firstName and checks if it saves.
     */
    @Test
    public void editDefaultClinicianFirstNameSuccess() {
        assertTrue(editDefaultClinicianFirstName("Gary"));
    }

    /**
     * Edits the clinician with an INVALID input of firstName and checks if it saves.
     */
    @Test
    public void editDefaultClinicianFirstNameFailure() {
        assertFalse(editDefaultClinicianFirstName("Invalid char *"));
    }

    /**
     * Edits the clinician with a VALID input of lastName and checks if it saves.
     */
    @Test
    public void editDefaultClinicianLastNameSuccess() {
        assertTrue(editDefaultClinicianLastName("Robinson"));
    }

    /**
     * Edits the clinician with an INVALID input of lastName and checks if it saves.
     */
    @Test
    public void editDefaultClinicianLastNameFailure() {
        assertFalse(editDefaultClinicianLastName("Invalid char &"));
    }

    /**
     * Edits the clinician with a VALID input of workAddress and checks if it saves.
     */
    @Test
    public void editDefaultClinicianWorkAddressSuccess() {
        assertTrue(editDefaultClinicianWorkAddress("200 Ilam Road"));
    }

    /**
     * Edits the clinician with an INVALID input of workAddress and checks if it saves.
     */
    @Test
    public void editDefaultClinicianWorkAddressFailure() {
        assertFalse(editDefaultClinicianWorkAddress("Invalid char ?"));
    }

    /**
     * Tests whether fields are reverted to their previous states when the cancel button is clicked.
     */
    @Test
    @Ignore
    public void cancelDefaultClinicianChangesSuccess() {
        assertTrue(cancelDefaultClinicianChanges());
    }

    /**
     * Edit the clinician with valid input of region and checks if it saves This test provides a valid input.
     */
    @Test
    public void editDefaultClinicianRegionSuccess() {
        assertTrue(editDefaultClinicianRegionBox(RegionEnum.GISBOURNE));
    }

    /**
     * Edit the clinician with given input and checks if it saves, after close and reopen
     *
     * @param newStaffId The staff's ID
     * @return a boolean to check whether if the input of the clinician id has been changed to the new staff id.
     */
    private boolean editDefaultClinicianStaffId(String newStaffId) {
        loginAsDefaultClinician();
        clickOnEdit();
        clickOn("#staffIdTextField");
        clearField();
        write(newStaffId);
        clickOn("#applyEditButton");
        return (getDefaultClinician().getStaffId().equals(newStaffId));
    }

    /**
     * Edits the clinician with given organisation input
     *
     * @param newOrganisation String of clinician's new organisation.
     * @return boolean of whether the input saved or not.
     */
    private boolean editDefaultClinicianOrganisation(String newOrganisation) {
        loginAsDefaultClinician();
        clickOnEdit();
        clickOn("#organisationTextField");
        clearField();
        write(newOrganisation);
        clickOn("#applyEditButton");
        return (getDefaultClinician().getHospital().equals(newOrganisation));
    }

    /**
     * Edits the clinician with given first name input
     *
     * @param newFirstName String of clinician's new intended first name.
     * @return boolean of whether the input was saved or not.
     */
    private boolean editDefaultClinicianFirstName(String newFirstName) {
        loginAsDefaultClinician();
        clickOnEdit();
        clickOn("#firstNameTextField");
        clearField();
        write(newFirstName);
        clickOn("#applyEditButton");
        return (getDefaultClinician().getFirstName().equals(newFirstName));
    }

    /**
     * Edits the clinician with given last name input
     *
     * @param newLastName String of clinician's new intended last name.
     * @return boolean of whether the input was saved or not.
     */
    private boolean editDefaultClinicianLastName(String newLastName) {
        loginAsDefaultClinician();
        clickOnEdit();
        clickOn("#lastNameTextField");
        clearField();
        write(newLastName);
        clickOn("#applyEditButton");
        return (getDefaultClinician().getLastName().equals(newLastName));
    }

    /**
     * Edit the clinician regionChoiceBox and checks if it saves Edit the clinician's region with a given input and
     * checks if it saves.
     *
     * @param newRegion The new region to be set.
     * @return a boolean to check whether if the input of the clinician id has been changed to the new staff id.
     */
    private boolean editDefaultClinicianRegionBox(RegionEnum newRegion) {
        loginAsDefaultClinician();
        clickOnEdit();
        clickOn("#regionChoiceBox");
        press(KeyCode.TAB);
        press(KeyCode.ENTER);
        clickOn("#applyEditButton");
        return (getDefaultClinician().getRegion().equals(newRegion));
    }

    /**
     * Edits the clinician with given work address input
     *
     * @param newWorkAddress String of clinician's new intended work address.
     * @return boolean of whether the input was saved or not.
     */
    private boolean editDefaultClinicianWorkAddress(String newWorkAddress) {
        loginAsDefaultClinician();
        clickOnEdit();
        clickOn("#workAddressTextField");
        clearField();
        write(newWorkAddress);
        clickOn("#applyEditButton");
        return (getDefaultClinician().getAddress().equals(newWorkAddress));
    }

    private boolean cancelDefaultClinicianChanges() {
        loginAsDefaultClinician();
        clickOnEdit();
        setAllFields();
        editAllFields();
        clickOn("#editCancelButton");
        return checkAllFields();
    }

    private void setAllFields() {
        // Change fields from defaults (to give all fields testable values)
        clickOn("#staffIdTextField");
        clearField();
        write("123");

        clickOn("#organisationTextField");
        clearField();
        write("Dr MOE");

        clickOn("#firstNameTextField");
        clearField();
        write("Gary");

        clickOn("#lastNameTextField");
        clearField();
        write("Robinson");

        // Sets to Auckland
        clickOn("#regionChoiceBox");
        press(KeyCode.TAB);
        press(KeyCode.ENTER);

        clickOn("#workAddressTextField");
        clearField();
        write("200 Ilam Road");

        // Saves changes
        clickOn("#applyEditButton");
    }

    private void editAllFields() {
        // Change fields off previous values
        clickOn("#staffIdTextField");
        clearField();
        write("changed");

        clickOn("#organisationTextField");
        clearField();
        write("changed");

        clickOn("#firstNameTextField");
        clearField();
        write("changed");

        clickOn("#lastNameTextField");
        clearField();
        write("changed");

        // Sets to Auckland
        clickOn("#regionChoiceBox");
        press(KeyCode.TAB);
        press(KeyCode.TAB);
        press(KeyCode.ENTER);

        clickOn("#workAddressTextField");
        clearField();
        write("changed");
    }

    private boolean checkAllFields() {
        return getDefaultClinician().getStaffId().equals("123") &&
                getDefaultClinician().getHospital().equals("Dr MOE") &&
                getDefaultClinician().getFirstName().equals("Gary") &&
                getDefaultClinician().getLastName().equals("Robinson") &&
                getDefaultClinician().getRegion().equals("Auckland") && // todo - fix this
                getDefaultClinician().getAddress().equals("200 Ilam Road");
    }

    /**
     * Login the page as a default Clinician
     */
    private void loginAsDefaultClinician() {
        String defaultClinicianLoginName = getDefaultClinician().getUsername();
        if (defaultClinicianLoginName != null) {
            clickOn("#usernameTxtField").write(defaultClinicianLoginName);
            clickOn("#loginBtn");
        }
    }
}