package GUITests;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.stage.Stage;
import org.junit.Ignore;
import org.junit.Test;
import seng302.Enum.OrganEnum;
import seng302.Enum.RegionEnum;
import seng302.GUI.Clinician.ClinicianSceneController;

import java.io.IOException;
import java.util.logging.Logger;

import static junit.framework.TestCase.assertEquals;

@Ignore
public class UndoRedoClinicianTransplantWaitingTest extends TestFXBase {
    private final Logger LOGGER = Logger.getLogger(UndoRedoClinicianTransplantWaitingTest.class.getName());
    @FXML
    private ClinicianSceneController controller;
    @FXML
    private Button undo, redo;

    @Override
    public void start(Stage stage) {
        FXMLLoader loader = new FXMLLoader(ClinicianSceneController.class.getResource("clinician.fxml"));
        Parent mainRoot = null;
        try {
            mainRoot = loader.load();
        } catch (IOException e) {
            LOGGER.severe(e.getMessage());
        }
        if (mainRoot != null) {
            Scene scene = new Scene(mainRoot);
            stage.setScene(scene);
            stage.show();
        }
        controller = loader.getController();
        undo = lookup("#undo").query();
        redo = lookup("#redo").query();
    }


    @Test
    public void testUndoAndRedoOneChoicBox() {
        clickOn("#clinicianTransplantWaitingTab");

        ChoiceBox<Enum> clinicianOrganComboBox = lookup("#clinicianOrganComboBox").query();
        clinicianOrganComboBox.setValue(OrganEnum.HEART);
        undo.fire();
        assertEquals(1, controller.getUndoRedoController().getUndoDequeSize());
        redo.fire();
        assertEquals(2, controller.getUndoRedoController().getUndoDequeSize());
    }


    @Test
    public void testUndoAndRedoAllChoicBox() {
        clickOn("#clinicianTransplantWaitingTab");

        ChoiceBox<Enum> clinicianOrganComboBox = lookup("#clinicianOrganComboBox").query();
        clinicianOrganComboBox.setValue(OrganEnum.HEART);
        ChoiceBox<Enum> clinicianRegionComboBox = lookup("#clinicianRegionComboBox").query();
        clinicianRegionComboBox.setValue(RegionEnum.CANTERBURY);
        assertEquals(3, controller.getUndoRedoController().getUndoDequeSize());
        undo.fire();
        assertEquals(2, controller.getUndoRedoController().getUndoDequeSize());
        redo.fire();
        assertEquals(3, controller.getUndoRedoController().getUndoDequeSize());

    }


}
