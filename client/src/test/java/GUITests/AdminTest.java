package GUITests;

import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import seng302.CustomException.DoesNotExist;
import seng302.Model.Admin;
import seng302.ModelController.ProfileController;
import seng302.ServerInteracton.ServerQueries.AdminRequests;
import seng302.ServerInteracton.ServerQueries.Authentication.AuthToken;

import static org.junit.Assert.assertEquals;
import static org.testfx.api.FxAssert.verifyThat;
import static org.testfx.util.NodeQueryUtils.isVisible;

// Works offline but cannot see #rolesPane from runner
@Ignore
public class AdminTest extends TestFXBase {

    /**
     * Test with a valid Admin name. The value has been set up in the parent class - TestFXBase
     */
    @Ignore //we need to update this test to use the server and database
    @Test
    public void loginAsDefaultAdminSuccess() {
        assertEquals(getDefaultAdmin().getUsername(), "admin");
        loginAsDefaultAdmin();
        verifyThat("#mainAnchorPane", isVisible());
    }

    /**
     * Login the page as a default Clinician
     */
    private void loginAsDefaultAdmin() {
        String defaultAdminUsername = getDefaultAdmin().getUsername();
        String defaultAdminPassword = getDefaultAdmin().getPassword();

        if (defaultAdminUsername != null) {
            clickOn("#toggleLoginAsAdmin");
            clickOn("#usernameTxtField").write(defaultAdminUsername);
            clickOn("#passwordTxtField").write(defaultAdminPassword);
            clickOn("#loginBtn");
        }
    }

    /**
     * Roles tab is reachable and visible.
     */
    @Test
    public void switchToAdminRolesPaneSuccess() {
        loginAsDefaultAdmin();
        switchToRolesTab();
        verifyThat("#rolesTab", isVisible());
    }

    /**
     * Tests if a donor can be created correctly through the admin roles tab.
     *
     * @throws DoesNotExist If the created user is not found. This will fail the test.
     */
    @Ignore //we need to update this test to use the server and database
    @Test
    public void createDonor() throws DoesNotExist {
        loginAsDefaultAdmin();
        switchToRolesTab();
        clickOn("#createDonorButton");
        clickOn("#usernameTextField").write("testDonor");
        clickOn("#firstNameTextField").write("testFirstName");
        clickOn("#createUserButton");
        assertEquals("testFirstName", ProfileController.getProfile("testDonor").getFirstName());
    }

    /**
     * Tests to see if a donor can be deleted from the roles pane.
     */
    @Ignore //we need to update this test to use the server and database
    @Test
    public void deleteDonor() {
        assertEquals(3, ProfileController.getProfiles().size());
        loginAsDefaultAdmin();
        switchToRolesTab();
        clickOn("#createDonorButton");
        clickOn("#usernameTextField").write("testDonor");
        clickOn("#firstNameTextField").write("testFirstName");
        clickOn("#createUserButton");
        press(KeyCode.ENTER);
        assertEquals(4, ProfileController.getProfiles().size());
        clickOn("#deleteDonorTextField").write("testDonor");
        clickOn("#deleteDonorButton");
        assertEquals(3, ProfileController.getProfiles().size());
    }

    /**
     * Creating an admin from the roles tab.
     *
     * @throws DoesNotExist in the case that defaultAdmin does not exist
     */
    @Test
    public void createAdmin() throws DoesNotExist {
        loginAsDefaultAdmin();
        switchToRolesTab();
        clickOn("#createAdminButton");
        clickOn("#usernameTextField").write("testAdmin");
        clickOn("#passwordTextField").write("testPassword");
        clickOn("#createAdminPaneButton");
        press(KeyCode.ENTER);
        release(KeyCode.ENTER);
        Admin admin = AdminRequests.getInstance()
                .getAdmin("testAdmin", AuthToken.getInstance().getToken()).getBody();
        assertEquals("testPassword", admin.getPassword());
    }

    /**
     * Deleting and admin from the roles pane.
     */
    @Test
    public void deleteAdmin() {
        loginAsDefaultAdmin();
        switchToRolesTab();
        clickOn("#createAdminButton");
        clickOn("#usernameTextField").write("testAdmin");
        clickOn("#passwordTextField").write("testPassword");
        clickOn("#createAdminPaneButton");
        press(KeyCode.ENTER);
        release(KeyCode.ENTER);

        Admin admin = AdminRequests.getInstance().getAdmin("testAdmin", AuthToken.getInstance().getToken()).getBody();
        assertEquals("testPassword", admin.getPassword());
        clickOn("#deleteAdminTextField").write("testAdmin");
        clickOn("#deleteAdminButton");

        try {
            AdminRequests.getInstance().getAdmin("testAdmin", AuthToken.getInstance().getToken());
        } catch (HttpClientErrorException e) {
            assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
        }


    }

    /**
     * User not found message.
     */
    @Ignore //we need to update this test to use the server and database
    @Test
    public void donorDeleteErrorMessage() {
        loginAsDefaultAdmin();
        switchToRolesTab();
        clickOn("#deleteDonorTextField").write("No Existing User"); // A user that doesn't exist
        clickOn("#deleteDonorButton");
        verifyThat("#donorErrorMsg", isVisible());
    }

    /**
     * Admin not found message.
     */
    @Test
    public void adminDeleteErrorMessage() {
        loginAsDefaultAdmin();
        switchToRolesTab();
        clickOn("#deleteAdminTextField").write("No Existing Admin"); // A user that doesn't exist
        clickOn("#deleteAdminButton");
        verifyThat("#adminErrorMsg", isVisible());
    }

    /**
     * Easy switch to rolesTab
     */
    private void switchToRolesTab() {
        clickOn("#rolesTab");
    }

    /**
     * Tests that the pressing undo will undo the last action for the usernameTextField.
     */
    @Test
    public void testUndoForUsernameTextField() {
        loginAsDefaultAdmin();
        switchToRolesTab();
        clickOn("#createAdminButton");
        clickOn("#usernameTextField");
        write("a");
        clickOn("#undoButton");
        TextField usernameTextField = lookup("#usernameTextField").query();
        assertEquals("", usernameTextField.getText());
    }

    /**
     * Tests that the pressing undo will undo the last action for the passwordTextField.
     */
    @Test
    public void testUndoForPasswordTextField() {
        loginAsDefaultAdmin();
        switchToRolesTab();
        clickOn("#createAdminButton");
        clickOn("#passwordTextField");
        write("a");
        clickOn("#undoButton");
        TextField passwordTextField = lookup("#passwordTextField").query();
        assertEquals("", passwordTextField.getText());
    }

    /**
     * Tests that the pressing redo will redo the last action for the usernameTextField.
     */
    @Test
    public void testRedoForUsernameTextField() {
        loginAsDefaultAdmin();
        switchToRolesTab();
        clickOn("#createAdminButton");
        clickOn("#usernameTextField");
        write("a");
        clickOn("#undoButton");
        clickOn("#redoButton");
        TextField usernameTextField = lookup("#usernameTextField").query();
        assertEquals("a", usernameTextField.getText());
    }

    /**
     * Tests that the pressing redo will redo the last action for the passwordTextField.
     */
    @Test
    public void testRedoForPasswordTextField() {
        loginAsDefaultAdmin();
        switchToRolesTab();
        clickOn("#createAdminButton");
        clickOn("#passwordTextField");
        write("a");
        clickOn("#undoButton");
        clickOn("#redoButton");
        TextField passwordTextField = lookup("#passwordTextField").query();
        assertEquals("a", passwordTextField.getText());
    }

    /**
     * Tests a mixture of undo and redo events over multiple fields to ensure it works robustly.
     */
    @Test
    public void testAMixtureOfUndoRedoOverMultipleFields() {
        loginAsDefaultAdmin();
        switchToRolesTab();
        clickOn("#createAdminButton");

        clickOn("#usernameTextField");
        write("aaaaa");
        clickOn("#undoButton");
        clickOn("#passwordTextField");
        write("bbb");
        clickOn("#undoButton");
        clickOn("#undoButton");
        clickOn("#undoButton");
        clickOn("#redoButton");
        clickOn("#usernameTextField");
        write("cc");
        clickOn("#undoButton");
        clickOn("#undoButton");
        clickOn("#redoButton");

        TextField usernameTextField = lookup("#usernameTextField").query();
        TextField passwordTextField = lookup("#passwordTextField").query();


        assertEquals("aaaac", usernameTextField.getText());
        assertEquals("b", passwordTextField.getText());
    }
}
