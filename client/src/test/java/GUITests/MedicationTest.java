package GUITests;

import javafx.scene.control.ListView;
import javafx.scene.input.KeyCode;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * A class to test medications
 */
@Ignore
public class MedicationTest extends TestFXBase {

    /**
     * Simulate a click on medications tab in profile.fxml
     */
    private void clickMedicationsTab() {
        clickOn("#medicationsTab");
    }

    /**
     * Add drug with a given string name
     *
     * @param drugName The drug name.
     */
    private void addDrug(String drugName) {
        loginAsProfileUser();
        clickMedicationsTab();
        clickOn("#medicationSearch");
        write(drugName);
        press(KeyCode.ENTER);
        clickOn("#newMedBtn");
        clickOn("#newMedBtn");
        clickOn("#newMedBtn");
    }

    /**
     * Test adding a drug with valid input
     */
    @Test
    @Ignore
    //socket timeout error
    public void addDrugSuccess() {
        addDrug("Abreva");
        ListView currentMedication = lookup("#currentMedication").query();
        assertTrue(currentMedication.getItems().contains("Abreva"));
    }

    /**
     * Test adding a drug with invalid input
     */
    @Test
    public void addDrugFailure() {
        addDrug("blah");
        ListView currentMedication = lookup("#currentMedication").query();
        assertFalse(currentMedication.getItems().contains("blah"));
    }

    /**
     * Move from current medications to previous medications
     */
    @Test
    public void moveDrugToPreviousMedications() {
        addDrug("Abreva");
        ListView currentMedication = lookup("#currentMedication").query();
        currentMedication.getSelectionModel().selectFirst();
        clickOn("#moveToPastMedBtn");
        assertFalse(currentMedication.getItems().contains("Abreva"));

    }

    /**
     * Move from current medications to previous medications
     */
    @Test
    public void moveDrugToCurrentMedication() {
//        release(new KeyCode[5]);
//        release(new MouseButton[5]);
        addDrug("Abreva");
        ListView currentMedication = lookup("#currentMedication").query();
        currentMedication.getSelectionModel().selectFirst();
        clickOn("#moveToPastMedBtn");
        sleep(1);
        ListView previousMedication = lookup("#previousMedication").query();
        previousMedication.getSelectionModel().selectFirst();
        clickOn("#moveToCurrentMedBtn");
        assertFalse(previousMedication.getItems().contains("Abreva"));

    }

    /**
     * Test delete from current medications
     */
    @Test
    public void deleteFromCurrentMedication() {
        addDrug("Abreva");
        ListView currentMedication = lookup("#currentMedication").query();
        currentMedication.getSelectionModel().selectFirst();
        clickOn("#deleteMedBtn");
        assertFalse(currentMedication.getItems().contains("Abreva"));
    }

    /**
     * Test delete from previous medications
     */
    @Test
    public void deleteFromPreviousMedication() {
        addDrug("Abreva");
        ListView currentMedication = lookup("#currentMedication").query();
        currentMedication.getSelectionModel().selectFirst();
        clickOn("#moveToPastMedBtn");
        ListView previousMedication = lookup("#previousMedication").query();
        previousMedication.getSelectionModel().selectFirst();
        clickOn("#deleteMedBtn");
        assertFalse(previousMedication.getItems().contains("Abreva"));
    }
}
