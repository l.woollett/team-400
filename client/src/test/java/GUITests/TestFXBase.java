package GUITests;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.DialogPane;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import org.junit.*;
import org.testfx.api.FxRobot;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit.ApplicationTest;
import seng302.CustomException.DoesNotExist;
import seng302.Enum.BloodTypeEnum;
import seng302.Enum.GenderEnum;
import seng302.Enum.OrganEnum;
import seng302.Enum.RegionEnum;
import seng302.Main.AppGui;
import seng302.Model.Admin;
import seng302.Model.Clinician;
import seng302.Model.Profile;
import seng302.ModelController.AdminController;
import seng302.ModelController.ClinicianController;
import seng302.ModelController.ProfileController;
import seng302.Utilities.DataInteraction;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeoutException;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * An abstract class, the parent of all Gui test classes.
 */
@Ignore
public abstract class TestFXBase extends ApplicationTest {
    private static final String DEFAULT_CLINICIAN_NAME = "defaultClinician";
    private static final String DEFAULT_ADMIN_NAME = "admin";
    private static final String testFileName = "testFXUserData.json";
    private static final Logger LOGGER = Logger.getLogger(TestFXBase.class.getName());
    String invalidCSSStyle = getClass().getResource("/seng302/Stylesheets/invalid.css").toExternalForm();
    private ArrayList<Profile> allProfileList = new ArrayList<>();
    private ArrayList<Admin> adminList = new ArrayList<>();
    private Clinician defaultClinician = null;
    private Admin defaultAdmin = null;
    private LocalDate dobDate = LocalDate.parse("02-02-1939", DateTimeFormatter.ofPattern("dd-MM-yyyy"));

    /**
     * Can not have a headful unit test on the server. The TestFx Robot can not move the mouse.
     */
    @BeforeClass
    public static void headlessSetUp() {
        System.setProperty("testfx.robot", "glass");
        System.setProperty("testfx.headless", "true");
        System.setProperty("prism.order", "sw");
        System.setProperty("prism.text", "t2k");
        System.setProperty("java.awt.headless", "true");
        System.setProperty("headless.geometry", "3000x3000-32");

        // Change the file name for testing so our production data does not get overridden.
        DataInteraction.setTestFileName(testFileName);
    }

    @AfterClass
    public static void fileTearDown() {
        try {
            Path path = FileSystems.getDefault().getPath(testFileName);
            Files.deleteIfExists(path);
            DataInteraction.setTestFileName("userdata.json");
        } catch (IOException e) {
            LOGGER.severe(e.getMessage());
        }
    }

    /**
     * Prepare testing resource
     */
    @Before
    public void Setup() {
        setupProfileList();
        setProfiles();
        setupDefaultAdmin();
    }

    /**
     * Clear out any key press or mouse move nodes and hide stage after the test completes
     *
     * @throws TimeoutException throws time out exception in order any thing got stuck
     */
    @After
    public void afterEachTest() throws TimeoutException {
        FxToolkit.cleanupStages();
        release(new KeyCode[]{});
        release(new MouseButton[]{});
        emptyProfileList();
        emptyDefaultClinician();
        emptyAdminList();
    }

    /**
     * Start test on the given scene - login.fxml
     *
     * @param stage, the current state to run
     */
    @Override
    public void start(Stage stage) {
        FXMLLoader loader = new FXMLLoader(AppGui.class.getResource("login.fxml"));
        Parent mainRoot = null;
        try {
            mainRoot = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (mainRoot != null) {
            Scene scene = new Scene(mainRoot);
            stage.setScene(scene);
            stage.show();
        }
    }

    /**
     * Set up a list of profiles for testing. The first record is a donor, the second record is a receiver.
     */
    private void setProfiles() {
        ProfileController.setProfiles(allProfileList);
        if (!allProfileList.isEmpty()) {
            ProfileController.setProfiles(allProfileList);
        }
        if (!adminList.isEmpty()) {
            AdminController.setAdmins(adminList);
        }
    }

    /**
     * Add two profiles to a list of profiles
     */
    private void setupProfileList() {
        Profile donor = new Profile("fName", "mName",
                "lName", dobDate, null,
                GenderEnum.MALE, 1.78f, 70f, BloodTypeEnum.A_NEGATIVE, "test address",
                RegionEnum.AUCKLAND, getCreatedDate(), null, true, getDonationOrganList(),
                "uName1", false, null);
        Profile receiver1 = new Profile("fNameName", "mNameName",
                "lNameName", dobDate, null,
                GenderEnum.FEMALE, 1.70f, 70, BloodTypeEnum.A_NEGATIVE, "test address",
                RegionEnum.AUCKLAND, getCreatedDate(), null, true, getDonationOrganList(),
                "uName2", true, getReceiveOrganList());
        Profile receiver2 = new Profile("fNameName", "mNameName",
                "lNameName", dobDate, null,
                GenderEnum.FEMALE, 1.70f, 70, BloodTypeEnum.A_NEGATIVE, "test address",
                RegionEnum.AUCKLAND, getCreatedDate(), null, true, getDonationOrganList(),
                "uName3", true, getReceiveOrganList());
        if (allProfileList.isEmpty()) {
            allProfileList.add(donor);
            allProfileList.add(receiver1);
            allProfileList.add(receiver2);
        }
    }

    /**
     * Set up created date
     *
     * @return the created date
     */
    private Date getCreatedDate() {
        String createdDateString = "2018-04-28";
        Date createdDateDate = null;
        try {
            createdDateDate = new SimpleDateFormat("yyyy-MM-dd").parse(createdDateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return createdDateDate;
    }

    private ArrayList<OrganEnum> getReceiveOrganList() {
        ArrayList<OrganEnum> organList = new ArrayList<>();
        organList.add(OrganEnum.CORNEA);
        organList.add(OrganEnum.LIVER);
        return organList;
    }

    /**
     * Clean out test data of a list of profiles set up in this test class
     */
    private void emptyProfileList() {
        ProfileController.setProfiles(new ArrayList<>());
    }

    /**
     * Clean out test data of admins.
     */
    private void emptyAdminList() {
        AdminController.setAdmins(new ArrayList<>());
    }

    /**
     * Set up a list of donated organs
     *
     * @return the list of donated organs
     */
    private ArrayList<OrganEnum> getDonationOrganList() {
        ArrayList<OrganEnum> donationOrganList = new ArrayList<>();
        donationOrganList.add(OrganEnum.LIVER);
        donationOrganList.add(OrganEnum.CORNEA);
        return donationOrganList;
    }

    /**
     * Sets up default admin for testing
     */
    private void setupDefaultAdmin() {
        AdminController.addDefaultAdmin();
    }

    /**
     * Get the default clinician for testing
     *
     * @return the default clinician
     */
    Clinician getDefaultClinician() {
        try {
            defaultClinician = ClinicianController.getClinician(DEFAULT_CLINICIAN_NAME);
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
        }
        return defaultClinician;
    }

    /**
     * Get the default admin for testing.
     *
     * @return the default admin.
     */
    Admin getDefaultAdmin() {
        try {
            defaultAdmin = AdminController.getAdmin(DEFAULT_ADMIN_NAME);
        } catch (DoesNotExist doesNotExist) {
            doesNotExist.printStackTrace();
        }
        return defaultAdmin;
    }

    /**
     * Get the list of existing profile for testing
     *
     * @return the list of existing donors
     */
    ArrayList<Profile> getAllProfileList() {
        return allProfileList;
    }

    /**
     * Clean out the default clinician
     */
    private void emptyDefaultClinician() {
        defaultClinician = null;
    }

    /**
     * Helper method for simulate a user clicks on Edit button as a clinician
     */
    void clickOnEdit() {
        clickOn("#fileMenu");
        clickOn("#editMenuItem");
    }

    /**
     * Helper method for simulate a user clicks on Edit button as a profile
     */
    void clickOnProfileEdit() {
        clickOn("#donorFileMenu");
        clickOn("#editMenuItem");
    }

    /**
     * Clears the entire selected field.
     */
    void clearField() {
        push(KeyCode.CONTROL, KeyCode.A).push(KeyCode.DELETE);
    }

    /**
     * Login as a clinician
     */
    void loginAsDefaultClinicianUser() {
        String profileLoginName = "defaultClinician";
        clickOn("#usernameTxtField").write(profileLoginName);
        clickOn("#loginBtn");
    }

    /**
     * Login as a user
     */
    void loginAsProfileUser() {
        String profileLoginName = getAllProfileList().get(0).getUsername();
        clickOn("#usernameTxtField").write(profileLoginName);
        clickOn("#loginBtn");
    }

    /**
     * Private helper method to test alert dialog
     *
     * @param expectedHeader,  the header message of the alert dialog
     * @param expectedContent, the content message of the alert dialog
     */
    void alertDialogHasHeaderAndContent(final String expectedHeader, final String expectedContent) {
        final javafx.stage.Stage actualAlertDialog = getTopModalStage();
        assertNotNull(actualAlertDialog);

        final DialogPane dialogPane = (DialogPane) actualAlertDialog.getScene().getRoot();
        if (expectedHeader != null) {
            assertEquals(expectedHeader, actualAlertDialog.getTitle());
        }
        assertEquals(expectedContent, dialogPane.getContentText());
    }

    /**
     * Private helper method, to find the stage of the alert dialog is running. Get a list of windows but ordered from
     * top[0] to bottom[n] ones. It is needed to get the first found modal window.
     *
     * @return the stage is running the alert dialog that needs to test with
     */
    private javafx.stage.Stage getTopModalStage() {
        FxRobot robot = new FxRobot();
        final List<Window> allWindows = new ArrayList<>(robot.robotContext().getWindowFinder().listWindows());
        Collections.reverse(allWindows);

        return (javafx.stage.Stage) allWindows
                .stream()
                .filter(window -> window instanceof javafx.stage.Stage)
                .filter(window -> ((javafx.stage.Stage) window).getModality() == Modality.APPLICATION_MODAL)
                .findFirst()
                .orElse(null);
    }
}
