package GUITests;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import seng302.Enum.GenderEnum;
import seng302.Enum.RegionEnum;
import seng302.GUI.LoginSceneController;
import seng302.GUI.Profile.ProfileSceneController;
import seng302.Model.Clinician;
import seng302.Model.Hospital;
import seng302.Model.Profile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Date;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

@Ignore
public class UndoRedoMedicationTabTest extends TestFXBase {


    private ProfileSceneController controller;
    private Button undo, redo;
    private ListView<String> currentMedList;
    private ListView<String> pastMedList;
    private TextField medicationSearch;
    private Button newMedBtn, deleteMedBtn, moveToCurrentMedBtn, moveToPastMedBtn;

    @Override
    public void start(Stage stage) {
        FXMLLoader loader = new FXMLLoader(ProfileSceneController.class.getResource("profile.fxml"));
        Parent mainRoot = null;
        try {
            mainRoot = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (mainRoot != null) {
            Scene scene = new Scene(mainRoot);
            stage.setScene(scene);
            stage.show();
        }
        controller = loader.getController();
        Profile testProfile = new Profile("test", LocalDate.of(1994, 1, 1), new Date(), "test");
        controller.initParentStageReference();
        controller.setCurrentProfile(testProfile);
        LoginSceneController.setActiveClinician(new Clinician("test", "test", "test", "test", "test",
                RegionEnum.CANTERBURY, GenderEnum.MALE, new Date(), LocalDate.of(195, 1, 1),
                new Hospital("Hospital Name", RegionEnum.CANTERBURY, "Hospital Address", 0, 0), "test"));
    }

    @Before
    public void setup() {
        clickOn("#medicationsTab");
        currentMedList = lookup("#currentMedication").query();
        pastMedList = lookup("#previousMedication").query();
        medicationSearch = lookup("#medicationSearch").query();
        controller.getUndoRedoController().reset();
        undo = lookup("#undo").query();
        redo = lookup("#redo").query();
        newMedBtn = lookup("#newMedBtn").query();
        deleteMedBtn = lookup("#deleteMedBtn").query();
        moveToCurrentMedBtn = lookup("#moveToCurrentMedBtn").query();
        moveToPastMedBtn = lookup("#moveToPastMedBtn").query();

    }

    /**
     * Test the undo/redo functionality for the dual lists in the medications tab for the Profile scene
     */
    @Test
    public void medicationDualListTest() {
        medicationSearch.setText("Abreva");
        sleep(600);
        press(KeyCode.ENTER);
        clickOn("#newMedBtn");
        assertTrue(currentMedList.getItems().contains("Abreva"));
        assertTrue(controller.getCurrentProfile().getCurrentMedications().contains("Abreva"));
        undo.fire();
        assertFalse(controller.getCurrentProfile().getCurrentMedications().contains("Abreva"));
        assertFalse(currentMedList.getItems().contains("Abreva"));
        redo.fire();
        assertTrue(currentMedList.getItems().contains("Abreva"));
        assertTrue(controller.getCurrentProfile().getCurrentMedications().contains("Abreva"));
        currentMedList.getSelectionModel().selectFirst();
        moveToPastMedBtn.fire();
        undo.fire();
        assertFalse(pastMedList.getItems().contains("Abreva"));
        assertTrue(currentMedList.getItems().contains("Abreva"));
        redo.fire();
        assertTrue(pastMedList.getItems().contains("Abreva"));
        pastMedList.getSelectionModel().selectFirst();
        moveToCurrentMedBtn.fire();
        undo.fire();
        assertFalse(currentMedList.getItems().contains("Abreva"));
        assertTrue(pastMedList.getItems().contains("Abreva"));
        redo.fire();
        assertFalse(pastMedList.getItems().contains("Abreva"));
        assertTrue(currentMedList.getItems().contains("Abreva"));
        currentMedList.getSelectionModel().selectFirst();
        deleteMedBtn.fire();
        undo.fire();
        assertTrue(currentMedList.getItems().contains("Abreva"));
        assertFalse(pastMedList.getItems().contains("Abreva"));
        redo.fire();
        assertFalse(currentMedList.getItems().contains("Abreva"));
        assertFalse(pastMedList.getItems().contains("Abreva"));
        assertFalse(controller.getCurrentProfile().getCurrentMedications().contains("Abreva"));
    }
}

