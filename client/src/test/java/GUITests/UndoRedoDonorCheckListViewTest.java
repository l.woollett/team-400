package GUITests;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import seng302.Enum.OrganEnum;
import seng302.GUI.Profile.ProfileSceneController;
import seng302.Model.Profile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.*;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@Ignore
public class UndoRedoDonorCheckListViewTest extends TestFXBase {

    @FXML
    private ProfileSceneController controller;
    private Button undo, redo;
    private Map<OrganEnum, ObservableValue<Boolean>> organChecklist = new HashMap<>();

    @Override
    public void start(Stage stage) {
        FXMLLoader loader = new FXMLLoader(ProfileSceneController.class.getResource("profile.fxml"));
        Parent mainRoot = null;
        try {
            mainRoot = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (mainRoot != null) {
            Scene scene = new Scene(mainRoot);
            stage.setScene(scene);
            stage.show();
        }
        controller = loader.getController();
        Profile testProfile = new Profile("test", LocalDate.of(1994, 1, 1), new Date(), "test");
        controller.initParentStageReference();
        testProfile.setIsDonor(true);
        List<OrganEnum> donorOrgans = new ArrayList<>();
        donorOrgans.add(OrganEnum.LUNG);
        testProfile.setDonorOrgans(donorOrgans);
        controller.setCurrentProfile(testProfile);
        organChecklist = controller.getDonorAnchorPaneController().getOrganChecklist();
    }

    @Before
    public void setup() {
        clickOn("#donationTab");
        clickOn("#donorFileMenu");
        clickOn("#editMenuItem");
        controller.getUndoRedoController().reset();
        undo = lookup("#undo").query();
        redo = lookup("#redo").query();
    }

    @Test
    public void editListEventUndoRedoTest() {
        SimpleBooleanProperty property = (SimpleBooleanProperty) organChecklist.get(OrganEnum.LUNG);
        property.setValue(false);
        assertFalse(organChecklist.get(OrganEnum.LUNG).getValue());
        undo.fire();
        assertTrue(organChecklist.get(OrganEnum.LUNG).getValue());
        redo.fire();
        assertFalse(organChecklist.get(OrganEnum.LUNG).getValue());
    }
}
