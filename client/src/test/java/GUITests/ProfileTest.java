package GUITests;

import javafx.geometry.VerticalDirection;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.text.Text;
import org.junit.Ignore;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.testfx.api.FxAssert.verifyThat;
import static org.testfx.matcher.base.NodeMatchers.isVisible;
import static org.testfx.matcher.control.TextMatchers.hasText;

/**
 * A class to test with existing user, with positive and negative tests
 */
@Ignore
public class ProfileTest extends TestFXBase {

    /**
     * Test with a valid profile name. The value has been set up in the parent class - TextFxBase
     */
    @Test
    public void loginAsProfileUserSuccessTest() {
        assertEquals(getAllProfileList().get(0).getUsername(), "uName1");
        assertEquals(getAllProfileList().get(1).getUsername(), "uName2");
        String profileLoginName = getAllProfileList().get(0).getUsername();

        clickOn("#usernameTxtField").write(profileLoginName);
        clickOn("#loginBtn");
        verifyThat("#mainAnchorPane", isVisible());
        verifyThat("#userName", hasText("uName1"));
        verifyThat("#firstName", hasText("fName"));
        verifyThat("#middleName", hasText("mName"));
        verifyThat("#gender", hasText("Male"));
        verifyEmptyHistoryTextArea();
    }

    /**
     * Test with an invalid profile name. The value has not been set up in the parent class - TextFxBase
     */
    @Test
    public void loginAsProfileUserFailTest() {
        assertEquals(getAllProfileList().size(), 3);
        clickOn("#usernameTxtField").write("testName");
        clickOn("#loginBtn");
        Text errorMsg = lookup("#loginErrorTxt").query();
        verifyThat("#loginPane", isVisible());
        assertEquals(getAllProfileList().size(), 3);
        assertEquals(errorMsg.getText(), "Invalid Login");
        assertEquals(getAllProfileList().size(), 3);
    }

    /**
     * Test with update valid values after a user login without organs donation
     */
    @Test
    public void updateProfileUserWithoutOrganDonationSuccessTest() {
        loginAsProfileUserSuccessTest();
        clickOnProfileEdit();
        clickOn("#firstNameEdit");
        clearField();
        write("fName");
        clickOn("#middleNameEdit");
        clearField();
        write("mName");
        clickOn("#lastNameEdit");
        clearField();
        write("lName");
        clickOn("#dateOfBirthPicker");
        clearField();
        write("1/10/1998");
        clickOn("#birthGenderChoiceBox").write("FEMALE");
        clickOn("#bloodTypeChoiceBox").write("A+");
        clickOn("#btnRegisterDonor").press(KeyCode.ENTER);
        String today = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
        clickOn("#dateOfDeathPicker");
        clearField();
        write(today);
        press(KeyCode.TAB);
        release(KeyCode.TAB);
        clickOn("#weightEdit");
        clearField();
        write("70.09");
        press(KeyCode.TAB);
        release(KeyCode.TAB);
        write("86/120");
        press(KeyCode.TAB);
        release(KeyCode.TAB);
        write("Upper Riccarton");
        press(KeyCode.TAB);
        release(KeyCode.TAB);
        clickOn("#heightEdit");
        clearField();
        write("1.68");
        press(KeyCode.TAB);
        release(KeyCode.TAB);
        clickOn("#smokerCheckBox").press(KeyCode.ENTER);
        press(KeyCode.TAB);
        release(KeyCode.TAB);
        clickOn("#alcoholConsumptionChoiceBox").clickOn("NONE");
        press(KeyCode.TAB);
        release(KeyCode.TAB);
        scroll(5, VerticalDirection.DOWN);
        clickOn("#regionChoiceBox").clickOn("Auckland");
        clickOn("#applyEditButton");

        DatePicker dob = lookup("#dateOfBirthPicker").query();
        TextField weightEdit = lookup("#weightEdit").query();

        TextField heightEdit = lookup("#heightEdit").query();
        TextField firstNameEdit = lookup("#firstNameEdit").query();
        TextField middleNameEdit = lookup("#middleNameEdit").query();
        TextField lastNameEdit = lookup("#lastNameEdit").query();
        TextField bloodPressureEdit = lookup("#bloodPressureEdit").query();
        TextField addressEdit = lookup("#addressEdit").query();
        ChoiceBox regionChoiceBox = lookup("#regionChoiceBox").query();

        assertEquals(weightEdit.getText(), String.valueOf(getAllProfileList().get(0).getWeight()));
        assertEquals(dob.getValue(), getAllProfileList().get(0).getDateOfBirth());
        assertEquals(heightEdit.getText(), String.valueOf(getAllProfileList().get(0).getHeight()));
        assertEquals(firstNameEdit.getText(), getAllProfileList().get(0).getFirstName());
        assertEquals(middleNameEdit.getText(), getAllProfileList().get(0).getMiddleName());
        assertEquals(lastNameEdit.getText(), getAllProfileList().get(0).getLastName());
        assertEquals(bloodPressureEdit.getText(), String.valueOf(getAllProfileList().get(0).getBloodPressure()));
        assertEquals(addressEdit.getText(), getAllProfileList().get(0).getAddress());
        assertEquals(regionChoiceBox.getValue(), getAllProfileList().get(0).getRegion());

        verifyHistoryTextArea();
    }

    /**
     * Test invalid first name, height, weight update
     */
    @Test
    public void updateProfileUserWithInvalidValueUpdateTest() {
        loginAsProfileUserSuccessTest();
        clickOnProfileEdit();
        clickOn("#firstNameEdit");
        clearField();
        write("fName ba12 -");
        TextField firstNameEdit = lookup("#firstNameEdit").query();
        clickOn("#weightEdit");
        clearField();
        write("20");
        TextField weightEdit = lookup("#weightEdit").query();
        clickOn("#heightEdit");
        clearField();
        write("30");
        TextField heightEdit = lookup("#heightEdit").query();
        clickOn("#applyEditButton");

        assertEquals(firstNameEdit.getStylesheets().contains(invalidCSSStyle), true);
        assertEquals(weightEdit.getStylesheets().contains(invalidCSSStyle), true);
        assertEquals(heightEdit.getStylesheets().contains(invalidCSSStyle), true);
    }

    /**
     * Helper method for verify the result in the history tab
     */
    private void verifyHistoryTextArea() {
        TextArea historyTextArea = lookup("#historyTextArea").query();
        assertTrue(!historyTextArea.getParagraphs().isEmpty());
    }

    /**
     * Helper method for verify the result in the history tab should be empty
     */
    private void verifyEmptyHistoryTextArea() {
        TextArea historyTextArea = lookup("#historyTextArea").query();
        assertTrue(historyTextArea.getParagraphs().size() == 1);
    }

    /**
     * Tests changing to one tab from another and then using undo to return to the original tab.
     */
    @Test
    public void testSingleUndoForTabSwap() {
        loginAsProfileUser();

        clickOn("#medicationsTab");

        TabPane tabPane = lookup("#profileTabPane").query();

        clickOn("#undo");
        assertEquals(0, tabPane.getSelectionModel().getSelectedIndex());
    }

    /**
     * Tests clicking multiple unique tabs and then using undo to go back to the first tab clicked.
     */
    @Test
    public void testMultipleUndoTabSwapInSequence() {
        loginAsProfileUser();

        clickOn("#medicationsTab");
        clickOn("#historyTab");
        clickOn("#medicalHistoryTab");
        clickOn("#procedureHistoryTab");

        TabPane tabPane = lookup("#profileTabPane").query();

        clickOn("#undo");
        assertEquals(5, tabPane.getSelectionModel().getSelectedIndex());
        clickOn("#undo");
        assertEquals(4, tabPane.getSelectionModel().getSelectedIndex());
        clickOn("#undo");
        assertEquals(3, tabPane.getSelectionModel().getSelectedIndex());
        clickOn("#undo");
        assertEquals(0, tabPane.getSelectionModel().getSelectedIndex());
    }

    /**
     * Tests clicking multiple unique tabs and then using undo to go back to the first tab clicked.
     */
    @Test
    public void testMultipleUndoTabSwapInArbitraryOrder() {
        loginAsProfileUser();

        clickOn("#historyTab");
        clickOn("#medicationsTab");
        clickOn("#procedureHistoryTab");
        clickOn("#medicationsTab");
        clickOn("#historyTab");
        clickOn("#medicalHistoryTab");

        TabPane tabPane = lookup("#profileTabPane").query();

        clickOn("#undo");
        assertEquals(4, tabPane.getSelectionModel().getSelectedIndex());
        clickOn("#undo");
        assertEquals(3, tabPane.getSelectionModel().getSelectedIndex());
        clickOn("#undo");
        assertEquals(6, tabPane.getSelectionModel().getSelectedIndex());
        clickOn("#undo");
        assertEquals(3, tabPane.getSelectionModel().getSelectedIndex());
        clickOn("#undo");
        assertEquals(4, tabPane.getSelectionModel().getSelectedIndex());
        clickOn("#undo");
        assertEquals(0, tabPane.getSelectionModel().getSelectedIndex());
    }

    /**
     * Test to make sure clicking the same tab twice in a row does not push two undoable events to the
     * UndoRedoController.
     */
    @Test
    public void testClickingSameTabTwiceDoesNotPushTwoUndoEvents() {
        loginAsProfileUser();

        clickOn("#historyTab");
        clickOn("#historyTab");

        clickOn("#undo");

        TabPane tabPane = lookup("#profileTabPane").query();

        clickOn("#undo");
        assertEquals(0, tabPane.getSelectionModel().getSelectedIndex());
    }

    /**
     * Tests that a singe redo works correctly.
     */
    @Test
    public void testSingleRedo() {
        loginAsProfileUser();

        clickOn("#historyTab");
        clickOn("#undo");

        TabPane tabPane = lookup("#profileTabPane").query();

        clickOn("#redo");
        assertEquals(4, tabPane.getSelectionModel().getSelectedIndex());
    }

    /**
     * Test that multiple redo's in a row works correctly.
     */
    @Test
    public void testMultipleRedoInARow() {
        loginAsProfileUser();

        clickOn("#medicationsTab");
        clickOn("#historyTab");
        clickOn("#medicalHistoryTab");
        clickOn("#procedureHistoryTab");

        clickOn("#undo");
        clickOn("#undo");
        clickOn("#undo");
        clickOn("#undo");

        TabPane tabPane = lookup("#profileTabPane").query();

        clickOn("#redo");
        assertEquals(3, tabPane.getSelectionModel().getSelectedIndex());
        clickOn("#redo");
        assertEquals(4, tabPane.getSelectionModel().getSelectedIndex());
        clickOn("#redo");
        assertEquals(5, tabPane.getSelectionModel().getSelectedIndex());
        clickOn("#redo");
        assertEquals(6, tabPane.getSelectionModel().getSelectedIndex());
    }

    /**
     * Tests that very quick doing an undo and redo in quick succession does not break the design.
     */
    @Test
    public void undoRedoAlternationStressTest() {
        loginAsProfileUser();

        clickOn("#medicationsTab");

        for (int i = 0; i < 6; i++) {
            undoAndThenRedo();
        }

        TabPane tabPane = lookup("#profileTabPane").query();

        assertEquals(3, tabPane.getSelectionModel().getSelectedIndex());
    }

    /**
     * Clicks the undo button and then the redo button.
     */
    private void undoAndThenRedo() {
        clickOn("#undo");
        clickOn("#redo");
    }

    /**
     * Tests swapping between tabs and clicking undo/redo in quick succession does not break the design.
     */
    @Test
    public void alternatingBetweenUndoAndTabClickingStressTest() {
        loginAsProfileUser();

        clickOn("#medicationsTab");
        clickOn("#undo");
        clickOn("#procedureHistoryTab");
        clickOn("#historyTab");
        clickOn("#undo");
        clickOn("#historyTab");
        clickOn("#undo");
        clickOn("#redo");

        TabPane tabPane = lookup("#profileTabPane").query();

        assertEquals(4, tabPane.getSelectionModel().getSelectedIndex());
    }
}
