package GUITests;

import javafx.scene.Node;
import javafx.scene.control.TableView;
import javafx.scene.text.Text;
import org.junit.Ignore;
import org.junit.Test;
import org.loadui.testfx.GuiTest;
import seng302.Enum.OrganEnum;
import seng302.Model.Profile;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@Ignore
public class OrganReceiverAndDonorTest extends TestFXBase {
    /**
     * Simulate a click on Search tab after login as a clinician
     */
    private void clickSearchTab() {
        clickOn("#searchTab");
    }


    /**
     * Simulate a click on button organ receiver status after login as a clinician
     */
    private void clickOrganReceiverStatusButton() {
        clickOn("#btnRegisterReceiver");
    }

    /**
     * Simulate a click on button apply changes after login as a clinician
     */
    private void clickApplyChangesButton() {
        clickOn("#applyEditButton");
    }

    /**
     * Helper method to log in as a profile with receiver organs registered
     */
    private void loginAsReceiverProfileUser() {
        String profileLoginName = getAllProfileList().get(2).getUsername();
        clickOn("#usernameTxtField").write(profileLoginName);
        clickOn("#loginBtn");
    }

    /**
     * Test on unregister an organ receiver to see expected behaviors
     */
    @Test
    @Ignore
    public void unregisterOranReceiverSuccessfulTest() {
        loginAsDefaultClinicianUser();
        clickSearchTab();
        doubleClickOn((Node) lookup("#clinicianSearchTable").lookup(".table-row-cell").nth(0).query());
        sleep(500);
        clickOnProfileEdit();
        clickOrganReceiverStatusButton();
        clickApplyChangesButton();
        Node receiverTab = GuiTest.find("#receiverTab");
        assertFalse(receiverTab.isDisable());
        Text diseaseName = lookup("#receiverStatusText").query();
        assertTrue(diseaseName.getText().equals("Not Registered"));
    }

    /**
     * Test on register an organ receiver to see expected behaviors
     */
    @Test
    public void registerOrganReceiverSuccessfulTest() {
        loginAsDefaultClinicianUser();
        clickSearchTab();
        doubleClickOn((Node) lookup("#clinicianSearchTable").lookup(".table-row-cell").nth(1).query());
        TableView receivingOrganList = lookup("#receivingOrganList").query();

        assertTrue(receivingOrganList.getItems().size() == 2);
        Profile profile2 = getAllProfileList().get(1);
        profile2.addReceivingOrgan(OrganEnum.BONE_MARROW);
        assertTrue(profile2.getReceiverOrgans().size() == 3);
    }


    /**
     * Test on an existing registered profile to see expected behaviors
     */
    @Test
    public void viewRegisterOrganAsProfileTest() {
        loginAsReceiverProfileUser();
        Text diseaseName = lookup("#receiverStatusText").query();
        assertTrue(diseaseName.getText().equals("Registered Receiver"));
        clickOn("#receiverTab");
        TableView receivingOrganListItems = lookup("#receivingOrganList").query();
        assertTrue(receivingOrganListItems.getItems().size() == 2);
    }

    /**
     * Test on an existing unregistered profile to see expected behaviors
     */
    @Test
    public void viewNoRegisterOrganAsProfileTest() {
        loginAsProfileUser();
        Text diseaseName = lookup("#receiverStatusText").query();
        assertTrue(diseaseName.getText().equals("Not Registered"));
        clickOn("#receiverTab");
        TableView receivingOrganListItems = lookup("#receivingOrganList").query();
        assertTrue(receivingOrganListItems.getItems().size() == 0);
    }


}
