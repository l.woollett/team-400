package GUITests;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import seng302.Enum.GenderEnum;
import seng302.Enum.OrganEnum;
import seng302.Enum.RegionEnum;
import seng302.GUI.LoginSceneController;
import seng302.GUI.Profile.ProfileSceneController;
import seng302.Model.Clinician;
import seng302.Model.Hospital;
import seng302.Model.Profile;
import seng302.Model.ReceiverOrgan;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@Ignore
public class UndoRedoReceiverCheckTableViewTest extends TestFXBase {

    @FXML
    private ProfileSceneController controller;
    private Button undo, redo;
    private TableColumn<ReceiverOrgan, Boolean> checkBoxColumn;
    private TableView<ReceiverOrgan> tableView;


    @Override
    public void start(Stage stage) {
        FXMLLoader loader = new FXMLLoader(ProfileSceneController.class.getResource("profile.fxml"));
        Parent mainRoot = null;
        try {
            mainRoot = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (mainRoot != null) {
            Scene scene = new Scene(mainRoot);
            stage.setScene(scene);
            stage.show();
        }
        controller = loader.getController();
        Profile testProfile = new Profile("test", LocalDate.of(1994, 1, 1), new Date(), "test");
        controller.initParentStageReference();
        testProfile.setIsReceiver(true);
        List<OrganEnum> receiverOrgans = new ArrayList<>();
        receiverOrgans.add(OrganEnum.LIVER);
        testProfile.setReceiverOrgans(receiverOrgans);
        controller.setCurrentProfile(testProfile);
        LoginSceneController.setActiveClinician(new Clinician("test", "test", "test", "test", "test",
                RegionEnum.CANTERBURY, GenderEnum.MALE, new Date(), LocalDate.of(195, 1, 1),
                new Hospital("Hospital Name", RegionEnum.CANTERBURY, "Hospital Address", 0, 0), "test"));
    }

    @Before
    public void setup() {
        clickOn("#receiverTab");
        clickOn("#donorFileMenu");
        clickOn("#editMenuItem");
        tableView = controller.getOrganReceiverAnchorPaneController().getReceivingOrganList();
        checkBoxColumn = controller.getOrganReceiverAnchorPaneController().getCheckBoxColumn();
        controller.getUndoRedoController().reset();
        undo = lookup("#undo").query();
        redo = lookup("#redo").query();
    }

    @Test
    @Ignore //Completely changed implementation
    public void editCheckTableViewEditUndoRedoTest() {
        SimpleBooleanProperty testBool = new SimpleBooleanProperty(false);
        for (ReceiverOrgan organ : tableView.getItems()) {
            if (organ.getRegisteredOrgan().equals(OrganEnum.LIVER)) {
                testBool = (SimpleBooleanProperty) checkBoxColumn.getCellObservableValue(organ);
            }
        }
        testBool.setValue(false);
        undo.fire();
        assertTrue(testBool.getValue());
        redo.fire();
        assertFalse(testBool.getValue());
    }

}
