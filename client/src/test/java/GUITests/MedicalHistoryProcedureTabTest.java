package GUITests;

import javafx.scene.Node;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseButton;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import seng302.Model.Procedure;
import seng302.Model.Profile;
import seng302.ModelController.ProfileController;

import java.time.LocalDate;
import java.util.Date;

import static org.junit.Assert.assertEquals;

@Ignore
public class MedicalHistoryProcedureTabTest extends TestFXBase {

    @Before
    public void navigateToProcedureTab() {
        Profile profile = new Profile("Kirill", LocalDate.parse("1997-02-20"), new Date(), "kirrilian");
        ProfileController.addProfile("kirrilian", profile);

        String defaultClinicianLoginName = getDefaultClinician().getUsername();
        clickOn("#usernameTxtField").write(defaultClinicianLoginName);
        clickOn("#loginBtn");
        clickOn("#searchTab");
        clickOn("#clinicianSearchTextField");
        write("Kirill");
        //sleep(5000);
        doubleClickOn((Node) lookup("#clinicianSearchTable").lookup(".table-row-cell").nth(0).query());
        release(new MouseButton[0]);
        clickOn("#procedureHistoryTab");
    }

    /**
     * See if the clinician can add a procedure.
     */
    @Test
    @Ignore
    public void addPastProcedure() {
        clickOn("#summaryInput").write("Heart Transplant");
        DatePicker procedureDateInput = lookup("#procedureDateInput").query();
        procedureDateInput.setValue(LocalDate.parse("1997-09-30"));
        clickOn("#descriptionInput").write("Patient will have a heart transplant");
        clickOn("#addProcedure");
        TableView<Procedure> tableView = lookup("#pastProceduresTableView").query();
        Procedure procedure = tableView.getItems().get(0);
        assertEquals("Heart Transplant", procedure.getSummary());
        assertEquals("Patient will have a heart transplant", procedure.getDescription());
        assertEquals(LocalDate.parse("1997-09-30"), procedure.getDateOfProcedure());
        assertEquals(0, procedure.getAffectedOrgans().size());
    }

    /**
     * See if the clinician can add a procedure.
     */
    @Test
    public void addPendingProcedure() {
        clickOn("#summaryInput").write("Heart Transplant");
        DatePicker procedureDateInput = lookup("#procedureDateInput").query();
        LocalDate date = LocalDate.now();
        LocalDate expectedDate = LocalDate.now();
        date.plusYears(1);
        expectedDate.plusYears(1);
        procedureDateInput.setValue(date);
        clickOn("#descriptionInput").write("Patient will have a heart transplant");
        clickOn("#addProcedure");

        TableView<Procedure> tableView = lookup("#pendingProceduresTableView").query();
        Procedure procedure = tableView.getItems().get(0);

        assertEquals("Heart Transplant", procedure.getSummary());
        assertEquals("Patient will have a heart transplant", procedure.getDescription());
        assertEquals(expectedDate, procedure.getDateOfProcedure());
        assertEquals(0, procedure.getAffectedOrgans().size());
    }
}
