package GUITests;

import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import seng302.Model.Disease;
import seng302.Utilities.ProfileDisplayUtility;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDate;

import static org.junit.Assert.*;
import static org.testfx.api.FxAssert.verifyThat;

@Ignore
public class MedicalHistoryDiseaseTabTest extends TestFXBase {

    /**
     * Login the page as a default Clinician
     */
    private void loginAsDefaultClinician() {
        String defaultClinicianLoginName = getDefaultClinician().getUsername();
        if (defaultClinicianLoginName != null) {
            clickOn("#usernameTxtField").write(defaultClinicianLoginName);
            clickOn("#loginBtn");
        }
    }

    private void openMedicalHistoryFromClinician() {
        clickOn("#searchTab");
        clickOn("#clinicianSearchTextField");
        write("fname mname");

        doubleClickOn((Node) lookup("#clinicianSearchTable").lookup(".table-row-cell").nth(0).query());
        release(new MouseButton[0]);
        clickOn("#medicalHistoryTab");
    }

    private void addNewDisease(String diseaseName, String diseaseDate, boolean isChronic) {
        clickOn("#diseaseNameTextField");
        write(diseaseName);
        clickOn("#diseaseDatePicker");
        clearField();
        write(diseaseDate);
        if (isChronic) {
            clickOn("#isChronicDiseaseCheckBox");
        }
        clickOn("#addNewDiseaseButton");
    }

    /**
     * Opens the update disease dialog for the index of the item in the table
     *
     * @param index the index of the item(row) which needs to have the update dialog opened on.
     */
    private void showUpdateDiseaseDialog(int index) {
        clickOn((Node) lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(index).query());
        rightClickOn((Node) lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(index).query());
        rightClickOn((Node) lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(index).query());
        clickOn((Node) lookup(".menu-item").nth(0).query());
    }

    @Before
    public void setUp() {
        loginAsDefaultClinician();
        openMedicalHistoryFromClinician();
    }

    @After
    public void tearTown() {
        try {
            Method clearOpenedStagesRecord = ProfileDisplayUtility.class.getDeclaredMethod("clearStageRecords");
            clearOpenedStagesRecord.setAccessible(true);
            clearOpenedStagesRecord.invoke(ProfileDisplayUtility.class);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void navigateToMedicalHistoryTabPass() {
        verifyThat("#mainDiseaseAnchorPane", Node::isVisible);
    }

    @Test
    public void currentDiseasesTableVisiblePass() {
        verifyThat("#currentDiseaseTableView", Node::isVisible);
    }

    @Test
    public void pastDiseasesTableVisiblePass() {
        verifyThat("#pastDiseaseTableView", Node::isVisible);
    }

    @Test
    public void datePickerIsSetToTodayOnLaunchPass() {
        DatePicker diseaseDatePicker = lookup("#diseaseDatePicker").query();
        assertTrue(diseaseDatePicker.getValue().equals(LocalDate.now()));
    }

    @Test
    public void addDiseaseToCurrentDiseasesPass() {
        clickOn("#diseaseNameTextField");
        write("Test Disease");
        clickOn("#addNewDiseaseButton");
        TableView currentDiseaseTable = lookup("#currentDiseaseTableView").query();
        assertTrue(currentDiseaseTable.getItems().contains(new Disease("Test Disease", false, null)));
        verifyThat("#mainDiseaseAnchorPane", Node::isVisible);
    }

    @Test
    public void addDiseaseFailDialogPopUp() {
        clickOn("#addNewDiseaseButton");
        alertDialogHasHeaderAndContent("Error",
                "You must complete the required fields marked in red to create a new disease.");
        press(KeyCode.ENTER);
        release(KeyCode.ENTER);
        TableView currentDiseaseTable = lookup("#currentDiseaseTableView").query();

        assertFalse(currentDiseaseTable.getItems().contains(new Disease("Not Test Disease", false, null)));
    }

    /**
     * The diseases should be ordered automatically by chronic status and then by date in descending order. Their
     * chronic status has higher priority over the date of diagnosis.
     * <p>
     * Disease should be added to the top of the table as the dates of diagnosis are getting more recent per add.
     */
    @Test
    @Ignore
    public void addDiseaseTestAllChronicDefaultSortAddInReverse() {
        addNewDisease("cancer", "17/04/2018", true);
        addNewDisease("cancer2", "18/04/2018", true);
        addNewDisease("cancer3", "19/04/2018", true);
        TableRow tr0 = lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(0).query();
        assertTrue(tr0.getItem().equals(new Disease("cancer3", true, LocalDate.parse("2018-04-19"))));
        TableRow tr1 = lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(1).query();
        assertTrue(tr1.getItem().equals(new Disease("cancer2", true, LocalDate.parse("2018-04-18"))));
        TableRow tr2 = lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(2).query();
        assertTrue(tr2.getItem().equals(new Disease("cancer", true, LocalDate.parse("2018-04-17"))));
    }

    /**
     * The diseases should be ordered automatically by chronic status and then by date in descending order. Their
     * chronic status has higher priority over the date of diagnosis.
     * <p>
     * Disease should be added to the top of the table as the dates of diagnosis are getting more recent per add.
     */
    @Test
    @Ignore
    public void addNonChronicDiseaseTestDefaultSortAddInReverse() {
        addNewDisease("cancer", "17/04/2018", false);
        addNewDisease("cancer2", "18/04/2018", false);
        addNewDisease("cancer3", "19/04/2018", false);
        TableRow tr0 = lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(0).query();
        assertTrue(tr0.getItem().equals(new Disease("cancer3", false, LocalDate.parse("2018-04-19"))));
        TableRow tr1 = lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(1).query();
        assertTrue(tr1.getItem().equals(new Disease("cancer2", false, LocalDate.parse("2018-04-18"))));
        TableRow tr2 = lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(2).query();
        assertTrue(tr2.getItem().equals(new Disease("cancer", false, LocalDate.parse("2018-04-17"))));
    }

    /**
     * The diseases should be ordered automatically by chronic status and then by date in descending order. Their
     * chronic status has higher priority over the date of diagnosis.
     * <p>
     * First Disease should be added to the top of the table as it is both chronic and most recent. Next two disease
     * will be added below it, but with the last one being ahead of the first and is it more recent.
     */
    @Test
    @Ignore
    public void addMixedChronicDiseaseTestDefaultSortAddInReverse() {
        addNewDisease("cancer", "17/04/2018", true);
        addNewDisease("cancer2", "18/04/2018", false);
        addNewDisease("cancer3", "19/04/2018", false);
        TableRow tr0 = lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(0).query();
        assertTrue(tr0.getItem().equals(new Disease("cancer", true, LocalDate.parse("2018-04-17"))));
        TableRow tr1 = lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(1).query();
        assertTrue(tr1.getItem().equals(new Disease("cancer3", false, LocalDate.parse("2018-04-19"))));
        TableRow tr2 = lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(2).query();
        assertTrue(tr2.getItem().equals(new Disease("cancer2", false, LocalDate.parse("2018-04-18"))));
    }

    /**
     * The diseases should be ordered automatically by chronic status and then by date in descending order. Their
     * chronic status has higher priority over the date of diagnosis.
     * <p>
     * First Disease should be added to the top of the table as it is the only one. It is non-chronic. Second disease
     * should be above the first as it is non-chronic and is more recent than the first. Finally, the last disease
     * should be added to the top as it is chronic and the date should not matter.
     */
    @Test
    @Ignore
    public void addMixedChronicDiseaseTestDefaultSortAddNonChronicFirstInReverse() {
        addNewDisease("cancer", "17/04/2018", false);
        addNewDisease("cancer2", "18/04/2018", false);
        addNewDisease("cancer3", "19/04/2018", true);
        TableRow tr0 = lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(0).query();
        assertTrue(tr0.getItem().equals(new Disease("cancer3", true, LocalDate.parse("2018-04-19"))));
        TableRow tr1 = lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(1).query();
        assertTrue(tr1.getItem().equals(new Disease("cancer2", false, LocalDate.parse("2018-04-18"))));
        TableRow tr2 = lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(2).query();
        assertTrue(tr2.getItem().equals(new Disease("cancer", false, LocalDate.parse("2018-04-17"))));
    }

    @Test
    public void cureDiseasePass() {
        addNewDisease("Test Disease", "17/04/2018", false);
        clickOn((Node) lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(0).query());
        clickOn("#moveDiseaseToPastDiseasesButton");
        TableView pastDiseaseTable = lookup("#pastDiseaseTableView").query();
        assertTrue(pastDiseaseTable.getItems()
                .contains(new Disease("Test Disease", false, LocalDate.parse("2018-04-17"))));
    }

    @Test
    public void cureDiseaseFail() {
        addNewDisease("Test Disease", "17/04/2018", true);
        clickOn((Node) lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(0).query());
        clickOn("#moveDiseaseToPastDiseasesButton");
        alertDialogHasHeaderAndContent("Error. Cannot move disease!",
                "Disease must not be chronic if it is to be moved to the past diseases.");
        press(KeyCode.ENTER);
        release(KeyCode.ENTER);
        TableView pastDiseaseTable = lookup("#pastDiseaseTableView").query();
        assertFalse(
                pastDiseaseTable.getItems().contains(new Disease("Test Disease", true, LocalDate.parse("2018-04-17"))));
    }

    @Test
    @Ignore
    public void cureDiseaseAutoReorderAddMoreRecentDate() {
        addNewDisease("Test Disease", "17/04/2018", false);
        clickOn((Node) lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(0).query());
        clickOn("#moveDiseaseToPastDiseasesButton");
        addNewDisease("Test Disease2", "18/04/2018", false);
        clickOn((Node) lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(0).query());
        clickOn("#moveDiseaseToPastDiseasesButton");

        TableRow tr0 = lookup("#pastDiseaseTableView").lookup(".table-row-cell").nth(0).query();
        assertTrue(tr0.getItem().equals(new Disease("Test Disease2", false, LocalDate.parse("2018-04-18"))));
        TableRow tr1 = lookup("#pastDiseaseTableView").lookup(".table-row-cell").nth(1).query();
        assertTrue(tr1.getItem().equals(new Disease("Test Disease", false, LocalDate.parse("2018-04-17"))));
    }

    @Test
    public void cureChronicDiseaseFail() {
        addNewDisease("cancer", "15/04/2018", true);
        clickOn((Node) lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(0).query());
        clickOn("#moveDiseaseToPastDiseasesButton");
        alertDialogHasHeaderAndContent("Error. Cannot move disease!",
                "Disease must not be chronic if it is to be moved to the past diseases.");
    }

    @Test
    @Ignore
    public void deleteDiseasePass() {
        addNewDisease("Test Disease", "17/04/2018", false);
        clickOn((Node) lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(0).query());
        rightClickOn((Node) lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(0).query());
        rightClickOn((Node) lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(0).query());
        clickOn((Node) lookup(".menu-item").nth(1).query());
        TableView currentDiseaseTable = lookup("#currentDiseaseTableView").query();
        assertTrue(currentDiseaseTable.getItems().size() == 0);
    }

    @Test
    @Ignore
    public void updateDiseaseAutoFillFields() {
        addNewDisease("Test Disease", "17/04/2018", true);
        showUpdateDiseaseDialog(0);
        TextField diseaseName = lookup("#updateDiseaseNameTextField").query();
        assertTrue(diseaseName.getText().equals("Test Disease"));
        CheckBox isChronicCheckBox = lookup("#updateChronicCheckBox").query();
        assertTrue(isChronicCheckBox.isSelected());
        DatePicker datePicker = lookup("#updateDateOfDiagnosisDatePicker").query();
        assertTrue(datePicker.getValue().equals(LocalDate.parse("2018-04-17")));
    }

    @Test
    @Ignore
    public void updateDiseaseNamePass() {
        addNewDisease("Test Disease", "17/04/2018", true);
        showUpdateDiseaseDialog(0);

        clickOn("#updateDiseaseNameTextField");
        clearField();
        write("Test Disease Edit");
        clickOn("#updateSaveButton");

        TableRow tr0 = lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(0).query();
        assertTrue(tr0.getItem().equals(new Disease("Test Disease Edit", true, LocalDate.parse("2018-04-17"))));
    }

    @Test
    @Ignore
    public void updateDiseaseDatePass() {
        addNewDisease("Test Disease", "17/04/2018", true);
        showUpdateDiseaseDialog(0);

        clickOn("#updateDateOfDiagnosisDatePicker");
        clearField();
        write("14/04/2018");
        clickOn("#updateSaveButton");

        TableRow tr0 = lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(0).query();
        assertTrue(tr0.getItem().equals(new Disease("Test Disease", true, LocalDate.parse("2018-04-14"))));
    }

    @Test
    @Ignore
    public void updateDiseaseChronicPass() {
        addNewDisease("Test Disease", "17/04/2018", false);
        showUpdateDiseaseDialog(0);

        clickOn("#updateChronicCheckBox");
        clickOn("#updateSaveButton");

        TableRow tr0 = lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(0).query();
        assertTrue(tr0.getItem().equals(new Disease("Test Disease", true, LocalDate.parse("2018-04-17"))));
    }

    @Test
    @Ignore
    public void updateDiseaseFailDobAfterToday() {
        addNewDisease("Test Disease", "01/01/2018", false);
        showUpdateDiseaseDialog(0);

        clickOn("#updateDateOfDiagnosisDatePicker");
        clearField();
        write("14/04/4999");
        clickOn("#updateSaveButton");

        alertDialogHasHeaderAndContent("Error", "Invalid date of diagnosis");
    }

    @Test
    @Ignore
    public void updateDiseaseEmptyDiseaseName() {
        addNewDisease("Test Disease", "01/01/2018", false);
        showUpdateDiseaseDialog(0);

        clickOn("#updateDiseaseNameTextField");
        clearField();
        clickOn("#updateSaveButton");

        alertDialogHasHeaderAndContent("Error", "Invalid disease name");
    }

    /**
     * This test is checking whether you have 2 non-chronic diseases and when you update one of them to be chronic it
     * will appear above the other one. The chronic disease will be diagnosed after the non-chronic one to ensure this
     * is working as intended.
     */
    @Test
    @Ignore
    public void updateDiseaseAutoReorderToChronic() {
        addNewDisease("cancer", "17/04/2018", false);
        addNewDisease("cancer2", "18/04/2018", false);

        showUpdateDiseaseDialog(1);
        clickOn("#updateChronicCheckBox");
        clickOn("#updateSaveButton");

        TableRow tr0 = lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(0).query();
        assertTrue(tr0.getItem().equals(new Disease("cancer", true, LocalDate.parse("2018-04-17"))));

        TableRow tr1 = lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(1).query();
        assertTrue(tr1.getItem().equals(new Disease("cancer2", false, LocalDate.parse("2018-04-18"))));
    }

    @Test
    @Ignore
    public void updateDiseaseAutoReorderNonChronic() {
        addNewDisease("cancer", "15/04/2018", false);
        addNewDisease("cancer2", "17/04/2018", false);

        showUpdateDiseaseDialog(1);
        clickOn("#updateDateOfDiagnosisDatePicker");
        clearField();
        write("18/04/2018");
        clickOn("#updateSaveButton");

        TableRow tr0 = lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(0).query();
        assertTrue(tr0.getItem().equals(new Disease("cancer", false, LocalDate.parse("2018-04-18"))));

        TableRow tr1 = lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(1).query();
        assertTrue(tr1.getItem().equals(new Disease("cancer2", false, LocalDate.parse("2018-04-15"))));
    }

    @Test
    @Ignore
    public void updateDiseaseAutoReorderBothChronic() {
        addNewDisease("cancer", "15/04/2018", true);
        addNewDisease("cancer2", "17/04/2018", true);

        showUpdateDiseaseDialog(1);
        clickOn("#updateDateOfDiagnosisDatePicker");
        clearField();
        write("18/04/2018");
        clickOn("#updateSaveButton");

        TableRow tr0 = lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(0).query();
        assertTrue(tr0.getItem().equals(new Disease("cancer", true, LocalDate.parse("2018-04-18"))));

        TableRow tr1 = lookup("#currentDiseaseTableView").lookup(".table-row-cell").nth(1).query();
        assertTrue(tr1.getItem().equals(new Disease("cancer2", true, LocalDate.parse("2018-04-15"))));
    }
}
