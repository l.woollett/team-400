package GUITests;

import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.testfx.api.FxAssert.verifyThat;
import static org.testfx.matcher.base.NodeMatchers.isVisible;

/**
 * This is the test class of Javafx - clinicianTransplantWaitingTab and it's controller class
 */
@Ignore
public class ClinicianTransplantWaitingTest extends TestFXBase {

    /**
     * Simulate a click on medications tab in profile.fxml
     */
    private void clickMedicationsTab() {
        clickOn("#clinicianTransplantWaitingTab");
    }

    /**
     * Test with an non-empty transplant waiting list.The value has not been set up in the parent class - TextFxBase
     */
    @Test
    public void verityColumnsVisible() {
        loginAsDefaultClinicianUser();
        clickMedicationsTab();
        TableView currentWaitingList = lookup("#clinicianTransplantWaitingList").query();
        assertEquals(currentWaitingList.getColumns().size(), 6);
    }

    /**
     * Test with an non-empty transplant waiting list.The value has not been set up in the parent class - TextFxBase
     */
    @Test
    public void noneEmptyTransplantWaitingListTest() {
        loginAsDefaultClinicianUser();
        clickMedicationsTab();
        TableView currentWaitingList = lookup("#clinicianTransplantWaitingList").query();
        assertEquals(currentWaitingList.getItems().size(), 4);
    }

    /**
     * Test with an non-empty transplant waiting list. By selecting the first record on the
     * clinicianTransplantWaitingList, double click the record opens up the related profile page. The value has not been
     * set up in the parent class - TextFxBase
     */
    @Test
    public void openDonorOnTransplantWaitingListTest() {
        loginAsDefaultClinicianUser();
        clickMedicationsTab();
        moveBy(10, 90);
        doubleClickOn(MouseButton.PRIMARY);
        verifyThat("#mainAnchorPane", isVisible());

    }

    /**
     * Test with selecting an option in the organ list, there are organs in the list as expected number, 2.
     */
    @Test
    public void filterByAvailableOrganTest() {
        loginAsDefaultClinicianUser();
        clickMedicationsTab();
        clickOn("#clinicianOrganComboBox");
        type(KeyCode.DOWN);
        type(KeyCode.ENTER);
        TableView currentWaitingList = lookup("#clinicianTransplantWaitingList").query();
        assertEquals(2, currentWaitingList.getItems().size());
    }

    /**
     * Test with selecting an option in the organ list, there are organs in the list as expected number, 0.
     */
    @Test
    public void filterByNoneAvailableOrganTest() {
        loginAsDefaultClinicianUser();
        clickMedicationsTab();
        clickOn("#clinicianOrganComboBox");
        type(KeyCode.DOWN);
        type(KeyCode.DOWN);
        type(KeyCode.ENTER);
        TableView currentWaitingList = lookup("#clinicianTransplantWaitingList").query();
        assertEquals(0, currentWaitingList.getItems().size());
    }

    /**
     * Test with selecting an option in the region list, there are organs in the list as expected number, 4.
     */
    @Test
    public void filterByAvailableRegionTest() {
        loginAsDefaultClinicianUser();
        clickMedicationsTab();
        clickOn("#clinicianRegionComboBox");
        type(KeyCode.DOWN);
        type(KeyCode.ENTER);
        TableView currentWaitingList = lookup("#clinicianTransplantWaitingList").query();
        assertEquals(4, currentWaitingList.getItems().size());
    }

    /**
     * Test with selecting an option in the organ list, there are organs in the list as expected number, 0.
     */
    @Test
    public void filterByNoneAvailableRegionTest() {
        loginAsDefaultClinicianUser();
        clickMedicationsTab();
        clickOn("#clinicianRegionComboBox");
        type(KeyCode.DOWN);
        type(KeyCode.DOWN);
        type(KeyCode.ENTER);
        TableView currentWaitingList = lookup("#clinicianTransplantWaitingList").query();
        assertEquals(0, currentWaitingList.getItems().size());
    }

    /**
     * Test with selecting an option in the organ list and region list, there are records in the list as expected
     * number, 2.
     */
    @Test
    public void filterByAvailableOrganAndRegionTest() {
        loginAsDefaultClinicianUser();
        clickMedicationsTab();
        clickOn("#clinicianOrganComboBox");
        type(KeyCode.DOWN);
        type(KeyCode.ENTER);
        clickOn("#clinicianRegionComboBox");
        type(KeyCode.DOWN);
        type(KeyCode.ENTER);
        TableView currentWaitingList = lookup("#clinicianTransplantWaitingList").query();
        assertEquals(2, currentWaitingList.getItems().size());
    }

    /**
     * Test with selecting an option in the organ list and region list, there are records in the list as expected
     * number, 0.
     */
    @Test
    public void filterByNoneAvailableOrganAndRegionTest() {
        loginAsDefaultClinicianUser();
        clickMedicationsTab();
        clickOn("#clinicianOrganComboBox");
        type(KeyCode.DOWN);
        type(KeyCode.DOWN);
        type(KeyCode.ENTER);
        clickOn("#clinicianRegionComboBox");
        type(KeyCode.DOWN);
        type(KeyCode.DOWN);
        type(KeyCode.ENTER);
        TableView currentWaitingList = lookup("#clinicianTransplantWaitingList").query();
        assertEquals(0, currentWaitingList.getItems().size());
    }

    /**
     * Test with selecting an option in the organ list and region list, then clear out the filters by click on the clear
     * filter button. There are organs in the list as expected number, 4.
     */
    @Test
    public void clearFilterTest() {
        filterByAvailableOrganAndRegionTest();
        clickOn("#clearFilterButton");
        TableView currentWaitingList = lookup("#clinicianTransplantWaitingList").query();
        assertEquals(4, currentWaitingList.getItems().size());
    }
}
