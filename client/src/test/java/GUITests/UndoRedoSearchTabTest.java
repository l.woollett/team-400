package GUITests;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.junit.Ignore;
import org.junit.Test;
import seng302.Enum.GenderEnum;
import seng302.Enum.OrganEnum;
import seng302.Enum.RegionEnum;
import seng302.Enum.RoleEnum;
import seng302.GUI.Clinician.ClinicianSceneController;

import java.io.IOException;
import java.util.logging.Logger;

import static junit.framework.TestCase.assertEquals;

@Ignore
public class UndoRedoSearchTabTest extends TestFXBase {

    private Logger LOGGER = Logger.getLogger(UndoRedoSearchTabTest.class.getName());
    @FXML
    private ClinicianSceneController controller;
    @FXML
    private Button undo, redo;

    @Override
    public void start(Stage stage) {
        FXMLLoader loader = new FXMLLoader(ClinicianSceneController.class.getResource("clinician.fxml"));
        Parent mainRoot = null;
        try {
            mainRoot = loader.load();
        } catch (IOException e) {
            LOGGER.severe(e.getMessage());
        }
        if (mainRoot != null) {
            Scene scene = new Scene(mainRoot);
            stage.setScene(scene);
            stage.show();
        }
        controller = loader.getController();
        undo = lookup("#undo").query();
        redo = lookup("#redo").query();
    }

    @Test
    public void undoRedoOnTextFieldsTest() {
        clickOn("#clinicianTransplantWaitingTab");

        TextField resultTxt = lookup("#resultTxt").query();
        resultTxt.setText("a");
        TextField ageFilter = lookup("#ageFilter").query();
        ageFilter.setText("1");
        undo.fire();
        assertEquals(3, controller.getUndoRedoController().getUndoDequeSize());
        redo.fire();
        assertEquals(4, controller.getUndoRedoController().getUndoDequeSize());
    }

    @Test
    public void undoRedoOnChoiceboxFieldsTest() {
        clickOn("#clinicianTransplantWaitingTab");

        ChoiceBox<Enum> cmbPreferredGenderItems = lookup("#cmbPreferredGenderItems").query();
        cmbPreferredGenderItems.setValue(GenderEnum.MALE);
        ChoiceBox<Enum> cmbBirthGenderItems = lookup("#cmbBirthGenderItems").query();
        cmbBirthGenderItems.setValue(GenderEnum.MALE);
        ChoiceBox<Enum> cmbRegionItems = lookup("#cmbRegionItems").query();
        cmbRegionItems.setValue(RegionEnum.CANTERBURY);
        ChoiceBox<Enum> cmbDonorItems = lookup("#cmbDonorItems").query();
        cmbDonorItems.setValue(RoleEnum.DONOR);
        ChoiceBox<Enum> cmbDonorOrganItems = lookup("#cmbDonorOrganItems").query();
        cmbDonorOrganItems.setValue(OrganEnum.LIVER);
        ChoiceBox<Enum> cmbReceivingOrganItems = lookup("#cmbReceivingOrganItems").query();
        cmbReceivingOrganItems.setValue(OrganEnum.LIVER);
        undo.fire();
        undo.fire();
        assertEquals(5, controller.getUndoRedoController().getUndoDequeSize());
        redo.fire();
        assertEquals(6, controller.getUndoRedoController().getUndoDequeSize());

    }

    @Test
    public void undoRedoOnCombinationOfBothFieldsTest() {
        clickOn("#clinicianTransplantWaitingTab");
        TextField resultTxt = lookup("#resultTxt").query();
        resultTxt.setText("a");
        TextField ageFilter = lookup("#ageFilter").query();
        ageFilter.setText("1");
        ChoiceBox<Enum> cmbPreferredGenderItems = lookup("#cmbPreferredGenderItems").query();
        cmbPreferredGenderItems.setValue(GenderEnum.MALE);
        ChoiceBox<Enum> cmbBirthGenderItems = lookup("#cmbBirthGenderItems").query();
        cmbBirthGenderItems.setValue(GenderEnum.MALE);
        undo.fire();
        undo.fire();
        assertEquals(4, controller.getUndoRedoController().getUndoDequeSize());
        redo.fire();
        assertEquals(5, controller.getUndoRedoController().getUndoDequeSize());

    }
}
