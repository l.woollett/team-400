package GUITests;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;
import org.junit.Ignore;
import org.junit.Test;
import seng302.CustomException.DoesNotExist;
import seng302.GUI.LoginSceneController;
import seng302.Model.Profile;
import seng302.ModelController.ProfileController;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

import static org.hamcrest.CoreMatchers.isA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.testfx.api.FxAssert.verifyThat;

/**
 * A class to test in creating a new user, with positive and negative tests
 */
@Ignore
public class CreateUserTest extends TestFXBase {

    /**
     * Start test on the given scene - createUserLogin.fxml Reset width and height size for this page, so all nodes will be
     * visible on Windows and Linux
     */
    @Override
    public void start(Stage stage) {

        try {
            FXMLLoader loader = new FXMLLoader(LoginSceneController.class.getResource("createUserLogin.fxml"));
            Parent mainRoot = loader.load();
            Scene scene = new Scene(mainRoot, 700, 800);
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Test that we can create a user with the minimum fields.
     */
    @Test
    @Ignore
    public void createProfileSuccessfulMinimalFieldsTest() {
        assertEquals(3, ProfileController.getProfiles().size());
        clickOn("#usernameTextField").write("userName");
        clickOn("#firstNameTextField").write("firstName");
        DatePicker dob = lookup("#dateOfBirthDatePicker").query();
        dob.setValue(LocalDate.of(1900, 10, 1));
        clickOn("#createUserButton");
        assertEquals(4, ProfileController.getProfiles().size());
    }

    /**
     * Test that we can create a user with the minimum fields with alert successful
     */
    @Test
    @Ignore
    public void createProfileSuccessfulMinimalFieldsWithAlertTest() {
        createProfileSuccessfulMinimalFieldsTest();
        press(KeyCode.ENTER);
        release(KeyCode.ENTER);
        assertEquals(4, ProfileController.getProfiles().size());

    }

    /**
     * Test that we can create a user with some missing required field first to verify with the alert message, then
     * complete all required field and check with alert successful
     */
    @Test
    @Ignore
    public void createProfileWithAlertsTest() {
        clickOn("#usernameTextField").write("userName");
        clickOn("#createUserButton");
        alertDialogHasHeaderAndContent(null, "You must complete the mandatory fields.");
        press(KeyCode.ENTER);
        release(KeyCode.ENTER);
        assertEquals(3, ProfileController.getProfiles().size());
    }

    /**
     * Test that we can create a user with all fields.
     */
    @Test
    @Ignore
    public void createProfileSuccessfulAllFieldsTest() {
        assertEquals(3, ProfileController.getProfiles().size());

        clickOn("#usernameTextField").write("userName");
        clickOn("#firstNameTextField").write("firstName");
        clickOn("#middleNameTextField").write("middleName");
        clickOn("#lastNameTextField").write("lastName");
        DatePicker dob = lookup("#dateOfBirthDatePicker").query();
        dob.setValue(LocalDate.of(1900, 10, 1));
        String today = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
        clickOn("#dateOfDeathDatePicker");
        clearField();
        write(today);

        clickOn("#genderChoiceBox").clickOn("Male");
        clickOn("#bloodTypeChoiceBox").clickOn("A+");
        clickOn("#heightTextField").write("1.68");
        clickOn("#weightTextField").write("70.09");
        clickOn("#addressTextField").write("Ilam Road");
        clickOn("#regionChoiceBox").clickOn("Canterbury");
        clickOn("#boneCheckBox");

        clickOn("#createUserButton");
        try {
            verifyThat(ProfileController.getProfile("userName"), isA(Profile.class));
            assertEquals(4, ProfileController.getProfiles().size());
        } catch (DoesNotExist doesNotExist) {
            fail(doesNotExist.getMessage());
        }
    }

    /**
     * Test if we can click the cancel button and user won't be created.
     */
    @Test
    public void createProfileCancelled() {
        clickOn("#usernameTextField").write("Mike123");
        clickOn("#firstNameTextField").write("Mike");
        clickOn("#cancelButton");
        assertEquals(3, ProfileController.getProfiles().size());
    }
}
